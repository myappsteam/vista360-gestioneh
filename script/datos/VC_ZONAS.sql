/*
 Navicat Premium Data Transfer

 Source Server         : ORACLE
 Source Server Type    : Oracle
 Source Server Version : 120200
 Source Host           : localhost:1521
 Source Schema         : VISTA360

 Target Server Type    : Oracle
 Target Server Version : 120200
 File Encoding         : 65001

 Date: 26/10/2020 17:07:46
*/


-- ----------------------------
-- Table structure for VC_ZONAS
-- ----------------------------
DROP TABLE "VISTA360"."VC_ZONAS";
CREATE TABLE "VISTA360"."VC_ZONAS" (
  "ID_VC_ZONAS" NUMBER(8,2) VISIBLE NOT NULL ,
  "NOMBRE" VARCHAR2(100 BYTE) VISIBLE ,
  "OBSERVACION" VARCHAR2(100 BYTE) VISIBLE ,
  "ESTADO" NUMBER(8,2) VISIBLE NOT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of VC_ZONAS
-- ----------------------------
INSERT INTO "VISTA360"."VC_ZONAS" VALUES ('1', 'Oruro', NULL, '1');
INSERT INTO "VISTA360"."VC_ZONAS" VALUES ('2', 'Cochabamba', NULL, '1');
INSERT INTO "VISTA360"."VC_ZONAS" VALUES ('3', 'Villamontes', NULL, '1');
INSERT INTO "VISTA360"."VC_ZONAS" VALUES ('4', 'Yacuiba', NULL, '1');
INSERT INTO "VISTA360"."VC_ZONAS" VALUES ('5', 'Potosi', NULL, '1');
INSERT INTO "VISTA360"."VC_ZONAS" VALUES ('6', 'Pando', NULL, '1');
INSERT INTO "VISTA360"."VC_ZONAS" VALUES ('7', 'Camiri', NULL, '1');
INSERT INTO "VISTA360"."VC_ZONAS" VALUES ('8', 'Puerto Suarez', NULL, '1');
INSERT INTO "VISTA360"."VC_ZONAS" VALUES ('9', 'Sucre', NULL, '1');
INSERT INTO "VISTA360"."VC_ZONAS" VALUES ('10', 'La Paz', NULL, '1');
INSERT INTO "VISTA360"."VC_ZONAS" VALUES ('11', 'Beni', NULL, '1');
INSERT INTO "VISTA360"."VC_ZONAS" VALUES ('12', 'Tarija', NULL, '1');
INSERT INTO "VISTA360"."VC_ZONAS" VALUES ('13', 'Santa Cruz', NULL, '1');

-- ----------------------------
-- Primary Key structure for table VC_ZONAS
-- ----------------------------
ALTER TABLE "VISTA360"."VC_ZONAS" ADD CONSTRAINT "PK_VC_ZONAS" PRIMARY KEY ("ID_VC_ZONAS");

-- ----------------------------
-- Checks structure for table VC_ZONAS
-- ----------------------------
ALTER TABLE "VISTA360"."VC_ZONAS" ADD CONSTRAINT "SYS_C0067744" CHECK ("ID_VC_ZONAS" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "VISTA360"."VC_ZONAS" ADD CONSTRAINT "SYS_C0067745" CHECK ("ESTADO" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
