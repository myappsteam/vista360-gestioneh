package com.myapps.vista_ccc.servicio;

import com.huawei.www.bme.cbsinterface.uvcservices.UvcServices_ServiceLocator;
import com.huawei.www.bme.cbsinterface.uvcservices.UvcServicsBindingStub;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.UtilUrl;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class ServicioVoucher implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static Logger log = Logger.getLogger(ServicioVoucher.class);

	public static Map<String, NodoServicio<UvcServicsBindingStub>> ports = new HashMap<String, NodoServicio<UvcServicsBindingStub>>();

	public static synchronized NodoServicio<UvcServicsBindingStub> initPort(String url, String username, int timeout) throws Exception {
		long ini = System.currentTimeMillis();
		url = UtilUrl.getIp(url);
		long fin = System.currentTimeMillis();
		log.debug("[username: " + username + ", url: " + url + "] Tiempo de respuesta para obtener ip: " + (fin - ini) + " milisegundos");

		ini = System.currentTimeMillis();

		NodoServicio<UvcServicsBindingStub> port = ports.get(username);

		if (port == null) {
			log.debug("Creando un nuevo port ... " + username);
			port = getService(url, username, timeout);
			port.setPrimeraVez(true);
		} else {
			log.debug("Ya existe el port creado ... " + username);
			port.setEnUso(true);
			port.setFechaInicio(new Date());
			port.setFechaFin(null);
			port.setPrimeraVez(false);
		}
		fin = System.currentTimeMillis();
		
		port.setTiempoConexion(fin-ini);
		
		log.debug("[username: " + username + "] Tiempo de respuesta para obtener conexion: " + (fin - ini) + " milisegundos");
		return port;
	}

	public static synchronized NodoServicio<UvcServicsBindingStub> getService(String url, String username, int timeout) throws Exception {

		NodoServicio<UvcServicsBindingStub> port = ports.get(username);

		if (port == null) {

			UvcServices_ServiceLocator service = new UvcServices_ServiceLocator(url);

			log.debug("url UVCService: " + service.getUvcServicesPortAddress());

			UvcServicsBindingStub portStub = (UvcServicsBindingStub) service.getUvcServicesPort();
			portStub.setTimeout(timeout);

			port = new NodoServicio<UvcServicsBindingStub>();
			port.setPort(portStub);
			port.setKey(username);

			port.setEnUso(true);
			port.setFechaInicio(new Date());
			port.setFechaFin(null);

			ports.put(username, port);

		}

		return port;
	}

	public static synchronized void liberar(String key) {
		if (ports != null) {
			synchronized (ports) {
				ArrayList<String> listaEliminar = new ArrayList<>();

				for (Entry<String, NodoServicio<UvcServicsBindingStub>> entry : ports.entrySet()) {
					if (entry.getKey().endsWith(key)) {
						listaEliminar.add(entry.getKey());
					}
				}
				int sizeAntes = ports.size();
				for (String item : listaEliminar) {
					ports.remove(item);
				}
				int sizeDespues = ports.size();
				log.debug("Eliminados del hashmap UVCService [key: " + key + "][size antes: " + sizeAntes + ", size despues: " + sizeDespues + "]: " + listaEliminar.size() + " conexiones : " + listaEliminar);
			}
		}
	}

	public static synchronized void liberarPorTimeOut() {
		if (ports != null) {
			synchronized (ports) {
				ArrayList<String> listaEliminar = new ArrayList<>();

				for (Entry<String, NodoServicio<UvcServicsBindingStub>> entry : ports.entrySet()) {

					if (!entry.getValue().isEnUso() && entry.getValue().getFechaFin() != null) {
						long milisegundos = (new Date()).getTime() - entry.getValue().getFechaFin().getTime();
						long segundos = milisegundos / 1000;
						long minutos = segundos / 60;

						if (minutos >= Parametros.timpoFueraPoolConexion) {
							listaEliminar.add(entry.getKey());
						}
					}
				}

				int sizeAntes = ports.size();
				for (String item : listaEliminar) {
					ports.remove(item);
				}
				int sizeDespues = ports.size();

				log.debug("Eliminados por time out de la cola de conexiones activas UVCService [size antes: " + sizeAntes + ", size despues: " + sizeDespues + "]: " + listaEliminar.size() + " conexiones : " + listaEliminar);
			}
		}
	}
}
