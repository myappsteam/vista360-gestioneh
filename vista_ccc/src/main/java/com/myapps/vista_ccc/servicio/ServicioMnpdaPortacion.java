package com.myapps.vista_ccc.servicio;

import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.UtilUrl;
import myapps.org.soap.mnppdaportacion.MNPPortacion;
import myapps.org.soap.mnppdaportacion.MNPPortacionPortStub;
import myapps.org.soap.mnppdaportacion.MNPPortacionServiceLocator;
import org.apache.log4j.Logger;

import javax.xml.rpc.ServiceException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class ServicioMnpdaPortacion implements Serializable {
	
	private static final long serialVersionUID = 1L;
	public static Logger LOG = Logger.getLogger(ServicioMnpdaPortacion.class);
	public static Map<String, NodoServicio<MNPPortacionPortStub>> mapsPort = new HashMap<String, NodoServicio<MNPPortacionPortStub>>();
	
	public static synchronized NodoServicio<MNPPortacionPortStub> initPort(String url, String userName,
			int timeOut) throws Exception {

		long ini = System.currentTimeMillis();
		url = UtilUrl.getIp(url);
		long fin = System.currentTimeMillis();
		LOG.debug("[username: " + userName + ", url: " + url + "] Tiempo de respuesta para obtener ip: " + (fin - ini)
				+ " milisegundos");

		ini = System.currentTimeMillis();
		
		
		

		NodoServicio<MNPPortacionPortStub> port = mapsPort.get(userName);

		if (port == null) {
			LOG.debug("Creando un nuevo port ... " + userName);
			port = getServices(url, userName, timeOut);
			port.setPrimeraVez(true);
		} else {
			LOG.debug("Ya existe el port creado ... " + userName);
			port.setEnUso(true);
			port.setFechaInicio(new Date());
			port.setFechaFin(null);
			port.setPrimeraVez(false);
		}

		fin = System.currentTimeMillis();
		port.setTiempoConexion(fin - ini);

		LOG.debug("[username: " + userName + "] Tiempo de respuesta para obtener conexion: " + (fin - ini)
				+ " milisegundos");

		return port;
	}
	
	public static synchronized NodoServicio<MNPPortacionPortStub> getServices(String url, String userName,
			int timeOut) throws ServiceException {

		NodoServicio<MNPPortacionPortStub> port = mapsPort.get(userName);
		if (port == null) {

			MNPPortacionServiceLocator services = new MNPPortacionServiceLocator();
			services.setMNPPortacionPortBindingEndpointAddress(url);
						
			LOG.debug("url mnpdaPortacion: " + services.getMNPPortacionPortBindingAddress());
			MNPPortacionPortStub portStub = (MNPPortacionPortStub) services.getPort(MNPPortacion.class);
			
			portStub.setTimeout(timeOut);

			port = new NodoServicio<MNPPortacionPortStub>();
			port.setPort(portStub);
			port.setKey(userName);
			port.setEnUso(true);
			port.setFechaInicio(new Date());
			port.setFechaFin(null);

			mapsPort.put(userName, port);
		}
		return port;
	}
	
	public static synchronized void liberar(String key) {
		if (mapsPort != null) {
			synchronized (mapsPort) {
				ArrayList<String> listaEliminar = new ArrayList<>();

				for (Entry<String, NodoServicio<MNPPortacionPortStub>> entry : mapsPort.entrySet()) {
					if (entry.getKey().endsWith(key)) {
						listaEliminar.add(entry.getKey());
					}
				}

				int sizeAntes = mapsPort.size();
				for (String item : listaEliminar) {
					mapsPort.remove(item);
				}
				int sizeDespues = mapsPort.size();

				LOG.debug("Eliminados del hashmap MnpdaPortacion [key: " + key + "][size antes: " + sizeAntes
						+ ", size despues: " + sizeDespues + "]: " + listaEliminar.size() + " conexiones : "
						+ listaEliminar);
			}
		}
	}
	
	public static void liberarPorTimeOut() {
		if (mapsPort != null) {
			synchronized (mapsPort) {
				ArrayList<String> listaEliminar = new ArrayList<>();

				for (Entry<String, NodoServicio<MNPPortacionPortStub>> entry : mapsPort.entrySet()) {

					if (!entry.getValue().isEnUso() && entry.getValue().getFechaFin() != null) {
						long milisegundos = (new Date()).getTime() - entry.getValue().getFechaFin().getTime();
						long segundos = milisegundos / 1000;
						long minutos = segundos / 60;

						if (minutos >= Parametros.timpoFueraPoolConexion) {
							listaEliminar.add(entry.getKey());
						}
					}
				}
				int sizeAntes = mapsPort.size();
				for (String item : listaEliminar) {
					mapsPort.remove(item);
				}
				int sizeDespues = mapsPort.size();

				LOG.debug("Eliminados por time out de la cola de conexiones activas mnpdaPortacion [size antes: " + sizeAntes
						+ ", size despues: " + sizeDespues + "]: " + listaEliminar.size() + " conexiones : "
						+ listaEliminar);
			}
		}
	}
	

}
