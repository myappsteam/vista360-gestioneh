package com.myapps.vista_ccc.servicio;

import java.io.Serializable;

public class TimeOut implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String pagina;
	private Boolean ejecutando;

	public TimeOut() {
		super();
	}

	public TimeOut(String pagina, Boolean ejecutando) {
		super();
		this.pagina = pagina;
		this.ejecutando = ejecutando;
	}

	public String getPagina() {
		return pagina;
	}

	public void setPagina(String pagina) {
		this.pagina = pagina;
	}

	public Boolean getEjecutando() {
		return ejecutando;
	}

	public void setEjecutando(Boolean ejecutando) {
		this.ejecutando = ejecutando;
	}

	@Override
	public String toString() {
		return "TimeOut [pagina=" + pagina + ", ejecutando=" + ejecutando + "]";
	}

}
