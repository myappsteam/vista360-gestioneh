package com.myapps.vista_ccc.servicio;

import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.UtilUrl;
import com.myapps.www.WSPendingDebtsDetail.WSPendingDebtsDetailSoapBindingStub;
import com.myapps.www.WSPendingDebtsDetail.WSPendingDebtsDetail_ServiceLocator;
import org.apache.axis.message.SOAPHeaderElement;
import org.apache.log4j.Logger;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class ServicioPendingDebtsQuantity implements Serializable {

	/**
	 * 
	 */

	private static final long serialVersionUID = 1L;

	public static Logger log = Logger.getLogger(ServicioPendingDebtsQuantity.class);

	public static Map<String, NodoServicio<WSPendingDebtsDetailSoapBindingStub>> ports = new HashMap<String, NodoServicio<WSPendingDebtsDetailSoapBindingStub>>();

	public static synchronized NodoServicio<WSPendingDebtsDetailSoapBindingStub> initPort(String url, String username,
			int timeout, String user, String pass) throws Exception {
		log.debug("[url service deuda detallada ttp: " + url + "]");
		long ini = System.currentTimeMillis();
		url = UtilUrl.getIp(url);
		long fin = System.currentTimeMillis();
		log.debug("[username: " + username + ", url: " + url + "] Tiempo de respuesta para obtener ip: " + (fin - ini)
				+ " milisegundos");

		ini = System.currentTimeMillis();

		NodoServicio<WSPendingDebtsDetailSoapBindingStub> port = ports.get(username);

		if (port == null) {
			log.debug("Creando un nuevo port ... " + username);
			port = getService(url, username, timeout, user, pass);
			port.setPrimeraVez(true);
		} else {
			log.debug("Ya existe el port creado ... " + username);
			port.setEnUso(true);
			port.setFechaInicio(new Date());
			port.setFechaFin(null);
			port.setPrimeraVez(false);
		}
		fin = System.currentTimeMillis();

		port.setTiempoConexion(fin - ini);

		log.debug("[username: " + username + "] Tiempo de respuesta para obtener conexion: " + (fin - ini)
				+ " milisegundos");

		return port;
	}

	public static synchronized NodoServicio<WSPendingDebtsDetailSoapBindingStub> getService(String url, String username,
			int timeout, String user, String pass) throws Exception {

		NodoServicio<WSPendingDebtsDetailSoapBindingStub> port = ports.get(username);

		if (port == null) {

			WSPendingDebtsDetail_ServiceLocator service = new WSPendingDebtsDetail_ServiceLocator();

			log.debug("url PendingDebtsQuantity: " + service.getWSPendingDebtsDetailSOAPAddress());

			WSPendingDebtsDetailSoapBindingStub portStub = (WSPendingDebtsDetailSoapBindingStub) service
					.getWSPendingDebtsDetailSOAP(new URL(url));
			portStub.setTimeout(timeout);
			if (!user.equals("") && !pass.equals(""))
				addWsSecurityHeader(portStub, user, pass);
			port = new NodoServicio<WSPendingDebtsDetailSoapBindingStub>();
			port.setPort(portStub);
			port.setKey(username);

			port.setEnUso(true);
			port.setFechaInicio(new Date());
			port.setFechaFin(null);

			ports.put(username, port);

		}

		return port;
	}

	public static synchronized void liberar(String key) {
		if (ports != null) {
			synchronized (ports) {
				ArrayList<String> listaEliminar = new ArrayList<>();

				for (Entry<String, NodoServicio<WSPendingDebtsDetailSoapBindingStub>> entry : ports.entrySet()) {
					if (entry.getKey().endsWith(key)) {
						listaEliminar.add(entry.getKey());
					}
				}

				int sizeAntes = ports.size();
				for (String item : listaEliminar) {
					ports.remove(item);
				}
				int sizeDespues = ports.size();

				log.debug("Eliminados del hashmap PendingDebtsQuantity [key: " + key + "][size antes: " + sizeAntes
						+ ", size despues: " + sizeDespues + "]: " + listaEliminar.size() + " conexiones : "
						+ listaEliminar);
			}
		}
	}

	public static synchronized void liberarPorTimeOut() {
		if (ports != null) {
			synchronized (ports) {
				ArrayList<String> listaEliminar = new ArrayList<>();

				for (Entry<String, NodoServicio<WSPendingDebtsDetailSoapBindingStub>> entry : ports.entrySet()) {

					if (!entry.getValue().isEnUso() && entry.getValue().getFechaFin() != null) {
						long milisegundos = (new Date()).getTime() - entry.getValue().getFechaFin().getTime();
						long segundos = milisegundos / 1000;
						long minutos = segundos / 60;

						if (minutos >= Parametros.timpoFueraPoolConexion) {
							listaEliminar.add(entry.getKey());
						}
					}
				}

				int sizeAntes = ports.size();
				for (String item : listaEliminar) {
					ports.remove(item);
				}
				int sizeDespues = ports.size();

				log.debug("Eliminados por time out de la cola de conexiones activas PendingDebtsQuantity [size antes: "
						+ sizeAntes + ", size despues: " + sizeDespues + "]: " + listaEliminar.size() + " conexiones : "
						+ listaEliminar);
			}
		}
	}

	synchronized public static void addWsSecurityHeader(org.apache.axis.client.Stub binding, String wsUser,
			String wsPass) throws SOAPException {

		QName headerName = new QName(
				"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "Security");
		SOAPHeaderElement header = new SOAPHeaderElement(headerName);
		header.setActor(null);
		header.setPrefix("sp");
		header.setMustUnderstand(true);

		SOAPElement utElem = header.addChildElement("UsernameToken");
		SOAPElement userNameElem = utElem.addChildElement("Username");
		userNameElem.removeContents();
		userNameElem.setValue(wsUser);

		SOAPElement passwordElem = utElem.addChildElement("Password");
		passwordElem.setValue(wsPass);
		binding.setHeader(header);
	}

}
