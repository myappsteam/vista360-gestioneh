package com.myapps.vista_ccc.servicio;

import java.io.Serializable;
import java.util.Date;

public class NodoServicio<T> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String key;
	private boolean enUso;
	private T port;
	private Date fechaInicio;
	private Date fechaFin;
	private boolean primeraVez;
	private Long tiempoConexion;
	
	public Long getTiempoConexion() {
		return tiempoConexion;
	}

	public void setTiempoConexion(Long tiempoConexion) {
		this.tiempoConexion = tiempoConexion;
	}

	public boolean isPrimeraVez() {
		return primeraVez;
	}

	public void setPrimeraVez(boolean primeraVez) {
		this.primeraVez = primeraVez;
	}

	public NodoServicio() {
		super();
	}

	public NodoServicio(String key, boolean enUso, T port, Date fechaInicio, Date fechaFin, boolean primeraVez) {
		super();
		this.key = key;
		this.enUso = enUso;
		this.port = port;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.primeraVez = primeraVez;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public boolean isEnUso() {
		return enUso;
	}

	public void setEnUso(boolean enUso) {
		this.enUso = enUso;
	}

	public T getPort() {
		return port;
	}

	public void setPort(T port) {
		this.port = port;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	@Override
	public String toString() {
		return "NodoServicio [key=" + key + ", enUso=" + enUso + ", port=" + port + ", fechaInicio=" + fechaInicio + ", fechaFin=" + fechaFin + "]";
	}

}
