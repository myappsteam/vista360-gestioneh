package com.myapps.vista_ccc.servicio;

import com.myapps.detalle_navegacion.servicio.ServicioDetalleNavegacion;
import org.apache.log4j.Logger;

public class ThreadServicio extends Thread {

	public static Logger log = Logger.getLogger(ThreadServicio.class);

	private boolean live;
	private TimeOut timeOut;

	public ThreadServicio(TimeOut timeout) {
		super();
		this.timeOut = timeout;
	}

	public void run() {
		this.live = true;
		while (live) {

			try {
				log.debug("Iniciando liberacion de cola de conexiones activas: " + timeOut.getPagina());
				ServicioBilletera.liberarPorTimeOut();
				ServicioCallingCircle.liberarPorTimeOut();
				ServicioCustomized.liberarPorTimeOut();
				ServicioHistorial.liberarPorTimeOut();
				ServicioVoucher.liberarPorTimeOut();
				ServicioDetalleNavegacion.liberarPorTimeOut();
				log.debug("Finalizo la liberacion de cola de conexiones activas: " + timeOut.getPagina());
				// Thread.sleep(30000);
				// System.out.println("------------------------------------- FIN DEL HILO --------------------------------");
			} catch (Exception e) {
				log.error("-------------------------- Error: " + e.getMessage());
				live = false;
			} finally {
				timeOut.setEjecutando(false);
				timeOut.setPagina(null);
				this.live = false;
			}
		}
	}
}
