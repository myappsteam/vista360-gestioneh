package com.myapps.vista_ccc.servicio;

import com.huawei.www.bme.cbsinterface.bccustomizedservices.BCCustomizedServicesBindingStub;
import com.huawei.www.bme.cbsinterface.bccustomizedservices.BCCustomizedServices_ServiceLocator;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.UtilUrl;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class ServicioCustomized implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static Logger log = Logger.getLogger(ServicioCustomized.class);

	public static Map<String, NodoServicio<BCCustomizedServicesBindingStub>> ports = new HashMap<String, NodoServicio<BCCustomizedServicesBindingStub>>();

	public static synchronized NodoServicio<BCCustomizedServicesBindingStub> initPort(String url, String username, int timeout) throws Exception {
		long ini = System.currentTimeMillis();
		url = UtilUrl.getIp(url);
		long fin = System.currentTimeMillis();
		log.debug("[username: " + username + ", url: " + url + "] Tiempo de respuesta para obtener ip: " + (fin - ini) + " milisegundos");

		ini = System.currentTimeMillis();

		NodoServicio<BCCustomizedServicesBindingStub> port = ports.get(username);

		if (port == null) {
			log.debug("Creando un nuevo port ... " + username);
			port = getService(url, username, timeout);
		} else {
			log.debug("Ya existe el port creado ... " + username);
			port.setEnUso(true);
			port.setFechaInicio(new Date());
			port.setFechaFin(null);
		}
		fin = System.currentTimeMillis();
		log.debug("[username: " + username + "] Tiempo de respuesta para obtener conexion: " + (fin - ini) + " milisegundos");
		return port;
	}

	public static synchronized NodoServicio<BCCustomizedServicesBindingStub> getService(String url, String username, int timeout) throws Exception {

		NodoServicio<BCCustomizedServicesBindingStub> port = ports.get(username);

		if (port == null) {

			BCCustomizedServices_ServiceLocator service = new BCCustomizedServices_ServiceLocator(url);

			log.debug("url BCCustomizedServices: " + service.getBCCustomizedServicesPortAddress());

			BCCustomizedServicesBindingStub portStub = (BCCustomizedServicesBindingStub) service.getBCCustomizedServicesPort();
			portStub.setTimeout(timeout);

			port = new NodoServicio<BCCustomizedServicesBindingStub>();
			port.setPort(portStub);
			port.setKey(url + username);

			port.setEnUso(true);
			port.setFechaInicio(new Date());
			port.setFechaFin(null);

			ports.put(username, port);

		}

		return port;
	}

	public static synchronized void liberar(String key) {
		if (ports != null) {
			synchronized (ports) {
				ArrayList<String> listaEliminar = new ArrayList<>();

				for (Entry<String, NodoServicio<BCCustomizedServicesBindingStub>> entry : ports.entrySet()) {
					if (entry.getKey().endsWith(key)) {
						listaEliminar.add(entry.getKey());
					}
				}
				int sizeAntes = ports.size();
				for (String item : listaEliminar) {
					ports.remove(item);
				}
				int sizeDespues = ports.size();
				log.debug("Eliminados del hashmap BCCustomizedServices [key: " + key + "][size antes: " + sizeAntes + ", size despues: " + sizeDespues + "]: " + listaEliminar.size() + " conexiones : " + listaEliminar);
			}
		}
	}

	public static synchronized void liberarPorTimeOut() {
		if (ports != null) {
			synchronized (ports) {
				ArrayList<String> listaEliminar = new ArrayList<>();

				for (Entry<String, NodoServicio<BCCustomizedServicesBindingStub>> entry : ports.entrySet()) {

					if (!entry.getValue().isEnUso() && entry.getValue().getFechaFin() != null) {
						long milisegundos = (new Date()).getTime() - entry.getValue().getFechaFin().getTime();
						long segundos = milisegundos / 1000;
						long minutos = segundos / 60;

						if (minutos >= Parametros.timpoFueraPoolConexion) {
							listaEliminar.add(entry.getKey());
						}
					}
				}

				int sizeAntes = ports.size();
				for (String item : listaEliminar) {
					ports.remove(item);
				}
				int sizeDespues = ports.size();

				log.debug("Eliminados por time out de la cola de conexiones activas BCCustomizedServices [size antes: " + sizeAntes + ", size despues: " + sizeDespues + "]: " + listaEliminar.size() + " conexiones : " + listaEliminar);
			}
		}
	}
}
