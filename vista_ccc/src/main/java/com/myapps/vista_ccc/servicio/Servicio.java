package com.myapps.vista_ccc.servicio;

import org.apache.log4j.Logger;

import java.io.Serializable;

public class Servicio implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(Servicio.class);

	private static TimeOut to = new TimeOut(null, false);

	public static void eliminarConexionesInactivas(String page) {

		log.debug("Iniciando revision de cola de conexiones page: " + page + ", ejecutando: " + to.getEjecutando() + ", pagina: " + to.getPagina());
		String pagina = to.getPagina();
	
		if (pagina != null && page != null) {
			if (pagina.equals(page)) {
				if (!to.getEjecutando()) {
					to.setEjecutando(true);
					ThreadServicio ts = new ThreadServicio(to);
					ts.start();
					// log.debug("++++++++++++++++++++++++++++++++++ FIN DEL HILO ++++++++++++++++++++++++++++");
					return;
				} else {
					log.debug("Ya se esta revisando la cola de conexiones activas: " + pagina);
				}
			} else {
				log.debug("Ya se esta revisando la cola de conexiones activas: " + pagina);
			}
		}
	}

	public static synchronized void setPagina(String page) {
		if (to.getPagina() == null) {
			to.setPagina(page);
		}
	}

}
