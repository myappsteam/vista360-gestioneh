package com.myapps.vista_ccc.business;

import com.myapps.vista_ccc.dao.MasterDao;
import com.myapps.vista_ccc.dao.MasterDaoInterface;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
public class OfferingBL implements Serializable, MasterDaoInterface{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private MasterDao masterDao;
	
	@Override
	public String validar(Object entidad, boolean nuevo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(Object entidad) throws Exception {
		masterDao.save(entidad);
	}

	@Override
	public void remove(Object entidad) throws Exception {
		masterDao.remove(entidad);
	}

	@Override
	public void update(Object entidad) throws Exception {
		masterDao.update(entidad);
	}

	@Override
	public Object find(Object key, Class clase) throws Exception {
		return masterDao.find(key, clase);
	}

	@Override
	public List findAll() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
