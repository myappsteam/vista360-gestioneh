package com.myapps.vista_ccc.business;

import com.myapps.vista_ccc.dao.MasterDao;
import com.myapps.vista_ccc.dao.MasterDaoInterface;
import com.myapps.vista_ccc.dao.ParameterQuery;
import com.myapps.vista_ccc.entity.VcLogAdicional;
import com.myapps.vista_ccc.entity.VcLogAuditoria;

import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;

public class LogAuditoriaBL implements Serializable, MasterDaoInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private MasterDao masterDao;

	@Override
	public String validar(Object entidad, boolean nuevo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(Object entidad) throws Exception {
		masterDao.save(entidad);
	}

	@Override
	public void remove(Object entidad) throws Exception {
		masterDao.remove(entidad);
	}

	@Override
	public void update(Object entidad) throws Exception {
		masterDao.update(entidad);
	}

	@Override
	public Object find(Object key, Class clase) throws Exception {
		return masterDao.find(key, clase);
	}

	@Override
	public List findAll() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List findAll(long id) throws Exception {
		// TODO Auto-generated method stub
		String sql="SELECT a FROM VcLogAuditoria a WHERE a.idLogAuditoria= :idAuditoria";
				ParameterQuery pq = ParameterQuery.getParameterQuery();
				pq.put("idAuditoria", id);
				return masterDao.findAllQuery(VcLogAuditoria.class, sql, pq);
	}

	public void guardarAuditoria(VcLogAuditoria entidad, List<VcLogAdicional> entidades) throws Exception {
		masterDao.guardarAuditoria(entidad, entidades);
	}
	
	public List<VcLogAuditoria> getlog() {
		return masterDao.getList();
	}

}
