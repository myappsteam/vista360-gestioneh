package com.myapps.vista_ccc.business;

import com.myapps.vista_ccc.bean.model.Incidente;
import com.myapps.vista_ccc.dao.MasterDao;
import com.myapps.vista_ccc.dao.MasterDaoInterface;
import com.myapps.vista_ccc.dao.ParameterQuery;
import com.myapps.vista_ccc.entity.VcIncidente;
import org.apache.log4j.Logger;
import org.primefaces.model.SortOrder;

import javax.inject.Inject;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class IncidenteBL implements Serializable, MasterDaoInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static Logger log = Logger.getLogger(AccountHistoryBL.class);

	@Inject
	private MasterDao masterDao;

	@Override
	public String validar(Object entidad, boolean nuevo) {
		VcIncidente incidente = (VcIncidente) entidad;

		if (incidente != null) {
			if (incidente.getVcLogAuditoria() != null && incidente.getVcLogAuditoria().getIdLogAuditoria() <= 0) {
				return "El campo Id Auditoria no valido";
			}

			VcIncidente incidenteAux = masterDao.getIncidente(incidente.getVcLogAuditoria().getIdLogAuditoria());
			if (incidenteAux == null)
				return "";

			return "La auditoria ya fue Registrada en Incidente";

		} else
			return "incidente no valido";
	}

	@Override
	public void save(Object entidad) throws Exception {
		masterDao.save(entidad);
	}

	@Override
	public void remove(Object entidad) throws Exception {
		masterDao.remove(entidad);
	}

	@Override
	public void update(Object entidad) throws Exception {
		masterDao.update(entidad);
	}

	@Override
	public Object find(Object key, Class clase) throws Exception {
		return masterDao.find(key, clase);
	}

	@Override
	public List findAll() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public List<VcIncidente> getIncidentes(String usuario, String vista, Date FechaIni, Date FechaFin, String atencion, String id) {
		return masterDao.getIncidente(usuario, vista, FechaIni, FechaFin, atencion, id);
	}

	@SuppressWarnings("unchecked")
	public List<Incidente> findByPage(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters, String usuario, String vista, Date FechaIni, Date FechaFin,
			String atencion, String id) {
//		log.info("first:" + first + " pageSize:" + pageSize + "paramString:" + sortField + " sortOrder:" + sortOrder.toString() + " paramMap:" + filters.toString());
//		log.debug("id: " + id + ",usuario: " + usuario + ", vista: " + vista + ", FechaIni" + FechaIni + ", FechaFin" + FechaFin + ", atencion: " + atencion);

		String sql = "SELECT v.*, VCL.VISTA FROM VC_INCIDENTE v JOIN VC_LOG_AUDITORIA VCL ON V.ID_LOG_AUDITORIA = VCL.ID_LOG_AUDITORIA";
		List<Incidente> incidente = new ArrayList<Incidente>();

//		log.debug("sortField: " + sortField + ", sortOrder: " + sortOrder.toString());

		try {

			StringBuilder sqlWhere = new StringBuilder("");
			if (!vista.isEmpty()) {
				sqlWhere.append(" WHERE  VCL.VISTA like :vis");
			}

			if (!atencion.isEmpty()) {
				if (sqlWhere.length() == 0) {
					sqlWhere.append(" WHERE v.atencion like :at");
				} else {
					sqlWhere.append(" AND v.atencion like :at");
				}
			}

			if (!usuario.isEmpty()) {
				if (sqlWhere.length() == 0) {
					sqlWhere.append(" WHERE v.usuario = :user");
				} else {
					sqlWhere.append(" AND v.usuario = :user");
				}
			}

			if (!id.isEmpty()) {
				if (sqlWhere.length() == 0) {
					sqlWhere.append(" WHERE v.id_incidente = :id");
				} else {
					sqlWhere.append(" AND v.id_incidente = :id");
				}
			}
			if (FechaIni != null) {
				if (sqlWhere.length() == 0) {
					sqlWhere.append(" WHERE v.FECHA_INCIDENTE >= :FechaIni");
				} else {
					sqlWhere.append(" AND v.FECHA_INCIDENTE >= :FechaIni");
				}
			}

			if (FechaFin != null) {
				if (sqlWhere.length() == 0) {
					sqlWhere.append(" WHERE v.FECHA_INCIDENTE <= :FechaFin");
				} else {
					sqlWhere.append(" AND v.FECHA_INCIDENTE <= :FechaFin");
				}
			}

			// filters
			for (Map.Entry<String, Object> entry : filters.entrySet()) {
				String filterProperty = entry.getKey();
				String filterValue = entry.getValue().toString();

				log.debug("filterProperty: " + filterProperty + ", filterValue: " + filterValue);
				if (filterProperty.equals("id"))
					filterProperty = "v.ID_INCIDENTE";
				if (filterProperty.equals("fechaIncidente"))
					filterProperty = "v.FECHA_INCIDENTE";
				else if (filterProperty.equals("fechaAtencion"))
					filterProperty = "v.FECHA_ATENCION";
				else if (filterProperty.equals("usuario"))
					filterProperty = "V.USUARIO";
				else if (filterProperty.equals("vista"))
					filterProperty = "VCL.VISTA";
				else if (filterProperty.equals("atendido")) {
					filterProperty = "V.ATENCION";
					if (Boolean.valueOf(filterValue))
						filterValue = "1";
					else
						filterValue = "0";
				}

				filterValue = filterValue.toLowerCase();
				sqlWhere = sqlWhere.append(" AND lower(" + filterProperty + ") like '%" + filterValue + "%'");
			}
			//
			sql = sql + sqlWhere.toString();
			//
			if (sortField != null) {
				if (sortField.equals("id"))
					sortField = "v.ID_INCIDENTE";
				else if (sortField.equals("fechaIncidente"))
					sortField = "v.FECHA_INCIDENTE";
				else if (sortField.equals("fechaAtencion"))
					sortField = "v.FECHA_ATENCION";
				else if (sortField.equals("usuario"))
					sortField = "V.USUARIO";
				else if (sortField.equals("vista"))
					sortField = "VCL.VISTA";
				else if (sortField.equals("atendido"))
					sortField = "V.ATENCION";

				if (sortField != null && !sortField.trim().isEmpty()) {
					sql = sql + " ORDER BY " + sortField;
				}

				if (sortOrder.toString().equals("ASCENDING") && sortField != null) {
					sql = sql + " ASC";
				}
				if (sortOrder.toString().equals("DESCENDING") && sortField != null) {
					sql = sql + " DESC";
				}
			} else {
				// sql = sql + " ORDER BY ID_ACCOUNT_HISTORY ASC";
				sql = sql + " ORDER BY v.ID_INCIDENTE DESC";
			}
			// sql = sql + sql2;

//			log.debug("_______________________________sql: " + sql);

			ParameterQuery pq = ParameterQuery.getParameterQuery();
			if (!vista.isEmpty())
				pq.put("vis", vista);
			if (!atencion.isEmpty())
				pq.put("at", atencion);

			if (!usuario.isEmpty())
				pq.put("user", usuario);
			if (!id.isEmpty())
				pq.put("id", id);

			if (FechaIni != null)
				pq.put("FechaIni", FechaIni);
			if (FechaFin != null)
				pq.put("FechaFin", FechaFin);
			List<VcIncidente> lista = masterDao.findAllNativeQuery(VcIncidente.class, sql, pq, first, pageSize);

			if (lista != null) {
				for (VcIncidente item : lista) {
					Date fatencion = null;
					if (item.getFechaAtencion() != null)
						fatencion = item.getFechaAtencion().getTime();
					Incidente inc = new Incidente(String.valueOf(item.getIdIncidente()), String.valueOf(item.getVcLogAuditoria().getIdLogAuditoria()), item.getFechaIncidente().getTime(), fatencion,
							item.getAtencion(), item.getUsuario(), item.getVcLogAuditoria().getVista());
					incidente.add(inc);
				}
			}
		} catch (Exception e) {
			log.error("Error al paginar la tabla incidente: ", e);
		}
		return incidente;
	}

	@SuppressWarnings("unchecked")
	public Integer count(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters, String usuario, String vista, Date FechaIni, Date FechaFin, String atencion,
			String id) {
		Integer size = new Integer(0);

		String sql = "SELECT COUNT(*) FROM VC_INCIDENTE v JOIN VC_LOG_AUDITORIA VCL ON V.ID_LOG_AUDITORIA = VCL.ID_LOG_AUDITORIA";
		// log.debug("sortField: " + sortField + ", sortOrder: " + sortOrder.toString());

		try {

			StringBuilder sqlWhere = new StringBuilder("");
			if (!vista.isEmpty()) {
				sqlWhere.append(" WHERE  VCL.VISTA like :vis");
			}

			if (!atencion.isEmpty()) {
				if (sqlWhere.length() == 0) {
					sqlWhere.append(" WHERE v.atencion like :at");
				} else {
					sqlWhere.append(" AND v.atencion like :at");
				}
			}

			if (!usuario.isEmpty()) {
				if (sqlWhere.length() == 0) {
					sqlWhere.append(" WHERE v.usuario = :user");
				} else {
					sqlWhere.append(" AND v.usuario = :user");
				}
			}

			if (!id.isEmpty()) {
				if (sqlWhere.length() == 0) {
					sqlWhere.append(" WHERE v.id_incidente = :id");
				} else {
					sqlWhere.append(" AND v.id_incidente = :id");
				}
			}
			if (FechaIni != null) {
				if (sqlWhere.length() == 0) {
					sqlWhere.append(" WHERE v.FECHA_INCIDENTE >= :FechaIni");
				} else {
					sqlWhere.append(" AND v.FECHA_INCIDENTE >= :FechaIni");
				}
			}

			if (FechaFin != null) {
				if (sqlWhere.length() == 0) {
					sqlWhere.append(" WHERE v.FECHA_INCIDENTE <= :FechaFin");
				} else {
					sqlWhere.append(" AND v.FECHA_INCIDENTE <= :FechaFin");
				}
			}

			// filters
			for (Map.Entry<String, Object> entry : filters.entrySet()) {
				String filterProperty = entry.getKey();
				String filterValue = entry.getValue().toString();

				log.debug("filterProperty: " + filterProperty + ", filterValue: " + filterValue);
				if (filterProperty.equals("id"))
					filterProperty = "v.ID_INCIDENTE";
				if (filterProperty.equals("fechaIncidente"))
					filterProperty = "v.FECHA_INCIDENTE";
				else if (filterProperty.equals("fechaAtencion"))
					filterProperty = "v.FECHA_ATENCION";
				else if (filterProperty.equals("usuario"))
					filterProperty = "V.USUARIO";
				else if (filterProperty.equals("vista"))
					filterProperty = "VCL.VISTA";
				else if (filterProperty.equals("atendido")) {
					filterProperty = "V.ATENCION";
					if (Boolean.valueOf(filterValue))
						filterValue = "1";
					else
						filterValue = "0";
				}
				filterValue = filterValue.toLowerCase();
				sqlWhere = sqlWhere.append(" AND lower(" + filterProperty + ") like '%" + filterValue + "%'");
			}
			//
			String filtro = sql.toString() + sqlWhere.toString();
			ParameterQuery pq = ParameterQuery.getParameterQuery();

			if (!vista.isEmpty())
				pq.put("vis", vista);
			if (!atencion.isEmpty())
				pq.put("at", atencion);

			if (!usuario.isEmpty())
				pq.put("user", usuario);
			if (!id.isEmpty())
				pq.put("id", id);

			if (FechaIni != null)
				pq.put("FechaIni", FechaIni);
			if (FechaFin != null)
				pq.put("FechaFin", FechaFin);

			List<BigDecimal> l = masterDao.findAllNativeQuery(filtro, pq);
//			log.info("size data sql: " + filtro);
//			log.info("size data model: " + l != null && l.size() > 0 ? l.get(0).intValue() : 0);
			return l != null && l.size() > 0 ? l.get(0).intValue() : 0;

		} catch (Exception e) {
			log.error("Error al calcular size de la tabla incidente: ", e);
		}

		return size;
	}

}
