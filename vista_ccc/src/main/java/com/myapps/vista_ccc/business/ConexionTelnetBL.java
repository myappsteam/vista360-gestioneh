package com.myapps.vista_ccc.business;

import com.myapps.vista_ccc.dao.MasterDao;
import com.myapps.vista_ccc.dao.MasterDaoInterface;
import com.myapps.vista_ccc.dao.ParameterQuery;
import com.myapps.vista_ccc.entity.VcConexionTelnet;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;


@Named
public class ConexionTelnetBL implements Serializable, MasterDaoInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(ConexionTelnetBL.class);

	@Inject
	private MasterDao masterDao;

	@Override
	public String validar(Object entidad, boolean nuevo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(Object entidad) throws Exception {
		masterDao.save(entidad);
	}

	@Override
	public void remove(Object entidad) throws Exception {
		masterDao.remove(entidad);
	}

	@Override
	public void update(Object entidad) throws Exception {
		masterDao.update(entidad);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Object find(Object key, Class clase) throws Exception {
		return masterDao.find(key, clase);
	}

	@SuppressWarnings("rawtypes")
	public List findAll() throws Exception {
		String sql = "select t from VcConexionTelnet t where t.estado = 'Y'";
		return masterDao.findAllQuery(VcConexionTelnet.class, sql, null);
	}
	
	@SuppressWarnings("rawtypes")
	public List findAllTipoConexion() throws Exception {
		String sql = "select t from VcConexionTelnet t where t.activo = 'Y'";
		return masterDao.findAllQuery(VcConexionTelnet.class, sql, null);
	}

	public List findAllActiveInactive() throws Exception {
		String sql = "select t from VcConexionTelnet t";
		return masterDao.findAllQuery(VcConexionTelnet.class, sql, null);
	}
	@SuppressWarnings("unchecked")
	public List<VcConexionTelnet> findAllActive(long idServidor) {
		try{
		String sql = "select t from VcConexionTelnet t where t.estado = 'Y'  and t.vcServidor.idServidor = :idServidor";

		ParameterQuery pq = ParameterQuery.getParameterQuery();
		pq.put("idServidor", idServidor);
		return masterDao.findAllQuery(VcConexionTelnet.class, sql, pq);
		}
		catch(Exception e){
			e.printStackTrace();
			log.debug("Error de consulta base de datos ConexionTelnetBL: "+ e.getMessage());
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<VcConexionTelnet> findAllActiveOrderByCodServidor(long idServidor,String codServidor){
		try{
		String sql = "select t from VcConexionTelnet t where t.estado = 'Y' and t.vcServidor.idServidor = :idServidor order by (case t.codServidor when :codServidor then 0 else 1 end), t.codServidor desc";

		ParameterQuery pq = ParameterQuery.getParameterQuery();
		pq.put("idServidor", idServidor);
		pq.put("codServidor", codServidor);
		return masterDao.findAllQuery(VcConexionTelnet.class, sql, pq);
		}
		catch(Exception e){
			e.printStackTrace();
			log.debug("Error de consulta base de datos ConexionTelnetBL: "+ e.getMessage());
			return null;
		}
	}

}
