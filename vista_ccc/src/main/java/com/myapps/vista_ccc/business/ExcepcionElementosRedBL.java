package com.myapps.vista_ccc.business;

import com.myapps.vista_ccc.dao.MasterDao;
import com.myapps.vista_ccc.dao.MasterDaoInterface;
import com.myapps.vista_ccc.dao.ParameterQuery;
import com.myapps.vista_ccc.entity.VcConfigElementoRed;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
public class ExcepcionElementosRedBL implements Serializable, MasterDaoInterface{
	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(ExcepcionElementosRedBL.class);
	
	@Inject
	private MasterDao masterDao;

	@Override
	public String validar(Object entidad, boolean nuevo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(Object entidad) throws Exception {
		masterDao.save(entidad);
	}

	@Override
	public void remove(Object entidad) throws Exception {
		masterDao.remove(entidad);
	}

	@Override
	public void update(Object entidad) throws Exception {
		masterDao.update(entidad);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Object find(Object key, Class clase) throws Exception {
		return masterDao.find(key, clase);
	}

	@SuppressWarnings("rawtypes")
	public List findAllHLR() throws Exception {
		String sql = "select t from VcConfigElementoRed t";
		return masterDao.findAllQuery(VcConfigElementoRed.class, sql, null);
	}

	public List findAllActiveInactive() throws Exception {
		String sql = "select t from VcConfigElementoRed t";
		return masterDao.findAllQuery(VcConfigElementoRed.class, sql, null);
	}
	
	@SuppressWarnings("unchecked")
	public List<VcConfigElementoRed> findAllActive2() {
		try{
		String sql = "select t from VcConfigElementoRed t ";

		ParameterQuery pq = ParameterQuery.getParameterQuery();
		return masterDao.findAllQuery(VcConfigElementoRed.class, sql, pq);
		}
		catch(Exception e){
			e.printStackTrace();
			log.debug("Error de consulta base de datos VcConfigElementoRed: "+ e.getMessage());
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<VcConfigElementoRed> findAllExcepciones(long idServidor) {
		try{
		String sql = "select t from VcConfigElementoRed t where t.vcServidor.idServidor = :idServidor";

		ParameterQuery pq = ParameterQuery.getParameterQuery();
		pq.put("idServidor", idServidor);
		return masterDao.findAllQuery(VcConfigElementoRed.class, sql, pq);
		}
		catch(Exception e){
			e.printStackTrace();
			log.debug("Error de consulta base de datos VcConfigElementoRed: "+ e.getMessage());
			return null;
		}
	}

	@Override
	public List findAll() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}


}
