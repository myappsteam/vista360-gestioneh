package com.myapps.vista_ccc.business;

import com.myapps.vista_ccc.bean.model.History;
import com.myapps.vista_ccc.dao.MasterDao;
import com.myapps.vista_ccc.dao.MasterDaoInterface;
import com.myapps.vista_ccc.dao.ParameterQuery;
import com.myapps.vista_ccc.entity.VcAccountHistory;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.UtilDate;
import org.apache.log4j.Logger;
import org.primefaces.model.SortOrder;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Named
public class AccountHistoryBL implements Serializable, MasterDaoInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(AccountHistoryBL.class);

	@Inject
	private MasterDao masterDao;

	@Override
	public String validar(Object entidad, boolean nuevo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(Object entidad) throws Exception {
		masterDao.save(entidad);
	}

	@Override
	public void remove(Object entidad) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(Object entidad) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public Object find(Object key, Class clase) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List findAll() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public void eliminarHistorial(String login) throws Exception {
		String sql = "DELETE FROM VC_ACCOUNT_HISTORY WHERE usuario = :login";
		ParameterQuery pq = ParameterQuery.getParameterQuery();
		pq.put("login", login);

		masterDao.executeUpdateNativeQuery(sql, pq);
	}

	public void eliminarHistorial(String isdn, String login) throws Exception {
		String sql = "DELETE FROM VC_ACCOUNT_HISTORY WHERE isdn = :isdn AND usuario = :login";
		ParameterQuery pq = ParameterQuery.getParameterQuery();
		pq.put("isdn", isdn);
		pq.put("login", login);

		masterDao.executeUpdateNativeQuery(sql, pq);
	}

	public void eliminarHistorialPorFecha(Date fecha) throws Exception {
		String sql = "DELETE FROM VC_ACCOUNT_HISTORY WHERE fecha_creacion < :fecha";
		ParameterQuery pq = ParameterQuery.getParameterQuery();
		pq.put("fecha", fecha);

		masterDao.executeUpdateNativeQuery(sql, pq);
	}

	@SuppressWarnings("unchecked")
	public List<History> findByPage(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters, String isdn, String login) {
//		first = first + 1;
		String sql = "SELECT * FROM VC_ACCOUNT_HISTORY";
		List<History> historial = new ArrayList<History>();

		// log.debug("sortField: " + sortField + ", sortOrder: " + sortOrder.toString());

		try {

			// StringBuilder sqlWhere = new StringBuilder(" WHERE isdn = '" + isdn + "' AND usuario = '" + login + "'");
			// StringBuilder sqlWhere = new StringBuilder(" WHERE o.isdn = :isdn AND o.usuario = :login");
			StringBuilder sqlWhere = new StringBuilder(" WHERE isdn = :isdn AND usuario = :login");

			for (Map.Entry<String, Object> entry : filters.entrySet()) {
				String filterProperty = entry.getKey();
				String filterValue = entry.getValue().toString();

				log.debug("filterProperty: " + filterProperty + ", filterValue: " + filterValue);
				if (filterProperty.equals("id"))
					filterProperty = "ID_ACCOUNT_HISTORY";
				else if (filterProperty.equals("categoria"))
					filterProperty = "SERVICE_CATEGORY";
				else if (filterProperty.equals("tipoFlujo"))
					filterProperty = "FLOW_TYPE";
				else if (filterProperty.equals("tipoServicio"))
					filterProperty = "SERIVE_TYPE";
				else if (filterProperty.equals("descripcionServicio"))
					filterProperty = "SERVICE_TYPE_NAME";
				else if (filterProperty.equals("startDate"))
					filterProperty = "START_TIME";

				filterValue = filterValue.toLowerCase();
				// if (sqlWhere.isEmpty()) {
				// sqlWhere = " WHERE lower(" + filterProperty + ") like '%" + filterValue + "%'";
				// } else {
				sqlWhere = sqlWhere.append(" AND lower(" + filterProperty + ") like '%" + filterValue + "%'");
				// }
			}

			sql = sql + sqlWhere.toString();

			if (sortField != null) {
				if (sortField.equals("id"))
					sortField = "ID_ACCOUNT_HISTORY";
				else if (sortField.equals("categoria"))
					sortField = "SERVICE_CATEGORY";
				else if (sortField.equals("tipoFlujo"))
					sortField = "FLOW_TYPE";
				else if (sortField.equals("tipoServicio"))
					sortField = "SERIVE_TYPE";
				else if (sortField.equals("descripcionServicio"))
					sortField = "SERVICE_TYPE_NAME";
				else if (sortField.equals("startDate"))
					sortField = "START_TIME";

				if (sortField != null && !sortField.trim().isEmpty()) {
					sql = sql + " ORDER BY " + sortField;
				}

				if (sortOrder.toString().equals("ASCENDING") && sortField != null) {
					sql = sql + " ASC";
				}
				if (sortOrder.toString().equals("DESCENDING") && sortField != null) {
					sql = sql + " DESC";
				}
			} else {
				// sql = sql + " ORDER BY ID_ACCOUNT_HISTORY ASC";
				sql = sql + " ORDER BY START_TIME DESC";
			}
			// sql = sql + sql2;

			// log.debug("_______________________________sql: " + sql);

			ParameterQuery pq = ParameterQuery.getParameterQuery();
			pq.put("isdn", isdn);
			pq.put("login", login);

			List<VcAccountHistory> lista = masterDao.findAllNativeQuery(VcAccountHistory.class, sql, pq, first, pageSize);

			if (lista != null) {
				for (VcAccountHistory item : lista) {
					History h = new History();
					h.setIsdn(item.getIsdn());
					h.setId(item.getIdAccountHistory() + "");
					h.setCategoria(item.getServiceCategory());
					h.setTipoFlujo(item.getFlowType());
					h.setTipoServicio(item.getSeriveType());
					h.setDescripcionServicio(item.getServiceTypeName());
					h.setStartDate(UtilDate.dateToString(item.getStartTime().getTime(), Parametros.fechaFormatPage));
					h.setCdrSecuencia(item.getCdrSeq());
					if (item.getMeasureUnit() != null)
						h.setMeasureUnit(item.getMeasureUnit().intValue());

					// QUERY CDR
					h.setOtherNumber(item.getOtherNumber());
					h.setRoamFlag(item.getRoamFlag());
					h.setCallingCellId(item.getCallingCellId());
					h.setCalledCellId(item.getCalledCellId());
					h.setSpecialNumber(item.getSpecialNumberIndicator());

					if (item.getEndTime() != null)
						h.setEndDate(UtilDate.dateToString(item.getEndTime().getTime(), Parametros.fechaFormatPage));

					h.setBillCycleId(item.getBillCycleId());

					if (item.getStartTime() != null)
						h.setStartTime(UtilDate.dateToString(item.getStartTime().getTime(), Parametros.fechaFormatWs));
					if (item.getEndTime() != null)
						h.setEndTime(UtilDate.dateToString(item.getEndTime().getTime(), Parametros.fechaFormatWs));

					h.setRefundIndicator(item.getRefundIndicator());

					if (item.getActualVolume() != null)
						h.setActualVolume(item.getActualVolume().longValue() + "");
					if (item.getRatingVolume() != null)
						h.setRatingVolume(item.getRatingVolume().longValue() + "");
					if (item.getFreeVolume() != null)
						h.setFreeVolume(item.getFreeVolume().longValue() + "");
					if (item.getMeasureUnitCdr() != null)
						h.setMeasureUnitCdr(item.getMeasureUnitCdr().intValue());

					if (item.getActualChargeAmt() != null)
						h.setActualChargeAmt(item.getActualChargeAmt().longValue() + "");
					if (item.getFreeChargeAmt() != null)
						h.setFreeChargeAmt(item.getFreeChargeAmt().longValue() + "");
					if (item.getCurrencyIdCdr() != null)
						h.setCurrencyIdCdr(item.getCurrencyIdCdr().intValue() + "");

					h.setActualTaxCode(item.getActualTaxCode());
					if (item.getActualTaxAmt() != null)
						h.setActualTaxAmt(item.getActualTaxAmt().longValue() + "");
					h.setAdditionalProperty(item.getAdditionalProperty());

					// AJUSTES
					h.setCategory(item.getAjCategory());
					h.setPrimaryIdentity(item.getAjPrimaryIdentity());
					h.setTradeTime(item.getAjTradeTime());
					h.setRemark(item.getAjRemark());
					h.setChannelId(item.getAjChannelId());
					if (item.getAjTransId() != null)
						h.setTransId(item.getAjTransId().longValue() + "");
					h.setExTransId(item.getAjExtTransId());
					h.setAjAdditionalProperty(item.getAjAdditionalProperty());
					

					// RECARGAS
					h.setReCategory(item.getReCategory());
					h.setReTradeTime(item.getReTradeTime());
					h.setRePrimaryIdentity(item.getRePrimaryIdentity());
					h.setRechargeType(item.getReRechargeType());
					h.setRechargeChannelId(item.getReRechargeChannelId());
					if (item.getReRechargeAmount() != null)
						h.setRechargeAmount(item.getReRechargeAmount().longValue() + "");
					if (item.getReCurrencyId() != null)
						h.setCurrencyId(item.getReCurrencyId().intValue() + "");
					h.setCardSequence(item.getReCardSequence());
					h.setRechargeReason(item.getReRechargeReason());
					h.setResultCode(item.getReResultCode());
					if (item.getReTransId() != null)
						h.setReTransId(item.getReTransId().longValue() + "");
					h.setReExTransId(item.getReExtTransId());
					h.setExRechargeType(item.getReExtRechargeType());
					if (item.getReRechargeTax() != null)
						h.setRechargeTax(item.getReRechargeTax().longValue() + "");
					if (item.getReRechargePenalty() != null)
						h.setRechargePenalty(item.getReRechargePenalty().longValue() + "");
					h.setReversalFlag(item.getReReversalFlag());
					h.setReversalReason(item.getReReversalReason());
					h.setReversalTime(item.getReReversalTime());
					h.setReAdditionalProperty(item.getReAdditionalProperty());

					historial.add(h);
				}
			}
		} catch (Exception e) {
			log.error("Error al paginar la tabla account history: ", e);
		}
		return historial;
	}

	@SuppressWarnings("unchecked")
	public Integer count(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters, String isdn, String login) {
		StringBuilder sql = new StringBuilder("SELECT COUNT(0) AS CANT FROM VC_ACCOUNT_HISTORY");

		Integer size = new Integer(0);
		try {
			// StringBuilder aux = new StringBuilder(" WHERE isdn = '" + isdn + "' AND usuario = '" + login + "' ");
			StringBuilder aux = new StringBuilder(" WHERE isdn = :isdn AND usuario = :login ");
			for (Map.Entry<String, Object> entry : filters.entrySet()) {
				String filterProperty = entry.getKey();
				String filterValue = entry.getValue().toString();

				if (filterProperty.equals("id"))
					filterProperty = "ID_ACCOUNT_HISTORY";
				else if (filterProperty.equals("categoria"))
					filterProperty = "SERVICE_CATEGORY";
				else if (filterProperty.equals("tipoFlujo"))
					filterProperty = "FLOW_TYPE";
				else if (filterProperty.equals("tipoServicio"))
					filterProperty = "SERIVE_TYPE";
				else if (filterProperty.equals("descripcionServicio"))
					filterProperty = "SERVICE_TYPE_NAME";
				else if (filterProperty.equals("startDate"))
					filterProperty = "START_TIME";

				filterValue = filterValue.toLowerCase();
				// if (aux.toString().isEmpty()) {
				// aux.append(" WHERE lower(" + filterProperty + ") like '%" + filterValue + "%'");
				// } else {
				aux.append(" AND lower(" + filterProperty + ") like '%" + filterValue + "%'");
				// }
			}

			String filtro = sql.toString() + aux.toString();
			ParameterQuery pq = ParameterQuery.getParameterQuery();
			pq.put("isdn", isdn);
			pq.put("login", login);

			List<BigDecimal> l = masterDao.findAllNativeQuery(filtro, pq);
			return l != null && l.size() > 0 ? l.get(0).intValue() : 0;

		} catch (Exception e) {
			log.error("Error al calcular size de la tabla account history: ", e);
		}
		// log.debug("size data model: " + size);
		return size;
	}
}
