package com.myapps.vista_ccc.business;

import com.myapps.vista_ccc.dao.MasterDao;
import com.myapps.vista_ccc.dao.MasterDaoInterface;
import com.myapps.vista_ccc.dao.ParameterQuery;
import com.myapps.vista_ccc.entity.VcConsulta;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
public class ConsultaBL implements Serializable, MasterDaoInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(ConsultaBL.class);

	@Inject
	private MasterDao masterDao;

	@Override
	public String validar(Object entidad, boolean nuevo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(Object entidad) throws Exception {
		masterDao.save(entidad);
	}

	@Override
	public void remove(Object entidad) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(Object entidad) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public Object find(Object key, Class clase) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public List findAll(long id) throws Exception {
		// TODO Auto-generated method stub
		String sql="SELECT c FROM VcConsulta c WHERE c.vcLogAuditoria.idLogAuditoria= :idAuditoria";
				ParameterQuery pq = ParameterQuery.getParameterQuery();
				pq.put("idAuditoria", id);
				return masterDao.findAllQuery(VcConsulta.class, sql, pq);
	}
	
	@Override
	public List findAll() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
