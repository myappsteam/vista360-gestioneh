package com.myapps.vista_ccc.business;

import com.myapps.vista_ccc.dao.MasterDao;
import com.myapps.vista_ccc.dao.MasterDaoInterface;
import com.myapps.vista_ccc.dao.ParameterQuery;
import com.myapps.vista_ccc.entity.VcConsultaRed;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
public class ConsultaRedBL implements Serializable, MasterDaoInterface{
	
	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(ConsultaRedBL.class);
	
	@Override
	public String validar(Object entidad, boolean nuevo) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Inject
	private MasterDao masterDao;
	
	@Override
	public void save(Object entidad) throws Exception {
		masterDao.save(entidad);
	}
	
	@Override
	public void remove(Object entidad) throws Exception {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void update(Object entidad) throws Exception {
		// TODO Auto-generated method stub

	}
	
	@Override
	public Object find(Object key, Class clase) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public List findAll() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List findAll(long id) throws Exception {
		// TODO Auto-generated method stub
		String sql="SELECT c FROM VcConsultaRed c WHERE c.vcLogAuditoria.idLogAuditoria= :idAuditoria ORDER BY c.idConsulta ASC ";
				ParameterQuery pq = ParameterQuery.getParameterQuery();
				pq.put("idAuditoria", id);
				return masterDao.findAllQuery(VcConsultaRed.class, sql, pq);
	}
}
