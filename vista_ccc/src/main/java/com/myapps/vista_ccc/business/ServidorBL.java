package com.myapps.vista_ccc.business;

import com.myapps.vista_ccc.dao.MasterDao;
import com.myapps.vista_ccc.dao.MasterDaoInterface;
import com.myapps.vista_ccc.dao.ParameterQuery;
import com.myapps.vista_ccc.entity.VcServidor;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;


@Named
public class ServidorBL implements Serializable, MasterDaoInterface {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(ServidorBL.class);

	@Inject
	private MasterDao masterDao;

	@Override
	public String validar(Object entidad, boolean nuevo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(Object entidad) throws Exception {
		masterDao.save(entidad);
	}

	@Override
	public void remove(Object entidad) throws Exception {
		masterDao.remove(entidad);
	}

	@Override
	public void update(Object entidad) throws Exception {
		masterDao.update(entidad);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Object find(Object key, Class clase) throws Exception {
		return masterDao.find(key, clase);
	}

	@SuppressWarnings("rawtypes")
	public List findAll() throws Exception {
		String sql = "select t from VcServidor t";
		return masterDao.findAllQuery(VcServidor.class, sql, null);
	}

	public List findAllActiveInactive() throws Exception {
		String sql = "select t from VcServidor t";
		return masterDao.findAllQuery(VcServidor.class, sql, null);
	}

	@SuppressWarnings("unchecked")
	public List<VcServidor> findAllActive(String idServidorPadre) throws Exception {
		String sql = "select t from VcServidor t where  t.idServidorPadre = :idServidorPadre";
//		log.debug("MasterDao "+ masterDao);
		ParameterQuery pq = ParameterQuery.getParameterQuery();
//		log.debug("pq "+ pq);
		pq.put("idServidorPadre", idServidorPadre);
		return masterDao.findAllQuery(VcServidor.class, sql, pq);
	}
	
	@SuppressWarnings("unchecked")
	public List<VcServidor> findAllActiveElementosDeRed(String idServidor) throws Exception {
		String sql = "select t from VcServidor t where  t.idServidor = :idServidor";
//		log.debug("MasterDao "+ masterDao);
		ParameterQuery pq = ParameterQuery.getParameterQuery();
//		log.debug("pq "+ pq);
		pq.put("idServidor", idServidor);
		return masterDao.findAllQuery(VcServidor.class, sql, pq);
	}
}
