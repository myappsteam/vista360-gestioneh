package com.myapps.vista_ccc.ldap;

import com.myapps.vista_ccc.util.Parametros;
import org.apache.log4j.Logger;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import javax.naming.ldap.InitialLdapContext;
import java.util.ArrayList;
import java.util.Hashtable;

public class ActiveDirectory {

	public static int EXIT_USER = 1;
	public static int NOT_EXIT_USER = 2;
	public static int ERROR = 3;

	public static Logger log = Logger.getLogger(ActiveDirectory.class);

	public static int existUser(String user, String password) {

		if (password == null || password.isEmpty()) {
			return NOT_EXIT_USER;
		}
		Hashtable env = new Hashtable();

		try {
			env.put(Context.INITIAL_CONTEXT_FACTORY, Parametros.activeInitialContext);
			env.put(Context.PROVIDER_URL, Parametros.activeProviderUrl);
			env.put(Context.SECURITY_AUTHENTICATION, Parametros.activeSecurityAuthentication);
			// Set up environment for creating initial context
			env.put(Context.SECURITY_PRINCIPAL, Parametros.activeSecurityPrincipal + user);
			env.put(Context.SECURITY_CREDENTIALS, password);
		} catch (Exception e) {
			/*
			 * log.error("[existUser] Error al verificar la conectividad con el direcctorio activo" + "Directory: "+e.getMessage());//
			 */
			return ERROR;
		}

		try {
			// Create initial context
			InitialDirContext ctx = new InitialDirContext(env);
			// Close the context when we're done
			ctx.close();
			return EXIT_USER;
		} catch (NamingException ex) {
			/*
			 * log.error("[existUser] Error al autenticar en el servidor de Active " + "Directory: "+ex.getMessage());//
			 */
			return NOT_EXIT_USER;
		} catch (Exception e) {/*
								 * log.error("[existUser] Error al autenticar en el servidor de Active " + "Directory: "+e.getMessage());//
								 */
			return NOT_EXIT_USER;
		}
	}

	/**
	 * Obtener Los Grupos a los que pertenece un Usuario
	 * 
	 * @param usuario
	 *            Nombre del usuario que se esta buscando
	 * @return Una lista con los grupos a los que pertenece un usuario
	 */
	public static ArrayList getGroups(String user) {

		ArrayList list = null;

		Hashtable env = new Hashtable();

		env.put("java.naming.factory.initial", Parametros.activeInitialContext);
		env.put("java.naming.provider.url", Parametros.activeProviderUrl);
		env.put("java.naming.security.authentication", Parametros.activeSecurityAuthentication);
		env.put("java.naming.security.principal", Parametros.activeSecurityPrincipal + Parametros.activeSecurityUser);
		env.put("java.naming.security.credentials", Parametros.activeSecurityCredentials);

		InitialLdapContext ctx = null;
		try {
			// Create the initial directory context
			ctx = new InitialLdapContext(env, null);
			// Especificar La base Para Buscar
			String searchBase = "DC=tigo,DC=net,DC=bo";
			// Create the search controls
			SearchControls searchCtls = new SearchControls();

			// Especificar el alcance de búsqueda
			searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			String returnAtts[] = { "memberOf" };

			// Especificado el filtro de busqueda LDAP
			String searchFilter = "(&(objectCategory=person)(objectClass=user)(mailNickname=" + user + "))";

			searchCtls.setReturningAttributes(returnAtts);

			// Buscar un Objeto usando el Filtro
			NamingEnumeration answer = ctx.search(searchBase, searchFilter, searchCtls);
			int totalResults = 0;
			// Bucle a través de los resultados de la búsqueda

			while (answer.hasMoreElements()) {
				SearchResult sr = (SearchResult) answer.next();
				// Print out the groups
				Attributes attrs = sr.getAttributes();
				if (attrs != null) {
					list = new ArrayList();
					for (NamingEnumeration ne = attrs.getAll(); ne.hasMore();) {
						Attribute attr = (Attribute) ne.next();
						String grupo;
						for (NamingEnumeration e = attr.getAll(); e.hasMore(); totalResults++) {
							grupo = e.next().toString().trim();
							grupo = grupo.substring(3, grupo.indexOf(",")).trim();
							list.add(grupo);
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("[getGroups] Error al obtener el listado de grupos: " + e.getMessage());
		} finally {
			if (ctx != null) {
				try {
					ctx.close();
				} catch (NamingException e) {
					log.error("[getGroups] Error al cerrar ctx");
				}
			}
		}
		return list;
	}

}