package com.myapps.vista_ccc.reportes_bccs.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by michael on 9/4/2019.
 */
@Entity
@Table(name="REPORTE_FECHA_LIMITE")
public class ReporteFechaLimite implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name="REPORTE_HISTORICO_ID_REPORTE_HISTORICO_GENERATOR" )
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="REPORTE_HISTORICO_ID_REPORTE_HISTORICO_GENERATOR")
    @Column(name="ID_FECHA_LIMITE")
    private long idReporteHistorico;
    @Column(name="NOMBRE_REPORTE")
    private String nombreReporte;
    @Column(name="DESCRIPCION_REPORTE")
    private String descripcionReporte;
    @Column(name="DIAS_LIMITE")
    private Integer diasLimite;

    public long getIdReporteHistorico() {
        return idReporteHistorico;
    }

    public void setIdReporteHistorico(long idReporteHistorico) {
        this.idReporteHistorico = idReporteHistorico;
    }

    public String getNombreReporte() {
        return nombreReporte;
    }

    public void setNombreReporte(String nombreReporte) {
        this.nombreReporte = nombreReporte;
    }

    public String getDescripcionReporte() {
        return descripcionReporte;
    }

    public void setDescripcionReporte(String descripcionReporte) {
        this.descripcionReporte = descripcionReporte;
    }

    public Integer getDiasLimite() {
        return diasLimite;
    }

    public void setDiasLimite(Integer diasLimite) {
        this.diasLimite = diasLimite;
    }

    @Override
    public String toString() {
        return "ReporteFechaLimite{" +
                "idReporteHistorico=" + idReporteHistorico +
                ", nombreReporte='" + nombreReporte + '\'' +
                ", descripcionReporte='" + descripcionReporte + '\'' +
                ", diasLimite=" + diasLimite +
                '}';
    }
}
