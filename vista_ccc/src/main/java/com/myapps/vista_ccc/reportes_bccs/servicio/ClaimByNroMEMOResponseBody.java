/**
 * ClaimByNroMEMOResponseBody.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.myapps.vista_ccc.reportes_bccs.servicio;

public class ClaimByNroMEMOResponseBody  implements java.io.Serializable {
    private String nroMemo;

    private String codProceso;

    private String cliente;

    private String contrato;

    private String tipoCuenta;

    private String nroCuenta;

    private String memoFecha;

    private String memoHora;

    private String memoDescripcion;

    public ClaimByNroMEMOResponseBody() {
    }

    public ClaimByNroMEMOResponseBody(
           String nroMemo,
           String codProceso,
           String cliente,
           String contrato,
           String tipoCuenta,
           String nroCuenta,
           String memoFecha,
           String memoHora,
           String memoDescripcion) {
           this.nroMemo = nroMemo;
           this.codProceso = codProceso;
           this.cliente = cliente;
           this.contrato = contrato;
           this.tipoCuenta = tipoCuenta;
           this.nroCuenta = nroCuenta;
           this.memoFecha = memoFecha;
           this.memoHora = memoHora;
           this.memoDescripcion = memoDescripcion;
    }


    /**
     * Gets the nroMemo value for this ClaimByNroMEMOResponseBody.
     * 
     * @return nroMemo
     */
    public String getNroMemo() {
        return nroMemo;
    }


    /**
     * Sets the nroMemo value for this ClaimByNroMEMOResponseBody.
     * 
     * @param nroMemo
     */
    public void setNroMemo(String nroMemo) {
        this.nroMemo = nroMemo;
    }


    /**
     * Gets the codProceso value for this ClaimByNroMEMOResponseBody.
     * 
     * @return codProceso
     */
    public String getCodProceso() {
        return codProceso;
    }


    /**
     * Sets the codProceso value for this ClaimByNroMEMOResponseBody.
     * 
     * @param codProceso
     */
    public void setCodProceso(String codProceso) {
        this.codProceso = codProceso;
    }


    /**
     * Gets the cliente value for this ClaimByNroMEMOResponseBody.
     * 
     * @return cliente
     */
    public String getCliente() {
        return cliente;
    }


    /**
     * Sets the cliente value for this ClaimByNroMEMOResponseBody.
     * 
     * @param cliente
     */
    public void setCliente(String cliente) {
        this.cliente = cliente;
    }


    /**
     * Gets the contrato value for this ClaimByNroMEMOResponseBody.
     * 
     * @return contrato
     */
    public String getContrato() {
        return contrato;
    }


    /**
     * Sets the contrato value for this ClaimByNroMEMOResponseBody.
     * 
     * @param contrato
     */
    public void setContrato(String contrato) {
        this.contrato = contrato;
    }


    /**
     * Gets the tipoCuenta value for this ClaimByNroMEMOResponseBody.
     * 
     * @return tipoCuenta
     */
    public String getTipoCuenta() {
        return tipoCuenta;
    }


    /**
     * Sets the tipoCuenta value for this ClaimByNroMEMOResponseBody.
     * 
     * @param tipoCuenta
     */
    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }


    /**
     * Gets the nroCuenta value for this ClaimByNroMEMOResponseBody.
     * 
     * @return nroCuenta
     */
    public String getNroCuenta() {
        return nroCuenta;
    }


    /**
     * Sets the nroCuenta value for this ClaimByNroMEMOResponseBody.
     * 
     * @param nroCuenta
     */
    public void setNroCuenta(String nroCuenta) {
        this.nroCuenta = nroCuenta;
    }


    /**
     * Gets the memoFecha value for this ClaimByNroMEMOResponseBody.
     * 
     * @return memoFecha
     */
    public String getMemoFecha() {
        return memoFecha;
    }


    /**
     * Sets the memoFecha value for this ClaimByNroMEMOResponseBody.
     * 
     * @param memoFecha
     */
    public void setMemoFecha(String memoFecha) {
        this.memoFecha = memoFecha;
    }


    /**
     * Gets the memoHora value for this ClaimByNroMEMOResponseBody.
     * 
     * @return memoHora
     */
    public String getMemoHora() {
        return memoHora;
    }


    /**
     * Sets the memoHora value for this ClaimByNroMEMOResponseBody.
     * 
     * @param memoHora
     */
    public void setMemoHora(String memoHora) {
        this.memoHora = memoHora;
    }


    /**
     * Gets the memoDescripcion value for this ClaimByNroMEMOResponseBody.
     * 
     * @return memoDescripcion
     */
    public String getMemoDescripcion() {
        return memoDescripcion;
    }


    /**
     * Sets the memoDescripcion value for this ClaimByNroMEMOResponseBody.
     * 
     * @param memoDescripcion
     */
    public void setMemoDescripcion(String memoDescripcion) {
        this.memoDescripcion = memoDescripcion;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof ClaimByNroMEMOResponseBody)) return false;
        ClaimByNroMEMOResponseBody other = (ClaimByNroMEMOResponseBody) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.nroMemo==null && other.getNroMemo()==null) || 
             (this.nroMemo!=null &&
              this.nroMemo.equals(other.getNroMemo()))) &&
            ((this.codProceso==null && other.getCodProceso()==null) || 
             (this.codProceso!=null &&
              this.codProceso.equals(other.getCodProceso()))) &&
            ((this.cliente==null && other.getCliente()==null) || 
             (this.cliente!=null &&
              this.cliente.equals(other.getCliente()))) &&
            ((this.contrato==null && other.getContrato()==null) || 
             (this.contrato!=null &&
              this.contrato.equals(other.getContrato()))) &&
            ((this.tipoCuenta==null && other.getTipoCuenta()==null) || 
             (this.tipoCuenta!=null &&
              this.tipoCuenta.equals(other.getTipoCuenta()))) &&
            ((this.nroCuenta==null && other.getNroCuenta()==null) || 
             (this.nroCuenta!=null &&
              this.nroCuenta.equals(other.getNroCuenta()))) &&
            ((this.memoFecha==null && other.getMemoFecha()==null) || 
             (this.memoFecha!=null &&
              this.memoFecha.equals(other.getMemoFecha()))) &&
            ((this.memoHora==null && other.getMemoHora()==null) || 
             (this.memoHora!=null &&
              this.memoHora.equals(other.getMemoHora()))) &&
            ((this.memoDescripcion==null && other.getMemoDescripcion()==null) || 
             (this.memoDescripcion!=null &&
              this.memoDescripcion.equals(other.getMemoDescripcion())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNroMemo() != null) {
            _hashCode += getNroMemo().hashCode();
        }
        if (getCodProceso() != null) {
            _hashCode += getCodProceso().hashCode();
        }
        if (getCliente() != null) {
            _hashCode += getCliente().hashCode();
        }
        if (getContrato() != null) {
            _hashCode += getContrato().hashCode();
        }
        if (getTipoCuenta() != null) {
            _hashCode += getTipoCuenta().hashCode();
        }
        if (getNroCuenta() != null) {
            _hashCode += getNroCuenta().hashCode();
        }
        if (getMemoFecha() != null) {
            _hashCode += getMemoFecha().hashCode();
        }
        if (getMemoHora() != null) {
            _hashCode += getMemoHora().hashCode();
        }
        if (getMemoDescripcion() != null) {
            _hashCode += getMemoDescripcion().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClaimByNroMEMOResponseBody.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ClaimByNroMEMOResponseBody"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nroMemo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nroMemo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codProceso");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codProceso"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cliente");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cliente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contrato");
        elemField.setXmlName(new javax.xml.namespace.QName("", "contrato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoCuenta");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipoCuenta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nroCuenta");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nroCuenta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("memoFecha");
        elemField.setXmlName(new javax.xml.namespace.QName("", "memoFecha"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("memoHora");
        elemField.setXmlName(new javax.xml.namespace.QName("", "memoHora"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("memoDescripcion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "memoDescripcion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
