
package com.myapps.vista_ccc.reportes_bccs.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Error implements Serializable
{

    @SerializedName("httpStatus")
    @Expose
    private Integer httpStatus;
    @SerializedName("detail")
    @Expose
    private String detail;
    private final static long serialVersionUID = 8052003332943175744L;

    public Integer getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(Integer httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

}
