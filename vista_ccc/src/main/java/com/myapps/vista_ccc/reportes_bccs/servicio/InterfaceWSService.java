/**
 * InterfaceWSService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.myapps.vista_ccc.reportes_bccs.servicio;

public interface InterfaceWSService extends javax.xml.rpc.Service {
    public String getInterfaceWSPortAddress();

    public com.myapps.vista_ccc.reportes_bccs.servicio.InterfaceWS getInterfaceWSPort() throws javax.xml.rpc.ServiceException;

    public com.myapps.vista_ccc.reportes_bccs.servicio.InterfaceWS getInterfaceWSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
