package com.myapps.vista_ccc.reportes_bccs.servicio.rest;

import com.myapps.vista_ccc.reportes_bccs.commons.AuditoriaCuentasMigradas;
import com.myapps.vista_ccc.reportes_bccs.commons.HttpMethod;
import com.myapps.vista_ccc.reportes_bccs.commons.StatusResponse;
import com.myapps.vista_ccc.reportes_bccs.error.HttpStatusAcceptException;
import com.myapps.vista_ccc.reportes_bccs.error.HttpStatusException;
import com.myapps.vista_ccc.util.Parametros;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.net.ConnectException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

@Named
public class ReporteBCCSRestConsumer implements Serializable {

	@Inject
	private ApacheHttpClientRestServiceReporteBCCS clientRest;
	@Inject
	private AuditoriaCuentasMigradas auditoria;

	private static Logger logger = Logger.getLogger(ReporteBCCSRestConsumer.class);

	public String consumerCuentaMigrada(String request, String urlService, HttpMethod httpMethod, String usuario,
									long idAuditoriaHistorial) throws HttpStatusException, IOException, HttpStatusAcceptException {
		long ini = System.currentTimeMillis();
		logger.info("Request Json: " + request);
		String output = "";
		String urlConsumer = new StringBuffer(Parametros.urlCuentaMigrada).append(urlService).toString();
		logger.info("Url service: " + urlConsumer);
		try {
			output = clientRest.httpClientRest(httpMethod, request, urlConsumer, Parametros.tokenCuentaMigradaAuth,
					Parametros.timeOutServiceCuentaMigrada);
			logger.info("Response Cuenta Migrada Json: " + output);
			long fin = System.currentTimeMillis();
			logger.info(
					"[Serivicio RESTful: " + urlConsumer + "] Tiempo de respuesta: " + (fin - ini) + " milisegundos");
			if (Parametros.saveRequestResponse) {
				try {
					auditoria.saveXml(request, usuario, urlConsumer, "validarcuentamigrada", request, output,
							idAuditoriaHistorial, (fin - ini), (fin - ini));
				} catch (Exception e) {
					logger.warn("No se logro registrar los request y response del servicio RESTful Cuenta Migrada: ", e);
				}
			}

		} catch (ConnectException e) {
			logger.error("No se pudo establecer una conexion con el servicio Rest Send SMS " + e.getMessage(), e);
			throw new HttpStatusException(StatusResponse.HTTP_NOT_FOUND.getDescripcion(),
					StatusResponse.HTTP_NOT_FOUND);

		} catch (ConnectTimeoutException e) {
			logger.error("Tiempo de espera agotado para respuesta de servicio Send SMS " + urlConsumer + " "
					+ e.getMessage(), e);
			;
			throw new HttpStatusException(StatusResponse.HTTP_ERROR.getDescripcion() + ", Causa: " + e.getMessage(),
					StatusResponse.HTTP_ERROR);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyStoreException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
		return output;
	}

}
