package com.myapps.vista_ccc.reportes_bccs.error;

public class HttpStatusAcceptException extends Exception {

	public int getCodigo() {
		return codigo;
	}

	private static final long serialVersionUID = 1L;
	private final transient String json;
	private final transient int codigo;

	public HttpStatusAcceptException(String json, int codigo) {
		this.json = json;
		this.codigo = codigo;
	}

	public String getJson() {
		return json;
	}

}
