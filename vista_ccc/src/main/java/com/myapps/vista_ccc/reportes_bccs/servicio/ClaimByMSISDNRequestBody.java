/**
 * ClaimByMSISDNRequestBody.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.myapps.vista_ccc.reportes_bccs.servicio;

public class ClaimByMSISDNRequestBody  implements java.io.Serializable {
    private String cuenta;

    private String fechaInicio;

    private String fechaFin;

    private com.myapps.vista_ccc.reportes_bccs.servicio.ParameterType[] additionalParameters;

    public ClaimByMSISDNRequestBody() {
    }

    public ClaimByMSISDNRequestBody(
           String cuenta,
           String fechaInicio,
           String fechaFin,
           com.myapps.vista_ccc.reportes_bccs.servicio.ParameterType[] additionalParameters) {
           this.cuenta = cuenta;
           this.fechaInicio = fechaInicio;
           this.fechaFin = fechaFin;
           this.additionalParameters = additionalParameters;
    }


    /**
     * Gets the cuenta value for this ClaimByMSISDNRequestBody.
     * 
     * @return cuenta
     */
    public String getCuenta() {
        return cuenta;
    }


    /**
     * Sets the cuenta value for this ClaimByMSISDNRequestBody.
     * 
     * @param cuenta
     */
    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }


    /**
     * Gets the fechaInicio value for this ClaimByMSISDNRequestBody.
     * 
     * @return fechaInicio
     */
    public String getFechaInicio() {
        return fechaInicio;
    }


    /**
     * Sets the fechaInicio value for this ClaimByMSISDNRequestBody.
     * 
     * @param fechaInicio
     */
    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }


    /**
     * Gets the fechaFin value for this ClaimByMSISDNRequestBody.
     * 
     * @return fechaFin
     */
    public String getFechaFin() {
        return fechaFin;
    }


    /**
     * Sets the fechaFin value for this ClaimByMSISDNRequestBody.
     * 
     * @param fechaFin
     */
    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }


    /**
     * Gets the additionalParameters value for this ClaimByMSISDNRequestBody.
     * 
     * @return additionalParameters
     */
    public com.myapps.vista_ccc.reportes_bccs.servicio.ParameterType[] getAdditionalParameters() {
        return additionalParameters;
    }


    /**
     * Sets the additionalParameters value for this ClaimByMSISDNRequestBody.
     * 
     * @param additionalParameters
     */
    public void setAdditionalParameters(com.myapps.vista_ccc.reportes_bccs.servicio.ParameterType[] additionalParameters) {
        this.additionalParameters = additionalParameters;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof ClaimByMSISDNRequestBody)) return false;
        ClaimByMSISDNRequestBody other = (ClaimByMSISDNRequestBody) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cuenta==null && other.getCuenta()==null) || 
             (this.cuenta!=null &&
              this.cuenta.equals(other.getCuenta()))) &&
            ((this.fechaInicio==null && other.getFechaInicio()==null) || 
             (this.fechaInicio!=null &&
              this.fechaInicio.equals(other.getFechaInicio()))) &&
            ((this.fechaFin==null && other.getFechaFin()==null) || 
             (this.fechaFin!=null &&
              this.fechaFin.equals(other.getFechaFin()))) &&
            ((this.additionalParameters==null && other.getAdditionalParameters()==null) || 
             (this.additionalParameters!=null &&
              java.util.Arrays.equals(this.additionalParameters, other.getAdditionalParameters())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCuenta() != null) {
            _hashCode += getCuenta().hashCode();
        }
        if (getFechaInicio() != null) {
            _hashCode += getFechaInicio().hashCode();
        }
        if (getFechaFin() != null) {
            _hashCode += getFechaFin().hashCode();
        }
        if (getAdditionalParameters() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAdditionalParameters());
                 i++) {
                Object obj = java.lang.reflect.Array.get(getAdditionalParameters(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClaimByMSISDNRequestBody.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ClaimByMSISDNRequestBody"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cuenta");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cuenta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaInicio");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fechaInicio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaFin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fechaFin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("additionalParameters");
        elemField.setXmlName(new javax.xml.namespace.QName("", "additionalParameters"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ParameterType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "parameterType"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
