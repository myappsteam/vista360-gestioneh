/**
 * Device.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.myapps.vista_ccc.reportes_bccs.servicio;

public class Device  implements java.io.Serializable {
    private com.myapps.vista_ccc.reportes_bccs.servicio.Client client;

    private String nroRegistro;

    private String IMEI;

    private String RETM;

    private String formaRegistro;

    private String nroCuentaRegistro;

    private String usuarioRegistro;

    private String fechaIngreso;

    private String fechaActualizacion;

    private String horaActualizacion;

    private String lista;

    private String estadoControl;

    private String observacion;

    public Device() {
    }

    public Device(
           com.myapps.vista_ccc.reportes_bccs.servicio.Client client,
           String nroRegistro,
           String IMEI,
           String RETM,
           String formaRegistro,
           String nroCuentaRegistro,
           String usuarioRegistro,
           String fechaIngreso,
           String fechaActualizacion,
           String horaActualizacion,
           String lista,
           String estadoControl,
           String observacion) {
           this.client = client;
           this.nroRegistro = nroRegistro;
           this.IMEI = IMEI;
           this.RETM = RETM;
           this.formaRegistro = formaRegistro;
           this.nroCuentaRegistro = nroCuentaRegistro;
           this.usuarioRegistro = usuarioRegistro;
           this.fechaIngreso = fechaIngreso;
           this.fechaActualizacion = fechaActualizacion;
           this.horaActualizacion = horaActualizacion;
           this.lista = lista;
           this.estadoControl = estadoControl;
           this.observacion = observacion;
    }


    /**
     * Gets the client value for this Device.
     * 
     * @return client
     */
    public com.myapps.vista_ccc.reportes_bccs.servicio.Client getClient() {
        return client;
    }


    /**
     * Sets the client value for this Device.
     * 
     * @param client
     */
    public void setClient(com.myapps.vista_ccc.reportes_bccs.servicio.Client client) {
        this.client = client;
    }


    /**
     * Gets the nroRegistro value for this Device.
     * 
     * @return nroRegistro
     */
    public String getNroRegistro() {
        return nroRegistro;
    }


    /**
     * Sets the nroRegistro value for this Device.
     * 
     * @param nroRegistro
     */
    public void setNroRegistro(String nroRegistro) {
        this.nroRegistro = nroRegistro;
    }


    /**
     * Gets the IMEI value for this Device.
     * 
     * @return IMEI
     */
    public String getIMEI() {
        return IMEI;
    }


    /**
     * Sets the IMEI value for this Device.
     * 
     * @param IMEI
     */
    public void setIMEI(String IMEI) {
        this.IMEI = IMEI;
    }


    /**
     * Gets the RETM value for this Device.
     * 
     * @return RETM
     */
    public String getRETM() {
        return RETM;
    }


    /**
     * Sets the RETM value for this Device.
     * 
     * @param RETM
     */
    public void setRETM(String RETM) {
        this.RETM = RETM;
    }


    /**
     * Gets the formaRegistro value for this Device.
     * 
     * @return formaRegistro
     */
    public String getFormaRegistro() {
        return formaRegistro;
    }


    /**
     * Sets the formaRegistro value for this Device.
     * 
     * @param formaRegistro
     */
    public void setFormaRegistro(String formaRegistro) {
        this.formaRegistro = formaRegistro;
    }


    /**
     * Gets the nroCuentaRegistro value for this Device.
     * 
     * @return nroCuentaRegistro
     */
    public String getNroCuentaRegistro() {
        return nroCuentaRegistro;
    }


    /**
     * Sets the nroCuentaRegistro value for this Device.
     * 
     * @param nroCuentaRegistro
     */
    public void setNroCuentaRegistro(String nroCuentaRegistro) {
        this.nroCuentaRegistro = nroCuentaRegistro;
    }


    /**
     * Gets the usuarioRegistro value for this Device.
     * 
     * @return usuarioRegistro
     */
    public String getUsuarioRegistro() {
        return usuarioRegistro;
    }


    /**
     * Sets the usuarioRegistro value for this Device.
     * 
     * @param usuarioRegistro
     */
    public void setUsuarioRegistro(String usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }


    /**
     * Gets the fechaIngreso value for this Device.
     * 
     * @return fechaIngreso
     */
    public String getFechaIngreso() {
        return fechaIngreso;
    }


    /**
     * Sets the fechaIngreso value for this Device.
     * 
     * @param fechaIngreso
     */
    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }


    /**
     * Gets the fechaActualizacion value for this Device.
     * 
     * @return fechaActualizacion
     */
    public String getFechaActualizacion() {
        return fechaActualizacion;
    }


    /**
     * Sets the fechaActualizacion value for this Device.
     * 
     * @param fechaActualizacion
     */
    public void setFechaActualizacion(String fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }


    /**
     * Gets the horaActualizacion value for this Device.
     * 
     * @return horaActualizacion
     */
    public String getHoraActualizacion() {
        return horaActualizacion;
    }


    /**
     * Sets the horaActualizacion value for this Device.
     * 
     * @param horaActualizacion
     */
    public void setHoraActualizacion(String horaActualizacion) {
        this.horaActualizacion = horaActualizacion;
    }


    /**
     * Gets the lista value for this Device.
     * 
     * @return lista
     */
    public String getLista() {
        return lista;
    }


    /**
     * Sets the lista value for this Device.
     * 
     * @param lista
     */
    public void setLista(String lista) {
        this.lista = lista;
    }


    /**
     * Gets the estadoControl value for this Device.
     * 
     * @return estadoControl
     */
    public String getEstadoControl() {
        return estadoControl;
    }


    /**
     * Sets the estadoControl value for this Device.
     * 
     * @param estadoControl
     */
    public void setEstadoControl(String estadoControl) {
        this.estadoControl = estadoControl;
    }


    /**
     * Gets the observacion value for this Device.
     * 
     * @return observacion
     */
    public String getObservacion() {
        return observacion;
    }


    /**
     * Sets the observacion value for this Device.
     * 
     * @param observacion
     */
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof Device)) return false;
        Device other = (Device) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.client==null && other.getClient()==null) || 
             (this.client!=null &&
              this.client.equals(other.getClient()))) &&
            ((this.nroRegistro==null && other.getNroRegistro()==null) || 
             (this.nroRegistro!=null &&
              this.nroRegistro.equals(other.getNroRegistro()))) &&
            ((this.IMEI==null && other.getIMEI()==null) || 
             (this.IMEI!=null &&
              this.IMEI.equals(other.getIMEI()))) &&
            ((this.RETM==null && other.getRETM()==null) || 
             (this.RETM!=null &&
              this.RETM.equals(other.getRETM()))) &&
            ((this.formaRegistro==null && other.getFormaRegistro()==null) || 
             (this.formaRegistro!=null &&
              this.formaRegistro.equals(other.getFormaRegistro()))) &&
            ((this.nroCuentaRegistro==null && other.getNroCuentaRegistro()==null) || 
             (this.nroCuentaRegistro!=null &&
              this.nroCuentaRegistro.equals(other.getNroCuentaRegistro()))) &&
            ((this.usuarioRegistro==null && other.getUsuarioRegistro()==null) || 
             (this.usuarioRegistro!=null &&
              this.usuarioRegistro.equals(other.getUsuarioRegistro()))) &&
            ((this.fechaIngreso==null && other.getFechaIngreso()==null) || 
             (this.fechaIngreso!=null &&
              this.fechaIngreso.equals(other.getFechaIngreso()))) &&
            ((this.fechaActualizacion==null && other.getFechaActualizacion()==null) || 
             (this.fechaActualizacion!=null &&
              this.fechaActualizacion.equals(other.getFechaActualizacion()))) &&
            ((this.horaActualizacion==null && other.getHoraActualizacion()==null) || 
             (this.horaActualizacion!=null &&
              this.horaActualizacion.equals(other.getHoraActualizacion()))) &&
            ((this.lista==null && other.getLista()==null) || 
             (this.lista!=null &&
              this.lista.equals(other.getLista()))) &&
            ((this.estadoControl==null && other.getEstadoControl()==null) || 
             (this.estadoControl!=null &&
              this.estadoControl.equals(other.getEstadoControl()))) &&
            ((this.observacion==null && other.getObservacion()==null) || 
             (this.observacion!=null &&
              this.observacion.equals(other.getObservacion())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getClient() != null) {
            _hashCode += getClient().hashCode();
        }
        if (getNroRegistro() != null) {
            _hashCode += getNroRegistro().hashCode();
        }
        if (getIMEI() != null) {
            _hashCode += getIMEI().hashCode();
        }
        if (getRETM() != null) {
            _hashCode += getRETM().hashCode();
        }
        if (getFormaRegistro() != null) {
            _hashCode += getFormaRegistro().hashCode();
        }
        if (getNroCuentaRegistro() != null) {
            _hashCode += getNroCuentaRegistro().hashCode();
        }
        if (getUsuarioRegistro() != null) {
            _hashCode += getUsuarioRegistro().hashCode();
        }
        if (getFechaIngreso() != null) {
            _hashCode += getFechaIngreso().hashCode();
        }
        if (getFechaActualizacion() != null) {
            _hashCode += getFechaActualizacion().hashCode();
        }
        if (getHoraActualizacion() != null) {
            _hashCode += getHoraActualizacion().hashCode();
        }
        if (getLista() != null) {
            _hashCode += getLista().hashCode();
        }
        if (getEstadoControl() != null) {
            _hashCode += getEstadoControl().hashCode();
        }
        if (getObservacion() != null) {
            _hashCode += getObservacion().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Device.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "Device"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("client");
        elemField.setXmlName(new javax.xml.namespace.QName("", "client"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "Client"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nroRegistro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nroRegistro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IMEI");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IMEI"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RETM");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RETM"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("formaRegistro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "formaRegistro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nroCuentaRegistro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nroCuentaRegistro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuarioRegistro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usuarioRegistro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaIngreso");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fechaIngreso"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaActualizacion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fechaActualizacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horaActualizacion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horaActualizacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lista");
        elemField.setXmlName(new javax.xml.namespace.QName("", "lista"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estadoControl");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estadoControl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("observacion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "observacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
