package com.myapps.vista_ccc.reportes_bccs.entity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

public class CuentaMigradaResponseOk implements Serializable{
	private static final long serialVersionUID = 1L;

	private String estado;
	private String msisdn;
	private String fechaMigracion;

	public static List<CuentaMigradaResponseOk> getInstanceListByJson(String json) {
		Type listType = new TypeToken<List<CuentaMigradaResponseOk>>() {
		}.getType();
		Gson gson = new Gson();
		return gson.fromJson(json, listType);
	}

	/*public static CuentaMigradaResponseOk getInstanceByJson(String json) {
		Type listType = new TypeToken<CuentaMigradaResponseOk>() {
		}.getType();
		Gson gson = new Gson();
		return gson.fromJson(json, listType);
	}*/

	public static CuentaMigradaResponseOk getInstanceByJson(String stringJson) {
		Gson gson = new Gson();
		return gson.fromJson(stringJson, CuentaMigradaResponseOk.class);
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getFechaMigracion() {
		return fechaMigracion;
	}

	public void setFechaMigracion(String fechaMigracion) {
		this.fechaMigracion = fechaMigracion;
	}

	@Override
	public String toString() {
		return "CuentaMigradaResponseOk{" +
				"estado='" + estado + '\'' +
				", msisdn='" + msisdn + '\'' +
				", fechaMigracion='" + fechaMigracion + '\'' +
				'}';
	}
}
