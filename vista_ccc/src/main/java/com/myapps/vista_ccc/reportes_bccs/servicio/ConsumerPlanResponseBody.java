/**
 * ConsumerPlanResponseBody.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.myapps.vista_ccc.reportes_bccs.servicio;

public class ConsumerPlanResponseBody  implements java.io.Serializable {
    private String cliente;

    private String razonSocial;

    private String contrato;

    private String nroCuenta;

    private String tipoCuenta;

    private String tipoCuentaDescripcion;

    private String planConsumo;

    private String planConsumoDescripcion;

    private String fechaHabilitacion;

    private String fechaVencimiento;

    private String fechaUltimaFactura;

    private String estadoCuentaPlan;

    private String cantActualCuentas;

    private String cantReservada;

    private String fechaUltimaAsignacion;

    private String fechaSiguienteAsignacion;

    private String analizaAplicacionAumento;

    private String vecesAplicarAumento;

    private String cantAplicada;

    private String porcAumentoDepositol;

    private String valor;

    private String moneda;

    private String monedaDescripcion;

    private com.myapps.vista_ccc.reportes_bccs.servicio.CambiosPlan[] cambiosPlan;

    public ConsumerPlanResponseBody() {
    }

    public ConsumerPlanResponseBody(
           String cliente,
           String razonSocial,
           String contrato,
           String nroCuenta,
           String tipoCuenta,
           String tipoCuentaDescripcion,
           String planConsumo,
           String planConsumoDescripcion,
           String fechaHabilitacion,
           String fechaVencimiento,
           String fechaUltimaFactura,
           String estadoCuentaPlan,
           String cantActualCuentas,
           String cantReservada,
           String fechaUltimaAsignacion,
           String fechaSiguienteAsignacion,
           String analizaAplicacionAumento,
           String vecesAplicarAumento,
           String cantAplicada,
           String porcAumentoDepositol,
           String valor,
           String moneda,
           String monedaDescripcion,
           com.myapps.vista_ccc.reportes_bccs.servicio.CambiosPlan[] cambiosPlan) {
           this.cliente = cliente;
           this.razonSocial = razonSocial;
           this.contrato = contrato;
           this.nroCuenta = nroCuenta;
           this.tipoCuenta = tipoCuenta;
           this.tipoCuentaDescripcion = tipoCuentaDescripcion;
           this.planConsumo = planConsumo;
           this.planConsumoDescripcion = planConsumoDescripcion;
           this.fechaHabilitacion = fechaHabilitacion;
           this.fechaVencimiento = fechaVencimiento;
           this.fechaUltimaFactura = fechaUltimaFactura;
           this.estadoCuentaPlan = estadoCuentaPlan;
           this.cantActualCuentas = cantActualCuentas;
           this.cantReservada = cantReservada;
           this.fechaUltimaAsignacion = fechaUltimaAsignacion;
           this.fechaSiguienteAsignacion = fechaSiguienteAsignacion;
           this.analizaAplicacionAumento = analizaAplicacionAumento;
           this.vecesAplicarAumento = vecesAplicarAumento;
           this.cantAplicada = cantAplicada;
           this.porcAumentoDepositol = porcAumentoDepositol;
           this.valor = valor;
           this.moneda = moneda;
           this.monedaDescripcion = monedaDescripcion;
           this.cambiosPlan = cambiosPlan;
    }


    /**
     * Gets the cliente value for this ConsumerPlanResponseBody.
     * 
     * @return cliente
     */
    public String getCliente() {
        return cliente;
    }


    /**
     * Sets the cliente value for this ConsumerPlanResponseBody.
     * 
     * @param cliente
     */
    public void setCliente(String cliente) {
        this.cliente = cliente;
    }


    /**
     * Gets the razonSocial value for this ConsumerPlanResponseBody.
     * 
     * @return razonSocial
     */
    public String getRazonSocial() {
        return razonSocial;
    }


    /**
     * Sets the razonSocial value for this ConsumerPlanResponseBody.
     * 
     * @param razonSocial
     */
    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }


    /**
     * Gets the contrato value for this ConsumerPlanResponseBody.
     * 
     * @return contrato
     */
    public String getContrato() {
        return contrato;
    }


    /**
     * Sets the contrato value for this ConsumerPlanResponseBody.
     * 
     * @param contrato
     */
    public void setContrato(String contrato) {
        this.contrato = contrato;
    }


    /**
     * Gets the nroCuenta value for this ConsumerPlanResponseBody.
     * 
     * @return nroCuenta
     */
    public String getNroCuenta() {
        return nroCuenta;
    }


    /**
     * Sets the nroCuenta value for this ConsumerPlanResponseBody.
     * 
     * @param nroCuenta
     */
    public void setNroCuenta(String nroCuenta) {
        this.nroCuenta = nroCuenta;
    }


    /**
     * Gets the tipoCuenta value for this ConsumerPlanResponseBody.
     * 
     * @return tipoCuenta
     */
    public String getTipoCuenta() {
        return tipoCuenta;
    }


    /**
     * Sets the tipoCuenta value for this ConsumerPlanResponseBody.
     * 
     * @param tipoCuenta
     */
    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }


    /**
     * Gets the tipoCuentaDescripcion value for this ConsumerPlanResponseBody.
     * 
     * @return tipoCuentaDescripcion
     */
    public String getTipoCuentaDescripcion() {
        return tipoCuentaDescripcion;
    }


    /**
     * Sets the tipoCuentaDescripcion value for this ConsumerPlanResponseBody.
     * 
     * @param tipoCuentaDescripcion
     */
    public void setTipoCuentaDescripcion(String tipoCuentaDescripcion) {
        this.tipoCuentaDescripcion = tipoCuentaDescripcion;
    }


    /**
     * Gets the planConsumo value for this ConsumerPlanResponseBody.
     * 
     * @return planConsumo
     */
    public String getPlanConsumo() {
        return planConsumo;
    }


    /**
     * Sets the planConsumo value for this ConsumerPlanResponseBody.
     * 
     * @param planConsumo
     */
    public void setPlanConsumo(String planConsumo) {
        this.planConsumo = planConsumo;
    }


    /**
     * Gets the planConsumoDescripcion value for this ConsumerPlanResponseBody.
     * 
     * @return planConsumoDescripcion
     */
    public String getPlanConsumoDescripcion() {
        return planConsumoDescripcion;
    }


    /**
     * Sets the planConsumoDescripcion value for this ConsumerPlanResponseBody.
     * 
     * @param planConsumoDescripcion
     */
    public void setPlanConsumoDescripcion(String planConsumoDescripcion) {
        this.planConsumoDescripcion = planConsumoDescripcion;
    }


    /**
     * Gets the fechaHabilitacion value for this ConsumerPlanResponseBody.
     * 
     * @return fechaHabilitacion
     */
    public String getFechaHabilitacion() {
        return fechaHabilitacion;
    }


    /**
     * Sets the fechaHabilitacion value for this ConsumerPlanResponseBody.
     * 
     * @param fechaHabilitacion
     */
    public void setFechaHabilitacion(String fechaHabilitacion) {
        this.fechaHabilitacion = fechaHabilitacion;
    }


    /**
     * Gets the fechaVencimiento value for this ConsumerPlanResponseBody.
     * 
     * @return fechaVencimiento
     */
    public String getFechaVencimiento() {
        return fechaVencimiento;
    }


    /**
     * Sets the fechaVencimiento value for this ConsumerPlanResponseBody.
     * 
     * @param fechaVencimiento
     */
    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }


    /**
     * Gets the fechaUltimaFactura value for this ConsumerPlanResponseBody.
     * 
     * @return fechaUltimaFactura
     */
    public String getFechaUltimaFactura() {
        return fechaUltimaFactura;
    }


    /**
     * Sets the fechaUltimaFactura value for this ConsumerPlanResponseBody.
     * 
     * @param fechaUltimaFactura
     */
    public void setFechaUltimaFactura(String fechaUltimaFactura) {
        this.fechaUltimaFactura = fechaUltimaFactura;
    }


    /**
     * Gets the estadoCuentaPlan value for this ConsumerPlanResponseBody.
     * 
     * @return estadoCuentaPlan
     */
    public String getEstadoCuentaPlan() {
        return estadoCuentaPlan;
    }


    /**
     * Sets the estadoCuentaPlan value for this ConsumerPlanResponseBody.
     * 
     * @param estadoCuentaPlan
     */
    public void setEstadoCuentaPlan(String estadoCuentaPlan) {
        this.estadoCuentaPlan = estadoCuentaPlan;
    }


    /**
     * Gets the cantActualCuentas value for this ConsumerPlanResponseBody.
     * 
     * @return cantActualCuentas
     */
    public String getCantActualCuentas() {
        return cantActualCuentas;
    }


    /**
     * Sets the cantActualCuentas value for this ConsumerPlanResponseBody.
     * 
     * @param cantActualCuentas
     */
    public void setCantActualCuentas(String cantActualCuentas) {
        this.cantActualCuentas = cantActualCuentas;
    }


    /**
     * Gets the cantReservada value for this ConsumerPlanResponseBody.
     * 
     * @return cantReservada
     */
    public String getCantReservada() {
        return cantReservada;
    }


    /**
     * Sets the cantReservada value for this ConsumerPlanResponseBody.
     * 
     * @param cantReservada
     */
    public void setCantReservada(String cantReservada) {
        this.cantReservada = cantReservada;
    }


    /**
     * Gets the fechaUltimaAsignacion value for this ConsumerPlanResponseBody.
     * 
     * @return fechaUltimaAsignacion
     */
    public String getFechaUltimaAsignacion() {
        return fechaUltimaAsignacion;
    }


    /**
     * Sets the fechaUltimaAsignacion value for this ConsumerPlanResponseBody.
     * 
     * @param fechaUltimaAsignacion
     */
    public void setFechaUltimaAsignacion(String fechaUltimaAsignacion) {
        this.fechaUltimaAsignacion = fechaUltimaAsignacion;
    }


    /**
     * Gets the fechaSiguienteAsignacion value for this ConsumerPlanResponseBody.
     * 
     * @return fechaSiguienteAsignacion
     */
    public String getFechaSiguienteAsignacion() {
        return fechaSiguienteAsignacion;
    }


    /**
     * Sets the fechaSiguienteAsignacion value for this ConsumerPlanResponseBody.
     * 
     * @param fechaSiguienteAsignacion
     */
    public void setFechaSiguienteAsignacion(String fechaSiguienteAsignacion) {
        this.fechaSiguienteAsignacion = fechaSiguienteAsignacion;
    }


    /**
     * Gets the analizaAplicacionAumento value for this ConsumerPlanResponseBody.
     * 
     * @return analizaAplicacionAumento
     */
    public String getAnalizaAplicacionAumento() {
        return analizaAplicacionAumento;
    }


    /**
     * Sets the analizaAplicacionAumento value for this ConsumerPlanResponseBody.
     * 
     * @param analizaAplicacionAumento
     */
    public void setAnalizaAplicacionAumento(String analizaAplicacionAumento) {
        this.analizaAplicacionAumento = analizaAplicacionAumento;
    }


    /**
     * Gets the vecesAplicarAumento value for this ConsumerPlanResponseBody.
     * 
     * @return vecesAplicarAumento
     */
    public String getVecesAplicarAumento() {
        return vecesAplicarAumento;
    }


    /**
     * Sets the vecesAplicarAumento value for this ConsumerPlanResponseBody.
     * 
     * @param vecesAplicarAumento
     */
    public void setVecesAplicarAumento(String vecesAplicarAumento) {
        this.vecesAplicarAumento = vecesAplicarAumento;
    }


    /**
     * Gets the cantAplicada value for this ConsumerPlanResponseBody.
     * 
     * @return cantAplicada
     */
    public String getCantAplicada() {
        return cantAplicada;
    }


    /**
     * Sets the cantAplicada value for this ConsumerPlanResponseBody.
     * 
     * @param cantAplicada
     */
    public void setCantAplicada(String cantAplicada) {
        this.cantAplicada = cantAplicada;
    }


    /**
     * Gets the porcAumentoDepositol value for this ConsumerPlanResponseBody.
     * 
     * @return porcAumentoDepositol
     */
    public String getPorcAumentoDepositol() {
        return porcAumentoDepositol;
    }


    /**
     * Sets the porcAumentoDepositol value for this ConsumerPlanResponseBody.
     * 
     * @param porcAumentoDepositol
     */
    public void setPorcAumentoDepositol(String porcAumentoDepositol) {
        this.porcAumentoDepositol = porcAumentoDepositol;
    }


    /**
     * Gets the valor value for this ConsumerPlanResponseBody.
     * 
     * @return valor
     */
    public String getValor() {
        return valor;
    }


    /**
     * Sets the valor value for this ConsumerPlanResponseBody.
     * 
     * @param valor
     */
    public void setValor(String valor) {
        this.valor = valor;
    }


    /**
     * Gets the moneda value for this ConsumerPlanResponseBody.
     * 
     * @return moneda
     */
    public String getMoneda() {
        return moneda;
    }


    /**
     * Sets the moneda value for this ConsumerPlanResponseBody.
     * 
     * @param moneda
     */
    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }


    /**
     * Gets the monedaDescripcion value for this ConsumerPlanResponseBody.
     * 
     * @return monedaDescripcion
     */
    public String getMonedaDescripcion() {
        return monedaDescripcion;
    }


    /**
     * Sets the monedaDescripcion value for this ConsumerPlanResponseBody.
     * 
     * @param monedaDescripcion
     */
    public void setMonedaDescripcion(String monedaDescripcion) {
        this.monedaDescripcion = monedaDescripcion;
    }


    /**
     * Gets the cambiosPlan value for this ConsumerPlanResponseBody.
     * 
     * @return cambiosPlan
     */
    public com.myapps.vista_ccc.reportes_bccs.servicio.CambiosPlan[] getCambiosPlan() {
        return cambiosPlan;
    }


    /**
     * Sets the cambiosPlan value for this ConsumerPlanResponseBody.
     * 
     * @param cambiosPlan
     */
    public void setCambiosPlan(com.myapps.vista_ccc.reportes_bccs.servicio.CambiosPlan[] cambiosPlan) {
        this.cambiosPlan = cambiosPlan;
    }

    public com.myapps.vista_ccc.reportes_bccs.servicio.CambiosPlan getCambiosPlan(int i) {
        return this.cambiosPlan[i];
    }

    public void setCambiosPlan(int i, com.myapps.vista_ccc.reportes_bccs.servicio.CambiosPlan _value) {
        this.cambiosPlan[i] = _value;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof ConsumerPlanResponseBody)) return false;
        ConsumerPlanResponseBody other = (ConsumerPlanResponseBody) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cliente==null && other.getCliente()==null) || 
             (this.cliente!=null &&
              this.cliente.equals(other.getCliente()))) &&
            ((this.razonSocial==null && other.getRazonSocial()==null) || 
             (this.razonSocial!=null &&
              this.razonSocial.equals(other.getRazonSocial()))) &&
            ((this.contrato==null && other.getContrato()==null) || 
             (this.contrato!=null &&
              this.contrato.equals(other.getContrato()))) &&
            ((this.nroCuenta==null && other.getNroCuenta()==null) || 
             (this.nroCuenta!=null &&
              this.nroCuenta.equals(other.getNroCuenta()))) &&
            ((this.tipoCuenta==null && other.getTipoCuenta()==null) || 
             (this.tipoCuenta!=null &&
              this.tipoCuenta.equals(other.getTipoCuenta()))) &&
            ((this.tipoCuentaDescripcion==null && other.getTipoCuentaDescripcion()==null) || 
             (this.tipoCuentaDescripcion!=null &&
              this.tipoCuentaDescripcion.equals(other.getTipoCuentaDescripcion()))) &&
            ((this.planConsumo==null && other.getPlanConsumo()==null) || 
             (this.planConsumo!=null &&
              this.planConsumo.equals(other.getPlanConsumo()))) &&
            ((this.planConsumoDescripcion==null && other.getPlanConsumoDescripcion()==null) || 
             (this.planConsumoDescripcion!=null &&
              this.planConsumoDescripcion.equals(other.getPlanConsumoDescripcion()))) &&
            ((this.fechaHabilitacion==null && other.getFechaHabilitacion()==null) || 
             (this.fechaHabilitacion!=null &&
              this.fechaHabilitacion.equals(other.getFechaHabilitacion()))) &&
            ((this.fechaVencimiento==null && other.getFechaVencimiento()==null) || 
             (this.fechaVencimiento!=null &&
              this.fechaVencimiento.equals(other.getFechaVencimiento()))) &&
            ((this.fechaUltimaFactura==null && other.getFechaUltimaFactura()==null) || 
             (this.fechaUltimaFactura!=null &&
              this.fechaUltimaFactura.equals(other.getFechaUltimaFactura()))) &&
            ((this.estadoCuentaPlan==null && other.getEstadoCuentaPlan()==null) || 
             (this.estadoCuentaPlan!=null &&
              this.estadoCuentaPlan.equals(other.getEstadoCuentaPlan()))) &&
            ((this.cantActualCuentas==null && other.getCantActualCuentas()==null) || 
             (this.cantActualCuentas!=null &&
              this.cantActualCuentas.equals(other.getCantActualCuentas()))) &&
            ((this.cantReservada==null && other.getCantReservada()==null) || 
             (this.cantReservada!=null &&
              this.cantReservada.equals(other.getCantReservada()))) &&
            ((this.fechaUltimaAsignacion==null && other.getFechaUltimaAsignacion()==null) || 
             (this.fechaUltimaAsignacion!=null &&
              this.fechaUltimaAsignacion.equals(other.getFechaUltimaAsignacion()))) &&
            ((this.fechaSiguienteAsignacion==null && other.getFechaSiguienteAsignacion()==null) || 
             (this.fechaSiguienteAsignacion!=null &&
              this.fechaSiguienteAsignacion.equals(other.getFechaSiguienteAsignacion()))) &&
            ((this.analizaAplicacionAumento==null && other.getAnalizaAplicacionAumento()==null) || 
             (this.analizaAplicacionAumento!=null &&
              this.analizaAplicacionAumento.equals(other.getAnalizaAplicacionAumento()))) &&
            ((this.vecesAplicarAumento==null && other.getVecesAplicarAumento()==null) || 
             (this.vecesAplicarAumento!=null &&
              this.vecesAplicarAumento.equals(other.getVecesAplicarAumento()))) &&
            ((this.cantAplicada==null && other.getCantAplicada()==null) || 
             (this.cantAplicada!=null &&
              this.cantAplicada.equals(other.getCantAplicada()))) &&
            ((this.porcAumentoDepositol==null && other.getPorcAumentoDepositol()==null) || 
             (this.porcAumentoDepositol!=null &&
              this.porcAumentoDepositol.equals(other.getPorcAumentoDepositol()))) &&
            ((this.valor==null && other.getValor()==null) || 
             (this.valor!=null &&
              this.valor.equals(other.getValor()))) &&
            ((this.moneda==null && other.getMoneda()==null) || 
             (this.moneda!=null &&
              this.moneda.equals(other.getMoneda()))) &&
            ((this.monedaDescripcion==null && other.getMonedaDescripcion()==null) || 
             (this.monedaDescripcion!=null &&
              this.monedaDescripcion.equals(other.getMonedaDescripcion()))) &&
            ((this.cambiosPlan==null && other.getCambiosPlan()==null) || 
             (this.cambiosPlan!=null &&
              java.util.Arrays.equals(this.cambiosPlan, other.getCambiosPlan())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCliente() != null) {
            _hashCode += getCliente().hashCode();
        }
        if (getRazonSocial() != null) {
            _hashCode += getRazonSocial().hashCode();
        }
        if (getContrato() != null) {
            _hashCode += getContrato().hashCode();
        }
        if (getNroCuenta() != null) {
            _hashCode += getNroCuenta().hashCode();
        }
        if (getTipoCuenta() != null) {
            _hashCode += getTipoCuenta().hashCode();
        }
        if (getTipoCuentaDescripcion() != null) {
            _hashCode += getTipoCuentaDescripcion().hashCode();
        }
        if (getPlanConsumo() != null) {
            _hashCode += getPlanConsumo().hashCode();
        }
        if (getPlanConsumoDescripcion() != null) {
            _hashCode += getPlanConsumoDescripcion().hashCode();
        }
        if (getFechaHabilitacion() != null) {
            _hashCode += getFechaHabilitacion().hashCode();
        }
        if (getFechaVencimiento() != null) {
            _hashCode += getFechaVencimiento().hashCode();
        }
        if (getFechaUltimaFactura() != null) {
            _hashCode += getFechaUltimaFactura().hashCode();
        }
        if (getEstadoCuentaPlan() != null) {
            _hashCode += getEstadoCuentaPlan().hashCode();
        }
        if (getCantActualCuentas() != null) {
            _hashCode += getCantActualCuentas().hashCode();
        }
        if (getCantReservada() != null) {
            _hashCode += getCantReservada().hashCode();
        }
        if (getFechaUltimaAsignacion() != null) {
            _hashCode += getFechaUltimaAsignacion().hashCode();
        }
        if (getFechaSiguienteAsignacion() != null) {
            _hashCode += getFechaSiguienteAsignacion().hashCode();
        }
        if (getAnalizaAplicacionAumento() != null) {
            _hashCode += getAnalizaAplicacionAumento().hashCode();
        }
        if (getVecesAplicarAumento() != null) {
            _hashCode += getVecesAplicarAumento().hashCode();
        }
        if (getCantAplicada() != null) {
            _hashCode += getCantAplicada().hashCode();
        }
        if (getPorcAumentoDepositol() != null) {
            _hashCode += getPorcAumentoDepositol().hashCode();
        }
        if (getValor() != null) {
            _hashCode += getValor().hashCode();
        }
        if (getMoneda() != null) {
            _hashCode += getMoneda().hashCode();
        }
        if (getMonedaDescripcion() != null) {
            _hashCode += getMonedaDescripcion().hashCode();
        }
        if (getCambiosPlan() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCambiosPlan());
                 i++) {
                Object obj = java.lang.reflect.Array.get(getCambiosPlan(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ConsumerPlanResponseBody.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ConsumerPlanResponseBody"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cliente");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cliente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("razonSocial");
        elemField.setXmlName(new javax.xml.namespace.QName("", "razonSocial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contrato");
        elemField.setXmlName(new javax.xml.namespace.QName("", "contrato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nroCuenta");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nroCuenta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoCuenta");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipoCuenta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoCuentaDescripcion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipoCuentaDescripcion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planConsumo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "planConsumo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planConsumoDescripcion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "planConsumoDescripcion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaHabilitacion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fechaHabilitacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaVencimiento");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fechaVencimiento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaUltimaFactura");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fechaUltimaFactura"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estadoCuentaPlan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estadoCuentaPlan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cantActualCuentas");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cantActualCuentas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cantReservada");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cantReservada"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaUltimaAsignacion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fechaUltimaAsignacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaSiguienteAsignacion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fechaSiguienteAsignacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("analizaAplicacionAumento");
        elemField.setXmlName(new javax.xml.namespace.QName("", "analizaAplicacionAumento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vecesAplicarAumento");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vecesAplicarAumento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cantAplicada");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cantAplicada"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("porcAumentoDepositol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "porcAumentoDepositol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "valor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("moneda");
        elemField.setXmlName(new javax.xml.namespace.QName("", "moneda"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("monedaDescripcion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "monedaDescripcion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cambiosPlan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cambiosPlan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "CambiosPlan"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
