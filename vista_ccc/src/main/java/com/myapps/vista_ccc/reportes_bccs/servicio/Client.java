/**
 * Client.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.myapps.vista_ccc.reportes_bccs.servicio;

public class Client  implements java.io.Serializable {
    private String nroRegistro;

    private String tipoPersona;

    private String razonSocial;

    private String nroMatricula;

    private String NIT;

    private String nroDocumentoDescripcion;

    private String nroDocumento;

    private String nombres;

    private String apellidos;

    private String fechaNacimiento;

    private String OEP;

    private String nacionalidad;

    private String formaRegistro;

    private String usuarioIngreso;

    private String fechaIngreso;

    private String horaIngreso;

    public Client() {
    }

    public Client(
           String nroRegistro,
           String tipoPersona,
           String razonSocial,
           String nroMatricula,
           String NIT,
           String nroDocumentoDescripcion,
           String nroDocumento,
           String nombres,
           String apellidos,
           String fechaNacimiento,
           String OEP,
           String nacionalidad,
           String formaRegistro,
           String usuarioIngreso,
           String fechaIngreso,
           String horaIngreso) {
           this.nroRegistro = nroRegistro;
           this.tipoPersona = tipoPersona;
           this.razonSocial = razonSocial;
           this.nroMatricula = nroMatricula;
           this.NIT = NIT;
           this.nroDocumentoDescripcion = nroDocumentoDescripcion;
           this.nroDocumento = nroDocumento;
           this.nombres = nombres;
           this.apellidos = apellidos;
           this.fechaNacimiento = fechaNacimiento;
           this.OEP = OEP;
           this.nacionalidad = nacionalidad;
           this.formaRegistro = formaRegistro;
           this.usuarioIngreso = usuarioIngreso;
           this.fechaIngreso = fechaIngreso;
           this.horaIngreso = horaIngreso;
    }


    /**
     * Gets the nroRegistro value for this Client.
     * 
     * @return nroRegistro
     */
    public String getNroRegistro() {
        return nroRegistro;
    }


    /**
     * Sets the nroRegistro value for this Client.
     * 
     * @param nroRegistro
     */
    public void setNroRegistro(String nroRegistro) {
        this.nroRegistro = nroRegistro;
    }


    /**
     * Gets the tipoPersona value for this Client.
     * 
     * @return tipoPersona
     */
    public String getTipoPersona() {
        return tipoPersona;
    }


    /**
     * Sets the tipoPersona value for this Client.
     * 
     * @param tipoPersona
     */
    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }


    /**
     * Gets the razonSocial value for this Client.
     * 
     * @return razonSocial
     */
    public String getRazonSocial() {
        return razonSocial;
    }


    /**
     * Sets the razonSocial value for this Client.
     * 
     * @param razonSocial
     */
    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }


    /**
     * Gets the nroMatricula value for this Client.
     * 
     * @return nroMatricula
     */
    public String getNroMatricula() {
        return nroMatricula;
    }


    /**
     * Sets the nroMatricula value for this Client.
     * 
     * @param nroMatricula
     */
    public void setNroMatricula(String nroMatricula) {
        this.nroMatricula = nroMatricula;
    }


    /**
     * Gets the NIT value for this Client.
     * 
     * @return NIT
     */
    public String getNIT() {
        return NIT;
    }


    /**
     * Sets the NIT value for this Client.
     * 
     * @param NIT
     */
    public void setNIT(String NIT) {
        this.NIT = NIT;
    }


    /**
     * Gets the nroDocumentoDescripcion value for this Client.
     * 
     * @return nroDocumentoDescripcion
     */
    public String getNroDocumentoDescripcion() {
        return nroDocumentoDescripcion;
    }


    /**
     * Sets the nroDocumentoDescripcion value for this Client.
     * 
     * @param nroDocumentoDescripcion
     */
    public void setNroDocumentoDescripcion(String nroDocumentoDescripcion) {
        this.nroDocumentoDescripcion = nroDocumentoDescripcion;
    }


    /**
     * Gets the nroDocumento value for this Client.
     * 
     * @return nroDocumento
     */
    public String getNroDocumento() {
        return nroDocumento;
    }


    /**
     * Sets the nroDocumento value for this Client.
     * 
     * @param nroDocumento
     */
    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }


    /**
     * Gets the nombres value for this Client.
     * 
     * @return nombres
     */
    public String getNombres() {
        return nombres;
    }


    /**
     * Sets the nombres value for this Client.
     * 
     * @param nombres
     */
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }


    /**
     * Gets the apellidos value for this Client.
     * 
     * @return apellidos
     */
    public String getApellidos() {
        return apellidos;
    }


    /**
     * Sets the apellidos value for this Client.
     * 
     * @param apellidos
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }


    /**
     * Gets the fechaNacimiento value for this Client.
     * 
     * @return fechaNacimiento
     */
    public String getFechaNacimiento() {
        return fechaNacimiento;
    }


    /**
     * Sets the fechaNacimiento value for this Client.
     * 
     * @param fechaNacimiento
     */
    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }


    /**
     * Gets the OEP value for this Client.
     * 
     * @return OEP
     */
    public String getOEP() {
        return OEP;
    }


    /**
     * Sets the OEP value for this Client.
     * 
     * @param OEP
     */
    public void setOEP(String OEP) {
        this.OEP = OEP;
    }


    /**
     * Gets the nacionalidad value for this Client.
     * 
     * @return nacionalidad
     */
    public String getNacionalidad() {
        return nacionalidad;
    }


    /**
     * Sets the nacionalidad value for this Client.
     * 
     * @param nacionalidad
     */
    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }


    /**
     * Gets the formaRegistro value for this Client.
     * 
     * @return formaRegistro
     */
    public String getFormaRegistro() {
        return formaRegistro;
    }


    /**
     * Sets the formaRegistro value for this Client.
     * 
     * @param formaRegistro
     */
    public void setFormaRegistro(String formaRegistro) {
        this.formaRegistro = formaRegistro;
    }


    /**
     * Gets the usuarioIngreso value for this Client.
     * 
     * @return usuarioIngreso
     */
    public String getUsuarioIngreso() {
        return usuarioIngreso;
    }


    /**
     * Sets the usuarioIngreso value for this Client.
     * 
     * @param usuarioIngreso
     */
    public void setUsuarioIngreso(String usuarioIngreso) {
        this.usuarioIngreso = usuarioIngreso;
    }


    /**
     * Gets the fechaIngreso value for this Client.
     * 
     * @return fechaIngreso
     */
    public String getFechaIngreso() {
        return fechaIngreso;
    }


    /**
     * Sets the fechaIngreso value for this Client.
     * 
     * @param fechaIngreso
     */
    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }


    /**
     * Gets the horaIngreso value for this Client.
     * 
     * @return horaIngreso
     */
    public String getHoraIngreso() {
        return horaIngreso;
    }


    /**
     * Sets the horaIngreso value for this Client.
     * 
     * @param horaIngreso
     */
    public void setHoraIngreso(String horaIngreso) {
        this.horaIngreso = horaIngreso;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof Client)) return false;
        Client other = (Client) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.nroRegistro==null && other.getNroRegistro()==null) || 
             (this.nroRegistro!=null &&
              this.nroRegistro.equals(other.getNroRegistro()))) &&
            ((this.tipoPersona==null && other.getTipoPersona()==null) || 
             (this.tipoPersona!=null &&
              this.tipoPersona.equals(other.getTipoPersona()))) &&
            ((this.razonSocial==null && other.getRazonSocial()==null) || 
             (this.razonSocial!=null &&
              this.razonSocial.equals(other.getRazonSocial()))) &&
            ((this.nroMatricula==null && other.getNroMatricula()==null) || 
             (this.nroMatricula!=null &&
              this.nroMatricula.equals(other.getNroMatricula()))) &&
            ((this.NIT==null && other.getNIT()==null) || 
             (this.NIT!=null &&
              this.NIT.equals(other.getNIT()))) &&
            ((this.nroDocumentoDescripcion==null && other.getNroDocumentoDescripcion()==null) || 
             (this.nroDocumentoDescripcion!=null &&
              this.nroDocumentoDescripcion.equals(other.getNroDocumentoDescripcion()))) &&
            ((this.nroDocumento==null && other.getNroDocumento()==null) || 
             (this.nroDocumento!=null &&
              this.nroDocumento.equals(other.getNroDocumento()))) &&
            ((this.nombres==null && other.getNombres()==null) || 
             (this.nombres!=null &&
              this.nombres.equals(other.getNombres()))) &&
            ((this.apellidos==null && other.getApellidos()==null) || 
             (this.apellidos!=null &&
              this.apellidos.equals(other.getApellidos()))) &&
            ((this.fechaNacimiento==null && other.getFechaNacimiento()==null) || 
             (this.fechaNacimiento!=null &&
              this.fechaNacimiento.equals(other.getFechaNacimiento()))) &&
            ((this.OEP==null && other.getOEP()==null) || 
             (this.OEP!=null &&
              this.OEP.equals(other.getOEP()))) &&
            ((this.nacionalidad==null && other.getNacionalidad()==null) || 
             (this.nacionalidad!=null &&
              this.nacionalidad.equals(other.getNacionalidad()))) &&
            ((this.formaRegistro==null && other.getFormaRegistro()==null) || 
             (this.formaRegistro!=null &&
              this.formaRegistro.equals(other.getFormaRegistro()))) &&
            ((this.usuarioIngreso==null && other.getUsuarioIngreso()==null) || 
             (this.usuarioIngreso!=null &&
              this.usuarioIngreso.equals(other.getUsuarioIngreso()))) &&
            ((this.fechaIngreso==null && other.getFechaIngreso()==null) || 
             (this.fechaIngreso!=null &&
              this.fechaIngreso.equals(other.getFechaIngreso()))) &&
            ((this.horaIngreso==null && other.getHoraIngreso()==null) || 
             (this.horaIngreso!=null &&
              this.horaIngreso.equals(other.getHoraIngreso())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNroRegistro() != null) {
            _hashCode += getNroRegistro().hashCode();
        }
        if (getTipoPersona() != null) {
            _hashCode += getTipoPersona().hashCode();
        }
        if (getRazonSocial() != null) {
            _hashCode += getRazonSocial().hashCode();
        }
        if (getNroMatricula() != null) {
            _hashCode += getNroMatricula().hashCode();
        }
        if (getNIT() != null) {
            _hashCode += getNIT().hashCode();
        }
        if (getNroDocumentoDescripcion() != null) {
            _hashCode += getNroDocumentoDescripcion().hashCode();
        }
        if (getNroDocumento() != null) {
            _hashCode += getNroDocumento().hashCode();
        }
        if (getNombres() != null) {
            _hashCode += getNombres().hashCode();
        }
        if (getApellidos() != null) {
            _hashCode += getApellidos().hashCode();
        }
        if (getFechaNacimiento() != null) {
            _hashCode += getFechaNacimiento().hashCode();
        }
        if (getOEP() != null) {
            _hashCode += getOEP().hashCode();
        }
        if (getNacionalidad() != null) {
            _hashCode += getNacionalidad().hashCode();
        }
        if (getFormaRegistro() != null) {
            _hashCode += getFormaRegistro().hashCode();
        }
        if (getUsuarioIngreso() != null) {
            _hashCode += getUsuarioIngreso().hashCode();
        }
        if (getFechaIngreso() != null) {
            _hashCode += getFechaIngreso().hashCode();
        }
        if (getHoraIngreso() != null) {
            _hashCode += getHoraIngreso().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Client.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "Client"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nroRegistro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nroRegistro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoPersona");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipoPersona"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("razonSocial");
        elemField.setXmlName(new javax.xml.namespace.QName("", "razonSocial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nroMatricula");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nroMatricula"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NIT");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NIT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nroDocumentoDescripcion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nroDocumentoDescripcion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nroDocumento");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nroDocumento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nombres");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nombres"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("apellidos");
        elemField.setXmlName(new javax.xml.namespace.QName("", "apellidos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaNacimiento");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fechaNacimiento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OEP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "OEP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nacionalidad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nacionalidad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("formaRegistro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "formaRegistro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuarioIngreso");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usuarioIngreso"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaIngreso");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fechaIngreso"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horaIngreso");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horaIngreso"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
