package com.myapps.vista_ccc.reportes_bccs.error;

import java.util.Map;

public class WhiteListResponseException extends Exception {

	private static final long serialVersionUID = 1L;
	private final transient Map<String, String> response;
	private final transient int codigo;

	public WhiteListResponseException(Map<String, String> response, int codigo) {
		this.response = response;
		this.codigo = codigo;
	}

	public Map<String, String> getResponse() {
		return response;
	}

	public int getCodigo() {
		return codigo;
	}

}
