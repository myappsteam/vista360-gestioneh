/**
 * DetalleCuenta.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.myapps.vista_ccc.reportes_bccs.servicio;

public class DetalleCuenta  implements java.io.Serializable {
    private String fecha;

    private String hora;

    private String estadoAnterior;

    private String estadoPosterior;

    private String usuario;

    private String apellidos;

    private String nombres;

    private String nroMEMO;

    private String codProceso;

    private String descripcionProceso;

    private String codDetalleProceso;

    private String descripcionDetalle;

    public DetalleCuenta() {
    }

    public DetalleCuenta(
           String fecha,
           String hora,
           String estadoAnterior,
           String estadoPosterior,
           String usuario,
           String apellidos,
           String nombres,
           String nroMEMO,
           String codProceso,
           String descripcionProceso,
           String codDetalleProceso,
           String descripcionDetalle) {
           this.fecha = fecha;
           this.hora = hora;
           this.estadoAnterior = estadoAnterior;
           this.estadoPosterior = estadoPosterior;
           this.usuario = usuario;
           this.apellidos = apellidos;
           this.nombres = nombres;
           this.nroMEMO = nroMEMO;
           this.codProceso = codProceso;
           this.descripcionProceso = descripcionProceso;
           this.codDetalleProceso = codDetalleProceso;
           this.descripcionDetalle = descripcionDetalle;
    }


    /**
     * Gets the fecha value for this DetalleCuenta.
     * 
     * @return fecha
     */
    public String getFecha() {
        return fecha;
    }


    /**
     * Sets the fecha value for this DetalleCuenta.
     * 
     * @param fecha
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }


    /**
     * Gets the hora value for this DetalleCuenta.
     * 
     * @return hora
     */
    public String getHora() {
        return hora;
    }


    /**
     * Sets the hora value for this DetalleCuenta.
     * 
     * @param hora
     */
    public void setHora(String hora) {
        this.hora = hora;
    }


    /**
     * Gets the estadoAnterior value for this DetalleCuenta.
     * 
     * @return estadoAnterior
     */
    public String getEstadoAnterior() {
        return estadoAnterior;
    }


    /**
     * Sets the estadoAnterior value for this DetalleCuenta.
     * 
     * @param estadoAnterior
     */
    public void setEstadoAnterior(String estadoAnterior) {
        this.estadoAnterior = estadoAnterior;
    }


    /**
     * Gets the estadoPosterior value for this DetalleCuenta.
     * 
     * @return estadoPosterior
     */
    public String getEstadoPosterior() {
        return estadoPosterior;
    }


    /**
     * Sets the estadoPosterior value for this DetalleCuenta.
     * 
     * @param estadoPosterior
     */
    public void setEstadoPosterior(String estadoPosterior) {
        this.estadoPosterior = estadoPosterior;
    }


    /**
     * Gets the usuario value for this DetalleCuenta.
     * 
     * @return usuario
     */
    public String getUsuario() {
        return usuario;
    }


    /**
     * Sets the usuario value for this DetalleCuenta.
     * 
     * @param usuario
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }


    /**
     * Gets the apellidos value for this DetalleCuenta.
     * 
     * @return apellidos
     */
    public String getApellidos() {
        return apellidos;
    }


    /**
     * Sets the apellidos value for this DetalleCuenta.
     * 
     * @param apellidos
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }


    /**
     * Gets the nombres value for this DetalleCuenta.
     * 
     * @return nombres
     */
    public String getNombres() {
        return nombres;
    }


    /**
     * Sets the nombres value for this DetalleCuenta.
     * 
     * @param nombres
     */
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }


    /**
     * Gets the nroMEMO value for this DetalleCuenta.
     * 
     * @return nroMEMO
     */
    public String getNroMEMO() {
        return nroMEMO;
    }


    /**
     * Sets the nroMEMO value for this DetalleCuenta.
     * 
     * @param nroMEMO
     */
    public void setNroMEMO(String nroMEMO) {
        this.nroMEMO = nroMEMO;
    }


    /**
     * Gets the codProceso value for this DetalleCuenta.
     * 
     * @return codProceso
     */
    public String getCodProceso() {
        return codProceso;
    }


    /**
     * Sets the codProceso value for this DetalleCuenta.
     * 
     * @param codProceso
     */
    public void setCodProceso(String codProceso) {
        this.codProceso = codProceso;
    }


    /**
     * Gets the descripcionProceso value for this DetalleCuenta.
     * 
     * @return descripcionProceso
     */
    public String getDescripcionProceso() {
        return descripcionProceso;
    }


    /**
     * Sets the descripcionProceso value for this DetalleCuenta.
     * 
     * @param descripcionProceso
     */
    public void setDescripcionProceso(String descripcionProceso) {
        this.descripcionProceso = descripcionProceso;
    }


    /**
     * Gets the codDetalleProceso value for this DetalleCuenta.
     * 
     * @return codDetalleProceso
     */
    public String getCodDetalleProceso() {
        return codDetalleProceso;
    }


    /**
     * Sets the codDetalleProceso value for this DetalleCuenta.
     * 
     * @param codDetalleProceso
     */
    public void setCodDetalleProceso(String codDetalleProceso) {
        this.codDetalleProceso = codDetalleProceso;
    }


    /**
     * Gets the descripcionDetalle value for this DetalleCuenta.
     * 
     * @return descripcionDetalle
     */
    public String getDescripcionDetalle() {
        return descripcionDetalle;
    }


    /**
     * Sets the descripcionDetalle value for this DetalleCuenta.
     * 
     * @param descripcionDetalle
     */
    public void setDescripcionDetalle(String descripcionDetalle) {
        this.descripcionDetalle = descripcionDetalle;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof DetalleCuenta)) return false;
        DetalleCuenta other = (DetalleCuenta) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.fecha==null && other.getFecha()==null) || 
             (this.fecha!=null &&
              this.fecha.equals(other.getFecha()))) &&
            ((this.hora==null && other.getHora()==null) || 
             (this.hora!=null &&
              this.hora.equals(other.getHora()))) &&
            ((this.estadoAnterior==null && other.getEstadoAnterior()==null) || 
             (this.estadoAnterior!=null &&
              this.estadoAnterior.equals(other.getEstadoAnterior()))) &&
            ((this.estadoPosterior==null && other.getEstadoPosterior()==null) || 
             (this.estadoPosterior!=null &&
              this.estadoPosterior.equals(other.getEstadoPosterior()))) &&
            ((this.usuario==null && other.getUsuario()==null) || 
             (this.usuario!=null &&
              this.usuario.equals(other.getUsuario()))) &&
            ((this.apellidos==null && other.getApellidos()==null) || 
             (this.apellidos!=null &&
              this.apellidos.equals(other.getApellidos()))) &&
            ((this.nombres==null && other.getNombres()==null) || 
             (this.nombres!=null &&
              this.nombres.equals(other.getNombres()))) &&
            ((this.nroMEMO==null && other.getNroMEMO()==null) || 
             (this.nroMEMO!=null &&
              this.nroMEMO.equals(other.getNroMEMO()))) &&
            ((this.codProceso==null && other.getCodProceso()==null) || 
             (this.codProceso!=null &&
              this.codProceso.equals(other.getCodProceso()))) &&
            ((this.descripcionProceso==null && other.getDescripcionProceso()==null) || 
             (this.descripcionProceso!=null &&
              this.descripcionProceso.equals(other.getDescripcionProceso()))) &&
            ((this.codDetalleProceso==null && other.getCodDetalleProceso()==null) || 
             (this.codDetalleProceso!=null &&
              this.codDetalleProceso.equals(other.getCodDetalleProceso()))) &&
            ((this.descripcionDetalle==null && other.getDescripcionDetalle()==null) || 
             (this.descripcionDetalle!=null &&
              this.descripcionDetalle.equals(other.getDescripcionDetalle())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFecha() != null) {
            _hashCode += getFecha().hashCode();
        }
        if (getHora() != null) {
            _hashCode += getHora().hashCode();
        }
        if (getEstadoAnterior() != null) {
            _hashCode += getEstadoAnterior().hashCode();
        }
        if (getEstadoPosterior() != null) {
            _hashCode += getEstadoPosterior().hashCode();
        }
        if (getUsuario() != null) {
            _hashCode += getUsuario().hashCode();
        }
        if (getApellidos() != null) {
            _hashCode += getApellidos().hashCode();
        }
        if (getNombres() != null) {
            _hashCode += getNombres().hashCode();
        }
        if (getNroMEMO() != null) {
            _hashCode += getNroMEMO().hashCode();
        }
        if (getCodProceso() != null) {
            _hashCode += getCodProceso().hashCode();
        }
        if (getDescripcionProceso() != null) {
            _hashCode += getDescripcionProceso().hashCode();
        }
        if (getCodDetalleProceso() != null) {
            _hashCode += getCodDetalleProceso().hashCode();
        }
        if (getDescripcionDetalle() != null) {
            _hashCode += getDescripcionDetalle().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DetalleCuenta.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "DetalleCuenta"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fecha");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fecha"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hora");
        elemField.setXmlName(new javax.xml.namespace.QName("", "hora"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estadoAnterior");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estadoAnterior"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estadoPosterior");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estadoPosterior"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuario");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usuario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("apellidos");
        elemField.setXmlName(new javax.xml.namespace.QName("", "apellidos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nombres");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nombres"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nroMEMO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nroMEMO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codProceso");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codProceso"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descripcionProceso");
        elemField.setXmlName(new javax.xml.namespace.QName("", "descripcionProceso"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codDetalleProceso");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codDetalleProceso"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descripcionDetalle");
        elemField.setXmlName(new javax.xml.namespace.QName("", "descripcionDetalle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
