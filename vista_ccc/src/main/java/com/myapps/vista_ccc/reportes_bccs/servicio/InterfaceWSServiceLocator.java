/**
 * InterfaceWSServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.myapps.vista_ccc.reportes_bccs.servicio;

public class InterfaceWSServiceLocator extends org.apache.axis.client.Service implements com.myapps.vista_ccc.reportes_bccs.servicio.InterfaceWSService {

    public InterfaceWSServiceLocator() {
    }


    public InterfaceWSServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public InterfaceWSServiceLocator(String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for InterfaceWSPort
    private String InterfaceWSPort_address = "http://172.28.10.69:8195/cxf/InterfaceWS";

    public String getInterfaceWSPortAddress() {
        return InterfaceWSPort_address;
    }

    // The WSDD service name defaults to the port name.
    private String InterfaceWSPortWSDDServiceName = "InterfaceWSPort";

    public String getInterfaceWSPortWSDDServiceName() {
        return InterfaceWSPortWSDDServiceName;
    }

    public void setInterfaceWSPortWSDDServiceName(String name) {
        InterfaceWSPortWSDDServiceName = name;
    }

    public com.myapps.vista_ccc.reportes_bccs.servicio.InterfaceWS getInterfaceWSPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(InterfaceWSPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getInterfaceWSPort(endpoint);
    }

    public com.myapps.vista_ccc.reportes_bccs.servicio.InterfaceWS getInterfaceWSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.myapps.vista_ccc.reportes_bccs.servicio.InterfaceWSServiceSoapBindingStub _stub = new com.myapps.vista_ccc.reportes_bccs.servicio.InterfaceWSServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getInterfaceWSPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setInterfaceWSPortEndpointAddress(String address) {
        InterfaceWSPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.myapps.vista_ccc.reportes_bccs.servicio.InterfaceWS.class.isAssignableFrom(serviceEndpointInterface)) {
                com.myapps.vista_ccc.reportes_bccs.servicio.InterfaceWSServiceSoapBindingStub _stub = new com.myapps.vista_ccc.reportes_bccs.servicio.InterfaceWSServiceSoapBindingStub(new java.net.URL(InterfaceWSPort_address), this);
                _stub.setPortName(getInterfaceWSPortWSDDServiceName());
                return _stub;
            }
        }
        catch (Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        String inputPortName = portName.getLocalPart();
        if ("InterfaceWSPort".equals(inputPortName)) {
            return getInterfaceWSPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "InterfaceWSService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "InterfaceWSPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(String portName, String address) throws javax.xml.rpc.ServiceException {
        
if ("InterfaceWSPort".equals(portName)) {
            setInterfaceWSPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
