package com.myapps.vista_ccc.reportes_bccs.commons;

public enum HttpMethod {
	
	GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE;

}
