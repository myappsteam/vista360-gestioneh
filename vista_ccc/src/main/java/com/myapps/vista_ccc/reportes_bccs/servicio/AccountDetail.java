/**
 * AccountDetail.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.myapps.vista_ccc.reportes_bccs.servicio;

public class AccountDetail  implements java.io.Serializable {
    private String nroCliente;

    private String secRNTC;

    private String nroCuenta;

    private String formaRegistro;

    private String usuarioRegistro;

    private String fechaIngreso;

    private String fechaActualizacion;

    private String horaActualizacion;

    private String estadoControl;

    private String observacion;

    public AccountDetail() {
    }

    public AccountDetail(
           String nroCliente,
           String secRNTC,
           String nroCuenta,
           String formaRegistro,
           String usuarioRegistro,
           String fechaIngreso,
           String fechaActualizacion,
           String horaActualizacion,
           String estadoControl,
           String observacion) {
           this.nroCliente = nroCliente;
           this.secRNTC = secRNTC;
           this.nroCuenta = nroCuenta;
           this.formaRegistro = formaRegistro;
           this.usuarioRegistro = usuarioRegistro;
           this.fechaIngreso = fechaIngreso;
           this.fechaActualizacion = fechaActualizacion;
           this.horaActualizacion = horaActualizacion;
           this.estadoControl = estadoControl;
           this.observacion = observacion;
    }


    /**
     * Gets the nroCliente value for this AccountDetail.
     * 
     * @return nroCliente
     */
    public String getNroCliente() {
        return nroCliente;
    }


    /**
     * Sets the nroCliente value for this AccountDetail.
     * 
     * @param nroCliente
     */
    public void setNroCliente(String nroCliente) {
        this.nroCliente = nroCliente;
    }


    /**
     * Gets the secRNTC value for this AccountDetail.
     * 
     * @return secRNTC
     */
    public String getSecRNTC() {
        return secRNTC;
    }


    /**
     * Sets the secRNTC value for this AccountDetail.
     * 
     * @param secRNTC
     */
    public void setSecRNTC(String secRNTC) {
        this.secRNTC = secRNTC;
    }


    /**
     * Gets the nroCuenta value for this AccountDetail.
     * 
     * @return nroCuenta
     */
    public String getNroCuenta() {
        return nroCuenta;
    }


    /**
     * Sets the nroCuenta value for this AccountDetail.
     * 
     * @param nroCuenta
     */
    public void setNroCuenta(String nroCuenta) {
        this.nroCuenta = nroCuenta;
    }


    /**
     * Gets the formaRegistro value for this AccountDetail.
     * 
     * @return formaRegistro
     */
    public String getFormaRegistro() {
        return formaRegistro;
    }


    /**
     * Sets the formaRegistro value for this AccountDetail.
     * 
     * @param formaRegistro
     */
    public void setFormaRegistro(String formaRegistro) {
        this.formaRegistro = formaRegistro;
    }


    /**
     * Gets the usuarioRegistro value for this AccountDetail.
     * 
     * @return usuarioRegistro
     */
    public String getUsuarioRegistro() {
        return usuarioRegistro;
    }


    /**
     * Sets the usuarioRegistro value for this AccountDetail.
     * 
     * @param usuarioRegistro
     */
    public void setUsuarioRegistro(String usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }


    /**
     * Gets the fechaIngreso value for this AccountDetail.
     * 
     * @return fechaIngreso
     */
    public String getFechaIngreso() {
        return fechaIngreso;
    }


    /**
     * Sets the fechaIngreso value for this AccountDetail.
     * 
     * @param fechaIngreso
     */
    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }


    /**
     * Gets the fechaActualizacion value for this AccountDetail.
     * 
     * @return fechaActualizacion
     */
    public String getFechaActualizacion() {
        return fechaActualizacion;
    }


    /**
     * Sets the fechaActualizacion value for this AccountDetail.
     * 
     * @param fechaActualizacion
     */
    public void setFechaActualizacion(String fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }


    /**
     * Gets the horaActualizacion value for this AccountDetail.
     * 
     * @return horaActualizacion
     */
    public String getHoraActualizacion() {
        return horaActualizacion;
    }


    /**
     * Sets the horaActualizacion value for this AccountDetail.
     * 
     * @param horaActualizacion
     */
    public void setHoraActualizacion(String horaActualizacion) {
        this.horaActualizacion = horaActualizacion;
    }


    /**
     * Gets the estadoControl value for this AccountDetail.
     * 
     * @return estadoControl
     */
    public String getEstadoControl() {
        return estadoControl;
    }


    /**
     * Sets the estadoControl value for this AccountDetail.
     * 
     * @param estadoControl
     */
    public void setEstadoControl(String estadoControl) {
        this.estadoControl = estadoControl;
    }


    /**
     * Gets the observacion value for this AccountDetail.
     * 
     * @return observacion
     */
    public String getObservacion() {
        return observacion;
    }


    /**
     * Sets the observacion value for this AccountDetail.
     * 
     * @param observacion
     */
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof AccountDetail)) return false;
        AccountDetail other = (AccountDetail) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.nroCliente==null && other.getNroCliente()==null) || 
             (this.nroCliente!=null &&
              this.nroCliente.equals(other.getNroCliente()))) &&
            ((this.secRNTC==null && other.getSecRNTC()==null) || 
             (this.secRNTC!=null &&
              this.secRNTC.equals(other.getSecRNTC()))) &&
            ((this.nroCuenta==null && other.getNroCuenta()==null) || 
             (this.nroCuenta!=null &&
              this.nroCuenta.equals(other.getNroCuenta()))) &&
            ((this.formaRegistro==null && other.getFormaRegistro()==null) || 
             (this.formaRegistro!=null &&
              this.formaRegistro.equals(other.getFormaRegistro()))) &&
            ((this.usuarioRegistro==null && other.getUsuarioRegistro()==null) || 
             (this.usuarioRegistro!=null &&
              this.usuarioRegistro.equals(other.getUsuarioRegistro()))) &&
            ((this.fechaIngreso==null && other.getFechaIngreso()==null) || 
             (this.fechaIngreso!=null &&
              this.fechaIngreso.equals(other.getFechaIngreso()))) &&
            ((this.fechaActualizacion==null && other.getFechaActualizacion()==null) || 
             (this.fechaActualizacion!=null &&
              this.fechaActualizacion.equals(other.getFechaActualizacion()))) &&
            ((this.horaActualizacion==null && other.getHoraActualizacion()==null) || 
             (this.horaActualizacion!=null &&
              this.horaActualizacion.equals(other.getHoraActualizacion()))) &&
            ((this.estadoControl==null && other.getEstadoControl()==null) || 
             (this.estadoControl!=null &&
              this.estadoControl.equals(other.getEstadoControl()))) &&
            ((this.observacion==null && other.getObservacion()==null) || 
             (this.observacion!=null &&
              this.observacion.equals(other.getObservacion())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNroCliente() != null) {
            _hashCode += getNroCliente().hashCode();
        }
        if (getSecRNTC() != null) {
            _hashCode += getSecRNTC().hashCode();
        }
        if (getNroCuenta() != null) {
            _hashCode += getNroCuenta().hashCode();
        }
        if (getFormaRegistro() != null) {
            _hashCode += getFormaRegistro().hashCode();
        }
        if (getUsuarioRegistro() != null) {
            _hashCode += getUsuarioRegistro().hashCode();
        }
        if (getFechaIngreso() != null) {
            _hashCode += getFechaIngreso().hashCode();
        }
        if (getFechaActualizacion() != null) {
            _hashCode += getFechaActualizacion().hashCode();
        }
        if (getHoraActualizacion() != null) {
            _hashCode += getHoraActualizacion().hashCode();
        }
        if (getEstadoControl() != null) {
            _hashCode += getEstadoControl().hashCode();
        }
        if (getObservacion() != null) {
            _hashCode += getObservacion().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AccountDetail.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "AccountDetail"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nroCliente");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nroCliente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("secRNTC");
        elemField.setXmlName(new javax.xml.namespace.QName("", "secRNTC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nroCuenta");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nroCuenta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("formaRegistro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "formaRegistro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuarioRegistro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usuarioRegistro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaIngreso");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fechaIngreso"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaActualizacion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fechaActualizacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horaActualizacion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horaActualizacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estadoControl");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estadoControl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("observacion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "observacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
