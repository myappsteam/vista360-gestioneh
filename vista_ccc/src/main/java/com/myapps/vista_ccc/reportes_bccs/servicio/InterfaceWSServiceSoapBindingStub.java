/**
 * InterfaceWSServiceSoapBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.myapps.vista_ccc.reportes_bccs.servicio;

public class InterfaceWSServiceSoapBindingStub extends org.apache.axis.client.Stub implements com.myapps.vista_ccc.reportes_bccs.servicio.InterfaceWS {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[6];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("accountQuery");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "AccountQuery"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "AccountRequest"), com.myapps.vista_ccc.reportes_bccs.servicio.AccountRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "AccountResponse"));
        oper.setReturnClass(com.myapps.vista_ccc.reportes_bccs.servicio.AccountResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "accountQueryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("changeStatusLogQuery");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ChangeStatusLogQuery"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ChangeStatusLogRequest"), com.myapps.vista_ccc.reportes_bccs.servicio.ChangeStatusLogRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ChangeStatusLogResponse"));
        oper.setReturnClass(com.myapps.vista_ccc.reportes_bccs.servicio.ChangeStatusLogResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "changeStatusLogQueryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("claimQueryByNroMEMO");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ClaimQueryByNroMEMO"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ClaimByNroMEMORequest"), com.myapps.vista_ccc.reportes_bccs.servicio.ClaimByNroMEMORequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ClaimByNroMEMOResponse"));
        oper.setReturnClass(com.myapps.vista_ccc.reportes_bccs.servicio.ClaimByNroMEMOResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "claimQueryByNroMEMOResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("claimQueryByMSISDN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ClaimQueryByMSISDN"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ClaimByMSISDNRequest"), com.myapps.vista_ccc.reportes_bccs.servicio.ClaimByMSISDNRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ClaimByMSISDNResponse"));
        oper.setReturnClass(com.myapps.vista_ccc.reportes_bccs.servicio.ClaimByMSISDNResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "claimQueryByMSISDNResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("deviceQuery");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "DeviceQuery"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "DeviceRequest"), com.myapps.vista_ccc.reportes_bccs.servicio.DeviceRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "DeviceResponse"));
        oper.setReturnClass(com.myapps.vista_ccc.reportes_bccs.servicio.DeviceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "deviceQueryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("consumerPlanQuery");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ConsumerPlanQuery"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ConsumerPlanRequest"), com.myapps.vista_ccc.reportes_bccs.servicio.ConsumerPlanRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ConsumerPlanResponse"));
        oper.setReturnClass(com.myapps.vista_ccc.reportes_bccs.servicio.ConsumerPlanResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "consumerPlanQueryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

    }

    public InterfaceWSServiceSoapBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public InterfaceWSServiceSoapBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public InterfaceWSServiceSoapBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "AccountDetail");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.AccountDetail.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "AccountRequest");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.AccountRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "AccountRequestBody");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.AccountRequestBody.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "AccountResponse");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.AccountResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "AccountResponseBody");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.Accounts[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "Accounts");
            qName2 = new javax.xml.namespace.QName("", "accounts");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "Accounts");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.Accounts.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "AdditionalParameters");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.ParameterType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ParameterType");
            qName2 = new javax.xml.namespace.QName("", "parameterType");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "CambiosPlan");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.CambiosPlan.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ChangeStatusLogRequest");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.ChangeStatusLogRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ChangeStatusLogRequestBody");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.ChangeStatusLogRequestBody.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ChangeStatusLogResponse");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.ChangeStatusLogResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ChangeStatusLogResponseBody");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.ChangeStatusLogResponseBody.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ClaimByMSISDNRequest");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.ClaimByMSISDNRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ClaimByMSISDNRequestBody");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.ClaimByMSISDNRequestBody.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ClaimByMSISDNResponse");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.ClaimByMSISDNResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ClaimByMSISDNResponseBody");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.Reclamo[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "Reclamo");
            qName2 = new javax.xml.namespace.QName("", "reclamo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ClaimByNroMEMORequest");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.ClaimByNroMEMORequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ClaimByNroMEMORequestBody");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.ClaimByNroMEMORequestBody.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ClaimByNroMEMOResponse");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.ClaimByNroMEMOResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ClaimByNroMEMOResponseBody");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.ClaimByNroMEMOResponseBody.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "Client");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.Client.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ConsumerPlanRequest");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.ConsumerPlanRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ConsumerPlanRequestBody");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.ConsumerPlanRequestBody.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ConsumerPlanResponse");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.ConsumerPlanResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ConsumerPlanResponseBody");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.ConsumerPlanResponseBody.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "DetalleCuenta");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.DetalleCuenta.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "Device");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.Device.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "DeviceRequest");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.DeviceRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "DeviceRequestBody");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.DeviceRequestBody.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "DeviceResponse");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.DeviceResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "DeviceResponseBody");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.Device[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "Device");
            qName2 = new javax.xml.namespace.QName("", "accounts");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ParameterType");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.ParameterType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "Reclamo");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.Reclamo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "RequestHeader");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.RequestHeader.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ResponseHeader");
            cachedSerQNames.add(qName);
            cls = com.myapps.vista_ccc.reportes_bccs.servicio.ResponseHeader.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                String key = (String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        Class cls = (Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            Class sf = (Class)
                                 cachedSerFactories.get(i);
                            Class df = (Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.myapps.vista_ccc.reportes_bccs.servicio.AccountResponse accountQuery(com.myapps.vista_ccc.reportes_bccs.servicio.AccountRequest accountQuery) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.queryExecute.tigo.com/InterfaceWS/accountQueryRequest");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "accountQuery"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {accountQuery});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.myapps.vista_ccc.reportes_bccs.servicio.AccountResponse) _resp;
            } catch (Exception _exception) {
                return (com.myapps.vista_ccc.reportes_bccs.servicio.AccountResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.myapps.vista_ccc.reportes_bccs.servicio.AccountResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.myapps.vista_ccc.reportes_bccs.servicio.ChangeStatusLogResponse changeStatusLogQuery(com.myapps.vista_ccc.reportes_bccs.servicio.ChangeStatusLogRequest changeStatusLogQuery) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.queryExecute.tigo.com/InterfaceWS/changeStatusLogQueryRequest");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "changeStatusLogQuery"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {changeStatusLogQuery});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.myapps.vista_ccc.reportes_bccs.servicio.ChangeStatusLogResponse) _resp;
            } catch (Exception _exception) {
                return (com.myapps.vista_ccc.reportes_bccs.servicio.ChangeStatusLogResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.myapps.vista_ccc.reportes_bccs.servicio.ChangeStatusLogResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.myapps.vista_ccc.reportes_bccs.servicio.ClaimByNroMEMOResponse claimQueryByNroMEMO(com.myapps.vista_ccc.reportes_bccs.servicio.ClaimByNroMEMORequest claimQueryByNroMEMO) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.queryExecute.tigo.com/InterfaceWS/claimQueryByNroMEMORequest");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "claimQueryByNroMEMO"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {claimQueryByNroMEMO});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.myapps.vista_ccc.reportes_bccs.servicio.ClaimByNroMEMOResponse) _resp;
            } catch (Exception _exception) {
                return (com.myapps.vista_ccc.reportes_bccs.servicio.ClaimByNroMEMOResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.myapps.vista_ccc.reportes_bccs.servicio.ClaimByNroMEMOResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.myapps.vista_ccc.reportes_bccs.servicio.ClaimByMSISDNResponse claimQueryByMSISDN(com.myapps.vista_ccc.reportes_bccs.servicio.ClaimByMSISDNRequest claimQueryByMSISDN) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.queryExecute.tigo.com/InterfaceWS/claimQueryByMSISDNRequest");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "claimQueryByMSISDN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {claimQueryByMSISDN});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.myapps.vista_ccc.reportes_bccs.servicio.ClaimByMSISDNResponse) _resp;
            } catch (Exception _exception) {
                return (com.myapps.vista_ccc.reportes_bccs.servicio.ClaimByMSISDNResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.myapps.vista_ccc.reportes_bccs.servicio.ClaimByMSISDNResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.myapps.vista_ccc.reportes_bccs.servicio.DeviceResponse deviceQuery(com.myapps.vista_ccc.reportes_bccs.servicio.DeviceRequest deviceQuery) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.queryExecute.tigo.com/InterfaceWS/deviceQueryRequest");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "deviceQuery"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {deviceQuery});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.myapps.vista_ccc.reportes_bccs.servicio.DeviceResponse) _resp;
            } catch (Exception _exception) {
                return (com.myapps.vista_ccc.reportes_bccs.servicio.DeviceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.myapps.vista_ccc.reportes_bccs.servicio.DeviceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.myapps.vista_ccc.reportes_bccs.servicio.ConsumerPlanResponse consumerPlanQuery(com.myapps.vista_ccc.reportes_bccs.servicio.ConsumerPlanRequest consumerPlanQuery) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ws.queryExecute.tigo.com/InterfaceWS/consumerPlanQueryRequest");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "consumerPlanQuery"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {consumerPlanQuery});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.myapps.vista_ccc.reportes_bccs.servicio.ConsumerPlanResponse) _resp;
            } catch (Exception _exception) {
                return (com.myapps.vista_ccc.reportes_bccs.servicio.ConsumerPlanResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.myapps.vista_ccc.reportes_bccs.servicio.ConsumerPlanResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
