/**
 * DeviceRequestBody.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.myapps.vista_ccc.reportes_bccs.servicio;

public class DeviceRequestBody  implements java.io.Serializable {
    private String imei;

    private com.myapps.vista_ccc.reportes_bccs.servicio.ParameterType[] additionalParameters;

    public DeviceRequestBody() {
    }

    public DeviceRequestBody(
           String imei,
           com.myapps.vista_ccc.reportes_bccs.servicio.ParameterType[] additionalParameters) {
           this.imei = imei;
           this.additionalParameters = additionalParameters;
    }


    /**
     * Gets the imei value for this DeviceRequestBody.
     * 
     * @return imei
     */
    public String getImei() {
        return imei;
    }


    /**
     * Sets the imei value for this DeviceRequestBody.
     * 
     * @param imei
     */
    public void setImei(String imei) {
        this.imei = imei;
    }


    /**
     * Gets the additionalParameters value for this DeviceRequestBody.
     * 
     * @return additionalParameters
     */
    public com.myapps.vista_ccc.reportes_bccs.servicio.ParameterType[] getAdditionalParameters() {
        return additionalParameters;
    }


    /**
     * Sets the additionalParameters value for this DeviceRequestBody.
     * 
     * @param additionalParameters
     */
    public void setAdditionalParameters(com.myapps.vista_ccc.reportes_bccs.servicio.ParameterType[] additionalParameters) {
        this.additionalParameters = additionalParameters;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof DeviceRequestBody)) return false;
        DeviceRequestBody other = (DeviceRequestBody) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.imei==null && other.getImei()==null) || 
             (this.imei!=null &&
              this.imei.equals(other.getImei()))) &&
            ((this.additionalParameters==null && other.getAdditionalParameters()==null) || 
             (this.additionalParameters!=null &&
              java.util.Arrays.equals(this.additionalParameters, other.getAdditionalParameters())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getImei() != null) {
            _hashCode += getImei().hashCode();
        }
        if (getAdditionalParameters() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAdditionalParameters());
                 i++) {
                Object obj = java.lang.reflect.Array.get(getAdditionalParameters(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DeviceRequestBody.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "DeviceRequestBody"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("imei");
        elemField.setXmlName(new javax.xml.namespace.QName("", "imei"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("additionalParameters");
        elemField.setXmlName(new javax.xml.namespace.QName("", "additionalParameters"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ParameterType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "parameterType"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
