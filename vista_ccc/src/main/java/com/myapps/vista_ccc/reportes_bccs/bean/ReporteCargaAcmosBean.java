package com.myapps.vista_ccc.reportes_bccs.bean;

import com.myapps.user.bean.ControlerBitacora;
import com.myapps.user.ldap.DescriptorBitacora;
import com.myapps.vista_ccc.reportes_bccs.business.ReporteCargaAcmoBL;
import com.myapps.vista_ccc.reportes_bccs.commons.ConfigRangoFecha;
import com.myapps.vista_ccc.reportes_bccs.entity.ResponseCuentaMigrada;
import com.myapps.vista_ccc.reportes_bccs.error.CuentaMigradaException;
import com.myapps.vista_ccc.reportes_bccs.error.HttpStatusException;
import com.myapps.vista_ccc.reportes_bccs.servicio.ClaimByNroMEMOResponseBody;
import com.myapps.vista_ccc.reportes_bccs.servicio.Reclamo;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.SysMessage;
import com.myapps.vista_ccc.util.UtilNumber;
import com.myapps.vista_ccc.util.UtilUrl;
import org.apache.axis.AxisFault;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.xml.rpc.ServiceException;
import java.io.Serializable;
import java.net.SocketTimeoutException;
import java.rmi.RemoteException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by michael on 11/4/2019.
 */
@ManagedBean
@ViewScoped
public class ReporteCargaAcmosBean implements Serializable {
	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(ReporteCargaAcmosBean.class);

	@Inject
	private ReporteCargaAcmoBL reporteCargaAcmoBL;
	@Inject
	private ControlerBitacora controlerBitacora;
	@Inject
	private AuditoriaResportesBCCS auditoria;
	private Date fechaInicio;
	private Date fechaFin;
	private String pattern;
	private String msisdn;
	private int rangoDiasConsultaLogs;
	private Reclamo[] cargaAcmos;
	private transient ResponseCuentaMigrada cuentaMigrada;
	List<Reclamo> lstReporteAcmo;
	private long idAuditoriaHistorial;
	private String ip;
	private Reclamo reclamoSleccionado;
	private boolean habilitadoBotonVerDetalle;
	private ClaimByNroMEMOResponseBody claimByNroMEMOResponseBody;
	private String nroMemo;
	private String estado;

	@PostConstruct
	public void init() {
		log.info(obtenerUsuario() + " ingreso a consulta carga acmos de reporte BCCS.");
		rangoDiasConsultaLogs = reporteCargaAcmoBL
				.traerCantidadDias(ConfigRangoFecha.REPORTE_HISTORICO_CARGA_ACMOS.getCodigo());
		iniciar(rangoDiasConsultaLogs);
		cargarIp();
		setReclamoSleccionado(null);
		habilitadoBotonVerDetalle = true;
		estado = Parametros.estadoCuenta;
	}

	private void cargarIp() {
		try {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
					.getRequest();
			ip = UtilUrl.getClientIp(request);
		} catch (Exception e) {
			log.error("Error al obtener ip: ", e);
		}
	}

	private String obtenerUsuario() {
		String login = "";
		try {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
					.getRequest();
			login = (String) request.getSession().getAttribute("TEMP$USER_NAME");
			// log.info("login: " + login);
		} catch (Exception e) {
			log.error("Error al obtener usuario: ", e);
		}
		return login;
	}

	public void iniciar(int rangoDias) {
		try {
			pattern = Parametros.formatoFechaReporteBccs;
			LocalDateTime fechaHoy = LocalDateTime.now();
			fechaFin = Date.from(fechaHoy.toInstant(ZoneOffset.UTC));
			fechaHoy = fechaHoy.minusDays(rangoDias);
			fechaInicio = Date.from(fechaHoy.toInstant(ZoneOffset.UTC));
		} catch (Exception e) {
			log.error("Error al iniciar fechas para consulta carga acmos, reporte BCCS: ", e);
		}
	}

	public boolean validarIsdn() {
		boolean validar = false;
		if (msisdn != null && !msisdn.trim().isEmpty() && UtilNumber.esNroTigo(msisdn))
			validar = true;
		else
			SysMessage.warn("Número de teléfono no válido", null);
		return validar;
	}

	public void cargarReporte() {
		if (validarIsdn() && reporteCargaAcmoBL.validarFecha(fechaInicio, fechaFin, rangoDiasConsultaLogs)) {
			try {
				idAuditoriaHistorial = auditoria.guardarAuditoria(Calendar.getInstance(), obtenerUsuario(), ip,
						Parametros.vistaReporteBCCS, Parametros.comandoReportesBCCS, msisdn, null);
				cargaAcmos = reporteCargaAcmoBL.obtenerCargaAcmo(msisdn, fechaInicio, fechaFin, idAuditoriaHistorial);
				//metodo para ordenar reporte por nro de reclamo
				if(cargaAcmos != null){
					lstReporteAcmo= Arrays.asList(cargaAcmos);
					/*for (Reclamo hh:cargaAcmos){
						hh.setNroReclamoOrder(Long.parseLong(hh.getNroReclamo()));
					}*/
				}
				log.debug("Reporte consulta carga acmos para Nro Cuenta: " + msisdn);
				SysMessage.info("Consulta exitosa.", null);
			} catch (Exception e) {
				tratarMostrarError(e);
			}
		}

		try {
			controlerBitacora.accion(DescriptorBitacora.CARGA_ACMOS, "Se hizo consulta del Nro Cuenta: " + this.msisdn);
		} catch (Exception e) {
			log.error("Error al guardar bitacora en el sistema: ", e);
			SysMessage.error("Error al guardar bitacora", null);
		}
	}

	public void tratarMostrarError(Exception e) {
		if (e.getCause() instanceof AxisFault) {
			String msg = e.getMessage();
			msg = msg.toLowerCase();
			if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase())
					|| msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase())
					|| msg.contains("UnknownHostException".toLowerCase())
					|| msg.contains("No route to host".toLowerCase())
					|| msg.contains("Connection refused".toLowerCase())) {
				log.error("Error de conexion al servicio Reporte BCCS claimQuery: ", e);
				SysMessage.error("Error de conexion del servicio Reporte BCCS claimQuery", null);
			} else {
				log.error("Error AxisFault accountQuery: ", e);
				SysMessage.error("Error AxisFault de conexion al servicio Reporte BCCS claimQuery", null);
			}
		} else if (e.getCause() instanceof SocketTimeoutException) {
			log.error("Error de timeout al conectarse con el servicio Reporte BCCS claimQuery: ", e);
			SysMessage.error("Tiempo de espera para el servicio Reporte BCCS claimQuery agotado.", null);
		} else if (e.getCause() instanceof ServiceException) {
			log.error("Error de servicio al conectarse al servicio Reporte BCCS claimQuery: ", e);
			SysMessage.error("No se pudo conectar al servicio Reporte BCCS claimQuery", null);
		} else if (e.getCause() instanceof RemoteException) {
			log.error("Error remoto al conectarse al servicio Reporte BCCS claimQuery: ", e);
			SysMessage.error("Error remoto al conectarse al servicio Reporte BCCS claimQuery", null);
		} else {
			SysMessage.error("Error interno de sistema", null);
			log.error("Error al obtener reporte: ", e);
		}
	}

	public void limpiar() {
		iniciar(rangoDiasConsultaLogs);
		msisdn = "";
		cargaAcmos = null;
	}

	public void onRowSelectRadio() {
		habilitadoBotonVerDetalle = false;
		log.info(habilitadoBotonVerDetalle);
		/*cargaAcmos = new ArrayList<>();
		if (reclamoSleccionado != null) {
			log.debug(reclamoSleccionado);
			// if (deudaSeleccionada.getListaDetalle() != null) {
			// listaDetalleDeuda = deudaSeleccionada.getListaDetalle();
			// }
		}*/
	}

	public void verDetalle() {
		log.debug(reclamoSleccionado);

		claimByNroMEMOResponseBody = new ClaimByNroMEMOResponseBody();

		if (reclamoSleccionado != null)
			try {
				nroMemo = reclamoSleccionado.getNroMemo();
				idAuditoriaHistorial = auditoria.guardarAuditoria(Calendar.getInstance(), obtenerUsuario(), ip,
						Parametros.vistaReporteBCCS, Parametros.comandoReportesBCCS, msisdn, null);
				claimByNroMEMOResponseBody = reporteCargaAcmoBL.obtenerCargaAcmoByMemo(nroMemo,idAuditoriaHistorial);
				//metodo para ordenar reporte por nro de reclamo
				if(claimByNroMEMOResponseBody != null){
					lstReporteAcmo= Arrays.asList(cargaAcmos);
					/*for (Reclamo hh:cargaAcmos){
						hh.setNroReclamoOrder(Long.parseLong(hh.getNroReclamo()));
					}*/
				}
				log.debug("Reporte consulta carga acmos para Nro Memo: " + nroMemo);
				SysMessage.info("Consulta exitosa.", null);
			} catch (Exception e) {
				tratarMostrarError(e);
			}
	}

	public void consumirCuentaMigrada(){

		cuentaMigrada = null;
		try {
			idAuditoriaHistorial = auditoria.guardarAuditoria(Calendar.getInstance(), obtenerUsuario(), ip,
					Parametros.vistaReporteBCCS, Parametros.comandoCuentaMigrada, msisdn, null);
			cuentaMigrada = reporteCargaAcmoBL.obtenerCuentaMigrada(msisdn, estado, obtenerUsuario(), idAuditoriaHistorial);
			SysMessage.info("Consulta exitosa.", null);
		} catch (CuentaMigradaException e) {
			log.info("Respuesta de servicio codigo: " + e.getResponseInfo().getCodigo() + ", mensaje: "
					+ e.getResponseInfo().getMensaje());
			SysMessage.info(e.getResponseInfo().getCodigo() + " : " + e.getResponseInfo().getMensaje(), null);
		} catch (HttpStatusException e) {
			log.error("Excepcion al consumir el servicio de CuentaMigradas " + e.getMessage(), e);
			SysMessage.warn("No se pudo completar la consulta.", null);
		} catch (Exception e) {
			log.error("Error interno de aplicacion " + e.getMessage(), e);
			SysMessage.error("Problema de aplicacion, no se pudo completar la consulta.", null);
		}


		try {
			controlerBitacora.accion(DescriptorBitacora.CONSULTA_CUENTAMIGRADAS,
					"Se realizó la busqueda de resporte CONSULTA_CUENTAMIGRADAS enviados de linea: " + msisdn);
		} catch (Exception e) {
			log.error("Error al guardar bitacora en el sistema: ", e);
			SysMessage.error("Error al guardar bitacora", null);
		}
	}
	public void cargarReporteCuentaMigrada() {
		if (validarIsdn() && reporteCargaAcmoBL.validarFecha(fechaInicio, fechaFin, rangoDiasConsultaLogs)) {
			consumirCuentaMigrada();
			if (cuentaMigrada.getCuentaMigradaResponseOk() != null && cuentaMigrada.getCuentaMigradaResponseOk().getEstado() != null) {
				//log.info("fecha migracion: "+cuentaMigrada.getFechaMigracion());
				cargarReporte();
			} else if (cuentaMigrada.getResponseError().getErrors() != null) {
				cargaAcmos = null;
				cuentaMigrada = null;
				SysMessage.warn("La cuenta no ha sido migrada", null);
                /*for (Error valor : cuentaMigrada.getResponseError().getErrors()) {
                    SysMessage.warn(valor.getDetail(), null);
                }*/
			}
		}

		try {
			controlerBitacora.accion(DescriptorBitacora.CONSULTA_CUENTAS,
					"Se hizo consulta del Nro Cuenta: " + this.msisdn);
		} catch (Exception e) {
			log.error("Error al guardar bitacora en el sistema: ", e);
			SysMessage.error("Error al guardar bitacora", null);
		}
	}

	public ResponseCuentaMigrada getCuentaMigrada() {
		return cuentaMigrada;
	}

	public void setCuentaMigrada(ResponseCuentaMigrada cuentaMigrada) {
		this.cuentaMigrada = cuentaMigrada;
	}

	public String getNroMemo() {
		return nroMemo;
	}

	public void setNroMemo(String nroMemo) {
		this.nroMemo = nroMemo;
	}

	public Reclamo getReclamoSleccionado() {
		return reclamoSleccionado;
	}

	public void setReclamoSleccionado(Reclamo reclamoSleccionado) {
		this.reclamoSleccionado = reclamoSleccionado;
	}

	public List<Reclamo> getLstReporteAcmo() {
		return lstReporteAcmo;
	}

	public boolean isHabilitadoBotonVerDetalle() {
		return habilitadoBotonVerDetalle;
	}

	public void setHabilitadoBotonVerDetalle(boolean habilitadoBotonVerDetalle) {
		this.habilitadoBotonVerDetalle = habilitadoBotonVerDetalle;
	}

	public ClaimByNroMEMOResponseBody getClaimByNroMEMOResponseBody() {
		return claimByNroMEMOResponseBody;
	}

	public void setClaimByNroMEMOResponseBody(ClaimByNroMEMOResponseBody claimByNroMEMOResponseBody) {
		this.claimByNroMEMOResponseBody = claimByNroMEMOResponseBody;
	}

	public void setLstReporteAcmo(List<Reclamo> lstReporteAcmo) {
		this.lstReporteAcmo = lstReporteAcmo;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public Reclamo[] getCargaAcmos() {
		return cargaAcmos;
	}

	public void setCargaAcmos(Reclamo[] cargaAcmos) {
		this.cargaAcmos = cargaAcmos;
	}

}
