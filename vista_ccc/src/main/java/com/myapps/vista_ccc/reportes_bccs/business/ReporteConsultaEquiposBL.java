package com.myapps.vista_ccc.reportes_bccs.business;

import com.myapps.vista_ccc.reportes_bccs.dao.ConfigReporteDao;
import com.myapps.vista_ccc.reportes_bccs.entity.ReporteFechaLimite;
import com.myapps.vista_ccc.reportes_bccs.servicio.*;
import com.myapps.vista_ccc.reportes_bccs.servicio.consumer.ResporteBCCSServiceConsumer;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Created by michael on 12/4/2019.
 */
@Named
public class ReporteConsultaEquiposBL implements Serializable {
	@Inject
	private ConfigReporteDao configReporteDao;
	@Inject
	private ResporteBCCSServiceConsumer consumer;

	public Device[] obtenerReporteCuentaEquipos(String imei, long idAuditoriaHistorial) throws Exception {
		Device[] cs = null;
		DeviceResponse response = consumer.consumirDeviceResponse(construirRequst(imei), idAuditoriaHistorial);
		if (response !=null && response.getAccountResponseBody() != null)
			cs = response.getAccountResponseBody();
		return cs;
	}

	public DeviceRequest construirRequst(String imei) {
		DeviceRequest request = new DeviceRequest();
		DeviceRequestBody body = new DeviceRequestBody();
		body.setImei(imei);
		request.setRequestBody(body);
		return request;
	}

	public Device[] obtenerReporteEquipos(String imei) {
		Device[] e = new Device[2];
		Device d1 = new Device();
		Client c = llenarDatosClient();
		d1.setClient(c);
		d1.setEstadoControl("ParametroDao");
		d1.setNroRegistro("ParametroDao");
		d1.setIMEI("PRUEBA");
		d1.setRETM("RETM");
		d1.setFormaRegistro("FORMA");
		d1.setNroCuentaRegistro("Cuenta");
		d1.setUsuarioRegistro("Usuarios");
		d1.setFechaIngreso("Fecha");
		d1.setFechaActualizacion("fecha");
		d1.setHoraActualizacion("HORA");
		d1.setLista("Lista");
		d1.setEstadoControl("estado");
		d1.setObservacion("observaciones");
		e[0] = d1;
		return e;
	}

	public Client llenarDatosClient() {
		Client cuentaClient = new Client();
		cuentaClient.setNombres("Roger");
		cuentaClient.setApellidos("Cruz");
		return cuentaClient;
	}

	public int traerCantidadDias(int idReporte) {
		ReporteFechaLimite limiteDias;
		limiteDias = configReporteDao.findByCodigoAndId(Long.valueOf(idReporte));
		return limiteDias.getDiasLimite();
	}
}
