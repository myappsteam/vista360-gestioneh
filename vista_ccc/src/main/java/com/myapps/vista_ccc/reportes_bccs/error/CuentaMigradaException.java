package com.myapps.vista_ccc.reportes_bccs.error;


import com.myapps.vista_ccc.reportes_bccs.entity.CuentaMigradaResponseInfo;

public class CuentaMigradaException extends Exception {

	private static final long serialVersionUID = 1L;
	private final transient CuentaMigradaResponseInfo responseInfo;

	public CuentaMigradaException(CuentaMigradaResponseInfo responseInfo) {
		this.responseInfo = responseInfo;
	}

	public CuentaMigradaResponseInfo getResponseInfo() {
		return responseInfo;
	}

}
