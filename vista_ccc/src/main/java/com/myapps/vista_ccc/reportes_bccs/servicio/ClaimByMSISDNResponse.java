/**
 * ClaimByMSISDNResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.myapps.vista_ccc.reportes_bccs.servicio;

public class ClaimByMSISDNResponse  implements java.io.Serializable {
    private com.myapps.vista_ccc.reportes_bccs.servicio.ResponseHeader responseHeader;

    private com.myapps.vista_ccc.reportes_bccs.servicio.Reclamo[] claimByMSISDNResponseBody;

    public ClaimByMSISDNResponse() {
    }

    public ClaimByMSISDNResponse(
           com.myapps.vista_ccc.reportes_bccs.servicio.ResponseHeader responseHeader,
           com.myapps.vista_ccc.reportes_bccs.servicio.Reclamo[] claimByMSISDNResponseBody) {
           this.responseHeader = responseHeader;
           this.claimByMSISDNResponseBody = claimByMSISDNResponseBody;
    }


    /**
     * Gets the responseHeader value for this ClaimByMSISDNResponse.
     * 
     * @return responseHeader
     */
    public com.myapps.vista_ccc.reportes_bccs.servicio.ResponseHeader getResponseHeader() {
        return responseHeader;
    }


    /**
     * Sets the responseHeader value for this ClaimByMSISDNResponse.
     * 
     * @param responseHeader
     */
    public void setResponseHeader(com.myapps.vista_ccc.reportes_bccs.servicio.ResponseHeader responseHeader) {
        this.responseHeader = responseHeader;
    }


    /**
     * Gets the claimByMSISDNResponseBody value for this ClaimByMSISDNResponse.
     * 
     * @return claimByMSISDNResponseBody
     */
    public com.myapps.vista_ccc.reportes_bccs.servicio.Reclamo[] getClaimByMSISDNResponseBody() {
        return claimByMSISDNResponseBody;
    }


    /**
     * Sets the claimByMSISDNResponseBody value for this ClaimByMSISDNResponse.
     * 
     * @param claimByMSISDNResponseBody
     */
    public void setClaimByMSISDNResponseBody(com.myapps.vista_ccc.reportes_bccs.servicio.Reclamo[] claimByMSISDNResponseBody) {
        this.claimByMSISDNResponseBody = claimByMSISDNResponseBody;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof ClaimByMSISDNResponse)) return false;
        ClaimByMSISDNResponse other = (ClaimByMSISDNResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.responseHeader==null && other.getResponseHeader()==null) || 
             (this.responseHeader!=null &&
              this.responseHeader.equals(other.getResponseHeader()))) &&
            ((this.claimByMSISDNResponseBody==null && other.getClaimByMSISDNResponseBody()==null) || 
             (this.claimByMSISDNResponseBody!=null &&
              java.util.Arrays.equals(this.claimByMSISDNResponseBody, other.getClaimByMSISDNResponseBody())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getResponseHeader() != null) {
            _hashCode += getResponseHeader().hashCode();
        }
        if (getClaimByMSISDNResponseBody() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getClaimByMSISDNResponseBody());
                 i++) {
                Object obj = java.lang.reflect.Array.get(getClaimByMSISDNResponseBody(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClaimByMSISDNResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ClaimByMSISDNResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responseHeader");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ResponseHeader"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ResponseHeader"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("claimByMSISDNResponseBody");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ClaimByMSISDNResponseBody"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "Reclamo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "reclamo"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
