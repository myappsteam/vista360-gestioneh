package com.myapps.vista_ccc.reportes_bccs.servicio.consumer;

import com.myapps.vista_ccc.reportes_bccs.servicio.InterfaceWS;
import com.myapps.vista_ccc.reportes_bccs.servicio.InterfaceWSServiceLocator;
import com.myapps.vista_ccc.reportes_bccs.servicio.InterfaceWSServiceSoapBindingStub;
import com.myapps.vista_ccc.servicio.NodoServicio;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.UtilUrl;
import org.apache.axis.message.SOAPHeaderElement;
import org.apache.log4j.Logger;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class ServicioReporteBCCS implements Serializable {

    /**
     *
     */

    private static final long serialVersionUID = 1L;

    public static Logger log = Logger.getLogger(ServicioReporteBCCS.class);

    public static Map<String, NodoServicio<InterfaceWSServiceSoapBindingStub>> ports = new HashMap<String, NodoServicio<InterfaceWSServiceSoapBindingStub>>();

    public static synchronized NodoServicio<InterfaceWSServiceSoapBindingStub> initPort(String url, String username,
                                                                                        int timeout, String user, String pass) throws Exception {
        log.debug("[url service reporte BCCS QueryExecute: " + url + "]");
        long ini = System.currentTimeMillis();
        url = UtilUrl.getIp(url);
        long fin = System.currentTimeMillis();
        log.debug("[username: " + username + ", url: " + url + "] Tiempo de respuesta para obtener ip: " + (fin - ini)
                + " milisegundos");

        ini = System.currentTimeMillis();

        NodoServicio<InterfaceWSServiceSoapBindingStub> port = ports.get(username);

        if (port == null) {
            log.debug("Creando un nuevo port ... " + username);
            port = getService(url, username, timeout, user, pass);
            //port = getService("https://jfuseappsdev.tigo.net.bo/cxf/InterfaceWS", username, timeout, user, pass);
            port.setPrimeraVez(true);
        } else {
            log.debug("Ya existe el port creado ... " + username);
            port.setEnUso(true);
            port.setFechaInicio(new Date());
            port.setFechaFin(null);
            port.setPrimeraVez(false);
        }
        fin = System.currentTimeMillis();

        port.setTiempoConexion(fin - ini);

        log.debug("[username: " + username + "] Tiempo de respuesta para obtener conexion: " + (fin - ini)
                + " milisegundos");

        return port;
    }

    public static synchronized NodoServicio<InterfaceWSServiceSoapBindingStub> getService(String url, String username,
                                                                                          int timeout, String user, String pass) throws Exception {

        NodoServicio<InterfaceWSServiceSoapBindingStub> port = ports.get(username);

        if (port == null) {

            InterfaceWSServiceLocator service = new InterfaceWSServiceLocator();
            service.setInterfaceWSPortEndpointAddress(url);
            log.debug("url reporte BCCS QueryExecute: " + service.getInterfaceWSPortAddress());

            InterfaceWSServiceSoapBindingStub portStub = (InterfaceWSServiceSoapBindingStub) service.getPort(InterfaceWS.class);
            portStub.setTimeout(timeout * 1000);
            if (user != null && pass != null && !user.equals("") && !pass.equals(""))
                addWsSecurityHeaderV2(portStub, user, pass);
            port = new NodoServicio<InterfaceWSServiceSoapBindingStub>();
            port.setPort(portStub);
            port.setKey(username);

            port.setEnUso(true);
            port.setFechaInicio(new Date());
            port.setFechaFin(null);

            ports.put(username, port);

        }

        return port;
    }

    public static synchronized void liberar(String key) {
        if (ports != null) {
            synchronized (ports) {
                ArrayList<String> listaEliminar = new ArrayList<>();

                for (Entry<String, NodoServicio<InterfaceWSServiceSoapBindingStub>> entry : ports.entrySet()) {
                    if (entry.getKey().endsWith(key)) {
                        listaEliminar.add(entry.getKey());
                    }
                }

                int sizeAntes = ports.size();
                for (String item : listaEliminar) {
                    ports.remove(item);
                }
                int sizeDespues = ports.size();

                log.debug("Eliminados del hashmap reporte BCCS QueryExecute [key: " + key + "][size antes: " + sizeAntes
                        + ", size despues: " + sizeDespues + "]: " + listaEliminar.size() + " conexiones : "
                        + listaEliminar);
            }
        }
    }

    public static synchronized void liberarPorTimeOut() {
        if (ports != null) {
            synchronized (ports) {
                ArrayList<String> listaEliminar = new ArrayList<>();

                for (Entry<String, NodoServicio<InterfaceWSServiceSoapBindingStub>> entry : ports.entrySet()) {

                    if (!entry.getValue().isEnUso() && entry.getValue().getFechaFin() != null) {
                        long milisegundos = (new Date()).getTime() - entry.getValue().getFechaFin().getTime();
                        long segundos = milisegundos / 1000;
                        long minutos = segundos / 60;

                        if (minutos >= Parametros.timpoFueraPoolConexion) {
                            listaEliminar.add(entry.getKey());
                        }
                    }
                }

                int sizeAntes = ports.size();
                for (String item : listaEliminar) {
                    ports.remove(item);
                }
                int sizeDespues = ports.size();

                log.debug(
                        "Eliminados por time out de la cola de conexiones activas reporte BCCS QueryExecute [size antes: "
                                + sizeAntes + ", size despues: " + sizeDespues + "]: " + listaEliminar.size()
                                + " conexiones : " + listaEliminar);
            }
        }
    }

    synchronized public static void addWsSecurityHeader(org.apache.axis.client.Stub binding, String wsUser,
                                                        String wsPass) throws SOAPException {

        QName headerName = new QName(
                "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "Security");
        SOAPHeaderElement header = new SOAPHeaderElement(headerName);
        header.setActor(null);
        header.setPrefix("sp");
        header.setMustUnderstand(true);

        SOAPElement utElem = header.addChildElement("UsernameToken");
        SOAPElement userNameElem = utElem.addChildElement("Username");
        userNameElem.removeContents();
        userNameElem.setValue(wsUser);

        SOAPElement passwordElem = utElem.addChildElement("Password");
        passwordElem.setValue(wsPass);
        binding.setHeader(header);
    }

    synchronized public static void addWsSecurityHeaderV2(org.apache.axis.client.Stub binding, String userName, String passwordUser) throws SOAPException, UnsupportedEncodingException {

        QName headerName = new QName("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "Security", "wsse");
        org.apache.axis.message.SOAPHeaderElement header = new org.apache.axis.message.SOAPHeaderElement(headerName);
        header.setActor(null);
        // not important, "wsse" is standard
        header.setPrefix("wsse");
        header.setMustUnderstand(true);

        // Add the UsernameToken element to the WS-Security header
        SOAPElement usernameToken = header.addChildElement("UsernameToken");
        usernameToken.setAttribute("xmlns:wsu", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
        SOAPElement username = usernameToken.addChildElement("Username");
        username.setValue(userName);
        SOAPElement password = usernameToken.addChildElement("Password");
        password.setValue(passwordUser);
        password.setAttribute("Type", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");

        //SOAPElement created = usernameToken.addChildElement("Created");
        //created.setValue(getCreated());

        SOAPElement nonce = usernameToken.addChildElement("Nonce");
        nonce.setPrefix("wsse");
        nonce.setAttribute("EncodingType", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary");
        //nonce.setValue(getNonce());

        // Finally, attach the header to the binding.
        binding.setHeader(header);

        try {
            // log.debug(header.getAsString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
