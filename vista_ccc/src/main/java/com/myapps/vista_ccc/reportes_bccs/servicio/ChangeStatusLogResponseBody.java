/**
 * ChangeStatusLogResponseBody.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.myapps.vista_ccc.reportes_bccs.servicio;

public class ChangeStatusLogResponseBody  implements java.io.Serializable {
    private String tipoCUenta;

    private String descripcion;

    private String nroCuenta;

    private com.myapps.vista_ccc.reportes_bccs.servicio.DetalleCuenta[] detalleCuentas;

    public ChangeStatusLogResponseBody() {
    }

    public ChangeStatusLogResponseBody(
           String tipoCUenta,
           String descripcion,
           String nroCuenta,
           com.myapps.vista_ccc.reportes_bccs.servicio.DetalleCuenta[] detalleCuentas) {
           this.tipoCUenta = tipoCUenta;
           this.descripcion = descripcion;
           this.nroCuenta = nroCuenta;
           this.detalleCuentas = detalleCuentas;
    }


    /**
     * Gets the tipoCUenta value for this ChangeStatusLogResponseBody.
     * 
     * @return tipoCUenta
     */
    public String getTipoCUenta() {
        return tipoCUenta;
    }


    /**
     * Sets the tipoCUenta value for this ChangeStatusLogResponseBody.
     * 
     * @param tipoCUenta
     */
    public void setTipoCUenta(String tipoCUenta) {
        this.tipoCUenta = tipoCUenta;
    }


    /**
     * Gets the descripcion value for this ChangeStatusLogResponseBody.
     * 
     * @return descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }


    /**
     * Sets the descripcion value for this ChangeStatusLogResponseBody.
     * 
     * @param descripcion
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }


    /**
     * Gets the nroCuenta value for this ChangeStatusLogResponseBody.
     * 
     * @return nroCuenta
     */
    public String getNroCuenta() {
        return nroCuenta;
    }


    /**
     * Sets the nroCuenta value for this ChangeStatusLogResponseBody.
     * 
     * @param nroCuenta
     */
    public void setNroCuenta(String nroCuenta) {
        this.nroCuenta = nroCuenta;
    }


    /**
     * Gets the detalleCuentas value for this ChangeStatusLogResponseBody.
     * 
     * @return detalleCuentas
     */
    public com.myapps.vista_ccc.reportes_bccs.servicio.DetalleCuenta[] getDetalleCuentas() {
        return detalleCuentas;
    }


    /**
     * Sets the detalleCuentas value for this ChangeStatusLogResponseBody.
     * 
     * @param detalleCuentas
     */
    public void setDetalleCuentas(com.myapps.vista_ccc.reportes_bccs.servicio.DetalleCuenta[] detalleCuentas) {
        this.detalleCuentas = detalleCuentas;
    }

    public com.myapps.vista_ccc.reportes_bccs.servicio.DetalleCuenta getDetalleCuentas(int i) {
        return this.detalleCuentas[i];
    }

    public void setDetalleCuentas(int i, com.myapps.vista_ccc.reportes_bccs.servicio.DetalleCuenta _value) {
        this.detalleCuentas[i] = _value;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof ChangeStatusLogResponseBody)) return false;
        ChangeStatusLogResponseBody other = (ChangeStatusLogResponseBody) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.tipoCUenta==null && other.getTipoCUenta()==null) || 
             (this.tipoCUenta!=null &&
              this.tipoCUenta.equals(other.getTipoCUenta()))) &&
            ((this.descripcion==null && other.getDescripcion()==null) || 
             (this.descripcion!=null &&
              this.descripcion.equals(other.getDescripcion()))) &&
            ((this.nroCuenta==null && other.getNroCuenta()==null) || 
             (this.nroCuenta!=null &&
              this.nroCuenta.equals(other.getNroCuenta()))) &&
            ((this.detalleCuentas==null && other.getDetalleCuentas()==null) || 
             (this.detalleCuentas!=null &&
              java.util.Arrays.equals(this.detalleCuentas, other.getDetalleCuentas())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTipoCUenta() != null) {
            _hashCode += getTipoCUenta().hashCode();
        }
        if (getDescripcion() != null) {
            _hashCode += getDescripcion().hashCode();
        }
        if (getNroCuenta() != null) {
            _hashCode += getNroCuenta().hashCode();
        }
        if (getDetalleCuentas() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDetalleCuentas());
                 i++) {
                Object obj = java.lang.reflect.Array.get(getDetalleCuentas(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ChangeStatusLogResponseBody.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ChangeStatusLogResponseBody"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoCUenta");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipoCUenta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descripcion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "descripcion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nroCuenta");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nroCuenta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("detalleCuentas");
        elemField.setXmlName(new javax.xml.namespace.QName("", "detalleCuentas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "DetalleCuenta"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
