package com.myapps.vista_ccc.reportes_bccs.entity;

import java.io.Serializable;

/**
 * Created by michael on 17/12/2019.
 */
public class ResponseCuentaMigrada implements Serializable{
    private static final long serialVersionUID = 1L;

    private CuentaMigradaResponseOk cuentaMigradaResponseOk;
    private ResponseError responseError;

    public CuentaMigradaResponseOk getCuentaMigradaResponseOk() {
        return cuentaMigradaResponseOk;
    }

    public void setCuentaMigradaResponseOk(CuentaMigradaResponseOk cuentaMigradaResponseOk) {
        this.cuentaMigradaResponseOk = cuentaMigradaResponseOk;
    }

    public ResponseError getResponseError() {
        return responseError;
    }

    public void setResponseError(ResponseError responseError) {
        this.responseError = responseError;
    }
}
