package com.myapps.vista_ccc.reportes_bccs.commons;

public enum ResponseWhiteList {

	RESPONSE_OK_200(200, "ok", "Consulta finalizada correctamente."),
	RESPONSE_OK_200_ADD(200, "Successfully added MSISDN to package expiry white-list",
			"Número de télefono añadido correctamente a lista negra."),
	RESPONSE_OK_200_DELETE(200, "Successfully removed MSISDN from package expiry white-list",
			"Número de télefono eliminado con éxito de lista negra."),
	RESPONSE_ERROR_409_ADD_YES(409, "MSISDN already exists in package expiry white-list",
			"El número de télefono ya existe en lista negra."),
	RESPONSE_ERROR_409_DELETE_YES(409, "MSISDN already unsubscribed in package expiry white-list",
			"El número de télefono ya se dio de baja en lista negra."),
	RESPONSE_ERROR_409(409, "error", "Conflicto al realizar la acción."),
	RESPONSE_ERROR_404(404, "MSISDN was not found", "El número de télefono no fue encontrado."),
	RESPONSE_ERROR_500(500, "error", "Error interno de servidor.");

	private int codigo;
	private String mensajeResponse;
	private String mensajeVista;

	ResponseWhiteList(int codigo, String mensajeResponse, String mensajeVista) {
		this.codigo = codigo;
		this.mensajeResponse = mensajeResponse;
		this.mensajeVista = mensajeVista;
	}

	public int getCodigo() {
		return codigo;
	}

	public String getMensajeResponse() {
		return mensajeResponse;
	}

	public String getMensajeVista() {
		return mensajeVista;
	}

}
