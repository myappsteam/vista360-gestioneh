/**
 * ChangeStatusLogResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.myapps.vista_ccc.reportes_bccs.servicio;

public class ChangeStatusLogResponse  implements java.io.Serializable {
    private com.myapps.vista_ccc.reportes_bccs.servicio.ResponseHeader responseHeader;

    private com.myapps.vista_ccc.reportes_bccs.servicio.ChangeStatusLogResponseBody accountResponseBody;

    public ChangeStatusLogResponse() {
    }

    public ChangeStatusLogResponse(
           com.myapps.vista_ccc.reportes_bccs.servicio.ResponseHeader responseHeader,
           com.myapps.vista_ccc.reportes_bccs.servicio.ChangeStatusLogResponseBody accountResponseBody) {
           this.responseHeader = responseHeader;
           this.accountResponseBody = accountResponseBody;
    }


    /**
     * Gets the responseHeader value for this ChangeStatusLogResponse.
     * 
     * @return responseHeader
     */
    public com.myapps.vista_ccc.reportes_bccs.servicio.ResponseHeader getResponseHeader() {
        return responseHeader;
    }


    /**
     * Sets the responseHeader value for this ChangeStatusLogResponse.
     * 
     * @param responseHeader
     */
    public void setResponseHeader(com.myapps.vista_ccc.reportes_bccs.servicio.ResponseHeader responseHeader) {
        this.responseHeader = responseHeader;
    }


    /**
     * Gets the accountResponseBody value for this ChangeStatusLogResponse.
     * 
     * @return accountResponseBody
     */
    public com.myapps.vista_ccc.reportes_bccs.servicio.ChangeStatusLogResponseBody getAccountResponseBody() {
        return accountResponseBody;
    }


    /**
     * Sets the accountResponseBody value for this ChangeStatusLogResponse.
     * 
     * @param accountResponseBody
     */
    public void setAccountResponseBody(com.myapps.vista_ccc.reportes_bccs.servicio.ChangeStatusLogResponseBody accountResponseBody) {
        this.accountResponseBody = accountResponseBody;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof ChangeStatusLogResponse)) return false;
        ChangeStatusLogResponse other = (ChangeStatusLogResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.responseHeader==null && other.getResponseHeader()==null) || 
             (this.responseHeader!=null &&
              this.responseHeader.equals(other.getResponseHeader()))) &&
            ((this.accountResponseBody==null && other.getAccountResponseBody()==null) || 
             (this.accountResponseBody!=null &&
              this.accountResponseBody.equals(other.getAccountResponseBody())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getResponseHeader() != null) {
            _hashCode += getResponseHeader().hashCode();
        }
        if (getAccountResponseBody() != null) {
            _hashCode += getAccountResponseBody().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ChangeStatusLogResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ChangeStatusLogResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responseHeader");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ResponseHeader"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ResponseHeader"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountResponseBody");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AccountResponseBody"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ChangeStatusLogResponseBody"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
