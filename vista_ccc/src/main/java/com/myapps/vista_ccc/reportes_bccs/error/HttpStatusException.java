package com.myapps.vista_ccc.reportes_bccs.error;

//import com.myapps.vista_ccc.white_list.commons.StatusResponse;

import com.myapps.vista_ccc.reportes_bccs.commons.StatusResponse;

public class HttpStatusException extends Exception {

	private static final long serialVersionUID = 1L;
	private StatusResponse statusResponse;

	public HttpStatusException(String message, StatusResponse statusResponse) {
		super(message);
		this.statusResponse = statusResponse;
	}

	public StatusResponse getStatusResponse() {
		return statusResponse;
	}

	public void setStatusResponse(StatusResponse statusResponse) {
		this.statusResponse = statusResponse;
	}

}
