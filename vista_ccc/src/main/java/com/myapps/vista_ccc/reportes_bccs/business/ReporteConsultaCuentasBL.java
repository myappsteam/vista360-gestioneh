package com.myapps.vista_ccc.reportes_bccs.business;

import com.google.gson.JsonSyntaxException;
import com.myapps.vista_ccc.reportes_bccs.commons.CuentasMigradasMethods;
import com.myapps.vista_ccc.reportes_bccs.commons.HttpMethod;
import com.myapps.vista_ccc.reportes_bccs.dao.ConfigReporteDao;
import com.myapps.vista_ccc.reportes_bccs.entity.*;
import com.myapps.vista_ccc.reportes_bccs.error.CuentaMigradaException;
import com.myapps.vista_ccc.reportes_bccs.error.HttpStatusAcceptException;
import com.myapps.vista_ccc.reportes_bccs.error.HttpStatusException;
import com.myapps.vista_ccc.reportes_bccs.servicio.*;
import com.myapps.vista_ccc.reportes_bccs.servicio.consumer.ResporteBCCSServiceConsumer;
import com.myapps.vista_ccc.reportes_bccs.servicio.rest.ReporteBCCSRestConsumer;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.SysMessage;
import com.myapps.vista_ccc.util.UtilDate;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by michael on 12/4/2019.
 */
@Named
public class ReporteConsultaCuentasBL implements Serializable {

	public static Logger log = Logger.getLogger(ReporteConsultaCuentasBL.class);
	@Inject
	private ConfigReporteDao configReporteDao;
	@Inject
	private ResporteBCCSServiceConsumer consumer;
	@Inject
	private ReporteBCCSRestConsumer consumerRest;

	public Accounts[] obtenerReporteCuentaEstado(String cuenta, String tipoDocumento, String documento,
                                                 Date fechaInicio, Date fechaFin, long idAuditoriaHistorial) throws Exception {
		Accounts[] cs = null;
		AccountResponse response = consumer.consumirAccountQuery(
				construirRequst(cuenta, tipoDocumento, documento, fechaInicio, fechaFin), idAuditoriaHistorial);
		if (response !=null && response.getAccountResponseBody() != null)
			cs = response.getAccountResponseBody();
		return cs;
	}

	public AccountRequest construirRequst(String cuenta, String tipoDocumento, String documento, Date fechaInicio,
                                          Date fechaFin) {
		AccountRequest request = new AccountRequest();
		AccountRequestBody body = new AccountRequestBody();
//		ParameterType parametroFecha = new ParameterType();
//		ParameterType[] parametrosAdd = new ParameterType[1];
//		parametroFecha.setParameterName(UtilDate.dateToString(fechaInicio, Parametros.formatoFechaReporteBccs));
//		parametroFecha.setParameterValue(UtilDate.dateToString(fechaFin, Parametros.formatoFechaReporteBccs));
//		parametrosAdd[0] = parametroFecha;
//		body.setAdditionalParameters(parametrosAdd);
		String formatoFecha = Parametros.formatoFechaReporteBccs;
		body.setCuenta(cuenta);
		body.setTipoDocumento(tipoDocumento);
		body.setDocumento(documento);
		body.setFechaInicio(UtilDate.dateToString(fechaInicio, formatoFecha));
		body.setFechaFin(UtilDate.dateToString(fechaFin, formatoFecha));
		request.setRequestBody(body);
		return request;
	}

	public Client llenarDatosClient() {
		Client cuentaClient = new Client();
		cuentaClient.setNombres("Roger");
		cuentaClient.setApellidos("Cruz");
		return cuentaClient;
	}

	public AccountDetail llenarDatosAccountDetail() {
		AccountDetail cuentaClient = new AccountDetail();
		cuentaClient.setNroCliente("66666666");
		cuentaClient.setNroCuenta("3333333");
		cuentaClient.setFechaActualizacion("04-04-2019 14:00:00");
		return cuentaClient;
	}

	public boolean validarFecha(Date startDate, Date endDate, int cantidadDias) {
		Date fechaSistema = new Date();
		boolean validar = false;
		if (startDate != null && endDate != null) {
			if (endDate.before(fechaSistema)) {
				if (startDate.before(endDate) || startDate.equals(endDate)) {
					if (UtilDate.verificarRangoDeFechas(startDate, endDate, cantidadDias))
						validar = true;
					else
						SysMessage.warn("La cantidad de días entre fecha inicio y fecha fin, no debe exceder los "
								+ cantidadDias + " días.", null);
				} else {
					SysMessage.warn("La Fecha Inicio tiene que ser menor o igual que la Fecha Fin", null);
				}
			} else {
				SysMessage.warn("La Fecha Fin tiene que ser menor o igual que la Fecha Actual del Sistema", null);
			}
		} else {
			SysMessage.warn("Fechas no validas.", null);
		}
		return validar;
	}

	public int traerCantidadDias(int idReporte) {
		ReporteFechaLimite limiteDias;
		limiteDias = configReporteDao.findByCodigoAndId(Long.valueOf(idReporte));
		return limiteDias.getDiasLimite();
	}

	public ResponseCuentaMigrada obtenerCuentaMigrada(String msisdn, String estado,  String usuario,
														long idAuditoriaHistoria) throws HttpStatusException, IOException, CuentaMigradaException {
		ResponseCuentaMigrada responseCuentaMigrada = new ResponseCuentaMigrada();
		String urlMethod = new StringBuffer().append(CuentasMigradasMethods.CUENTA_MIGRADA.getUri()).append("?msisdn=")
				.append(msisdn).append("&estado=").append(estado).toString();
		String whiteStringJson = "";
		try {
			whiteStringJson = consumerRest.consumerCuentaMigrada(msisdn, urlMethod, HttpMethod.GET, usuario,idAuditoriaHistoria);
			responseCuentaMigrada.setResponseError(convertJsonToCuentaMigradaError(whiteStringJson));
			if(responseCuentaMigrada.getResponseError().getErrors() == null || responseCuentaMigrada.getResponseError().getErrors().isEmpty()){
				responseCuentaMigrada.setCuentaMigradaResponseOk(convertJsonToCuentaMigrada(whiteStringJson));
			}
		} catch (HttpStatusAcceptException e) {
			log.info("Respuesta de sms_sent deferente de OK: " + e.getJson());
			throw new CuentaMigradaException(CuentaMigradaResponseInfo.getInstanceByJson(e.getJson()));
		}
		return responseCuentaMigrada;
	}

	public CuentaMigradaResponseOk convertJsonToCuentaMigrada(String json) {
		CuentaMigradaResponseOk cuentaMigradaResponseOk = null;
		try {
			cuentaMigradaResponseOk = CuentaMigradaResponseOk.getInstanceByJson(json);
			//log.info("FechMigracion: "+cuentaMigradaResponseOk.getFechaMigracion());
		} catch (JsonSyntaxException e) {
			log.info("Respuesta de servicio diferente de OK...");
		}
//		try {
//			throw new CuentaMigradaException(CuentaMigradaResponseInfo.getInstanceByJson(json));
//		} catch (JsonSyntaxException e) {
//			logger.warn("Respuesta no conocida retornada por es el servicio Sms Send...");
//		}
		return cuentaMigradaResponseOk;
	}

	public ResponseError convertJsonToCuentaMigradaError(String json) {
		ResponseError responseError = null;
		try {
			responseError = ResponseError.getInstanceByJson(json);
			//log.info("FechMigracion: "+cuentaMigradaResponseOk.getFechaMigracion());
		} catch (JsonSyntaxException e) {
			log.info("Respuesta de servicio diferente de OK...");
		}
//		try {
//			throw new CuentaMigradaException(CuentaMigradaResponseInfo.getInstanceByJson(json));
//		} catch (JsonSyntaxException e) {
//			logger.warn("Respuesta no conocida retornada por es el servicio Sms Send...");
//		}
		return responseError;
	}
}
