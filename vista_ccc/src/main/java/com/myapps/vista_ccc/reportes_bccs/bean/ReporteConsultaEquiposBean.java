package com.myapps.vista_ccc.reportes_bccs.bean;

import com.myapps.user.bean.ControlerBitacora;
import com.myapps.user.ldap.DescriptorBitacora;
import com.myapps.vista_ccc.reportes_bccs.business.ReporteConsultaEquiposBL;
import com.myapps.vista_ccc.reportes_bccs.servicio.Device;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.SysMessage;
import com.myapps.vista_ccc.util.UtilUrl;
import org.apache.axis.AxisFault;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.xml.rpc.ServiceException;
import java.io.Serializable;
import java.net.SocketTimeoutException;
import java.rmi.RemoteException;
import java.util.Calendar;

@ManagedBean
@ViewScoped
public class ReporteConsultaEquiposBean implements Serializable {
	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(ReporteConsultaEquiposBean.class);

	@Inject
	private ReporteConsultaEquiposBL reporteConsultaEquiposBL;
	@Inject
	private ControlerBitacora controlerBitacora;
	@Inject
	private AuditoriaResportesBCCS auditoria;
	private String pattern;
	private String msisdn;
	private String nroDocumento;
	private String imei;
	private Device[] equipos;
	private long idAuditoriaHistorial;
	private String ip;

	@PostConstruct
	public void init() {
		log.info(obtenerUsuario() + " ingreso a consulta equipos de reporte BCCS.");
		iniciar();
		cargarIp();
	}

	private void cargarIp() {
		try {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
					.getRequest();
			ip = UtilUrl.getClientIp(request);
		} catch (Exception e) {
			log.error("Error al obtener ip: ", e);
		}
	}

	private String obtenerUsuario() {
		String login = "";
		try {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
					.getRequest();
			login = (String) request.getSession().getAttribute("TEMP$USER_NAME");
			// log.info("login: " + login);
		} catch (Exception e) {
			log.error("Error al obtener usuario: ", e);
		}
		return login;
	}

	public void iniciar() {
		pattern = Parametros.formatoFechaReporteBccs;
	}

	public boolean validarImei() {
		boolean validar = false;
		if (imei != null && !imei.trim().isEmpty())
			validar = true;
		else
			SysMessage.warn("Imei no válido", null);
		return validar;
	}

	public void cargarReporte() {
		if (validarImei()) {
			try {
				idAuditoriaHistorial = auditoria.guardarAuditoria(Calendar.getInstance(), obtenerUsuario(), ip,
						Parametros.vistaReporteBCCS, Parametros.comandoReportesBCCS, msisdn, null);
				equipos = reporteConsultaEquiposBL.obtenerReporteCuentaEquipos(imei, idAuditoriaHistorial);
				log.debug("ingresando a vefificar reporte consulta equipos para IMEI: " + this.imei);
				SysMessage.info("Consulta exitosa.", null);
			} catch (Exception e) {
				tratarMostrarError(e);
			}
		}

		try {
			controlerBitacora.accion(DescriptorBitacora.CONSULTA_EQUIPOS, "Se hizo consulta del Imei: " + this.imei);
		} catch (Exception e) {
			log.error("Error al guardar bitacora en el sistema: ", e);
			SysMessage.error("Error al guardar bitacora", null);
		}
	}

	public void tratarMostrarError(Exception e) {
		if (e.getCause() instanceof AxisFault) {
			String msg = e.getMessage();
			msg = msg.toLowerCase();
			if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase())
					|| msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase())
					|| msg.contains("UnknownHostException".toLowerCase())
					|| msg.contains("No route to host".toLowerCase())
					|| msg.contains("Connection refused".toLowerCase())) {
				log.error("Error de conexion al servicio Reporte BCCS deviceQuery: ", e);
				SysMessage.error("Error de conexion del servicio Reporte BCCS deviceQuery", null);
			} else {
				log.error("Error AxisFault accountQuery: ", e);
				SysMessage.error("Error AxisFault de conexion al servicio Reporte BCCS deviceQuery", null);
			}
		} else if (e.getCause() instanceof SocketTimeoutException) {
			log.error("Error de timeout al conectarse con el servicio Reporte BCCS deviceQuery: ", e);
			SysMessage.error("Tiempo de espera para el servicio Reporte BCCS deviceQuery agotado.", null);
		} else if (e.getCause() instanceof ServiceException) {
			log.error("Error de servicio al conectarse al servicio Reporte BCCS deviceQuery: ", e);
			SysMessage.error("No se pudo conectar al servicio Reporte BCCS deviceQuery", null);
		} else if (e.getCause() instanceof RemoteException) {
			log.error("Error remoto al conectarse al servicio Reporte BCCS deviceQuery: ", e);
			SysMessage.error("Error remoto al conectarse al servicio Reporte BCCS deviceQuery", null);
		} else {
			SysMessage.error("Error interno de sistema", null);
			log.error("Error al obtener reporte: ", e);
		}
	}

	public void limpiar() {
		equipos = null;
		msisdn = "";
		nroDocumento = "";
		imei = "";

	}

	public Device[] getEquipos() {
		return equipos;
	}

	public void setEquipos(Device[] equipos) {
		this.equipos = equipos;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}
}
