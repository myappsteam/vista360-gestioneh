package com.myapps.vista_ccc.reportes_bccs.business;

import com.google.gson.JsonSyntaxException;
import com.myapps.vista_ccc.reportes_bccs.commons.CuentasMigradasMethods;
import com.myapps.vista_ccc.reportes_bccs.commons.HttpMethod;
import com.myapps.vista_ccc.reportes_bccs.dao.ConfigReporteDao;
import com.myapps.vista_ccc.reportes_bccs.entity.*;
import com.myapps.vista_ccc.reportes_bccs.error.CuentaMigradaException;
import com.myapps.vista_ccc.reportes_bccs.error.HttpStatusAcceptException;
import com.myapps.vista_ccc.reportes_bccs.error.HttpStatusException;
import com.myapps.vista_ccc.reportes_bccs.servicio.*;
import com.myapps.vista_ccc.reportes_bccs.servicio.consumer.ResporteBCCSServiceConsumer;
import com.myapps.vista_ccc.reportes_bccs.servicio.rest.ReporteBCCSRestConsumer;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.SysMessage;
import com.myapps.vista_ccc.util.UtilDate;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by michael on 15/4/2019.
 */
@Named
public class ReporteCambioPlanBL implements Serializable {
	public static Logger log = Logger.getLogger(ReporteCambioPlanBL.class);
	@Inject
	private ConfigReporteDao configReporteDao;
	@Inject
	private ResporteBCCSServiceConsumer consumer;
	@Inject
	private ReporteBCCSRestConsumer consumerRest;

	public ConsumerPlanResponseBody obtenerReporteCambioPlan(String cuenta, Date fechaInicio, Date fechaFin, long idAuditoriaHistorial)
			throws Exception {
		ConsumerPlanResponseBody cs = null;
		ConsumerPlanResponse response = consumer
				.consumirConsumerPlanResponse(construirRequst(cuenta, fechaInicio, fechaFin),idAuditoriaHistorial);
		if (response !=null && response.getChangeStatusLogResponseBody() != null)
			cs = response.getChangeStatusLogResponseBody();
		return cs;
	}

	public ConsumerPlanRequest construirRequst(String cuenta, Date fechaInicio, Date fechaFin) {
		ConsumerPlanRequest request = new ConsumerPlanRequest();
		ConsumerPlanRequestBody body = new ConsumerPlanRequestBody();
		ParameterType parametroFecha = new ParameterType();
		ParameterType[] parametrosAdd = new ParameterType[1];
		parametroFecha.setParameterName(UtilDate.dateToString(fechaInicio, Parametros.formatoFechaReporteBccs));
		parametroFecha.setParameterValue(UtilDate.dateToString(fechaFin, Parametros.formatoFechaReporteBccs));
		parametrosAdd[0] = parametroFecha;
		body.setCuenta(cuenta);
//		body.setFechaInicio(UtilDate.dateToString(fechaInicio, ""));
//		body.setFechaFin(UtilDate.dateToString(fechaFin, ""));
		body.setAdditionalParameters(parametrosAdd);
		request.setRequestBody(body);
		return request;
	}

	public ConsumerPlanResponseBody obtenerCambioPlan() {
		return null;
	}

	public boolean validarFecha(Date startDate, Date endDate, int cantidadDias) {
		Date fechaSistema = new Date();
		boolean validar = false;
		if (startDate != null && endDate != null) {
			if (endDate.before(fechaSistema)) {
				if (startDate.before(endDate) || startDate.equals(endDate)) {
					if (UtilDate.verificarRangoDeFechas(startDate, endDate, cantidadDias))
						validar = true;
					else
						SysMessage.warn("La cantidad de días entre fecha inicio y fecha fin, no debe exceder los "
								+ cantidadDias + " días.", null);
				} else {
					SysMessage.warn("La Fecha Inicio tiene que ser menor o igual que la Fecha Fin", null);
				}
			} else {
				SysMessage.warn("La Fecha Fin tiene que ser menor o igual que la Fecha Actual del Sistema", null);
			}
		} else {
			SysMessage.warn("Fechas no validas.", null);
		}
		return validar;
	}

	public int traerCantidadDias(int idReporte) {
		ReporteFechaLimite limiteDias;
		limiteDias = configReporteDao.findByCodigoAndId(Long.valueOf(idReporte));
		return limiteDias.getDiasLimite();
	}

	public ResponseCuentaMigrada obtenerCuentaMigrada(String msisdn, String estado, String usuario,
                                                      long idAuditoriaHistoria) throws HttpStatusException, IOException, CuentaMigradaException {
		ResponseCuentaMigrada responseCuentaMigrada = new ResponseCuentaMigrada();
		String urlMethod = new StringBuffer().append(CuentasMigradasMethods.CUENTA_MIGRADA.getUri()).append("?msisdn=")
				.append(msisdn).append("&estado=").append(estado).toString();
		String whiteStringJson = "";
		try {
			whiteStringJson = consumerRest.consumerCuentaMigrada(msisdn, urlMethod, HttpMethod.GET, usuario,idAuditoriaHistoria);
			responseCuentaMigrada.setResponseError(convertJsonToCuentaMigradaError(whiteStringJson));
			if(responseCuentaMigrada.getResponseError().getErrors() == null || responseCuentaMigrada.getResponseError().getErrors().isEmpty()){
				responseCuentaMigrada.setCuentaMigradaResponseOk(convertJsonToCuentaMigrada(whiteStringJson));
			}
		} catch (HttpStatusAcceptException e) {
			log.info("Respuesta de sms_sent deferente de OK: " + e.getJson());
			throw new CuentaMigradaException(CuentaMigradaResponseInfo.getInstanceByJson(e.getJson()));
		}
		return responseCuentaMigrada;
	}

	public ResponseError convertJsonToCuentaMigradaError(String json) {
		ResponseError responseError = null;
		try {
			responseError = ResponseError.getInstanceByJson(json);
			//log.info("FechMigracion: "+cuentaMigradaResponseOk.getFechaMigracion());
		} catch (JsonSyntaxException e) {
			log.info("Respuesta de servicio diferente de OK...");
		}
//		try {
//			throw new CuentaMigradaException(CuentaMigradaResponseInfo.getInstanceByJson(json));
//		} catch (JsonSyntaxException e) {
//			logger.warn("Respuesta no conocida retornada por es el servicio Sms Send...");
//		}
		return responseError;
	}

	public CuentaMigradaResponseOk convertJsonToCuentaMigrada(String json) {
		CuentaMigradaResponseOk list = new CuentaMigradaResponseOk();
		try {
			list = CuentaMigradaResponseOk.getInstanceByJson(json);
		} catch (JsonSyntaxException e) {
			log.info("Respuesta de servicio diferente de OK...");
		}
//		try {
//			throw new CuentaMigradaException(CuentaMigradaResponseInfo.getInstanceByJson(json));
//		} catch (JsonSyntaxException e) {
//			logger.warn("Respuesta no conocida retornada por es el servicio Sms Send...");
//		}
		return list;
	}
}
