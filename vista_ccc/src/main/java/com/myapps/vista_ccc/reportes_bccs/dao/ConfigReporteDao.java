package com.myapps.vista_ccc.reportes_bccs.dao;

import com.myapps.vista_ccc.business.ConexionTelnetBL;
import com.myapps.vista_ccc.dao.MasterDao;
import com.myapps.vista_ccc.dao.MasterDaoInterface;
import com.myapps.vista_ccc.reportes_bccs.entity.ReporteFechaLimite;
import org.apache.log4j.Logger;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import java.io.Serializable;
import java.util.List;

/**
 * Created by michael on 9/4/2019.
 */
@Named
public class ConfigReporteDao implements Serializable, MasterDaoInterface {
	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(ConexionTelnetBL.class);

	@PersistenceContext(unitName = "vista_ccc")
	private transient EntityManager entityManager;

	@Resource
	private transient UserTransaction transaction;

	@Inject
	private MasterDao masterDao;

	@Override
	public String validar(Object entidad, boolean nuevo) {
		return null;
	}

	@Override
	public void save(Object entidad) throws Exception {
		masterDao.remove(entidad);
	}

	@Override
	public void remove(Object entidad) throws Exception {
		masterDao.remove(entidad);
	}

	@Override
	public void update(Object entidad) throws Exception {
		masterDao.update(entidad);
	}

	@Override
	public Object find(Object key, Class clase) throws Exception {
		return masterDao.find(key, clase);
	}

	@Override
	public List findAll() throws Exception {
		String sql = "SELECT p FROM ReporteFechaLimite p";
		return masterDao.findAllQuery(ReporteFechaLimite.class, sql, null);
	}

	public void update(List<ReporteFechaLimite> parametros) throws Exception {
		for (ReporteFechaLimite param : parametros) {
			transaction.begin();
			entityManager.joinTransaction();
			try {
				entityManager.merge(param);
				transaction.commit();
			} catch (Exception e) {
				transaction.rollback();
				throw e;
			}
		}
	}

	public ReporteFechaLimite findByCodigoAndId(Long idReporte) {
//         = new ReporteFechaLimite();
//        try {
		Query q = entityManager.createQuery("FROM ReporteFechaLimite r WHERE (r.idReporteHistorico = ?1)");
		q.setParameter(1, idReporte);
		ReporteFechaLimite cantidadDias = (ReporteFechaLimite) q.getSingleResult();
//        }catch (Exception e){
//            log.error("No no pudo encortrar fecha limite por codigo e id: "+e.getMessage(),e);
//        }
		return cantidadDias;
	}

}
