/**
 * InterfaceWS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.myapps.vista_ccc.reportes_bccs.servicio;

public interface InterfaceWS extends java.rmi.Remote {
    public com.myapps.vista_ccc.reportes_bccs.servicio.AccountResponse accountQuery(com.myapps.vista_ccc.reportes_bccs.servicio.AccountRequest accountQuery) throws java.rmi.RemoteException;
    public com.myapps.vista_ccc.reportes_bccs.servicio.ChangeStatusLogResponse changeStatusLogQuery(com.myapps.vista_ccc.reportes_bccs.servicio.ChangeStatusLogRequest changeStatusLogQuery) throws java.rmi.RemoteException;
    public com.myapps.vista_ccc.reportes_bccs.servicio.ClaimByNroMEMOResponse claimQueryByNroMEMO(com.myapps.vista_ccc.reportes_bccs.servicio.ClaimByNroMEMORequest claimQueryByNroMEMO) throws java.rmi.RemoteException;
    public com.myapps.vista_ccc.reportes_bccs.servicio.ClaimByMSISDNResponse claimQueryByMSISDN(com.myapps.vista_ccc.reportes_bccs.servicio.ClaimByMSISDNRequest claimQueryByMSISDN) throws java.rmi.RemoteException;
    public com.myapps.vista_ccc.reportes_bccs.servicio.DeviceResponse deviceQuery(com.myapps.vista_ccc.reportes_bccs.servicio.DeviceRequest deviceQuery) throws java.rmi.RemoteException;
    public com.myapps.vista_ccc.reportes_bccs.servicio.ConsumerPlanResponse consumerPlanQuery(com.myapps.vista_ccc.reportes_bccs.servicio.ConsumerPlanRequest consumerPlanQuery) throws java.rmi.RemoteException;
}
