/**
 * Reclamo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.myapps.vista_ccc.reportes_bccs.servicio;

public class Reclamo  implements java.io.Serializable {
    private String nroReclamo;

    private String eqHumanoCarga;

    private String eHumanoCarga;

    private String estado;

    private String detalleFecha;

    private String detalleHora;

    private String nroMemo;

    private String tipo;

    private String acmo;

    public Reclamo() {
    }

    public Reclamo(
           String nroReclamo,
           String eqHumanoCarga,
           String eHumanoCarga,
           String estado,
           String detalleFecha,
           String detalleHora,
           String nroMemo,
           String tipo,
           String acmo) {
           this.nroReclamo = nroReclamo;
           this.eqHumanoCarga = eqHumanoCarga;
           this.eHumanoCarga = eHumanoCarga;
           this.estado = estado;
           this.detalleFecha = detalleFecha;
           this.detalleHora = detalleHora;
           this.nroMemo = nroMemo;
           this.tipo = tipo;
           this.acmo = acmo;
    }


    /**
     * Gets the nroReclamo value for this Reclamo.
     * 
     * @return nroReclamo
     */
    public String getNroReclamo() {
        return nroReclamo;
    }


    /**
     * Sets the nroReclamo value for this Reclamo.
     * 
     * @param nroReclamo
     */
    public void setNroReclamo(String nroReclamo) {
        this.nroReclamo = nroReclamo;
    }


    /**
     * Gets the eqHumanoCarga value for this Reclamo.
     * 
     * @return eqHumanoCarga
     */
    public String getEqHumanoCarga() {
        return eqHumanoCarga;
    }


    /**
     * Sets the eqHumanoCarga value for this Reclamo.
     * 
     * @param eqHumanoCarga
     */
    public void setEqHumanoCarga(String eqHumanoCarga) {
        this.eqHumanoCarga = eqHumanoCarga;
    }


    /**
     * Gets the eHumanoCarga value for this Reclamo.
     * 
     * @return eHumanoCarga
     */
    public String getEHumanoCarga() {
        return eHumanoCarga;
    }


    /**
     * Sets the eHumanoCarga value for this Reclamo.
     * 
     * @param eHumanoCarga
     */
    public void setEHumanoCarga(String eHumanoCarga) {
        this.eHumanoCarga = eHumanoCarga;
    }


    /**
     * Gets the estado value for this Reclamo.
     * 
     * @return estado
     */
    public String getEstado() {
        return estado;
    }


    /**
     * Sets the estado value for this Reclamo.
     * 
     * @param estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }


    /**
     * Gets the detalleFecha value for this Reclamo.
     * 
     * @return detalleFecha
     */
    public String getDetalleFecha() {
        return detalleFecha;
    }


    /**
     * Sets the detalleFecha value for this Reclamo.
     * 
     * @param detalleFecha
     */
    public void setDetalleFecha(String detalleFecha) {
        this.detalleFecha = detalleFecha;
    }


    /**
     * Gets the detalleHora value for this Reclamo.
     * 
     * @return detalleHora
     */
    public String getDetalleHora() {
        return detalleHora;
    }


    /**
     * Sets the detalleHora value for this Reclamo.
     * 
     * @param detalleHora
     */
    public void setDetalleHora(String detalleHora) {
        this.detalleHora = detalleHora;
    }


    /**
     * Gets the nroMemo value for this Reclamo.
     * 
     * @return nroMemo
     */
    public String getNroMemo() {
        return nroMemo;
    }


    /**
     * Sets the nroMemo value for this Reclamo.
     * 
     * @param nroMemo
     */
    public void setNroMemo(String nroMemo) {
        this.nroMemo = nroMemo;
    }


    /**
     * Gets the tipo value for this Reclamo.
     * 
     * @return tipo
     */
    public String getTipo() {
        return tipo;
    }


    /**
     * Sets the tipo value for this Reclamo.
     * 
     * @param tipo
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }


    /**
     * Gets the acmo value for this Reclamo.
     * 
     * @return acmo
     */
    public String getAcmo() {
        return acmo;
    }


    /**
     * Sets the acmo value for this Reclamo.
     * 
     * @param acmo
     */
    public void setAcmo(String acmo) {
        this.acmo = acmo;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof Reclamo)) return false;
        Reclamo other = (Reclamo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.nroReclamo==null && other.getNroReclamo()==null) || 
             (this.nroReclamo!=null &&
              this.nroReclamo.equals(other.getNroReclamo()))) &&
            ((this.eqHumanoCarga==null && other.getEqHumanoCarga()==null) || 
             (this.eqHumanoCarga!=null &&
              this.eqHumanoCarga.equals(other.getEqHumanoCarga()))) &&
            ((this.eHumanoCarga==null && other.getEHumanoCarga()==null) || 
             (this.eHumanoCarga!=null &&
              this.eHumanoCarga.equals(other.getEHumanoCarga()))) &&
            ((this.estado==null && other.getEstado()==null) || 
             (this.estado!=null &&
              this.estado.equals(other.getEstado()))) &&
            ((this.detalleFecha==null && other.getDetalleFecha()==null) || 
             (this.detalleFecha!=null &&
              this.detalleFecha.equals(other.getDetalleFecha()))) &&
            ((this.detalleHora==null && other.getDetalleHora()==null) || 
             (this.detalleHora!=null &&
              this.detalleHora.equals(other.getDetalleHora()))) &&
            ((this.nroMemo==null && other.getNroMemo()==null) || 
             (this.nroMemo!=null &&
              this.nroMemo.equals(other.getNroMemo()))) &&
            ((this.tipo==null && other.getTipo()==null) || 
             (this.tipo!=null &&
              this.tipo.equals(other.getTipo()))) &&
            ((this.acmo==null && other.getAcmo()==null) || 
             (this.acmo!=null &&
              this.acmo.equals(other.getAcmo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNroReclamo() != null) {
            _hashCode += getNroReclamo().hashCode();
        }
        if (getEqHumanoCarga() != null) {
            _hashCode += getEqHumanoCarga().hashCode();
        }
        if (getEHumanoCarga() != null) {
            _hashCode += getEHumanoCarga().hashCode();
        }
        if (getEstado() != null) {
            _hashCode += getEstado().hashCode();
        }
        if (getDetalleFecha() != null) {
            _hashCode += getDetalleFecha().hashCode();
        }
        if (getDetalleHora() != null) {
            _hashCode += getDetalleHora().hashCode();
        }
        if (getNroMemo() != null) {
            _hashCode += getNroMemo().hashCode();
        }
        if (getTipo() != null) {
            _hashCode += getTipo().hashCode();
        }
        if (getAcmo() != null) {
            _hashCode += getAcmo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Reclamo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "Reclamo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nroReclamo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nroReclamo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("eqHumanoCarga");
        elemField.setXmlName(new javax.xml.namespace.QName("", "eqHumanoCarga"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EHumanoCarga");
        elemField.setXmlName(new javax.xml.namespace.QName("", "eHumanoCarga"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estado");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("detalleFecha");
        elemField.setXmlName(new javax.xml.namespace.QName("", "detalleFecha"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("detalleHora");
        elemField.setXmlName(new javax.xml.namespace.QName("", "detalleHora"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nroMemo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nroMemo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("acmo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "acmo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
