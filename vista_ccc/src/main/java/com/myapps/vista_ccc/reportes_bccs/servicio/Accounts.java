/**
 * Accounts.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.myapps.vista_ccc.reportes_bccs.servicio;

public class Accounts  implements java.io.Serializable {
    private com.myapps.vista_ccc.reportes_bccs.servicio.Client client;

    private com.myapps.vista_ccc.reportes_bccs.servicio.AccountDetail accountDetail;

    public Accounts() {
    }

    public Accounts(
           com.myapps.vista_ccc.reportes_bccs.servicio.Client client,
           com.myapps.vista_ccc.reportes_bccs.servicio.AccountDetail accountDetail) {
           this.client = client;
           this.accountDetail = accountDetail;
    }


    /**
     * Gets the client value for this Accounts.
     * 
     * @return client
     */
    public com.myapps.vista_ccc.reportes_bccs.servicio.Client getClient() {
        return client;
    }


    /**
     * Sets the client value for this Accounts.
     * 
     * @param client
     */
    public void setClient(com.myapps.vista_ccc.reportes_bccs.servicio.Client client) {
        this.client = client;
    }


    /**
     * Gets the accountDetail value for this Accounts.
     * 
     * @return accountDetail
     */
    public com.myapps.vista_ccc.reportes_bccs.servicio.AccountDetail getAccountDetail() {
        return accountDetail;
    }


    /**
     * Sets the accountDetail value for this Accounts.
     * 
     * @param accountDetail
     */
    public void setAccountDetail(com.myapps.vista_ccc.reportes_bccs.servicio.AccountDetail accountDetail) {
        this.accountDetail = accountDetail;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof Accounts)) return false;
        Accounts other = (Accounts) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.client==null && other.getClient()==null) || 
             (this.client!=null &&
              this.client.equals(other.getClient()))) &&
            ((this.accountDetail==null && other.getAccountDetail()==null) || 
             (this.accountDetail!=null &&
              this.accountDetail.equals(other.getAccountDetail())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getClient() != null) {
            _hashCode += getClient().hashCode();
        }
        if (getAccountDetail() != null) {
            _hashCode += getAccountDetail().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Accounts.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "Accounts"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("client");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Client"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "Client"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("", "accountDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "AccountDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
