package com.myapps.vista_ccc.reportes_bccs.entity;

import com.myapps.vista_ccc.reportes_bccs.servicio.Accounts;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Cuenta implements Serializable {

	private String nroRegistro;

	private String tipoPersona;

	private String razonSocial;

	private String nroMatricula;

	private String NIT;

	private String nroDocumentoDescripcion;

	private String nroDocumento;

	private String nombres;

	private String apellidos;

	private String fechaNacimiento;

	private String OEP;

	private String nacionalidad;

	private String formaRegistro;

	private String usuarioIngreso;

	private String fechaIngreso;

	private String horaIngreso;

	public static List<Cuenta> getInstanceByClietn(List<Accounts> acounts) {
		List<Cuenta> cuentas = new ArrayList<Cuenta>();
		for (Accounts c : acounts) {
			Cuenta cc = new Cuenta();
			cc.apellidos = c.getClient().getApellidos();
			cc.nombres = c.getClient().getNombres();
			cuentas.add(cc);
		}
		return cuentas;
	}

	public String getNroRegistro() {
		return nroRegistro;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public String getNroMatricula() {
		return nroMatricula;
	}

	public String getNIT() {
		return NIT;
	}

	public String getNroDocumentoDescripcion() {
		return nroDocumentoDescripcion;
	}

	public String getNroDocumento() {
		return nroDocumento;
	}

	public String getNombres() {
		return nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public String getOEP() {
		return OEP;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public String getFormaRegistro() {
		return formaRegistro;
	}

	public String getUsuarioIngreso() {
		return usuarioIngreso;
	}

	public String getFechaIngreso() {
		return fechaIngreso;
	}

	public String getHoraIngreso() {
		return horaIngreso;
	}

}
