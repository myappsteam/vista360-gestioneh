package com.myapps.vista_ccc.reportes_bccs.bean;

import com.myapps.user.bean.ControlerBitacora;
import com.myapps.user.ldap.DescriptorBitacora;
import com.myapps.vista_ccc.reportes_bccs.dao.ConfigReporteDao;
import com.myapps.vista_ccc.reportes_bccs.entity.ReporteFechaLimite;
import com.myapps.vista_ccc.util.SysMessage;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.List;

/**
 * Created by michael on 9/4/2019.
 */
@ManagedBean
@ViewScoped
public class ConfigReporteBean implements Serializable {

	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(ConfigReporteBean.class);

	private List<ReporteFechaLimite> parametros;

	@Inject
	private ConfigReporteDao configReporteDao;
	@Inject
	private ControlerBitacora controlerBitacora;

	@PostConstruct
	public void init() {
		log.info(obtenerUsuario() + " ingreso a configuracion de dias por reporte BCCS.");
		loadParameters();
	}

	public void loadParameters() {
		try {
			parametros = configReporteDao.findAll();
			log.info("parametros recuperados " + parametros);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	public void actualizarParametros() {
		try {
			log.debug("actualizando fecha limite de reportes");
			configReporteDao.update(parametros);
			controlerBitacora.accion(DescriptorBitacora.ACTUALIZACION_FECHALIMITE,
					"Se hizo actualizacion de parametros de fecha limite: ");
			loadParameters();
			log.info("parametros recuperados " + parametros);
			SysMessage.info("Los datos se actualizaron correctamente.", null);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	private String obtenerUsuario() {
		String login = "";
		try {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
					.getRequest();
			login = (String) request.getSession().getAttribute("TEMP$USER_NAME");
			// log.info("login: " + login);
		} catch (Exception e) {
			log.error("Error al obtener usuario: ", e);
		}
		return login;
	}

	public List<ReporteFechaLimite> getParametros() {
		return parametros;
	}

	public void setParametros(List<ReporteFechaLimite> parametros) {
		this.parametros = parametros;
	}
}
