package com.myapps.vista_ccc.reportes_bccs.business;

import com.google.gson.JsonSyntaxException;
import com.myapps.vista_ccc.reportes_bccs.commons.CuentasMigradasMethods;
import com.myapps.vista_ccc.reportes_bccs.commons.HttpMethod;
import com.myapps.vista_ccc.reportes_bccs.dao.ConfigReporteDao;
import com.myapps.vista_ccc.reportes_bccs.entity.*;
import com.myapps.vista_ccc.reportes_bccs.error.CuentaMigradaException;
import com.myapps.vista_ccc.reportes_bccs.error.HttpStatusAcceptException;
import com.myapps.vista_ccc.reportes_bccs.error.HttpStatusException;
import com.myapps.vista_ccc.reportes_bccs.servicio.*;
import com.myapps.vista_ccc.reportes_bccs.servicio.consumer.ResporteBCCSServiceConsumer;
import com.myapps.vista_ccc.reportes_bccs.servicio.rest.ReporteBCCSRestConsumer;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.SysMessage;
import com.myapps.vista_ccc.util.UtilDate;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by michael on 15/4/2019.
 */
@Named
public class ReporteCargaAcmoBL implements Serializable {
	public static Logger log = Logger.getLogger(ReporteCargaAcmoBL.class);
	@Inject
	private ConfigReporteDao configReporteDao;
	@Inject
	private ResporteBCCSServiceConsumer consumer;
	@Inject
	private ReporteBCCSRestConsumer consumerRest;

	public Reclamo[] obtenerCargaAcmo(String cuenta, Date fechaInicio, Date fechaFin, long idAuditoriaHistorial)
			throws Exception {
		Reclamo[] cs = null;
		ClaimByMSISDNResponse response = consumer.consumirClaimResponse(construirRequst(cuenta, fechaInicio, fechaFin),
				idAuditoriaHistorial);
		if (response !=null && response.getClaimByMSISDNResponseBody() != null)
			cs = response.getClaimByMSISDNResponseBody();
		return cs;
	}

	public ClaimByNroMEMOResponseBody obtenerCargaAcmoByMemo(String memo, long idAuditoriaHistorial)
			throws Exception {
		ClaimByNroMEMOResponseBody cs = null;
		ClaimByNroMEMOResponse response = consumer.consumirClaimResponseByMemo(construirRequstByMemo(memo),
				idAuditoriaHistorial);
		if (response !=null && response.getClaimByNroMEMOResponseBody() != null)
			cs = response.getClaimByNroMEMOResponseBody();
		return cs;
	}

	public ClaimByMSISDNRequest construirRequst(String cuenta, Date fechaInicio, Date fechaFin) {
		ClaimByMSISDNRequest request = new ClaimByMSISDNRequest();
		ClaimByMSISDNRequestBody body = new ClaimByMSISDNRequestBody();
//		ParameterType parametroFecha = new ParameterType();
//		ParameterType[] parametrosAdd = new ParameterType[1];
//		parametroFecha.setParameterName(UtilDate.dateToString(fechaInicio, Parametros.formatoFechaReporteBccs));
//		parametroFecha.setParameterValue(UtilDate.dateToString(fechaFin, Parametros.formatoFechaReporteBccs));
//		parametrosAdd[0] = parametroFecha;
//		body.setAdditionalParameters(parametrosAdd);
		String formatoFecha = Parametros.formatoFechaReporteBccs;
		body.setFechaInicio(UtilDate.dateToString(fechaInicio, formatoFecha));
		body.setFechaFin(UtilDate.dateToString(fechaFin, formatoFecha));
		body.setCuenta(cuenta);
		request.setRequestBody(body);
		return request;
	}

	public ClaimByNroMEMORequest construirRequstByMemo(String memo) {
		ClaimByNroMEMORequest request = new ClaimByNroMEMORequest();
		ClaimByNroMEMORequestBody body = new ClaimByNroMEMORequestBody();
//		ParameterType parametroFecha = new ParameterType();
//		ParameterType[] parametrosAdd = new ParameterType[1];
//		parametroFecha.setParameterName(UtilDate.dateToString(fechaInicio, Parametros.formatoFechaReporteBccs));
//		parametroFecha.setParameterValue(UtilDate.dateToString(fechaFin, Parametros.formatoFechaReporteBccs));
//		parametrosAdd[0] = parametroFecha;
//		body.setAdditionalParameters(parametrosAdd);
		body.setMemo(memo);
		request.setRequestBody(body);
		return request;
	}

	public Reclamo[] obtenerReporteCargaAcmo(String cuenta, Date fechaInicio, Date fechafin) {
		return null;
	}

	/*public Reclamo[] llenarClain() {
		Reclamo claim = new Reclamo();
		claim.setEstadoReclamo("C");
		claim.setTipoReclamo("A");
		claim.setNroReclamo("120,169,570");
		claim.setTipoNotificacion("Personal");
		claim.setRespuesta("INTERNAL LOTHAR USAGE - DO NOT DELETE");
		claim.setSucursal("");
		claim.setNombrePortador("");
		claim.setFechaOcurrencia("");
		claim.setDireccionOcurrencia("");
		claim.setPosicion("");
		claim.setZonaOcurrencia("");
		claim.setSectorOcurrencia("");
		claim.setEqHumanoCarga("48,898");
		claim.setEHumanoCarga("MONASTERIO VIRUEZ,  HUGO");
		claim.setClaimDetail(llenarDetalleReclamo());
		claim.setClaimMEMO(llenarMEMOReclamo());
		claim.setMotivosContactos(llenarMotivosContactos());
		Claim[] cs = new Claim[1];
		cs[0] = claim;
		return cs;
	}

	public Detalle llenarDetalleReclamo() {
		Detalle detalle = new Detalle();
		detalle.setNroDetalle("1");
		detalle.setEstado("Solucionado");
		detalle.setOficina("Area de Call Center & Nuevos C");
		detalle.setResponsable("MORETTA SUAREZ, IRMA MONYKA");
		detalle.setObservacion("");
		detalle.setFecha("26/3/2019");
		detalle.setHora("14:33:24");
		detalle.setEHumanoCarga("MONASTERIO VIRUEZ,  HUGO");
		return detalle;
	}

	public Memo llenarMEMOReclamo() {
		Memo memo = new Memo();
		memo.setNroMemo("0");
		memo.setCodProceso("0");
		memo.setCliente("0");
		memo.setContrato("0");
		memo.setTipoCuenta("");
		memo.setNroCuenta("");
		memo.setFecha("");
		memo.setHora("00:00:00");
		memo.setDescripcion("");
		return memo;
	}

	public MotivoContactos llenarMotivosContactos() {
		MotivoContactos motivoContactos = new MotivoContactos();
		motivoContactos.setTipo("S");
		motivoContactos.setDescripcion("VERIFICACIÓN SUPERVISORES");
		return motivoContactos;
	}*/

	public boolean validarFecha(Date startDate, Date endDate, int cantidadDias) {
		Date fechaSistema = new Date();
		boolean validar = false;
		if (startDate != null && endDate != null) {
			if (endDate.before(fechaSistema)) {
				if (startDate.before(endDate) || startDate.equals(endDate)) {
					if (UtilDate.verificarRangoDeFechas(startDate, endDate, cantidadDias))
						validar = true;
					else
						SysMessage.warn("La cantidad de días entre fecha inicio y fecha fin, no debe exceder los "
								+ cantidadDias + " días.", null);
				} else {
					SysMessage.warn("La Fecha Inicio tiene que ser menor o igual que la Fecha Fin", null);
				}
			} else {
				SysMessage.warn("La Fecha Fin tiene que ser menor o igual que la Fecha Actual del Sistema", null);
			}
		} else {
			SysMessage.warn("Fechas no validas.", null);
		}
		return validar;
	}

	public int traerCantidadDias(int idReporte) {
		ReporteFechaLimite limiteDias;
		limiteDias = configReporteDao.findByCodigoAndId(Long.valueOf(idReporte));
		return limiteDias.getDiasLimite();
	}

	public ResponseCuentaMigrada obtenerCuentaMigrada(String msisdn, String estado, String usuario,
													  long idAuditoriaHistoria) throws HttpStatusException, IOException, CuentaMigradaException {
		ResponseCuentaMigrada responseCuentaMigrada = new ResponseCuentaMigrada();
		String urlMethod = new StringBuffer().append(CuentasMigradasMethods.CUENTA_MIGRADA.getUri()).append("?msisdn=")
				.append(msisdn).append("&estado=").append(estado).toString();
		String whiteStringJson = "";
		try {
			whiteStringJson = consumerRest.consumerCuentaMigrada(msisdn, urlMethod, HttpMethod.GET, usuario,idAuditoriaHistoria);
			responseCuentaMigrada.setResponseError(convertJsonToCuentaMigradaError(whiteStringJson));
			if(responseCuentaMigrada.getResponseError().getErrors() == null || responseCuentaMigrada.getResponseError().getErrors().isEmpty()){
				responseCuentaMigrada.setCuentaMigradaResponseOk(convertJsonToCuentaMigrada(whiteStringJson));
			}
		} catch (HttpStatusAcceptException e) {
			log.info("Respuesta de sms_sent deferente de OK: " + e.getJson());
			throw new CuentaMigradaException(CuentaMigradaResponseInfo.getInstanceByJson(e.getJson()));
		}
		return responseCuentaMigrada;
	}

	public ResponseError convertJsonToCuentaMigradaError(String json) {
		ResponseError responseError = null;
		try {
			responseError = ResponseError.getInstanceByJson(json);
			//log.info("FechMigracion: "+cuentaMigradaResponseOk.getFechaMigracion());
		} catch (JsonSyntaxException e) {
			log.info("Respuesta de servicio diferente de OK...");
		}
//		try {
//			throw new CuentaMigradaException(CuentaMigradaResponseInfo.getInstanceByJson(json));
//		} catch (JsonSyntaxException e) {
//			logger.warn("Respuesta no conocida retornada por es el servicio Sms Send...");
//		}
		return responseError;
	}

	public CuentaMigradaResponseOk convertJsonToCuentaMigrada(String json) {
		CuentaMigradaResponseOk list = new CuentaMigradaResponseOk();
		try {
			list = CuentaMigradaResponseOk.getInstanceByJson(json);
		} catch (JsonSyntaxException e) {
			log.info("Respuesta de servicio diferente de OK...");
		}
//		try {
//			throw new CuentaMigradaException(CuentaMigradaResponseInfo.getInstanceByJson(json));
//		} catch (JsonSyntaxException e) {
//			logger.warn("Respuesta no conocida retornada por es el servicio Sms Send...");
//		}
		return list;
	}
}
