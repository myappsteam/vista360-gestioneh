/**
 * CambiosPlan.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.myapps.vista_ccc.reportes_bccs.servicio;

public class CambiosPlan  implements java.io.Serializable {
    private String planConsumo;

    private String planConsumoDescripcion;

    private String fechaIngreso;

    private String fechaAplicada;

    private String estadoCambioPlan;

    private String serieComprobante;

    private String tipoComprobante;

    private String nroComprobante;

    private String usuario;

    public CambiosPlan() {
    }

    public CambiosPlan(
           String planConsumo,
           String planConsumoDescripcion,
           String fechaIngreso,
           String fechaAplicada,
           String estadoCambioPlan,
           String serieComprobante,
           String tipoComprobante,
           String nroComprobante,
           String usuario) {
           this.planConsumo = planConsumo;
           this.planConsumoDescripcion = planConsumoDescripcion;
           this.fechaIngreso = fechaIngreso;
           this.fechaAplicada = fechaAplicada;
           this.estadoCambioPlan = estadoCambioPlan;
           this.serieComprobante = serieComprobante;
           this.tipoComprobante = tipoComprobante;
           this.nroComprobante = nroComprobante;
           this.usuario = usuario;
    }


    /**
     * Gets the planConsumo value for this CambiosPlan.
     * 
     * @return planConsumo
     */
    public String getPlanConsumo() {
        return planConsumo;
    }


    /**
     * Sets the planConsumo value for this CambiosPlan.
     * 
     * @param planConsumo
     */
    public void setPlanConsumo(String planConsumo) {
        this.planConsumo = planConsumo;
    }


    /**
     * Gets the planConsumoDescripcion value for this CambiosPlan.
     * 
     * @return planConsumoDescripcion
     */
    public String getPlanConsumoDescripcion() {
        return planConsumoDescripcion;
    }


    /**
     * Sets the planConsumoDescripcion value for this CambiosPlan.
     * 
     * @param planConsumoDescripcion
     */
    public void setPlanConsumoDescripcion(String planConsumoDescripcion) {
        this.planConsumoDescripcion = planConsumoDescripcion;
    }


    /**
     * Gets the fechaIngreso value for this CambiosPlan.
     * 
     * @return fechaIngreso
     */
    public String getFechaIngreso() {
        return fechaIngreso;
    }


    /**
     * Sets the fechaIngreso value for this CambiosPlan.
     * 
     * @param fechaIngreso
     */
    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }


    /**
     * Gets the fechaAplicada value for this CambiosPlan.
     * 
     * @return fechaAplicada
     */
    public String getFechaAplicada() {
        return fechaAplicada;
    }


    /**
     * Sets the fechaAplicada value for this CambiosPlan.
     * 
     * @param fechaAplicada
     */
    public void setFechaAplicada(String fechaAplicada) {
        this.fechaAplicada = fechaAplicada;
    }


    /**
     * Gets the estadoCambioPlan value for this CambiosPlan.
     * 
     * @return estadoCambioPlan
     */
    public String getEstadoCambioPlan() {
        return estadoCambioPlan;
    }


    /**
     * Sets the estadoCambioPlan value for this CambiosPlan.
     * 
     * @param estadoCambioPlan
     */
    public void setEstadoCambioPlan(String estadoCambioPlan) {
        this.estadoCambioPlan = estadoCambioPlan;
    }


    /**
     * Gets the serieComprobante value for this CambiosPlan.
     * 
     * @return serieComprobante
     */
    public String getSerieComprobante() {
        return serieComprobante;
    }


    /**
     * Sets the serieComprobante value for this CambiosPlan.
     * 
     * @param serieComprobante
     */
    public void setSerieComprobante(String serieComprobante) {
        this.serieComprobante = serieComprobante;
    }


    /**
     * Gets the tipoComprobante value for this CambiosPlan.
     * 
     * @return tipoComprobante
     */
    public String getTipoComprobante() {
        return tipoComprobante;
    }


    /**
     * Sets the tipoComprobante value for this CambiosPlan.
     * 
     * @param tipoComprobante
     */
    public void setTipoComprobante(String tipoComprobante) {
        this.tipoComprobante = tipoComprobante;
    }


    /**
     * Gets the nroComprobante value for this CambiosPlan.
     * 
     * @return nroComprobante
     */
    public String getNroComprobante() {
        return nroComprobante;
    }


    /**
     * Sets the nroComprobante value for this CambiosPlan.
     * 
     * @param nroComprobante
     */
    public void setNroComprobante(String nroComprobante) {
        this.nroComprobante = nroComprobante;
    }


    /**
     * Gets the usuario value for this CambiosPlan.
     * 
     * @return usuario
     */
    public String getUsuario() {
        return usuario;
    }


    /**
     * Sets the usuario value for this CambiosPlan.
     * 
     * @param usuario
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof CambiosPlan)) return false;
        CambiosPlan other = (CambiosPlan) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.planConsumo==null && other.getPlanConsumo()==null) || 
             (this.planConsumo!=null &&
              this.planConsumo.equals(other.getPlanConsumo()))) &&
            ((this.planConsumoDescripcion==null && other.getPlanConsumoDescripcion()==null) || 
             (this.planConsumoDescripcion!=null &&
              this.planConsumoDescripcion.equals(other.getPlanConsumoDescripcion()))) &&
            ((this.fechaIngreso==null && other.getFechaIngreso()==null) || 
             (this.fechaIngreso!=null &&
              this.fechaIngreso.equals(other.getFechaIngreso()))) &&
            ((this.fechaAplicada==null && other.getFechaAplicada()==null) || 
             (this.fechaAplicada!=null &&
              this.fechaAplicada.equals(other.getFechaAplicada()))) &&
            ((this.estadoCambioPlan==null && other.getEstadoCambioPlan()==null) || 
             (this.estadoCambioPlan!=null &&
              this.estadoCambioPlan.equals(other.getEstadoCambioPlan()))) &&
            ((this.serieComprobante==null && other.getSerieComprobante()==null) || 
             (this.serieComprobante!=null &&
              this.serieComprobante.equals(other.getSerieComprobante()))) &&
            ((this.tipoComprobante==null && other.getTipoComprobante()==null) || 
             (this.tipoComprobante!=null &&
              this.tipoComprobante.equals(other.getTipoComprobante()))) &&
            ((this.nroComprobante==null && other.getNroComprobante()==null) || 
             (this.nroComprobante!=null &&
              this.nroComprobante.equals(other.getNroComprobante()))) &&
            ((this.usuario==null && other.getUsuario()==null) || 
             (this.usuario!=null &&
              this.usuario.equals(other.getUsuario())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPlanConsumo() != null) {
            _hashCode += getPlanConsumo().hashCode();
        }
        if (getPlanConsumoDescripcion() != null) {
            _hashCode += getPlanConsumoDescripcion().hashCode();
        }
        if (getFechaIngreso() != null) {
            _hashCode += getFechaIngreso().hashCode();
        }
        if (getFechaAplicada() != null) {
            _hashCode += getFechaAplicada().hashCode();
        }
        if (getEstadoCambioPlan() != null) {
            _hashCode += getEstadoCambioPlan().hashCode();
        }
        if (getSerieComprobante() != null) {
            _hashCode += getSerieComprobante().hashCode();
        }
        if (getTipoComprobante() != null) {
            _hashCode += getTipoComprobante().hashCode();
        }
        if (getNroComprobante() != null) {
            _hashCode += getNroComprobante().hashCode();
        }
        if (getUsuario() != null) {
            _hashCode += getUsuario().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CambiosPlan.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "CambiosPlan"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planConsumo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "planConsumo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planConsumoDescripcion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "planConsumoDescripcion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaIngreso");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fechaIngreso"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaAplicada");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fechaAplicada"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estadoCambioPlan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estadoCambioPlan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serieComprobante");
        elemField.setXmlName(new javax.xml.namespace.QName("", "serieComprobante"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoComprobante");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipoComprobante"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nroComprobante");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nroComprobante"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuario");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usuario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
