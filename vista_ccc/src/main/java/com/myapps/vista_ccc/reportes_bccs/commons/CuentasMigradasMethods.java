package com.myapps.vista_ccc.reportes_bccs.commons;

public enum CuentasMigradasMethods {

	WHITE_LIST(1, "pkg_expiry/white_list/"),
	WHITE_ADD(2,"pkg_expiry/add_to_white_list"),
	WHITE_DELETE(3,"pkg_expiry/delete_from_white_list"),
	CUENTA_MIGRADA(4,"cxf/api/validarCuentaMigrada");

	private int codigo;
	private String uri;

	CuentasMigradasMethods(int codigo, String uri) {
		this.codigo = codigo;
		this.uri = uri;
	}

	public int getCodigo() {
		return codigo;
	}

	public String getUri() {
		return uri;
	}

}
