package com.myapps.vista_ccc.reportes_bccs.servicio.consumer;

import com.myapps.vista_ccc.reportes_bccs.bean.AuditoriaResportesBCCS;
import com.myapps.vista_ccc.reportes_bccs.servicio.*;
import com.myapps.vista_ccc.servicio.NodoServicio;
import com.myapps.vista_ccc.util.Parametros;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
public class ResporteBCCSServiceConsumer implements Serializable {

	private static final Logger log = Logger.getLogger(ResporteBCCSServiceConsumer.class);
	private static String url = Parametros.urlServiceReporteBccs;
	private static int timeout = Parametros.timeoutServiceResporteBccs;
	private static String usuario = Parametros.usuarioServicioBCCS;
	private static String password = Parametros.passwordServicioBCCS;
	@Inject
	private AuditoriaResportesBCCS auditoria;

	public synchronized AccountResponse consumirAccountQuery(AccountRequest request, long idAuditoriaHistorial)
			throws Exception {
		NodoServicio<InterfaceWSServiceSoapBindingStub> portNodo = ServicioReporteBCCS.initPort(url, "ReportesBCCS", timeout,
				usuario, password);
		long ini = System.currentTimeMillis();
		AccountResponse response = null;
		if (portNodo != null && portNodo.getPort() != null) {
			synchronized (portNodo) {
				InterfaceWSServiceSoapBindingStub port = portNodo.getPort();
				response = port.accountQuery(request);
				log.info("Request ReportesBCCS QueryExecuteWS accountQuery -> "
						+ port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString());
				log.info("Response ReportesBCCS QueryExecuteWS accountQuery -> "
						+ portNodo.getPort()._getCall().getMessageContext().getResponseMessage().getSOAPPartAsString());
				long fin = System.currentTimeMillis();
				Long Con = null;
				if (portNodo.isPrimeraVez()) {
					Con = portNodo.getTiempoConexion();
				}
				log.debug("[cuenta o ducumento: " + request.getRequestBody().getCuenta()
						+ request.getRequestBody().getDocumento() + "] Tiempo de respuesta del servicio accountQuery: "
						+ (fin - ini) + " milisegundos");
				if (Parametros.saveRequestResponse) {
					try {
						auditoria.saveXml(
								request.getRequestBody().getCuenta() + request.getRequestBody().getDocumento(), usuario,
								url, "accountQuery",
								port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(),
								port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoriaHistorial, Con,
								(fin - ini));
					} catch (Exception e) {
						log.warn("No se logro registrar los request y response del servicio accountQuery: ", e);
					}
				}
			}
		}
		return response;
	}

	public synchronized ChangeStatusLogResponse consumirChangeStatusLogResponse(ChangeStatusLogRequest request,
                                                                                long idAuditoriaHistorial) throws Exception {
		long ini = System.currentTimeMillis();
		NodoServicio<InterfaceWSServiceSoapBindingStub> portNodo = ServicioReporteBCCS.initPort(url, "ReportesBCCS", timeout,
				usuario, password);
		ChangeStatusLogResponse response = null;
		if (portNodo != null && portNodo.getPort() != null) {
			synchronized (portNodo) {
				InterfaceWSServiceSoapBindingStub port = portNodo.getPort();
				response = port.changeStatusLogQuery(request);
				log.info("Request ReportesBCCS QueryExecuteWS changeStatusLogQuery -> "
						+ port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString());
				log.info("Response ReportesBCCS QueryExecuteWS changeStatusLogQuery -> "
						+ port._getCall().getMessageContext().getResponseMessage().getSOAPPartAsString());
				long fin = System.currentTimeMillis();
				Long Con = null;
				if (portNodo.isPrimeraVez()) {
					Con = portNodo.getTiempoConexion();
				}
				log.debug("[cuenta: " + request.getRequestBody().getCuenta()
						+ "] Tiempo de respuesta del servicio changeStatusLogQuery: " + (fin - ini) + " milisegundos");
				if (Parametros.saveRequestResponse) {
					try {
						auditoria.saveXml(request.getRequestBody().getCuenta(), usuario, url, "changeStatusLogQuery",
								port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(),
								port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoriaHistorial, Con,
								(fin - ini));
					} catch (Exception e) {
						log.warn("No se logro registrar los request y response del servicio changeStatusLogQuery: ", e);
					}
				}
			}
		}
		return response;
	}

	public synchronized ClaimByMSISDNResponse consumirClaimResponse(ClaimByMSISDNRequest request, long idAuditoriaHistorial)
			throws Exception {
		long ini = System.currentTimeMillis();
		NodoServicio<InterfaceWSServiceSoapBindingStub> portNodo = ServicioReporteBCCS.initPort(url, "ReportesBCCS", timeout,
				usuario, password);
		ClaimByMSISDNResponse response = null;
		if (portNodo != null && portNodo.getPort() != null) {
			synchronized (portNodo) {
				InterfaceWSServiceSoapBindingStub port = portNodo.getPort();
				response = port.claimQueryByMSISDN(request);
				log.info("Request ReportesBCCS QueryExecuteWS claimQueryByMSISDN -> "
						+ port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString());
				log.info("Response ReportesBCCS QueryExecuteWS claimQueryByMSISDN -> "
						+ port._getCall().getMessageContext().getResponseMessage().getSOAPPartAsString());
				long fin = System.currentTimeMillis();
				Long Con = null;
				if (portNodo.isPrimeraVez()) {
					Con = portNodo.getTiempoConexion();
				}
				log.debug("[cuenta: " + request.getRequestBody().getCuenta()
						+ "] Tiempo de respuesta del servicio claimQueryByMSISDN: " + (fin - ini) + " milisegundos");
				if (Parametros.saveRequestResponse) {
					try {
						auditoria.saveXml(request.getRequestBody().getCuenta(), usuario, url, "claimQueryByMSISDN",
								port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(),
								port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoriaHistorial, Con,
								(fin - ini));
					} catch (Exception e) {
						log.warn("No se logro registrar los request y response del servicio claimQueryByMSISDN: ", e);
					}
				}
			}
		}
		return response;

	}

	public synchronized ClaimByNroMEMOResponse consumirClaimResponseByMemo(ClaimByNroMEMORequest request, long idAuditoriaHistorial)
			throws Exception {
		long ini = System.currentTimeMillis();
		NodoServicio<InterfaceWSServiceSoapBindingStub> portNodo = ServicioReporteBCCS.initPort(url, "ReportesBCCS", timeout,
				usuario, password);
		ClaimByNroMEMOResponse response = null;
		if (portNodo != null && portNodo.getPort() != null) {
			synchronized (portNodo) {
				InterfaceWSServiceSoapBindingStub port = portNodo.getPort();
				response = port.claimQueryByNroMEMO(request);
				log.info("Request ReportesBCCS QueryExecuteWS claimQueryByMemo -> "
						+ port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString());
				log.info("Response ReportesBCCS QueryExecuteWS claimQueryByMemo -> "
						+ port._getCall().getMessageContext().getResponseMessage().getSOAPPartAsString());
				long fin = System.currentTimeMillis();
				Long Con = null;
				if (portNodo.isPrimeraVez()) {
					Con = portNodo.getTiempoConexion();
				}
				log.debug("[memo: " + request.getRequestBody().getMemo()
						+ "] Tiempo de respuesta del servicio claimQueryByMemo: " + (fin - ini) + " milisegundos");
				if (Parametros.saveRequestResponse) {
					try {
						auditoria.saveXml(request.getRequestBody().getMemo(), usuario, url, "claimQueryByMemo",
								port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(),
								port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoriaHistorial, Con,
								(fin - ini));
					} catch (Exception e) {
						log.warn("No se logro registrar los request y response del servicio claimQueryByMemo: ", e);
					}
				}
			}
		}
		return response;

	}

	public synchronized DeviceResponse consumirDeviceResponse(DeviceRequest request, long idAuditoriaHistorial)
			throws Exception {
		long ini = System.currentTimeMillis();
		NodoServicio<InterfaceWSServiceSoapBindingStub> portNodo = ServicioReporteBCCS.initPort(url, "ReportesBCCS", timeout,
				usuario, password);
		DeviceResponse response = null;
		if (portNodo != null && portNodo.getPort() != null) {
			synchronized (portNodo) {
				InterfaceWSServiceSoapBindingStub port = portNodo.getPort();
				response = port.deviceQuery(request);
				log.info("Request ReportesBCCS QueryExecuteWS deviceQuery -> "
						+ port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString());
				log.info("Response ReportesBCCS QueryExecuteWS deviceQuery -> "
						+ port._getCall().getMessageContext().getResponseMessage().getSOAPPartAsString());
				long fin = System.currentTimeMillis();
				Long Con = null;
				if (portNodo.isPrimeraVez()) {
					Con = portNodo.getTiempoConexion();
				}
				log.debug("[imei: " + request.getRequestBody().getImei()
						+ "] Tiempo de respuesta del servicio deviceQuery: " + (fin - ini) + " milisegundos");
				if (Parametros.saveRequestResponse) {
					try {
						auditoria.saveXml(request.getRequestBody().getImei(), usuario, url, "deviceQuery",
								port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(),
								port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoriaHistorial, Con,
								(fin - ini));
					} catch (Exception e) {
						log.warn("No se logro registrar los request y response del servicio deviceQuery: ", e);
					}
				}
			}
		}
		return response;
	}

	public synchronized ConsumerPlanResponse consumirConsumerPlanResponse(ConsumerPlanRequest request,
                                                                          long idAuditoriaHistorial) throws Exception {
		long ini = System.currentTimeMillis();
		NodoServicio<InterfaceWSServiceSoapBindingStub> portNodo = ServicioReporteBCCS.initPort(url, "ReportesBCCS", timeout,
				usuario, password);
		ConsumerPlanResponse response = null;
		if (portNodo != null && portNodo.getPort() != null) {
			synchronized (portNodo) {
				InterfaceWSServiceSoapBindingStub port = portNodo.getPort();
				response = port.consumerPlanQuery(request);
				log.info("Request ReportesBCCS QueryExecuteWS consumerPlanQuery -> "
						+ port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString());
				log.info("Response ReportesBCCS QueryExecuteWS consumerPlanQuery -> "
						+ port._getCall().getMessageContext().getResponseMessage().getSOAPPartAsString());
				long fin = System.currentTimeMillis();
				Long Con = null;
				if (portNodo.isPrimeraVez()) {
					Con = portNodo.getTiempoConexion();
				}
				log.debug("[cuenta: " + request.getRequestBody().getCuenta()
						+ "] Tiempo de respuesta del servicio consumerPlanQuery: " + (fin - ini) + " milisegundos");
				if (Parametros.saveRequestResponse) {
					try {
						auditoria.saveXml(request.getRequestBody().getCuenta(), usuario, url, "consumerPlanQuery",
								port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(),
								port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoriaHistorial, Con,
								(fin - ini));
					} catch (Exception e) {
						log.warn("No se logro registrar los request y response del servicio consumerPlanQuery: ", e);
					}
				}
			}
		}
		return response;
	}

}
