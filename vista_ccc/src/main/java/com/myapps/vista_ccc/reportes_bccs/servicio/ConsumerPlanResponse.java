/**
 * ConsumerPlanResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.myapps.vista_ccc.reportes_bccs.servicio;

public class ConsumerPlanResponse  implements java.io.Serializable {
    private com.myapps.vista_ccc.reportes_bccs.servicio.ResponseHeader responseHeader;

    private com.myapps.vista_ccc.reportes_bccs.servicio.ConsumerPlanResponseBody changeStatusLogResponseBody;

    public ConsumerPlanResponse() {
    }

    public ConsumerPlanResponse(
           com.myapps.vista_ccc.reportes_bccs.servicio.ResponseHeader responseHeader,
           com.myapps.vista_ccc.reportes_bccs.servicio.ConsumerPlanResponseBody changeStatusLogResponseBody) {
           this.responseHeader = responseHeader;
           this.changeStatusLogResponseBody = changeStatusLogResponseBody;
    }


    /**
     * Gets the responseHeader value for this ConsumerPlanResponse.
     * 
     * @return responseHeader
     */
    public com.myapps.vista_ccc.reportes_bccs.servicio.ResponseHeader getResponseHeader() {
        return responseHeader;
    }


    /**
     * Sets the responseHeader value for this ConsumerPlanResponse.
     * 
     * @param responseHeader
     */
    public void setResponseHeader(com.myapps.vista_ccc.reportes_bccs.servicio.ResponseHeader responseHeader) {
        this.responseHeader = responseHeader;
    }


    /**
     * Gets the changeStatusLogResponseBody value for this ConsumerPlanResponse.
     * 
     * @return changeStatusLogResponseBody
     */
    public com.myapps.vista_ccc.reportes_bccs.servicio.ConsumerPlanResponseBody getChangeStatusLogResponseBody() {
        return changeStatusLogResponseBody;
    }


    /**
     * Sets the changeStatusLogResponseBody value for this ConsumerPlanResponse.
     * 
     * @param changeStatusLogResponseBody
     */
    public void setChangeStatusLogResponseBody(com.myapps.vista_ccc.reportes_bccs.servicio.ConsumerPlanResponseBody changeStatusLogResponseBody) {
        this.changeStatusLogResponseBody = changeStatusLogResponseBody;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof ConsumerPlanResponse)) return false;
        ConsumerPlanResponse other = (ConsumerPlanResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.responseHeader==null && other.getResponseHeader()==null) || 
             (this.responseHeader!=null &&
              this.responseHeader.equals(other.getResponseHeader()))) &&
            ((this.changeStatusLogResponseBody==null && other.getChangeStatusLogResponseBody()==null) || 
             (this.changeStatusLogResponseBody!=null &&
              this.changeStatusLogResponseBody.equals(other.getChangeStatusLogResponseBody())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getResponseHeader() != null) {
            _hashCode += getResponseHeader().hashCode();
        }
        if (getChangeStatusLogResponseBody() != null) {
            _hashCode += getChangeStatusLogResponseBody().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ConsumerPlanResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ConsumerPlanResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responseHeader");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ResponseHeader"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ResponseHeader"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("changeStatusLogResponseBody");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ChangeStatusLogResponseBody"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.queryExecute.tigo.com/", "ConsumerPlanResponseBody"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
