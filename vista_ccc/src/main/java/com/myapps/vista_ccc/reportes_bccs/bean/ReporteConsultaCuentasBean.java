package com.myapps.vista_ccc.reportes_bccs.bean;

import com.myapps.user.bean.ControlerBitacora;
import com.myapps.user.ldap.DescriptorBitacora;
import com.myapps.vista_ccc.reportes_bccs.business.ReporteConsultaCuentasBL;
import com.myapps.vista_ccc.reportes_bccs.commons.ConfigRangoFecha;
import com.myapps.vista_ccc.reportes_bccs.entity.ResponseCuentaMigrada;
import com.myapps.vista_ccc.reportes_bccs.error.CuentaMigradaException;
import com.myapps.vista_ccc.reportes_bccs.error.HttpStatusException;
import com.myapps.vista_ccc.reportes_bccs.servicio.Accounts;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.SysMessage;
import com.myapps.vista_ccc.util.UtilNumber;
import com.myapps.vista_ccc.util.UtilUrl;
import org.apache.axis.AxisFault;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.xml.rpc.ServiceException;
import java.io.Serializable;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.rmi.RemoteException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.Date;

@ManagedBean
@ViewScoped
public class ReporteConsultaCuentasBean implements Serializable {
    private static final long serialVersionUID = 1L;
    public static Logger log = Logger.getLogger(ReporteConsultaCuentasBean.class);

    @Inject
    private ReporteConsultaCuentasBL reporteConsultaCuentasBL;
    @Inject
    private ControlerBitacora controlerBitacora;
    @Inject
    private AuditoriaResportesBCCS auditoria;
    private Date fechaInicio;
    private Date fechaFin;
    private String pattern;
    private int rangoDiasConsultaLogs;
    private String msisdn;
    private String nroDocumento;
    private String imei;
    private Accounts[] cuentas;
    private String select;
    private boolean habilitarCuenta = true;
    private boolean habilitarDoc = false;
    private long idAuditoriaHistorial;
    private String ip;
    private transient ResponseCuentaMigrada cuentaMigrada;
    private String estado;

    @PostConstruct
    public void init() {
        log.info(obtenerUsuario() + " ingreso a consulta cuenta de reporte BCCS.");
        rangoDiasConsultaLogs = reporteConsultaCuentasBL
                .traerCantidadDias(ConfigRangoFecha.REPORTE_HISTORICO_CONSULTA_CUENTAS.getCodigo());
        iniciar(rangoDiasConsultaLogs);
        cargarIp();
        estado = Parametros.estadoCuenta;
    }

    private void cargarIp() {
        try {
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                    .getRequest();
            ip = UtilUrl.getClientIp(request);
        } catch (Exception e) {
            log.error("Error al obtener ip: ", e);
        }
    }

    private String obtenerUsuario() {
        String login = "";
        try {
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                    .getRequest();
            login = (String) request.getSession().getAttribute("TEMP$USER_NAME");
            // log.info("login: " + login);
        } catch (Exception e) {
            log.error("Error al obtener usuario: ", e);
        }
        return login;
    }

    public void iniciar(int rangoDias) {
        try {
            pattern = Parametros.formatoFechaReporteBccs;
            LocalDateTime fechaHoy = LocalDateTime.now();
            fechaFin = Date.from(fechaHoy.toInstant(ZoneOffset.UTC));
            fechaHoy = fechaHoy.minusDays(rangoDias);
            fechaInicio = Date.from(fechaHoy.toInstant(ZoneOffset.UTC));
        } catch (Exception e) {
            log.error("Error al iniciar fechas para consulta cuenta, reporte BCCS: ", e);
        }
    }

    // validamos numero de cuenta y numero de documento
    public boolean validarCuenta() {
        boolean validar = false;
        if (msisdn != null && !msisdn.trim().isEmpty() && UtilNumber.esNroTigo(msisdn)) {
            validar = true;
        }
        return validar;
    }

    public boolean validarNroDocumento() {
        boolean validar = false;
        if (nroDocumento != null && !nroDocumento.trim().isEmpty()) {
            validar = true;
        }
        return validar;
    }

    public void consumirCuentaMigrada() {

        cuentaMigrada = null;
        try {
            idAuditoriaHistorial = auditoria.guardarAuditoria(Calendar.getInstance(), obtenerUsuario(), ip,
                    Parametros.vistaReporteBCCS, Parametros.comandoCuentaMigrada, msisdn, null);
            cuentaMigrada = reporteConsultaCuentasBL.obtenerCuentaMigrada(msisdn, estado, obtenerUsuario(), idAuditoriaHistorial);
            SysMessage.info("Consulta exitosa.", null);
        } catch (CuentaMigradaException e) {
            log.info("Respuesta de servicio codigo: " + e.getResponseInfo().getCodigo() + ", mensaje: "
                    + e.getResponseInfo().getMensaje());
            SysMessage.info(e.getResponseInfo().getCodigo() + " : " + e.getResponseInfo().getMensaje(), null);
        } catch (HttpStatusException e) {
            log.error("Excepcion al consumir el servicio de CuentaMigradas " + e.getMessage(), e);
            SysMessage.warn("No se pudo completar la consulta.", null);
        } catch (Exception e) {
            log.error("Error interno de aplicacion " + e.getMessage(), e);
            SysMessage.error("Problema de aplicacion, no se pudo completar la consulta.", null);
        }


        try {
            controlerBitacora.accion(DescriptorBitacora.CONSULTA_CUENTAMIGRADAS,
                    "Se realizó la busqueda de resporte CONSULTA_CUENTAMIGRADAS enviados de linea: " + msisdn);
        } catch (Exception e) {
            log.error("Error al guardar bitacora en el sistema: ", e);
            SysMessage.error("Error al guardar bitacora", null);
        }
    }

    public void cargarReporte() {

            try {
                log.info("Datos para consulta de cuenta, msisdn: " + msisdn + " nroDocumento: " + nroDocumento);
                idAuditoriaHistorial = auditoria.guardarAuditoria(Calendar.getInstance(), obtenerUsuario(), ip,
                        Parametros.vistaReporteBCCS, Parametros.comandoReportesBCCS, msisdn, null);
                cuentas = reporteConsultaCuentasBL.obtenerReporteCuentaEstado(msisdn, select, nroDocumento, fechaInicio,
                        fechaFin, idAuditoriaHistorial);
                log.debug("ingresando a vefificar reporte consulta cuentas para Nro Cuenta: " + this.msisdn
                        + " - Nro Doc: " + this.nroDocumento);
                SysMessage.info("Consulta exitosa.", null);

            } catch (Exception e) {
                tratarMostrarError(e);
            }


        try {
            controlerBitacora.accion(DescriptorBitacora.CONSULTA_CUENTAS,
                    "Se hizo consulta del Nro Cuenta: " + this.msisdn + " - Nro Doc: " + this.nroDocumento);
        } catch (Exception e) {
            log.error("Error al guardar bitacora en el sistema: ", e);
            SysMessage.error("Error al guardar bitacora", null);
        }
    }

    public void cargarReporteCuentaMigrada() {
        if (validarCuenta() && reporteConsultaCuentasBL.validarFecha(fechaInicio, fechaFin, rangoDiasConsultaLogs)) {
            consumirCuentaMigrada();
            //log.info("FechMigracion: "+cuentaMigrada);
            if (cuentaMigrada.getCuentaMigradaResponseOk() != null && cuentaMigrada.getCuentaMigradaResponseOk().getEstado() != null) {
                //log.info("fecha migracion: "+cuentaMigrada.getFechaMigracion());
                cargarReporte();
            } else if (cuentaMigrada.getResponseError().getErrors() != null) {
                cuentas = null;
                cuentaMigrada = null;
                SysMessage.warn("La cuenta no ha sido migrada", null);
                /*for (Error valor : cuentaMigrada.getResponseError().getErrors()) {
                    SysMessage.warn(valor.getDetail(), null);
                }*/
            }
        }else{
            if(validarNroDocumento() && reporteConsultaCuentasBL.validarFecha(fechaInicio, fechaFin, rangoDiasConsultaLogs)){
                cuentaMigrada = null;
                cargarReporte();
            }else{
                SysMessage.warn("Debe ingresar Nro de cuenta o Numero. de documento.", null);
            }
        }

        try {
            controlerBitacora.accion(DescriptorBitacora.CONSULTA_CUENTAS,
                    "Se hizo consulta del Nro Cuenta: " + this.msisdn + " - Nro Doc: " + this.nroDocumento);
        } catch (Exception e) {
            log.error("Error al guardar bitacora en el sistema: ", e);
            SysMessage.error("Error al guardar bitacora", null);
        }
    }

    public void tratarMostrarError(Exception e) {
        if (e.getCause() instanceof AxisFault) {
            String msg = e.getMessage();
            msg = msg.toLowerCase();
            if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase())
                    || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase())
                    || msg.contains("UnknownHostException".toLowerCase())
                    || msg.contains("No route to host".toLowerCase())
                    || msg.contains("Connection refused".toLowerCase())) {
                log.error("Error de conexion al servicio Reporte BCCS accountQuery: ", e);
                SysMessage.error("Error de conexion del servicio Reporte BCCS accountQuery", null);
            } else {
                log.error("Error AxisFault accountQuery: ", e);
                SysMessage.error("Error AxisFault de conexion al servicio Reporte BCCS accountQuery", null);
            }
        } else if (e.getCause() instanceof SocketTimeoutException) {
            log.error("Error de timeout al conectarse con el servicio Reporte BCCS accountQuery: ", e);
            SysMessage.error("Tiempo de espera para el servicio Reporte BCCS accountQuery agotado.", null);
        } else if (e.getCause() instanceof ServiceException || e.getCause() instanceof ConnectException) {
            log.error("Error al conectarse con el servicio Reporte BCCS accountQuery: ", e);
            SysMessage.error("No se pudo conectar con el servicio Reporte BCCS accountQuery", null);
        } else if (e.getCause() instanceof RemoteException) {
            log.error("Error remoto al conectarse al servicio Reporte BCCS accountQuery: ", e);
            SysMessage.error("Error remoto al conectarse al servicio Reporte BCCS accountQuery", null);
        } else {
            SysMessage.error("Error interno de sistema", null);
            log.error("Error al obtener reporte: ", e);
        }
    }

    public void limpiar() {
        iniciar(rangoDiasConsultaLogs);
        cuentas = null;
        msisdn = "";
        nroDocumento = "";
        imei = "";

    }

    public void limpiarNroDoc() {
//		((UIOutput) event.getSource()).setValue("");
        nroDocumento = "";
//		habilitarCuenta = true;
//		habilitarDoc = false;
    }

    public void limpiarNroCuenta() {
//		((UIOutput) event.getSource()).setValue("");
        msisdn = "";
//		habilitarCuenta = false;
//		habilitarDoc = true;
    }

    public ResponseCuentaMigrada getCuentaMigrada() {
        return cuentaMigrada;
    }

    public void setCuentaMigrada(ResponseCuentaMigrada cuentaMigrada) {
        this.cuentaMigrada = cuentaMigrada;
    }

    public boolean isHabilitarCuenta() {
        return habilitarCuenta;
    }

    public void setHabilitarCuenta(boolean habilitarCuenta) {
        this.habilitarCuenta = habilitarCuenta;
    }

    public boolean isHabilitarDoc() {
        return habilitarDoc;
    }

    public void setHabilitarDoc(boolean habilitarDoc) {
        this.habilitarDoc = habilitarDoc;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public Accounts[] getCuentas() {
        return cuentas;
    }

    public void setCuentas(Accounts[] cuentas) {
        this.cuentas = cuentas;
    }

    public String getSelect() {
        return select;
    }

    public void setSelect(String select) {
        this.select = select;
    }
}
