package com.myapps.vista_ccc.reportes_bccs.commons;

/**
 * Created by michael on 12/4/2019.
 */
public enum ConfigRangoFecha {
    REPORTE_HISTORICO_CONSULTA_CUENTAS(1, "REPORTE_HISTORICO_CONSULTA_CUENTAS"),
    REPORTE_HISTORICO_CAMBIO_ESTADO_LINEA(2,"REPORTE_HISTORICO_CAMBIO_ESTADO_LINEA"),
    REPORTE_HISTORICO_CAMBIO_PLAN(3,"REPORTE_HISTORICO_CAMBIO_PLAN"),
    REPORTE_HISTORICO_CARGA_ACMOS(4,"REPORTE_HISTORICO_CARGA_ACMOS"),
    REPORTE_HISTORICO_CONSULTA_EQUIPOS(5, "REPORTE_HISTORICO_CONSULTA_EQUIPOS");

    private int codigo;
    private String nombreReporte;

    ConfigRangoFecha(int codigo, String nombreReporte) {
        this.codigo = codigo;
        this.nombreReporte = nombreReporte;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getNombreReporte() {
        return nombreReporte;
    }
}
