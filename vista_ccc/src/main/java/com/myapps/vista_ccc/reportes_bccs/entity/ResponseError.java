
package com.myapps.vista_ccc.reportes_bccs.entity;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

public class ResponseError implements Serializable
{

    @SerializedName("errors")
    @Expose
    private List<Error> errors = null;
    private final static long serialVersionUID = 966805205849293165L;

    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }

    public static List<ResponseError> getInstanceListByJson(String json) {
        Type listType = new TypeToken<List<ResponseError>>() {
        }.getType();
        Gson gson = new Gson();
        return gson.fromJson(json, listType);
    }

    public static ResponseError getInstanceByJson(String stringJson) {
        Gson gson = new Gson();
        return gson.fromJson(stringJson, ResponseError.class);
    }

}
