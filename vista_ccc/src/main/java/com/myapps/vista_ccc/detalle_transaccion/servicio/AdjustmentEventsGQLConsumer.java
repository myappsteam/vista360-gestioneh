package com.myapps.vista_ccc.detalle_transaccion.servicio;

import com.myapps.vista_ccc.detalle_transaccion.commons.AuditoriaDetalleTransaccion;
import com.myapps.vista_ccc.detalle_transaccion.commons.StatusResponse;
import com.myapps.vista_ccc.detalle_transaccion.error.HttpStatusAcceptException;
import com.myapps.vista_ccc.detalle_transaccion.error.HttpStatusException;
import com.myapps.vista_ccc.util.Parametros;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.net.ConnectException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

@Named
public class AdjustmentEventsGQLConsumer implements Serializable {

    @Inject
    private ApacheHttpClientRestServiceDetalle clientRest;
    @Inject
    private AuditoriaDetalleTransaccion auditoria;

    private static Logger logger = Logger.getLogger(TargetIdGQLConsumer.class);

    public String consumerAdjustmentEvents(String msisdn, String request, String usuario,
                                           long idAuditoriaHistorial) throws HttpStatusException, IOException, HttpStatusAcceptException {
        long ini = System.currentTimeMillis();
        logger.info("Request Json: " + request);
        String output = "";
        String urlConsumer = Parametros.urlDetalleTransaccionService;
        logger.info("Url service: " + urlConsumer);
        try {
            output = clientRest.httpClientRest( request, urlConsumer, Parametros.timeOutServiceDetalleTransaccion);
            logger.info("Response Adjustment Json: " + output);
            long fin = System.currentTimeMillis();
            logger.info(
                    "[Servicio QGL: " + urlConsumer + "] Tiempo de respuesta: " + (fin - ini) + " milisegundos");
            if (Parametros.saveRequestResponse) {
                try {
                    auditoria.saveXml(msisdn, usuario, urlConsumer, "Adjustment", request, output,
                            idAuditoriaHistorial, (fin - ini), (fin - ini));
                } catch (Exception e) {
                    logger.warn("No se logro registrar los request y response del servicio QGL Adjustment: ", e);
                }
            }

        } catch (ConnectException e) {
            logger.error("No se pudo establecer una conexion con el servicio QGL Adjustment " + e.getMessage(), e);
            throw new HttpStatusException(StatusResponse.HTTP_NOT_FOUND.getDescripcion(),
                    StatusResponse.HTTP_NOT_FOUND);

        } catch (ConnectTimeoutException e) {
            logger.error("Tiempo de espera agotado para respuesta de servicio Adjustment " + urlConsumer + " "
                    + e.getMessage(), e);
            throw new HttpStatusException(StatusResponse.HTTP_ERROR.getDescripcion() + ", Causa: " + e.getMessage(),
                    StatusResponse.HTTP_ERROR);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        return output;
    }

}
