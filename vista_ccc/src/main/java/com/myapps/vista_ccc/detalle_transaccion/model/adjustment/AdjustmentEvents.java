
package com.myapps.vista_ccc.detalle_transaccion.model.adjustment;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

public class AdjustmentEvents implements Serializable
{

    @SerializedName("data")
    @Expose
    private Data data;
    private final static long serialVersionUID = 6387376070919767240L;

    public static AdjustmentEvents convertJsonAdjustment(String stringJson) {
        Gson gson = new Gson();
        return gson.fromJson(stringJson, AdjustmentEvents.class);
    }

    public static List<AdjustmentEvents> convertJsonToListAdjustment(String stringJson) {
        Type listType = new TypeToken<List<AdjustmentEvents>>() {
        }.getType();
        Gson gson = new Gson();
        return gson.fromJson(stringJson, listType);
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
