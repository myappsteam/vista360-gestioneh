package com.myapps.vista_ccc.detalle_transaccion.model.resources;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

public class ResponseResources implements Serializable {
    private data data;

    public static ResponseResources convertJsonTargetID(String stringJson) {
        Gson gson = new Gson();
        return gson.fromJson(stringJson, ResponseResources.class);
    }

    public static List<ResponseResources> convertJsonToListTargetID(String stringJson) {
        Type listType = new TypeToken<List<ResponseResources>>() {
        }.getType();
        Gson gson = new Gson();
        return gson.fromJson(stringJson, listType);
    }

    public data getData() {
        return data;
    }

    public void setData(data data) {
        this.data = data;
    }
}
