package com.myapps.vista_ccc.detalle_transaccion.bean;

import com.myapps.user.bean.ControlerBitacora;
import com.myapps.user.ldap.DescriptorBitacora;
import com.myapps.vista_ccc.detalle_transaccion.business.AdjustmentEventsBussines;
import com.myapps.vista_ccc.detalle_transaccion.business.DetalleTransaccionBussines;
import com.myapps.vista_ccc.detalle_transaccion.business.RechargeEventsBussines;
import com.myapps.vista_ccc.detalle_transaccion.business.UsageEventsBussines;
import com.myapps.vista_ccc.detalle_transaccion.commons.AuditoriaDetalleTransaccion;
import com.myapps.vista_ccc.detalle_transaccion.error.DetalleTransaccionException;
import com.myapps.vista_ccc.detalle_transaccion.error.HttpStatusException;
import com.myapps.vista_ccc.detalle_transaccion.model.DetalleTransaccion;
import com.myapps.vista_ccc.detalle_transaccion.model.adjustment.AdjustmentEvents;
import com.myapps.vista_ccc.detalle_transaccion.model.recharge.RechargeEvents;
import com.myapps.vista_ccc.detalle_transaccion.model.resources.ResponseResources;
import com.myapps.vista_ccc.detalle_transaccion.model.usage.Usage;
import com.myapps.vista_ccc.util.*;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by michael on 15/8/2019.
 */
@ManagedBean
@ViewScoped
public class DetalleTransaccionBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private static Logger log = Logger.getLogger(DetalleTransaccionBean.class);
    @Inject
    private DetalleTransaccionBussines detalleTransaccionBussines;
    @Inject
    private UsageEventsBussines usageEventsBussines;
    @Inject
    private RechargeEventsBussines rechargeEventsBussines;
    @Inject
    private AdjustmentEventsBussines adjustmentEventsBussines;
    @Inject
    private ControlerBitacora controlerBitacora;
    @Inject
    private AuditoriaDetalleTransaccion auditoria;


    private String login;
    private String ip;
    private String msisdn;
    private String patternFecha;
    private String patternFechaUTC;
    private long idAuditoriaHistorial;
    //private transient List<ResponseResources> detalleTransaccionList;
    private transient ResponseResources token;
    private transient Usage usageEvents;
    private transient RechargeEvents rechargeEvents;
    private transient AdjustmentEvents adjustmentEvents;
    private transient List<DetalleTransaccion> lstDetalleTransaccion;
    private Date startDate;
    private Date endDate;
    private String timeZone;
    List<DetalleTransaccion> sortedList;

    @PostConstruct
    public void init(){
        sortedList = new ArrayList<>();
        patternFecha = Parametros.patternFechaDetalleTransaccion;
        patternFechaUTC = "yyyy-MM-dd'T'HH:mm:ss.SSS";
        obtenerUsuario();
        cargarIp();
        //log.info("fecha en UTC: "+patternFecha);
    }

    private void obtenerUsuario() {
        try {
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                    .getRequest();
            login = (String) request.getSession().getAttribute("TEMP$USER_NAME");
            log.info("Usuario: " + login + ", ingresando a vista de detalle transaccion.");
        } catch (Exception e) {
            log.error("Error al obtener usuario: ", e);
        }
    }

    public void remoteAction() {
        timeZone = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap().get("nameParamImei");
        //log.info("Esta es la zona horaria: "+timeZone);
    }

    private void cargarIp() {
        try {
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                    .getRequest();
            ip = UtilUrl.getClientIp(request);
        } catch (Exception e) {
            log.error("Error al obtener ip: ", e);
        }
    }

    public boolean validarIsdn() {
        boolean validar = false;
        if (msisdn != null && !msisdn.trim().isEmpty() && UtilNumber.esNroTigo(msisdn))
            validar = true;
        else
            SysMessage.warn("Número de teléfono no válido", null);
        return validar;
    }

    public void listarDetalleTransaccion() {
        if (validarIsdn()) {
            token = null;
            lstDetalleTransaccion = new ArrayList<>();
            try {
                idAuditoriaHistorial = auditoria.guardarAuditoria(Calendar.getInstance(), login, ip, Parametros.vistaDetalleTransaccion, Parametros.comandoDetalleTransaccion, msisdn, null);
                token = detalleTransaccionBussines.obtenerDetalleTransaccion(msisdn, login, idAuditoriaHistorial);
                if(token != null && !token.getData().getResources().getEdges().isEmpty()){
                    String targetID = token.getData().getResources().getEdges().get(0).getNodes().getRealizedProduct().getTargetId();
                    consultarUsageEvents(msisdn,targetID);
                    consultarRechargeEvents(msisdn,targetID);
                    consultarAdjustmentEvents(msisdn,targetID);
                }

                cargarListaDetalle();
                SysMessage.info("Consulta exitosa.", null);
            } catch (DetalleTransaccionException e) {
                log.info("Respuesta de servicio codigo: " + e.getResponseInfo().getCodigo() + ", mensaje: "
                        + e.getResponseInfo().getMensaje());
                SysMessage.info(e.getResponseInfo().getCodigo() + " : " + e.getResponseInfo().getMensaje(), null);
            } catch (HttpStatusException e) {
                log.error("Excepcion al consumir el servicio de targetID " + e.getMessage(), e);
                SysMessage.warn("No se pudo completar la consulta.", null);
            } catch (Exception e) {
                log.error("Error interno de aplicacion " + e.getMessage(), e);
                SysMessage.error("Problema de aplicacion, no se pudo completar la consulta.", null);
            }
        }

        try {
            controlerBitacora.accion(DescriptorBitacora.RESOURCES,
                    "Se realizó la busqueda de detalle transaccion enviados de linea: " + msisdn);
        } catch (Exception e) {
            log.error("Error al guardar bitacora en el sistema: ", e);
            SysMessage.error("Error al guardar bitacora", null);
        }
    }

    public void consultarUsageEvents(String msisdn, String targetID) {
        //if (validarIsdn() && detalleTransaccionBussines.validarFecha(startDate, endDate)) {
        usageEvents = null;
        try {
            //idAuditoriaHistorial = auditoria.guardarAuditoria(Calendar.getInstance(), login, ip, Parametros.vistaDetalleTransaccion, Parametros.comandoDetalleTransaccion, msisdn, null);
            usageEvents = usageEventsBussines.obtenerUsageEvents(msisdn,targetID, login, idAuditoriaHistorial);
            SysMessage.info("Consulta exitosa.", null);
        } catch (DetalleTransaccionException e) {
            log.info("Respuesta de servicio codigo: " + e.getResponseInfo().getCodigo() + ", mensaje: "
                    + e.getResponseInfo().getMensaje());
            SysMessage.info(e.getResponseInfo().getCodigo() + " : " + e.getResponseInfo().getMensaje(), null);
        } catch (HttpStatusException e) {
            log.error("Excepcion al consumir el servicio de usage " + e.getMessage(), e);
            SysMessage.warn("No se pudo completar la consulta.", null);
        } catch (Exception e) {
            log.error("Error interno de aplicacion " + e.getMessage(), e);
            SysMessage.error("Problema de aplicacion, no se pudo completar la consulta.", null);
        }
        //}

        try {
            controlerBitacora.accion(DescriptorBitacora.USAGES,
                    "Se realizó la busqueda de detalle transaccion enviados de linea: " + msisdn);
        } catch (Exception e) {
            log.error("Error al guardar bitacora en el sistema: ", e);
            SysMessage.error("Error al guardar bitacora", null);
        }
    }

    public void consultarRechargeEvents(String msisdn, String targetID) {
        //if (validarIsdn() && detalleTransaccionBussines.validarFecha(startDate, endDate)) {
        rechargeEvents = null;
        try {
            //idAuditoriaHistorial = auditoria.guardarAuditoria(Calendar.getInstance(), login, ip, Parametros.vistaDetalleTransaccion, Parametros.comandoDetalleTransaccion, msisdn, null);
            rechargeEvents = rechargeEventsBussines.obtenerRechargeEvents(msisdn,targetID, login, idAuditoriaHistorial);
            SysMessage.info("Consulta exitosa.", null);
        } catch (DetalleTransaccionException e) {
            log.info("Respuesta de servicio codigo: " + e.getResponseInfo().getCodigo() + ", mensaje: "
                    + e.getResponseInfo().getMensaje());
            SysMessage.info(e.getResponseInfo().getCodigo() + " : " + e.getResponseInfo().getMensaje(), null);
        } catch (HttpStatusException e) {
            log.error("Excepcion al consumir el servicio de usage " + e.getMessage(), e);
            SysMessage.warn("No se pudo completar la consulta.", null);
        } catch (Exception e) {
            log.error("Error interno de aplicacion " + e.getMessage(), e);
            SysMessage.error("Problema de aplicacion, no se pudo completar la consulta.", null);
        }
        //}

        try {
            controlerBitacora.accion(DescriptorBitacora.RECHARGES,
                    "Se realizó la busqueda de detalle transaccion enviados de linea: " + msisdn);
        } catch (Exception e) {
            log.error("Error al guardar bitacora en el sistema: ", e);
            SysMessage.error("Error al guardar bitacora", null);
        }
    }

    public void consultarAdjustmentEvents(String msisdn, String targetID) {
        //if (validarIsdn() && detalleTransaccionBussines.validarFecha(startDate, endDate)) {
        adjustmentEvents = null;
        try {
            //idAuditoriaHistorial = auditoria.guardarAuditoria(Calendar.getInstance(), login, ip, Parametros.vistaDetalleTransaccion, Parametros.comandoDetalleTransaccion, msisdn, null);
            adjustmentEvents = adjustmentEventsBussines.obtenerAdjustmentEvents(msisdn, targetID, login, idAuditoriaHistorial);
            SysMessage.info("Consulta exitosa.", null);
        } catch (DetalleTransaccionException e) {
            log.info("Respuesta de servicio codigo: " + e.getResponseInfo().getCodigo() + ", mensaje: "
                    + e.getResponseInfo().getMensaje());
            SysMessage.info(e.getResponseInfo().getCodigo() + " : " + e.getResponseInfo().getMensaje(), null);
        } catch (HttpStatusException e) {
            log.error("Excepcion al consumir el servicio de usage " + e.getMessage(), e);
            SysMessage.warn("No se pudo completar la consulta.", null);
        } catch (Exception e) {
            log.error("Error interno de aplicacion " + e.getMessage(), e);
            SysMessage.error("Problema de aplicacion, no se pudo completar la consulta.", null);
        }
        //}

        try {
            controlerBitacora.accion(DescriptorBitacora.ADJUSTMENT,
                    "Se realizó la busqueda de detalle transaccion enviados de linea: " + msisdn);
        } catch (Exception e) {
            log.error("Error al guardar bitacora en el sistema: ", e);
            SysMessage.error("Error al guardar bitacora", null);
        }
    }

    public void cargarListaDetalle(){
        if(usageEvents !=null && usageEvents.getData() != null && !usageEvents.getData().getUsageEvents().getEdges().isEmpty()) {
            for (com.myapps.vista_ccc.detalle_transaccion.model.usage.Edge edge : usageEvents.getData().getUsageEvents().getEdges()) {
                if (edge.getNode() != null && edge.getNode().getDetailedCharges() != null && !edge.getNode().getDetailedCharges().getEdges().isEmpty()) {
                    for (com.myapps.vista_ccc.detalle_transaccion.model.usage.Edge__ edgeDetailedCharges : edge.getNode().getDetailedCharges().getEdges()) {
                        if (edgeDetailedCharges.getNode() != null && edgeDetailedCharges.getNode().getChargingBalanceChanges() != null
                                && !edgeDetailedCharges.getNode().getChargingBalanceChanges().getEdges().isEmpty()) {
                            for (com.myapps.vista_ccc.detalle_transaccion.model.usage.Edge___ edgeChargingBalanceChange : edgeDetailedCharges.getNode().getChargingBalanceChanges().getEdges()) {
                                if (edgeChargingBalanceChange.getNode() != null && edgeChargingBalanceChange.getNode().getTarget() != null
                                        && edgeChargingBalanceChange.getNode().getTarget().getSpecification() != null && edgeChargingBalanceChange.getNode().getTarget().getSpecification().getName() != null
                                        && edgeChargingBalanceChange.getNode().getTarget().getSpecification().getName().equals(Parametros.DetalleEspecification)) {
                                    DetalleTransaccion detalleTransaccion = new DetalleTransaccion();
                                    detalleTransaccion.setFechaHora(UtilDate.UTCToDate(edgeChargingBalanceChange.getNode().getChangedAt(),patternFechaUTC));
                                    detalleTransaccion.setTipoTransaccion(edge.getNode().getEventType());
                                    detalleTransaccion.setMonto(UtilNumber.redondear(edgeChargingBalanceChange.getNode().getChange().doubleValue(), Parametros.DetalleNroDecimales));
                                    lstDetalleTransaccion.add(detalleTransaccion);
                                }
                            }
                        }
                    }
                }
            }
        }
        if(rechargeEvents !=null && rechargeEvents.getData() != null && !rechargeEvents.getData().getRechargeEvents().getEdges().isEmpty()) {
            for (com.myapps.vista_ccc.detalle_transaccion.model.recharge.Edge edge : rechargeEvents.getData().getRechargeEvents().getEdges()) {
                //verificamos si bonus o recharge contienen Saldo Principal
                if (edge.getNode() != null && edge.getNode().getBonusEvent() != null && edge.getNode().getBonusEvent().getChargingBalanceChanges() != null
                        && !edge.getNode().getBonusEvent().getChargingBalanceChanges().getEdges().isEmpty()) {
                    for (com.myapps.vista_ccc.detalle_transaccion.model.recharge.Edge___ edgeBonusEventChargingBalanceChange : edge.getNode().getBonusEvent().getChargingBalanceChanges().getEdges()) {
                        if (edgeBonusEventChargingBalanceChange.getNode() != null && edgeBonusEventChargingBalanceChange.getNode().getTarget() != null
                                && edgeBonusEventChargingBalanceChange.getNode().getTarget().getSpecification() != null
                                && edgeBonusEventChargingBalanceChange.getNode().getTarget().getSpecification().getName() != null
                                && edgeBonusEventChargingBalanceChange.getNode().getTarget().getSpecification().getName().equals(Parametros.DetalleEspecification)) {
                            DetalleTransaccion detalleTransaccion = new DetalleTransaccion();
                            detalleTransaccion.setFechaHora(UtilDate.UTCToDate(edgeBonusEventChargingBalanceChange.getNode().getChangedAt(),patternFechaUTC));
                            detalleTransaccion.setTipoTransaccion(edge.getNode().getRechargeType());
                            detalleTransaccion.setMonto(UtilNumber.redondear(edgeBonusEventChargingBalanceChange.getNode().getChange().doubleValue(), Parametros.DetalleNroDecimales));
                            lstDetalleTransaccion.add(detalleTransaccion);
                        }
                    }
                }else{
                    if(edge.getNode() != null && edge.getNode().getRecharges() != null && !edge.getNode().getRecharges().getEdges().isEmpty()){
                        for (com.myapps.vista_ccc.detalle_transaccion.model.recharge.Edge_ edgeRecharges : edge.getNode().getRecharges().getEdges()){
                            if(edgeRecharges.getNode() != null && edgeRecharges.getNode().getChargingBalanceChanges() != null
                                    && !edgeRecharges.getNode().getChargingBalanceChanges().getEdges().isEmpty()){
                                    for(com.myapps.vista_ccc.detalle_transaccion.model.recharge.Edge__ edgeRechargesChargingBalanceChanges : edgeRecharges.getNode().getChargingBalanceChanges().getEdges()){
                                        if(edgeRechargesChargingBalanceChanges.getNode() != null && edgeRechargesChargingBalanceChanges.getNode().getTarget() != null
                                                && edgeRechargesChargingBalanceChanges.getNode().getTarget().getSpecification() != null
                                                && edgeRechargesChargingBalanceChanges.getNode().getTarget().getSpecification().getName().equals(Parametros.DetalleEspecification)){
                                            String tipoRecarga = edge.getNode().getRechargeType();
                                            if(tipoRecarga == null || tipoRecarga.trim().isEmpty()){
                                                tipoRecarga = "Recarga";
                                            }else{
                                                tipoRecarga = edge.getNode().getRechargeType();
                                            }
                                            DetalleTransaccion detalleTransaccion = new DetalleTransaccion();
                                            detalleTransaccion.setFechaHora(UtilDate.UTCToDate(edgeRechargesChargingBalanceChanges.getNode().getChangedAt(),patternFechaUTC));
                                            detalleTransaccion.setTipoTransaccion(tipoRecarga);
                                            detalleTransaccion.setMonto(UtilNumber.redondear(edgeRechargesChargingBalanceChanges.getNode().getChange().doubleValue(), Parametros.DetalleNroDecimales));
                                            lstDetalleTransaccion.add(detalleTransaccion);
                                        }
                                    }
                            }
                        }
                    }
                }
            }
        }

        if(adjustmentEvents !=null && adjustmentEvents.getData()!=null && !adjustmentEvents.getData().getAdjustmentEvents().getEdges().isEmpty()){
            for (com.myapps.vista_ccc.detalle_transaccion.model.adjustment.Edge edge: adjustmentEvents.getData().getAdjustmentEvents().getEdges()) {
                if(edge.getNode() != null && edge.getNode().getChargingBalanceChanges() != null && !edge.getNode().getChargingBalanceChanges().getEdges().isEmpty()) {
                    for (com.myapps.vista_ccc.detalle_transaccion.model.adjustment.Edge_ edgeChargingBalanceChanges : edge.getNode().getChargingBalanceChanges().getEdges()) {
                        if (edgeChargingBalanceChanges.getNode() != null && edgeChargingBalanceChanges.getNode().getTarget() != null
                                && edgeChargingBalanceChanges.getNode().getTarget().getSpecification() != null
                                && edgeChargingBalanceChanges.getNode().getTarget().getSpecification().getName().equals(Parametros.DetalleEspecification)) {
                            String reason = edge.getNode().getReason();
                            Double monto = UtilNumber.redondear(edgeChargingBalanceChanges.getNode().getChange().doubleValue(), Parametros.DetalleNroDecimales);
                            if (reason == null || reason.trim().isEmpty()) {
                                reason = "Recarga";
                                if (monto < 0) {
                                    reason = "Debito";
                                }
                            }
                            DetalleTransaccion detalleTransaccion = new DetalleTransaccion();
                            detalleTransaccion.setFechaHora(UtilDate.UTCToDate(edgeChargingBalanceChanges.getNode().getChangedAt(),patternFechaUTC));
                            detalleTransaccion.setTipoTransaccion(reason);
                            detalleTransaccion.setMonto(monto);
                            lstDetalleTransaccion.add(detalleTransaccion);
                        }
                    }
                }
            }
        }
        //lstDetalleTransaccion.sort((o1,o2) -> o2.getFechaHora().compareTo(o1.getFechaHora()));

    }

    public void limpiar() {
        msisdn = "";
    }

    public String getPatternFecha() {
        return patternFecha;
    }

    public void setPatternFecha(String patternFecha) {
        this.patternFecha = patternFecha;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public List<DetalleTransaccion> getLstDetalleTransaccion() {
        return lstDetalleTransaccion;
    }

    public void setLstDetalleTransaccion(List<DetalleTransaccion> lstDetalleTransaccion) {
        this.lstDetalleTransaccion = lstDetalleTransaccion;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public List<DetalleTransaccion> getSortedList() {
        return sortedList;
    }

    public void setSortedList(List<DetalleTransaccion> sortedList) {
        this.sortedList = sortedList;
    }

    /*public static void main(String[] args) throws ParseException {

        SimpleDateFormat sdfgmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        sdfgmt.setTimeZone(TimeZone.getTimeZone("GMT"));

        SimpleDateFormat sdfmad = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        sdfmad.setTimeZone(TimeZone.getTimeZone("America/La_Paz"));

        String inpt = "2019-09-26T00:08:49.000Z";
        Date inptdate = null;
        try {
            inptdate = sdfgmt.parse(inpt);
        } catch (ParseException e) {e.printStackTrace();}

        System.out.println("GMT:\t\t" + sdfgmt.format(inptdate));
        System.out.println("America/La_Paz:\t" + sdfmad.format(inptdate));
    }*/
}
