
package com.myapps.vista_ccc.detalle_transaccion.model.usage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Node__ implements Serializable
{

    @SerializedName("chargingBalanceChanges")
    @Expose
    private ChargingBalanceChanges chargingBalanceChanges;
    private final static long serialVersionUID = -8802249652189482519L;

    public ChargingBalanceChanges getChargingBalanceChanges() {
        return chargingBalanceChanges;
    }

    public void setChargingBalanceChanges(ChargingBalanceChanges chargingBalanceChanges) {
        this.chargingBalanceChanges = chargingBalanceChanges;
    }

}
