
package com.myapps.vista_ccc.detalle_transaccion.model.usage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.math.BigDecimal;

public class Node___ implements Serializable
{

    @SerializedName("changedAt")
    @Expose
    private String changedAt;
    @SerializedName("change")
    @Expose
    private BigDecimal change;
    @SerializedName("Saldo")
    @Expose
    private BigDecimal saldo;
    @SerializedName("target")
    @Expose
    private Target target;
    private final static long serialVersionUID = 5231516800758395026L;

    public String getChangedAt() {
        return changedAt;
    }

    public void setChangedAt(String changedAt) {
        this.changedAt = changedAt;
    }

    public BigDecimal getChange() {
        return change;
    }

    public void setChange(BigDecimal change) {
        this.change = change;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public Target getTarget() {
        return target;
    }

    public void setTarget(Target target) {
        this.target = target;
    }

}
