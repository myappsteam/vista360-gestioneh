
package com.myapps.vista_ccc.detalle_transaccion.model.usage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Node implements Serializable
{

    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("eventType")
    @Expose
    private String eventType;
    @SerializedName("eventDetails")
    @Expose
    private EventDetails eventDetails;
    @SerializedName("usedServiceUnits")
    @Expose
    private UsedServiceUnits usedServiceUnits;
    @SerializedName("totalChargedAmount")
    @Expose
    private TotalChargedAmount totalChargedAmount;
    @SerializedName("detailedCharges")
    @Expose
    private DetailedCharges detailedCharges;
    private final static long serialVersionUID = -2056104786808306577L;

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public EventDetails getEventDetails() {
        return eventDetails;
    }

    public void setEventDetails(EventDetails eventDetails) {
        this.eventDetails = eventDetails;
    }

    public UsedServiceUnits getUsedServiceUnits() {
        return usedServiceUnits;
    }

    public void setUsedServiceUnits(UsedServiceUnits usedServiceUnits) {
        this.usedServiceUnits = usedServiceUnits;
    }

    public TotalChargedAmount getTotalChargedAmount() {
        return totalChargedAmount;
    }

    public void setTotalChargedAmount(TotalChargedAmount totalChargedAmount) {
        this.totalChargedAmount = totalChargedAmount;
    }

    public DetailedCharges getDetailedCharges() {
        return detailedCharges;
    }

    public void setDetailedCharges(DetailedCharges detailedCharges) {
        this.detailedCharges = detailedCharges;
    }

}
