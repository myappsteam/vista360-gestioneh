
package com.myapps.vista_ccc.detalle_transaccion.model.recharge;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Edge implements Serializable
{

    @SerializedName("node")
    @Expose
    private Node node;
    private final static long serialVersionUID = -3984800693029696104L;

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

}
