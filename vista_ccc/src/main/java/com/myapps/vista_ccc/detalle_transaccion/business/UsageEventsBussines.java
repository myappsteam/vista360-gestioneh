package com.myapps.vista_ccc.detalle_transaccion.business;

import com.myapps.vista_ccc.detalle_transaccion.error.DetalleTransaccionException;
import com.myapps.vista_ccc.detalle_transaccion.error.HttpStatusAcceptException;
import com.myapps.vista_ccc.detalle_transaccion.error.HttpStatusException;
import com.myapps.vista_ccc.detalle_transaccion.model.DetalleTransaccionResponseInfo;
import com.myapps.vista_ccc.detalle_transaccion.model.usage.Usage;
import com.myapps.vista_ccc.detalle_transaccion.servicio.UsageEventsGQLConsumer;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.UtilDate;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by michael on 19/8/2019.
 */
public class UsageEventsBussines implements Serializable {
    private static final long serialVersionUID = 1L;
    private static Logger logger = Logger.getLogger(DetalleTransaccionBussines.class);
    @Inject
    private UsageEventsGQLConsumer consumerRest;

    private Date startDate;
    static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.ms'Z'";

    public Usage obtenerUsageEvents(String msisdn, String targetID, String usuario,
                                    long idAuditoriaHistoria) throws HttpStatusException, IOException, DetalleTransaccionException {
        startDate = UtilDate.sumarRestarDiasFecha(new Date(),-Parametros.detalleRangoDiasConsultaLogs);

        /*logger.info("fecha inicio con hora 0: "+UtilDate.setHoraInicio(startDate));
        logger.info("fecha fin con hora 23.59: "+UtilDate.setHoraFin(new Date()));*/

        String fechaInicio = UtilDate.dateToString(UtilDate.setHoraInicio(startDate),DATE_FORMAT);
        String fechaFin = UtilDate.dateToString(UtilDate.setHoraFin(new Date()),DATE_FORMAT);
        String body = Parametros.queryUsageEventsDetalleTransaccion;
        //String body = "{\"query\":\"query usageEvents {\\n  usageEvents(filter: \\\"(AND (EQ target.id \\\\\\\"#TOKEN#\\\\\\\") (AND (GT created-at \\\\\\\"2019-05-05T04:00:01.000Z\\\\\\\") (LT created-at \\\\\\\"2019-07-31T12:00:00.000Z\\\\\\\")))\\\") {\\n    edges {\\n      node {\\n        createdAt\\n        eventType\\n        eventDetails {\\n          __typename\\n          ... on CallEventDetails {\\n            trafficCase\\n            isRoaming\\n            ownNumber\\n            otherPartyNumber\\n          }\\n          ... on DataEventDetails {\\n            isRoaming\\n          }\\n          ... on ContentEventDetails {\\n            additionalInformation\\n            externalChargeType\\n            characteristics {\\n              entries {\\n                key\\n                value\\n              }\\n            }\\n          }\\n        }\\n        usedServiceUnits {\\n          edges {\\n            node {\\n              amount\\n              unitOfMeasure\\n            }\\n          }\\n        }\\n        totalChargedAmount {\\n          taxFreeAmount {\\n            amount\\n            currency\\n          }\\n        }\\n        detailedCharges {\\n          edges {\\n            node {\\n              chargingBalanceChanges {\\n                edges {\\n                  node {\\n                    changedAt\\n                    change\\n                    Saldo : valueAfter\\n                     target {\\n                      id\\n                      balanceId\\n                      specification {\\n                        name\\n                      }\\n                    }\\n                  }\\n                }\\n              }\\n            }\\n          }\\n        }\\n      }\\n    }\\n  }\\n}\\n\"}";
        String whiteStringJson = "";
        try {
            body = body.replace("#TOKEN#",targetID);
            body = body.replace("#FECHAINICIO#",fechaInicio);
            body = body.replace("#FECHAFIN#",fechaFin);
            whiteStringJson = consumerRest.consumerUsageEvents(msisdn,body, usuario, idAuditoriaHistoria);
        } catch (HttpStatusAcceptException e) {
            logger.info("Respuesta de usageEvents deferente de OK: " + e.getJson());
            throw new DetalleTransaccionException(DetalleTransaccionResponseInfo.getInstanceByJson(e.getJson()));
        }
        return Usage.convertJsonUsage(whiteStringJson);
    }

}
