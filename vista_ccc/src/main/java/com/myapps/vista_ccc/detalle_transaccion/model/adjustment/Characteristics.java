
package com.myapps.vista_ccc.detalle_transaccion.model.adjustment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Characteristics implements Serializable
{

    @SerializedName("entries")
    @Expose
    private List<Entry> entries = null;
    private final static long serialVersionUID = -2458262723627532172L;

    public List<Entry> getEntries() {
        return entries;
    }

    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    }

}
