
package com.myapps.vista_ccc.detalle_transaccion.model.recharge;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Node_ implements Serializable
{

    @SerializedName("chargeCategory")
    @Expose
    private String chargeCategory;
    @SerializedName("additionalInformation")
    @Expose
    private String additionalInformation;
    @SerializedName("chargingBalanceChanges")
    @Expose
    private ChargingBalanceChanges chargingBalanceChanges;
    private final static long serialVersionUID = 5228017034874341145L;

    public String getChargeCategory() {
        return chargeCategory;
    }

    public void setChargeCategory(String chargeCategory) {
        this.chargeCategory = chargeCategory;
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public ChargingBalanceChanges getChargingBalanceChanges() {
        return chargingBalanceChanges;
    }

    public void setChargingBalanceChanges(ChargingBalanceChanges chargingBalanceChanges) {
        this.chargingBalanceChanges = chargingBalanceChanges;
    }

}
