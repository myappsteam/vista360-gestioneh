
package com.myapps.vista_ccc.detalle_transaccion.model.recharge;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Node implements Serializable
{

    @SerializedName("eventStatus")
    @Expose
    private String eventStatus;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("rechargeType")
    @Expose
    private String rechargeType;
    @SerializedName("characteristics")
    @Expose
    private Characteristics characteristics;
    @SerializedName("rechargeAmount")
    @Expose
    private RechargeAmount rechargeAmount;
    @SerializedName("recharges")
    @Expose
    private Recharges recharges;
    @SerializedName("bonusEvent")
    @Expose
    private BonusEvent bonusEvent;
    private final static long serialVersionUID = 4659237173612276952L;

    public String getEventStatus() {
        return eventStatus;
    }

    public void setEventStatus(String eventStatus) {
        this.eventStatus = eventStatus;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getRechargeType() {
        return rechargeType;
    }

    public void setRechargeType(String rechargeType) {
        this.rechargeType = rechargeType;
    }

    public Characteristics getCharacteristics() {
        return characteristics;
    }

    public void setCharacteristics(Characteristics characteristics) {
        this.characteristics = characteristics;
    }

    public RechargeAmount getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(RechargeAmount rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

    public Recharges getRecharges() {
        return recharges;
    }

    public void setRecharges(Recharges recharges) {
        this.recharges = recharges;
    }

    public BonusEvent getBonusEvent() {
        return bonusEvent;
    }

    public void setBonusEvent(BonusEvent bonusEvent) {
        this.bonusEvent = bonusEvent;
    }

}
