package com.myapps.vista_ccc.detalle_transaccion.error;

import com.myapps.vista_ccc.detalle_transaccion.model.DetalleTransaccionResponseInfo;

/**
 * Created by michael on 16/8/2019.
 */
public class DetalleTransaccionException extends Exception {
    private static final long serialVersionUID = 1L;
    private final transient DetalleTransaccionResponseInfo responseInfo;

    public DetalleTransaccionException(DetalleTransaccionResponseInfo responseInfo) {
        this.responseInfo = responseInfo;
    }

    public DetalleTransaccionResponseInfo getResponseInfo() {
        return responseInfo;
    }
}
