
package com.myapps.vista_ccc.detalle_transaccion.model.usage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TotalChargedAmount implements Serializable
{

    @SerializedName("taxFreeAmount")
    @Expose
    private TaxFreeAmount taxFreeAmount;
    private final static long serialVersionUID = 8908220579454446730L;

    public TaxFreeAmount getTaxFreeAmount() {
        return taxFreeAmount;
    }

    public void setTaxFreeAmount(TaxFreeAmount taxFreeAmount) {
        this.taxFreeAmount = taxFreeAmount;
    }

}
