
package com.myapps.vista_ccc.detalle_transaccion.model.adjustment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Node implements Serializable
{

    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("externalReferenceId")
    @Expose
    private String externalReferenceId;
    @SerializedName("reason")
    @Expose
    private String reason;
    @SerializedName("characteristics")
    @Expose
    private Characteristics characteristics;
    @SerializedName("primaryId")
    @Expose
    private String primaryId;
    @SerializedName("chargingBalanceChanges")
    @Expose
    private ChargingBalanceChanges chargingBalanceChanges;
    private final static long serialVersionUID = -1617035407551670553L;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getExternalReferenceId() {
        return externalReferenceId;
    }

    public void setExternalReferenceId(String externalReferenceId) {
        this.externalReferenceId = externalReferenceId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Characteristics getCharacteristics() {
        return characteristics;
    }

    public void setCharacteristics(Characteristics characteristics) {
        this.characteristics = characteristics;
    }

    public String getPrimaryId() {
        return primaryId;
    }

    public void setPrimaryId(String primaryId) {
        this.primaryId = primaryId;
    }

    public ChargingBalanceChanges getChargingBalanceChanges() {
        return chargingBalanceChanges;
    }

    public void setChargingBalanceChanges(ChargingBalanceChanges chargingBalanceChanges) {
        this.chargingBalanceChanges = chargingBalanceChanges;
    }

}
