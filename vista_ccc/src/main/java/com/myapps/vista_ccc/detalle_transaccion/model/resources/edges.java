package com.myapps.vista_ccc.detalle_transaccion.model.resources;

import java.io.Serializable;


public class edges implements Serializable {
    private com.myapps.vista_ccc.detalle_transaccion.model.resources.node node;

    public com.myapps.vista_ccc.detalle_transaccion.model.resources.node getNodes() {
        return node;
    }

    public void setNodes(com.myapps.vista_ccc.detalle_transaccion.model.resources.node nodes) {
        this.node = nodes;
    }
}
