package com.myapps.vista_ccc.detalle_transaccion.model.resources;

import java.io.Serializable;
import java.util.List;

public class resources implements Serializable {
    private List<com.myapps.vista_ccc.detalle_transaccion.model.resources.edges> edges;

    public List<com.myapps.vista_ccc.detalle_transaccion.model.resources.edges> getEdges() {
        return edges;
    }

    public void setEdges(List<com.myapps.vista_ccc.detalle_transaccion.model.resources.edges> edges) {
        this.edges = edges;
    }
}
