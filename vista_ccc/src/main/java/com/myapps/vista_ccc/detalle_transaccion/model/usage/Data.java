
package com.myapps.vista_ccc.detalle_transaccion.model.usage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Data implements Serializable
{

    @SerializedName("usageEvents")
    @Expose
    private UsageEvents usageEvents;
    private final static long serialVersionUID = -2631169008791350744L;

    public UsageEvents getUsageEvents() {
        return usageEvents;
    }

    public void setUsageEvents(UsageEvents usageEvents) {
        this.usageEvents = usageEvents;
    }

}
