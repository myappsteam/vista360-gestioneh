package com.myapps.vista_ccc.detalle_transaccion.business;

import com.myapps.vista_ccc.detalle_transaccion.error.DetalleTransaccionException;
import com.myapps.vista_ccc.detalle_transaccion.error.HttpStatusAcceptException;
import com.myapps.vista_ccc.detalle_transaccion.error.HttpStatusException;
import com.myapps.vista_ccc.detalle_transaccion.model.DetalleTransaccionResponseInfo;
import com.myapps.vista_ccc.detalle_transaccion.model.recharge.RechargeEvents;
import com.myapps.vista_ccc.detalle_transaccion.servicio.RechargeEventsGQLConsumer;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.UtilDate;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by michael on 19/8/2019.
 */
public class RechargeEventsBussines implements Serializable {
    private static final long serialVersionUID = 1L;
    private static Logger logger = Logger.getLogger(DetalleTransaccionBussines.class);
    @Inject
    private RechargeEventsGQLConsumer consumerRest;

    private Date startDate;
    static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.ms'Z'";

    public RechargeEvents obtenerRechargeEvents(String msisdn, String targetID, String usuario,
                                                long idAuditoriaHistoria) throws HttpStatusException, IOException, DetalleTransaccionException {
        startDate = UtilDate.sumarRestarDiasFecha(new Date(),-Parametros.detalleRangoDiasConsultaLogs);
        /*logger.info("fecha inicio con hora 0: "+UtilDate.setHoraInicio(startDate));
        logger.info("fecha fin con hora 23.59: "+UtilDate.setHoraFin(new Date()));*/
        String fechaInicio = UtilDate.dateToString(UtilDate.setHoraInicio(startDate),DATE_FORMAT);
        String fechaFin = UtilDate.dateToString(UtilDate.setHoraFin(new Date()),DATE_FORMAT);
        String body = Parametros.queryRechargeEventsDetalleTransaccion;
        //String body = "{\"query\":\"query rechargeEvents {\\n  rechargeEvents(filter: \\\"(AND (EQ target.id \\\\\\\"#TOKEN#\\\\\\\") (AND (GT created-at \\\\\\\"2019-06-01T04:00:01.000Z\\\\\\\") (LT created-at \\\\\\\"2019-07-31T12:00:00.000Z\\\\\\\")))\\\") {\\n    edges {\\n      node {\\n        eventStatus\\n        source\\n        createdAt\\n        rechargeType\\n        characteristics {\\n          entries {\\n            key\\n            value\\n          }\\n        }\\n        rechargeAmount {\\n          taxFreeAmount {\\n            amount\\n            currency\\n          }\\n        }\\n        recharges {\\n          edges {\\n            node {\\n              chargeCategory\\n              additionalInformation\\n              chargingBalanceChanges {\\n                edges {\\n                  node {\\n                    changedAt\\n                    change\\n                    target {\\n                      id\\n                      balanceId\\n                      specification {\\n                        name\\n                      }\\n                    }\\n                  }\\n                }\\n              }\\n            }\\n          }\\n        }\\n        bonusEvent {\\n          createdAt          \\n          characteristics {\\n            entries {\\n              key\\n              value\\n            }\\n          }\\n         chargingBalanceChanges {\\n            edges {\\n              node {\\n                changedAt\\n                change\\n                target {\\n                  id\\n                  balanceId\\n                  specification {\\n                    name\\n                  }\\n                }\\n              }\\n            }\\n          }\\n        }\\n      }\\n    }\\n  }\\n}\\n\",\"operationName\":\"rechargeEvents\"}";
        String whiteStringJson = "";
        try {
            body = body.replace("#TOKEN#",targetID);
            body = body.replace("#FECHAINICIO#",fechaInicio);
            body = body.replace("#FECHAFIN#",fechaFin);
            whiteStringJson = consumerRest.consumerRechargeEvents(msisdn, body, usuario, idAuditoriaHistoria);
        } catch (HttpStatusAcceptException e) {
            logger.info("Respuesta de rechargeEvents diferente de OK: " + e.getJson());
            throw new DetalleTransaccionException(DetalleTransaccionResponseInfo.getInstanceByJson(e.getJson()));
        }
        return RechargeEvents.convertJsonRecharge(whiteStringJson);
    }

}
