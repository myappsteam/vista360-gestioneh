
package com.myapps.vista_ccc.detalle_transaccion.model.recharge;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Characteristics implements Serializable
{

    @SerializedName("entries")
    @Expose
    private List<Entry> entries = null;
    private final static long serialVersionUID = 3493765104368100608L;

    public List<Entry> getEntries() {
        return entries;
    }

    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    }

}
