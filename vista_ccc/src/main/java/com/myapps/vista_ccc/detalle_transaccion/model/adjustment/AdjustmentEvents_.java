
package com.myapps.vista_ccc.detalle_transaccion.model.adjustment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class AdjustmentEvents_ implements Serializable
{

    @SerializedName("edges")
    @Expose
    private List<Edge> edges = null;
    private final static long serialVersionUID = 3874308125653613737L;

    public List<Edge> getEdges() {
        return edges;
    }

    public void setEdges(List<Edge> edges) {
        this.edges = edges;
    }

}
