
package com.myapps.vista_ccc.detalle_transaccion.model.adjustment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Edge_ implements Serializable
{

    @SerializedName("node")
    @Expose
    private Node_ node;
    private final static long serialVersionUID = -5185101791161636258L;

    public Node_ getNode() {
        return node;
    }

    public void setNode(Node_ node) {
        this.node = node;
    }

}
