
package com.myapps.vista_ccc.detalle_transaccion.model.usage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Edge__ implements Serializable
{

    @SerializedName("node")
    @Expose
    private Node__ node;
    private final static long serialVersionUID = 646847749187038678L;

    public Node__ getNode() {
        return node;
    }

    public void setNode(Node__ node) {
        this.node = node;
    }

}
