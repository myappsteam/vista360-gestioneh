
package com.myapps.vista_ccc.detalle_transaccion.model.adjustment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Data implements Serializable
{

    @SerializedName("adjustmentEvents")
    @Expose
    private AdjustmentEvents_ adjustmentEvents;
    private final static long serialVersionUID = -646197826347432017L;

    public AdjustmentEvents_ getAdjustmentEvents() {
        return adjustmentEvents;
    }

    public void setAdjustmentEvents(AdjustmentEvents_ adjustmentEvents) {
        this.adjustmentEvents = adjustmentEvents;
    }

}
