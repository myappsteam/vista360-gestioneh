
package com.myapps.vista_ccc.detalle_transaccion.model.usage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class DetailedCharges implements Serializable
{

    @SerializedName("edges")
    @Expose
    private List<Edge__> edges = null;
    private final static long serialVersionUID = -1997499948458295170L;

    public List<Edge__> getEdges() {
        return edges;
    }

    public void setEdges(List<Edge__> edges) {
        this.edges = edges;
    }

}
