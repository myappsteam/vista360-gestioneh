package com.myapps.vista_ccc.detalle_transaccion.model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

/**
 * Created by michael on 16/8/2019.
 */
public class DetalleTransaccionResponseInfo {
    private static final long serialVersionUID = 1L;
    private String mensaje;
    private String codigo;

    public static DetalleTransaccionResponseInfo getInstanceByJson(String json) {
        Type listType = new TypeToken<DetalleTransaccionResponseInfo>() {
        }.getType();
        Gson gson = new Gson();
        return gson.fromJson(json, listType);
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

}
