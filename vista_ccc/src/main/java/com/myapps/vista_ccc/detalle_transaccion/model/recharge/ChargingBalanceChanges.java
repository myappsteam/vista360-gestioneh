
package com.myapps.vista_ccc.detalle_transaccion.model.recharge;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ChargingBalanceChanges implements Serializable
{

    @SerializedName("edges")
    @Expose
    private List<Edge__> edges = null;
    private final static long serialVersionUID = -7830039881961054368L;

    public List<Edge__> getEdges() {
        return edges;
    }

    public void setEdges(List<Edge__> edges) {
        this.edges = edges;
    }

}
