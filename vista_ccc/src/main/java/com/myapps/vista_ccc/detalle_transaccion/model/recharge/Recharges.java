
package com.myapps.vista_ccc.detalle_transaccion.model.recharge;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Recharges implements Serializable
{

    @SerializedName("edges")
    @Expose
    private List<Edge_> edges = null;
    private final static long serialVersionUID = -2404610634005036756L;

    public List<Edge_> getEdges() {
        return edges;
    }

    public void setEdges(List<Edge_> edges) {
        this.edges = edges;
    }

}
