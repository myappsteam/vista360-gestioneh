
package com.myapps.vista_ccc.detalle_transaccion.model.adjustment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ChargingBalanceChanges implements Serializable
{

    @SerializedName("edges")
    @Expose
    private List<Edge_> edges = null;
    private final static long serialVersionUID = -6604862669257335490L;

    public List<Edge_> getEdges() {
        return edges;
    }

    public void setEdges(List<Edge_> edges) {
        this.edges = edges;
    }

}
