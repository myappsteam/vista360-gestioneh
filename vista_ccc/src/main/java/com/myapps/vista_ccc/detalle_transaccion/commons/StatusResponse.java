package com.myapps.vista_ccc.detalle_transaccion.commons;

public enum StatusResponse {

	HTTP_UNAUTHORIZED(401,"Cliente HTTP No Autorizado {Unauthorized}"),
	HTTP_FORBIDDEN(403,"Cliente HTTP Prohibido {Forbidden}"),
	HTTP_NOT_FOUND(404, "Not Found"),
	HTTP_CONFLICT(409,"Conflicto"),
	HTTP_INTERNAL_SERVER_ERROR(500,"Error interno de servidor."),
	HTTP_ERROR(500,"Error de aplicacion White List"),
	HTTP_OK(200, "OK");

	private int code;
	private String descripcion;

	StatusResponse(int code, String descripcion) {
		this.code = code;
		this.descripcion = descripcion;
	}

	public int getCode() {
		return code;
	}

	public String getDescripcion() {
		return descripcion;
	}

}
