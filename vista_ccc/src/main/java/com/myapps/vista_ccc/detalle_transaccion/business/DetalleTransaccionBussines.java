package com.myapps.vista_ccc.detalle_transaccion.business;

import com.myapps.vista_ccc.detalle_transaccion.error.DetalleTransaccionException;
import com.myapps.vista_ccc.detalle_transaccion.error.HttpStatusAcceptException;
import com.myapps.vista_ccc.detalle_transaccion.error.HttpStatusException;
import com.myapps.vista_ccc.detalle_transaccion.model.DetalleTransaccionResponseInfo;
import com.myapps.vista_ccc.detalle_transaccion.model.resources.ResponseResources;
import com.myapps.vista_ccc.detalle_transaccion.servicio.TargetIdGQLConsumer;
import com.myapps.vista_ccc.util.Parametros;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;

/**
 * Created by michael on 16/8/2019.
 */
@Named
public class DetalleTransaccionBussines implements Serializable {
    private static final long serialVersionUID = 1L;
    private static Logger logger = Logger.getLogger(DetalleTransaccionBussines.class);
    @Inject
    private TargetIdGQLConsumer consumerRest;

    public ResponseResources obtenerDetalleTransaccion(String msisdn, String usuario,
                                                       long idAuditoriaHistoria) throws HttpStatusException, IOException, DetalleTransaccionException {
        String prefijo = "591";
        String bodyTargetID = new StringBuffer().append(prefijo).append(msisdn).toString();
        String body = Parametros.queryTargetIDDetalleTransaccion.replace("#TOKEN#",bodyTargetID);
        //String body = "{\"query\":\"query getTargetId {\\nresources(filter: \\\"(AND (EQ primary-id \\\\\\\"59169413583\\\\\\\") (EQ resource-type \\\\\\\"msisdn\\\\\\\"))\\\") {\\nedges {\\nnode {\\nrealizedProduct {\\ntargetId: id\\n}\\n}\\n}\\n}\\n}\\n\\n\",\"operationName\":\"getTargetId\"}";
        String whiteStringJson = "";
        try {
            whiteStringJson = consumerRest.consumerTargetID(msisdn,body, usuario, idAuditoriaHistoria);
        } catch (HttpStatusAcceptException e) {
            logger.info("Respuesta de targetID diferente de OK: " + e.getJson());
            throw new DetalleTransaccionException(DetalleTransaccionResponseInfo.getInstanceByJson(e.getJson()));
        }
        return ResponseResources.convertJsonTargetID(whiteStringJson);
    }

}
