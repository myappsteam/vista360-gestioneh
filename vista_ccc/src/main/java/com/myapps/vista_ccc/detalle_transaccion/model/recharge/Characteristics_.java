
package com.myapps.vista_ccc.detalle_transaccion.model.recharge;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Characteristics_ implements Serializable
{

    @SerializedName("entries")
    @Expose
    private List<Object> entries = null;
    private final static long serialVersionUID = 934270225035283304L;

    public List<Object> getEntries() {
        return entries;
    }

    public void setEntries(List<Object> entries) {
        this.entries = entries;
    }

}
