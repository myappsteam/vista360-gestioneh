
package com.myapps.vista_ccc.detalle_transaccion.model.recharge;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Edge__ implements Serializable
{

    @SerializedName("node")
    @Expose
    private Node__ node;
    private final static long serialVersionUID = 7344142473195942715L;

    public Node__ getNode() {
        return node;
    }

    public void setNode(Node__ node) {
        this.node = node;
    }

}
