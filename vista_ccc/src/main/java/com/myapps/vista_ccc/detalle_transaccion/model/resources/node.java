package com.myapps.vista_ccc.detalle_transaccion.model.resources;

import java.io.Serializable;

public class node implements Serializable {
    private com.myapps.vista_ccc.detalle_transaccion.model.resources.realizedProduct realizedProduct;

    public com.myapps.vista_ccc.detalle_transaccion.model.resources.realizedProduct getRealizedProduct() {
        return realizedProduct;
    }

    public void setRealizedProduct(com.myapps.vista_ccc.detalle_transaccion.model.resources.realizedProduct realizedProduct) {
        this.realizedProduct = realizedProduct;
    }
}
