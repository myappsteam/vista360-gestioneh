package com.myapps.vista_ccc.detalle_transaccion.servicio;

import com.myapps.vista_ccc.detalle_transaccion.commons.StatusResponse;
import com.myapps.vista_ccc.detalle_transaccion.error.HttpStatusAcceptException;
import com.myapps.vista_ccc.detalle_transaccion.error.HttpStatusException;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

@Named
public class ApacheHttpClientRestServiceDetalle implements Serializable {

	private static Logger logger = Logger.getLogger(ApacheHttpClientRestServiceDetalle.class);

	public synchronized String httpClientRest(String message, String urlString, int timeout) throws IOException, HttpStatusAcceptException, HttpStatusException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException {

		String responseBody;
		CloseableHttpResponse response;
		CloseableHttpClient httpClient = null;
		HttpHost proxy = new HttpHost("172.28.10.39", 2017, "http");
		//HttpClient httpClient = HttpClientBuilder.create().setProxy(proxy).build();

		logger.info("Consume service POST httpMethod: " + message);
		try{
			//httpClient = HttpClientBuilder.create().setProxy(proxy).build();
			httpClient = HttpClientBuilder.create().setProxy(proxy).setDefaultRequestConfig(generarConfiguracionRequest(timeout)).build();
			response = httpClient.execute(prepararRequetPost(message, urlString));
			responseBody = "";
			responseBody = validarResponse(response);
		}finally {
			httpClient.close();
		}

		return responseBody;
	}

	public RequestConfig generarConfiguracionRequest(int timeout) {
		return RequestConfig.custom().setConnectTimeout(timeout * 1000).setConnectionRequestTimeout(timeout * 1000)
				.setSocketTimeout(timeout * 1000).build();
	}

	public HttpPost prepararRequetPost(String body, String url) throws UnsupportedEncodingException {
		HttpPost postRequest = new HttpPost(url);
		postRequest.addHeader("content-type", "application/json");
		//postRequest.addHeader("Authorization", "Basic " + tokken);
		StringEntity entity = new StringEntity(body);
		postRequest.setEntity(entity);
		return postRequest;
	}

	public String validarResponse(HttpResponse response)
			throws HttpStatusAcceptException, IOException, HttpStatusException {
		if (response != null) {
			logger.info("Http Status Code: " + response.getStatusLine().getStatusCode());
			if (response.getStatusLine().getStatusCode() != StatusResponse.HTTP_OK.getCode()) {
				if (response.getStatusLine().getStatusCode() == StatusResponse.HTTP_NOT_FOUND.getCode()
						|| response.getStatusLine().getStatusCode() == StatusResponse.HTTP_CONFLICT.getCode()
						|| response.getStatusLine().getStatusCode() == StatusResponse.HTTP_ERROR.getCode()) {
					throw new HttpStatusAcceptException(EntityUtils.toString(response.getEntity()),
							response.getStatusLine().getStatusCode());
				} else if (response.getStatusLine().getStatusCode() == StatusResponse.HTTP_UNAUTHORIZED.getCode()) {
					throw new HttpStatusException(StatusResponse.HTTP_UNAUTHORIZED.getDescripcion(),
							StatusResponse.HTTP_UNAUTHORIZED);
				} else if (response.getStatusLine().getStatusCode() == StatusResponse.HTTP_FORBIDDEN.getCode()) {
					throw new HttpStatusException(StatusResponse.HTTP_FORBIDDEN.getDescripcion(),
							StatusResponse.HTTP_FORBIDDEN);
				} else {
					throw new HttpStatusException(response.getStatusLine().getReasonPhrase(),
							StatusResponse.HTTP_ERROR);
				}
			}
		} else {
			logger.error("No se pudo establecer una conexion con el servicio Rest Servicio resualto a Null.");
			throw new HttpStatusException(
					StatusResponse.HTTP_ERROR.getDescripcion() + "Causa: Servicio resulto a nulo.",
					StatusResponse.HTTP_ERROR);
		}
		return EntityUtils.toString(response.getEntity());
	}
}
