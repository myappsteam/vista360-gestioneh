
package com.myapps.vista_ccc.detalle_transaccion.model.recharge;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.math.BigDecimal;

public class Node__ implements Serializable
{

    @SerializedName("changedAt")
    @Expose
    private String changedAt;
    @SerializedName("change")
    @Expose
    private BigDecimal change;
    @SerializedName("target")
    @Expose
    private Target target;
    private final static long serialVersionUID = 8662597225386030213L;

    public String getChangedAt() {
        return changedAt;
    }

    public void setChangedAt(String changedAt) {
        this.changedAt = changedAt;
    }

    public BigDecimal getChange() {
        return change;
    }

    public void setChange(BigDecimal change) {
        this.change = change;
    }

    public Target getTarget() {
        return target;
    }

    public void setTarget(Target target) {
        this.target = target;
    }

}
