
package com.myapps.vista_ccc.detalle_transaccion.model.usage;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

public class Usage implements Serializable
{

    @SerializedName("data")
    @Expose
    private Data data;
    private final static long serialVersionUID = -7977816527538809555L;

    public static Usage convertJsonUsage(String stringJson) {
        Gson gson = new Gson();
        return gson.fromJson(stringJson, Usage.class);
    }

    public static List<Usage> convertJsonToListUsage(String stringJson) {
        Type listType = new TypeToken<List<Usage>>() {
        }.getType();
        Gson gson = new Gson();
        return gson.fromJson(stringJson, listType);
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
