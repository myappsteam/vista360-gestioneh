package com.myapps.vista_ccc.detalle_transaccion.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by michael on 20/8/2019.
 */
public class DetalleTransaccion implements Serializable {

    private final static long serialVersionUID = 1L;
    private Date fechaHora;
    private String tipoTransaccion;
    private Double monto;

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    public String getTipoTransaccion() {
        return tipoTransaccion;
    }

    public void setTipoTransaccion(String tipoTransaccion) {
        this.tipoTransaccion = tipoTransaccion;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }
}
