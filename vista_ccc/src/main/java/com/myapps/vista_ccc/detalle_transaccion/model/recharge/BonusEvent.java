
package com.myapps.vista_ccc.detalle_transaccion.model.recharge;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BonusEvent implements Serializable
{

    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("characteristics")
    @Expose
    private Characteristics_ characteristics;
    @SerializedName("chargingBalanceChanges")
    @Expose
    private ChargingBalanceChanges_ chargingBalanceChanges;
    private final static long serialVersionUID = -8975896837115854468L;

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Characteristics_ getCharacteristics() {
        return characteristics;
    }

    public void setCharacteristics(Characteristics_ characteristics) {
        this.characteristics = characteristics;
    }

    public ChargingBalanceChanges_ getChargingBalanceChanges() {
        return chargingBalanceChanges;
    }

    public void setChargingBalanceChanges(ChargingBalanceChanges_ chargingBalanceChanges) {
        this.chargingBalanceChanges = chargingBalanceChanges;
    }

}
