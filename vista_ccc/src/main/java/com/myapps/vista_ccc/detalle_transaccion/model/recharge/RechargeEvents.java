
package com.myapps.vista_ccc.detalle_transaccion.model.recharge;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

public class RechargeEvents implements Serializable
{

    @SerializedName("data")
    @Expose
    private Data data;
    private final static long serialVersionUID = 3283878859394445899L;

    public static RechargeEvents convertJsonRecharge(String stringJson) {
        Gson gson = new Gson();
        return gson.fromJson(stringJson, RechargeEvents.class);
    }

    public static List<RechargeEvents> convertJsonToListRecharge(String stringJson) {
        Type listType = new TypeToken<List<RechargeEvents>>() {
        }.getType();
        Gson gson = new Gson();
        return gson.fromJson(stringJson, listType);
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
