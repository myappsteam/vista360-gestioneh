
package com.myapps.vista_ccc.detalle_transaccion.model.recharge;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Target_ implements Serializable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("balanceId")
    @Expose
    private String balanceId;
    @SerializedName("specification")
    @Expose
    private Specification_ specification;
    private final static long serialVersionUID = 8026108210881278780L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBalanceId() {
        return balanceId;
    }

    public void setBalanceId(String balanceId) {
        this.balanceId = balanceId;
    }

    public Specification_ getSpecification() {
        return specification;
    }

    public void setSpecification(Specification_ specification) {
        this.specification = specification;
    }

}
