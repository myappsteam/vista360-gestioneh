
package com.myapps.vista_ccc.detalle_transaccion.model.recharge;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RechargeAmount implements Serializable
{

    @SerializedName("taxFreeAmount")
    @Expose
    private TaxFreeAmount taxFreeAmount;
    private final static long serialVersionUID = -1031126655824857570L;

    public TaxFreeAmount getTaxFreeAmount() {
        return taxFreeAmount;
    }

    public void setTaxFreeAmount(TaxFreeAmount taxFreeAmount) {
        this.taxFreeAmount = taxFreeAmount;
    }

}
