package com.myapps.vista_ccc.detalle_transaccion.business;

import com.myapps.vista_ccc.detalle_transaccion.error.DetalleTransaccionException;
import com.myapps.vista_ccc.detalle_transaccion.error.HttpStatusAcceptException;
import com.myapps.vista_ccc.detalle_transaccion.error.HttpStatusException;
import com.myapps.vista_ccc.detalle_transaccion.model.DetalleTransaccionResponseInfo;
import com.myapps.vista_ccc.detalle_transaccion.model.adjustment.AdjustmentEvents;
import com.myapps.vista_ccc.detalle_transaccion.servicio.AdjustmentEventsGQLConsumer;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.UtilDate;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by michael on 19/8/2019.
 */
public class AdjustmentEventsBussines implements Serializable {
    private static final long serialVersionUID = 1L;
    private static Logger logger = Logger.getLogger(DetalleTransaccionBussines.class);
    @Inject
    private AdjustmentEventsGQLConsumer consumerRest;

    private Date startDate;
    static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.ms'Z'";

    public AdjustmentEvents obtenerAdjustmentEvents(String msisdn, String targetID, String usuario,
                                                    long idAuditoriaHistoria) throws HttpStatusException, IOException, DetalleTransaccionException {
        startDate = UtilDate.sumarRestarDiasFecha(new Date(),-Parametros.detalleRangoDiasConsultaLogs);
        /*logger.info("fecha inicio con hora 0: "+UtilDate.setHoraInicio(startDate));
        logger.info("fecha fin con hora 23.59: "+UtilDate.setHoraFin(new Date()));*/
        String fechaInicio = UtilDate.dateToString(UtilDate.setHoraInicio(startDate),DATE_FORMAT);
        String fechaFin = UtilDate.dateToString(UtilDate.setHoraFin(new Date()),DATE_FORMAT);
        String body = Parametros.queryAdjustmentEventsDetalleTransaccion;
        //String body = "{\"query\":\"query adjustmentEvents {\\nadjustmentEvents(filter: \\\"(AND (EQ target.id \\\\\\\"#TOKEN#\\\\\\\") (AND (GT created-at \\\\\\\"2018-06-05T04:00:01.000Z\\\\\\\") (LT created-at \\\\\\\"2019-08-01T12:00:00.000Z\\\\\\\")))\\\") {\\nedges {\\nnode {\\nsource\\ncreatedAt\\nexternalReferenceId\\nreason\\ncharacteristics {\\nentries {\\nkey\\nvalue\\n}\\n}\\nprimaryId\\nchargingBalanceChanges {\\nedges {\\nnode {\\nchangedAt\\nchange\\ntarget {\\nid\\nbalanceId\\nspecification {\\nname\\n}\\n}\\n}\\n}\\n}\\n}\\n}\\n}\\n}\\n\",\"operationName\":\"rechargeEvents\"}";
        String whiteStringJson = "";
        try {
            body = body.replace("#TOKEN#",targetID);
            body = body.replace("#FECHAINICIO#",fechaInicio);
            body = body.replace("#FECHAFIN#",fechaFin);
            whiteStringJson = consumerRest.consumerAdjustmentEvents(msisdn, body, usuario, idAuditoriaHistoria);
        } catch (HttpStatusAcceptException e) {
            logger.info("Respuesta de adjustmentEvents diferente de OK: " + e.getJson());
            throw new DetalleTransaccionException(DetalleTransaccionResponseInfo.getInstanceByJson(e.getJson()));
        }
        return AdjustmentEvents.convertJsonAdjustment(whiteStringJson);
    }

}
