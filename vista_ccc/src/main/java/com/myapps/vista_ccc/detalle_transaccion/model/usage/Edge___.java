
package com.myapps.vista_ccc.detalle_transaccion.model.usage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Edge___ implements Serializable
{

    @SerializedName("node")
    @Expose
    private Node___ node;
    private final static long serialVersionUID = 140084478199014268L;

    public Node___ getNode() {
        return node;
    }

    public void setNode(Node___ node) {
        this.node = node;
    }

}
