package com.myapps.vista_ccc.detalle_transaccion.model.resources;

import java.io.Serializable;

public class realizedProduct implements Serializable {
    private String targetId;

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }
}
