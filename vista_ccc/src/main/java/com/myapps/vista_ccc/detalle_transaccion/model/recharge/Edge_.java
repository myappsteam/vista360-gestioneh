
package com.myapps.vista_ccc.detalle_transaccion.model.recharge;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Edge_ implements Serializable
{

    @SerializedName("node")
    @Expose
    private Node_ node;
    private final static long serialVersionUID = -7895527059734687697L;

    public Node_ getNode() {
        return node;
    }

    public void setNode(Node_ node) {
        this.node = node;
    }

}
