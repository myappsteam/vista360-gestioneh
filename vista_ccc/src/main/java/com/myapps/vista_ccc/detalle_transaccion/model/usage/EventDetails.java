
package com.myapps.vista_ccc.detalle_transaccion.model.usage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class EventDetails implements Serializable
{

    @SerializedName("__typename")
    @Expose
    private String typename;
    @SerializedName("trafficCase")
    @Expose
    private String trafficCase;
    @SerializedName("isRoaming")
    @Expose
    private boolean isRoaming;
    @SerializedName("ownNumber")
    @Expose
    private String ownNumber;
    @SerializedName("otherPartyNumber")
    @Expose
    private String otherPartyNumber;
    private final static long serialVersionUID = 7418773030123867390L;

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    public String getTrafficCase() {
        return trafficCase;
    }

    public void setTrafficCase(String trafficCase) {
        this.trafficCase = trafficCase;
    }

    public boolean isIsRoaming() {
        return isRoaming;
    }

    public void setIsRoaming(boolean isRoaming) {
        this.isRoaming = isRoaming;
    }

    public String getOwnNumber() {
        return ownNumber;
    }

    public void setOwnNumber(String ownNumber) {
        this.ownNumber = ownNumber;
    }

    public String getOtherPartyNumber() {
        return otherPartyNumber;
    }

    public void setOtherPartyNumber(String otherPartyNumber) {
        this.otherPartyNumber = otherPartyNumber;
    }

}
