
package com.myapps.vista_ccc.detalle_transaccion.model.usage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ChargingBalanceChanges implements Serializable
{

    @SerializedName("edges")
    @Expose
    private List<Edge___> edges = null;
    private final static long serialVersionUID = 8223505386541579412L;

    public List<Edge___> getEdges() {
        return edges;
    }

    public void setEdges(List<Edge___> edges) {
        this.edges = edges;
    }

}
