
package com.myapps.vista_ccc.detalle_transaccion.model.usage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Target implements Serializable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("balanceId")
    @Expose
    private String balanceId;
    @SerializedName("specification")
    @Expose
    private Specification specification;
    private final static long serialVersionUID = 5312977150795432333L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBalanceId() {
        return balanceId;
    }

    public void setBalanceId(String balanceId) {
        this.balanceId = balanceId;
    }

    public Specification getSpecification() {
        return specification;
    }

    public void setSpecification(Specification specification) {
        this.specification = specification;
    }

}
