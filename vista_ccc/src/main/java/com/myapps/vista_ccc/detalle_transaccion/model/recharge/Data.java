
package com.myapps.vista_ccc.detalle_transaccion.model.recharge;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Data implements Serializable
{

    @SerializedName("rechargeEvents")
    @Expose
    private RechargeEvents_ rechargeEvents;
    private final static long serialVersionUID = -8901406630301360L;

    public RechargeEvents_ getRechargeEvents() {
        return rechargeEvents;
    }

    public void setRechargeEvents(RechargeEvents_ rechargeEvents) {
        this.rechargeEvents = rechargeEvents;
    }

}
