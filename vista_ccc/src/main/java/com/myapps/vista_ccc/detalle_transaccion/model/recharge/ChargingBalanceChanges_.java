
package com.myapps.vista_ccc.detalle_transaccion.model.recharge;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ChargingBalanceChanges_ implements Serializable
{

    @SerializedName("edges")
    @Expose
    private List<Edge___> edges = null;
    private final static long serialVersionUID = 3827262753351426822L;

    public List<Edge___> getEdges() {
        return edges;
    }

    public void setEdges(List<Edge___> edges) {
        this.edges = edges;
    }

}
