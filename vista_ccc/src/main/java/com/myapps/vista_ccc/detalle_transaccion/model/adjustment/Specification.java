
package com.myapps.vista_ccc.detalle_transaccion.model.adjustment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Specification implements Serializable
{

    @SerializedName("name")
    @Expose
    private String name;
    private final static long serialVersionUID = -2583898170365712976L;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
