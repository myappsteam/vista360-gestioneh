package com.myapps.vista_ccc.thread;

import java.io.Serializable;
import java.math.BigInteger;

public class Control implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer nroThreads;
	private Integer nroFinalizadosPrimeraIteracion;
	private BigInteger totalCdrNum; // el total de la consulta, si el servicio genera 10000 y solo se requiere 100 registros
	private BigInteger fetchRowNum; // cantidad de filas o registros a traer por cada pagina
	private Long idAuditoria;

	public Control() {
		super();
	}

	public Control(Integer nroThreads, Integer nroFinalizadosPrimeraIteracion, BigInteger totalCdrNum, BigInteger fetchRowNum, Long idAuditoria) {
		super();
		this.nroThreads = nroThreads;
		this.nroFinalizadosPrimeraIteracion = nroFinalizadosPrimeraIteracion;
		this.setTotalCdrNum(totalCdrNum);
		this.fetchRowNum = fetchRowNum;
		this.idAuditoria = idAuditoria;
	}

	public synchronized void eliminarHilo() {
		nroThreads--;
	}

	public synchronized void finPrimeraIteracionHilo() {
		nroFinalizadosPrimeraIteracion--;
	}

	public Integer getNroThreads() {
		return nroThreads;
	}

	public void setNroThreads(Integer nroThreads) {
		this.nroThreads = nroThreads;
	}

	public BigInteger getTotalCdrNum() {
		return totalCdrNum;
	}

	public void setTotalCdrNum(BigInteger totalCdrNum) {
		this.totalCdrNum = totalCdrNum;
	}

	public BigInteger getFetchRowNum() {
		return fetchRowNum;
	}

	public void setFetchRowNum(BigInteger fetchRowNum) {
		this.fetchRowNum = fetchRowNum;
	}

	public Integer getNroFinalizadosPrimeraIteracion() {
		return nroFinalizadosPrimeraIteracion;
	}

	public void setNroFinalizadosPrimeraIteracion(Integer nroFinalizadosPrimeraIteracion) {
		this.nroFinalizadosPrimeraIteracion = nroFinalizadosPrimeraIteracion;
	}

	public Long getIdAuditoria() {
		return idAuditoria;
	}

	public void setIdAuditoria(Long idAuditoria) {
		this.idAuditoria = idAuditoria;
	}

}
