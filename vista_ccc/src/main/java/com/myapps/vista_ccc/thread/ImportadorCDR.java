package com.myapps.vista_ccc.thread;

import com.huawei.www.bme.cbsinterface.bbservices.BbServicsBindingStub;
import com.huawei.www.bme.cbsinterface.bbservices.QueryCDRResultCDRInfoAdditionalProperty;
import com.myapps.vista_ccc.bean.model.Nota;
import com.myapps.vista_ccc.business.*;
import com.myapps.vista_ccc.entity.*;
import com.myapps.vista_ccc.servicio.NodoServicio;
import com.myapps.vista_ccc.servicio.ServicioHistorial;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.ToXml;
import com.myapps.vista_ccc.util.UtilDate;
import com.myapps.vista_ccc.util.UtilUrl;
import org.apache.axis.AxisFault;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.xml.rpc.ServiceException;
import java.io.CharArrayWriter;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class ImportadorCDR extends Thread {

	private static Logger log = Logger.getLogger(ImportadorCDR.class);

	private Long idThread;
	private boolean live;
	private ControlButton controlButton;
	private BigInteger beginRowNum; // el puntero o indexOf de los registros
	private String isdn = "";
	private String login;
	private BigInteger totalCDRNum; // total de registro que genera y retorna el servicio web
	private String fechaInicio;
	private String fechaFin;

	private String serviceCategory;
	private String flowType;

	@Inject
	private LogExcepcionBL blExcepcion;
	
	private HashMap<String, String> hashServiceCategory;
	private HashMap<String, String> hashFlowType;
	private HashMap<String, String> hashServiceType;
	private HashMap<String, String> hashRoamFlag;
	private HashMap<String, String> hashSpecialNumber;

	int contador;

	@Inject
	private ConsultaBL blConsulta;

	@Inject
	private AccountHistoryBL blAccountHistory;

	@Inject
	private LogAuditoriaBL blAuditoria;

	@Inject
	private HiloBL blHilo;

	// ***************************************
	private Control control;

	public ImportadorCDR() {

	}

	public ImportadorCDR(Long idThread, Control control, ControlButton controlButton) {

		this.idThread = idThread;
		this.control = control;
		this.controlButton = controlButton;
		this.beginRowNum = new BigInteger("0");
		this.totalCDRNum = new BigInteger("0");
	}

	public void run() {
		this.live = true;
		boolean primeraConsulta = true;
		contador = 0;
		String error = "";

		try {

			VcHilo hilo = (VcHilo) blHilo.find(idThread, VcHilo.class);
			VcLogAuditoria auditoria = (VcLogAuditoria) getBlAuditoria().find(control.getIdAuditoria(), VcLogAuditoria.class);

			while (live) {
				try {
					if (!primeraConsulta) {
						log.debug("[ID]: " + idThread + "_______________________ beginRowNum > totalCDRNum: " + beginRowNum.intValue() + " > " + totalCDRNum.intValue());
						if (totalCDRNum.intValue() <= 0) {
							live = false;
						} else {
							if (beginRowNum.intValue() > totalCDRNum.intValue()) {
								live = false;
							}
						}

					}
					if (live) {

						try {

							log.debug("[ID]: " + idThread + "[isdn: " + getIsdn() + "] servicio BBServices queryCDR");

							long ini = System.currentTimeMillis();
							String wsdlLoc = Parametros.historialWsdl;

							URL url = new URL(UtilUrl.getIp(wsdlLoc));

							if (url.getProtocol().equalsIgnoreCase("https")) {
								int puerto = url.getPort();
								String host = url.getHost();
								if (host.equalsIgnoreCase("localhost")) {
									host = "127.0.0.1";
								}
								validarCertificado(Parametros.historialPathKeystore, host + ":" + puerto);
							}

							// com.huawei.www.bme.cbsinterface.bbservices.BbServicsBindingStub port = ServicioHistorial.initPort(wsdlLoc, idThread + isdn + login, Parametros.historialTimeOut);
							NodoServicio<BbServicsBindingStub> portNodo = ServicioHistorial.initPort(wsdlLoc, idThread + login, Parametros.historialTimeOut);

							if (portNodo != null && portNodo.getPort() != null) {

								synchronized (portNodo) {

									try {

										com.huawei.www.bme.cbsinterface.bbservices.BbServicsBindingStub port = portNodo.getPort();

										synchronized (port) {

											com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo ownershipInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo();
											ownershipInfo.setBEID(Parametros.historialOwnerShipInfoId);
											com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo accessSecurity = new com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo();
											accessSecurity.setLoginSystemCode(Parametros.historialLoginSystemCode);
											accessSecurity.setPassword(Parametros.historialPassword);
											com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo operatorInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo();
											operatorInfo.setOperatorID(Parametros.historialOperatorId);
											com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat timeFormat = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat();
											timeFormat.setTimeType(Parametros.historialTimeType);
											SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
											String messageSeq = format.format(new Date());

											com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderAdditionalProperty[] additionalProperty = null;
											com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader requestHeader = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader(Parametros.historialVersion, "", messageSeq, ownershipInfo, accessSecurity, operatorInfo, "", "", timeFormat,
													additionalProperty);

											com.huawei.www.bme.cbsinterface.bbservices.QueryCDRRequest queryCDRRequest = new com.huawei.www.bme.cbsinterface.bbservices.QueryCDRRequest();
											com.huawei.www.bme.cbsinterface.bbcommon.SubAccessCode subAccessCode = new com.huawei.www.bme.cbsinterface.bbcommon.SubAccessCode();
											subAccessCode.setPrimaryIdentity(isdn);

											queryCDRRequest.setSubAccessCode(subAccessCode);
											com.huawei.www.bme.cbsinterface.bbservices.QueryCDRRequestTimePeriod timePeriod = new com.huawei.www.bme.cbsinterface.bbservices.QueryCDRRequestTimePeriod();
											// timePeriod.setStartTime("20160701000000");
											// timePeriod.setEndTime("20160712235959");
											timePeriod.setStartTime(fechaInicio);
											timePeriod.setEndTime(fechaFin);

											queryCDRRequest.setTimePeriod(timePeriod);

											queryCDRRequest.setServiceCategory(serviceCategory);
											queryCDRRequest.setFlowType(flowType);

											queryCDRRequest.setTotalCDRNum(control.getTotalCdrNum());
											queryCDRRequest.setBeginRowNum(beginRowNum);
											queryCDRRequest.setFetchRowNum(control.getFetchRowNum());

											com.huawei.www.bme.cbsinterface.bbservices.QueryCDRRequestMsg queryCDRRequestMsg = new com.huawei.www.bme.cbsinterface.bbservices.QueryCDRRequestMsg(requestHeader, queryCDRRequest);
											com.huawei.www.bme.cbsinterface.bbservices.QueryCDRResultMsg queryCDR = port.queryCDR(queryCDRRequestMsg);

											long fin = System.currentTimeMillis();
											Long Con = null;
											if (portNodo.isPrimeraVez()){
												Con=portNodo.getTiempoConexion();
											}
											log.debug("[ID]: " + idThread + "[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio Historial: " + (fin - ini) + " milisegundos");

											if (Parametros.saveRequestResponse) {
												try {
													saveXml(isdn, login, wsdlLoc, "queryCDR", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), hilo, auditoria,Con,(fin - ini));
												} catch (Exception e) {
													log.warn("No se logro registrar los request y response del servicio: ", e);
												}
											}

											if (queryCDR != null && queryCDR.getResultHeader() != null) {
												if (queryCDR.getResultHeader().getResultCode().trim().equals("0")) {

													com.huawei.www.bme.cbsinterface.bbservices.QueryCDRResult queryCDRResult = queryCDR.getQueryCDRResult();

													if (queryCDRResult != null) {
														if (primeraConsulta) { // si es la primera vez que consulta el servicio
															totalCDRNum = queryCDRResult.getTotalCDRNum();
															// primeraConsulta = false;
															log.debug("[ID]: " + idThread + ", totalCDRNum: " + totalCDRNum);
														}

														com.huawei.www.bme.cbsinterface.bbservices.QueryCDRResultCDRInfo[] cdrInfo = queryCDRResult.getCDRInfo();
														if (cdrInfo != null && cdrInfo.length > 0) {
															for (com.huawei.www.bme.cbsinterface.bbservices.QueryCDRResultCDRInfo item : cdrInfo) {

																ArrayList<Nota> listProperty = new ArrayList<>();
																QueryCDRResultCDRInfoAdditionalProperty[] additionalPropertyList = item.getAdditionalProperty();
																if (additionalPropertyList != null && additionalPropertyList.length > 0) {
																	for (QueryCDRResultCDRInfoAdditionalProperty sp : additionalPropertyList) {
																		Nota nota = new Nota();
																		nota.setId(0);
																		nota.setName(sp.getCode());
																		nota.setValue(sp.getValue());
																		listProperty.add(nota);
																	}
																}
																ToXml xml = new ToXml();

																VcAccountHistory ah = new VcAccountHistory();
																ah.setAdditionalProperty(xml.toXml(listProperty));
																ah.setIsdn(isdn);
																ah.setUsuario(login);
																if (item.getServiceCategory() != null) {
																	String sc = hashServiceCategory.get(item.getServiceCategory().trim());
																	if (sc != null) {
																		ah.setServiceCategory(sc);
																	} else
																		ah.setServiceCategory(item.getServiceCategory().trim());
																}

																if (item.getFlowType() != null) {
																	String ft = hashFlowType.get(item.getFlowType().trim());
																	if (ft != null) {
																		ah.setFlowType(ft);
																	} else
																		ah.setFlowType(item.getFlowType().trim());
																}

																if (item.getSeriveType() != null) {
																	String st = hashServiceType.get(item.getSeriveType().trim());
																	if (st != null) {
																		ah.setSeriveType(st);
																	} else
																		ah.setSeriveType(item.getSeriveType().trim());
																}

																ah.setServiceTypeName(item.getServiceTypeName());

																Date dateStart = UtilDate.stringToDate(item.getStartTime(), Parametros.fechaFormatWs);
																Calendar calStart = Calendar.getInstance();
																calStart.setTimeInMillis(dateStart.getTime());
																ah.setStartTime(calStart);
																ah.setCdrSeq(item.getCdrSeq());
																Calendar nowDate = Calendar.getInstance();
																ah.setFechaCreacion(nowDate);

																Date dateEnd = UtilDate.stringToDate(item.getEndTime(), Parametros.fechaFormatWs);
																Calendar calEnd = Calendar.getInstance();
																calEnd.setTimeInMillis(dateEnd.getTime());
																ah.setEndTime(calEnd);
																ah.setOtherNumber(item.getOtherNumber());

																if (item.getRoamFlag() != null) {
																	String rf = hashRoamFlag.get(item.getRoamFlag().trim());
																	if (rf != null)
																		ah.setRoamFlag(rf);
																	else
																		ah.setRoamFlag(item.getRoamFlag().trim());
																}

																ah.setCallingCellId(item.getCallingCellID());
																ah.setCalledCellId(item.getCalledCellID());

																if (item.getSpecialNumberIndicator() != null) {
																	String sn = hashSpecialNumber.get(item.getSpecialNumberIndicator().trim());
																	if (sn != null)
																		ah.setSpecialNumberIndicator(sn);
																	else
																		ah.setSpecialNumberIndicator(item.getSpecialNumberIndicator().trim());
																}

																if (item.getBillCycleID() != null)
																	ah.setBillCycleId(item.getBillCycleID().trim());

																com.huawei.www.bme.cbsinterface.bbservices.QueryCDRResultCDRInfoVolumeInfo volumeInfo = item.getVolumeInfo();
																if (volumeInfo != null) {
																	BigInteger measureUnit = volumeInfo.getMeasureUnit();
																	if (measureUnit != null) {
																		ah.setMeasureUnit(new BigDecimal(measureUnit.intValue()));
																		ah.setMeasureUnitCdr(new BigDecimal(measureUnit.intValue()));
																	}
																	ah.setActualVolume(new BigDecimal(volumeInfo.getActualVolume()));
																	ah.setRatingVolume(new BigDecimal(volumeInfo.getRatingVolume()));
																	ah.setFreeVolume(new BigDecimal(volumeInfo.getFreeVolume()));
																}

																ah.setRefundIndicator(item.getRefundIndicator());

																com.huawei.www.bme.cbsinterface.bbservices.QueryCDRResultCDRInfoTotalChargeInfo tci = item.getTotalChargeInfo();
																if (tci != null) {
																	ah.setActualChargeAmt(new BigDecimal(tci.getActualChargeAmt()));
																	ah.setFreeChargeAmt(new BigDecimal(tci.getFreeChargeAmt()));
																	ah.setCurrencyIdCdr(new BigDecimal(tci.getCurrencyID()));

																	com.huawei.www.bme.cbsinterface.bbservices.QueryCDRResultCDRInfoTotalChargeInfoActualTax[] actualTax = tci.getActualTax();
																	if (actualTax != null && actualTax.length > 0) {
																		ah.setActualTaxCode(actualTax[0].getTaxCode());
																		ah.setActualTaxAmt(new BigDecimal(actualTax[0].getTaxAmt()));
																	}

																}

																ah.setVcHilo(hilo);
																getBlAccountHistory().save(ah);

																contador++;
																// log.debug("[ID]: " + idThread + "[isdn: " + getIsdn() + "] contador: " + contador);
															}
														} else {
															// log.debug("[ID]: " + idThread + " ***************************** QueryCDRResultCDRInfo NULO o VACIO **********************");
														}

													} else {
														log.warn("[ID]: " + idThread + "[isdn: " + getIsdn() + "] queryCDRResult resuelto a nulo");
													}
												} else {
													log.info("[ID]: " + idThread + "[isdn: " + getIsdn() + "] ResultCode: " + queryCDR.getResultHeader().getResultCode() + ", ResultDesc: " + queryCDR.getResultHeader().getResultDesc());
													guardarExcepcion(auditoria,"[ID]: " + idThread + "[isdn: " + getIsdn() + "] ResultCode: " + queryCDR.getResultHeader().getResultCode() + ", ResultDesc: " + queryCDR.getResultHeader().getResultDesc());
												}
											} else {
												if (queryCDR != null) {
													log.warn("[ID]: " + idThread + "[isdn: " + getIsdn() + "] ResultHeader resuelto a nulo");
												} else {
													log.warn("[ID]: " + idThread + "[isdn: " + getIsdn() + "] queryCDR resuelto a nulo");
												}
											}

										}
									} catch (Exception e) {
										throw e;
									} finally {
										portNodo.setEnUso(false);
										portNodo.setFechaFin(new Date());
									}
								}
							} else {
								log.error("[ID]: " + idThread + "[isdn: " + getIsdn() + "] Port Historial resuelto a nulo");
							}
						} catch (AxisFault a) {
							String msg = a.getMessage();
							msg = msg.toLowerCase();
							if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase()) || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase()) || msg.contains("UnknownHostException".toLowerCase())
									|| msg.contains("No route to host".toLowerCase()) || msg.contains("Connection refused".toLowerCase())) {
								log.error("[ID]: " + idThread + "[isdn: " + getIsdn() + "] Error de conexion al servicio BBServices, queryCDR: ", a);
							} else {
								log.error("[ID]: " + idThread + "[isdn: " + getIsdn() + "] Error AxisFault: ", a);
							}
							error = "Error de conexion al servicio BBServices, queryCDR: " + a.getMessage();
							guardarExcepcion(auditoria,"Error de conexion al servicio BBServices, queryCDR: " + stackTraceToString(a));
						} catch (ServiceException e) {
							log.error("[ID]: " + idThread + "[isdn: " + getIsdn() + "] Error al conectarse al servicio BBServices, queryCDR: ", e);
							error = "Error al conectarse al servicio BBServices queryCDR: " + e.getMessage();
							guardarExcepcion(auditoria,"Error al conectarse al servicio BBServices queryCDR: " + stackTraceToString(e));
						} catch (RemoteException e) {
							log.error("[ID]: " + idThread + "[isdn: " + getIsdn() + "] Error remoto al conectarse al servicio BBServices, queryCDR: ", e);
							error = "Error remoto al conectarse al servicio BBServices, queryCDR: " + e.getMessage();
							guardarExcepcion(auditoria,"Error remoto al conectarse al servicio BBServices, queryCDR: " + stackTraceToString(e));
						} catch (Exception e) {
							log.error("[ID]: " + idThread + "[isdn: " + getIsdn() + "] Error al consultar BBServices, queryCDR: ", e);
							error = "Error al consultar BBServices, queryCDR: " + e.getMessage();
							guardarExcepcion(auditoria,"Error al consultar BBServices, queryCDR: "  + stackTraceToString(e));
						}

						if (!error.isEmpty()) {
							controlButton.getErrores().add(error);
							error = "";
						}

						if (primeraConsulta) {
							control.finPrimeraIteracionHilo();
						}
						primeraConsulta = false;
						beginRowNum = beginRowNum.add(control.getFetchRowNum());
					}
					// Thread.sleep(3000);
				} catch (Exception e) {
					log.error("Error: ", e);
					guardarExcepcion(auditoria,"Error: " + stackTraceToString(e));
					// live = false;

				}

			}

			log.debug("[ID]: " + idThread + ", REG GUARDADOS AUX: " + contador);

		} catch (Exception e) {
			log.error("ERROR: ", e);
			live = false;
		} finally {
			log.debug("[ID]: " + idThread + ", REG GUARDADOS: " + contador);
			control.eliminarHilo();
			if (control.getNroThreads() == 0) {
				log.debug("[ID]: " + idThread + "_______________________________ ++++++++ FINALIZARON TODOS LOS HILOS ++++++++ ____________________________________");
				controlButton.setDisabled(false);
				controlButton.setFinalizado(true);
				log.debug("FINALIZARON TODOS LOS HILOS controlButton.disabled: " + controlButton.getDisabled());

				try {
					Thread.sleep(3000);
				} catch (Exception e2) {
					e2.getMessage();
				}
				controlButton.setPollStop(true);
				log.debug("FINALIZARON TODOS LOS HILOS controlButton.PollStop: " + controlButton.getPollStop());
			}
		}

		log.debug("FINALIZO idThread: " + idThread);
	}

	public String stackTraceToString(Throwable e) {
		CharArrayWriter sw = new CharArrayWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		pw.close();
		
	    return sw.toString(); // stack trace a
	}
	
	private void saveXml(String isdn, String login, String servicio, String metodo, String request, String response, VcHilo hilo, VcLogAuditoria auditoria,Long tiempoConexion, Long tiempoRespuesta) {
		try {
			VcConsulta c = new VcConsulta();
			c.setFechaCreacion(Calendar.getInstance());
			c.setIsdn(isdn);
			c.setLogin(login);
			c.setServicio(servicio);
			c.setMetodo(metodo);
			c.setRequest(request);
			c.setResponse(response);
			c.setTiempoConexion(tiempoConexion);
			c.setTiempoRespuesta(tiempoRespuesta);

			c.setVcHilo(hilo);
			c.setVcLogAuditoria(auditoria);

			getBlConsulta().save(c);
		} catch (Exception e) {
			log.warn("No se registro en bd el request y response: ", e);
		}
	}

	public void guardarExcepcion(VcLogAuditoria auditoria, String exepcion){
		log.debug("[saveVCExcepcion]: Ingresando..");
		VcLogExcepcion item = new VcLogExcepcion();
		try {
			//find = (VcLogAuditoria) blAuditoria.find(IdAuditoria, VcLogAuditoria.class);
			item.setFechaCreacion(Calendar.getInstance());
			item.setExcepcion(exepcion);
			item.setVcLogAuditoria(auditoria);
			
			getBlExcepcion().save(item);
		} catch (Exception e) {
			log.error("Error al guardar Excepcion: ", e);
		}
	}
	private void validarCertificado(String pathKeystore, String ipPort) {
		try {
			// Properties sysProperties = System.getProperties();
			log.debug("Ingresando a validar certificado pathKeystore: " + pathKeystore + ", ipPort: " + ipPort);

			System.setProperty("javax.net.ssl.trustStore", pathKeystore);
			System.setProperty("java.protocol.handler.pkgs", ipPort);

			HostnameVerifier hv = new HostnameVerifier() {
				public boolean verify(String urlHostName, SSLSession session) {
					// System.out.println("Warning: URL Host: " + urlHostName +
					// " vs. " + session.getPeerHost());
					return true;
				}
			};

			// trustAllHttpsCertificates();
			HttpsURLConnection.setDefaultHostnameVerifier(hv);
		} catch (Exception e) {
			log.error("Error al validar certificado: ", e);
		}
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getIsdn() {
		return isdn;
	}

	public void setIsdn(String isdn) {
		this.isdn = isdn;
	}

	public String getServiceCategory() {
		return serviceCategory;
	}

	public void setServiceCategory(String serviceCategory) {
		this.serviceCategory = serviceCategory;
	}

	public String getFlowType() {
		return flowType;
	}

	public void setFlowType(String flowType) {
		this.flowType = flowType;
	}

	public HashMap<String, String> getHashServiceCategory() {
		return hashServiceCategory;
	}

	public void setHashServiceCategory(HashMap<String, String> hashServiceCategory) {
		this.hashServiceCategory = hashServiceCategory;
	}

	public HashMap<String, String> getHashFlowType() {
		return hashFlowType;
	}

	public void setHashFlowType(HashMap<String, String> hashFlowType) {
		this.hashFlowType = hashFlowType;
	}

	public HashMap<String, String> getHashServiceType() {
		return hashServiceType;
	}

	public void setHashServiceType(HashMap<String, String> hashServiceType) {
		this.hashServiceType = hashServiceType;
	}

	public HashMap<String, String> getHashRoamFlag() {
		return hashRoamFlag;
	}

	public void setHashRoamFlag(HashMap<String, String> hashRoamFlag) {
		this.hashRoamFlag = hashRoamFlag;
	}

	public HashMap<String, String> getHashSpecialNumber() {
		return hashSpecialNumber;
	}

	public void setHashSpecialNumber(HashMap<String, String> hashSpecialNumber) {
		this.hashSpecialNumber = hashSpecialNumber;
	}

	public ConsultaBL getBlConsulta() {
		return blConsulta;
	}

	public void setBlConsulta(ConsultaBL blConsulta) {
		this.blConsulta = blConsulta;
	}

	public AccountHistoryBL getBlAccountHistory() {
		return blAccountHistory;
	}

	public void setBlAccountHistory(AccountHistoryBL blAccountHistory) {
		this.blAccountHistory = blAccountHistory;
	}

	public LogAuditoriaBL getBlAuditoria() {
		return blAuditoria;
	}

	public void setBlAuditoria(LogAuditoriaBL blAuditoria) {
		this.blAuditoria = blAuditoria;
	}

	public HiloBL getBlHilo() {
		return blHilo;
	}

	public void setBlHilo(HiloBL blHilo) {
		this.blHilo = blHilo;
	}

	public LogExcepcionBL getBlExcepcion() {
		return blExcepcion;
	}

	public void setBlExcepcion(LogExcepcionBL blExcepcion) {
		this.blExcepcion = blExcepcion;
	}

}
