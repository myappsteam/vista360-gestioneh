package com.myapps.vista_ccc.thread;

import com.myapps.vista_ccc.bean.model.Nota;
import com.myapps.vista_ccc.business.*;
import com.myapps.vista_ccc.entity.*;
import com.myapps.vista_ccc.servicio.NodoServicio;
import com.myapps.vista_ccc.servicio.ServicioBilletera;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.ToXml;
import com.myapps.vista_ccc.util.UtilDate;
import com.myapps.vista_ccc.util.UtilUrl;
import org.apache.axis.AxisFault;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.xml.rpc.ServiceException;
import java.io.CharArrayWriter;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class ImportadorAjustes extends Thread {

	private static Logger log = Logger.getLogger(ImportadorAjustes.class);

	private Long idThread;
	private boolean live;
	private ControlButton controlButton;
	private BigInteger beginRowNum; // el puntero o indexOf de los registros
	private String isdn = "";
	private String login;
	private BigInteger totalCDRNum; // total de registro que genera y retorna el servicio web
	private String fechaInicio;
	private String fechaFin;

	@Inject
	private ConsultaBL blConsulta;

	@Inject
	private AccountHistoryBL blAccountHistory;

	@Inject
	private LogAuditoriaBL blAuditoria;

	@Inject
	private LogExcepcionBL blExcepcion;
	
	@Inject
	private HiloBL blHilo;

	private Control control;
	private HashMap<String, String> hashAdjustmentChannelId;

	public ImportadorAjustes() {

	}

	public ImportadorAjustes(Long idThread, Control control, ControlButton controlButton) {
		this.idThread = idThread;
		this.control = control;
		this.controlButton = controlButton;
		this.beginRowNum = new BigInteger("0");
		this.totalCDRNum = new BigInteger("0");
	}

	public void run() {
		this.live = true;
		boolean primeraConsulta = true;
		int contador = 0;
		String error = "";

		try {

			VcHilo hilo = (VcHilo) blHilo.find(idThread, VcHilo.class);
			VcLogAuditoria auditoria = (VcLogAuditoria) getBlAuditoria().find(control.getIdAuditoria(), VcLogAuditoria.class);

			while (live) {
				try {
					if (!primeraConsulta) {
						log.debug("[ID]: " + idThread + "_______________________ beginRowNum > totalCDRNum: " + beginRowNum.intValue() + " > " + totalCDRNum.intValue());
						if (totalCDRNum.intValue() <= 0) {
							live = false;
						} else {
							if (beginRowNum.intValue() > totalCDRNum.intValue()) {
								live = false;
							}
						}

					}
					if (live) {

						try {

							log.debug("[ID]: " + idThread + "[isdn: " + getIsdn() + "] servicio ARServices queryAdjustLog");

							long ini = System.currentTimeMillis();
							String wsdlLoc = Parametros.billeteraWsdl;

							URL url = new URL(UtilUrl.getIp(wsdlLoc));

							if (url.getProtocol().equalsIgnoreCase("https")) {
								int puerto = url.getPort();
								String host = url.getHost();
								if (host.equalsIgnoreCase("localhost")) {
									host = "127.0.0.1";
								}
								validarCertificado(Parametros.billeteraPathKeystore, host + ":" + puerto);
							}

							// com.huawei.www.bme.cbsinterface.arservices.ArServicsBindingStub port = ServicioBilletera.initPort(wsdlLoc, idThread + isdn + login, Parametros.billeteraTimeOut);
							NodoServicio<com.huawei.www.bme.cbsinterface.arservices.ArServicsBindingStub> portNodo = ServicioBilletera.initPort(wsdlLoc, idThread + login, Parametros.billeteraTimeOut);

							if (portNodo != null && portNodo.getPort() != null) {

								synchronized (portNodo) {

									try {

										com.huawei.www.bme.cbsinterface.arservices.ArServicsBindingStub port = portNodo.getPort();

										synchronized (port) {

											com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo ownershipInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo();
											ownershipInfo.setBEID(Parametros.billeteraOwnerShipInfoId);
											com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo accessSecurity = new com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo();
											accessSecurity.setLoginSystemCode(Parametros.billeteraLoginSystemCode);
											accessSecurity.setPassword(Parametros.billeteraPassword);
											com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo operatorInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo();
											operatorInfo.setOperatorID(Parametros.billeteraOperatorId);
											com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat timeFormat = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat();
											timeFormat.setTimeType(Parametros.billeteraTimeType);
											SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
											String messageSeq = format.format(new Date());

											com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderAdditionalProperty[] additionalProperty = null;
											String version = Parametros.billeteraVersion;
											com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader requestHeader = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader(version, "", messageSeq, ownershipInfo, accessSecurity, operatorInfo, "", "", timeFormat, additionalProperty);

											com.huawei.www.bme.cbsinterface.arservices.QueryAdjustLogRequestMsg queryAdjustLogRequestMsg = new com.huawei.www.bme.cbsinterface.arservices.QueryAdjustLogRequestMsg();
											queryAdjustLogRequestMsg.setRequestHeader(requestHeader);
											com.huawei.www.bme.cbsinterface.arservices.QueryAdjustLogRequest queryAdjustLogRequest = new com.huawei.www.bme.cbsinterface.arservices.QueryAdjustLogRequest();
											queryAdjustLogRequest.setTotalRowNum(control.getTotalCdrNum());
											queryAdjustLogRequest.setBeginRowNum(beginRowNum);
											queryAdjustLogRequest.setFetchRowNum(control.getFetchRowNum());

											queryAdjustLogRequest.setStartTime(fechaInicio);
											queryAdjustLogRequest.setEndTime(fechaFin);

											com.huawei.www.bme.cbsinterface.arservices.QueryAdjustLogRequestQueryObj queryObj = new com.huawei.www.bme.cbsinterface.arservices.QueryAdjustLogRequestQueryObj();
											com.huawei.cbs.ar.wsservice.arcommon.SubAccessCode subAccessCode = new com.huawei.cbs.ar.wsservice.arcommon.SubAccessCode();
											subAccessCode.setPrimaryIdentity(isdn);
											queryObj.setSubAccessCode(subAccessCode);
											queryAdjustLogRequest.setQueryObj(queryObj);

											queryAdjustLogRequestMsg.setQueryAdjustLogRequest(queryAdjustLogRequest);
											com.huawei.www.bme.cbsinterface.arservices.QueryAdjustLogResultMsg queryAdjustLog = port.queryAdjustLog(queryAdjustLogRequestMsg);

											long fin = System.currentTimeMillis();
											Long Con = null;
											if (portNodo.isPrimeraVez()){
												Con=portNodo.getTiempoConexion();
											}
											log.debug("[ID]: " + idThread + "[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio ARServices queryAdjustLog: " + (fin - ini) + " milisegundos");

											if (Parametros.saveRequestResponse) {
												try {
													saveXml(isdn, login, wsdlLoc, "queryAdjustLog", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), hilo, auditoria,Con,(fin - ini));
												} catch (Exception e) {
													log.warn("No se logro registrar los request y response del servicio ARServices, queryAdjustLog: ", e);
												}
											}

											if (queryAdjustLog != null && queryAdjustLog.getResultHeader() != null) {
												if (queryAdjustLog.getResultHeader().getResultCode().trim().equals("0")) {

													com.huawei.www.bme.cbsinterface.arservices.QueryAdjustLogResult queryAdjustLogResult = queryAdjustLog.getQueryAdjustLogResult();

													if (queryAdjustLogResult != null) {
														if (primeraConsulta) { // si es la primera vez que consulta el servicio
															totalCDRNum = queryAdjustLogResult.getTotalRowNum();
															// primeraConsulta = false;
															log.debug("[ID]: " + idThread + ", totalCDRNum: " + totalCDRNum);
														}

														com.huawei.www.bme.cbsinterface.arservices.QueryAdjustLogResultAdjustInfo[] adjustInfo = queryAdjustLogResult.getAdjustInfo();

														if (adjustInfo != null && adjustInfo.length > 0) {
															for (com.huawei.www.bme.cbsinterface.arservices.QueryAdjustLogResultAdjustInfo item : adjustInfo) {

																// String strPropertys = "";
																ArrayList<Nota> listProperty = new ArrayList<>();
																com.huawei.cbs.ar.wsservice.arcommon.SimpleProperty[] additionalPropertyList = item.getAdditionalProperty();
																if (additionalPropertyList != null && additionalPropertyList.length > 0) {
																	for (com.huawei.cbs.ar.wsservice.arcommon.SimpleProperty sp : additionalPropertyList) {
																		Nota nota = new Nota();
																		nota.setId(0);
																		nota.setName(sp.getCode());
																		nota.setValue(sp.getValue());
																		listProperty.add(nota);
																	}
																}
																ToXml xml = new ToXml();

																VcAccountHistory ah = new VcAccountHistory();
																ah.setAjAdditionalProperty(xml.toXml(listProperty));
																ah.setIsdn(isdn);
																ah.setUsuario(login);
																ah.setServiceCategory("Adjustment");
																ah.setFlowType(item.getTransID() + "");

																if (item.getChannelID() != null) {
																	String channel = hashAdjustmentChannelId.get(item.getChannelID().trim());
																	if (channel != null) {
																		ah.setSeriveType(channel);
																	} else
																		ah.setSeriveType(item.getChannelID());
																}

																ah.setServiceTypeName(item.getExtTransID());

																Date dateStart = UtilDate.stringToDate(item.getTradeTime(), Parametros.fechaFormatWs);
																Calendar calStart = Calendar.getInstance();
																calStart.setTimeInMillis(dateStart.getTime());
																ah.setStartTime(calStart);

																Calendar nowDate = Calendar.getInstance();
																ah.setFechaCreacion(nowDate);

																ah.setAjCategory("Adjustment");
																ah.setAjPrimaryIdentity(isdn);
																if (item.getTradeTime() != null)
																	ah.setAjTradeTime(item.getTradeTime().trim());
																ah.setAjRemark(item.getRemark());
																ah.setAjChannelId(item.getChannelID());
																ah.setAjTransId(new BigDecimal(item.getTransID()));
																ah.setAjExtTransId(item.getExtTransID());
																ah.setVcHilo(hilo);

																getBlAccountHistory().save(ah);

																contador++;
																// log.debug("[ID]: " + idThread + "[isdn: " + getIsdn() + "] contador: " + contador);
															}
														} else {
															// log.debug("[ID]: " + idThread + " ***************************** QueryCDRResultCDRInfo NULO o VACIO **********************");
														}

													} else {
														log.warn("[ID]: " + idThread + "[isdn: " + getIsdn() + "] queryAdjustLogResult resuelto a nulo");
													}
												} else {
													log.info("[ID]: " + idThread + "[isdn: " + getIsdn() + "] ResultCode: " + queryAdjustLog.getResultHeader().getResultCode() + ", ResultDesc: " + queryAdjustLog.getResultHeader().getResultDesc());
												
												
												}
											} else {
												if (queryAdjustLog != null) {
													log.warn("[ID]: " + idThread + "[isdn: " + getIsdn() + "] ResultHeader resuelto a nulo");
													guardarExcepcion(auditoria, "[ID]: " + idThread + "[isdn: " + getIsdn() + "] ResultHeader resuelto a nulo");
												} else {
													log.warn("[ID]: " + idThread + "[isdn: " + getIsdn() + "] queryAdjustLog resuelto a nulo");
												}
											}

										}
									} catch (Exception e) {
										throw e;
									} finally {
										portNodo.setEnUso(false);
										portNodo.setFechaFin(new Date());
									}
								}

							} else {
								log.error("[ID]: " + idThread + "[isdn: " + getIsdn() + "] Port ARServices Historial resuelto a nulo");
							}
						} catch (AxisFault a) {
							String msg = a.getMessage();
							msg = msg.toLowerCase();
							if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase()) || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase()) || msg.contains("UnknownHostException".toLowerCase())
									|| msg.contains("No route to host".toLowerCase()) || msg.contains("Connection refused".toLowerCase())) {
								log.error("[ID]: " + idThread + "[isdn: " + getIsdn() + "] Error de conexion al servicio ARServices queryAdjustLog: ", a);
							} else {
								log.error("[ID]: " + idThread + "[isdn: " + getIsdn() + "] Error AxisFault: ", a);
							}
							error = "Error de conexion al servicio ARServices, queryAdjustLog: " + a.getMessage();
							guardarExcepcion(auditoria,"Error de conexion al servicio ARServices, queryAdjustLog: " +  stackTraceToString(a));
						} catch (ServiceException e) {
							log.error("[ID]: " + idThread + "[isdn: " + getIsdn() + "] Error al conectarse al servicio ARServices, queryAdjustLog: ", e);
							error = "Error al conectarse al servicio ARServices queryAdjustLog: " + e.getMessage();
							guardarExcepcion(auditoria,  "Error al conectarse al servicio ARServices queryAdjustLog: " + stackTraceToString(e));
						} catch (RemoteException e) {
							log.error("[ID]: " + idThread + "[isdn: " + getIsdn() + "] Error remoto al conectarse al servicio ARServices queryAdjustLog: ", e);
							error = "Error remoto al conectarse al servicio ARServices queryAdjustLog: " + e.getMessage();
							guardarExcepcion(auditoria,"Error remoto al conectarse al servicio ARServices queryAdjustLog: " +  stackTraceToString(e));
						} catch (Exception e) {
							log.error("[ID]: " + idThread + "[isdn: " + getIsdn() + "] Error al consultar ARServices queryAdjustLog: ", e);
							error = "Error al consultar ARServices, queryAdjustLog: " + e.getMessage();
							guardarExcepcion(auditoria,"Error al consultar ARServices, queryAdjustLog: " + stackTraceToString(e));
						}

						if (!error.isEmpty()) {
							controlButton.getErrores().add(error);
							error = "";
						}

						if (primeraConsulta) {
							control.finPrimeraIteracionHilo();
						}
						primeraConsulta = false;
						beginRowNum = beginRowNum.add(control.getFetchRowNum());

					}
					// Thread.sleep(3000);
				} catch (Exception e) {
					log.error("Error: ", e);
					guardarExcepcion(auditoria,"Error: " + stackTraceToString(e));
				}

			}

			log.debug("[ID]: " + idThread + ", REG GUARDADOS AUX: " + contador);

		} catch (Exception e) {
			log.error("ERROR: ", e);
			live = false;
		} finally {
			// log.info("[ID]: " + idThread + ", REG GUARDADOS: " + contador);
			control.eliminarHilo();
			if (control.getNroThreads() == 0) {
				log.debug("[ID]: " + idThread + "_______________________________ ++++++++ FINALIZARON TODOS LOS HILOS ++++++++ ____________________________________");
				controlButton.setDisabled(false);
				controlButton.setFinalizado(true);
				log.debug("FINALIZARON TODOS LOS HILOS controlButton.disabled: " + controlButton.getDisabled());
				try {
					Thread.sleep(3000);
				} catch (Exception e2) {
					e2.getMessage();
				}
				controlButton.setPollStop(true);
				log.debug("FINALIZARON TODOS LOS HILOS controlButton.PollStop: " + controlButton.getPollStop());
			}
		}

		log.debug("FINALIZO idThread: " + idThread);
	}
	
	public String stackTraceToString(Throwable e) {
		CharArrayWriter sw = new CharArrayWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		pw.close();
		
	    return sw.toString(); // stack trace a
	}

	private void validarCertificado(String pathKeystore, String ipPort) {
		try {
			// Properties sysProperties = System.getProperties();
			log.debug("Ingresando a validar certificado pathKeystore: " + pathKeystore + ", ipPort: " + ipPort);

			System.setProperty("javax.net.ssl.trustStore", pathKeystore);
			System.setProperty("java.protocol.handler.pkgs", ipPort);

			HostnameVerifier hv = new HostnameVerifier() {
				public boolean verify(String urlHostName, SSLSession session) {
					// System.out.println("Warning: URL Host: " + urlHostName +
					// " vs. " + session.getPeerHost());
					return true;
				}
			};

			// trustAllHttpsCertificates();
			HttpsURLConnection.setDefaultHostnameVerifier(hv);
		} catch (Exception e) {
			log.error("Error al validar certificado: ", e);
		}
	}

	private void saveXml(String isdn, String login, String servicio, String metodo, String request, String response, VcHilo hilo, VcLogAuditoria auditoria, Long tiempoConexion, Long tiempoRespuesta) {
		try {
			VcConsulta c = new VcConsulta();
			c.setFechaCreacion(Calendar.getInstance());
			c.setIsdn(isdn);
			c.setLogin(login);
			c.setServicio(servicio);
			c.setMetodo(metodo);
			c.setRequest(request);
			c.setResponse(response);
			c.setTiempoConexion(tiempoConexion);
			c.setTiempoRespuesta(tiempoRespuesta);
			
			c.setVcHilo(hilo);
			c.setVcLogAuditoria(auditoria);

			getBlConsulta().save(c);
		} catch (Exception e) {
			log.warn("No se registro en bd el request y response: ", e);
		}
	}

	public void guardarExcepcion(VcLogAuditoria auditoria, String exepcion){
		log.debug("[saveVCExcepcion]: Ingresando..");
		VcLogExcepcion item = new VcLogExcepcion();
		try {
			//find = (VcLogAuditoria) blAuditoria.find(IdAuditoria, VcLogAuditoria.class);
			item.setFechaCreacion(Calendar.getInstance());
			item.setExcepcion(exepcion);
			item.setVcLogAuditoria(auditoria);
			
			getBlExcepcion().save(item);
		} catch (Exception e) {
			log.error("Error al guardar Excepcion: ", e);
		}
	}
	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getIsdn() {
		return isdn;
	}

	public void setIsdn(String isdn) {
		this.isdn = isdn;
	}

	public HashMap<String, String> getHashAdjustmentChannelId() {
		return hashAdjustmentChannelId;
	}

	public void setHashAdjustmentChannelId(HashMap<String, String> hashAdjustmentChannelId) {
		this.hashAdjustmentChannelId = hashAdjustmentChannelId;
	}

	public ConsultaBL getBlConsulta() {
		return blConsulta;
	}

	public void setBlConsulta(ConsultaBL blConsulta) {
		this.blConsulta = blConsulta;
	}

	public AccountHistoryBL getBlAccountHistory() {
		return blAccountHistory;
	}

	public void setBlAccountHistory(AccountHistoryBL blAccountHistory) {
		this.blAccountHistory = blAccountHistory;
	}

	public LogAuditoriaBL getBlAuditoria() {
		return blAuditoria;
	}

	public void setBlAuditoria(LogAuditoriaBL blAuditoria) {
		this.blAuditoria = blAuditoria;
	}

	public HiloBL getBlHilo() {
		return blHilo;
	}

	public void setBlHilo(HiloBL blHilo) {
		this.blHilo = blHilo;
	}

	public LogExcepcionBL getBlExcepcion() {
		return blExcepcion;
	}

	public void setBlExcepcion(LogExcepcionBL blExcepcion) {
		this.blExcepcion = blExcepcion;
	}

}
