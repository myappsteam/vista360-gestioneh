package com.myapps.vista_ccc.thread;

import java.io.Serializable;
import java.util.List;

public class ControlButton implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Boolean disabled;
	private Boolean pollStop;
	private Boolean finalizado;
	private List<String> errores;

	public ControlButton(Boolean finalizado, Boolean disabled, Boolean pollStop) {
		super();
		this.setFinalizado(finalizado);
		this.disabled = disabled;
		this.pollStop = pollStop;
	}

	public Boolean getDisabled() {
		return disabled;
	}

	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}

	public Boolean getPollStop() {
		return pollStop;
	}

	public void setPollStop(Boolean pollStop) {
		this.pollStop = pollStop;
	}

	public Boolean getFinalizado() {
		return finalizado;
	}

	public void setFinalizado(Boolean finalizado) {
		this.finalizado = finalizado;
	}

	@Override
	public String toString() {
		return "ControlButton [disabled=" + disabled + ", pollStop=" + pollStop + ", finalizado=" + finalizado + "]";
	}

	public List<String> getErrores() {
		return errores;
	}

	public void setErrores(List<String> errores) {
		this.errores = errores;
	}

}
