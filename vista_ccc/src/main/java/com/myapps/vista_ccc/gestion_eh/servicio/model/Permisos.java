package com.myapps.vista_ccc.gestion_eh.servicio.model;

import java.io.Serializable;

public class Permisos implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private String nombre;
    private boolean estado;
    private String estadoString;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public String getEstadoString() {
        return estadoString;
    }

    public void setEstadoString(String estadoString) {
        this.estadoString = estadoString;
    }

    public Permisos(String id, String nombre, boolean estado, String estadoString) {
        this.id = id;
        this.nombre = nombre;
        this.estado = estado;
        this.estadoString = estadoString;
    }

    public Permisos() {
    }
}
