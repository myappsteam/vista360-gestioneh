package com.myapps.vista_ccc.gestion_eh.business;


import com.myapps.vista_ccc.gestion_eh.dao.VcPermisosSupervisorDAO;
import com.myapps.vista_ccc.gestion_eh.entity.VcPermisosSupervisorEntity;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
public class VcPermisosSupervisorBL implements Serializable {

    private static final long serialVersionUID = 1L;
    private static Logger log = Logger.getLogger(VcPermisosSupervisorBL.class);


    @Inject
    private VcPermisosSupervisorDAO dao;

    public void save(VcPermisosSupervisorEntity nodo) throws Exception {
        dao.save(nodo);
    }

    public VcPermisosSupervisorEntity getVcPermisosSupervisor(long idNodo) throws Exception {
        return dao.get(idNodo);
    }

    public void update(VcPermisosSupervisorEntity nodo) throws Exception {
        dao.update(nodo);
    }

    public void remove(VcPermisosSupervisorEntity nodo) throws Exception {
        dao.remove(nodo);
    }

    public List<VcPermisosSupervisorEntity> getList() throws Exception {
        return dao.getList();
    }


    public VcPermisosSupervisorEntity getSupervisorForUsuario(long idUsuario, String tipo) {
        VcPermisosSupervisorEntity valor = null;
        try {
            valor = dao.getSupervisorForUsuario(idUsuario, tipo);
        } catch (Exception e) {
            log.error("Exception: " + e.getMessage());
        }
        return valor;
    }

    public VcPermisosSupervisorEntity getConsultaSupervisorPermisoBL(long idUsuario) {
        VcPermisosSupervisorEntity valor = null;
        try {
            valor = dao.getSupervisorPermisos(idUsuario);
        } catch (Exception e) {
            log.error("Exception: " + e.getMessage());
        }
        return valor;
    }

    public VcPermisosSupervisorEntity getConsultaSupervisorPermisoGrupoIDBL(long idGrupoUsuario) {
        VcPermisosSupervisorEntity valor = null;
        try {
            valor = dao.getSupervisorPermisosGrupoID(idGrupoUsuario);
        } catch (Exception e) {
            log.error("Exception: " + e.getMessage());
        }
        return valor;
    }

    public List<VcPermisosSupervisorEntity> getIdPermisoSupervisor(long idPermisoSupervisor) {
        List<VcPermisosSupervisorEntity> list = new ArrayList<>();
        try {
            list = dao.getIdPermisoSupervisorDelete(idPermisoSupervisor);
        } catch (Exception e) {
            log.error("Exception: " + e.getMessage());
        }
        return list;
    }

    public List<VcPermisosSupervisorEntity> getIdPermisoSupervisorGrupoID(long idGrupoID) {
        List<VcPermisosSupervisorEntity> list = new ArrayList<>();
        try {
            list = dao.getIdPermisoSupervisorGrupoIDDelete(idGrupoID);
        } catch (Exception e) {
            log.error("Exception: " + e.getMessage());
        }
        return list;
    }

    public void removeDataPermisoSupervisor(VcPermisosSupervisorEntity nodo) throws Exception {
        dao.removeIdPermisoSupervisor(nodo);
    }

}
