package com.myapps.vista_ccc.gestion_eh.business;


import com.myapps.vista_ccc.gestion_eh.dao.VcZonasDAO;
import com.myapps.vista_ccc.gestion_eh.entity.VcZonasEntity;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
public class VcZonasBL implements Serializable {

    private static final long serialVersionUID = 1L;
    private static Logger log = Logger.getLogger(VcZonasBL.class);


    @Inject
    private VcZonasDAO dao;

    public void save(VcZonasEntity nodo) throws Exception {
        dao.save(nodo);
    }

    public VcZonasEntity getVcZona(long idNodo) throws Exception {
        return dao.get(idNodo);
    }

    public void update(VcZonasEntity nodo) throws Exception {
        dao.update(nodo);
    }


    public List<VcZonasEntity> getList() throws Exception {
        return dao.getList();
    }


    public VcZonasEntity getZonaForNombre(String nombre, long idSupervisor) {
        VcZonasEntity valor = null;
        try {
            if (nombre != null) {
                valor = dao.getZonaForNombre(nombre.trim(), idSupervisor);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return valor;
    }

    public VcZonasEntity getZonaForNombre(String nombre) {
        VcZonasEntity valor = null;
        try {
            if (nombre != null) {
                valor = dao.getZonaForNombre(nombre.trim());
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return valor;
    }

}
