package com.myapps.vista_ccc.gestion_eh.servicio;

import com.myapps.vista_ccc.gestion_eh.servicio.model.ExcepcionGenerica;
import com.myapps.vista_ccc.reset_pin.commons.AuditoriaResetPin;
import com.myapps.vista_ccc.reset_pin.model.gson.QueryUser;
import com.myapps.vista_ccc.reset_pin.servicio.UtilMap;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.white_list.commons.StatusResponse;
import org.apache.http.NoHttpResponseException;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.ConnectionPoolTimeoutException;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.net.ConnectException;
import java.net.ProtocolException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;


@Named
public class GestionEhRestConsumer implements Serializable {

    @Inject
    private ApacheHttpClientRestServiceGestionEh gestionClientRest;

    @Inject
    private AuditoriaResetPin auditoria;

    private static Logger logger = Logger.getLogger(GestionEhRestConsumer.class);


    public synchronized String consumerQueryGroup(String urlService, String usuario,
                                                  long idAuditoriaHistorial, String token) throws URISyntaxException, com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException, IOException {
        long ini = System.currentTimeMillis();
        String output = "";
        String urlConsumer = UtilMap.replaceParametros(urlService, UtilMap.mapear(Parametros.restUrlParametrosQueryGroup));
        logger.info("Url service: " + urlConsumer);
        try {
            output = gestionClientRest.getQueryGroups(urlConsumer, token);
            logger.info("Response QueryGroup Json: " + output);
            long fin = System.currentTimeMillis();
            logger.info("[Serivicio RESTful: QueryGroup ] Tiempo de respuesta: " + (fin - ini)
                    + " milisegundos");
            if (Parametros.saveRequestResponse) {
                try {
                    auditoria.saveXml(null, usuario, urlConsumer, "QueryGroup", "", output,
                            idAuditoriaHistorial, (fin - ini), (fin - ini));
                } catch (Exception e) {
                    logger.warn("No se logro registrar los request y response del servicio RESTful QueryGroup: ", e);
                }
            }
        } catch (UnknownHostException e) {
            logger.error("[UnknownHostException][QueryGroup] No se pudo alcanzar la url: " + Parametros.restUrlQueryGroup + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_ERROR.getCode(), "UnknownHostException", "[QueryGroup] No se pudo alcanzar la url: " + Parametros.restUrlQueryGroup + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);

        } catch (NoHttpResponseException e) {
            logger.error("[NoHttpResponseException][QueryGroup] El servidor web puede recibir solicitudes pero no puede procesarlas : " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_NOT_FOUND.getCode(), "NoHttpResponseException", "En algunas circunstancias, generalmente cuando se encuentra bajo una gran carga, el servidor web puede recibir solicitudes pero no puede procesarlas. La falta de recursos suficientes como subprocesos de trabajo es un buen ejemplo: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);

        } catch (ConnectionPoolTimeoutException e) {
            logger.error("[ConnectionPoolTimeoutException][QueryGroup] La excepción indica que el administrador de conexiones no puede obtener una conexión libre del grupo de conexiones dentro del período de tiempo dado: " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_NOT_FOUND.getCode(), "ConnectionPoolTimeoutException", "Esta excepción solo puede ocurrir cuando se usa el administrador de conexiones multiproceso. La excepción indica que el administrador de conexiones no puede obtener una conexión libre del grupo de conexiones dentro del período de tiempo dado: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);

        } catch (ProtocolException e) {
            logger.error("[ProtocolException][QueryGroup]  ProtocolException señala una violación de la especificación HTTP: " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_NOT_FOUND.getCode(), "ProtocolException", "ProtocolException señala una violación de la especificación HTTP: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);

        } catch (ConnectException e) {
            logger.error("[ConnectException][QueryGroup]  No se pudo establecer una conexion con el servicio Rest QueryGroup: " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_NOT_FOUND.getCode(), "ConnectException", "No se pudo establecer una conexion con el servicio Rest QueryGroup: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);
        } catch (ConnectTimeoutException e) {
            logger.error("[ConnectTimeoutException][QueryGroup]  Tiempo de espera agotado para respuesta de servicio QueryGroup: " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_ERROR.getCode(), "ConnectTimeoutException", "Tiempo de espera agotado para respuesta de servicio QueryGroup: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);
        }
        return output;
    }

    public synchronized String consumerQueryUserGroup(String urlService, String usuario, String codigo,
                                                      long idAuditoriaHistorial, String token) throws IOException, URISyntaxException, com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException {
        long ini = System.currentTimeMillis();
        String output = "";
        String urlConsumer = UtilMap.replaceParametros(urlService, UtilMap.mapear(Parametros.restUrlParametrosQueryUserGroup));
        logger.info("Url service: " + urlConsumer);
        try {
            output = gestionClientRest.getQueryUserGroups(urlConsumer, codigo, token);
            logger.info("Response QueryUserGroup Json: " + output);
            long fin = System.currentTimeMillis();
            logger.info("[Serivicio RESTful: QueryUserGroup ] Tiempo de respuesta: " + (fin - ini)
                    + " milisegundos");
            if (Parametros.saveRequestResponse) {
                try {
                    auditoria.saveXml(null, usuario, urlConsumer, "QueryUserGroup", "", output,
                            idAuditoriaHistorial, (fin - ini), (fin - ini));
                } catch (Exception e) {
                    logger.warn("No se logro registrar los request y response del servicio RESTful QueryUserGroup: ", e);
                }
            }

        } catch (UnknownHostException e) {
            logger.error("[UnknownHostException][QueryUserGroup] No se pudo alcanzar la url: " + Parametros.restUrlQueryUserGroup + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_ERROR.getCode(), "UnknownHostException", "[QueryUserGroup] No se pudo alcanzar la url: " + Parametros.restUrlQueryUserGroup + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);

        } catch (NoHttpResponseException e) {
            logger.error("[NoHttpResponseException][QueryUserGroup] El servidor web puede recibir solicitudes pero no puede procesarlas : " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_NOT_FOUND.getCode(), "NoHttpResponseException", "En algunas circunstancias, generalmente cuando se encuentra bajo una gran carga, el servidor web puede recibir solicitudes pero no puede procesarlas. La falta de recursos suficientes como subprocesos de trabajo es un buen ejemplo: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);

        } catch (ConnectionPoolTimeoutException e) {
            logger.error("[ConnectionPoolTimeoutException][QueryUserGroup] La excepción indica que el administrador de conexiones no puede obtener una conexión libre del grupo de conexiones dentro del período de tiempo dado: " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_NOT_FOUND.getCode(), "ConnectionPoolTimeoutException", "Esta excepción solo puede ocurrir cuando se usa el administrador de conexiones multiproceso. La excepción indica que el administrador de conexiones no puede obtener una conexión libre del grupo de conexiones dentro del período de tiempo dado: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);

        } catch (ProtocolException e) {
            logger.error("[ProtocolException][QueryUserGroup]  ProtocolException señala una violación de la especificación HTTP: " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_NOT_FOUND.getCode(), "ProtocolException", "ProtocolException señala una violación de la especificación HTTP: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);

        } catch (ConnectException e) {
            logger.error("[ConnectException][QueryUserGroup]  No se pudo establecer una conexion con el servicio Rest QueryUserGroup: " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_NOT_FOUND.getCode(), "ConnectException", "No se pudo establecer una conexion con el servicio Rest QueryUserGroup: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);
        } catch (ConnectTimeoutException e) {
            logger.error("[ConnectTimeoutException][QueryUserGroup]  Tiempo de espera agotado para respuesta de servicio QueryUserGroup: " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_ERROR.getCode(), "ConnectTimeoutException", "Tiempo de espera agotado para respuesta de servicio QueryUserGroup: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);
        }
        return output;
    }


    public synchronized boolean consumerCreateUser(QueryUser queryUser, String isdn, String urlService, String usuario,
                                                   long idAuditoriaHistorial, String token) throws IOException, com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException {
        long ini = System.currentTimeMillis();
        boolean output;
        String urlConsumer = UtilMap.replaceParametros(urlService, UtilMap.mapear(Parametros.restUrlParametrosCreateUser));
        logger.debug(" CreateUser Url service: " + urlConsumer);
        try {
            String body = Parametros.restUrlBodyCreateUser;
            body = body.replaceAll("%USERNAME%", queryUser.getUsername());
            body = body.replaceAll("%ESTADO%", String.valueOf(queryUser.getEnabled()));
            body = body.replaceAll("%NOMBRES%", queryUser.getFirstName().toUpperCase());
            body = body.replaceAll("%APELLIDOS%", queryUser.getLastName().toUpperCase());
            body = body.replaceAll("%EMAIL%", queryUser.getEmail());
            body = body.replaceAll("%DEALER%", queryUser.getAttributes().getEhCodeParent().get(0));
            body = body.replaceAll("%LIMITE%", queryUser.getAttributes().getMaxActivations().get(0));
            body = body.replaceAll("%MSISDN%", queryUser.getAttributes().getMsisdn().get(0));
            body = body.replaceAll("%CATEGORIA%", queryUser.getAttributes().getPreactivationRecyclePeriod().get(0));
            output = gestionClientRest.createUser(urlConsumer, token, body);
            logger.debug("Request CreateUser Json: " + body);
            logger.debug("Response CreateUser Json: " + output);
            long fin = System.currentTimeMillis();
            logger.info("[Serivicio RESTful: CreateUser ] Tiempo de respuesta: " + (fin - ini)
                    + " milisegundos");
            if (Parametros.saveRequestResponse) {
                try {
                    auditoria.saveXml(isdn, usuario, urlConsumer, "CreateUser", body, String.valueOf(output),
                            idAuditoriaHistorial, (fin - ini), (fin - ini));
                } catch (Exception e) {
                    logger.warn("No se logro registrar los request y response del servicio RESTful CreateUser: ", e);
                }
            }

        } catch (UnknownHostException e) {
            logger.error("[UnknownHostException][CreateUser] No se pudo alcanzar la url: " + Parametros.restUrlCreateUser + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_ERROR.getCode(), "UnknownHostException", "[CreateUser] No se pudo alcanzar la url: " + Parametros.restUrlCreateUser + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);

        } catch (NoHttpResponseException e) {
            logger.error("[NoHttpResponseException][CreateUser] El servidor web puede recibir solicitudes pero no puede procesarlas : " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_NOT_FOUND.getCode(), "NoHttpResponseException", "En algunas circunstancias, generalmente cuando se encuentra bajo una gran carga, el servidor web puede recibir solicitudes pero no puede procesarlas. La falta de recursos suficientes como subprocesos de trabajo es un buen ejemplo: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);

        } catch (ConnectionPoolTimeoutException e) {
            logger.error("[ConnectionPoolTimeoutException][CreateUser] La excepción indica que el administrador de conexiones no puede obtener una conexión libre del grupo de conexiones dentro del período de tiempo dado: " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_NOT_FOUND.getCode(), "ConnectionPoolTimeoutException", "Esta excepción solo puede ocurrir cuando se usa el administrador de conexiones multiproceso. La excepción indica que el administrador de conexiones no puede obtener una conexión libre del grupo de conexiones dentro del período de tiempo dado: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);

        } catch (ProtocolException e) {
            logger.error("[ProtocolException][CreateUser]  ProtocolException señala una violación de la especificación HTTP: " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_NOT_FOUND.getCode(), "ProtocolException", "ProtocolException señala una violación de la especificación HTTP: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);

        } catch (ConnectException e) {
            logger.error("[ConnectException][CreateUser]  No se pudo establecer una conexion con el servicio Rest QueryUserGroup: " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_NOT_FOUND.getCode(), "ConnectException", "No se pudo establecer una conexion con el servicio Rest CreateUser: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);
        } catch (ConnectTimeoutException e) {
            logger.error("[ConnectTimeoutException][CreateUser]  Tiempo de espera agotado para respuesta de servicio QueryUserGroup: " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_ERROR.getCode(), "ConnectTimeoutException", "Tiempo de espera agotado para respuesta de servicio CreateUser: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);
        }
        return output;
    }

    public synchronized boolean consumerUpdateUser(QueryUser queryUser, String userID, String isdn, String urlService, String usuario,
                                                   long idAuditoriaHistorial, String token) throws IOException, com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException {
        long ini = System.currentTimeMillis();
        boolean output;
        String urlConsumer = UtilMap.replaceParametros(urlService, UtilMap.mapear(Parametros.restUrlParametrosUpdateUser));
        urlConsumer = urlConsumer.replace("{user-id}", userID);
        logger.debug("Url service: " + urlConsumer);
        try {
            String body = Parametros.restUrlBodyUpdateUser;
            body = body.replaceAll("%USERNAME%", queryUser.getUsername());
            body = body.replaceAll("%ESTADO%", String.valueOf(queryUser.getEnabled()));
            body = body.replaceAll("%NOMBRES%", queryUser.getFirstName().toUpperCase());
            body = body.replaceAll("%APELLIDOS%", queryUser.getLastName().toUpperCase());
            body = body.replaceAll("%EMAIL%", queryUser.getEmail());
            body = body.replaceAll("%DEALER%", queryUser.getAttributes().getEhCodeParent().get(0));
            body = body.replaceAll("%LIMITE%", queryUser.getAttributes().getMaxActivations().get(0));
            body = body.replaceAll("%MSISDN%", queryUser.getAttributes().getMsisdn().get(0));
            body = body.replaceAll("%CATEGORIA%", queryUser.getAttributes().getPreactivationRecyclePeriod().get(0));
            output = gestionClientRest.updateUser(urlConsumer, token, body);
            logger.debug("Request UpdateUser Json: " + body);
            logger.debug("Response UpdateUser Json: " + output);
            long fin = System.currentTimeMillis();
            logger.info("[Serivicio RESTful: UpdateUser ] Tiempo de respuesta: " + (fin - ini)
                    + " milisegundos");
            if (Parametros.saveRequestResponse) {
                try {
                    auditoria.saveXml(isdn, usuario, urlConsumer, "UpdateUser", body, String.valueOf(output),
                            idAuditoriaHistorial, (fin - ini), (fin - ini));
                } catch (Exception e) {
                    logger.warn("No se logro registrar los request y response del servicio RESTful UpdateUser: ", e);
                }
            }

        } catch (UnknownHostException e) {
            logger.error("[UnknownHostException][UpdateUser] No se pudo alcanzar la url: " + Parametros.restUrlUpdateUser + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_ERROR.getCode(), "UnknownHostException", "[UpdateUser] No se pudo alcanzar la url: " + Parametros.restUrlUpdateUser + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);

        } catch (NoHttpResponseException e) {
            logger.error("[NoHttpResponseException][UpdateUser] El servidor web puede recibir solicitudes pero no puede procesarlas : " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_NOT_FOUND.getCode(), "NoHttpResponseException", "En algunas circunstancias, generalmente cuando se encuentra bajo una gran carga, el servidor web puede recibir solicitudes pero no puede procesarlas. La falta de recursos suficientes como subprocesos de trabajo es un buen ejemplo: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);

        } catch (ConnectionPoolTimeoutException e) {
            logger.error("[ConnectionPoolTimeoutException][UpdateUser] La excepción indica que el administrador de conexiones no puede obtener una conexión libre del grupo de conexiones dentro del período de tiempo dado: " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_NOT_FOUND.getCode(), "ConnectionPoolTimeoutException", "Esta excepción solo puede ocurrir cuando se usa el administrador de conexiones multiproceso. La excepción indica que el administrador de conexiones no puede obtener una conexión libre del grupo de conexiones dentro del período de tiempo dado: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);

        } catch (ProtocolException e) {
            logger.error("[ProtocolException][UpdateUser]  ProtocolException señala una violación de la especificación HTTP: " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_NOT_FOUND.getCode(), "ProtocolException", "ProtocolException señala una violación de la especificación HTTP: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);

        } catch (ConnectException e) {
            logger.error("[ConnectException][UpdateUser]  No se pudo establecer una conexion con el servicio Rest QueryGroup: " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_NOT_FOUND.getCode(), "ConnectException", "No se pudo establecer una conexion con el servicio Rest UpdateUser: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);
        } catch (ConnectTimeoutException e) {
            logger.error("[ConnectTimeoutException][UpdateUser]  Tiempo de espera agotado para respuesta de servicio QueryGroup: " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_ERROR.getCode(), "ConnectTimeoutException", "Tiempo de espera agotado para respuesta de servicio UpdateUser: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);
        }
        return output;
    }


    public synchronized boolean consumerAddGroupUser(String userID, String groupID, String isdn, String urlService, String usuario,
                                                     long idAuditoriaHistorial, String token) throws IOException, com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException {
        long ini = System.currentTimeMillis();
        boolean output;
        String urlConsumer = UtilMap.replaceParametros(urlService, UtilMap.mapear(Parametros.restUrlParametrosAddGroupUser));
        urlConsumer = urlConsumer.replace("{user-id}", userID);
        urlConsumer = urlConsumer.replace("{group-id}", groupID);
        logger.debug("Url service: " + urlConsumer);
        try {

            output = gestionClientRest.addGroupUser(urlConsumer, token);
            logger.debug("Response AddGroupUser Json: " + output);
            long fin = System.currentTimeMillis();
            logger.info("[Serivicio RESTful: AddGroupUser ] Tiempo de respuesta: " + (fin - ini)
                    + " milisegundos");
            if (Parametros.saveRequestResponse) {
                try {
                    auditoria.saveXml(isdn, usuario, urlConsumer, "AddGroupUser", "", String.valueOf(output),
                            idAuditoriaHistorial, (fin - ini), (fin - ini));
                } catch (Exception e) {
                    logger.warn("No se logro registrar los request y response del servicio RESTful AddGroupUser: ", e);
                }
            }

        } catch (UnknownHostException e) {
            logger.error("[UnknownHostException][AddGroupUser] No se pudo alcanzar la url: " + Parametros.restUrlAddGroupUser + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_ERROR.getCode(), "UnknownHostException", "[AddGroupUser] No se pudo alcanzar la url: " + Parametros.restUrlAddGroupUser + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);

        } catch (NoHttpResponseException e) {
            logger.error("[NoHttpResponseException][AddGroupUser] El servidor web puede recibir solicitudes pero no puede procesarlas : " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_NOT_FOUND.getCode(), "NoHttpResponseException", "En algunas circunstancias, generalmente cuando se encuentra bajo una gran carga, el servidor web puede recibir solicitudes pero no puede procesarlas. La falta de recursos suficientes como subprocesos de trabajo es un buen ejemplo: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);

        } catch (ConnectionPoolTimeoutException e) {
            logger.error("[ConnectionPoolTimeoutException][AddGroupUser] La excepción indica que el administrador de conexiones no puede obtener una conexión libre del grupo de conexiones dentro del período de tiempo dado: " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_NOT_FOUND.getCode(), "ConnectionPoolTimeoutException", "Esta excepción solo puede ocurrir cuando se usa el administrador de conexiones multiproceso. La excepción indica que el administrador de conexiones no puede obtener una conexión libre del grupo de conexiones dentro del período de tiempo dado: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);

        } catch (ProtocolException e) {
            logger.error("[ProtocolException][AddGroupUser]  ProtocolException señala una violación de la especificación HTTP: " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_NOT_FOUND.getCode(), "ProtocolException", "ProtocolException señala una violación de la especificación HTTP: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);

        } catch (ConnectException e) {
            logger.error("[ConnectException][AddGroupUser]  No se pudo establecer una conexion con el servicio Rest QueryGroup: " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_NOT_FOUND.getCode(), "ConnectException", "No se pudo establecer una conexion con el servicio Rest AddGroupUser: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);
        } catch (ConnectTimeoutException e) {
            logger.error("[ConnectTimeoutException][AddGroupUser]  Tiempo de espera agotado para respuesta de servicio QueryGroup: " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_ERROR.getCode(), "ConnectTimeoutException", "Tiempo de espera agotado para respuesta de servicio AddGroupUser: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);
        }
        return output;
    }


    public synchronized boolean consumerDeleteGroupUser(String userID, String groupID, String isdn, String urlService, String usuario,
                                                        long idAuditoriaHistorial, String token) throws IOException, com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException {
        long ini = System.currentTimeMillis();
        boolean output;
        String urlConsumer = UtilMap.replaceParametros(urlService, UtilMap.mapear(Parametros.restUrlParametrosDeleteGroupUser));
        urlConsumer = urlConsumer.replace("{user-id}", userID);
        urlConsumer = urlConsumer.replace("{group-id}", groupID);
        logger.debug("Url service: " + urlConsumer);
        try {

            output = gestionClientRest.deletGroupUser(urlConsumer, token);
            logger.debug("Response DeleteGroupUser Json: " + output);
            long fin = System.currentTimeMillis();
            logger.info("[Serivicio RESTful: DeleteGroupUser ] Tiempo de respuesta: " + (fin - ini)
                    + " milisegundos");
            if (Parametros.saveRequestResponse) {
                try {
                    auditoria.saveXml(isdn, usuario, urlConsumer, "DeleteGroupUser", "", String.valueOf(output),
                            idAuditoriaHistorial, (fin - ini), (fin - ini));
                } catch (Exception e) {
                    logger.warn("No se logro registrar los request y response del servicio RESTful DeleteGroupUser: ", e);
                }
            }

        } catch (UnknownHostException e) {
            logger.error("[UnknownHostException][DeleteGroupUser] No se pudo alcanzar la url: " + Parametros.restUrlDeleteGroupUser + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_ERROR.getCode(), "UnknownHostException", "[DeleteGroupUser] No se pudo alcanzar la url: " + Parametros.restUrlDeleteGroupUser + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);

        } catch (NoHttpResponseException e) {
            logger.error("[NoHttpResponseException][DeleteGroupUser] El servidor web puede recibir solicitudes pero no puede procesarlas : " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_NOT_FOUND.getCode(), "NoHttpResponseException", "En algunas circunstancias, generalmente cuando se encuentra bajo una gran carga, el servidor web puede recibir solicitudes pero no puede procesarlas. La falta de recursos suficientes como subprocesos de trabajo es un buen ejemplo: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);

        } catch (ConnectionPoolTimeoutException e) {
            logger.error("[ConnectionPoolTimeoutException][DeleteGroupUser] La excepción indica que el administrador de conexiones no puede obtener una conexión libre del grupo de conexiones dentro del período de tiempo dado: " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_NOT_FOUND.getCode(), "ConnectionPoolTimeoutException", "Esta excepción solo puede ocurrir cuando se usa el administrador de conexiones multiproceso. La excepción indica que el administrador de conexiones no puede obtener una conexión libre del grupo de conexiones dentro del período de tiempo dado: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);

        } catch (ProtocolException e) {
            logger.error("[ProtocolException][DeleteGroupUser]  ProtocolException señala una violación de la especificación HTTP: " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_NOT_FOUND.getCode(), "ProtocolException", "ProtocolException señala una violación de la especificación HTTP: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);

        } catch (ConnectException e) {
            logger.error("[ConnectException][DeleteGroupUser]  No se pudo establecer una conexion con el servicio Rest QueryGroup: " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_NOT_FOUND.getCode(), "ConnectException", "No se pudo establecer una conexion con el servicio Rest DeleteGroupUser: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);
        } catch (ConnectTimeoutException e) {
            logger.error("[ConnectTimeoutException][DeleteGroupUser]  Tiempo de espera agotado para respuesta de servicio QueryGroup: " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_ERROR.getCode(), "ConnectTimeoutException", "Tiempo de espera agotado para respuesta de servicio DeleteGroupUser: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);
        }
        return output;
    }


    public synchronized boolean consumerDeleteUser(String userID, String isdn, String urlService, String usuario,
                                                   long idAuditoriaHistorial, String token) throws IOException, com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException {
        long ini = System.currentTimeMillis();
        boolean output;
        String urlConsumer = UtilMap.replaceParametros(urlService, UtilMap.mapear(Parametros.restUrlParametrosDeleteUser));
        urlConsumer = urlConsumer.replace("{user-id}", userID);
        logger.debug("Url service: " + urlConsumer);
        try {
            output = gestionClientRest.deletUser(urlConsumer, token);
            logger.debug("Response DeleteUser Json: " + output);
            long fin = System.currentTimeMillis();
            logger.info("[Serivicio RESTful: DeleteUser ] Tiempo de respuesta: " + (fin - ini)
                    + " milisegundos");
            if (Parametros.saveRequestResponse) {
                try {
                    auditoria.saveXml(isdn, usuario, urlConsumer, "DeleteUser", "", String.valueOf(output),
                            idAuditoriaHistorial, (fin - ini), (fin - ini));
                } catch (Exception e) {
                    logger.warn("No se logro registrar los request y response del servicio RESTful DeleteUser: ", e);
                }
            }

        } catch (UnknownHostException e) {
            logger.error("[UnknownHostException][DeleteUser] No se pudo alcanzar la url: " + Parametros.restUrlDeleteUser + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_ERROR.getCode(), "UnknownHostException", "[DeleteUser] No se pudo alcanzar la url: " + Parametros.restUrlDeleteUser + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);

        } catch (NoHttpResponseException e) {
            logger.error("[NoHttpResponseException][DeleteUser] El servidor web puede recibir solicitudes pero no puede procesarlas : " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_NOT_FOUND.getCode(), "NoHttpResponseException", "En algunas circunstancias, generalmente cuando se encuentra bajo una gran carga, el servidor web puede recibir solicitudes pero no puede procesarlas. La falta de recursos suficientes como subprocesos de trabajo es un buen ejemplo: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);

        } catch (ConnectionPoolTimeoutException e) {
            logger.error("[ConnectionPoolTimeoutException][DeleteUser] La excepción indica que el administrador de conexiones no puede obtener una conexión libre del grupo de conexiones dentro del período de tiempo dado: " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_NOT_FOUND.getCode(), "ConnectionPoolTimeoutException", "Esta excepción solo puede ocurrir cuando se usa el administrador de conexiones multiproceso. La excepción indica que el administrador de conexiones no puede obtener una conexión libre del grupo de conexiones dentro del período de tiempo dado: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);

        } catch (ProtocolException e) {
            logger.error("[ProtocolException][DeleteUser]  ProtocolException señala una violación de la especificación HTTP: " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_NOT_FOUND.getCode(), "ProtocolException", "ProtocolException señala una violación de la especificación HTTP: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);

        } catch (ConnectException e) {
            logger.error("[ConnectException][DeleteUser]  No se pudo establecer una conexion con el servicio Rest QueryGroup: " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_NOT_FOUND.getCode(), "ConnectException", "No se pudo establecer una conexion con el servicio Rest DeleteUser: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);
        } catch (ConnectTimeoutException e) {
            logger.error("[ConnectTimeoutException][DeleteUser]  Tiempo de espera agotado para respuesta de servicio QueryGroup: " + e.getMessage());
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(StatusResponse.HTTP_ERROR.getCode(), "ConnectTimeoutException", "Tiempo de espera agotado para respuesta de servicio DeleteUser: " + e.getMessage(), "ERROR");
            throw new com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException(excepcionAux.getMensaje(), excepcionAux);
        }
        return output;
    }


}
