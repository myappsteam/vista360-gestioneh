package com.myapps.vista_ccc.gestion_eh.business;


import com.myapps.vista_ccc.gestion_eh.dao.VcDealerDAO;
import com.myapps.vista_ccc.gestion_eh.entity.VcDealerEntity;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
public class VcDealerBL implements Serializable {

    private static final long serialVersionUID = 1L;
    private static Logger log = Logger.getLogger(VcDealerBL.class);


    @Inject
    private VcDealerDAO dao;

    public void save(VcDealerEntity nodo) throws Exception {
        dao.save(nodo);
    }

    public VcDealerEntity getVcDealer(long idNodo) throws Exception {
        return dao.get(idNodo);
    }

    public void update(VcDealerEntity nodo) throws Exception {
        dao.update(nodo);
    }


    public List<VcDealerEntity> getList() throws Exception {
        return dao.getList();
    }


    public VcDealerEntity getDealerForEh(String eh) {
        VcDealerEntity valor = null;
        try {
            valor = dao.getForEH(eh);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return valor;
    }

}
