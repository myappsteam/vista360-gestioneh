package com.myapps.vista_ccc.gestion_eh.business;


import com.myapps.vista_ccc.gestion_eh.dao.VcSupervisorDealerZonasDAO;
import com.myapps.vista_ccc.gestion_eh.entity.VcSupervisorDealerZonasEntity;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
public class VcSupervisorDealerZOnasBL implements Serializable {

    private static final long serialVersionUID = 1L;
    private static Logger log = Logger.getLogger(VcSupervisorDealerZOnasBL.class);


    @Inject
    private VcSupervisorDealerZonasDAO dao;

    public void save(VcSupervisorDealerZonasEntity nodo) throws Exception {
        dao.save(nodo);
    }

    public VcSupervisorDealerZonasEntity getSupervisorDealerZonas(long idNodo) throws Exception {
        return dao.get(idNodo);
    }

    public void update(VcSupervisorDealerZonasEntity nodo) throws Exception {
        dao.update(nodo);
    }

    public void remove(VcSupervisorDealerZonasEntity nodo) throws Exception {
        dao.remove(nodo);
    }

    public List<VcSupervisorDealerZonasEntity> getList() throws Exception {
        return dao.getList();
    }

    public List<VcSupervisorDealerZonasEntity> getListForSupervisor(long idSupervisor) {
        List<VcSupervisorDealerZonasEntity> lista = new ArrayList<>();
        try {
            lista = dao.getListforSupervisor(idSupervisor);
        } catch (Exception e) {
            log.error("Exception: " + e.getMessage());
        }
        return lista;
    }

    public VcSupervisorDealerZonasEntity getEliminarIDPermisoSupervisor(long idPermisoSupervisor) {
        VcSupervisorDealerZonasEntity valor = null;
        try {
            valor = dao.eliminarSupervisorPermisoBL(idPermisoSupervisor);
        } catch (Exception e) {
            log.error("Exception: " + e.getMessage());
        }
        return valor;
    }

    public List<VcSupervisorDealerZonasEntity> getIdSupVenDelete(long idPermisoSupervisor) {
        List<VcSupervisorDealerZonasEntity> list = new ArrayList<>();
        try {
            list = dao.getIdSupervisorVendedorDelete(idPermisoSupervisor);
        } catch (Exception e) {
            log.error("Exception: " + e.getMessage());
        }
        return list;
    }

    public void removeDataSupDealerZona(VcSupervisorDealerZonasEntity nodo) throws Exception {
        dao.removeIdZonaDealer(nodo);
    }

}
