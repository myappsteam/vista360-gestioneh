package com.myapps.vista_ccc.gestion_eh.servicio;

import com.myapps.vista_ccc.gestion_eh.servicio.model.AceptadosGenerica;
import com.myapps.vista_ccc.gestion_eh.servicio.model.ExcepcionGenerica;
import com.myapps.vista_ccc.gestion_eh.servicio.model.Permisos;
import com.myapps.vista_ccc.gestion_eh.servicio.model.queryUserGroup.QueryUserGroup;
import com.myapps.vista_ccc.reset_pin.servicio.UtilMap;
import com.myapps.vista_ccc.util.Parametros;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class UtilEH {
    private static Logger logger = Logger.getLogger(UtilMap.class);


    public static boolean obtenerPermiso(String id, List<QueryUserGroup> permisosUsuario) {
        boolean valor = false;
        if (permisosUsuario != null) {
            for (QueryUserGroup permiso : permisosUsuario) {
                if (permiso.getId().equals(id)) {
                    valor = true;
                    break;
                }
            }
        }
        return valor;
    }

    public static String obtenerNombre(String id, String nombre) {
        String valor = nombre;
        List<Permisos> listaPermisos = UtilMap.armarPermisos(Parametros.ehPermisos);
        if (listaPermisos != null) {
            for (Permisos permiso : listaPermisos) {
                if (id.equals(permiso.getId())) {
                    valor = permiso.getNombre();
                }
            }
        }
        return valor;
    }

    public static List<ExcepcionGenerica> cargarExcepciones(String cadena) {
        List<ExcepcionGenerica> lista = new ArrayList<>();
        try {
            if (cadena != null) {
                String[] arrayOne = cadena.split("\\|");
                for (String string : arrayOne) {
                    String[] arrayTwo = string.split(";");
                    if (arrayTwo.length == 4) {
                        lista.add(new ExcepcionGenerica(Integer.valueOf(arrayTwo[0]), arrayTwo[1], arrayTwo[2], arrayTwo[3]));
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error al cargar la lista de Excepciones: " + cadena);
        }

        return lista;
    }

    public static List<AceptadosGenerica> cargarAcepatados(String cadena) {
        List<AceptadosGenerica> lista = new ArrayList<>();
        try {
            if (cadena != null) {
                String[] arrayOne = cadena.split("\\|");
                for (String string : arrayOne) {
                    String[] arrayTwo = string.split(";");
                    if (arrayTwo.length == 2) {
                        lista.add(new AceptadosGenerica(Integer.valueOf(arrayTwo[0]), arrayTwo[1]));
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error al cargar la lista de Aceptados: " + cadena);
        }

        return lista;
    }
}
