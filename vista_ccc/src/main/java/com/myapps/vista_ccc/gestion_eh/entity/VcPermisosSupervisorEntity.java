package com.myapps.vista_ccc.gestion_eh.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "VC_PERMISOS_SUPERVISOR")
public class VcPermisosSupervisorEntity implements Serializable {
    private long idPermisosSupervisor;
    private Long usuarioId;
    private Long grupoId;
    private Time fecha;
    private String accion;
    private boolean estado;
    private Collection<VcSupervisorDealerZonasEntity> vcSupervisorDealerZonasByIdPermisosSupervisor;

    @Id
    @Column(name = "ID_PERMISOS_SUPERVISOR")
    @SequenceGenerator(name = "pk_sequence", sequenceName = "SEQ_VC_SUPERVISOR_ID_SUPERVISO ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence")
    public long getIdPermisosSupervisor() {
        return idPermisosSupervisor;
    }

    public void setIdPermisosSupervisor(long idPermisosSupervisor) {
        this.idPermisosSupervisor = idPermisosSupervisor;
    }

    @Basic
    @Column(name = "USUARIO_ID")
    public Long getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Long usuarioId) {
        this.usuarioId = usuarioId;
    }

    @Basic
    @Column(name = "GRUPO_ID")
    public Long getGrupoId() {
        return grupoId;
    }

    public void setGrupoId(Long grupoId) {
        this.grupoId = grupoId;
    }

    @Basic
    @Column(name = "FECHA")
    public Time getFecha() {
        return fecha;
    }

    public void setFecha(Time fecha) {
        this.fecha = fecha;
    }

    @Basic
    @Column(name = "ACCION")
    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    @Basic
    @Column(name = "ESTADO")
    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VcPermisosSupervisorEntity that = (VcPermisosSupervisorEntity) o;
        return idPermisosSupervisor == that.idPermisosSupervisor &&
                Objects.equals(usuarioId, that.usuarioId) &&
                Objects.equals(grupoId, that.grupoId) &&
                Objects.equals(fecha, that.fecha) &&
                Objects.equals(accion, that.accion) &&
                Objects.equals(estado, that.estado);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idPermisosSupervisor, usuarioId, grupoId, fecha, accion, estado);
    }

    @OneToMany(mappedBy = "vcPermisosSupervisorByIdPermisosSupervisor", fetch=FetchType.EAGER)
    public Collection<VcSupervisorDealerZonasEntity> getVcSupervisorDealerZonasByIdPermisosSupervisor() {
        return vcSupervisorDealerZonasByIdPermisosSupervisor;
    }

    public void setVcSupervisorDealerZonasByIdPermisosSupervisor(Collection<VcSupervisorDealerZonasEntity> vcSupervisorDealerZonasByIdPermisosSupervisor) {
        this.vcSupervisorDealerZonasByIdPermisosSupervisor = vcSupervisorDealerZonasByIdPermisosSupervisor;
    }
}
