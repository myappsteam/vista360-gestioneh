package com.myapps.vista_ccc.gestion_eh.dao;

import com.myapps.vista_ccc.gestion_eh.entity.VcDealerZonasEntity;
import org.apache.log4j.Logger;

import javax.annotation.Resource;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
public class VcDealerZonasDAO implements Serializable {
    private static Logger log = Logger.getLogger(VcDealerZonasDAO.class);

    private static final long serialVersionUID = 1L;

    @PersistenceContext(unitName = "vista_ccc")
    private transient EntityManager entityManager;

    @Resource
    private transient UserTransaction transaction;

    public void save(VcDealerZonasEntity dato) throws Exception {
        transaction.begin();
        entityManager.persist(dato);
        transaction.commit();
    }

    public VcDealerZonasEntity get(long id) throws Exception {
        return entityManager.find(VcDealerZonasEntity.class, id);
    }

    public void update(VcDealerZonasEntity dato) throws Exception {
        transaction.begin();
        entityManager.merge(dato);
        transaction.commit();
    }

    public void remove(VcDealerZonasEntity dato) throws Exception {
        transaction.begin();
        entityManager.joinTransaction();
        try {
            entityManager.remove(entityManager.contains(dato) ? dato : entityManager.merge(dato));
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    public List<VcDealerZonasEntity> getList() throws Exception {
        return entityManager.createQuery("SELECT us FROM VcDealerZonasEntity us  Order by us.idDealerZonas").getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<VcDealerZonasEntity> getListForDealer(long idDealer, long idSupervisor) {
        List<VcDealerZonasEntity> lista = new ArrayList<>();
        try {
            String sql = "SELECT DZ.* FROM VC_DEALER_ZONAS DZ inner join VC_SUPERVISOR_DEALER_ZONAS SDZ ON DZ.ID_DEALER_ZONAS = SDZ.ID_DEALER_ZONAS WHERE DZ.ID_DEALER = :idDealer AND SDZ.ID_PERMISOS_SUPERVISOR = :idSupervisor AND SDZ.ESTADO = 1";
            Query query = entityManager.createNativeQuery(sql, VcDealerZonasEntity.class);
            query.setParameter("idDealer", idDealer);
            query.setParameter("idSupervisor", idSupervisor);
            lista = query.getResultList();
        } catch (NoResultException e) {
            log.error("No se encontraron resultados para el Dealer: " + idDealer + " y Supervisro: " + idSupervisor + "Exception:" + e.getMessage());
        }
        return lista;
    }


    @SuppressWarnings("unchecked")
    public List<VcDealerZonasEntity> getListForDealer(long idDealer) {
        List<VcDealerZonasEntity> lista = new ArrayList<>();
        try {
            String sql = "SELECT * from VC_DEALER_ZONAS WHERE ID_DEALER=:idDealer";
            Query query = entityManager.createNativeQuery(sql, VcDealerZonasEntity.class);
            query.setParameter("idDealer", idDealer);
            lista = query.getResultList();
        } catch (NoResultException e) {
            log.error("No se encontraron resultados para el Dealer: " + idDealer + " Exception:" + e.getMessage());
        }
        return lista;
    }


}
