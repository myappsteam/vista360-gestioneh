package com.myapps.vista_ccc.gestion_eh.business;


import com.google.gson.Gson;
import com.myapps.user.business.GrupoAdBL;
import com.myapps.user.business.UsuarioBL;
import com.myapps.user.model.MuGrupoAd;
import com.myapps.user.model.MuUsuario;
import com.myapps.vista_ccc.gestion_eh.servicio.GestionEhRestConsumer;
import com.myapps.vista_ccc.gestion_eh.servicio.model.queryGroup.QueryGroup;
import com.myapps.vista_ccc.reset_pin.error.RestExcepcion;
import com.myapps.vista_ccc.reset_pin.model.gson.QueryUser;
import com.myapps.vista_ccc.reset_pin.model.gson.TokenGson;
import com.myapps.vista_ccc.reset_pin.servicio.ResetPinRestConsumer;
import com.myapps.vista_ccc.util.Parametros;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.net.URISyntaxException;

@SuppressWarnings("unchecked")
@Named
public class AltaUsuarioBusiness implements Serializable {
    private static final long serialVersionUID = 1L;
    private static Logger logger = Logger.getLogger(AltaUsuarioBusiness.class);


    @Inject
    private UsuarioBL usuarioBL;
    @Inject
    public GrupoAdBL grupoAdBL;
    @Inject
    private ResetPinRestConsumer resetPinRestConsumer;
    @Inject
    private GestionEhRestConsumer gestionEhRestConsumer;

    public MuUsuario obtenerUsuario() {
        MuUsuario usuario = null;
        try {
            usuario = usuarioBL.obtenerUsuario();
            if (usuario == null) {
                MuGrupoAd grupoAd = grupoAdBL.obtenerGrupo();
                if (grupoAd != null) {
                    usuario = new MuUsuario();
                    usuario.setUsuarioId(grupoAd.getGrupoId());
                    usuario.setNombre("GRUPO");
                    usuario.setLogin(grupoAd.getNombre());
                    usuario.setMuRol(grupoAd.getMuRol());
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return usuario;
    }

    public TokenGson getToken(String usuario, long idAuditoriaHistorial)
            throws com.myapps.vista_ccc.white_list.error.HttpStatusException, IOException, RestExcepcion, com.myapps.vista_ccc.reset_pin.error.HttpStatusException {
        TokenGson tokenGson;
        try {
            Gson gson = new Gson();
            String token = resetPinRestConsumer.consumerToken(Parametros.restUrlToken, usuario, idAuditoriaHistorial);
            tokenGson = gson.fromJson(token, TokenGson.class);
        } catch (com.myapps.vista_ccc.white_list.error.HttpStatusAcceptException e) {
            logger.info("Respuesta de servicio getToken diferente de OK...");
            throw new RestExcepcion(e.getJson());
        }
        return tokenGson;
    }

    public QueryUser getQueryUser(String usuario, long idAuditoriaHistorial, String codigo, String token)
            throws com.myapps.vista_ccc.white_list.error.HttpStatusException, IOException, RestExcepcion, URISyntaxException, com.myapps.vista_ccc.reset_pin.error.HttpStatusException {
        QueryUser queryUser = null;
        try {
            Gson gson = new Gson();
            String user = resetPinRestConsumer.consumerQueryUser(Parametros.restUrlQueryUSer, usuario, idAuditoriaHistorial, codigo, token);
            QueryUser[] queryUserList = gson.fromJson(user, QueryUser[].class);
            if (queryUserList != null && queryUserList.length > 0) {
                for (QueryUser query : queryUserList) {
                    if (codigo.equals(query.getUsername())) {
                        queryUser = query;
                        break;
                    }
                }

            }
        } catch (com.myapps.vista_ccc.white_list.error.HttpStatusAcceptException e) {
            logger.info("Respuesta de servicio Query User diferente de OK...");
            throw new RestExcepcion(e.getJson());
        }
        return queryUser;
    }

    public QueryGroup[] getQueryGroup(String usuario, long idAuditoriaHistorial, String token)
            throws URISyntaxException, com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException, IOException {
        QueryGroup[] queryGroups = null;
        Gson gson = new Gson();
        String user = gestionEhRestConsumer.consumerQueryGroup(Parametros.restUrlQueryGroup, usuario, idAuditoriaHistorial, token);
        QueryGroup[] queryGroups1ista = gson.fromJson(user, QueryGroup[].class);
        if (queryGroups1ista != null) {
            queryGroups = queryGroups1ista;
        }
        return queryGroups;
    }


    public boolean createUser(QueryUser queryUser, String msisdn, String usuario, long idAuditoriaHistorial, String token)
            throws IOException, com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException {
        return gestionEhRestConsumer.consumerCreateUser(queryUser, msisdn, Parametros.restUrlCreateUser, usuario, idAuditoriaHistorial, token);

    }

    public boolean updateUser(QueryUser queryUser, String userID, String msisdn, String usuario, long idAuditoriaHistorial, String token)
            throws IOException, com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException {
        return gestionEhRestConsumer.consumerUpdateUser(queryUser, userID, msisdn, Parametros.restUrlUpdateUser, usuario, idAuditoriaHistorial, token);

    }

    public boolean addGroupUser(String userID, String groupID, String msisdn, String usuario, long idAuditoriaHistorial, String token)
            throws IOException, com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException {
        return gestionEhRestConsumer.consumerAddGroupUser(userID, groupID, msisdn, Parametros.restUrlAddGroupUser, usuario, idAuditoriaHistorial, token);
    }


}
