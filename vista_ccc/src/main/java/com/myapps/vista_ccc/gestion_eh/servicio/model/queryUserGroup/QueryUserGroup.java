package com.myapps.vista_ccc.gestion_eh.servicio.model.queryUserGroup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class QueryUserGroup implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("path")
    @Expose
    private String path;
    private final static long serialVersionUID = -4362950959431585116L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public QueryUserGroup(String id, String name, String path) {
        this.id = id;
        this.name = name;
        this.path = path;
    }

    public QueryUserGroup() {
    }
}