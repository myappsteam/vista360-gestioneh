package com.myapps.vista_ccc.gestion_eh.dao;

import com.myapps.vista_ccc.gestion_eh.entity.VcZonasEntity;
import org.apache.log4j.Logger;

import javax.annotation.Resource;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
public class VcZonasDAO implements Serializable {
    private static Logger log = Logger.getLogger(VcZonasDAO.class);

    private static final long serialVersionUID = 1L;

    @PersistenceContext(unitName = "vista_ccc")
    private transient EntityManager entityManager;

    @Resource
    private transient UserTransaction transaction;

    public void save(VcZonasEntity dato) throws Exception {
        transaction.begin();
        entityManager.persist(dato);
        transaction.commit();
    }

    public VcZonasEntity get(long id) throws Exception {
        return entityManager.find(VcZonasEntity.class, id);
    }

    public void update(VcZonasEntity dato) throws Exception {
        transaction.begin();
        entityManager.merge(dato);
        transaction.commit();
    }

    @SuppressWarnings("unchecked")
    public List<VcZonasEntity> getList() throws Exception {
        return entityManager.createQuery("SELECT us FROM VcZonasEntity us WHERE us.estado=true Order by us.nombre").getResultList();
    }

    @SuppressWarnings("unchecked")
    public VcZonasEntity getZonaForNombre(String nombre, long idSupervisor) throws Exception {
        VcZonasEntity vcZonasEntity = null;
        try {
            String sql = "SELECT * FROM VC_ZONAS WHERE UPPER(NOMBRE)=:nombre AND ID_ZONAS IN ( SELECT DZ.ID_ZONAS\n" +
                    "from VC_DEALER_ZONAS DZ inner join VC_SUPERVISOR_DEALER_ZONAS SDZ ON DZ.ID_DEALER_ZONAS = SDZ.ID_DEALER_ZONAS\n" +
                    "AND SDZ.ID_PERMISOS_SUPERVISOR = :idSupervisor)";
            Query query = entityManager.createNativeQuery(sql, VcZonasEntity.class);
            query.setParameter("nombre", nombre.toUpperCase());
            query.setParameter("idSupervisor", idSupervisor);
            vcZonasEntity = (VcZonasEntity) query.getSingleResult();
        } catch (NoResultException e) {
            log.warn("No se encontraron resultados para VcZonas: " + nombre + " Exception: " + e.getMessage());
        }
        return vcZonasEntity;
    }

    @SuppressWarnings("unchecked")
    public VcZonasEntity getZonaForNombre(String nombre) throws Exception {
        VcZonasEntity vcZonasEntity = null;
        try {
            String sql = "SELECT * FROM VC_ZONAS WHERE UPPER(NOMBRE)=:nombre";
            Query query = entityManager.createNativeQuery(sql, VcZonasEntity.class);
            query.setParameter("nombre", nombre.toUpperCase());
            vcZonasEntity = (VcZonasEntity) query.getSingleResult();
        } catch (NoResultException e) {
            log.warn("No se encontraron resultados para VcZonas: " + nombre + " Exception: " + e.getMessage());
        }
        return vcZonasEntity;
    }

    @SuppressWarnings("unchecked")
    public List<VcZonasEntity> getListPorDealer(Long idDealer) throws Exception {
        List<VcZonasEntity> list = new ArrayList<>();
        try {
            String sql = "select DISTINCT(F.ID_VC_ZONAS), F.NOMBRE, F.OBSERVACION, F.ESTADO from VC_DEALER_ZONAS D LEFT JOIN  VC_ZONAS F ON D.ID_VC_ZONAS = F.ID_VC_ZONAS WHERE D.ID_VC_DEALER = :idDealer AND F.ESTADO=1";
            Query query = entityManager.createNativeQuery(sql, VcZonasEntity.class);
            query.setParameter("idDealer", idDealer);
            list = query.getResultList();

        } catch (NoResultException e) {
            log.warn("No se encontraron resultados para Dealer: " + idDealer + " Exception: " + e.getMessage());
        }
        return list;
    }


}
