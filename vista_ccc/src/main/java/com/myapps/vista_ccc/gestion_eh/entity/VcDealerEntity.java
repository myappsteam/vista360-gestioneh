package com.myapps.vista_ccc.gestion_eh.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "VC_DEALER")
public class VcDealerEntity implements Serializable {
    private long idVcDealer;
    private String nombre;
    private String observacion;
    private boolean estado;
    private Collection<VcDealerZonasEntity> vcDealerZonasByIdVcDealer;
    private String eh;

    @Id
    @Column(name = "ID_DEALER")
    @SequenceGenerator(name = "pk_sequence", sequenceName = "SEQ_VC_DEALER_ID_DEALER", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence")
    public long getIdVcDealer() {
        return idVcDealer;
    }

    public void setIdVcDealer(long idVcDealer) {
        this.idVcDealer = idVcDealer;
    }

    @Basic
    @Column(name = "NOMBRE")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "OBSERVACION")
    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    @Basic
    @Column(name = "ESTADO")
    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VcDealerEntity that = (VcDealerEntity) o;
        return idVcDealer == that.idVcDealer &&
                estado == that.estado &&
                Objects.equals(nombre, that.nombre) &&
                Objects.equals(observacion, that.observacion);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idVcDealer, nombre, observacion, estado);
    }

    @OneToMany(mappedBy = "vcDealerByIdVcDealer", fetch=FetchType.EAGER)
    public Collection<VcDealerZonasEntity> getVcDealerZonasByIdVcDealer() {
        return vcDealerZonasByIdVcDealer;
    }

    public void setVcDealerZonasByIdVcDealer(Collection<VcDealerZonasEntity> vcDealerZonasByIdVcDealer) {
        this.vcDealerZonasByIdVcDealer = vcDealerZonasByIdVcDealer;
    }

    @Basic
    @Column(name = "EH")
    public String getEh() {
        return eh;
    }

    public void setEh(String eh) {
        this.eh = eh;
    }
}
