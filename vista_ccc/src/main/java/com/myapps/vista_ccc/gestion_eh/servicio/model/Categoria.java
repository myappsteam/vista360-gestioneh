package com.myapps.vista_ccc.gestion_eh.servicio.model;

import java.io.Serializable;

public class Categoria implements Serializable {
    private static final long serialVersionUID = 1L;

    private String nombre;
    private Long id;


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Categoria(String nombre, Long id) {
        this.nombre = nombre;
        this.id = id;
    }

    public Categoria() {
    }
}
