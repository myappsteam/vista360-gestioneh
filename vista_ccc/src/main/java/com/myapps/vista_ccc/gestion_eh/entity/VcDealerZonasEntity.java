package com.myapps.vista_ccc.gestion_eh.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "VC_DEALER_ZONAS")
public class VcDealerZonasEntity implements Serializable {
    private long idDealerZonas;
    private VcZonasEntity vcZonasByIdVcZonas;
    private VcDealerEntity vcDealerByIdVcDealer;
    private Collection<VcSupervisorDealerZonasEntity> vcSupervisorDealerZonasByIdDealerZonas;

    @Id
    @Column(name = "ID_DEALER_ZONAS")
    @SequenceGenerator(name = "pk_sequence", sequenceName = "SEQ_VC_DEALER_ZON_ID_DEALER_ZO", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence")
    public long getIdDealerZonas() {
        return idDealerZonas;
    }

    public void setIdDealerZonas(long idDealerZonas) {
        this.idDealerZonas = idDealerZonas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VcDealerZonasEntity that = (VcDealerZonasEntity) o;
        return idDealerZonas == that.idDealerZonas;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idDealerZonas);
    }

    @ManyToOne
    @JoinColumn(name = "ID_ZONAS", referencedColumnName = "ID_ZONAS")
    public VcZonasEntity getVcZonasByIdVcZonas() {
        return vcZonasByIdVcZonas;
    }

    public void setVcZonasByIdVcZonas(VcZonasEntity vcZonasByIdVcZonas) {
        this.vcZonasByIdVcZonas = vcZonasByIdVcZonas;
    }

    @ManyToOne
    @JoinColumn(name = "ID_DEALER", referencedColumnName = "ID_DEALER")
    public VcDealerEntity getVcDealerByIdVcDealer() {
        return vcDealerByIdVcDealer;
    }

    public void setVcDealerByIdVcDealer(VcDealerEntity vcDealerByIdVcDealer) {
        this.vcDealerByIdVcDealer = vcDealerByIdVcDealer;
    }

    @OneToMany(mappedBy = "vcDealerZonasByIdDealerZonas")
    public Collection<VcSupervisorDealerZonasEntity> getVcSupervisorDealerZonasByIdDealerZonas() {
        return vcSupervisorDealerZonasByIdDealerZonas;
    }

    public void setVcSupervisorDealerZonasByIdDealerZonas(Collection<VcSupervisorDealerZonasEntity> vcSupervisorDealerZonasByIdDealerZonas) {
        this.vcSupervisorDealerZonasByIdDealerZonas = vcSupervisorDealerZonasByIdDealerZonas;
    }
}
