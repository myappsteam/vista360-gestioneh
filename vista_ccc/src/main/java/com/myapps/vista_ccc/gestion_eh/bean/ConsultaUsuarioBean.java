package com.myapps.vista_ccc.gestion_eh.bean;


import com.myapps.user.bean.ControlerBitacora;
import com.myapps.user.ldap.DescriptorBitacora;
import com.myapps.user.model.MuUsuario;
import com.myapps.vista_ccc.gestion_eh.business.*;
import com.myapps.vista_ccc.gestion_eh.entity.VcDealerEntity;
import com.myapps.vista_ccc.gestion_eh.entity.VcPermisosSupervisorEntity;
import com.myapps.vista_ccc.gestion_eh.entity.VcSupervisorDealerZonasEntity;
import com.myapps.vista_ccc.gestion_eh.entity.VcZonasEntity;
import com.myapps.vista_ccc.gestion_eh.servicio.UtilEH;
import com.myapps.vista_ccc.gestion_eh.servicio.model.Categoria;
import com.myapps.vista_ccc.gestion_eh.servicio.model.Permisos;
import com.myapps.vista_ccc.gestion_eh.servicio.model.queryGroup.QueryGroup;
import com.myapps.vista_ccc.gestion_eh.servicio.model.queryUserGroup.QueryUserGroup;
import com.myapps.vista_ccc.reset_pin.commons.AuditoriaResetPin;
import com.myapps.vista_ccc.reset_pin.error.HttpStatusException;
import com.myapps.vista_ccc.reset_pin.error.RestExcepcion;
import com.myapps.vista_ccc.reset_pin.model.gson.QueryUser;
import com.myapps.vista_ccc.reset_pin.model.gson.TokenGson;
import com.myapps.vista_ccc.reset_pin.servicio.UtilMap;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.SysMessage;
import com.myapps.vista_ccc.util.UtilUrl;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.*;

@ManagedBean
@ViewScoped
public class ConsultaUsuarioBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String ERROR = "ERROR";
    private static Logger log = Logger.getLogger(ConsultaUsuarioBean.class);


    @Inject
    private VcSupervisorDealerZOnasBL vcSupervisorDealerZOnasBL;
    @Inject
    private VcPermisosSupervisorBL vcPermisosSupervisorBL;
    @Inject
    private ControlerBitacora controlerBitacora;
    @Inject
    private BajaUsuarioBusiness bajaUsuarioBusiness;
    @Inject
    private AuditoriaResetPin auditoria;
    @Inject
    private VcDealerBL vcDealerBL;
    @Inject
    private VcZonasBL vcZonasBL;

    private long idAuditoriaHistorial;
    private String ip;
    private String token;
    private String estado;
    private String codigo;
    private String login;
    private QueryUser user;
    private Map<String, String> mapResponseWhiteList;
    private MuUsuario usuarioActual;
    private List<VcDealerEntity> listaDealer;
    private String selectDealer;

    private List<VcZonasEntity> listaZonas;
    private String selectZonas;
    private boolean disable;
    private VcPermisosSupervisorEntity supervisor;
    private List<Categoria> listaCategoria;
    private String selectCategoria;
    private List<Permisos> listapermisos;
    private String msisdn;
    private String limite;
    private QueryUserGroup[] queryUserGroups;
    private QueryGroup[] queryGroups;
    private Map<String, String> mapa;

    @PostConstruct
    public void init() {
        cargarMapa();
        disable = true;
        usuarioActual = bajaUsuarioBusiness.obtenerUsuario();
        obtenerUsuario();
        cargarIp();
        user = new QueryUser();
        selectDealer = null;
        listaDealer = new ArrayList<>();
        selectZonas = null;
        listaZonas = new ArrayList<>();
        cargarDatosSupervisor();
        selectCategoria = null;
        listaCategoria = UtilMap.armarComboCategoria(Parametros.ehCategoria);
        listapermisos = UtilMap.armarPermisos(Parametros.ehPermisos);
    }


    /**
     * cargarDatosSupervisor()
     * Metodo para saber si el usuario Actual es supervisor, si es supervisor se obtendra la lista de Dealer a los cuales tiene acceso
     * el supervisor
     */
    private void cargarDatosSupervisor() {
        if (usuarioActual != null) {
            supervisor = vcPermisosSupervisorBL.getSupervisorForUsuario(usuarioActual.getUsuarioId(), usuarioActual.getNombre());
            if (supervisor != null) {
                List<VcSupervisorDealerZonasEntity> lista = vcSupervisorDealerZOnasBL.getListForSupervisor(supervisor.getIdPermisosSupervisor());
                if (lista != null) {
                    List<VcDealerEntity> listaDealerAuxiliar = new ArrayList<>();
                    for (VcSupervisorDealerZonasEntity sdz : lista) {
                        VcDealerEntity dealer = sdz.getVcDealerZonasByIdDealerZonas().getVcDealerByIdVcDealer();
                        listaDealerAuxiliar.add(dealer);
                    }
                    this.listaDealer = ordenarListaDealer(listaDealerAuxiliar);
                }
            }
        }
    }


    /**
     * @param listaInicio Lisra de Dealer pertenecientes al supervisor actual
     * @return Retorna una Lista de Dealer o Distribuidores sin elementos Repetidos
     * Este metodo solo elimina los Deales Repetidos
     */
    private List<VcDealerEntity> ordenarListaDealer(List<VcDealerEntity> listaInicio) {
        List<VcDealerEntity> lista = new ArrayList<>();
        if (listaInicio != null) {
            Map<Long, VcDealerEntity> mapaDealer = new HashMap<>();
            for (VcDealerEntity dealerEntity : listaInicio) {
                mapaDealer.put(dealerEntity.getIdVcDealer(), dealerEntity);
            }
            for (Map.Entry<Long, VcDealerEntity> p : mapaDealer.entrySet()) {
                lista.add(p.getValue());
            }
        }
        return lista;
    }


    private void cargarIp() {
        try {
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                    .getRequest();
            ip = UtilUrl.getClientIp(request);
        } catch (Exception e) {
            log.error("Error al obtener ip: ", e);
        }
    }

    /**
     * queryUserGroups obtiene todos los permisos y las Zonas del Vendedor
     * queryGroups obtiene los permisos de queryUserGroup trae todos los permisos y las Zonas disponibles
     */
    private void obtenerQueryGroup() {
        try {
            idAuditoriaHistorial = auditoria.guardarAuditoria(Calendar.getInstance(), login, ip,
                    DescriptorBitacora.GESTION_EH_BAJA_VENDEDOR.getFormulario(), 0, codigo, null);
            queryUserGroups = bajaUsuarioBusiness.getQueryUserGroup(login, idAuditoriaHistorial, user.getId(), token);
        } catch (com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException e) {
            auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
            log.error("[HttpStatusException] Problema con http client " + e.getMessage() + " " + e.getGenerica().toString());
            mostrarMensajeGenerico(e.getMessage(), e.getGenerica().getSummary());
        } catch (URISyntaxException e) {
            auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
            log.error("[URISyntaxException] No se pudo completar la consulta: " + e.getMessage(), e);
            SysMessage.error(Parametros.restMensajeErrorQueryUserGroup + "[RestExcepcion]: " + e.getMessage(), null);
        } catch (IOException e) {
            auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
            log.error("[IOException] No se pudo completar la consulta: " + e.getMessage(), e);
            SysMessage.error(Parametros.restMensajeErrorQueryUserGroup + "[IOException]: " + e.getMessage(), null);
        } catch (Exception e) {
            auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
            log.error("[Exception] No se pudo completar la consulta: " + e.getMessage(), e);
            SysMessage.error(Parametros.restMensajeErrorQueryUserGroup + "[Exception]: " + e.getMessage(), null);
        }


        try {
            idAuditoriaHistorial = auditoria.guardarAuditoria(Calendar.getInstance(), login, ip,
                    DescriptorBitacora.GESTION_EH_BAJA_VENDEDOR.getFormulario(), 0, codigo, null);
            queryGroups = bajaUsuarioBusiness.getQueryUserGroup(login, idAuditoriaHistorial, token);
        } catch (com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException e) {
            auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
            log.error("[HttpStatusException] Problema con http client " + e.getMessage() + " " + e.getGenerica().toString());
            mostrarMensajeGenerico(e.getMessage(), e.getGenerica().getSummary());
        } catch (URISyntaxException e) {
            auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
            log.error("[URISyntaxException] No se pudo completar la consulta: " + e.getMessage(), e);
            SysMessage.error(Parametros.restMensajeErrorQueryGroup + "[RestExcepcion]: " + e.getMessage(), null);
        } catch (IOException e) {
            auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
            log.error("[IOException] No se pudo completar la consulta: " + e.getMessage(), e);
            SysMessage.error(Parametros.restMensajeErrorQueryGroup + "[IOException]: " + e.getMessage(), null);
        } catch (Exception e) {
            auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
            log.error("[Exception] No se pudo completar la consulta: " + e.getMessage(), e);
            SysMessage.error(Parametros.restMensajeErrorQueryGroup + "[Exception]: " + e.getMessage(), null);
        }
        if (queryUserGroups != null && queryGroups != null) {
            if (cargarZona(Arrays.asList(queryUserGroups))) {
                cargarPermisos(Arrays.asList(queryUserGroups), Arrays.asList(queryGroups));
            }

        }

    }


    /**
     * @return Devuelve true si el Dealer obtenido pertenece al Servidor, para proceder con la modificacion
     * caso contrario se devolvera false para bloquear la vista
     */
    private boolean validarDealerParaSupervisor() {
        boolean valor = false;
        for (VcDealerEntity dealerEntity : listaDealer) {
            if (user != null && user.getAttributes() != null) {
                if (dealerEntity.getEh() != null && dealerEntity.getEh().equals(user.getAttributes().getEhCodeParent().get(0))) {
                    valor = true;
                    break;
                }
            }
        }
        return valor;
    }

    /**
     * Metodo que carga el Dealer o Distribuidor del vendedor obtenido al combo Dealer
     */
    private void cargarDistribuidor() {
        if (user != null && user.getAttributes() != null) {
            VcDealerEntity dealer = vcDealerBL.getDealerForEh(user.getAttributes().getEhCodeParent().get(0));
            if (dealer != null) {
                selectDealer = dealer.getNombre();

            }
        }
    }


    /**
     * @param queryGroups Lista de Permisos del Vendedor obtenido
     *                    Metodo que carga la Zona o Sucursal del vendedor obtenido al combo Zonas
     */
    private boolean cargarZona(List<QueryUserGroup> queryGroups) {
        boolean valor = false;
        if (queryGroups != null && supervisor != null) {
            VcZonasEntity zona = null;
            for (QueryUserGroup queryUserGroup : queryGroups) {
                zona = vcZonasBL.getZonaForNombre(queryUserGroup.getName(), supervisor.getIdPermisosSupervisor());
                if (zona != null) {
                    selectZonas = zona.getNombre();
                    valor = true;
                    break;
                }
            }
            if (zona == null) {
                limpiarDatos();
                SysMessage.warn("La Sucursal no esta habilitada para el Supervisor Actual", null);
            }
        }
        return valor;
    }

    /**
     * @param permisosUsuario Lista de Permisos del Vendedor Obtenido
     * @param queryGroups     Lista de Permisos y Zonas Globales
     *                        Metodo encargado de cargar los permisos a las listapermisos
     *                        tambien renombra los permisos si eston fueron configurados en la properties
     */
    private void cargarPermisos(List<QueryUserGroup> permisosUsuario, List<QueryGroup> queryGroups) {
        if (queryGroups != null && permisosUsuario != null) {
            listapermisos.clear();
            //Cargar permisos QuerGoup
            for (QueryGroup queryGroup : queryGroups) {
                if (!queryGroup.getName().equals(Parametros.ehIdentificadorSucursales)) {
                    boolean obtenerPermiso = UtilEH.obtenerPermiso(queryGroup.getId(), permisosUsuario);
                    listapermisos.add(new Permisos(queryGroup.getId(), queryGroup.getName(), obtenerPermiso, obtenerPermiso ? "Si" : "No"));
                }
            }
            //Renombrar
            for (Permisos permiso : listapermisos) {
                permiso.setNombre(UtilEH.obtenerNombre(permiso.getId(), permiso.getNombre()));
            }
        }
    }

    /**
     * Metodo ncargado de cargar los datos  a limite,misdn y categoria
     */
    private void cargarExtras() {
        if (user != null && user.getAttributes() != null && user.getAttributes().getPreactivationRecyclePeriod() != null) {
            limite = user.getAttributes().getMaxActivations().get(0);
            msisdn = user.getAttributes().getMsisdn().get(0);
            msisdn = msisdn.replace(Parametros.ehPrefijoTelefono, "");
            if (listaCategoria != null && user.getAttributes().getPreactivationRecyclePeriod().get(0) != null) {
                for (Categoria categoria : listaCategoria) {
                    if (String.valueOf(categoria.getId()).equals(user.getAttributes().getPreactivationRecyclePeriod().get(0))) {
                        selectCategoria = categoria.getNombre();
                    }
                }
            }
            if (selectCategoria == null) {
                SysMessage.warn("La categoria con ID: " + user.getAttributes().getPreactivationRecyclePeriod().get(0) + " no esta Registrada!", null);
            }
        }

    }


    /**
     * Metodo para limpieza parcial
     */
    public void limpiarDatos() {
        this.token = null;
        this.estado = null;
        this.user = new QueryUser();
        this.msisdn = null;
        this.limite = null;
        disable = true;
        selectCategoria = null;
        selectZonas = null;
        selectDealer = null;
    }

    /**
     * Metodo para limpieza completa del Formulario
     */
    public void limpiarTodo() {
        this.codigo = null;
        this.token = null;
        this.estado = null;
        this.user = new QueryUser();
        this.msisdn = null;
        this.limite = null;
        disable = true;
        selectCategoria = null;
        selectZonas = null;
        selectDealer = null;
        queryUserGroups = null;
        queryGroups = null;
        if (listapermisos != null) {
            for (Permisos permiso : listapermisos) {
                permiso.setEstadoString(null);
            }
        }
    }

    /**
     * Metodo para la busqueda del Vendedor por su codigo, Consume Servicios Rest de Token y QueryUser
     */
    public void buscarCodigo() {
        disable = true;
        if (codigo == null || codigo.isEmpty()) {
            mostrarMensajeGenerico("EL codigo es requerido!", "WARN");
        } else {
            if (supervisor != null) {
                limpiarDatos();
                try {
                    idAuditoriaHistorial = auditoria.guardarAuditoria(Calendar.getInstance(), login, ip,
                            DescriptorBitacora.GESTION_EH_BAJA_VENDEDOR.getFormulario(), 0, codigo, null);
                    TokenGson tokenGson = bajaUsuarioBusiness.getToken(login, idAuditoriaHistorial);
                    token = tokenGson.getAccessToken();
                } catch (UnknownHostException e) {
                    auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                    log.error("[UnknownHostException][getToken] No se pudo alcanzar la url: " + Parametros.restUrlToken + e.getMessage());
                    SysMessage.error("[getToken] No se pudo alcanzar la url: " + Parametros.restUrlToken + " [UnknownHostException]: " + e.getMessage(), null);
                } catch (com.myapps.vista_ccc.white_list.error.HttpStatusException e) {
                    auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                    log.error("[HttpStatusException][getToken] Problema con http client " + e.getMessage() + " " + e.getStatusResponse().getCode());
                    SysMessage.error("[getToken] " + Parametros.restMensajeErrorToken + "[HttpStatusException]: " + e.getMessage(), null);
                } catch (HttpStatusException e) {
                    auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                    log.error(" [HttpStatusException][getToken] Problema con http client " + e.getMessage() + " " + e.getStatusResponse().getCode());
                    SysMessage.error("[getToken] " + Parametros.restMensajeErrorToken + "[HttpStatusException]: " + e.getMessage(), null);
                } catch (RestExcepcion restExcepcion) {
                    auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(restExcepcion));
                    log.error("[RestExcepcion][getToken] No se pudo completar la consulta: " + restExcepcion.getMessage(), restExcepcion);
                    SysMessage.error("[getToken] " + Parametros.restMensajeErrorToken + "[RestExcepcion]: " + restExcepcion.getMessage(), null);
                } catch (IOException e) {
                    auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                    log.error("[IOException][getToken] No se pudo completar la consulta: " + e.getMessage(), e);
                    SysMessage.error("[getToken] " + Parametros.restMensajeErrorToken + "[IOException]: " + e.getMessage(), null);
                } catch (Exception e) {
                    auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                    log.error("[Exception][getToken] No se pudo completar la consulta: " + e.getMessage(), e);
                    SysMessage.error("[getToken] " + Parametros.restMensajeErrorToken + "[Exception]: " + e.getMessage(), null);
                }

                if (token != null) {
                    try {
                        idAuditoriaHistorial = auditoria.guardarAuditoria(Calendar.getInstance(), login, ip,
                                DescriptorBitacora.GESTION_EH_BAJA_VENDEDOR.getFormulario(), 0, codigo, null);
                        validarUsuario(bajaUsuarioBusiness.getQueryUser(login, idAuditoriaHistorial, codigo, token));
                    } catch (UnknownHostException e) {
                        auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                        log.error("[UnknownHostException][getQueryUser] No se pudo alcanzar la url: " + Parametros.restUrlQueryUSer + e.getMessage());
                        SysMessage.error("[getQueryUser] No se pudo alcanzar la url: " + Parametros.restUrlQueryUSer + " [UnknownHostException]: " + e.getMessage(), null);
                    } catch (com.myapps.vista_ccc.white_list.error.HttpStatusException e) {
                        auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                        log.error(" [HttpStatusException][getQueryUser] Problema con http client " + e.getMessage() + " " + e.getStatusResponse().getCode());
                        SysMessage.error("[getQueryUser] " + Parametros.restMensajeErrorUpdateUser + "[HttpStatusException]: " + e.getMessage(), null);
                    } catch (HttpStatusException e) {
                        auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                        log.error(" [HttpStatusException][getQueryUser] Problema con http client " + e.getMessage() + " " + e.getStatusResponse().getCode());
                        SysMessage.error("[getQueryUser] " + Parametros.restMensajeErrorUpdateUser + "[HttpStatusException]: " + e.getMessage(), null);
                    } catch (RestExcepcion restExcepcion) {
                        auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(restExcepcion));
                        log.error("[RestExcepcion][getQueryUser] No se pudo completar la consulta: " + restExcepcion.getMessage(), restExcepcion);
                        SysMessage.error("[getQueryUser] " + Parametros.restMensajeErrorUpdateUser + "[RestExcepcion]: " + restExcepcion.getMessage(), null);
                    } catch (URISyntaxException e) {
                        auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                        log.error("[URISyntaxException][getQueryUser] No se pudo completar la consulta: " + e.getMessage(), e);
                        SysMessage.error("[getQueryUser] " + Parametros.restMensajeErrorUpdateUser + "[URISyntaxException]: " + e.getMessage(), null);
                    } catch (IOException e) {
                        auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                        log.error("[IOException][getQueryUser] No se pudo completar la consulta: " + e.getMessage(), e);
                        SysMessage.error("[getQueryUser] " + Parametros.restMensajeErrorUpdateUser + "[IOException]: " + e.getMessage(), null);
                    } catch (Exception e) {
                        auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                        log.error("[Exception][getQueryUser] No se pudo completar la consulta: " + e.getMessage(), e);
                        SysMessage.error("[getQueryUser] " + Parametros.restMensajeErrorUpdateUser + "[Exception]: " + e.getMessage(), null);
                    }

                    try {
                        controlerBitacora.accion(DescriptorBitacora.GESTION_EH_CONSULTA_VENDEDOR,
                                "Se realizó la busqueda en ConsultaVendedor con el código: " + codigo);
                    } catch (Exception e) {
                        log.error("Error al guardar bitacora en el sistema: ", e);
                        SysMessage.error("Error al guardar bitacora", null);
                    }
                }
            } else {
                SysMessage.warn("Usuario: " + login + " no es Supervisor", null);
            }

        }
    }


    /**
     * @param user Metodo para validar si el Vendedor obtenido es Valido y cargar sus Datos
     */
    public void validarUsuario(QueryUser user) {
        try {
            if (user != null && user.getId() != null) {
                this.user = user;
                this.estado = user.getEnabled() ? "Habilitado" : "Deshabilitado";
                if (validarDealerParaSupervisor()) {
                    cargarDistribuidor();
                    obtenerQueryGroup();
                    cargarExtras();
                } else {
                   limpiarDatos();
                    if (user.getAttributes() != null) {
                        VcDealerEntity dealerEntity = vcDealerBL.getDealerForEh(user.getAttributes().getEhCodeParent().get(0));
                        if (dealerEntity != null) {
                            SysMessage.warn("El Distribuidor:" + dealerEntity.getNombre() + "  no esta habilitado para el usuario actual", null);
                        } else {
                            SysMessage.warn("El Distribuidor con EH: " + user.getAttributes().getEhCodeParent().get(0) + " no esta registrado!", null);
                        }
                    } else {
                        SysMessage.warn("El Vendedor no tiene Atributos Disponibles", null);
                    }
                }
            } else if (user == null) {
                mostrarMensajeGenerico(Parametros.restMensajeEmptyQueryUSer, "WARN");
            } else {
                mostrarMensajeGenerico(Parametros.restMensajeErrorQueryUSer, ERROR);
            }
        } catch (Exception e) {
            SysMessage.error(e.getMessage(), null);
        }

    }


    public void mostrarMensajeGenerico(String mensaje, String categoria) {
        switch (categoria) {
            case "INFO":
                SysMessage.info(mensaje, null);
                break;
            case "WARN":
                SysMessage.warn(mensaje, null);
                break;
            case ERROR:
                SysMessage.error(mensaje, null);
                break;
            default:
                SysMessage.error("Categoria no Definida " + mensaje, null);
                break;
        }
    }


    private void obtenerUsuario() {
        try {
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                    .getRequest();
            login = (String) request.getSession().getAttribute("TEMP$USER_NAME");
            log.info("Usuario: " + login + ", ingresando a vista de Consulta Vendedor.");
        } catch (Exception e) {
            log.error("Error al obtener usuario: ", e);
        }
    }


    /**
     * Metodo para cargar validaciones, de Properties a mapa
     */
    public void cargarMapa() {
        mapa = new HashMap<>();
        mapa.put("EH.SUMMARY.MENSAJES", Parametros.ehSummaryMensajes);

        mapa.put("EH.EXPRESION.CODIGO", Parametros.ehExpresionCodigo);
        mapa.put("EH.EXPRESION.CODIGO.BUSCADOR", Parametros.ehExpresionCodigoBuscador);
        mapa.put("EH.EXPRESION.CODIGO.MENSAJE.ERROR", Parametros.ehExpresionCodigoMensajeError);
        mapa.put("EH.EXPRESION.CODIGO.MENSAJE.REQUERIDO", Parametros.ehExpresionCodigoMensajeRequerido);

        mapa.put("EH.EXPRESION.NOMBRES", Parametros.ehExpresionNombres);
        mapa.put("EH.EXPRESION.NOMBRES.MENSAJE.ERROR", Parametros.ehExpresionNombresMensajeError);
        mapa.put("EH.EXPRESION.NOMBRES.MENSAJE.REQUERIDO", Parametros.ehExpresionNombresMensajeRequerido);

        mapa.put("EH.EXPRESION.APELLIDOS", Parametros.ehExpresionApellidos);
        mapa.put("EH.EXPRESION.APELLIDOS.MENSAJE.ERROR", Parametros.ehExpresionApellidosMensajeError);
        mapa.put("EH.EXPRESION.APELLIDOS.MENSAJE.REQUERIDO", Parametros.ehExpresionApellidosMensajeRequerido);

        mapa.put("EH.EXPRESION.TELEFONO", Parametros.ehExpresionTelefono);
        mapa.put("EH.EXPRESION.TELEFONO.MENSAJE.ERROR", Parametros.ehExpresionTelefonoMensajeError);
        mapa.put("EH.EXPRESION.TELEFONO.MENSAJE.REQUERIDO", Parametros.ehExpresionTelefonoMensajeRequerido);

        mapa.put("EH.EXPRESION.EMAIL", Parametros.ehExpresionEmail);
        mapa.put("EH.EXPRESION.EMAIL.MENSAJE.ERROR", Parametros.ehExpresionEmailMensajeError);

        mapa.put("EH.EXPRESION.LIMITE", Parametros.ehExpresionLimite);
        mapa.put("EH.EXPRESION.LIMITE.MENSAJE.ERROR", Parametros.getEhExpresionLimiteMensajeError);
        mapa.put("EH.EXPRESION.LIMITE.MENSAJE.REQUERIDO", Parametros.getEhExpresionLimiteMensajeRequerido);
    }


    public void abrirDialogoConfirmacion() {
        RequestContext.getCurrentInstance().execute("PF('carDelet').show();");
    }


    public String stackTraceToString(Throwable e) {
        CharArrayWriter sw = new CharArrayWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        pw.close();

        return sw.toString();
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public QueryUser getUser() {
        return user;
    }

    public void setUser(QueryUser user) {
        this.user = user;
    }

    public Map<String, String> getMapResponseWhiteList() {
        return mapResponseWhiteList;
    }

    public void setMapResponseWhiteList(Map<String, String> mapResponseWhiteList) {
        this.mapResponseWhiteList = mapResponseWhiteList;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }


    public String getSelectDealer() {
        return selectDealer;
    }

    public void setSelectDealer(String selectDealer) {
        this.selectDealer = selectDealer;
    }


    public String getSelectZonas() {
        return selectZonas;
    }

    public void setSelectZonas(String selectZonas) {
        this.selectZonas = selectZonas;
    }

    public boolean isDisable() {
        return disable;
    }

    public void setDisable(boolean disable) {
        this.disable = disable;
    }


    public String getSelectCategoria() {
        return selectCategoria;
    }

    public void setSelectCategoria(String selectCategoria) {
        this.selectCategoria = selectCategoria;
    }

    public List<Permisos> getListapermisos() {
        return listapermisos;
    }

    public void setListapermisos(List<Permisos> listapermisos) {
        this.listapermisos = listapermisos;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getLimite() {
        return limite;
    }

    public void setLimite(String limite) {
        this.limite = limite;
    }

    public Map<String, String> getMapa() {
        return mapa;
    }

    public void setMapa(Map<String, String> mapa) {
        this.mapa = mapa;
    }
}
