package com.myapps.vista_ccc.gestion_eh.dao;

import com.myapps.vista_ccc.gestion_eh.entity.VcPermisosSupervisorEntity;
import org.apache.log4j.Logger;

import javax.annotation.Resource;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
public class VcPermisosSupervisorDAO implements Serializable {
    private static Logger log = Logger.getLogger(VcPermisosSupervisorDAO.class);

    private static final long serialVersionUID = 1L;

    @PersistenceContext(unitName = "vista_ccc")
    private transient EntityManager entityManager;

    @Resource
    private transient UserTransaction transaction;

    public void save(VcPermisosSupervisorEntity dato) throws Exception {
        transaction.begin();
        entityManager.persist(dato);
        transaction.commit();
    }

    public VcPermisosSupervisorEntity get(long id) throws Exception {
        return entityManager.find(VcPermisosSupervisorEntity.class, id);
    }

    public void update(VcPermisosSupervisorEntity dato) throws Exception {
        transaction.begin();
        entityManager.merge(dato);
        transaction.commit();
    }

    public void remove(VcPermisosSupervisorEntity dato) throws Exception {
        transaction.begin();
        entityManager.joinTransaction();
        try {
            entityManager.remove(entityManager.contains(dato) ? dato : entityManager.merge(dato));
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    public List<VcPermisosSupervisorEntity> getList() throws Exception {
        return entityManager.createQuery("SELECT us FROM VcPermisosSupervisorEntity us WHERE us.estado=true Order by us.idPermisosSupervisor").getResultList();
    }

    @SuppressWarnings("unchecked")
    public VcPermisosSupervisorEntity getSupervisorForUsuario(long idUsuario, String tipo) {
        VcPermisosSupervisorEntity valor = null;
        try {
            Query query;
            if (tipo != null && tipo.equals("GRUPO")) {
                query = entityManager.createQuery("SELECT us FROM VcPermisosSupervisorEntity us WHERE us.estado=true and us.grupoId=:idUsuario", VcPermisosSupervisorEntity.class);
            } else {
                query = entityManager.createQuery("SELECT us FROM VcPermisosSupervisorEntity us WHERE us.estado=true and us.usuarioId=:idUsuario", VcPermisosSupervisorEntity.class);
            }
            query.setParameter("idUsuario", idUsuario);
            valor = (VcPermisosSupervisorEntity) query.getSingleResult();
        } catch (NoResultException e) {
            log.warn("El usuario con id: " + idUsuario + " No esta registrado como supervisor Exception: " + e.getMessage());
        }
        return valor;
    }

    @SuppressWarnings("unchecked")
    public VcPermisosSupervisorEntity getSupervisorPermisos(long idUsuario) {
        VcPermisosSupervisorEntity valor = null;
        try {
            Query query = entityManager.createQuery("SELECT us FROM VcPermisosSupervisorEntity us WHERE us.usuarioId = :idUsuario", VcPermisosSupervisorEntity.class);
            query.setParameter("idUsuario", idUsuario);
            valor = (VcPermisosSupervisorEntity) query.getSingleResult();
        } catch (NoResultException e) {
            log.warn("El usuario con id: " + idUsuario + " No esta registrado como supervisor Exception: " + e.getMessage());
        }
        return valor;
    }

    @SuppressWarnings("unchecked")
    public VcPermisosSupervisorEntity getSupervisorPermisosGrupoID(long idGrupoUsuario) {
        VcPermisosSupervisorEntity valor = null;
        try {
            Query query = entityManager.createQuery("SELECT us FROM VcPermisosSupervisorEntity us WHERE us.grupoId = :idGrupoUsuario", VcPermisosSupervisorEntity.class);
            query.setParameter("idGrupoUsuario", idGrupoUsuario);
            valor = (VcPermisosSupervisorEntity) query.getSingleResult();
        } catch (NoResultException e) {
            log.warn("El usuario con id: " + idGrupoUsuario + " No esta registrado como supervisor Exception: " + e.getMessage());
        }
        return valor;
    }

    @SuppressWarnings("unchecked")
    public List<VcPermisosSupervisorEntity> getIdPermisoSupervisorDelete(long idPermisoSupervisor) throws Exception {
        List<VcPermisosSupervisorEntity> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM VC_PERMISOS_SUPERVISOR PS WHERE PS.USUARIO_ID = :idPermisoSupervisor";
            Query query = entityManager.createNativeQuery(sql, VcPermisosSupervisorEntity.class);
            query.setParameter("idPermisoSupervisor", idPermisoSupervisor);
            list = query.getResultList();
        } catch (NoResultException e) {
            log.warn("No se encontraron resultados para el SupervisorDealerZonas: " + idPermisoSupervisor);
        }
        return list;
    }

    @SuppressWarnings("unchecked")
    public List<VcPermisosSupervisorEntity> getIdPermisoSupervisorGrupoIDDelete(long idGrupoID) throws Exception {
        List<VcPermisosSupervisorEntity> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM VC_PERMISOS_SUPERVISOR PS WHERE PS.GRUPO_ID = :idGrupoID";
            Query query = entityManager.createNativeQuery(sql, VcPermisosSupervisorEntity.class);
            query.setParameter("idGrupoID", idGrupoID);
            list = query.getResultList();
        } catch (NoResultException e) {
            log.warn("No se encontraron resultados para el SupervisorDealerZonas: " + idGrupoID);
        }
        return list;
    }

    public void removeIdPermisoSupervisor(VcPermisosSupervisorEntity dato) throws Exception {
        try {
            transaction.begin();
            entityManager.remove(entityManager.contains(dato) ? dato : entityManager.merge(dato));
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        }
    }


}
