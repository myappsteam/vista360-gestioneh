package com.myapps.vista_ccc.gestion_eh.servicio;


import com.myapps.vista_ccc.gestion_eh.servicio.model.AceptadosGenerica;
import com.myapps.vista_ccc.gestion_eh.servicio.model.ExcepcionGenerica;
import com.myapps.vista_ccc.util.Parametros;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.*;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Named
public class ApacheHttpClientRestServiceGestionEh implements Serializable {

    private static Logger logger = Logger.getLogger(ApacheHttpClientRestServiceGestionEh.class);
    private List<ExcepcionGenerica> listaExcepcionesQueryGroup;
    private List<AceptadosGenerica> listaAceptadosQueryGroup;

    private List<ExcepcionGenerica> listaExcepcionesQueryUserGroup;
    private List<AceptadosGenerica> listaAceptadosQueryUserGroup;

    private List<ExcepcionGenerica> listaExcepcionesUpdateUser;
    private List<AceptadosGenerica> listaAceptadosUpdateUser;

    private List<ExcepcionGenerica> listaExcepcionesAddGroupUser;
    private List<AceptadosGenerica> listaAceptadosAddGroupUser;

    private List<ExcepcionGenerica> listaExcepcionesDeleteGroupUser;
    private List<AceptadosGenerica> listaAceptadosDeleteGroupUser;

    private List<ExcepcionGenerica> listaExcepcionesRemoveUser;
    private List<AceptadosGenerica> listaAceptadosRemoveUser;


    private List<ExcepcionGenerica> listaExcepcionesCreateUser;
    private List<AceptadosGenerica> listaAceptadosCreateUser;


    public String validarResponseString(HttpResponse response, List<AceptadosGenerica> listaAceptados, List<ExcepcionGenerica> listaExcepciones)
            throws IOException, HttpStatusException {
        if (response != null && listaAceptados != null && listaExcepciones != null) {
            logger.info("Http Status Code: " + response.getStatusLine().getStatusCode());
            if (!respuestaValida(listaAceptados, response)) {
                for (ExcepcionGenerica excepcion : listaExcepciones) {
                    if (excepcion.getCodigo() == response.getStatusLine().getStatusCode()) {
                        logger.warn("[Exception]: " + excepcion.toString() + " [BODY]:" + EntityUtils.toString(response.getEntity()));
                        throw new HttpStatusException(excepcion.getMensaje(), excepcion);
                    }
                }
                logger.warn("[Exception]:  El Http Status Code: " + response.getStatusLine().getStatusCode() + " no fue definido en las properties " + " [BODY]:" + EntityUtils.toString(response.getEntity()));
                ExcepcionGenerica excepcionAux = new ExcepcionGenerica(0, "Desconocido", "[Exception]:  El Http Status Code: " + response.getStatusLine().getStatusCode() + " no fue definido en las properties ", "ERROR");
                throw new HttpStatusException(excepcionAux.getMensaje(), excepcionAux);
            }
        } else {

            logger.error("[Exception]: No se pudo establecer una conexion con el servicio Rest Servicio resualto a Null.");
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(0, "Error en la Conexion", "No se pudo establecer una conexion con el servicio Rest Servicio resualto a Null.", "ERROR");
            throw new HttpStatusException(excepcionAux.getMensaje(), excepcionAux);
        }
        return EntityUtils.toString(response.getEntity());
    }

    public boolean validarResponseBolean(HttpResponse response, List<AceptadosGenerica> listaAceptados, List<ExcepcionGenerica> listaExcepciones)
            throws IOException, HttpStatusException {
        boolean result = false;
        if (response != null && listaAceptados != null && listaExcepciones != null) {
            logger.info("Http Status Code: " + response.getStatusLine().getStatusCode());
            if (!respuestaValida(listaAceptados, response)) {
                for (ExcepcionGenerica excepcion : listaExcepciones) {
                    if (excepcion.getCodigo() == response.getStatusLine().getStatusCode()) {
                        logger.warn("[Exception]: " + excepcion.toString() + " [BODY]:" + EntityUtils.toString(response.getEntity()));
                        throw new HttpStatusException(excepcion.getMensaje(), excepcion);
                    }
                }
                logger.warn("[Exception]:  El Http Status Code: " + response.getStatusLine().getStatusCode() + " no fue definido en las properties " + " [BODY]:" + EntityUtils.toString(response.getEntity()));
                ExcepcionGenerica excepcionAux = new ExcepcionGenerica(0, "Desconocido", "[Exception]:  El Http Status Code: " + response.getStatusLine().getStatusCode() + " no fue definido en las properties ", "ERROR");
                throw new HttpStatusException(excepcionAux.getMensaje(), excepcionAux);
            } else {
                result = true;
            }
        } else {
            logger.error("[Exception]: No se pudo establecer una conexion con el servicio Rest Servicio resualto a Null.");
            ExcepcionGenerica excepcionAux = new ExcepcionGenerica(0, "Error en la Conexion", "No se pudo establecer una conexion con el servicio Rest Servicio resualto a Null.", "ERROR");
            throw new HttpStatusException(excepcionAux.getMensaje(), excepcionAux);
        }
        return result;
    }

    public boolean respuestaValida(List<AceptadosGenerica> lista, HttpResponse response) {
        boolean valor = false;
        if (lista != null && response != null) {
            for (AceptadosGenerica generica : lista) {
                if (generica.getCodigo() == response.getStatusLine().getStatusCode()) {
                    logger.info("Response Valido:" + generica.toString());
                    valor = true;
                    break;
                }
            }
        }
        return valor;
    }


    public synchronized boolean updateUser(String apiRestAuthURL, String tokken, String body) throws IOException, HttpStatusException {
        boolean result;
        HttpPut httpPut = new HttpPut(apiRestAuthURL);
        httpPut.addHeader("Content-Type", "application/json");
        httpPut.addHeader("Authorization", "Bearer " + tokken);
        httpPut.setEntity(new StringEntity(body));
        CloseableHttpResponse response;
        CloseableHttpClient httpClient = null;
        try {
            httpClient = HttpClientBuilder.create().setDefaultRequestConfig(generarConfiguracionRequest(Parametros.restTimeoutUpdateUser)).build();
            response = httpClient.execute(httpPut);
            result = validarResponseBolean(response, listaAceptadosUpdateUser, listaExcepcionesUpdateUser);
        } finally {
            httpClient.close();
        }
        return result;
    }


    public synchronized boolean createUser(String apiRestAuthURL, String tokken, String body) throws IOException, HttpStatusException {
        boolean result;
        HttpPost httpPost = new HttpPost(apiRestAuthURL);
        httpPost.addHeader("Content-Type", "application/json");
        httpPost.addHeader("Authorization", "Bearer " + tokken);
        httpPost.setEntity(new StringEntity(body));
        CloseableHttpResponse response;
        CloseableHttpClient httpClient = null;
        try {
            httpClient = HttpClientBuilder.create().setDefaultRequestConfig(generarConfiguracionRequest(Parametros.restTimeoutCreateUser)).build();
            response = httpClient.execute(httpPost);
            result = validarResponseBolean(response, listaAceptadosCreateUser, listaExcepcionesCreateUser);
        } finally {
            httpClient.close();
        }
        return result;
    }

    public synchronized boolean addGroupUser(String apiRestAuthURL, String tokken) throws IOException, HttpStatusException {
        boolean result;
        HttpPut httpPut = new HttpPut(apiRestAuthURL);
        httpPut.addHeader("Content-Type", "application/json");
        httpPut.addHeader("Authorization", "Bearer " + tokken);
        CloseableHttpResponse response;
        CloseableHttpClient httpClient = null;
        try {
            httpClient = HttpClientBuilder.create().setDefaultRequestConfig(generarConfiguracionRequest(Parametros.restTimeoutAddGroupUser)).build();
            response = httpClient.execute(httpPut);
            result = validarResponseBolean(response, listaAceptadosAddGroupUser, listaExcepcionesAddGroupUser);
        } finally {
            httpClient.close();
        }
        return result;
    }


    public synchronized boolean deletGroupUser(String apiRestAuthURL, String tokken) throws IOException, HttpStatusException {
        boolean result;
        HttpDelete httpDelete = new HttpDelete(apiRestAuthURL);
        httpDelete.addHeader("Content-Type", "application/json");
        httpDelete.addHeader("Authorization", "Bearer " + tokken);
        CloseableHttpResponse response;
        CloseableHttpClient httpClient = null;
        try {
            httpClient = HttpClientBuilder.create().setDefaultRequestConfig(generarConfiguracionRequest(Parametros.restTimeoutDeleteGroupUser)).build();
            response = httpClient.execute(httpDelete);
            result = validarResponseBolean(response, listaAceptadosDeleteGroupUser, listaExcepcionesDeleteGroupUser);
        } finally {
            httpClient.close();
        }
        return result;
    }

    public synchronized boolean deletUser(String apiRestAuthURL, String tokken) throws IOException, HttpStatusException {
        boolean result;
        HttpDelete httpDelete = new HttpDelete(apiRestAuthURL);
        httpDelete.addHeader("Content-Type", "application/json");
        httpDelete.addHeader("Authorization", "Bearer " + tokken);
        CloseableHttpResponse response;
        CloseableHttpClient httpClient = null;
        try {
            httpClient = HttpClientBuilder.create().setDefaultRequestConfig(generarConfiguracionRequest(Parametros.restTimeoutDeleteUser)).build();
            response = httpClient.execute(httpDelete);
            result = validarResponseBolean(response, listaAceptadosRemoveUser, listaExcepcionesRemoveUser);
        } finally {
            httpClient.close();
        }
        return result;
    }


    public synchronized String getQueryGroups(String apiRestAuthURL, String tokken) throws IOException, URISyntaxException, HttpStatusException {
        String result;
        URI uri = new URIBuilder(apiRestAuthURL).build();
        HttpRequestBase httpGet = new HttpGet(uri);
        httpGet.addHeader("Content-Type", "application/json");
        httpGet.addHeader("Authorization", "Bearer " + tokken);
        CloseableHttpResponse response;
        CloseableHttpClient httpClient = null;
        try {
            httpClient = HttpClientBuilder.create().setDefaultRequestConfig(generarConfiguracionRequest(Parametros.restTimeoutQueryGroup)).build();
            response = httpClient.execute(httpGet);
            result = validarResponseString(response, listaAceptadosQueryGroup, listaExcepcionesQueryGroup);
        } finally {
            httpClient.close();
        }
        return result;
    }

    public synchronized String getQueryUserGroups(String apiRestAuthURL, String codigo, String tokken) throws IOException, URISyntaxException, HttpStatusException {
        String result;
        apiRestAuthURL = apiRestAuthURL.replace("{user-id}", codigo);
        URI uri = new URIBuilder(apiRestAuthURL).build();
        HttpRequestBase httpGet = new HttpGet(uri);
        httpGet.addHeader("Content-Type", "application/json");
        httpGet.addHeader("Authorization", "Bearer " + tokken);
        CloseableHttpResponse response;
        CloseableHttpClient httpClient = null;
        try {
            httpClient = HttpClientBuilder.create().setDefaultRequestConfig(generarConfiguracionRequest(Parametros.restTimeoutQueryUserGroup)).build();
            response = httpClient.execute(httpGet);
            result = validarResponseString(response, listaAceptadosQueryUserGroup, listaExcepcionesQueryUserGroup);
        } finally {
            httpClient.close();
        }
        return result;
    }


    public RequestConfig generarConfiguracionRequest(int timeout) {
        return RequestConfig.custom().setConnectTimeout(timeout * 1000).setConnectionRequestTimeout(timeout * 1000)
                .setSocketTimeout(timeout * 1000).build();
    }

    public ApacheHttpClientRestServiceGestionEh() {
        listaExcepcionesQueryGroup = UtilEH.cargarExcepciones(Parametros.restListaExcepcionesQueryGroup);
        listaAceptadosQueryGroup = UtilEH.cargarAcepatados(Parametros.restListaAceptadosQueryGroup);

        listaExcepcionesQueryUserGroup = UtilEH.cargarExcepciones(Parametros.restListaExcepcionesQueryUserGroup);
        listaAceptadosQueryUserGroup = UtilEH.cargarAcepatados(Parametros.restListaAceptadosQueryUserGroup);

        listaExcepcionesUpdateUser = UtilEH.cargarExcepciones(Parametros.restListaExcepcionesUpdateUser);
        listaAceptadosUpdateUser = UtilEH.cargarAcepatados(Parametros.restListaAceptadosUpdateUser);

        listaExcepcionesAddGroupUser = UtilEH.cargarExcepciones(Parametros.restListaExcepcionesAddGroupUser);
        listaAceptadosAddGroupUser = UtilEH.cargarAcepatados(Parametros.restListaAceptadosAddGroupUser);

        listaExcepcionesDeleteGroupUser = UtilEH.cargarExcepciones(Parametros.restListaExcepcionesDeleteGroupUser);
        listaAceptadosDeleteGroupUser = UtilEH.cargarAcepatados(Parametros.restListaAceptadosDeleteGroupUser);

        listaExcepcionesRemoveUser = UtilEH.cargarExcepciones(Parametros.restListaExcepcionesDeleteUser);
        listaAceptadosRemoveUser = UtilEH.cargarAcepatados(Parametros.restListaAceptadosDeleteUser);

        listaAceptadosCreateUser = UtilEH.cargarAcepatados(Parametros.restListaAceptadosCreateUser);
        listaExcepcionesCreateUser = UtilEH.cargarExcepciones(Parametros.restListaExcepcionesCreateUser);

    }
}
