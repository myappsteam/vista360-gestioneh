package com.myapps.vista_ccc.gestion_eh.servicio;


import com.myapps.vista_ccc.gestion_eh.servicio.model.ExcepcionGenerica;

public class HttpStatusException extends Exception {
    private static final long serialVersionUID = 1L;
    private ExcepcionGenerica generica;

    public HttpStatusException(String message, ExcepcionGenerica generica) {
        super(message);
        this.generica = generica;
    }

    public ExcepcionGenerica getGenerica() {
        return generica;
    }

    public void setGenerica(ExcepcionGenerica generica) {
        this.generica = generica;
    }
}
