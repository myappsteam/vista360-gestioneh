package com.myapps.vista_ccc.gestion_eh.dao;

import com.myapps.vista_ccc.gestion_eh.entity.VcDealerEntity;
import org.apache.log4j.Logger;

import javax.annotation.Resource;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import java.io.Serializable;
import java.util.List;

@Named
public class VcDealerDAO implements Serializable {
    private static Logger log = Logger.getLogger(VcDealerDAO.class);

    private static final long serialVersionUID = 1L;

    @PersistenceContext(unitName = "vista_ccc")
    private transient EntityManager entityManager;

    @Resource
    private transient UserTransaction transaction;

    public void save(VcDealerEntity dato) throws Exception {
        transaction.begin();
        entityManager.persist(dato);
        transaction.commit();
    }

    public VcDealerEntity get(long id) throws Exception {
        return entityManager.find(VcDealerEntity.class, id);
    }

    public void update(VcDealerEntity dato) throws Exception {
        transaction.begin();
        entityManager.merge(dato);
        transaction.commit();
    }

    @SuppressWarnings("unchecked")
    public List<VcDealerEntity> getList() throws Exception {
        return entityManager.createQuery("SELECT us FROM VcDealerEntity us WHERE us.estado=true Order by us.nombre").getResultList();
    }

    public VcDealerEntity getForEH(String eh) throws Exception {
        VcDealerEntity valor = null;
        try {
            String sql = "SELECT * from VC_DEALER WHERE EH=:eh";
            Query query = entityManager.createNativeQuery(sql, VcDealerEntity.class);
            query.setParameter("eh", eh);
            valor = (VcDealerEntity) query.getSingleResult();
        } catch (NoResultException e) {
            log.warn("No se encontraron resultados para EH: " + eh + " Exception: " + e.getMessage());
        }
        return valor;
    }


}
