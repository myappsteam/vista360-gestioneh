package com.myapps.vista_ccc.gestion_eh.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "VC_SUPERVISOR_DEALER_ZONAS")
public class VcSupervisorDealerZonasEntity implements Serializable {
    private long vcPermisosSupervisor;
    private boolean estado;
    private VcPermisosSupervisorEntity vcPermisosSupervisorByIdPermisosSupervisor;
    private VcDealerZonasEntity vcDealerZonasByIdDealerZonas;

    @Id
    @Column(name = "ID_SUPERVISOR_DEALER_ZONAS")
    @SequenceGenerator(name = "pk_sequence", sequenceName = "SEQ_VC_SUPERVISOR_ID_SUPERVISO", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence")
    public long getVcPermisosSupervisor() {
        return vcPermisosSupervisor;
    }

    public void setVcPermisosSupervisor(long vcPermisosSupervisor) {
        this.vcPermisosSupervisor = vcPermisosSupervisor;
    }

    @Basic
    @Column(name = "ESTADO")
    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VcSupervisorDealerZonasEntity that = (VcSupervisorDealerZonasEntity) o;
        return vcPermisosSupervisor == that.vcPermisosSupervisor &&
                estado == that.estado;
    }

    @Override
    public int hashCode() {

        return Objects.hash(vcPermisosSupervisor, estado);
    }

    @ManyToOne
    @JoinColumn(name = "ID_PERMISOS_SUPERVISOR", referencedColumnName = "ID_PERMISOS_SUPERVISOR")
    public VcPermisosSupervisorEntity getVcPermisosSupervisorByIdPermisosSupervisor() {
        return vcPermisosSupervisorByIdPermisosSupervisor;
    }

    public void setVcPermisosSupervisorByIdPermisosSupervisor(VcPermisosSupervisorEntity vcPermisosSupervisorByIdPermisosSupervisor) {
        this.vcPermisosSupervisorByIdPermisosSupervisor = vcPermisosSupervisorByIdPermisosSupervisor;
    }

    @ManyToOne
    @JoinColumn(name = "ID_DEALER_ZONAS", referencedColumnName = "ID_DEALER_ZONAS")
    public VcDealerZonasEntity getVcDealerZonasByIdDealerZonas() {
        return vcDealerZonasByIdDealerZonas;
    }

    public void setVcDealerZonasByIdDealerZonas(VcDealerZonasEntity vcDealerZonasByIdDealerZonas) {
        this.vcDealerZonasByIdDealerZonas = vcDealerZonasByIdDealerZonas;
    }
}
