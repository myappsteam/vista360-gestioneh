package com.myapps.vista_ccc.gestion_eh.dao;

import com.myapps.vista_ccc.gestion_eh.entity.VcPermisosSupervisorEntity;
import com.myapps.vista_ccc.gestion_eh.entity.VcSupervisorDealerZonasEntity;
import org.apache.log4j.Logger;

import javax.annotation.Resource;
import javax.inject.Named;
import javax.persistence.*;
import javax.transaction.UserTransaction;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
public class VcSupervisorDealerZonasDAO implements Serializable {
    private static Logger log = Logger.getLogger(VcSupervisorDealerZonasDAO.class);

    private static final long serialVersionUID = 1L;

    @PersistenceContext(unitName = "vista_ccc")
    private transient EntityManager entityManager;

    @Resource
    private transient UserTransaction transaction;

    public void save(VcSupervisorDealerZonasEntity dato) throws Exception {
        transaction.begin();
        entityManager.persist(dato);
        transaction.commit();
    }

    public VcSupervisorDealerZonasEntity get(long id) throws Exception {
        return entityManager.find(VcSupervisorDealerZonasEntity.class, id);
    }

    public void update(VcSupervisorDealerZonasEntity dato) throws Exception {
        transaction.begin();
        entityManager.merge(dato);
        transaction.commit();
    }

    public void remove(VcSupervisorDealerZonasEntity dato) throws Exception {
        transaction.begin();
        entityManager.joinTransaction();
        try {
            entityManager.remove(entityManager.contains(dato) ? dato : entityManager.merge(dato));
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    public List<VcSupervisorDealerZonasEntity> getList() throws Exception {
        return entityManager.createQuery("SELECT us FROM VcSupervisorDealerZonasEntity us WHERE us.estado=true Order by us.vcPermisosSupervisor").getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<VcSupervisorDealerZonasEntity> getListforSupervisor(long idSupervisor) throws Exception {
        List<VcSupervisorDealerZonasEntity> lista = new ArrayList<>();
        try {
            String sql = "SELECT * FROM VC_SUPERVISOR_DEALER_ZONAS WHERE ID_PERMISOS_SUPERVISOR=:idSupervisor AND ESTADO=1";
            //Query query = entityManager.createQuery("SELECT us FROM VcSupervisorDealerZonasEntity us WHERE us.estado=true and us.vcPermisosSupervisor=:idSupervisor Order by us.vcPermisosSupervisor");
            Query query = entityManager.createNativeQuery(sql, VcSupervisorDealerZonasEntity.class);
            query.setParameter("idSupervisor", idSupervisor);
            lista = query.getResultList();
        } catch (NoResultException e) {
            log.warn("No se encontraron resultados para el supervisor: " + idSupervisor);
        }
        return lista;
    }

    @SuppressWarnings("unchecked")
    public List<VcSupervisorDealerZonasEntity> getListDealerZonas(long usuarioID) throws Exception {
        List<VcSupervisorDealerZonasEntity> lista = new ArrayList<>();
        try {
            String sql = "SELECT PS.ID_PERMISOS_SUPERVISOR, PS.USUARIO_ID, SD.ID_DEALER_ZONAS, VZ.NOMBRE FROM VC_PERMISOS_SUPERVISOR PS INNER JOIN VC_SUPERVISOR_DEALER_ZONAS SD ON PS.ID_PERMISOS_SUPERVISOR = SD.ID_PERMISOS_SUPERVISOR INNER JOIN VC_DEALER_ZONAS VDS ON SD.ID_DEALER_ZONAS = VDS.ID_DEALER_ZONAS INNER JOIN VC_ZONAS VZ ON VZ.ID_VC_ZONAS = VDS.ID_VC_ZONAS WHERE PS.USUARIO_ID = :usuarioID AND PS.ESTADO = 1";
            //Query query = entityManager.createQuery("SELECT us FROM VcSupervisorDealerZonasEntity us WHERE us.estado=true and us.vcPermisosSupervisor=:idSupervisor Order by us.vcPermisosSupervisor");
            Query query = entityManager.createNativeQuery(sql, VcSupervisorDealerZonasEntity.class);
            query.setParameter("usuarioID", usuarioID);
            lista = query.getResultList();
        } catch (NoResultException e) {
            log.warn("No se encontraron resultados para el dealerZonas: " + usuarioID);
        }
        return lista;
    }

    @SuppressWarnings("unchecked")
    public VcPermisosSupervisorEntity getSupervisorPermisos(long idUsuario) {
        VcPermisosSupervisorEntity valor = null;
        try {
            Query query = entityManager.createQuery("SELECT us FROM VcPermisosSupervisorEntity us WHERE us.usuarioId = :idUsuario", VcPermisosSupervisorEntity.class);
            query.setParameter("idUsuario", idUsuario);
            valor = (VcPermisosSupervisorEntity) query.getSingleResult();
        } catch (NoResultException e) {
            log.warn("El usuario con id: " + idUsuario + " No esta registrado como supervisor Exception: " + e.getMessage());
        }
        return valor;
    }

    @SuppressWarnings("unchecked")
    public VcSupervisorDealerZonasEntity eliminarSupervisorPermisoBL(long idPermisoSupervisor) {
        VcSupervisorDealerZonasEntity valor = null;
        try {
            TypedQuery query = entityManager.createQuery("DELETE FROM VcSupervisorDealerZonasEntity SD WHERE SD.vcPermisosSupervisorByIdPermisosSupervisor.idPermisosSupervisor = :idPermisoSupervisor", VcSupervisorDealerZonasEntity.class);
            query.setParameter("idPermisoSupervisor", idPermisoSupervisor);
            if (query.getResultList().isEmpty()) {
                valor = null;
            } else {
                valor = (VcSupervisorDealerZonasEntity) query.getSingleResult();
            }
        } catch (NoResultException e) {
            log.warn("El la tabla Vc_SUPERVISOR_DEALER_ZONAS el usuario con idPermisoSupervisor: " + idPermisoSupervisor + " Exception: " + e.getMessage());
        }
        return valor;
    }

    @SuppressWarnings("unchecked")
    public List<VcSupervisorDealerZonasEntity> getIdSupervisorVendedorDelete(long idPermisoSupervisor) throws Exception {
        List<VcSupervisorDealerZonasEntity> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM VC_SUPERVISOR_DEALER_ZONAS SD WHERE SD.ID_PERMISOS_SUPERVISOR = :idPermisoSupervisor";
            Query query = entityManager.createNativeQuery(sql, VcSupervisorDealerZonasEntity.class);
            query.setParameter("idPermisoSupervisor", idPermisoSupervisor);
            list = query.getResultList();
        } catch (NoResultException e) {
            log.warn("No se encontraron resultados para el SupervisorDealerZonas: " + idPermisoSupervisor);
        }
        return list;
    }

    public void removeIdZonaDealer(VcSupervisorDealerZonasEntity dato) throws Exception {
        transaction.begin();
        try {
            entityManager.remove(entityManager.contains(dato) ? dato : entityManager.merge(dato));
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        }
    }

}
