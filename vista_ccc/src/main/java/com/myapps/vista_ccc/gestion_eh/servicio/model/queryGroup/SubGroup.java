
package com.myapps.vista_ccc.gestion_eh.servicio.model.queryGroup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class SubGroup implements Serializable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("path")
    @Expose
    private String path;
    @SerializedName("subGroups")
    @Expose
    private List<Object> subGroups = null;
    private final static long serialVersionUID = -1192874266418062082L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<Object> getSubGroups() {
        return subGroups;
    }

    public void setSubGroups(List<Object> subGroups) {
        this.subGroups = subGroups;
    }

}
