package com.myapps.vista_ccc.gestion_eh.bean;


import com.myapps.user.bean.ControlerBitacora;
import com.myapps.user.ldap.DescriptorBitacora;
import com.myapps.user.model.MuUsuario;
import com.myapps.vista_ccc.gestion_eh.business.*;
import com.myapps.vista_ccc.gestion_eh.entity.*;
import com.myapps.vista_ccc.gestion_eh.servicio.UtilEH;
import com.myapps.vista_ccc.gestion_eh.servicio.model.Categoria;
import com.myapps.vista_ccc.gestion_eh.servicio.model.Permisos;
import com.myapps.vista_ccc.gestion_eh.servicio.model.queryGroup.QueryGroup;
import com.myapps.vista_ccc.gestion_eh.servicio.model.queryGroup.SubGroup;
import com.myapps.vista_ccc.reset_pin.commons.AuditoriaResetPin;
import com.myapps.vista_ccc.reset_pin.error.HttpStatusException;
import com.myapps.vista_ccc.reset_pin.error.RestExcepcion;
import com.myapps.vista_ccc.reset_pin.model.gson.Attributes;
import com.myapps.vista_ccc.reset_pin.model.gson.QueryUser;
import com.myapps.vista_ccc.reset_pin.model.gson.TokenGson;
import com.myapps.vista_ccc.reset_pin.servicio.UtilMap;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.SysMessage;
import com.myapps.vista_ccc.util.UtilUrl;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.*;

@ManagedBean
@ViewScoped
public class AltaUsuarioBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String ERROR = "ERROR";
    private static Logger log = Logger.getLogger(AltaUsuarioBean.class);

    @Inject
    private VcDealerZonasBL vcDealerZonasBL;
    @Inject
    private VcSupervisorDealerZOnasBL vcSupervisorDealerZOnasBL;
    @Inject
    private VcPermisosSupervisorBL vcPermisosSupervisorBL;
    @Inject
    private ControlerBitacora controlerBitacora;
    @Inject
    private AltaUsuarioBusiness altaUsuarioBusiness;
    @Inject
    private AuditoriaResetPin auditoria;

    @Inject
    private VcDealerBL vcDealerBL;
    @Inject
    private VcZonasBL vcZonasBL;

    private long idAuditoriaHistorial;
    private String ip;
    private String token;
    private String estado;
    private String login;
    private QueryUser user;
    private Map<String, String> mapResponseWhiteList;
    private MuUsuario usuarioActual;
    private List<VcDealerEntity> listaDealer;
    private VcDealerEntity selectDealer;

    private List<VcZonasEntity> listaZonas;
    private VcZonasEntity selectZonas;
    private boolean disable;
    private VcPermisosSupervisorEntity supervisor;
    private List<Categoria> listaCategoria;
    private Categoria selectCategoria;
    private List<Permisos> listapermisos;
    private String msisdn;
    private String limite;
    private QueryGroup[] queryGroups;
    private Map<String, String> mapa;

    @PostConstruct
    public void init() {
        try {
            cargarMapa();

            disable = true;
            user = new QueryUser();
            usuarioActual = altaUsuarioBusiness.obtenerUsuario();
            obtenerUsuario();
            cargarIp();
            obtenerQueryGroup();

            selectDealer = new VcDealerEntity();
            listaDealer = new ArrayList<>();
            selectZonas = new VcZonasEntity();
            listaZonas = new ArrayList<>();
            cargarDatosSupervisor();
            selectCategoria = new Categoria();
            listaCategoria = UtilMap.armarComboCategoria(Parametros.ehCategoria);
        } catch (Exception e) {
            SysMessage.error("Error al Iniciar: " + e.getMessage(), null);
        }

    }


    /**
     * cargarDatosSupervisor()
     * Metodo para saber si el usuario Actual es supervisor, si es supervisor se obtendra la lista de Dealer a los cuales tiene acceso
     * el supervisor
     */
    private void cargarDatosSupervisor() {
        if (usuarioActual != null) {
            supervisor = vcPermisosSupervisorBL.getSupervisorForUsuario(usuarioActual.getUsuarioId(), usuarioActual.getNombre());
            if (supervisor != null) {
                List<VcSupervisorDealerZonasEntity> lista = vcSupervisorDealerZOnasBL.getListForSupervisor(supervisor.getIdPermisosSupervisor());
                if (lista != null) {
                    List<VcDealerEntity> listaDealerAuxiliar = new ArrayList<>();
                    for (VcSupervisorDealerZonasEntity sdz : lista) {
                        VcDealerEntity dealer = sdz.getVcDealerZonasByIdDealerZonas().getVcDealerByIdVcDealer();
                        listaDealerAuxiliar.add(dealer);
                    }
                    this.listaDealer = ordenarListaDealer(listaDealerAuxiliar);
                }
            } else {
                SysMessage.warn("Usuario: " + login + " no es Supervisor", null);
            }
        }
    }

    /**
     * cargarComboZonas
     *
     * @param vcDealer Metodo para cargar el combo Zonas, el combo cambia cada vez que se selecciona un Dealer o Distribuidor
     *                 comboZonas es comboSucursal vcDealer es un Objeto Deales o Distribuidor
     */
    public void cargarComboZonas(VcDealerEntity vcDealer) {
        if (supervisor != null) {
            List<VcDealerZonasEntity> lisaZonas = vcDealerZonasBL.getListForDealer(vcDealer.getIdVcDealer(), supervisor.getIdPermisosSupervisor());
            if (lisaZonas != null) {
                List<VcZonasEntity> listaZonasEntity = new ArrayList<>();
                for (VcDealerZonasEntity zonas : lisaZonas) {
                    VcZonasEntity zonasEntity = zonas.getVcZonasByIdVcZonas();
                    listaZonasEntity.add(zonasEntity);
                }
                this.listaZonas = ordenarListaZonas(listaZonasEntity);
            }
        }
    }

    /**
     * @param listaInicio Lisra de Dealer pertenecientes al supervisor actual
     * @return Retorna una Lista de Dealer o Distribuidores sin elementos Repetidos
     * Este metodo solo elimina los Deales Repetidos
     */
    private List<VcDealerEntity> ordenarListaDealer(List<VcDealerEntity> listaInicio) {
        List<VcDealerEntity> lista = new ArrayList<>();
        if (listaInicio != null) {
            Map<Long, VcDealerEntity> mapaDealer = new HashMap<>();
            for (VcDealerEntity dealerEntity : listaInicio) {
                mapaDealer.put(dealerEntity.getIdVcDealer(), dealerEntity);
            }
            for (Map.Entry<Long, VcDealerEntity> p : mapaDealer.entrySet()) {
                lista.add(p.getValue());
            }
        }
        return lista;
    }

    /**
     * @param listaInicio Lista de Zonas o Sucursales que pertenecen al Dealer o Distribuidor seleccionado
     * @return Lista de Zonas sin elementos repetidos
     * Este metodo solo elimina los Zonas Repetidos
     */
    private List<VcZonasEntity> ordenarListaZonas(List<VcZonasEntity> listaInicio) {
        List<VcZonasEntity> lista = new ArrayList<>();
        if (listaInicio != null) {
            Map<Long, VcZonasEntity> mapaZonas = new HashMap<>();
            for (VcZonasEntity zonasEntity : listaInicio) {
                mapaZonas.put(zonasEntity.getIdVcZonas(), zonasEntity);
            }
            for (Map.Entry<Long, VcZonasEntity> p : mapaZonas.entrySet()) {
                lista.add(p.getValue());
            }
        }
        return lista;
    }

    private void cargarIp() {
        try {
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                    .getRequest();
            ip = UtilUrl.getClientIp(request);
        } catch (Exception e) {
            log.error("Error al obtener ip: ", e);
        }
    }

    /**
     * queryUserGroups obtiene todos los permisos y las Zonas del Vendedor
     * queryGroups obtiene los permisos de queryUserGroup trae todos los permisos y las Zonas disponibles
     */
    private void obtenerQueryGroup() {
        try {
            idAuditoriaHistorial = auditoria.guardarAuditoria(Calendar.getInstance(), login, ip,
                    DescriptorBitacora.GESTION_EH_MODIFICAR_VENDEDOR.getFormulario(), 0, usuarioActual != null ? usuarioActual.getLogin() : "", null);
            TokenGson tokenGson = altaUsuarioBusiness.getToken(login, idAuditoriaHistorial);
            token = tokenGson.getAccessToken();
        } catch (UnknownHostException e) {
            auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
            log.error("[UnknownHostException][getToken] No se pudo alcanzar la url: " + Parametros.restUrlToken + e.getMessage());
            SysMessage.error("[getToken] No se pudo alcanzar la url: " + Parametros.restUrlToken + " [UnknownHostException]: " + e.getMessage(), null);
        } catch (HttpStatusException e) {
            auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
            log.error("[HttpStatusException][getToken] Problema con http client " + e.getMessage() + " " + e.getStatusResponse().getCode());
            SysMessage.error("[getToken] " + Parametros.restMensajeErrorToken + "[HttpStatusException]: " + e.getMessage(), null);
        } catch (RestExcepcion restExcepcion) {
            auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(restExcepcion));
            log.error("[RestExcepcion][getToken] No se pudo completar la consulta: " + restExcepcion.getMessage(), restExcepcion);
            SysMessage.error("[getToken] " + Parametros.restMensajeErrorToken + "[RestExcepcion]: " + restExcepcion.getMessage(), null);
        } catch (com.myapps.vista_ccc.white_list.error.HttpStatusException e) {
            auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
            log.error("[HttpStatusException] No se pudo completar la consulta: " + e.getMessage(), e);
            SysMessage.error("[getToken] " + Parametros.restMensajeErrorToken + "[HttpStatusException]: " + e.getMessage(), null);
        } catch (IOException e) {
            auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
            log.error("[IOException][getToken] No se pudo completar la consulta: " + e.getMessage(), e);
            SysMessage.error("[getToken] " + Parametros.restMensajeErrorToken + "[IOException]: " + e.getMessage(), null);
        } catch (Exception e) {
            auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
            log.error("[Exception][getToken] No se pudo completar la consulta: " + e.getMessage(), e);
            SysMessage.error("[getToken] " + Parametros.restMensajeErrorToken + "[Exception]: " + e.getMessage(), null);
        }
        if (token != null) {
            try {
                idAuditoriaHistorial = auditoria.guardarAuditoria(Calendar.getInstance(), login, ip,
                        DescriptorBitacora.GESTION_EH_MODIFICAR_VENDEDOR.getFormulario(), 0, usuarioActual != null ? usuarioActual.getLogin() : "", null);
                queryGroups = altaUsuarioBusiness.getQueryGroup(login, idAuditoriaHistorial, token);
            } catch (com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException e) {
                auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                log.error("[HttpStatusException][getQueryGroup] Problema con http client " + e.getMessage() + " " + e.getGenerica().toString());
                mostrarMensajeGenerico(e.getMessage(), e.getGenerica().getSummary());
            } catch (URISyntaxException e) {
                auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                log.error("[URISyntaxException][getQueryGroup] No se pudo completar la consulta: " + e.getMessage(), e);
                SysMessage.error("[getQueryGroup] " + Parametros.restMensajeErrorQueryGroup + "[RestExcepcion]: " + e.getMessage(), null);
            } catch (IOException e) {
                auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                log.error("[IOException][getQueryGroup] No se pudo completar la consulta: " + e.getMessage(), e);
                SysMessage.error("[getQueryGroup] " + Parametros.restMensajeErrorQueryGroup + "[IOException]: " + e.getMessage(), null);
            } catch (Exception e) {
                auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                log.error("[Exception][getQueryGroup] No se pudo completar la consulta: " + e.getMessage(), e);
                SysMessage.error("[getQueryGroup] " + Parametros.restMensajeErrorQueryGroup + "[Exception]: " + e.getMessage(), null);
            }
            if (queryGroups != null) {
                cargarPermisos(Arrays.asList(queryGroups));
            }

        }

    }


    /**
     * @param queryGroups Lista de Permisos y Zonas Globales
     *                    Metodo encargado de cargar los permisos a las listapermisos
     *                    tambien renombra los permisos si eston fueron configurados en la properties
     */
    private void cargarPermisos(List<QueryGroup> queryGroups) {
        if (queryGroups != null) {
            listapermisos = new ArrayList<>();
            //Cargar permisos QuerGoup
            for (QueryGroup queryGroup : queryGroups) {
                if (!queryGroup.getName().equals(Parametros.ehIdentificadorSucursales)) {
                    listapermisos.add(new Permisos(queryGroup.getId(), queryGroup.getName(), false, ""));
                }
            }
            //Renombrar
            for (Permisos permiso : listapermisos) {
                permiso.setNombre(UtilEH.obtenerNombre(permiso.getId(), permiso.getNombre()));
            }
        }
    }


    /**
     * Metodo para limpieza completa del Formulario
     */
    public void limpiarTodo() {
        this.token = null;
        this.estado = null;
        this.user = new QueryUser();
        this.msisdn = null;
        this.limite = null;
        disable = true;
        selectCategoria = new Categoria();
        selectZonas = new VcZonasEntity();
        selectDealer = new VcDealerEntity();
        //   queryGroups = null;
        obtenerQueryGroup();
        if (listapermisos != null) {
            for (Permisos permiso : listapermisos) {
                permiso.setEstado(false);
                permiso.setEstadoString(null);
            }
        }
    }


    /**
     * @param user Metodo para validar si el Vendedor obtenido es Valido y cargar sus Datos
     */
    public void validarUsuario(QueryUser user) {
        try {
            if (user != null && user.getId() != null) {
                this.user = user;
                this.estado = Boolean.TRUE.equals(user.getEnabled()) ? "Habilitado" : "Deshabilitado";
            } else if (user == null) {
                mostrarMensajeGenerico(Parametros.restMensajeEmptyQueryUSer, "WARN");
            } else {
                mostrarMensajeGenerico(Parametros.restMensajeErrorQueryUSer, ERROR);
            }
        } catch (Exception e) {
            SysMessage.error(e.getMessage(), null);
        }

    }


    public void mostrarMensajeGenerico(String mensaje, String categoria) {
        switch (categoria) {
            case "INFO":
                SysMessage.info(mensaje, null);
                break;
            case "WARN":
                SysMessage.warn(mensaje, null);
                break;
            case ERROR:
                SysMessage.error(mensaje, null);
                break;
            default:
                SysMessage.error("Categoria no Definida " + mensaje, null);
                break;
        }
    }


    private void obtenerUsuario() {
        try {
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                    .getRequest();
            login = (String) request.getSession().getAttribute("TEMP$USER_NAME");
            log.info("Usuario: " + login + ", ingresando a vista de Alta Vendedor.");
        } catch (Exception e) {
            log.error("Error al obtener usuario: ", e);
        }
    }

    public void crearUsuario() {
        boolean creacion = false;
        if (supervisor != null) {
            try {
                idAuditoriaHistorial = auditoria.guardarAuditoria(Calendar.getInstance(), login, ip,
                        DescriptorBitacora.GESTION_EH_MODIFICAR_VENDEDOR.getFormulario(), 0, usuarioActual != null ? usuarioActual.getLogin() : "", null);
                TokenGson tokenGson = altaUsuarioBusiness.getToken(login, idAuditoriaHistorial);
                token = tokenGson.getAccessToken();
            } catch (UnknownHostException e) {
                auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                log.error("[UnknownHostException][getToken] No se pudo alcanzar la url: " + Parametros.restUrlToken + e.getMessage());
                SysMessage.error("[getToken] No se pudo alcanzar la url: " + Parametros.restUrlToken + " [UnknownHostException]: " + e.getMessage(), null);
            } catch (com.myapps.vista_ccc.white_list.error.HttpStatusException e) {
                auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                log.error("[HttpStatusException][getToken] Problema con http client " + e.getMessage() + " " + e.getStatusResponse().getCode());
                SysMessage.error("[getToken] " + Parametros.restMensajeErrorToken + "[HttpStatusException]: " + e.getMessage(), null);
            } catch (HttpStatusException e) {
                auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                log.error("[HttpStatusException][getToken] Problema con http client " + e.getMessage() + " " + e.getStatusResponse().getCode());
                SysMessage.error("[getToken] " + Parametros.restMensajeErrorToken + "[HttpStatusException]: " + e.getMessage(), null);
            } catch (RestExcepcion restExcepcion) {
                auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(restExcepcion));
                log.error("[RestExcepcion][getToken] No se pudo completar la consulta: " + restExcepcion.getMessage(), restExcepcion);
                SysMessage.error("[getToken] " + Parametros.restMensajeErrorToken + "[RestExcepcion]: " + restExcepcion.getMessage(), null);
            } catch (IOException e) {
                auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                log.error("[IOException][getToken] No se pudo completar la consulta: " + e.getMessage(), e);
                SysMessage.error("[getToken] " + Parametros.restMensajeErrorToken + "[IOException]: " + e.getMessage(), null);
            } catch (Exception e) {
                auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                log.error("[Exception][getToken] No se pudo completar la consulta: " + e.getMessage(), e);
                SysMessage.error("[getToken] " + Parametros.restMensajeErrorToken + "[Exception]: " + e.getMessage(), null);
            }

            if (token != null) {
                try {
                    idAuditoriaHistorial = auditoria.guardarAuditoria(Calendar.getInstance(), login, ip,
                            DescriptorBitacora.GESTION_EH_MODIFICAR_VENDEDOR.getFormulario(), 0, usuarioActual != null ? usuarioActual.getLogin() : "", null);
                    user.setAttributes(new Attributes());
                    user.getAttributes().getEhCodeParent().add(0, vcDealerBL.getVcDealer(selectDealer.getIdVcDealer()).getEh());
                    user.getAttributes().getMaxActivations().add(0, limite);
                    user.getAttributes().getMsisdn().add(0, Parametros.ehPrefijoTelefono + msisdn);
                    user.getAttributes().getPreactivationRecyclePeriod().add(0, String.valueOf(selectCategoria.getId()));
                    creacion = altaUsuarioBusiness.createUser(user, user.getAttributes().getMsisdn().get(0), login, idAuditoriaHistorial, token);
                } catch (com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException e) {
                    auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                    log.error("[HttpStatusException][createUser] Problema con http client " + e.getMessage() + " " + e.getGenerica());
                    mostrarMensajeGenerico(e.getMessage(), e.getGenerica().getSummary());
                } catch (HttpStatusException e) {
                    auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                    log.error("[HttpStatusException][createUser] Problema con http client " + e.getMessage() + " " + e.getStatusResponse().getCode());
                    SysMessage.error("[createUser] " + Parametros.restMensajeErrorUpdateUser + "[HttpStatusException]: " + e.getMessage(), null);
                } catch (RestExcepcion restExcepcion) {
                    auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(restExcepcion));
                    log.error("[RestExcepcion][createUser] No se pudo completar la consulta: " + restExcepcion.getMessage(), restExcepcion);
                    SysMessage.error("[createUser] " + Parametros.restMensajeErrorUpdateUser + "[RestExcepcion]: " + restExcepcion.getMessage(), null);
                } catch (IOException e) {
                    auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                    log.error("[IOException][createUser] No se pudo completar la consulta: " + e.getMessage(), e);
                    SysMessage.error("[createUser] " + Parametros.restMensajeErrorUpdateUser + "[IOException]: " + e.getMessage(), null);
                } catch (Exception e) {
                    auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                    log.error("[Exception][createUser] No se pudo completar la consulta: " + e.getMessage(), e);
                    SysMessage.error("[createUser] " + Parametros.restMensajeErrorUpdateUser + "[Exception]: " + e.getMessage(), null);
                }

                if (creacion) {
                    SysMessage.info(Parametros.restMensajeCreateUser, null);
                    obtenerQueryUser();
                    try {
                        if (listapermisos != null) {
                            for (Permisos permiso : listapermisos) {
                                if (permiso != null && permiso.isEstado()) {
                                    if (altaUsuarioBusiness.addGroupUser(user.getId(), permiso.getId(), msisdn, login, idAuditoriaHistorial, token)) {
                                        SysMessage.info("Permiso " + permiso.getNombre() + " añadido", null);
                                    } else {
                                        SysMessage.warn("Error al  añadir el Permiso " + permiso.getNombre(), null);
                                    }
                                }
                            }
                        }
                    } catch (com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException e) {
                        auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                        log.error("[HttpStatusException][addGroupUser] Problema con http client " + e.getMessage() + " " + e.getGenerica());
                        mostrarMensajeGenerico(e.getMessage(), e.getGenerica().getSummary());
                    } catch (IOException e) {
                        auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                        log.error("[IOException][addGroupUser]  No se pudo completar la consulta: " + e.getMessage(), e);
                        SysMessage.error("[addGroupUser] " + Parametros.restMensajeErrorAddGroupUser + "[IOException]: " + e.getMessage(), null);
                    } catch (Exception e) {
                        auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                        log.error("[Exception][addGroupUser]  No se pudo completar la consulta: " + e.getMessage(), e);
                        SysMessage.error("[addGroupUser] " + Parametros.restMensajeErrorAddGroupUser + "[Exception]: " + e.getMessage(), null);
                    }


                    try {
                        //zona
                        VcZonasEntity zona = obtenerZona(selectZonas.getIdVcZonas(), listaZonas);
                        //adicionar zona
                        if (queryGroups != null && zona != null) {
                            for (QueryGroup group : Arrays.asList(queryGroups)) {
                                if (group != null && group.getName().equals(Parametros.ehIdentificadorSucursales) && group.getSubGroups() != null) {
                                    for (SubGroup sub : group.getSubGroups()) {
                                        if (sub != null && sub.getName().toUpperCase().trim().equals(zona.getNombre().toUpperCase().trim())) {
                                            altaUsuarioBusiness.addGroupUser(user.getId(), sub.getId(), msisdn, login, idAuditoriaHistorial, token);
                                        }
                                    }
                                }
                            }
                        }
                    } catch (com.myapps.vista_ccc.gestion_eh.servicio.HttpStatusException e) {
                        auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                        log.error("[HttpStatusException][addGroupUser] Problema con http client " + e.getMessage() + " " + e.getGenerica().toString());
                        mostrarMensajeGenerico(e.getMessage(), e.getGenerica().getSummary());
                    } catch (IOException e) {
                        auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                        log.error("[IOException][addGroupUser] No se pudo completar la consulta: " + e.getMessage(), e);
                        SysMessage.error("[addGroupUser] " + Parametros.restMensajeErrorAddGroupUser + "[IOException]: " + e.getMessage(), null);
                    } catch (Exception e) {
                        auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
                        log.error("[Exception][addGroupUser] No se pudo completar la consulta: " + e.getMessage(), e);
                        SysMessage.error("[addGroupUser] " + Parametros.restMensajeErrorAddGroupUser + "[Exception]: " + e.getMessage(), null);
                    }

                    try {
                        controlerBitacora.accion(DescriptorBitacora.GESTION_EH_ALTA_VENDEDOR, "Se creo el Vendedor con Codigo: " + user.getUsername());
                    } catch (Exception e) {
                        log.error("Error al guardar bitacora en el sistema: ", e);
                        SysMessage.error("Error al guardar bitacora", null);
                    }
                    limpiarTodo();
                }


            }
        } else {
            SysMessage.warn("Usuario: " + login + " no es Supervisor", null);
        }


    }

    private void obtenerQueryUser() {
        try {
            idAuditoriaHistorial = auditoria.guardarAuditoria(Calendar.getInstance(), login, ip,
                    DescriptorBitacora.GESTION_EH_MODIFICAR_VENDEDOR.getFormulario(), 0, usuarioActual != null ? usuarioActual.getLogin() : "", null);
            validarUsuario(altaUsuarioBusiness.getQueryUser(login, idAuditoriaHistorial, user.getUsername(), token));
        } catch (UnknownHostException e) {
            auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
            log.error("[UnknownHostException][getQueryUser] No se pudo alcanzar la url: " + Parametros.restUrlQueryUSer + e.getMessage());
            SysMessage.error("[getQueryUser] No se pudo alcanzar la url: " + Parametros.restUrlQueryUSer + " [UnknownHostException]: " + e.getMessage(), null);
        } catch (com.myapps.vista_ccc.white_list.error.HttpStatusException e) {
            auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
            log.error("[IOException][getQueryUser] No se pudo completar la consulta: " + e.getMessage(), e);
            SysMessage.error("[getQueryUser] " + Parametros.restMensajeErrorUpdateUser + "[IOException]: " + e.getMessage(), null);
        } catch (HttpStatusException e) {
            auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
            log.error(" [HttpStatusException][getQueryUser] Problema con http client " + e.getMessage() + " " + e.getStatusResponse().getCode());
            SysMessage.error("[getQueryUser] " + Parametros.restMensajeErrorUpdateUser + "[HttpStatusException]: " + e.getMessage(), null);
        } catch (RestExcepcion restExcepcion) {
            auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(restExcepcion));
            log.error("[RestExcepcion][getQueryUser] No se pudo completar la consulta: " + restExcepcion.getMessage(), restExcepcion);
            SysMessage.error("[getQueryUser] " + Parametros.restMensajeErrorUpdateUser + "[RestExcepcion]: " + restExcepcion.getMessage(), null);
        } catch (URISyntaxException e) {
            auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
            log.error("[URISyntaxException][getQueryUser] No se pudo completar la consulta: " + e.getMessage(), e);
            SysMessage.error("[getQueryUser] " + Parametros.restMensajeErrorUpdateUser + "[URISyntaxException]: " + e.getMessage(), null);
        } catch (IOException e) {
            auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
            log.error("[IOException][getQueryUser] No se pudo completar la consulta: " + e.getMessage(), e);
            SysMessage.error("[getQueryUser] " + Parametros.restMensajeErrorUpdateUser + "[IOException]: " + e.getMessage(), null);
        } catch (Exception e) {
            auditoria.guardarExcepcion(idAuditoriaHistorial, stackTraceToString(e));
            log.error("[Exception][getQueryUser] No se pudo completar la consulta: " + e.getMessage(), e);
            SysMessage.error("[getQueryUser] " + Parametros.restMensajeErrorUpdateUser + "[Exception]: " + e.getMessage(), null);
        }
    }

    /**
     * Metodo para cargar validaciones, de Properties a mapa
     */
    public void cargarMapa() {
        mapa = new HashMap<>();
        mapa.put("EH.SUMMARY.MENSAJES", Parametros.ehSummaryMensajes);

        mapa.put("EH.EXPRESION.CODIGO", Parametros.ehExpresionCodigo);
        mapa.put("EH.EXPRESION.CODIGO.BUSCADOR", Parametros.ehExpresionCodigoBuscador);
        mapa.put("EH.EXPRESION.CODIGO.MENSAJE.ERROR", Parametros.ehExpresionCodigoMensajeError);
        mapa.put("EH.EXPRESION.CODIGO.MENSAJE.REQUERIDO", Parametros.ehExpresionCodigoMensajeRequerido);

        mapa.put("EH.EXPRESION.NOMBRES", Parametros.ehExpresionNombres);
        mapa.put("EH.EXPRESION.NOMBRES.MENSAJE.ERROR", Parametros.ehExpresionNombresMensajeError);
        mapa.put("EH.EXPRESION.NOMBRES.MENSAJE.REQUERIDO", Parametros.ehExpresionNombresMensajeRequerido);

        mapa.put("EH.EXPRESION.APELLIDOS", Parametros.ehExpresionApellidos);
        mapa.put("EH.EXPRESION.APELLIDOS.MENSAJE.ERROR", Parametros.ehExpresionApellidosMensajeError);
        mapa.put("EH.EXPRESION.APELLIDOS.MENSAJE.REQUERIDO", Parametros.ehExpresionApellidosMensajeRequerido);

        mapa.put("EH.EXPRESION.TELEFONO", Parametros.ehExpresionTelefono);
        mapa.put("EH.EXPRESION.TELEFONO.MENSAJE.ERROR", Parametros.ehExpresionTelefonoMensajeError);
        mapa.put("EH.EXPRESION.TELEFONO.MENSAJE.REQUERIDO", Parametros.ehExpresionTelefonoMensajeRequerido);

        mapa.put("EH.EXPRESION.EMAIL", Parametros.ehExpresionEmail);
        mapa.put("EH.EXPRESION.EMAIL.MENSAJE.ERROR", Parametros.ehExpresionEmailMensajeError);

        mapa.put("EH.EXPRESION.LIMITE", Parametros.ehExpresionLimite);
        mapa.put("EH.EXPRESION.LIMITE.MENSAJE.ERROR", Parametros.getEhExpresionLimiteMensajeError);
        mapa.put("EH.EXPRESION.LIMITE.MENSAJE.REQUERIDO", Parametros.getEhExpresionLimiteMensajeRequerido);
    }

    public VcZonasEntity obtenerZona(long idZona, List<VcZonasEntity> listaZonas) {
        VcZonasEntity vcZonasEntity = null;
        if (listaZonas != null) {
            for (VcZonasEntity zona : listaZonas) {
                if (idZona == zona.getIdVcZonas()) {
                    vcZonasEntity = zona;
                    break;
                }
            }
        }
        return vcZonasEntity;
    }

    public String stackTraceToString(Throwable e) {
        CharArrayWriter sw = new CharArrayWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        pw.close();

        return sw.toString();
    }


    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }


    public QueryUser getUser() {
        return user;
    }

    public void setUser(QueryUser user) {
        this.user = user;
    }

    public Map<String, String> getMapResponseWhiteList() {
        return mapResponseWhiteList;
    }

    public void setMapResponseWhiteList(Map<String, String> mapResponseWhiteList) {
        this.mapResponseWhiteList = mapResponseWhiteList;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<VcDealerEntity> getListaDealer() {
        return listaDealer;
    }

    public void setListaDealer(List<VcDealerEntity> listaDealer) {
        this.listaDealer = listaDealer;
    }

    public VcDealerEntity getSelectDealer() {
        return selectDealer;
    }

    public void setSelectDealer(VcDealerEntity selectDealer) {
        this.selectDealer = selectDealer;
    }

    public List<VcZonasEntity> getListaZonas() {
        return listaZonas;
    }

    public void setListaZonas(List<VcZonasEntity> listaZonas) {
        this.listaZonas = listaZonas;
    }

    public VcZonasEntity getSelectZonas() {
        return selectZonas;
    }

    public void setSelectZonas(VcZonasEntity selectZonas) {
        this.selectZonas = selectZonas;
    }

    public boolean isDisable() {
        return disable;
    }

    public void setDisable(boolean disable) {
        this.disable = disable;
    }


    public List<Categoria> getListaCategoria() {
        return listaCategoria;
    }

    public void setListaCategoria(List<Categoria> listaCategoria) {
        this.listaCategoria = listaCategoria;
    }

    public Categoria getSelectCategoria() {
        return selectCategoria;
    }

    public void setSelectCategoria(Categoria selectCategoria) {
        this.selectCategoria = selectCategoria;
    }

    public List<Permisos> getListapermisos() {
        return listapermisos;
    }

    public void setListapermisos(List<Permisos> listapermisos) {
        this.listapermisos = listapermisos;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getLimite() {
        return limite;
    }

    public void setLimite(String limite) {
        this.limite = limite;
    }

    public Map<String, String> getMapa() {
        return mapa;
    }

    public void setMapa(Map<String, String> mapa) {
        this.mapa = mapa;
    }
}
