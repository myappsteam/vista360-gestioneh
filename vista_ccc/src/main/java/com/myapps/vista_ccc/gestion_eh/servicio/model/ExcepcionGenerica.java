package com.myapps.vista_ccc.gestion_eh.servicio.model;

import java.io.Serializable;

public class ExcepcionGenerica implements Serializable {
    private static final long serialVersionUID = 1L;
    private int codigo;
    private String nombre;
    private String mensaje;
    private String summary;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public ExcepcionGenerica(int codigo, String nombre, String mensaje, String summary) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.mensaje = mensaje;
        this.summary = summary;
    }

    public ExcepcionGenerica() {
    }

    @Override
    public String toString() {
        return "Excepcion{" +
                "codigo=" + codigo +
                ", nombre='" + nombre + '\'' +
                ", mensaje='" + mensaje + '\'' +
                '}';
    }
}
