package com.myapps.vista_ccc.gestion_eh.business;


import com.myapps.vista_ccc.gestion_eh.dao.VcDealerZonasDAO;
import com.myapps.vista_ccc.gestion_eh.entity.VcDealerZonasEntity;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
public class VcDealerZonasBL implements Serializable {

    private static final long serialVersionUID = 1L;
    private static Logger log = Logger.getLogger(VcDealerZonasBL.class);


    @Inject
    private VcDealerZonasDAO dao;

    public void save(VcDealerZonasEntity nodo) throws Exception {
        dao.save(nodo);
    }

    public VcDealerZonasEntity getVcDealerZona(long idNodo) throws Exception {
        return dao.get(idNodo);
    }

    public void update(VcDealerZonasEntity nodo) throws Exception {
        dao.update(nodo);
    }

    public void remove(VcDealerZonasEntity nodo) throws Exception {
        dao.remove(nodo);
    }

    public List<VcDealerZonasEntity> getList() throws Exception {
        return dao.getList();
    }


    public List<VcDealerZonasEntity> getListForDealer(long idDealer, long idSupervisor) {
        List<VcDealerZonasEntity> valor = new ArrayList<>();
        try {
            valor = dao.getListForDealer(idDealer, idSupervisor);
        } catch (Exception e) {
            log.error("Exception: " + e.getMessage());
        }
        return valor;
    }

    public List<VcDealerZonasEntity> getListForDealer(long idDealer) {
        List<VcDealerZonasEntity> valor = new ArrayList<>();
        try {
            valor = dao.getListForDealer(idDealer);
        } catch (Exception e) {
            log.error("Exception: " + e.getMessage());
        }
        return valor;
    }

}
