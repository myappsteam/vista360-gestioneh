package com.myapps.vista_ccc.gestion_eh.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "VC_ZONAS")
public class VcZonasEntity implements Serializable {
    private long idVcZonas;
    private String nombre;
    private String observacion;
    private boolean estado;
    private Collection<VcDealerZonasEntity> vcDealerZonasByIdVcZonas;

    @Id
    @Column(name = "ID_ZONAS")
    @SequenceGenerator(name = "pk_sequence", sequenceName = "SEQ_VC_ZONAS_ID_ZONAS ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence")
    public long getIdVcZonas() {
        return idVcZonas;
    }

    public void setIdVcZonas(long idVcZonas) {
        this.idVcZonas = idVcZonas;
    }

    @Basic
    @Column(name = "NOMBRE")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "OBSERVACION")
    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    @Basic
    @Column(name = "ESTADO")
    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VcZonasEntity that = (VcZonasEntity) o;
        return idVcZonas == that.idVcZonas &&
                estado == that.estado &&
                Objects.equals(nombre, that.nombre) &&
                Objects.equals(observacion, that.observacion);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idVcZonas, nombre, observacion, estado);
    }

    @OneToMany(mappedBy = "vcZonasByIdVcZonas")
    public Collection<VcDealerZonasEntity> getVcDealerZonasByIdVcZonas() {
        return vcDealerZonasByIdVcZonas;
    }

    public void setVcDealerZonasByIdVcZonas(Collection<VcDealerZonasEntity> vcDealerZonasByIdVcZonas) {
        this.vcDealerZonasByIdVcZonas = vcDealerZonasByIdVcZonas;
    }
}
