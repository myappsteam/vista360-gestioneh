package com.myapps.vista_ccc.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;


/**
 * The persistent class for the VC_LOG_EXCEPCION database table.
 * 
 */
@Entity
@Table(name="VC_LOG_EXCEPCION")
@NamedQuery(name="VcLogExcepcion.findAll", query="SELECT v FROM VcLogExcepcion v")
public class VcLogExcepcion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="VC_LOG_EXCEPCION_IDLOGEXCEPCION_GENERATOR", sequenceName="SEQ_VC_LOG_EXCEPCION", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="VC_LOG_EXCEPCION_IDLOGEXCEPCION_GENERATOR")
	@Column(name="ID_LOG_EXCEPCION")
	private long idLogExcepcion;

	private String excepcion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CREACION")
	private Calendar fechaCreacion;

	//uni-directional many-to-one association to VcLogAuditoria
	@ManyToOne
	@JoinColumn(name="ID_LOG_AUDITORIA")
	private VcLogAuditoria vcLogAuditoria;

	public VcLogExcepcion() {
	}

	public long getIdLogExcepcion() {
		return this.idLogExcepcion;
	}

	public void setIdLogExcepcion(long idLogExcepcion) {
		this.idLogExcepcion = idLogExcepcion;
	}

	public String getExcepcion() {
		return this.excepcion;
	}

	public void setExcepcion(String excepcion) {
		this.excepcion = excepcion;
	}

	public Calendar getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Calendar fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public VcLogAuditoria getVcLogAuditoria() {
		return this.vcLogAuditoria;
	}

	public void setVcLogAuditoria(VcLogAuditoria vcLogAuditoria) {
		this.vcLogAuditoria = vcLogAuditoria;
	}

}