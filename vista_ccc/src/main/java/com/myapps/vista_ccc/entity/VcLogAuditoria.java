package com.myapps.vista_ccc.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;


/**
 * The persistent class for the VC_LOG_AUDITORIA database table.
 * 
 */
@Entity
@Table(name="VC_LOG_AUDITORIA")
@NamedQuery(name="VcLogAuditoria.findAll", query="SELECT v FROM VcLogAuditoria v")
public class VcLogAuditoria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="VC_LOG_AUDITORIA_IDLOGAUDITORIA_GENERATOR", sequenceName="SEQ_VC_LOG_AUDITORIA", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="VC_LOG_AUDITORIA_IDLOGAUDITORIA_GENERATOR")
	@Column(name="ID_LOG_AUDITORIA")
	private long idLogAuditoria;

	private String adicional;

	private Integer comando;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CONSULTA")
	private Calendar fechaConsulta;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CREACION")
	private Calendar fechaCreacion;

	private String ip;

	private String isdn;

	private String usuario;

	private String vista;

	public VcLogAuditoria() {
	}

	public long getIdLogAuditoria() {
		return this.idLogAuditoria;
	}

	public void setIdLogAuditoria(long idLogAuditoria) {
		this.idLogAuditoria = idLogAuditoria;
	}

	public String getAdicional() {
		return this.adicional;
	}

	public void setAdicional(String adicional) {
		this.adicional = adicional;
	}

	public Integer getComando() {
		return this.comando;
	}

	public void setComando(Integer comando) {
		this.comando = comando;
	}

	public Calendar getFechaConsulta() {
		return this.fechaConsulta;
	}

	public void setFechaConsulta(Calendar fechaConsulta) {
		this.fechaConsulta = fechaConsulta;
	}

	public Calendar getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Calendar fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getIsdn() {
		return this.isdn;
	}

	public void setIsdn(String isdn) {
		this.isdn = isdn;
	}

	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getVista() {
		return this.vista;
	}

	public void setVista(String vista) {
		this.vista = vista;
	}

}