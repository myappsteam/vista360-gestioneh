package com.myapps.vista_ccc.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;


/**
 * The persistent class for the VC_LOG_ADICIONAL database table.
 * 
 */
@Entity
@Table(name="VC_LOG_ADICIONAL")
@NamedQuery(name="VcLogAdicional.findAll", query="SELECT v FROM VcLogAdicional v")
public class VcLogAdicional implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="VC_LOG_ADICIONAL_IDLOGADICIONAL_GENERATOR", sequenceName="SEQ_VC_LOG_ADICIONAL", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="VC_LOG_ADICIONAL_IDLOGADICIONAL_GENERATOR")
	@Column(name="ID_LOG_ADICIONAL")
	private long idLogAdicional;

	private String codigo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CREACION")
	private Calendar fechaCreacion;

	private String valor;

	//uni-directional many-to-one association to VcLogAuditoria
	@ManyToOne
	@JoinColumn(name="ID_LOG_AUDITORIA")
	private VcLogAuditoria vcLogAuditoria;

	public VcLogAdicional() {
	}

	public long getIdLogAdicional() {
		return this.idLogAdicional;
	}

	public void setIdLogAdicional(long idLogAdicional) {
		this.idLogAdicional = idLogAdicional;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Calendar getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Calendar fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getValor() {
		return this.valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public VcLogAuditoria getVcLogAuditoria() {
		return this.vcLogAuditoria;
	}

	public void setVcLogAuditoria(VcLogAuditoria vcLogAuditoria) {
		this.vcLogAuditoria = vcLogAuditoria;
	}

}