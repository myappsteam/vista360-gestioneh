package com.myapps.vista_ccc.entity;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the VC_EXCEPCION_ELEMENTOS_RED database table.
 * 
 */
@Entity
@Table(name="VC_CONFIG_ELEMENTO_RED")
@NamedQuery(name="VcConfigElementoRed.findAll", query="SELECT v FROM VcConfigElementoRed v")
public class VcConfigElementoRed implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_EXCEPCION")
	private long idExcepcion;

	@Column(name="EXCEPCION_SERVIDOR")
	private String excepcionServidor;
	
	public VcServidor getVcServidor() {
		return vcServidor;
	}

	public void setVcServidor(VcServidor vcServidor) {
		this.vcServidor = vcServidor;
	}

	@ManyToOne
	@JoinColumn(name="ID_SERVIDOR")
	private VcServidor vcServidor;
	//bi-directional many-to-one association to VcServidor
//	@ManyToOne
//	@JoinColumn(name="ID_SERVIDOR")
//	private VcServidor vcServidor;

	public VcConfigElementoRed() {
	}

	public long getIdExcepcion() {
		return this.idExcepcion;
	}

	public void setIdExcepcion(long idExcepcion) {
		this.idExcepcion = idExcepcion;
	}

	public String getExcepcionServidor() {
		return this.excepcionServidor;
	}

	public void setExcepcionServidor(String excepcionServidor) {
		this.excepcionServidor = excepcionServidor;
	}

//	public VcServidor getVcServidor() {
//		return this.vcServidor;
//	}
//
//	public void setVcServidor(VcServidor vcServidor) {
//		this.vcServidor = vcServidor;
//	}

}