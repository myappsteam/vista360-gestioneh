package com.myapps.vista_ccc.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;


/**
 * The persistent class for the VC_ACCOUNT_HISTORY database table.
 * 
 */
@Entity
@Table(name="VC_ACCOUNT_HISTORY")
@NamedQuery(name="VcAccountHistory.findAll", query="SELECT v FROM VcAccountHistory v")
public class VcAccountHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="VC_ACCOUNT_HISTORY_IDACCOUNTHISTORY_GENERATOR", sequenceName="SEQ_VC_ACCOUNT_HISTORY", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="VC_ACCOUNT_HISTORY_IDACCOUNTHISTORY_GENERATOR")
	@Column(name="ID_ACCOUNT_HISTORY")
	private long idAccountHistory;

	@Column(name="ACTUAL_CHARGE_AMT")
	private BigDecimal actualChargeAmt;

	@Column(name="ACTUAL_TAX_AMT")
	private BigDecimal actualTaxAmt;

	@Column(name="ACTUAL_TAX_CODE")
	private String actualTaxCode;

	@Column(name="ACTUAL_VOLUME")
	private BigDecimal actualVolume;
	
	@Lob
	@Column(name="ADDITIONAL_PROPERTY")
	private String additionalProperty;

	@Column(name="AJ_CATEGORY")
	private String ajCategory;

	@Column(name="AJ_CHANNEL_ID")
	private String ajChannelId;

	@Column(name="AJ_EXT_TRANS_ID")
	private String ajExtTransId;

	@Column(name="AJ_PRIMARY_IDENTITY")
	private String ajPrimaryIdentity;

	@Column(name="AJ_REMARK")
	private String ajRemark;

	@Column(name="AJ_TRADE_TIME")
	private String ajTradeTime;

	@Column(name="AJ_TRANS_ID")
	private BigDecimal ajTransId;
	
	@Lob
	@Column(name="AJ_ADDITIONAL_PROPERTY")
	private String ajAdditionalProperty;

	@Column(name="BILL_CYCLE_ID")
	private String billCycleId;

	@Column(name="CALLED_CELL_ID")
	private String calledCellId;

	@Column(name="CALLING_CELL_ID")
	private String callingCellId;

	@Column(name="CDR_SEQ")
	private String cdrSeq;

	@Column(name="CURRENCY_ID_CDR")
	private BigDecimal currencyIdCdr;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="END_TIME")
	private Calendar endTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CREACION")
	private Calendar fechaCreacion;

	@Column(name="FLOW_TYPE")
	private String flowType;

	@Column(name="FREE_CHARGE_AMT")
	private BigDecimal freeChargeAmt;

	@Column(name="FREE_VOLUME")
	private BigDecimal freeVolume;

	private String isdn;

	@Column(name="MEASURE_UNIT")
	private BigDecimal measureUnit;

	@Column(name="MEASURE_UNIT_CDR")
	private BigDecimal measureUnitCdr;

	@Column(name="OTHER_NUMBER")
	private String otherNumber;

	@Column(name="RATING_VOLUME")
	private BigDecimal ratingVolume;

	@Column(name="RE_CARD_SEQUENCE")
	private String reCardSequence;

	@Column(name="RE_CATEGORY")
	private String reCategory;

	@Column(name="RE_CURRENCY_ID")
	private BigDecimal reCurrencyId;

	@Column(name="RE_EXT_RECHARGE_TYPE")
	private String reExtRechargeType;

	@Column(name="RE_EXT_TRANS_ID")
	private String reExtTransId;

	@Column(name="RE_PRIMARY_IDENTITY")
	private String rePrimaryIdentity;

	@Column(name="RE_RECHARGE_AMOUNT")
	private BigDecimal reRechargeAmount;

	@Column(name="RE_RECHARGE_CHANNEL_ID")
	private String reRechargeChannelId;

	@Column(name="RE_RECHARGE_PENALTY")
	private BigDecimal reRechargePenalty;

	@Column(name="RE_RECHARGE_REASON")
	private String reRechargeReason;

	@Column(name="RE_RECHARGE_TAX")
	private BigDecimal reRechargeTax;

	@Column(name="RE_RECHARGE_TYPE")
	private String reRechargeType;

	@Column(name="RE_RESULT_CODE")
	private String reResultCode;

	@Column(name="RE_REVERSAL_FLAG")
	private String reReversalFlag;

	@Column(name="RE_REVERSAL_REASON")
	private String reReversalReason;

	@Column(name="RE_REVERSAL_TIME")
	private String reReversalTime;

	@Column(name="RE_TRADE_TIME")
	private String reTradeTime;

	@Column(name="RE_TRANS_ID")
	private BigDecimal reTransId;
	
	@Lob
	@Column(name="RE_ADDITIONAL_PROPERTY")
	private String reAdditionalProperty;

	@Column(name="REFUND_INDICATOR")
	private String refundIndicator;

	@Column(name="ROAM_FLAG")
	private String roamFlag;

	@Column(name="SERIVE_TYPE")
	private String seriveType;

	@Column(name="SERVICE_CATEGORY")
	private String serviceCategory;

	@Column(name="SERVICE_TYPE_NAME")
	private String serviceTypeName;

	@Column(name="SPECIAL_NUMBER_INDICATOR")
	private String specialNumberIndicator;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="START_TIME")
	private Calendar startTime;

	private String usuario;

	//uni-directional many-to-one association to VcHilo
	@ManyToOne
	@JoinColumn(name="ID_HILO")
	private VcHilo vcHilo;

	public VcAccountHistory() {
	}

	public long getIdAccountHistory() {
		return this.idAccountHistory;
	}

	public void setIdAccountHistory(long idAccountHistory) {
		this.idAccountHistory = idAccountHistory;
	}

	public BigDecimal getActualChargeAmt() {
		return this.actualChargeAmt;
	}

	public void setActualChargeAmt(BigDecimal actualChargeAmt) {
		this.actualChargeAmt = actualChargeAmt;
	}

	public BigDecimal getActualTaxAmt() {
		return this.actualTaxAmt;
	}

	public void setActualTaxAmt(BigDecimal actualTaxAmt) {
		this.actualTaxAmt = actualTaxAmt;
	}

	public String getActualTaxCode() {
		return this.actualTaxCode;
	}

	public void setActualTaxCode(String actualTaxCode) {
		this.actualTaxCode = actualTaxCode;
	}

	public BigDecimal getActualVolume() {
		return this.actualVolume;
	}

	public void setActualVolume(BigDecimal actualVolume) {
		this.actualVolume = actualVolume;
	}

	public String getAjCategory() {
		return this.ajCategory;
	}

	public void setAjCategory(String ajCategory) {
		this.ajCategory = ajCategory;
	}

	public String getAjChannelId() {
		return this.ajChannelId;
	}

	public void setAjChannelId(String ajChannelId) {
		this.ajChannelId = ajChannelId;
	}

	public String getAjExtTransId() {
		return this.ajExtTransId;
	}

	public void setAjExtTransId(String ajExtTransId) {
		this.ajExtTransId = ajExtTransId;
	}

	public String getAjPrimaryIdentity() {
		return this.ajPrimaryIdentity;
	}

	public void setAjPrimaryIdentity(String ajPrimaryIdentity) {
		this.ajPrimaryIdentity = ajPrimaryIdentity;
	}

	public String getAjRemark() {
		return this.ajRemark;
	}

	public void setAjRemark(String ajRemark) {
		this.ajRemark = ajRemark;
	}

	public String getAjTradeTime() {
		return this.ajTradeTime;
	}

	public void setAjTradeTime(String ajTradeTime) {
		this.ajTradeTime = ajTradeTime;
	}

	public BigDecimal getAjTransId() {
		return this.ajTransId;
	}

	public void setAjTransId(BigDecimal ajTransId) {
		this.ajTransId = ajTransId;
	}

	public String getBillCycleId() {
		return this.billCycleId;
	}

	public void setBillCycleId(String billCycleId) {
		this.billCycleId = billCycleId;
	}

	public String getCalledCellId() {
		return this.calledCellId;
	}

	public void setCalledCellId(String calledCellId) {
		this.calledCellId = calledCellId;
	}

	public String getCallingCellId() {
		return this.callingCellId;
	}

	public void setCallingCellId(String callingCellId) {
		this.callingCellId = callingCellId;
	}

	public String getCdrSeq() {
		return this.cdrSeq;
	}

	public void setCdrSeq(String cdrSeq) {
		this.cdrSeq = cdrSeq;
	}

	public BigDecimal getCurrencyIdCdr() {
		return this.currencyIdCdr;
	}

	public void setCurrencyIdCdr(BigDecimal currencyIdCdr) {
		this.currencyIdCdr = currencyIdCdr;
	}

	public Calendar getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Calendar endTime) {
		this.endTime = endTime;
	}

	public Calendar getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Calendar fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getFlowType() {
		return this.flowType;
	}

	public void setFlowType(String flowType) {
		this.flowType = flowType;
	}

	public BigDecimal getFreeChargeAmt() {
		return this.freeChargeAmt;
	}

	public void setFreeChargeAmt(BigDecimal freeChargeAmt) {
		this.freeChargeAmt = freeChargeAmt;
	}

	public BigDecimal getFreeVolume() {
		return this.freeVolume;
	}

	public void setFreeVolume(BigDecimal freeVolume) {
		this.freeVolume = freeVolume;
	}

	public String getIsdn() {
		return this.isdn;
	}

	public void setIsdn(String isdn) {
		this.isdn = isdn;
	}

	public BigDecimal getMeasureUnit() {
		return this.measureUnit;
	}

	public void setMeasureUnit(BigDecimal measureUnit) {
		this.measureUnit = measureUnit;
	}

	public BigDecimal getMeasureUnitCdr() {
		return this.measureUnitCdr;
	}

	public void setMeasureUnitCdr(BigDecimal measureUnitCdr) {
		this.measureUnitCdr = measureUnitCdr;
	}

	public String getOtherNumber() {
		return this.otherNumber;
	}

	public void setOtherNumber(String otherNumber) {
		this.otherNumber = otherNumber;
	}

	public BigDecimal getRatingVolume() {
		return this.ratingVolume;
	}

	public void setRatingVolume(BigDecimal ratingVolume) {
		this.ratingVolume = ratingVolume;
	}

	public String getReCardSequence() {
		return this.reCardSequence;
	}

	public void setReCardSequence(String reCardSequence) {
		this.reCardSequence = reCardSequence;
	}

	public String getReCategory() {
		return this.reCategory;
	}

	public void setReCategory(String reCategory) {
		this.reCategory = reCategory;
	}

	public BigDecimal getReCurrencyId() {
		return this.reCurrencyId;
	}

	public void setReCurrencyId(BigDecimal reCurrencyId) {
		this.reCurrencyId = reCurrencyId;
	}

	public String getReExtRechargeType() {
		return this.reExtRechargeType;
	}

	public void setReExtRechargeType(String reExtRechargeType) {
		this.reExtRechargeType = reExtRechargeType;
	}

	public String getReExtTransId() {
		return this.reExtTransId;
	}

	public void setReExtTransId(String reExtTransId) {
		this.reExtTransId = reExtTransId;
	}

	public String getRePrimaryIdentity() {
		return this.rePrimaryIdentity;
	}

	public void setRePrimaryIdentity(String rePrimaryIdentity) {
		this.rePrimaryIdentity = rePrimaryIdentity;
	}

	public BigDecimal getReRechargeAmount() {
		return this.reRechargeAmount;
	}

	public void setReRechargeAmount(BigDecimal reRechargeAmount) {
		this.reRechargeAmount = reRechargeAmount;
	}

	public String getReRechargeChannelId() {
		return this.reRechargeChannelId;
	}

	public void setReRechargeChannelId(String reRechargeChannelId) {
		this.reRechargeChannelId = reRechargeChannelId;
	}

	public BigDecimal getReRechargePenalty() {
		return this.reRechargePenalty;
	}

	public void setReRechargePenalty(BigDecimal reRechargePenalty) {
		this.reRechargePenalty = reRechargePenalty;
	}

	public String getReRechargeReason() {
		return this.reRechargeReason;
	}

	public void setReRechargeReason(String reRechargeReason) {
		this.reRechargeReason = reRechargeReason;
	}

	public BigDecimal getReRechargeTax() {
		return this.reRechargeTax;
	}

	public void setReRechargeTax(BigDecimal reRechargeTax) {
		this.reRechargeTax = reRechargeTax;
	}

	public String getReRechargeType() {
		return this.reRechargeType;
	}

	public void setReRechargeType(String reRechargeType) {
		this.reRechargeType = reRechargeType;
	}

	public String getReResultCode() {
		return this.reResultCode;
	}

	public void setReResultCode(String reResultCode) {
		this.reResultCode = reResultCode;
	}

	public String getReReversalFlag() {
		return this.reReversalFlag;
	}

	public void setReReversalFlag(String reReversalFlag) {
		this.reReversalFlag = reReversalFlag;
	}

	public String getReReversalReason() {
		return this.reReversalReason;
	}

	public void setReReversalReason(String reReversalReason) {
		this.reReversalReason = reReversalReason;
	}

	public String getReReversalTime() {
		return this.reReversalTime;
	}

	public void setReReversalTime(String reReversalTime) {
		this.reReversalTime = reReversalTime;
	}

	public String getReTradeTime() {
		return this.reTradeTime;
	}

	public void setReTradeTime(String reTradeTime) {
		this.reTradeTime = reTradeTime;
	}

	public BigDecimal getReTransId() {
		return this.reTransId;
	}

	public void setReTransId(BigDecimal reTransId) {
		this.reTransId = reTransId;
	}

	public String getRefundIndicator() {
		return this.refundIndicator;
	}

	public void setRefundIndicator(String refundIndicator) {
		this.refundIndicator = refundIndicator;
	}

	public String getRoamFlag() {
		return this.roamFlag;
	}

	public void setRoamFlag(String roamFlag) {
		this.roamFlag = roamFlag;
	}

	public String getSeriveType() {
		return this.seriveType;
	}

	public void setSeriveType(String seriveType) {
		this.seriveType = seriveType;
	}

	public String getServiceCategory() {
		return this.serviceCategory;
	}

	public void setServiceCategory(String serviceCategory) {
		this.serviceCategory = serviceCategory;
	}

	public String getServiceTypeName() {
		return this.serviceTypeName;
	}

	public void setServiceTypeName(String serviceTypeName) {
		this.serviceTypeName = serviceTypeName;
	}

	public String getSpecialNumberIndicator() {
		return this.specialNumberIndicator;
	}

	public void setSpecialNumberIndicator(String specialNumberIndicator) {
		this.specialNumberIndicator = specialNumberIndicator;
	}

	public Calendar getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Calendar startTime) {
		this.startTime = startTime;
	}

	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public VcHilo getVcHilo() {
		return this.vcHilo;
	}

	public void setVcHilo(VcHilo vcHilo) {
		this.vcHilo = vcHilo;
	}

	public String getReAdditionalProperty() {
		return reAdditionalProperty;
	}

	public void setReAdditionalProperty(String reAdditionalProperty) {
		this.reAdditionalProperty = reAdditionalProperty;
	}

	public String getAjAdditionalProperty() {
		return ajAdditionalProperty;
	}

	public void setAjAdditionalProperty(String ajAdditionalProperty) {
		this.ajAdditionalProperty = ajAdditionalProperty;
	}

	public String getAdditionalProperty() {
		return additionalProperty;
	}

	public void setAdditionalProperty(String additionalProperty) {
		this.additionalProperty = additionalProperty;
	}

}