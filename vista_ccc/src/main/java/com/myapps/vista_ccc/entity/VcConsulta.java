package com.myapps.vista_ccc.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;


/**
 * The persistent class for the VC_CONSULTA database table.
 * 
 */
@Entity
@Table(name="VC_CONSULTA")
@NamedQuery(name="VcConsulta.findAll", query="SELECT v FROM VcConsulta v")
public class VcConsulta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="VC_CONSULTA_IDCONSULTA_GENERATOR", sequenceName="SEQ_VC_CONSULTA", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="VC_CONSULTA_IDCONSULTA_GENERATOR")
	@Column(name="ID_CONSULTA")
	private long idConsulta;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CREACION")
	private Calendar fechaCreacion;

	@Column(name="TIEMPO_CONEXION  ")
	private Long tiempoConexion;

	@Column(name="TIEMPO_RESPUESTA  ")
	private Long tiempoRespuesta;

	
	
	
	public Long getTiempoConexion() {
		return tiempoConexion;
	}

	public void setTiempoConexion(Long tiempoConexion) {
		this.tiempoConexion = tiempoConexion;
	}

	public Long getTiempoRespuesta() {
		return tiempoRespuesta;
	}

	public void setTiempoRespuesta(Long tiempoRespuesta) {
		this.tiempoRespuesta = tiempoRespuesta;
	}

	private String isdn;

	private String login;

	private String metodo;

	@Lob
	@Column(name="REQUEST")
	private String request;

	@Lob
	@Column(name="RESPONSE")
	private String response;

	private String servicio;

	//uni-directional many-to-one association to VcHilo
	@ManyToOne
	@JoinColumn(name="ID_HILO")
	private VcHilo vcHilo;

	//uni-directional many-to-one association to VcLogAuditoria
	@ManyToOne
	@JoinColumn(name="ID_LOG_AUDITORIA")
	private VcLogAuditoria vcLogAuditoria;

	public VcConsulta() {
	}

	public long getIdConsulta() {
		return this.idConsulta;
	}

	public void setIdConsulta(long idConsulta) {
		this.idConsulta = idConsulta;
	}

	public Calendar getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Calendar fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getIsdn() {
		return this.isdn;
	}

	public void setIsdn(String isdn) {
		this.isdn = isdn;
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getMetodo() {
		return this.metodo;
	}

	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}

	public String getRequest() {
		return this.request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getResponse() {
		return this.response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getServicio() {
		return this.servicio;
	}

	public void setServicio(String servicio) {
		this.servicio = servicio;
	}

	public VcHilo getVcHilo() {
		return this.vcHilo;
	}

	public void setVcHilo(VcHilo vcHilo) {
		this.vcHilo = vcHilo;
	}

	public VcLogAuditoria getVcLogAuditoria() {
		return this.vcLogAuditoria;
	}

	public void setVcLogAuditoria(VcLogAuditoria vcLogAuditoria) {
		this.vcLogAuditoria = vcLogAuditoria;
	}

}