package com.myapps.vista_ccc.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


/**
 * The persistent class for the VC_SERVIDOR database table.
 * 
 */
@Entity
@Table(name="VC_SERVIDOR")
@NamedQuery(name="VcServidor.findAll", query="SELECT v FROM VcServidor v")
public class VcServidor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_SERVIDOR")
	private long idServidor;

	@Column(name="ID_SERVIDOR_PADRE")
	private String idServidorPadre;

	@Column(name="KEY_COD_SERVIDOR")
	private String keyCodServidor;

	@Column(name="NOMBRE_SERVIDOR")
	private String nombreServidor;

	@Column(name="TIPO_CONEXION")
	private Integer tipoConexion;

	//bi-directional many-to-one association to VcConexionTelnet
	@OneToMany(mappedBy="vcServidor")
	private List<VcConexionTelnet> vcConexionTelnets;

	public VcServidor() {
	}

	public long getIdServidor() {
		return this.idServidor;
	}

	public void setIdServidor(long idServidor) {
		this.idServidor = idServidor;
	}

	public String getIdServidorPadre() {
		return this.idServidorPadre;
	}

	public void setIdServidorPadre(String idServidorPadre) {
		this.idServidorPadre = idServidorPadre;
	}

	public String getKeyCodServidor() {
		return this.keyCodServidor;
	}

	public void setKeyCodServidor(String keyCodServidor) {
		this.keyCodServidor = keyCodServidor;
	}

	public String getNombreServidor() {
		return this.nombreServidor;
	}

	public void setNombreServidor(String nombreServidor) {
		this.nombreServidor = nombreServidor;
	}

	public Integer getTipoConexion() {
		return this.tipoConexion;
	}

	public void setTipoConexion(Integer tipoConexion) {
		this.tipoConexion = tipoConexion;
	}

	public List<VcConexionTelnet> getVcConexionTelnets() {
		return this.vcConexionTelnets;
	}

	public void setVcConexionTelnets(List<VcConexionTelnet> vcConexionTelnets) {
		this.vcConexionTelnets = vcConexionTelnets;
	}

	public VcConexionTelnet addVcConexionTelnet(VcConexionTelnet vcConexionTelnet) {
		getVcConexionTelnets().add(vcConexionTelnet);
		vcConexionTelnet.setVcServidor(this);

		return vcConexionTelnet;
	}

	public VcConexionTelnet removeVcConexionTelnet(VcConexionTelnet vcConexionTelnet) {
		getVcConexionTelnets().remove(vcConexionTelnet);
		vcConexionTelnet.setVcServidor(null);

		return vcConexionTelnet;
	}

}