package com.myapps.vista_ccc.entity;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the VC_CONEXION_TELNET database table.
 * 
 */
@Entity
@Table(name="VC_CONEXION_TELNET")
@NamedQuery(name="VcConexionTelnet.findAll", query="SELECT v FROM VcConexionTelnet v")
public class VcConexionTelnet implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_CONEXION")
	private long idConexion;

	@Column(name="COD_SERVIDOR")
	private String codServidor;

	private String estado;

	private String ip;
	
	private String descripcion;

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getUsuarioSsh() {
		return usuarioSsh;
	}

	public void setUsuarioSsh(String usuarioSsh) {
		this.usuarioSsh = usuarioSsh;
	}

	public String getPasswordSsh() {
		return passwordSsh;
	}

	public void setPasswordSsh(String passwordSsh) {
		this.passwordSsh = passwordSsh;
	}

	@Column(name="NOMBRE_ELEMENTO_RED")
	private String nombreElementoRed;
	
	@Column(name="TIPO_CONEXION")
	private String tipoConexion;
	
	@Column(name="USUARIO_SSH")
	private String usuarioSsh;
	
	@Column(name="PASSWORD_SSH")
	private String passwordSsh;

	public String getTipoConexion() {
		return tipoConexion;
	}

	public void setTipoConexion(String tipoConexion) {
		this.tipoConexion = tipoConexion;
	}
	@Column(name="PASSWORD_COMANDO")
	private String passwordComando;

	private Integer puerto;

	@Column(name="USUARIO_COMANDO")
	private String usuarioComando;

	//bi-directional many-to-one association to VcServidor
	@ManyToOne
	@JoinColumn(name="ID_SERVIDOR")
	private VcServidor vcServidor;

	public VcConexionTelnet() {
	}

	public long getIdConexion() {
		return this.idConexion;
	}

	public void setIdConexion(long idConexion) {
		this.idConexion = idConexion;
	}

	public String getCodServidor() {
		return this.codServidor;
	}

	public void setCodServidor(String codServidor) {
		this.codServidor = codServidor;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getIp() {
		return this.ip.trim().trim();
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getNombreElementoRed() {
		return this.nombreElementoRed;
	}

	public void setNombreElementoRed(String nombreElementoRed) {
		this.nombreElementoRed = nombreElementoRed;
	}



	public String getPasswordComando() {
		return passwordComando;
	}

	public void setPasswordComando(String passwordComando) {
		this.passwordComando = passwordComando;
	}

	public String getUsuarioComando() {
		return usuarioComando;
	}

	public void setUsuarioComando(String usuarioComando) {
		this.usuarioComando = usuarioComando;
	}

	public Integer getPuerto() {
		return this.puerto;
	}

	public void setPuerto(Integer puerto) {
		this.puerto = puerto;
	}


	public VcServidor getVcServidor() {
		return this.vcServidor;
	}

	public void setVcServidor(VcServidor vcServidor) {
		this.vcServidor = vcServidor;
	}

}