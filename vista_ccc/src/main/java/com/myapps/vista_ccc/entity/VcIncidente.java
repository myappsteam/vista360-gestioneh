package com.myapps.vista_ccc.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;


/**
 * The persistent class for the VC_INCIDENTE database table.
 * 
 */
@Entity
@Table(name="VC_INCIDENTE")
@NamedQuery(name="VcIncidente.findAll", query="SELECT v FROM VcIncidente v")
public class VcIncidente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="VC_INCIDENTE_IDINCIDENTE_GENERATOR", sequenceName="SEQ_VC_INCIDENTE", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="VC_INCIDENTE_IDINCIDENTE_GENERATOR")
	@Column(name="ID_INCIDENTE")
	private long idIncidente;

	private boolean atencion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_ATENCION")
	private Calendar fechaAtencion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_INCIDENTE")
	private Calendar fechaIncidente;

	private String usuario;

	//bi-directional many-to-one association to VcLogAuditoria
	@ManyToOne
	@JoinColumn(name="ID_LOG_AUDITORIA")
	private VcLogAuditoria vcLogAuditoria;

	public VcIncidente() {
	}

	public long getIdIncidente() {
		return this.idIncidente;
	}

	public void setIdIncidente(long idIncidente) {
		this.idIncidente = idIncidente;
	}

	public boolean getAtencion() {
		return this.atencion;
	}

	public void setAtencion(boolean atencion) {
		this.atencion = atencion;
	}

	public Calendar getFechaAtencion() {
		return this.fechaAtencion;
	}

	public void setFechaAtencion(Calendar fechaAtencion) {
		this.fechaAtencion = fechaAtencion;
	}

	public Calendar getFechaIncidente() {
		return this.fechaIncidente;
	}

	public void setFechaIncidente(Calendar fechaIncidente) {
		this.fechaIncidente = fechaIncidente;
	}

	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public VcLogAuditoria getVcLogAuditoria() {
		return this.vcLogAuditoria;
	}

	public void setVcLogAuditoria(VcLogAuditoria vcLogAuditoria) {
		this.vcLogAuditoria = vcLogAuditoria;
	}

}