package com.myapps.vista_ccc.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;


/**
 * The persistent class for the VC_OFFERING database table.
 * 
 */
@Entity
@Table(name="VC_OFFERING")
@NamedQuery(name="VcOffering.findAll", query="SELECT v FROM VcOffering v")
public class VcOffering implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(unique=true, nullable=false, length=50)
	private String offeringid;

	@Column(precision=5, scale=4)
	private BigDecimal amount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private Calendar created;

	@Column(nullable=false, precision=1)
	private Integer enabled;

	@Column(nullable=false, precision=1)
	private Integer mandatory;

	@Column(length=100)
	private String name;

	@Column(length=50)
	private String offeringcode;

	@Column(length=50)
	private Long offeringparent;

	private BigDecimal paymentmode;

	@Column(name="\"TYPE\"", nullable=false, length=50)
	private String type;

	@Temporal(TemporalType.TIMESTAMP)
	private Calendar updated;

	public VcOffering() {
	}

	public String getOfferingid() {
		return this.offeringid;
	}

	public void setOfferingid(String offeringid) {
		this.offeringid = offeringid;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Calendar getCreated() {
		return this.created;
	}

	public void setCreated(Calendar created) {
		this.created = created;
	}

	public Integer getEnabled() {
		return this.enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public Integer getMandatory() {
		return this.mandatory;
	}

	public void setMandatory(Integer mandatory) {
		this.mandatory = mandatory;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOfferingcode() {
		return this.offeringcode;
	}

	public void setOfferingcode(String offeringcode) {
		this.offeringcode = offeringcode;
	}

	public Long getOfferingparent() {
		return this.offeringparent;
	}

	public void setOfferingparent(Long offeringparent) {
		this.offeringparent = offeringparent;
	}

	public BigDecimal getPaymentmode() {
		return this.paymentmode;
	}

	public void setPaymentmode(BigDecimal paymentmode) {
		this.paymentmode = paymentmode;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Calendar getUpdated() {
		return this.updated;
	}

	public void setUpdated(Calendar updated) {
		this.updated = updated;
	}

}