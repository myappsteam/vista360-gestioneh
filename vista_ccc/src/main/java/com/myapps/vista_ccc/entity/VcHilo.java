package com.myapps.vista_ccc.entity;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the VC_HILO database table.
 * 
 */
@Entity
@Table(name="VC_HILO")
@NamedQuery(name="VcHilo.findAll", query="SELECT v FROM VcHilo v")
public class VcHilo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_HILO")
	private long idHilo;

	private String activo;

	private Integer comando;

	private String description;

	@Column(name="FLOW_TYPE")
	private String flowType;

	@Column(name="NAME_CHECKBOX")
	private String nameCheckbox;

	@Column(name="NAME_HISTORY")
	private String nameHistory;

	@Column(name="NAME_TYPE")
	private String nameType;

	@Column(name="SERVICE_CATEGORY")
	private String serviceCategory;

	public VcHilo() {
	}

	public long getIdHilo() {
		return this.idHilo;
	}

	public void setIdHilo(long idHilo) {
		this.idHilo = idHilo;
	}

	public String getActivo() {
		return this.activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public Integer getComando() {
		return this.comando;
	}

	public void setComando(Integer comando) {
		this.comando = comando;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFlowType() {
		return this.flowType;
	}

	public void setFlowType(String flowType) {
		this.flowType = flowType;
	}

	public String getNameCheckbox() {
		return this.nameCheckbox;
	}

	public void setNameCheckbox(String nameCheckbox) {
		this.nameCheckbox = nameCheckbox;
	}

	public String getNameHistory() {
		return this.nameHistory;
	}

	public void setNameHistory(String nameHistory) {
		this.nameHistory = nameHistory;
	}

	public String getNameType() {
		return this.nameType;
	}

	public void setNameType(String nameType) {
		this.nameType = nameType;
	}

	public String getServiceCategory() {
		return this.serviceCategory;
	}

	public void setServiceCategory(String serviceCategory) {
		this.serviceCategory = serviceCategory;
	}

}