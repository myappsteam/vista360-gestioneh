package com.myapps.vista_ccc.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;


/**
 * The persistent class for the VC_CONSULTA_RED database table.
 * 
 */
@Entity
@Table(name="VC_CONSULTA_RED")
@NamedQuery(name="VcConsultaRed.findAll", query="SELECT v FROM VcConsultaRed v")
public class VcConsultaRed implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="VC_CONSULTA_RED_IDCONSULTA_GENERATOR", sequenceName="SEQ_VC_CONSULTA_RED", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="VC_CONSULTA_RED_IDCONSULTA_GENERATOR")
	@Column(name="ID_CONSULTA")
	private long idConsulta;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CREACION")
	private Calendar fechaCreacion;

	private String ip;

	private String isdn;

	private String login;

	private String tipo;

	@Lob
	private String trama;

	//uni-directional many-to-one association to VcLogAuditoria
	@ManyToOne
	@JoinColumn(name="ID_LOG_AUDITORIA")
	private VcLogAuditoria vcLogAuditoria;

	public VcConsultaRed() {
	}

	public long getIdConsulta() {
		return this.idConsulta;
	}

	public void setIdConsulta(long idConsulta) {
		this.idConsulta = idConsulta;
	}

	public Calendar getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Calendar fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getIsdn() {
		return this.isdn;
	}

	public void setIsdn(String isdn) {
		this.isdn = isdn;
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getTipo() {
		return this.tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getTrama() {
		return this.trama;
	}

	public void setTrama(String trama) {
		this.trama = trama;
	}

	public VcLogAuditoria getVcLogAuditoria() {
		return this.vcLogAuditoria;
	}

	public void setVcLogAuditoria(VcLogAuditoria vcLogAuditoria) {
		this.vcLogAuditoria = vcLogAuditoria;
	}

}