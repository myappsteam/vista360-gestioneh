
package com.myapps.vista_ccc.reset_pin.model.gson.sms;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Relationships implements Serializable
{

    @SerializedName("resource")
    @Expose
    private Resource resource;
    private  static final long serialVersionUID = 4777587994241132441L;

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

}
