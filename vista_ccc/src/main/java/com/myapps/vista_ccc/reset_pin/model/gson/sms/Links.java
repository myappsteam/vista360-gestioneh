package com.myapps.vista_ccc.reset_pin.model.gson.sms;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Links implements Serializable
{

    @SerializedName("related")
    @Expose
    private String related;
    private  static final long serialVersionUID = -709136252016566236L;

    public String getRelated() {
        return related;
    }

    public void setRelated(String related) {
        this.related = related;
    }

}
