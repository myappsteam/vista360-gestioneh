package com.myapps.vista_ccc.reset_pin.model.gson;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Attributes implements Serializable {

    @SerializedName("eh-code-parent")
    @Expose
    private List<String> ehCodeParent = new ArrayList<>();
    @SerializedName("max-activations")
    @Expose
    private List<String> maxActivations = new ArrayList<>();
    @SerializedName("msisdn")
    @Expose
    private List<String> msisdn = new ArrayList<>();
    @SerializedName("preactivation-recycle-period")
    @Expose
    private List<String> preactivationRecyclePeriod = new ArrayList<>();
    private static final long serialVersionUID = -8992980833207362972L;

    public List<String> getEhCodeParent() {
        return ehCodeParent;
    }

    public void setEhCodeParent(List<String> ehCodeParent) {
        this.ehCodeParent = ehCodeParent;
    }

    public List<String> getMaxActivations() {
        return maxActivations;
    }

    public void setMaxActivations(List<String> maxActivations) {
        this.maxActivations = maxActivations;
    }

    public List<String> getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(List<String> msisdn) {
        this.msisdn = msisdn;
    }

    public List<String> getPreactivationRecyclePeriod() {
        return preactivationRecyclePeriod;
    }

    public void setPreactivationRecyclePeriod(List<String> preactivationRecyclePeriod) {
        this.preactivationRecyclePeriod = preactivationRecyclePeriod;
    }

}