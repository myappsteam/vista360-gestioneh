
package com.myapps.vista_ccc.reset_pin.model.gson.sms;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ResponseSMS implements Serializable {

    @SerializedName("data")
    @Expose
    private Data data;
    private  static final long serialVersionUID = -9155285998683819328L;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
