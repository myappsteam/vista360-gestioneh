package com.myapps.vista_ccc.reset_pin.bean;

import com.myapps.user.bean.ControlerBitacora;
import com.myapps.user.ldap.DescriptorBitacora;
import com.myapps.vista_ccc.reset_pin.business.ResetPinBusiness;
import com.myapps.vista_ccc.reset_pin.commons.AuditoriaResetPin;
import com.myapps.vista_ccc.reset_pin.model.gson.QueryUser;
import com.myapps.vista_ccc.reset_pin.model.gson.TokenGson;
import com.myapps.vista_ccc.reset_pin.servicio.pin.GeneradorPin;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.SysMessage;
import com.myapps.vista_ccc.util.UtilUrl;
import com.myapps.vista_ccc.white_list.error.HttpStatusException;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Map;

@ManagedBean
@ViewScoped
public class ResetPinBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String ERROR = "ERROR";
    private static Logger log = Logger.getLogger(ResetPinBean.class);


    @Inject
    private ControlerBitacora controlerBitacora;
    @Inject
    private ResetPinBusiness resetPinBusiness;
    @Inject
    private AuditoriaResetPin auditoria;
    private long idAuditoriaHistorial;
    private String ip;
    private String token;
    private String estado;
    private String codigo;
    private String login;
    private QueryUser user;
    private Map<String, String> mapResponseWhiteList;

    @PostConstruct
    public void init() {
        obtenerUsuario();
        cargarIp();
        user = new QueryUser();
    }

    private void cargarIp() {
        try {
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                    .getRequest();
            ip = UtilUrl.getClientIp(request);
        } catch (Exception e) {
            log.error("Error al obtener ip: ", e);
        }
    }


    public void limpiarDatos() {
        this.token = null;
        this.user = new QueryUser();
        this.estado = null;
    }

    public void limpiarTodo() {
        this.codigo = null;
        this.token = null;
        this.estado = null;
        this.user = new QueryUser();
    }

    public void buscarCodigo() {
        if (codigo == null || codigo.isEmpty()) {
            mostrarMensajeGenerico("EL codigo es requerido!", "WARN");
        } else {

            limpiarDatos();
            idAuditoriaHistorial = auditoria.guardarAuditoria(Calendar.getInstance(), login, ip,
                    DescriptorBitacora.RESET_PIN.getFormulario(), 0, codigo, null);
            try {
                TokenGson tokenGson = resetPinBusiness.getToken(login, idAuditoriaHistorial);
                token = tokenGson.getAccessToken();
                validarUsuario(resetPinBusiness.getQueryUser(login, idAuditoriaHistorial, codigo, token));
            } catch (HttpStatusException e) {
                log.error("Problema con http client " + e.getMessage() + " " + e.getStatusResponse().getCode());
                SysMessage.error(e.getStatusResponse().getCode() + " " + e.getMessage(), null);

            } catch (Exception e) {
                log.error("No se pudo completar la consulta: " + e.getMessage(), e);
                SysMessage.error(Parametros.restMensajeErrorToken, null);
            }


            try {
                controlerBitacora.accion(DescriptorBitacora.RESET_PIN,
                        "Se realizó la busqueda en Reset Pin con el código: " + codigo);
            } catch (Exception e) {
                log.error("Error al guardar bitacora en el sistema: ", e);
                SysMessage.error("Error al guardar bitacora", null);
            }
        }
    }


    public void resetearPin() {
        RequestContext.getCurrentInstance().execute("PF('carDelet').hide()");
        if (token == null || token.isEmpty()) {
            mostrarMensajeGenerico("El token no es valido", "WARN");
        } else if (user == null) {
            mostrarMensajeGenerico("El usuario no es valido", "WARN");
        } else {
            String pin = GeneradorPin.generarPin();
            idAuditoriaHistorial = auditoria.guardarAuditoria(Calendar.getInstance(), login, ip,
                    DescriptorBitacora.RESET_PIN.getFormulario(), 0, codigo, null);
            try {
                if (resetPinBusiness.resetPassword(user.getId(), user.getAttributes().getMsisdn().get(0), login, idAuditoriaHistorial, pin, token)) {
                    mostrarMensajeGenerico(Parametros.restMensajeExitosoResetPin + imprimirPin(pin), "INFO");
                    if (!user.getEnabled()) {
                        boolean mensajeEnableUser = resetPinBusiness.updateUser(user.getId(), user.getAttributes().getMsisdn().get(0), login, idAuditoriaHistorial, token);
                        if (mensajeEnableUser) {
                            mostrarMensajeGenerico(Parametros.restMensajeEnableUser, "INFO");
                        } else {
                            mostrarMensajeGenerico("Error en UpdateUser", "WARN");
                        }
                    }
                    String sms = resetPinBusiness.sendSms(user.getAttributes().getMsisdn().get(0), login, idAuditoriaHistorial, pin);
                    //String sms = resetPinBusiness.sendSms("+59177800803", login, idAuditoriaHistorial, pin);
                    mostrarMensajeGenerico(sms, "INFO");
                } else {
                    mostrarMensajeGenerico(Parametros.restMensajeErrorResetPin, "WARN");
                }
            } catch (HttpStatusException e) {
                log.error("Problema con http client " + e.getMessage() + " " + e.getStatusResponse().getCode());
                SysMessage.error(e.getStatusResponse().getCode() + " " + e.getMessage(), null);

            } catch (Exception e) {
                log.error("No se pudo completar la consulta: " + e.getMessage(), e);
                SysMessage.error("Problema de aplicacion, no se pudo completar la consulta.", null);
            }

            try {
                controlerBitacora.accion(DescriptorBitacora.RESET_PIN,
                        "Se reseteo el Pin para la linea: " + user.getAttributes().getMsisdn().get(0) + " obtenido del código: " + codigo);
            } catch (Exception e) {
                log.error("Error al guardar bitacora en el sistema: ", e);
                SysMessage.error("Error al guardar bitacora", null);
            }
            limpiarTodo();
        }


    }

    public void validarUsuario(QueryUser user) {
        if (user != null && user.getId() != null) {
            this.user = user;
            this.estado = user.getEnabled() ? "Habilitado" : "Deshabilitado";
        } else if (user == null) {
            mostrarMensajeGenerico(Parametros.restMensajeEmptyQueryUSer, "WARN");
        } else {
            mostrarMensajeGenerico(Parametros.restMensajeErrorQueryUSer, ERROR);
        }

    }


    public void mostrarMensajeGenerico(String mensaje, String categoria) {
        switch (categoria) {
            case "INFO":
                SysMessage.info(mensaje, null);
                break;
            case "WARN":
                SysMessage.warn(mensaje, null);
                break;
            case ERROR:
                SysMessage.error(mensaje, null);
                break;
            default:
                SysMessage.error("Categoria no Definida " + mensaje, null);
                break;
        }
    }


    private void obtenerUsuario() {
        try {
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                    .getRequest();
            login = (String) request.getSession().getAttribute("TEMP$USER_NAME");
            log.info("Usuario: " + login + ", ingresando a vista de Reset PIN.");
        } catch (Exception e) {
            log.error("Error al obtener usuario: ", e);
        }
    }

    public String imprimirPin(String pin) {
        return Parametros.mostrarPin ? " el nuevo PIN es: " + pin : "";

    }


    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public QueryUser getUser() {
        return user;
    }

    public void setUser(QueryUser user) {
        this.user = user;
    }

    public Map<String, String> getMapResponseWhiteList() {
        return mapResponseWhiteList;
    }

    public void setMapResponseWhiteList(Map<String, String> mapResponseWhiteList) {
        this.mapResponseWhiteList = mapResponseWhiteList;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
