package com.myapps.vista_ccc.reset_pin.model.gson;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class QueryUser implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("createdTimestamp")
    @Expose
    private Long createdTimestamp;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("enabled")
    @Expose
    private Boolean enabled;
    @SerializedName("totp")
    @Expose
    private Boolean totp;
    @SerializedName("emailVerified")
    @Expose
    private Boolean emailVerified;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("attributes")
    @Expose
    private Attributes attributes;
    @SerializedName("disableableCredentialTypes")
    @Expose
    private  List<Object> disableableCredentialTypes = new ArrayList<>();
    @SerializedName("requiredActions")
    @Expose
    private  List<Object> requiredActions = new ArrayList<>();
    @SerializedName("notBefore")
    @Expose
    private Long notBefore;
    @SerializedName("access")
    @Expose
    private Access access;
    private static final long serialVersionUID = -3013373893145858302L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getCreatedTimestamp() {
        return createdTimestamp;
    }

    public void setCreatedTimestamp(Long createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getTotp() {
        return totp;
    }

    public void setTotp(Boolean totp) {
        this.totp = totp;
    }

    public Boolean getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(Boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public List<Object> getDisableableCredentialTypes() {
        return disableableCredentialTypes;
    }

    public void setDisableableCredentialTypes(List<Object> disableableCredentialTypes) {
        this.disableableCredentialTypes = disableableCredentialTypes;
    }

    public List<Object> getRequiredActions() {
        return requiredActions;
    }

    public void setRequiredActions(List<Object> requiredActions) {
        this.requiredActions = requiredActions;
    }

    public Long getNotBefore() {
        return notBefore;
    }

    public void setNotBefore(Long notBefore) {
        this.notBefore = notBefore;
    }

    public Access getAccess() {
        return access;
    }

    public void setAccess(Access access) {
        this.access = access;
    }

    @Override
    public String toString() {
        return "QueryUser{" +
                "id='" + id + '\'' +
                ", createdTimestamp=" + createdTimestamp +
                ", username='" + username + '\'' +
                ", enabled=" + enabled +
                ", totp=" + totp +
                ", emailVerified=" + emailVerified +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", attributes=" + attributes +
                ", disableableCredentialTypes=" + disableableCredentialTypes +
                ", requiredActions=" + requiredActions +
                ", notBefore=" + notBefore +
                ", access=" + access +
                '}';
    }
}