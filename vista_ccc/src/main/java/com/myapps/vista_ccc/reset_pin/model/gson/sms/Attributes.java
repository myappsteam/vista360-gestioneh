
package com.myapps.vista_ccc.reset_pin.model.gson.sms;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Attributes implements Serializable
{

    @SerializedName("completed-at")
    @Expose
    private String completedAt;
    @SerializedName("lifecycle-status")
    @Expose
    private String lifecycleStatus;
    @SerializedName("raw-message")
    @Expose
    private String rawMessage;
    @SerializedName("function")
    @Expose
    private String function;
    @SerializedName("requested-at")
    @Expose
    private String requestedAt;
    private  static final long serialVersionUID = -2723907082956911981L;

    public String getCompletedAt() {
        return completedAt;
    }

    public void setCompletedAt(String completedAt) {
        this.completedAt = completedAt;
    }

    public String getLifecycleStatus() {
        return lifecycleStatus;
    }

    public void setLifecycleStatus(String lifecycleStatus) {
        this.lifecycleStatus = lifecycleStatus;
    }

    public String getRawMessage() {
        return rawMessage;
    }

    public void setRawMessage(String rawMessage) {
        this.rawMessage = rawMessage;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getRequestedAt() {
        return requestedAt;
    }

    public void setRequestedAt(String requestedAt) {
        this.requestedAt = requestedAt;
    }

}
