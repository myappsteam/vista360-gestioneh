package com.myapps.vista_ccc.reset_pin.business;


import com.google.gson.Gson;
import com.myapps.vista_ccc.reset_pin.commons.AuditoriaResetPin;
import com.myapps.vista_ccc.reset_pin.error.RestExcepcion;
import com.myapps.vista_ccc.reset_pin.model.gson.QueryUser;
import com.myapps.vista_ccc.reset_pin.model.gson.TokenGson;
import com.myapps.vista_ccc.reset_pin.model.gson.sms.ResponseSMS;
import com.myapps.vista_ccc.reset_pin.servicio.ResetPinRestConsumer;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.white_list.error.HttpStatusAcceptException;
import com.myapps.vista_ccc.white_list.error.HttpStatusException;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.net.URISyntaxException;

@SuppressWarnings("unchecked")
@Named
public class ResetPinBusiness implements Serializable {
    private static final long serialVersionUID = 1L;
    private static Logger logger = Logger.getLogger(ResetPinBusiness.class);


    @Inject
    private AuditoriaResetPin auditoria;

    @Inject
    private ResetPinRestConsumer resetPinRestConsumer;


    public TokenGson getToken(String usuario, long idAuditoriaHistorial)
            throws HttpStatusException, IOException, RestExcepcion, com.myapps.vista_ccc.reset_pin.error.HttpStatusException {
        TokenGson tokenGson;
        try {
            Gson gson = new Gson();
            String token = resetPinRestConsumer.consumerToken(Parametros.restUrlToken, usuario, idAuditoriaHistorial);
            tokenGson = gson.fromJson(token, TokenGson.class);
        } catch (HttpStatusAcceptException e) {
            logger.info("Respuesta de servicio getToken diferente de OK...");
            throw new RestExcepcion(e.getJson());
        }
        return tokenGson;
    }


    public QueryUser getQueryUser(String usuario, long idAuditoriaHistorial, String codigo, String token)
            throws HttpStatusException, IOException, RestExcepcion, URISyntaxException, com.myapps.vista_ccc.reset_pin.error.HttpStatusException {
        QueryUser queryUser = null;
        try {
            Gson gson = new Gson();
            String user = resetPinRestConsumer.consumerQueryUser(Parametros.restUrlQueryUSer, usuario, idAuditoriaHistorial, codigo, token);
            QueryUser[] queryUserList = gson.fromJson(user, QueryUser[].class);
            if (queryUserList != null && queryUserList.length > 0) {
                for (QueryUser query : queryUserList) {
                    if (codigo.equals(query.getUsername())) {
                        queryUser = query;
                        break;
                    }
                }

            }
        } catch (HttpStatusAcceptException e) {
            logger.info("Respuesta de servicio Query User diferente de OK...");
            throw new RestExcepcion(e.getJson());
        }
        return queryUser;
    }

    public boolean resetPassword(String userID, String msisdn, String usuario, long idAuditoriaHistorial, String pin, String token)
            throws HttpStatusException, IOException, RestExcepcion, com.myapps.vista_ccc.reset_pin.error.HttpStatusException {
        boolean queryUser = false;
        try {
            queryUser = resetPinRestConsumer.consumerResetPassword(userID, msisdn, Parametros.restUrlResetPin, usuario, idAuditoriaHistorial, pin
                    , token);
        } catch (HttpStatusAcceptException e) {
            logger.info("Respuesta de servicio ResetPassword diferente de OK...");
            throw new RestExcepcion(e.getJson());
        }
        return queryUser;
    }


    public boolean updateUser(String userID, String msisdn, String usuario, long idAuditoriaHistorial, String token)
            throws HttpStatusException, IOException, RestExcepcion, com.myapps.vista_ccc.reset_pin.error.HttpStatusException {
        boolean update = false;
        try {
            update = resetPinRestConsumer.consumerEnableUser(userID, msisdn, Parametros.restUrlEnableUser, usuario, idAuditoriaHistorial, token);
        } catch (HttpStatusAcceptException e) {
            logger.info("Respuesta de servicio UpdateUser diferente de OK...");
            throw new RestExcepcion(e.getJson());
        }
        return update;
    }


    public String sendSms(String msisdn, String usuario, long idAuditoriaHistorial, String pin)
            throws HttpStatusException, IOException, RestExcepcion, com.myapps.vista_ccc.reset_pin.error.HttpStatusException {
        String sms = "";
        try {
            Gson gson = new Gson();
            sms = resetPinRestConsumer.consumerSendSMS(msisdn, Parametros.restUrlSMS, usuario, idAuditoriaHistorial, pin);
            ResponseSMS responseSMS = gson.fromJson(sms, ResponseSMS.class);
            if (responseSMS != null) {
                sms = "Sms Enviado Satisfactoriamente!";
            } else {
                sms = "Error al enviar SMS";
            }
        } catch (HttpStatusAcceptException e) {
            logger.info("Respuesta de servicio SendSMS diferente de OK...");
            throw new RestExcepcion(e.getJson());
        }
        logger.info("RESPONSE: " + sms);
        return sms;
    }

}
