package com.myapps.vista_ccc.reset_pin.error;

import com.myapps.vista_ccc.white_list.commons.StatusResponse;

public class HttpStatusException extends Exception {

	private static final long serialVersionUID = 1L;
	private StatusResponse statusResponse;

	public HttpStatusException(String message, StatusResponse statusResponse) {
		super(message);
		this.statusResponse = statusResponse;
	}

	public StatusResponse getStatusResponse() {
		return statusResponse;
	}

	public void setStatusResponse(StatusResponse statusResponse) {
		this.statusResponse = statusResponse;
	}

}
