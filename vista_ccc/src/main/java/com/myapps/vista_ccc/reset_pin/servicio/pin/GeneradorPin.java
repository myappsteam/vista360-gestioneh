package com.myapps.vista_ccc.reset_pin.servicio.pin;


import com.myapps.vista_ccc.util.Parametros;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.UUID;

public class GeneradorPin {

    private static final Logger log = LogManager.getLogger(GeneradorPin.class);


    public static String generarInt(int longuitud) {
        boolean valorBoleano = false;
        String valor = "";
        try {
            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
            while (!valorBoleano) {
                int myInt = sr.nextInt(multiplicar(9, longuitud)) + multiplicar(1, longuitud);
                valor = String.valueOf(myInt);
                valorBoleano = validarPin(valor);
            }
        } catch (NoSuchAlgorithmException e) {
            log.error("ERROR AL GENERAR PIN NUMERICO: " + e);
        }

        return valor;
    }

    public static int multiplicar(int numero, int longuitud) {
        for (int i = 1; i < longuitud; i++) {
            numero = numero * 10;
        }
        return numero;
    }

    public static String generarString(int longuitud) {
        boolean valorBoleano = false;
        String resultado = "";
        while (!valorBoleano) {
            String valor = UUID.randomUUID().toString();
            resultado = valor.substring(0, longuitud);
            valorBoleano = validarPin(resultado);
        }


        return resultado;
    }

    public static boolean validarPin(String valor) {
        boolean resultado;
        if (Cola.contains(valor)) {
            resultado = false;
        } else {
            if (Cola.size() > Parametros.pinCola) {
                while (Cola.size() != Parametros.pinCola) {
                    log.debug("Cola size:" + Cola.size());
                    log.debug("Valor Removido: " + Cola.poll());
                }
            }
            Cola.put(valor);
            resultado = true;
        }
        return resultado;
    }

    public static String generarPin() {
        String valor;
        switch (Parametros.pinTipo) {
            case "NUMERICO":
                valor = generarInt(Parametros.pinLonguitud);
                break;
            case "ALFANUMERICO":
                valor = generarString(Parametros.pinLonguitud);
                break;
            default:
                valor = "Configuracion incorrecta";
                log.error("ERROR: Configuracion incorrecta Generador PINREST");
                break;
        }
        return valor;
    }

}
