package com.myapps.vista_ccc.reset_pin.error;



public class RestExcepcion extends Exception {

    private static final long serialVersionUID = 1L;
    private final transient String responseInfo;

    public RestExcepcion(String responseInfo) {
        this.responseInfo = responseInfo;
    }

    public String getResponseInfo() {
        return responseInfo;
    }

}
