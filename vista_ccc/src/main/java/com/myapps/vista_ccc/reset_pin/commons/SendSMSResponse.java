package com.myapps.vista_ccc.reset_pin.commons;

public enum SendSMSResponse {
    BAD_REQUEST(400, "When request encoding is corrupted. When request has malformed header, query parameters or content. When using not\n" +
            "allowed URI scheme."),
    FORBIDDEN(403, "When authorization failed. When creating new entity with client generated identifier. When creating new resource is not allowed\n" +
            "for whatever reason."),
    HTTP_UNAUTHORIZED(401, "HTTP 401 Unauthorized\". Invalid Token"),
    HTTP_NOT_FOUND(404, "When request is missing required query parameter."),
    CONFLICT(409, "When sending invalid type for resource."),
    UNPROCESSABLEENTITY(422, "When the request cannot be processed due to any semantic reason."),
    HTTP_INTERNAL_SERVER_ERROR(500, "When error occurs during processing the request."),
    ACCEPTED(202, "ACCEPTED ");

    private int code;
    private String descripcion;

    SendSMSResponse(int code, String descripcion) {
        this.code = code;
        this.descripcion = descripcion;
    }

    public int getCode() {
        return code;
    }

    public String getDescripcion() {
        return descripcion;
    }

}
