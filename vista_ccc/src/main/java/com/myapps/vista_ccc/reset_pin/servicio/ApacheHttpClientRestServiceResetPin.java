package com.myapps.vista_ccc.reset_pin.servicio;


import com.myapps.vista_ccc.reset_pin.commons.QueryUserResponse;
import com.myapps.vista_ccc.reset_pin.commons.SendSMSResponse;
import com.myapps.vista_ccc.reset_pin.error.HttpStatusException;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.white_list.commons.StatusResponse;
import com.myapps.vista_ccc.white_list.error.HttpStatusAcceptException;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Named
public class ApacheHttpClientRestServiceResetPin implements Serializable {

    private static Logger logger = Logger.getLogger(ApacheHttpClientRestServiceResetPin.class);

    public String validarResponseSMS(HttpResponse response)
            throws HttpStatusAcceptException, IOException, HttpStatusException {
        if (response != null) {
            logger.info("Http Status Code: " + response.getStatusLine().getStatusCode());
            if (response.getStatusLine().getStatusCode() != SendSMSResponse.ACCEPTED.getCode()) {
                if (response.getStatusLine().getStatusCode() == SendSMSResponse.HTTP_NOT_FOUND.getCode()
                        || response.getStatusLine().getStatusCode() == SendSMSResponse.CONFLICT.getCode()
                        || response.getStatusLine().getStatusCode() == SendSMSResponse.HTTP_INTERNAL_SERVER_ERROR.getCode()) {
                    throw new HttpStatusAcceptException(EntityUtils.toString(response.getEntity()),
                            response.getStatusLine().getStatusCode());
                } else if (response.getStatusLine().getStatusCode() == SendSMSResponse.HTTP_UNAUTHORIZED.getCode()) {
                    throw new HttpStatusException(SendSMSResponse.HTTP_UNAUTHORIZED.getDescripcion(),
                            StatusResponse.HTTP_UNAUTHORIZED);
                } else if (response.getStatusLine().getStatusCode() == SendSMSResponse.FORBIDDEN.getCode()) {
                    throw new HttpStatusException(SendSMSResponse.FORBIDDEN.getDescripcion(),
                            StatusResponse.HTTP_FORBIDDEN);
                } else {
                    throw new HttpStatusException(response.getStatusLine().getReasonPhrase(),
                            StatusResponse.HTTP_ERROR);
                }
            }
        } else {
            logger.error("No se pudo establecer una conexion con el servicio Rest Servicio resualto a Null.");
            throw new HttpStatusException(
                    StatusResponse.HTTP_ERROR.getDescripcion() + "Causa: Servicio resulto a nulo.",
                    StatusResponse.HTTP_ERROR);
        }
        return EntityUtils.toString(response.getEntity());
    }

    public String validarResponse(HttpResponse response)
            throws HttpStatusAcceptException, IOException, HttpStatusException {
        if (response != null) {
            logger.info("Http Status Code: " + response.getStatusLine().getStatusCode());
            if (response.getStatusLine().getStatusCode() != QueryUserResponse.HTTP_OK.getCode()) {
                if (response.getStatusLine().getStatusCode() == QueryUserResponse.HTTP_NOT_FOUND.getCode()
                        || response.getStatusLine().getStatusCode() == QueryUserResponse.CONFLICT.getCode()
                        || response.getStatusLine().getStatusCode() == QueryUserResponse.HTTP_INTERNAL_SERVER_ERROR.getCode()) {
                    throw new HttpStatusAcceptException(EntityUtils.toString(response.getEntity()),
                            response.getStatusLine().getStatusCode());
                } else if (response.getStatusLine().getStatusCode() == QueryUserResponse.HTTP_UNAUTHORIZED.getCode()) {
                    throw new HttpStatusException(StatusResponse.HTTP_UNAUTHORIZED.getDescripcion(),
                            StatusResponse.HTTP_UNAUTHORIZED);
                } else if (response.getStatusLine().getStatusCode() == QueryUserResponse.FORBIDDEN.getCode()) {
                    throw new HttpStatusException(StatusResponse.HTTP_FORBIDDEN.getDescripcion(),
                            StatusResponse.HTTP_FORBIDDEN);
                } else {
                    throw new HttpStatusException(response.getStatusLine().getReasonPhrase(),
                            StatusResponse.HTTP_ERROR);
                }
            }
        } else {
            logger.error("No se pudo establecer una conexion con el servicio Rest Servicio resualto a Null.");
            throw new HttpStatusException(
                    StatusResponse.HTTP_ERROR.getDescripcion() + "Causa: Servicio resulto a nulo.",
                    StatusResponse.HTTP_ERROR);
        }
        return EntityUtils.toString(response.getEntity());
    }


    public boolean validarResponsePut(HttpResponse response)
            throws HttpStatusAcceptException, IOException, HttpStatusException {
        boolean result = false;
        if (response != null) {
            logger.info("Http Status Code: " + response.getStatusLine().getStatusCode());
            if (response.getStatusLine().getStatusCode() != StatusResponse.NO_CONTENT.getCode()) {
                if (response.getStatusLine().getStatusCode() == StatusResponse.HTTP_NOT_FOUND.getCode()
                        || response.getStatusLine().getStatusCode() == StatusResponse.HTTP_CONFLICT.getCode()
                        || response.getStatusLine().getStatusCode() == StatusResponse.HTTP_ERROR.getCode()) {
                    throw new HttpStatusAcceptException(EntityUtils.toString(response.getEntity()),
                            response.getStatusLine().getStatusCode());
                } else if (response.getStatusLine().getStatusCode() == StatusResponse.HTTP_UNAUTHORIZED.getCode()) {
                    throw new HttpStatusException(StatusResponse.HTTP_UNAUTHORIZED.getDescripcion(),
                            StatusResponse.HTTP_UNAUTHORIZED);
                } else if (response.getStatusLine().getStatusCode() == StatusResponse.HTTP_FORBIDDEN.getCode()) {
                    throw new HttpStatusException(StatusResponse.HTTP_FORBIDDEN.getDescripcion(),
                            StatusResponse.HTTP_FORBIDDEN);
                } else {
                    throw new HttpStatusException(response.getStatusLine().getReasonPhrase(),
                            StatusResponse.HTTP_ERROR);
                }
            } else {
                result = true;
            }
        } else {
            logger.error("No se pudo establecer una conexion con el servicio Rest Servicio resualto a Null.");
            throw new HttpStatusException(
                    StatusResponse.HTTP_ERROR.getDescripcion() + "Causa: Servicio resulto a nulo.",
                    StatusResponse.HTTP_ERROR);
        }
        return result;
    }


    public synchronized String getToken(String apiRestAuthURL) throws IOException, HttpStatusAcceptException, HttpStatusException {
        String result;
        List<NameValuePair> parametros = new ArrayList<>();
        LinkedHashMap<String, String> url = UtilMap.mapear(Parametros.restparametrosToken);
        for (Map.Entry<String, String> entry : url.entrySet()) {
            parametros.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
        }

        HttpPost post = new HttpPost(apiRestAuthURL);
        post.addHeader("Content-Type", "application/x-www-form-urlencoded");
        post.setEntity(new UrlEncodedFormEntity(parametros, StandardCharsets.UTF_8));
        CloseableHttpResponse response;
        CloseableHttpClient httpClient = null;
        try {
            httpClient = HttpClientBuilder.create().setDefaultRequestConfig(generarConfiguracionRequest(Parametros.restTimeoutToken)).build();
            response = httpClient.execute(post);
            result = validarResponse(response);
        } finally {
            httpClient.close();
        }
        return result;
    }


    public synchronized String getQueryUser(String apiRestAuthURL, String codigo, String tokken) throws IOException, URISyntaxException, HttpStatusException, HttpStatusAcceptException {
        String result;
        List<NameValuePair> parametros = new ArrayList<>();
        parametros.add(new BasicNameValuePair("username", codigo));
        URI uri = new URIBuilder(apiRestAuthURL).addParameters(parametros).build();
        HttpRequestBase httpGet = new HttpGet(uri);
        httpGet.addHeader("Content-Type", "application/json");
        httpGet.addHeader("Authorization", "Bearer " + tokken);
        CloseableHttpResponse response;
        CloseableHttpClient httpClient = null;
        try {
            httpClient = HttpClientBuilder.create().setDefaultRequestConfig(generarConfiguracionRequest(Parametros.restTimeoutQueryUSer)).build();
            response = httpClient.execute(httpGet);
            result = validarResponse(response);
        } finally {
            httpClient.close();
        }
        return result;
    }

    public synchronized boolean resetpassword(String apiRestAuthURL, String tokken, String pin, String body) throws IOException, HttpStatusException, HttpStatusAcceptException {
        boolean result;
        HttpPut httpPut = new HttpPut(apiRestAuthURL);
        httpPut.addHeader("Content-Type", "application/json");
        httpPut.addHeader("Authorization", "Bearer " + tokken);
        httpPut.setEntity(new StringEntity(body));
        CloseableHttpResponse response;
        CloseableHttpClient httpClient = null;
        try {
            httpClient = HttpClientBuilder.create().setDefaultRequestConfig(generarConfiguracionRequest(Parametros.restTimeoutResetPin)).build();
            response = httpClient.execute(httpPut);
            result = validarResponsePut(response);
        } finally {
            httpClient.close();
        }
        return result;
    }

    public synchronized boolean updateUser(String apiRestAuthURL, String tokken, String body) throws IOException, HttpStatusException, HttpStatusAcceptException {
        boolean result;
        HttpPut httpPut = new HttpPut(apiRestAuthURL);
        httpPut.addHeader("Content-Type", "application/json");
        httpPut.addHeader("Authorization", "Bearer " + tokken);
        httpPut.setEntity(new StringEntity(body));
        CloseableHttpResponse response;
        CloseableHttpClient httpClient = null;
        try {
            httpClient = HttpClientBuilder.create().setDefaultRequestConfig(generarConfiguracionRequest(Parametros.restTimeoutEnableUser)).build();
            response = httpClient.execute(httpPut);
            result = validarResponsePut(response);
        } finally {
            httpClient.close();
        }
        return result;
    }


    public synchronized String sendSMS(String apiRestAuthURL, String msisdn, String pin, String body) throws IOException, HttpStatusException, HttpStatusAcceptException {
        String result = "";
        HttpPost httpPost = new HttpPost(apiRestAuthURL);
        httpPost.addHeader("Content-Type", "application/json");
        httpPost.setEntity(new StringEntity(body));
        CloseableHttpResponse response;
        CloseableHttpClient httpClient = null;
        try {
            httpClient = HttpClientBuilder.create().setDefaultRequestConfig(generarConfiguracionRequest(Parametros.restTimeoutSMS)).build();
            response = httpClient.execute(httpPost);
            result = validarResponseSMS(response);
        } finally {
            httpClient.close();
        }
        logger.info("SMS REQUEST: " + body + " SMS RESPONSE: " + result);
        return result;
    }


    public RequestConfig generarConfiguracionRequest(int timeout) {
        return RequestConfig.custom().setConnectTimeout(timeout * 1000).setConnectionRequestTimeout(timeout * 1000)
                .setSocketTimeout(timeout * 1000).build();
    }
}
