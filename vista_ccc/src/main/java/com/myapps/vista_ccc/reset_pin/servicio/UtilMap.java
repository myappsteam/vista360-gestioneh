package com.myapps.vista_ccc.reset_pin.servicio;

import com.myapps.vista_ccc.gestion_eh.servicio.model.Categoria;
import com.myapps.vista_ccc.gestion_eh.servicio.model.Permisos;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class UtilMap {
    private static Logger logger = Logger.getLogger(UtilMap.class);


    public static LinkedHashMap<String, String> mapear(String cadena) {
        LinkedHashMap<String, String> valor = new LinkedHashMap<>();
        if (cadena != null) {
            String[] arrayOne = cadena.split("\\|");
            for (String string : arrayOne) {
                String[] arrayTwo = string.split(":::");
                if (arrayTwo.length == 2) {
                    valor.put(arrayTwo[0], arrayTwo[1]);
                }

            }
        }
        return valor;
    }


    public static String replaceParametros(String url, Map<String, String> mapa) {
        String valor = "";
        if (url != null) {
            valor = url;
            for (Map.Entry<String, String> entry : mapa.entrySet()) {
                valor = valor.replace(entry.getKey(), entry.getValue());
            }
        } else {
            logger.error("[replaceParametros] El valor es nullo");
        }
        return valor;
    }

    public static List<Categoria> armarComboCategoria(String cadena) {
        List<Categoria> valor = new ArrayList<>();
        if (cadena != null) {
            String[] arrayOne = cadena.split("\\|");
            for (String string : arrayOne) {
                String[] arrayTwo = string.split(";");
                if (arrayTwo.length == 2) {
                    valor.add(new Categoria(arrayTwo[0], Long.parseLong(arrayTwo[1])));
                }

            }
        }
        return valor;
    }

    public static List<Permisos> armarPermisos(String cadena) {
        List<Permisos> valor = new ArrayList<>();
        try {
            if (cadena != null) {
                String[] arrayOne = cadena.split("\\|");
                for (String string : arrayOne) {
                    String[] arrayTwo = string.split(";");
                    if (arrayTwo.length == 2) {
                        valor.add(new Permisos(arrayTwo[1], arrayTwo[0], false, ""));
                    }

                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        return valor;
    }
}
