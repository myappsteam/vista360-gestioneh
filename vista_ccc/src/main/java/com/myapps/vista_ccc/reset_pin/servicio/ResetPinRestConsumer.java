package com.myapps.vista_ccc.reset_pin.servicio;

import com.myapps.vista_ccc.reset_pin.commons.AuditoriaResetPin;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.white_list.commons.StatusResponse;
import com.myapps.vista_ccc.white_list.error.HttpStatusAcceptException;
import com.myapps.vista_ccc.white_list.error.HttpStatusException;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.net.ConnectException;
import java.net.URISyntaxException;


@Named
public class ResetPinRestConsumer implements Serializable {

    @Inject
    private ApacheHttpClientRestServiceResetPin clientRest;
    @Inject
    private AuditoriaResetPin auditoria;

    private static Logger logger = Logger.getLogger(ResetPinRestConsumer.class);

    public synchronized String consumerToken(String urlService, String usuario,
                                             long idAuditoriaHistorial) throws HttpStatusException, IOException, HttpStatusAcceptException, com.myapps.vista_ccc.reset_pin.error.HttpStatusException {
        long ini = System.currentTimeMillis();
        String output = "";
        String urlConsumer = UtilMap.replaceParametros(urlService, UtilMap.mapear(Parametros.restUrlParametrosToken));
        logger.info("Url service: " + urlConsumer);
        try {

            output = clientRest.getToken(urlConsumer);
            logger.info("Response GetToken Json: " + output);
            long fin = System.currentTimeMillis();
            logger.info("[Serivicio RESTful: GetToken ] Tiempo de respuesta: " + (fin - ini)
                    + " milisegundos");
            if (Parametros.saveRequestResponse) {
                try {
                    auditoria.saveXml(null, usuario, urlConsumer, "GetToken", Parametros.restparametrosToken, output,
                            idAuditoriaHistorial, (fin - ini), (fin - ini));
                } catch (Exception e) {
                    logger.warn("No se logro registrar los request y response del servicio RESTful GetToken: ", e);
                }
            }
        } catch (ConnectException e) {
            logger.error("No se pudo establecer una conexion con el servicio Rest GetToken " + e.getMessage(), e);

            throw new HttpStatusException(StatusResponse.HTTP_NOT_FOUND.getDescripcion(),
                    StatusResponse.HTTP_NOT_FOUND);
        } catch (ConnectTimeoutException e) {
            logger.error("Tiempo de espera agotado para respuesta de servicio GetToken " + urlConsumer + " "
                    + e.getMessage(), e);

            throw new HttpStatusException(StatusResponse.HTTP_ERROR.getDescripcion() + ", Causa: " + e.getMessage(),
                    StatusResponse.HTTP_ERROR);
        }
        return output;
    }


    public synchronized String consumerQueryUser(String urlService, String usuario,
                                                 long idAuditoriaHistorial, String codigo, String token) throws HttpStatusException, IOException, HttpStatusAcceptException, URISyntaxException, com.myapps.vista_ccc.reset_pin.error.HttpStatusException {
        long ini = System.currentTimeMillis();
        String output = "";
        String urlConsumer = UtilMap.replaceParametros(urlService, UtilMap.mapear(Parametros.restUrlParametrosQueryUSer));
        urlConsumer = urlConsumer.replace("{username}", codigo);
        logger.info("Url service: " + urlConsumer);
        try {

            output = clientRest.getQueryUser(urlConsumer, codigo, token);
            logger.info("Response QueryUser Json: " + output);
            long fin = System.currentTimeMillis();
            logger.info("[Serivicio RESTful: QueryUser ] Tiempo de respuesta: " + (fin - ini)
                    + " milisegundos");
            if (Parametros.saveRequestResponse) {
                try {
                    auditoria.saveXml(null, usuario, urlConsumer, "QueryUser", urlConsumer, output,
                            idAuditoriaHistorial, (fin - ini), (fin - ini));
                } catch (Exception e) {
                    logger.warn("No se logro registrar los request y response del servicio RESTful QueryUser: ", e);
                }
            }

        } catch (ConnectException e) {
            logger.error("No se pudo establecer una conexion con el servicio Rest QueryUser " + e.getMessage(), e);

            throw new HttpStatusException(StatusResponse.HTTP_NOT_FOUND.getDescripcion(),
                    StatusResponse.HTTP_NOT_FOUND);
        } catch (ConnectTimeoutException e) {
            logger.error("Tiempo de espera agotado para respuesta de servicio QueryUser " + urlConsumer + " "
                    + e.getMessage(), e);

            throw new HttpStatusException(StatusResponse.HTTP_ERROR.getDescripcion() + ", Causa: " + e.getMessage(),
                    StatusResponse.HTTP_ERROR);
        }
        return output;
    }


    public synchronized boolean consumerResetPassword(String userID, String isdn, String urlService, String usuario,
                                                      long idAuditoriaHistorial, String pin, String token) throws HttpStatusException, IOException, HttpStatusAcceptException, com.myapps.vista_ccc.reset_pin.error.HttpStatusException {
        long ini = System.currentTimeMillis();
        boolean output;
        String urlConsumer = UtilMap.replaceParametros(urlService, UtilMap.mapear(Parametros.restUrlParametrosResetPin));
        urlConsumer = urlConsumer.replace("{user-id}", userID);
        logger.debug("Url service: " + urlConsumer);
        try {
            String body = Parametros.restBodyResetPin.replace("PASSWORD", pin);
            output = clientRest.resetpassword(urlConsumer, token, pin, body);
            logger.debug("Request ResetPassword Json: " + body);
            logger.debug("Response ResetPassword Json: " + output);
            long fin = System.currentTimeMillis();
            logger.info("[Serivicio RESTful: ResetPassword ] Tiempo de respuesta: " + (fin - ini)
                    + " milisegundos");
            if (Parametros.saveRequestResponse) {
                try {
                    auditoria.saveXml(isdn, usuario, urlConsumer, "ResetPassword", body, String.valueOf(output),
                            idAuditoriaHistorial, (fin - ini), (fin - ini));
                } catch (Exception e) {
                    logger.warn("No se logro registrar los request y response del servicio RESTful ResetPassword: ", e);
                }
            }

        } catch (ConnectException e) {
            logger.error("No se pudo establecer una conexion con el servicio Rest ResetPassword " + e.getMessage(), e);

            throw new HttpStatusException(StatusResponse.HTTP_NOT_FOUND.getDescripcion(),
                    StatusResponse.HTTP_NOT_FOUND);
        } catch (ConnectTimeoutException e) {
            logger.error("Tiempo de espera agotado para respuesta de servicio ResetPassword " + urlConsumer + " "
                    + e.getMessage(), e);

            throw new HttpStatusException(StatusResponse.HTTP_ERROR.getDescripcion() + ", Causa: " + e.getMessage(),
                    StatusResponse.HTTP_ERROR);
        }
        return output;
    }

    public synchronized boolean consumerEnableUser(String userID, String isdn, String urlService, String usuario,
                                                   long idAuditoriaHistorial, String token) throws HttpStatusException, IOException, HttpStatusAcceptException, com.myapps.vista_ccc.reset_pin.error.HttpStatusException {
        long ini = System.currentTimeMillis();
        boolean output;
        String urlConsumer = UtilMap.replaceParametros(urlService, UtilMap.mapear(Parametros.restUrlParametrosEnableUser));
        urlConsumer = urlConsumer.replace("{user-id}", userID);
        logger.debug("Url service: " + urlConsumer);
        try {
            String body = Parametros.restUrlBodyEnableUser;
            output = clientRest.updateUser(urlConsumer, token, body);
            logger.debug("Request UpdateUser Json: " + body);
            logger.debug("Response UpdateUser Json: " + output);
            long fin = System.currentTimeMillis();
            logger.info("[Serivicio RESTful: UpdateUser ] Tiempo de respuesta: " + (fin - ini)
                    + " milisegundos");
            if (Parametros.saveRequestResponse) {
                try {
                    auditoria.saveXml(isdn, usuario, urlConsumer, "UpdateUser", body + " ISDN: " + isdn + " UserId: " + userID, String.valueOf(output),
                            idAuditoriaHistorial, (fin - ini), (fin - ini));
                } catch (Exception e) {
                    logger.warn("No se logro registrar los request y response del servicio RESTful UpdateUser: ", e);
                }
            }

        } catch (ConnectException e) {
            logger.error("No se pudo establecer una conexion con el servicio Rest UpdateUser " + e.getMessage(), e);

            throw new HttpStatusException(StatusResponse.HTTP_NOT_FOUND.getDescripcion(),
                    StatusResponse.HTTP_NOT_FOUND);
        } catch (ConnectTimeoutException e) {
            logger.error("Tiempo de espera agotado para respuesta de servicio UpdateUser " + urlConsumer + " "
                    + e.getMessage(), e);

            throw new HttpStatusException(StatusResponse.HTTP_ERROR.getDescripcion() + ", Causa: " + e.getMessage(),
                    StatusResponse.HTTP_ERROR);
        }
        return output;
    }


    public synchronized String consumerSendSMS(String isdn, String urlService, String usuario,
                                               long idAuditoriaHistorial, String pin) throws HttpStatusException, IOException, HttpStatusAcceptException, com.myapps.vista_ccc.reset_pin.error.HttpStatusException {
        long ini = System.currentTimeMillis();
        String output = "";
        String urlConsumer = urlService;
        logger.debug("Url service: " + urlConsumer);
        try {
            String body = Parametros.restUrlBodySMS.replace("%MSISDN%", isdn).replace("%PIN%", pin).replace("%MENSAJE%", Parametros.restMensajeSMS);
            output = clientRest.sendSMS(urlConsumer, isdn, pin, body);
            logger.debug("Request SendMSM Json: " + body);
            logger.debug("Response SendMSM Json: " + output);
            long fin = System.currentTimeMillis();
            logger.info("[Serivicio RESTful: SendMSM ] Tiempo de respuesta: " + (fin - ini)
                    + " milisegundos");
            if (Parametros.saveRequestResponse) {
                try {
                    auditoria.saveXml(isdn, usuario, urlConsumer, "SendMSM", body, output,
                            idAuditoriaHistorial, (fin - ini), (fin - ini));
                } catch (Exception e) {
                    logger.warn("No se logro registrar los request y response del servicio RESTful SendMSM: ", e);
                }
            }

        } catch (ConnectException e) {
            logger.error("No se pudo establecer una conexion con el servicio Rest SendMSM " + e.getMessage(), e);

            throw new HttpStatusException(StatusResponse.HTTP_NOT_FOUND.getDescripcion(),
                    StatusResponse.HTTP_NOT_FOUND);
        } catch (ConnectTimeoutException e) {
            logger.error("Tiempo de espera agotado para respuesta de servicio SendMSM " + urlConsumer + " "
                    + e.getMessage(), e);

            throw new HttpStatusException(StatusResponse.HTTP_ERROR.getDescripcion() + ", Causa: " + e.getMessage(),
                    StatusResponse.HTTP_ERROR);
        }
        return output;
    }

}
