
package com.myapps.vista_ccc.reset_pin.model.gson.sms;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Links_ implements Serializable
{

    @SerializedName("self")
    @Expose
    private String self;
    @SerializedName("related")
    @Expose
    private String related;
    private  static final long serialVersionUID = -1997401322966666665L;

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getRelated() {
        return related;
    }

    public void setRelated(String related) {
        this.related = related;
    }

}
