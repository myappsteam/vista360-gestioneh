
package com.myapps.vista_ccc.reset_pin.model.gson.sms;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Resource implements Serializable
{

    @SerializedName("links")
    @Expose
    private Links links;
    @SerializedName("data")
    @Expose
    private Data_ data;
    private  static final long serialVersionUID = 9064694447427544589L;

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }

    public Data_ getData() {
        return data;
    }

    public void setData(Data_ data) {
        this.data = data;
    }

}
