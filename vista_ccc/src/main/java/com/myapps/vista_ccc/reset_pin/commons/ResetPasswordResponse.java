package com.myapps.vista_ccc.reset_pin.commons;

public enum ResetPasswordResponse {
    BAD_REQUEST(400, "Bad Request"),
    HTTP_UNAUTHORIZED(401, "HTTP 401 Unauthorized\". Invalid Token"),
    HTTP_NOT_FOUND(404, "User not found"),
    HTTP_INTERNAL_SERVER_ERROR(500, "Error interno de servidor."),
    HTTP_OK(204, "OK ");

    private int code;
    private String descripcion;

    ResetPasswordResponse(int code, String descripcion) {
        this.code = code;
        this.descripcion = descripcion;
    }

    public int getCode() {
        return code;
    }

    public String getDescripcion() {
        return descripcion;
    }

}
