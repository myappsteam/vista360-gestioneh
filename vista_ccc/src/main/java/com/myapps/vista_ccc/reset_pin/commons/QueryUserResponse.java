package com.myapps.vista_ccc.reset_pin.commons;

public enum QueryUserResponse {
    FORBIDDEN(403, "When authorization failed. When creating new entity with client generated identifier. When creating new resource is not allowed\n" +
            "for whatever reason."),
    CONFLICT(409, "When sending invalid type for resource."),

    HTTP_UNAUTHORIZED(401, "HTTP 401 Unauthorized\" (Invalid token or expired.)"),
    HTTP_NOT_FOUND(404, "Realm not found"),
    HTTP_INTERNAL_SERVER_ERROR(500, "Error interno de servidor."),
    HTTP_OK(200, "OK (no empty response)"),
    CREATED(201, "CREATED");
    private int code;
    private String descripcion;

    QueryUserResponse(int code, String descripcion) {
        this.code = code;
        this.descripcion = descripcion;
    }

    public int getCode() {
        return code;
    }

    public String getDescripcion() {
        return descripcion;
    }

}
