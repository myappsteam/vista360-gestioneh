package com.myapps.vista_ccc.reset_pin.servicio.pin;


import java.io.Serializable;
import java.util.LinkedList;
import java.util.Queue;

public class Cola implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static final Queue<String> cola;

    static {
        cola = new LinkedList<>();
    }

    public synchronized static boolean put(String trama) {
        return cola.offer(trama);
    }

    public synchronized static boolean contains(String trama) {
        return cola.contains(trama);
    }

    public synchronized static String poll() {
        return cola.poll();
    }

    public synchronized static int size() {
        return cola.size();
    }

    public synchronized static void vaciar() {
        String poll = cola.poll();
        while (poll != null) {
            poll = cola.poll();
        }
    }
}
