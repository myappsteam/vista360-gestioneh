package com.myapps.vista_ccc.bean;

import com.huawei.www.bme.cbsinterface.bccommon.*;
import com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestQueryObjSubAccessCode;
import com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoResultSubscriber;
import com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoResultSubscriberSubscriberInfo;
import com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoResultSubscriberSupplementaryOffering;
import com.myapps.user.bean.ControlerBitacora;
import com.myapps.user.ldap.DescriptorBitacora;
import com.myapps.vista_ccc.bean.model.*;
import com.myapps.vista_ccc.bean.model.Currency;
import com.myapps.vista_ccc.business.*;
import com.myapps.vista_ccc.entity.*;
import com.myapps.vista_ccc.servicio.*;
import com.myapps.vista_ccc.thread.*;
import com.myapps.vista_ccc.util.*;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.axis.AxisFault;
import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.TreeNode;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.servlet.http.HttpServletRequest;
import javax.xml.rpc.ServiceException;
import java.io.CharArrayWriter;
import java.io.PrintWriter;
import java.io.Serializable;
import java.math.BigInteger;
import java.net.URL;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.*;

@ManagedBean
@ViewScoped
public class CcwsBean implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(CcwsBean.class);

	@Inject
	private HiloBL blHilo;

	@Inject
	private LogAuditoriaBL blAuditoria;

	@Inject
	private ConsultaBL blConsulta;

	@Inject
	private ConsultaRedBL blConsultaRed;

	@Inject
	private AccountHistoryBL blAccountHistory;

	@Inject
	private IncidenteBL blIncidente;

	@Inject
	private LogExcepcionBL blExcepcion;

	@Inject
	private ConexionTelnetBL blConexionTelnet;

	@Inject
	private ControlerBitacora controlerBitacora;

	@Inject
	private ServidorBL servidorBL;

	@Inject
	private ExcepcionElementosRedBL ExcepcionBL;

	// public static final String PLATAFORMA = "CBS HUAWEI";
	// public static final String TAB_HISTORIAL = "CONSULTA HISTORIAL";
	// public static final String TAB_VOUCHER = "CONSULTA VOUCHER";
	// public static final String TAB_CALLING_CIRCLE = "VISTA CALLING CIRCLE";
	// public static final String TAB_CUG = "CONSULTA CUG";
	// public static final String TAB_BILLETERA = "CONSULTA BILLETERA";
	// public static final String NUMEROS_FAVORITOS = "NUMEROS FAVORITOS";
	// public static final String OFERTAS = "OFERTAS";

	private String nro;
	private String isdn;
	private String login;
	private String ip;
	private long idAuditoriaHistorial;
	private long CodigoAuditoria;
	private String title;

	private boolean disableTab;
	private boolean readInput;
	private boolean disableButton;
	private boolean all;

	private boolean disableIncidente;
	private Enum vistaIncidente;

	// private String ffPhonebook;
	private List<Persona> personas;

	private List<Checkbox> listCheckbox;
	private Checkbox[] listCheckboxSelected;
	private int maxDias;

	@ManagedProperty(value = "#{checkboxService}")
	private CheckboxService checkboxService;

	// HISTORIAL
	private History selectedHistory;
	private List<Property> propertys;

	private ControlButton controlButton;
	private ControlButton controlButtonTelnet;

	private Date fromDate;
	private Date toDate;
	private Integer daySelected;
	private String valueRadioDay;
	private String valueRadioBetweenDay;
	private boolean disabledRadioDay;
	private boolean disabledRadioBetweenDay;

	private HashMap<String, String> hashServiceCategory;
	private HashMap<String, String> hashFlowType;
	private HashMap<String, String> hashServiceType;
	// private HashMap<String, String> hashCheckBox;
	private HashMap<String, String> hashRoamFlag;
	private HashMap<String, String> hashSpecialNumber;
	private HashMap<String, String> hashAdjustmentType;
	private HashMap<String, String> hashRechargeType;
	private HashMap<String, String> hashRechargeChannelId;
	private HashMap<String, String> hashResultCode;
	private HashMap<String, String> hashReversalFlag;
	private HashMap<String, String> hashAdjustmentChannelId;
	private HashMap<String, String> hashRefundIndicator;

	private LazyDataModel<History> model;
	private ArrayList<BalanceDetail> listBalanceDetail;
	private boolean enabledColumnCurrentAmount;

	// BILLETERAS
	private List<Balance> balances;
	private Balance selectedBalance;
	private HashMap<String, String> hashBalanceType;
	private HashMap<String, String> hashFreeUnit;
	private HashMap<String, String> hashAcckey;
	private int id;
	private HashMap<String, FreeUnitMeasure> hashFreeUnitMeasure;
	private HashMap<String, String> hashDaysExpired;
	private List<LifeCycle> lifeCycles;
	private ArrayList<String> listBilleteraExpired;

	// FAVORITOS
	private TreeNode rootF;
	private HashMap<String, String> hashNumeroFavorito;

	// OFERTA
	private String tipoSubscriber;
	private String statusDetail;
	private String blackList;

	// status detail
	private HashMap<String, String> hashBlackListDescription;

	private TreeNode rootO;

	private HashMap<String, String> hashPaymentMode;
	private HashMap<String, String> hashStatus;
	private HashMap<String, String> hashBundledFlag;
	private HashMap<String, String> hashOfferingClass;
	private HashMap<String, String> hashStatusDetail;
	private ArrayList<Integer> listPositions;

	@Inject
	private OfferingBL blOffering;
	// VOUCHER
	private String batchNumber;
	private String serialNumber;
	private Voucher voucher;
	private ArrayList<SimpleVoucherOperation> listVoucherOperation;
	private HashMap<String, String> hashOperationType;
	private HashMap<String, String> hashCardState;
	private HashMap<String, String> hashCurrency;

	// CUG
	private Cug cug;
	private HashMap<String, String> hashMemberTypeCode;

	// CALLING CIRCLE
	private String circleName;
	private String circleNamePool;
	private CallingCircle selectedPrimaryIdentity;
	private List<CallingCircle> callingCircles;
	private List<CallingCircleProperties> callingCirclesProperties;

	@PostConstruct
	public void init() {
		try {
			// log.debug("iniciaando
			// postconstruct....................................................");
			inicializando();
			controlButton = new ControlButton(false, false, true);
			controlButtonTelnet = new ControlButton(false, false, true);

		} catch (Exception e) {
			log.error("Error al iniciar, " + e);
		}
	}

	public void inicializando() {
		log.debug("iniciando ccc");
		try {
			Servicio.setPagina(this.getClass().getSimpleName());
			Servicio.eliminarConexionesInactivas(this.getClass().getSimpleName());
			// <link
			// href="#{request.contextPath}/resources/css/simple-ripple-effect.css"
			// rel="stylesheet" type="text/css" />
			// <script type="text/javascript"
			// src="#{request.contextPath}/resources/js/jquery-2.1.3.js"></script>
			// <script type="text/javascript"
			// src="#{request.contextPath}/resources/js/simple-ripple-effect.js"></script>

			setEnabledColumnCurrentAmount(Parametros.enabledColumnCurrentAmount);

			maxDias = Parametros.historialForDays;
			disabledRadioDay = true;
			disableButton = true;
			isdn = "";

			// Control Registro Incidentes
			disableIncidente = true;
			vistaIncidente = null;
			CodigoAuditoria = 0;
			// Favoritos
			rootF = new DefaultTreeNode(new NumeroFavorito("Root", "", "", "", ""), null);

			// Ofertas
			rootO = new DefaultTreeNode(new Oferta("Root", "", "", "", "", ""), null);
			tipoSubscriber = "";
			statusDetail = "";
			blackList = "";

			//
			readInput = false;

			setDisableTab(true);
			// ffPhonebook = "F&F Phonebook";
			voucher = new Voucher();
			setValueRadioDay("History For");
			setValueRadioBetweenDay("");
			// disabledRadioDay = false;
			disabledRadioBetweenDay = true;
			daySelected = 1;

			callingCircles = new ArrayList<CallingCircle>();
			setCallingCirclesProperties(new ArrayList<CallingCircleProperties>());

			balances = new ArrayList<Balance>();
			setListBalanceDetail(new ArrayList<BalanceDetail>());
			lifeCycles = new ArrayList<LifeCycle>();

			propertys = new ArrayList<Property>();

			listVoucherOperation = new ArrayList<SimpleVoucherOperation>();

			nro = "";
			// nro = "78048990";
			// nro = "78001023";

			// batchNumber = "1602000000";
			// serialNumber = "00004";
			// circleName = "Group20160115051479";
			batchNumber = "";
			serialNumber = "";
			circleName = "";
			cargarIp();
			cargarHashCardStates();
			cargarHashMemberTypeCode();
			cargarHashOperationTypes();
			cargarHashServiceCategory();
			cargarHashFlowType();
			cargarHashServiceType();
			cargarHashBalanceType();
			cargarHashFreeUnitType();
			cargarHashRoamFlag();
			cargarHashAdjustmentType();
			cargarHashSpecialNumber();
			cargarHashRechargeType();
			cargarHashRechargeChannelId();
			cargarHashResultCode();
			cargarHashReversalFlag();
			cargarHashRefundIndicator();
			cargarHashAdjustmentChannelId();
			cargarHashDaysExpired(Parametros.configFile + "days_expired.xml");
			cargarHashAccKey();
			cargarCheckbox();
			obtenerUsuario();

			cargarListBilleteraExpired();

			// Numeros favoritos
			cargarHashNumeroFavorito();

			// ofertas
			cargarHashPaymentMode();
			cargarHashStatus();
			cargarHashBundledFlag();
			cargarHashOfferingClass();
			cargarHashStatusDetail(Parametros.configFile + "status_detail.xml");
			cargarListPosition();
			cargarHashBlackListDescription();

			Calendar c = Calendar.getInstance();
			c.add(Calendar.HOUR_OF_DAY, -Parametros.historialHoraTemporal);

			try {
				blAccountHistory.eliminarHistorial(login);
			} catch (Exception e) {
				log.error("Error al eliminar registros temporales, isdn: " + isdn + ", login: " + getLogin(), e);
			}

			try {
				log.debug("Se van a eliminar registros con fecha menor a: " + UtilDate.dateToString(c.getTime(), "dd-MM-yyyy HH:mm:ss"));
				blAccountHistory.eliminarHistorialPorFecha(c.getTime());
			} catch (Exception e) {
				log.error("Error al eliminar historial: ", e);
			}

			loadCurrency(Parametros.configFile + "currency.xml");
			loadFreeUnitMeasure(Parametros.configFile + "freeunit_measure.xml");

			paginar();
		} catch (Exception e) {
			log.error("Error al iniciar, ", e);
		}
	}

	public void selectAll() {
		if (all) { // activar todos los check
			listCheckboxSelected = new Checkbox[listCheckbox.size()];
			int i = 0;
			for (Checkbox c : listCheckbox) {
				listCheckboxSelected[i] = c;
				i++;
			}
		} else {
			listCheckboxSelected = new Checkbox[0];
		}
	}

	private void obtenerUsuario() {
		try {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			this.login = (String) request.getSession().getAttribute("TEMP$USER_NAME");
			// log.info("login: " + login);
		} catch (Exception e) {
			log.error("Error al obtener usuario: ", e);
		}
	}

	public void limpiar() {

		// try{
		// String command = "ls -la";
		//
		// String user = "prueba";
		// String host = "localhost";
		// Integer port = 2222;
		// String pass = "123";
		//
		// JSch jsch = new JSch();
		// Session session = jsch.getSession(user, host, port);
		// Properties config = new Properties();
		// config.put("StrictHostKeyChecking", "no");
		// session.setConfig(config);
		// session.setPassword(pass);
		// session.connect();
		//
		// log.error("Is connected: " +session.isConnected());
		// Channel channel = session.openChannel("exec");
		// ((ChannelExec)channel).setCommand("cmd C:/ mkdir temp");
		//// ((ChannelExec)channel).setCommand(command);
		// channel.setInputStream(null);
		// ((ChannelExec)channel).setErrStream(System.err);
		//
		// InputStream input = (InputStream) channel.getInputStream();
		// channel.connect();
		//
		// log.error("Channel Connected to machine " + host + " server with
		// command: " + command );
		//
		// try{
		// InputStreamReader inputReader = new InputStreamReader(input);
		// BufferedReader bufferedReader = new BufferedReader(inputReader);
		// String line = null;
		//
		// while((line = bufferedReader.readLine()) != null){
		// log.error(line);
		// }
		// bufferedReader.close();
		// inputReader.close();
		// }catch(IOException ex){
		// ex.printStackTrace();
		// log.error("error:" + ex.getMessage());
		// }
		//
		// channel.disconnect();
		// session.disconnect();
		// }catch(Exception ex){
		// log.error("error:" + ex.getMessage());
		// ex.printStackTrace();
		// }
		this.all = false;
		this.nro = "";
		this.isdn = "";
		readInput = false;

		disableButton = true;
		disabledRadioDay = true;
		disableIncidente = true;
		vistaIncidente = null;
		// Favoritos
		rootF = new DefaultTreeNode(new NumeroFavorito("Root", "", "", "", ""), null);

		// Ofertas
		rootO = new DefaultTreeNode(new Oferta("Root", "", "", "", "", ""), null);
		tipoSubscriber = "";
		statusDetail = "";
		blackList = "";

		voucher = new Voucher();
		listVoucherOperation = new ArrayList<SimpleVoucherOperation>();
		batchNumber = "";
		serialNumber = "";

		callingCircles = new ArrayList<CallingCircle>();
		circleName = "";

		cug = new Cug();

		balances = new ArrayList<Balance>();
		lifeCycles = new ArrayList<LifeCycle>();

		model = new LazyCarDataModel(new ArrayList<History>(), isdn, getLogin());
		propertys = new ArrayList<>();
		listBalanceDetail = new ArrayList<>();
		daySelected = 1;
		fromDate = null;
		toDate = null;
		listCheckboxSelected = new Checkbox[0];

	}

	// public void logout() {
	// try {
	// ExternalContext ec =
	// FacesContext.getCurrentInstance().getExternalContext();
	// ec.redirect(ec.getRequestContextPath() + "/Login?type=logout_bean");
	// } catch (Exception e) {
	// log.error("Error al cerrar sesion: ", e);
	// }
	// }

	private void cargarIp() {
		try {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();

			ip = UtilUrl.getClientIp(request);

		} catch (Exception e) {
			log.error("Error al obtener ip: ", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void cargarCheckbox() {
		try {
			listCheckbox = new ArrayList<Checkbox>();
			listCheckboxSelected = new Checkbox[0];

			List<VcHilo> lista = blHilo.findAll();

			// Hilo h = new Hilo();
			// List<Hilo> lista = h.lista();
			if (lista != null) {
				for (VcHilo hilo : lista) {
					boolean disabled = false;
					if (hilo.getActivo().equals("N"))
						disabled = true;
					Checkbox cb = new Checkbox(hilo.getIdHilo() + "", hilo.getNameCheckbox(), disabled);
					listCheckbox.add(cb);
				}
			}
			checkboxService.setListCheckbox(listCheckbox);
		} catch (Exception e) {
			log.error("Error al cargar checkbox de la bd: ", e);
		}
	}

	public void selectRadioDay() {
		log.debug("select radio day");
		try {
			setValueRadioDay("History For");
			setValueRadioBetweenDay("");
			disabledRadioBetweenDay = true;
			disabledRadioDay = false;
		} catch (Exception e) {
			log.error("Error al seleccionar radio day: ", e);
		}
	}

	public void selectRadioBetweenDay() {
		log.debug("select radio between day, disableTab: " + disableTab);
		try {
			setValueRadioDay("");
			setValueRadioBetweenDay("Between Day");
			disabledRadioDay = true;
			disabledRadioBetweenDay = false;
		} catch (Exception e) {
			log.error("Error al seleccionar radio day: ", e);
		}
	}

	private void loadCurrency(String path) {
		log.debug("Ingresando a cargar monedas");
		try {
			hashCurrency = new HashMap<String, String>();

			ArrayList<Currency> listCurrency = (new ToXml()).readObject(path);
			if (listCurrency != null) {
				for (Currency cu : listCurrency) {
					hashCurrency.put(cu.getId().intValue() + "", cu.getDescription().trim());
				}
			}
			// log.debug("hash map value: " + currencys.get("1049"));
		} catch (Exception e) {
			log.error("Error al cargar archivo de monedas: ", e);
			SysMessage.error("Error al cargar archivo de monedas: " + e.getMessage(), null);
		}
	}

	private void loadFreeUnitMeasure(String path) {
		log.debug("Ingresando a cargar freeunit measure");
		try {
			hashFreeUnitMeasure = new HashMap<String, FreeUnitMeasure>();

			ArrayList<FreeUnitMeasure> list = (new ToXml()).readObject(path);
			if (list != null) {
				for (FreeUnitMeasure free : list) {
					hashFreeUnitMeasure.put(free.getMeasureUnit(), free);
				}
			}
			// log.debug("hash map value: " + currencys.get("1049"));
		} catch (Exception e) {
			log.error("Error al cargar archivo de monedas: ", e);
			SysMessage.error("Error al cargar archivo de monedas: " + e.getMessage(), null);
		}
	}

	private void cargarHashAccKey() {
		try {
			hashAcckey = new HashMap<String, String>();
			String states = Parametros.billeteraAccKey;
			StringTokenizer st = new StringTokenizer(states, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				try {
					StringTokenizer stt = new StringTokenizer(nextToken.trim(), ":");
					String key = stt.nextToken();
					String value = stt.nextToken();
					hashAcckey.put(key.trim(), value.trim());
				} catch (Exception e) {
					e.getMessage();
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash AccKey : ", e);
			SysMessage.error("Error al cargar hash AccKey : " + e.getMessage(), null);
		}
	}

	private void cargarHashOperationTypes() {
		try {
			hashOperationType = new HashMap<String, String>();
			String operations = Parametros.voucherOperationType;
			StringTokenizer st = new StringTokenizer(operations, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				try {
					StringTokenizer stt = new StringTokenizer(nextToken.trim(), ":");
					String key = stt.nextToken();
					String value = stt.nextToken();
					// log.debug("key: " + key + ", value: " + value);
					hashOperationType.put(key.trim(), value.trim());
				} catch (Exception e) {
					e.getMessage();
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash operation types: ", e);
			SysMessage.error("Error al cargar hash operation types: " + e.getMessage(), null);
		}
	}

	private void cargarHashAdjustmentChannelId() {
		try {
			hashAdjustmentChannelId = new HashMap<String, String>();
			String states = Parametros.historialAdjustmentChannelId;
			StringTokenizer st = new StringTokenizer(states, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				try {
					StringTokenizer stt = new StringTokenizer(nextToken.trim(), ":");
					String key = stt.nextToken();
					String value = stt.nextToken();
					hashAdjustmentChannelId.put(key.trim(), value.trim());
				} catch (Exception e) {
					e.getMessage();
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash adjustment channel id: ", e);
			SysMessage.error("Error al cargar hash adjustment channel id: " + e.getMessage(), null);
		}
	}

	private void cargarHashCardStates() {
		try {
			hashCardState = new HashMap<String, String>();
			String states = Parametros.voucherCardState;
			StringTokenizer st = new StringTokenizer(states, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				try {
					StringTokenizer stt = new StringTokenizer(nextToken.trim(), ":");
					String key = stt.nextToken();
					String value = stt.nextToken();
					hashCardState.put(key.trim(), value.trim());
				} catch (Exception e) {
					e.getMessage();
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash card states: ", e);
			SysMessage.error("Error al cargar hash card states: " + e.getMessage(), null);
		}
	}

	private void cargarHashRefundIndicator() {
		try {
			hashRefundIndicator = new HashMap<String, String>();
			String states = Parametros.historialRefundIndicator;
			StringTokenizer st = new StringTokenizer(states, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				try {
					StringTokenizer stt = new StringTokenizer(nextToken.trim(), ":");
					String key = stt.nextToken();
					String value = stt.nextToken();
					hashRefundIndicator.put(key.trim(), value.trim());
				} catch (Exception e) {
					e.getMessage();
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash refund indicator: ", e);
			SysMessage.error("Error al cargar hash refund indicator: " + e.getMessage(), null);
		}
	}

	private void cargarHashResultCode() {
		try {
			hashResultCode = new HashMap<String, String>();
			String states = Parametros.historialResultCode;
			StringTokenizer st = new StringTokenizer(states, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				try {
					StringTokenizer stt = new StringTokenizer(nextToken.trim(), ":");
					String key = stt.nextToken();
					String value = stt.nextToken();
					hashResultCode.put(key.trim(), value.trim());
				} catch (Exception e) {
					e.getMessage();
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash result code: ", e);
			SysMessage.error("Error al cargar hash result code: " + e.getMessage(), null);
		}
	}

	private void cargarHashReversalFlag() {
		try {
			hashReversalFlag = new HashMap<String, String>();
			String states = Parametros.historialReversalFlag;
			StringTokenizer st = new StringTokenizer(states, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				try {
					StringTokenizer stt = new StringTokenizer(nextToken.trim(), ":");
					String key = stt.nextToken();
					String value = stt.nextToken();
					hashReversalFlag.put(key.trim(), value.trim());
				} catch (Exception e) {
					e.getMessage();
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash reversal flag: ", e);
			SysMessage.error("Error al cargar hash reversal flag: " + e.getMessage(), null);
		}
	}

	private void cargarHashRechargeChannelId() {
		try {
			hashRechargeChannelId = new HashMap<String, String>();
			String states = Parametros.historialRechargeChannelId;
			StringTokenizer st = new StringTokenizer(states, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				try {
					StringTokenizer stt = new StringTokenizer(nextToken.trim(), ":");
					String key = stt.nextToken();
					String value = stt.nextToken();
					hashRechargeChannelId.put(key.trim(), value.trim());
				} catch (Exception e) {
					e.getMessage();
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash recharge channel id: ", e);
			SysMessage.error("Error al cargar hash recharge channel id: " + e.getMessage(), null);
		}
	}

	private void cargarHashRoamFlag() {
		try {
			hashRoamFlag = new HashMap<String, String>();
			String states = Parametros.historialRoamFlag;
			StringTokenizer st = new StringTokenizer(states, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				try {
					StringTokenizer stt = new StringTokenizer(nextToken.trim(), ":");
					String key = stt.nextToken();
					String value = stt.nextToken();
					hashRoamFlag.put(key.trim(), value.trim());
				} catch (Exception e) {
					e.getMessage();
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash roam flag: ", e);
			SysMessage.error("Error al cargar hash roam flag: " + e.getMessage(), null);
		}
	}

	private void cargarHashRechargeType() {
		try {
			hashRechargeType = new HashMap<String, String>();
			String states = Parametros.historialRechargeType;
			StringTokenizer st = new StringTokenizer(states, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				try {
					StringTokenizer stt = new StringTokenizer(nextToken.trim(), ":");
					String key = stt.nextToken();
					String value = stt.nextToken();
					hashRechargeType.put(key.trim(), value.trim());
				} catch (Exception e) {
					e.getMessage();
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash recharge type: ", e);
			SysMessage.error("Error al cargar hash recharge type: " + e.getMessage(), null);
		}
	}

	private void cargarHashAdjustmentType() {
		try {
			hashAdjustmentType = new HashMap<String, String>();
			String states = Parametros.historialAdjustmentType;
			StringTokenizer st = new StringTokenizer(states, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				try {
					StringTokenizer stt = new StringTokenizer(nextToken.trim(), ":");
					String key = stt.nextToken();
					String value = stt.nextToken();
					hashAdjustmentType.put(key.trim(), value.trim());
				} catch (Exception e) {
					e.getMessage();
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash adjustment type: ", e);
			SysMessage.error("Error al cargar hash adjustment type: " + e.getMessage(), null);
		}
	}

	private void cargarHashSpecialNumber() {
		try {
			hashSpecialNumber = new HashMap<String, String>();
			String states = Parametros.historialSpecialNumberIndicator;
			StringTokenizer st = new StringTokenizer(states, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				try {
					StringTokenizer stt = new StringTokenizer(nextToken.trim(), ":");
					String key = stt.nextToken();
					String value = stt.nextToken();
					hashSpecialNumber.put(key.trim(), value.trim());
				} catch (Exception e) {
					e.getMessage();
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash special number indicator: ", e);
			SysMessage.error("Error al cargar hash special number indicator: " + e.getMessage(), null);
		}
	}

	private void cargarHashServiceCategory() {
		try {
			hashServiceCategory = new HashMap<String, String>();
			String valores = Parametros.historialServiceCategory;
			StringTokenizer st = new StringTokenizer(valores, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				try {
					StringTokenizer stt = new StringTokenizer(nextToken.trim(), ":");
					String key = stt.nextToken();
					String value = stt.nextToken();
					// log.debug("key: " + key + ", value: " + value);
					hashServiceCategory.put(key.trim(), value.trim());
				} catch (Exception e) {
					e.getMessage();
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash services category: ", e);
			SysMessage.error("Error al cargar hash services category: " + e.getMessage(), null);
		}
	}

	private void cargarHashBalanceType() {
		try {
			hashBalanceType = new HashMap<String, String>();
			String states = Parametros.billeterabalancetype;
			StringTokenizer st = new StringTokenizer(states, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				try {
					StringTokenizer stt = new StringTokenizer(nextToken, ":");
					String key = stt.nextToken();
					String value = stt.nextToken();
					// log.debug("key: " + key + ", value: " + value);
					hashBalanceType.put(key.trim(), value.trim());
				} catch (Exception e) {
					e.getMessage();
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash Balance Type: ", e);
			SysMessage.error("Error al cargar hash Balance Type: " + e.getMessage(), null);
		}
	}

	private void cargarHashFreeUnitType() {
		try {
			hashFreeUnit = new HashMap<String, String>();
			String states = Parametros.billeterafreeunittype;
			StringTokenizer st = new StringTokenizer(states, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				try {
					StringTokenizer stt = new StringTokenizer(nextToken, ":");
					String key = stt.nextToken();
					String value = stt.nextToken();
					hashFreeUnit.put(key.trim(), value.trim());
				} catch (Exception e) {
					e.getMessage();
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash Balance Type: ", e);
			SysMessage.error("Error al cargar hash Balance Type: " + e.getMessage(), null);
		}
	}

	private void cargarHashFlowType() {
		try {
			hashFlowType = new HashMap<String, String>();
			String valores = Parametros.historialFlowType;
			StringTokenizer st = new StringTokenizer(valores, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				try {
					StringTokenizer stt = new StringTokenizer(nextToken.trim(), ":");
					String key = stt.nextToken();
					String value = stt.nextToken();
					hashFlowType.put(key.trim(), value.trim());
				} catch (Exception e) {
					e.getMessage();
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash flow type: ", e);
			SysMessage.error("Error al cargar hash flow type: " + e.getMessage(), null);
		}
	}

	private void cargarHashServiceType() {
		try {
			hashServiceType = new HashMap<String, String>();
			String valores = Parametros.historialServiceType;
			StringTokenizer st = new StringTokenizer(valores, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				try {
					StringTokenizer stt = new StringTokenizer(nextToken.trim(), ":");
					String key = stt.nextToken();
					String value = stt.nextToken();
					hashServiceType.put(key.trim(), value.trim());
				} catch (Exception e) {
					e.getMessage();
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash service type: ", e);
			SysMessage.error("Error al cargar hash service type: " + e.getMessage(), null);
		}
	}

	private void cargarHashMemberTypeCode() {
		try {
			hashMemberTypeCode = new HashMap<String, String>();
			String states = Parametros.cugMemberTypeCode;
			StringTokenizer st = new StringTokenizer(states, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				try {
					StringTokenizer stt = new StringTokenizer(nextToken, ":");
					String key = stt.nextToken();
					String value = stt.nextToken();
					hashMemberTypeCode.put(key.trim(), value.trim());
				} catch (Exception e) {
					e.getMessage();
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash member type code: ", e);
			SysMessage.error("Error al cargar hash member type code: " + e.getMessage(), null);
		}
	}

	private void cargarHashNumeroFavorito() {
		try {
			hashNumeroFavorito = new HashMap<String, String>();
			String states = Parametros.numerosFavoritos;
			StringTokenizer st = new StringTokenizer(states, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				hashNumeroFavorito.put(nextToken.trim(), nextToken.trim());
			}
		} catch (Exception e) {
			log.error("Error al cargar hash Numero Favorito: ", e);
			SysMessage.error("Error al cargar hash Numero Favorito: " + e.getMessage(), null);
		}
	}

	private void cargarHashBlackListDescription() {
		try {
			hashBlackListDescription = new HashMap<String, String>();
			String operations = Parametros.ofertaStatusBlackListDescription;
			StringTokenizer st = new StringTokenizer(operations, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				try {
					StringTokenizer stt = new StringTokenizer(nextToken.trim(), ":");
					String key = stt.nextToken();
					String value = stt.nextToken();
					// log.debug("key: " + key + ", value: " + value);
					hashBlackListDescription.put(key.trim(), value.trim());
				} catch (Exception e) {
					e.getMessage();
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash BlackLista Description: ", e);
			SysMessage.error("Error al cargar hash BlackLista Description: " + e.getMessage(), null);
		}
	}

	@SuppressFBWarnings(value = "DLS_DEAD_LOCAL_STORE")
	public void CargarFavorito() {
		log.debug("Ingresando a cargar Numeros Favoritos");
		long idAuditoria = 0;
		try {

			rootF = new DefaultTreeNode(new NumeroFavorito("Root", "", "", "", ""), null);
			if (login == null || login.trim().isEmpty()) {
				SysMessage.warn("Usuario no válido", null);
				return;
			}

			if (getIsdn() != null && !getIsdn().trim().isEmpty()) {

				// setDisableButton(true);

				idAuditoria = guardarAuditoria(Calendar.getInstance(), this.login, this.ip, Parametros.vistaNumeroFavorito, Parametros.comandoNumeroFavorito, this.isdn, null);
				if (idAuditoria <= 0) {
					SysMessage.error("Error al guardar log de auditoria", null);
					return;
				}
				disableIncidente = false;
				vistaIncidente = DescriptorBitacora.NUMERO_FAVORITO;
				try {
					controlerBitacora.accion(DescriptorBitacora.NUMERO_FAVORITO, "Se hizo consulta de numeros favoritos para el numero: " + isdn);
				} catch (Exception e) {
					log.error("Error al guardar bitacora en el sistema: ", e);
					SysMessage.error("Error al guardar bitacora", null);
				}

				String wsdlLoc = Parametros.cugWsdl;

				URL url = new URL(UtilUrl.getIp(wsdlLoc));

				long ini = System.currentTimeMillis();

				if (url.getProtocol().equalsIgnoreCase("https")) {
					int puerto = url.getPort();
					String host = url.getHost();
					if (host.equalsIgnoreCase("localhost")) {
						host = "127.0.0.1";
					}
					validarCertificado(Parametros.cugPathKeystore, host + ":" + puerto);
				}

				NodoServicio<com.huawei.www.bme.cbsinterface.bcservices.BcServicsBindingStub> portNodo = ServicioCallingCircle.initPort(wsdlLoc, "NUMERO_FAVORITO" + login, Parametros.cugTimeOut);

				if (portNodo != null && portNodo.getPort() != null) {

					synchronized (portNodo) {

						try {

							com.huawei.www.bme.cbsinterface.bcservices.BcServicsBindingStub port = portNodo.getPort();

							com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo ownershipInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo();
							ownershipInfo.setBEID(Parametros.cugOwnerShipInfoId);
							com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo accessSecurity = new com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo();
							accessSecurity.setLoginSystemCode(Parametros.cugLoginSystemCode);
							accessSecurity.setPassword(Parametros.cugPassword);
							com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo operatorInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo();
							operatorInfo.setOperatorID(Parametros.cugOperatorId);
							com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat timeFormat = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat();
							timeFormat.setTimeType(Parametros.cugTimeType);

							SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
							String messageSeq = format.format(new Date());

							com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderAdditionalProperty[] additionalProperty = null;
							com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader requestHeader = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader(Parametros.cugVersion, "", messageSeq, ownershipInfo, accessSecurity, operatorInfo, "", "", timeFormat, additionalProperty);

							com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestMsg queryCustomerInfoRequestMsg = new com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestMsg();
							queryCustomerInfoRequestMsg.setRequestHeader(requestHeader);
							com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequest queryCustomerInfoRequest = new com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequest();
							com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestQueryObj queryObj = new com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestQueryObj();

							QueryCustomerInfoRequestQueryObjSubAccessCode subAccessCode = new QueryCustomerInfoRequestQueryObjSubAccessCode();
							subAccessCode.setPrimaryIdentity(isdn);
							queryObj.setSubAccessCode(subAccessCode);
							queryCustomerInfoRequest.setQueryObj(queryObj);
							queryCustomerInfoRequestMsg.setQueryCustomerInfoRequest(queryCustomerInfoRequest);
							com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoResultMsg queryCustomerInfo = port.queryCustomerInfo(queryCustomerInfoRequestMsg);

							long fin = System.currentTimeMillis();
							Long Con = null;
							if (portNodo.isPrimeraVez()) {
								Con = portNodo.getTiempoConexion();
							}
							log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio BCServices, queryCustomerInfo: " + (fin - ini) + " milisegundos");

							if (Parametros.saveRequestResponse) {
								try {
									saveXml(isdn, getLogin(), wsdlLoc, "queryCustomerInfo", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoria, Con, (fin - ini));
								} catch (Exception e) {
									log.warn("No se logro registrar los request y response del servicio BCServices, queryCustomerInfo: ", e);
								}
							}

							if (queryCustomerInfo != null && queryCustomerInfo.getResultHeader() != null) {
								if (queryCustomerInfo.getResultHeader().getResultCode().trim().equals("0")) {
									com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoResult queryCustomerInfoResult = queryCustomerInfo.getQueryCustomerInfoResult();

									if (queryCustomerInfoResult != null) {
										QueryCustomerInfoResultSubscriber subscriber = queryCustomerInfoResult.getSubscriber();

										if (subscriber != null) {

											// -------- OFERTAS
											// SUPLEMENTARIAS
											// ---------

											QueryCustomerInfoResultSubscriberSupplementaryOffering[] supplementaryOffering = subscriber.getSupplementaryOffering();

											if (supplementaryOffering != null && supplementaryOffering.length > 0) {

												for (QueryCustomerInfoResultSubscriberSupplementaryOffering item : supplementaryOffering) {

													// TreeNode
													// suplementaria =
													// new
													// DefaultTreeNode(new
													// NumeroFavorito("Ofertas:
													// Suplementarias", "",
													// "",
													// ""), root);
													// suplementaria.setExpanded(true);

													boolean creadoSuplementaria = false;

													OfferingKey offeringKey = item.getOfferingKey();
													if (offeringKey != null) {

														if (offeringKey.getOfferingID() != null) {
															String ID = hashNumeroFavorito.get(offeringKey.getOfferingID().longValue() + "");
															if (ID != null) {

																ProductInst[] productInst = item.getProductInst();
																if (productInst != null && productInst.length > 0) {

																	for (ProductInst pi : productInst) {

																		ProductInstPInstProperty[] pInstProperty = pi.getPInstProperty();

																		if (pInstProperty != null && pInstProperty.length > 0) {

																			TreeNode suplementaria = null;

																			if (!creadoSuplementaria) {
																				suplementaria = new DefaultTreeNode(new NumeroFavorito("Ofertas: Suplementarias", "", "", "", "#e2e2e2"), rootF);
																				suplementaria.setExpanded(true);
																				creadoSuplementaria = true;
																			}

																			for (ProductInstPInstProperty pip : pInstProperty) { // CADA
																				// ITEM
																				// ES
																				// UN
																				// NUMERO
																				// FAVORITO
																				NumeroFavorito num = new NumeroFavorito();

																				InstPropertySubPropInst[] sub = pip.getSubPropInst();
																				if (sub != null && sub.length > 0) {
																					for (InstPropertySubPropInst ips : sub) {
																						if (ips.getSubPropCode().equals(Parametros.numeroFavoritoFnSerial)) {
																							num.setSerialNo(ips.getValue());
																						}
																						if (ips.getSubPropCode().equals(Parametros.numeroFavoritoFnNumber)) {
																							num.setFavorito(ips.getValue());
																						}
																						if (ips.getSubPropCode().equals(Parametros.numeroFavoritoFnType)) {
																							num.setTipo(ips.getValue());
																						}
																						if (ips.getSubPropCode().equals(Parametros.numeroFavoritoFnGroup)) {
																							num.setGrupo(ips.getValue());
																						}
																					}
																				}
																				@SuppressWarnings("unused")
																				TreeNode tnSuplementaria = new DefaultTreeNode(num, suplementaria);
																			}
																		} else {
																			log.debug("[isdn: " + isdn + "] pInstProperty resuelto a nulo o vacio");
																		}
																	}
																} else {
																	log.debug("[isdn: " + isdn + "] productInst resuelto a nulo o vacio");
																}

															} else {
																log.debug("[isdn: " + isdn + "] offeringKey.getOfferingID no esta parametrizado como numero favorito");
															}
														} else {
															log.debug("[isdn: " + isdn + "] offeringKey.getOfferingID resuelto a nulo");
														}

													} else {
														log.debug("[isdn: " + isdn + "] offeringKey resuelto a nulo");
													}
												}
											} else {
												log.warn("No existen ofertas suplementarias para: " + isdn);
											}

										} else {
											log.warn("Subcriber no encontrado");
											SysMessage.warn("Subcriber no encontrado", null);
											guardarExcepcion(idAuditoria, "Subcriber no encontrado");
										}

									} else {
										log.warn("[isdn: " + getIsdn() + "] queryCustomerInfoResult resuelto a nulo");
									}
								} else {
									log.info("[isdn: " + getIsdn() + "] queryCustomerInfo ResultCode: " + queryCustomerInfo.getResultHeader().getResultCode() + ", ResultDesc: " + queryCustomerInfo.getResultHeader().getResultDesc());
									SysMessage.warn("Servicio BCServices, queryCustomerInfo Code: " + queryCustomerInfo.getResultHeader().getResultCode() + ", Desc: " + queryCustomerInfo.getResultHeader().getResultDesc(), null);
									guardarExcepcion(idAuditoria, "Servicio BCServices, queryCustomerInfo Code: " + queryCustomerInfo.getResultHeader().getResultCode() + ", Desc: " + queryCustomerInfo.getResultHeader().getResultDesc());
								}

							} else {
								if (queryCustomerInfo != null) {
									log.warn("[isdn: " + getIsdn() + "] queryCustomerInfo ResultHeader resuelto a nulo");
								} else {
									log.warn("[isdn: " + getIsdn() + "] queryCustomerInfo queryCustomerInfo resuelto a nulo");
								}
							}

							// INICIO querySubLifeCycle

							com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequest querySubLifeCycleRequest = new com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequest(subAccessCode);
							com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequestMsg querySubLifeCycleRequestMsg = new com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequestMsg(requestHeader, querySubLifeCycleRequest);

							com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleResultMsg querySubLifeCycle = port.querySubLifeCycle(querySubLifeCycleRequestMsg);

							long fin2 = System.currentTimeMillis();
							Long Con2 = null;
							if (portNodo.isPrimeraVez()) {
								Con2 = portNodo.getTiempoConexion();
							}
							log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio BCServices, querySubLifeCycle: " + (fin2 - ini) + " milisegundos");
							if (Parametros.saveRequestResponse) {
								try {
									saveXml(isdn, getLogin(), wsdlLoc, "querySubLifeCycle", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoria, Con2, (fin2 - ini));
								} catch (Exception e) {
									log.warn("No se logro registrar los request y response del servicio BCServices, querySubLifeCycle:", e);
								}
							}

							if (querySubLifeCycle != null && querySubLifeCycle.getResultHeader() != null) {
								if (querySubLifeCycle.getResultHeader().getResultCode().trim().equals("0")) {
									com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleResult querySubLifeCycleResult = querySubLifeCycle.getQuerySubLifeCycleResult();

									if (querySubLifeCycleResult != null) {
										log.debug("querySubLifeCycle.getResultHeader: " + querySubLifeCycle.getResultHeader());
									} else {
										log.warn("[isdn: " + getIsdn() + "] QuerySubLifeCycleResult resuelto a nulo");
									}
								} else {
									log.info("[isdn: " + getIsdn() + "] querySubLifeCycle ResultCode: " + querySubLifeCycle.getResultHeader().getResultCode() + ", ResultDesc: " + querySubLifeCycle.getResultHeader().getResultDesc());
									SysMessage.warn("Servicio BCServices, querySubLifeCycle Code: " + querySubLifeCycle.getResultHeader().getResultCode() + ", Desc: " + querySubLifeCycle.getResultHeader().getResultDesc(), null);
									guardarExcepcion(idAuditoria, "Servicio BCServices, querySubLifeCycle Code: " + querySubLifeCycle.getResultHeader().getResultCode() + ", Desc: " + querySubLifeCycle.getResultHeader().getResultDesc());
								}
							} else {
								if (querySubLifeCycle != null) {
									log.warn("[isdn: " + getIsdn() + "] querySubLifeCycle ResultHeader resuelto a nulo");
								} else {
									log.warn("[isdn: " + getIsdn() + "] querySubLifeCycle QuerySubLifeCycle resuelto a nulo");
								}
							}

							// FIN
						} finally {
							portNodo.setEnUso(false);
							portNodo.setFechaFin(new Date());
						}
					}

				} else {
					log.error("[isdn: " + getIsdn() + "] Port BCServices resuelto a nulo");
				}

				SysMessage.info("Consulta finalizada.", null);
			} else {
				SysMessage.warn("Nro de Cuenta no valido", null);
			}
		} catch (AxisFault a) {
			String msg = a.getMessage();
			msg = msg.toLowerCase();
			if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase()) || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase()) || msg.contains("UnknownHostException".toLowerCase()) || msg.contains("No route to host".toLowerCase())
					|| msg.contains("Connection refused".toLowerCase())) {
				log.error("Error de conexion al servicio BCServices Numero Favorito: ", a);
				SysMessage.error("Error de conexion del servicio BCServices", null);
				guardarExcepcion(idAuditoria, "Error de conexion al servicio BCServices: " + stackTraceToString(a));
			} else {
				log.error("Error AxisFault Numero Favorito: ", a);
				SysMessage.error("Error AxisFault de conexion del servicio BCServices", null);
				guardarExcepcion(idAuditoria, "Error AxisFault de conexion del servicio BCServices: " + stackTraceToString(a));
			}
		} catch (ServiceException e) {
			log.error("Error de servicio al conectarse al servicio BCServices Numero Favorito: ", e);
			SysMessage.error("Error de servicio al conectarse al servicio BCServices", null);
			guardarExcepcion(idAuditoria, "Error de servicio al conectarse al servicio BCServices: " + stackTraceToString(e));
		} catch (RemoteException e) {
			log.error("Error remoto al conectarse al servicio BCServices Numero Favorito: ", e);
			SysMessage.error("Error remoto al conectarse al servicio BCServices", null);
			guardarExcepcion(idAuditoria, "Error remoto al conectarse al servicio BCServices: " + stackTraceToString(e));
		} catch (Exception e) {
			log.error("Error al consultar Numeros Favoritos", e);
			SysMessage.error("Error al consultar", null);
			guardarExcepcion(idAuditoria, "Error al consultar: " + stackTraceToString(e));
		}

	}

	public void CargarSubscriber() {
		log.debug("Ingresando a cargar Status Subscriber");
		String currentStatusIndex = "";
		String rblackListStatus = "";
		String status = "";
		long idAuditoria = 0;
		try {
			if (login == null || login.trim().isEmpty()) {
				SysMessage.warn("Usuario no válido", null);
				return;
			}

			if (getIsdn() != null && !getIsdn().trim().isEmpty()) {

				idAuditoria = guardarAuditoria(Calendar.getInstance(), this.login, this.ip, Parametros.vistaCCC, Parametros.comandoCCC, this.isdn, null);
				if (idAuditoria <= 0) {
					SysMessage.error("Error al guardar log de auditoria", null);
					return;
				}
				disableIncidente = false;
				vistaIncidente = DescriptorBitacora.CCC;
				try {
					controlerBitacora.accion(DescriptorBitacora.CCC, "Se hizo la carga de Status Subscriber para el numero: " + isdn);
				} catch (Exception e) {
					log.error("Error al guardar bitacora en el sistema: ", e);
					SysMessage.error("Error al guardar bitacora", null);
				}

				String wsdlLoc = Parametros.cugWsdl;

				URL url = new URL(UtilUrl.getIp(wsdlLoc));

				long ini = System.currentTimeMillis();

				if (url.getProtocol().equalsIgnoreCase("https")) {
					int puerto = url.getPort();
					String host = url.getHost();
					if (host.equalsIgnoreCase("localhost")) {
						host = "127.0.0.1";
					}
					validarCertificado(Parametros.cugPathKeystore, host + ":" + puerto);
				}

				NodoServicio<com.huawei.www.bme.cbsinterface.bcservices.BcServicsBindingStub> portNodo = ServicioCallingCircle.initPort(wsdlLoc, "CCC" + login, Parametros.cugTimeOut);

				if (portNodo != null && portNodo.getPort() != null) {

					synchronized (portNodo) {

						try {

							com.huawei.www.bme.cbsinterface.bcservices.BcServicsBindingStub port = portNodo.getPort();

							com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo ownershipInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo();
							ownershipInfo.setBEID(Parametros.cugOwnerShipInfoId);
							com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo accessSecurity = new com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo();
							accessSecurity.setLoginSystemCode(Parametros.cugLoginSystemCode);
							accessSecurity.setPassword(Parametros.cugPassword);
							com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo operatorInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo();
							operatorInfo.setOperatorID(Parametros.cugOperatorId);
							com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat timeFormat = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat();
							timeFormat.setTimeType(Parametros.cugTimeType);

							SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
							String messageSeq = format.format(new Date());

							com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderAdditionalProperty[] additionalProperty = null;
							com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader requestHeader = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader(Parametros.cugVersion, "", messageSeq, ownershipInfo, accessSecurity, operatorInfo, "", "", timeFormat, additionalProperty);

							com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestMsg queryCustomerInfoRequestMsg = new com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestMsg();
							queryCustomerInfoRequestMsg.setRequestHeader(requestHeader);
							com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequest queryCustomerInfoRequest = new com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequest();
							com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestQueryObj queryObj = new com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestQueryObj();

							QueryCustomerInfoRequestQueryObjSubAccessCode subAccessCode = new QueryCustomerInfoRequestQueryObjSubAccessCode();
							subAccessCode.setPrimaryIdentity(isdn);
							queryObj.setSubAccessCode(subAccessCode);
							queryCustomerInfoRequest.setQueryObj(queryObj);
							queryCustomerInfoRequestMsg.setQueryCustomerInfoRequest(queryCustomerInfoRequest);
							com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoResultMsg queryCustomerInfo = port.queryCustomerInfo(queryCustomerInfoRequestMsg);

							long fin = System.currentTimeMillis();
							Long Con = null;
							if (portNodo.isPrimeraVez()) {
								Con = portNodo.getTiempoConexion();
							}
							log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio BCServices: " + (fin - ini) + " milisegundos");

							if (Parametros.saveRequestResponse) {
								try {
									saveXml(isdn, getLogin(), wsdlLoc, "queryCustomerInfo", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoria, Con, (fin - ini));
								} catch (Exception e) {
									log.warn("No se logro registrar los request y response del servicio BCServices, queryCustomerInfo:", e);
								}
							}

							if (queryCustomerInfo != null && queryCustomerInfo.getResultHeader() != null) {
								if (queryCustomerInfo.getResultHeader().getResultCode().trim().equals("0")) {
									com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoResult queryCustomerInfoResult = queryCustomerInfo.getQueryCustomerInfoResult();

									if (queryCustomerInfoResult != null) {
										QueryCustomerInfoResultSubscriber subscriber = queryCustomerInfoResult.getSubscriber();

										if (subscriber != null) {

											QueryCustomerInfoResultSubscriberSubscriberInfo subscriberInfo = subscriber.getSubscriberInfo();
											statusDetail = "";
											if (subscriberInfo != null) {
												if (subscriberInfo.getStatusDetail() != null && !subscriberInfo.getStatusDetail().trim().isEmpty()) {

													log.debug("list position: " + listPositions);
													if (listPositions != null && !listPositions.isEmpty()) {
														StringBuilder sb = new StringBuilder();
														for (Integer pos : listPositions) {
															String digito = subscriberInfo.getStatusDetail().substring(pos - 1, pos);
															sb.append(digito);
														}
														log.debug("combinacion Status: " + sb.toString());
														status = sb.toString();
													}
												}
											}

											if (subscriber.getPaymentMode() != null) {
												tipoSubscriber = hashPaymentMode.get(subscriber.getPaymentMode().trim());
												if (tipoSubscriber == null) {
													tipoSubscriber = subscriber.getPaymentMode().trim();
												}
											} else {
												tipoSubscriber = "";
											}
										} else {
											log.warn("Subcriber no encontrado");
											SysMessage.warn("Subcriber no encontrado", null);
											guardarExcepcion(idAuditoria, "Subcriber no encontrado");
										}

									} else {
										log.warn("[isdn: " + getIsdn() + "] queryCustomerInfoResult resuelto a nulo");
									}
								} else {
									log.info("[isdn: " + getIsdn() + "] queryCustomerInfo ResultCode: " + queryCustomerInfo.getResultHeader().getResultCode() + ", ResultDesc: " + queryCustomerInfo.getResultHeader().getResultDesc());
									SysMessage.warn("Servicio BCServices, queryCustomerInfo Code: " + queryCustomerInfo.getResultHeader().getResultCode() + ", Desc: " + queryCustomerInfo.getResultHeader().getResultDesc(), null);
									guardarExcepcion(idAuditoria, "Servicio BCServices, queryCustomerInfo Code: " + queryCustomerInfo.getResultHeader().getResultCode() + ", Desc: " + queryCustomerInfo.getResultHeader().getResultDesc());
								}

							} else {
								if (queryCustomerInfo != null) {
									log.warn("[isdn: " + getIsdn() + "] queryCustomerInfo ResultHeader resuelto a nulo");
								} else {
									log.warn("[isdn: " + getIsdn() + "] queryCustomerInfo queryCustomerInfo resuelto a nulo");
								}
							}

							// consulta QueryLifeCycle

							com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequest querySubLifeCycleRequest = new com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequest(subAccessCode);
							com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequestMsg querySubLifeCycleRequestMsg = new com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequestMsg(requestHeader, querySubLifeCycleRequest);

							com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleResultMsg querySubLifeCycle = port.querySubLifeCycle(querySubLifeCycleRequestMsg);

							long fin2 = System.currentTimeMillis();
							Long Con2 = null;
							if (portNodo.isPrimeraVez()) {
								Con2 = portNodo.getTiempoConexion();
							}
							log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio BCServices, querySubLifeCycle: " + (fin2 - ini) + " milisegundos");
							if (Parametros.saveRequestResponse) {
								try {
									saveXml(isdn, getLogin(), wsdlLoc, "querySubLifeCycle", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoria, Con2, (fin2 - ini));
								} catch (Exception e) {
									log.warn("No se logro registrar los request y response del servicio BCServices, querySubLifeCycle:", e);
								}
							}
							if (querySubLifeCycle != null && querySubLifeCycle.getResultHeader() != null) {
								if (querySubLifeCycle.getResultHeader().getResultCode().trim().equals("0")) {

									com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleResult querySubLifeCycleResult = querySubLifeCycle.getQuerySubLifeCycleResult();
									if (querySubLifeCycleResult != null) {
										String StatusIndex = querySubLifeCycleResult.getCurrentStatusIndex();
										String blackListStatus = querySubLifeCycleResult.getRBlacklistStatus();
										if (StatusIndex != null)
											currentStatusIndex = StatusIndex;

										if (blackListStatus != null) {
											rblackListStatus = blackListStatus;

										}
										this.blackList = hashBlackListDescription.get(blackListStatus.toString());

										log.debug("currentStatusIndex: " + StatusIndex);
										log.debug("rblackListStatus: " + blackListStatus);
									} else {
										log.warn("[isdn: " + getIsdn() + "] QuerySubLifeCycleResult resuelto a nulo");
									}
								} else {
									log.info("[isdn: " + getIsdn() + "] querySubLifeCycle ResultCode: " + querySubLifeCycle.getResultHeader().getResultCode() + ", ResultDesc: " + querySubLifeCycle.getResultHeader().getResultDesc());
									SysMessage.warn("Servicio BCServices, querySubLifeCycle Code: " + querySubLifeCycle.getResultHeader().getResultCode() + ", Desc: " + querySubLifeCycle.getResultHeader().getResultDesc(), null);
									guardarExcepcion(idAuditoria, "Servicio BCServices, querySubLifeCycle Code: " + querySubLifeCycle.getResultHeader().getResultCode() + ", Desc: " + querySubLifeCycle.getResultHeader().getResultDesc());
								}
							} else {
								if (querySubLifeCycle != null) {
									log.warn("[isdn: " + getIsdn() + "] querySubLifeCycle ResultHeader resuelto a nulo");
								} else {
									log.warn("[isdn: " + getIsdn() + "] querySubLifeCycle QuerySubLifeCycle resuelto a nulo");
								}
							}

							log.debug("combinacion Status Subscriber: status=" + status + ", rblackListStatus=" + rblackListStatus + ", currentStatusIndex=" + currentStatusIndex);

							statusDetail = hashStatusDetail.get(status + "|" + rblackListStatus + "|" + currentStatusIndex);

							if (statusDetail == null) {
								statusDetail = Parametros.ofertaStatusDetailMsgNotFound;
								log.debug("combinacion Status Subscriber no encontrada: status=" + status + ", rblackListStatus=" + rblackListStatus + ", currentStatusIndex=" + currentStatusIndex);
							}
						} finally {
							portNodo.setEnUso(false);
							portNodo.setFechaFin(new Date());
						}
					}

				} else {
					log.error("[isdn: " + getIsdn() + "] Port BCServices resuelto a nulo");
				}

				SysMessage.info("Consulta finalizada.", null);
			} else {
				SysMessage.warn("Nro de Cuenta no valido", null);
			}
		} catch (

				AxisFault a) {
			String msg = a.getMessage();
			msg = msg.toLowerCase();
			if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase()) || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase()) || msg.contains("UnknownHostException".toLowerCase()) || msg.contains("No route to host".toLowerCase())
					|| msg.contains("Connection refused".toLowerCase())) {
				log.error("Error de conexion al servicio BCServices Subscriber: ", a);
				SysMessage.error("Error de conexion del servicio BCServices", null);
				guardarExcepcion(idAuditoria, "Error de conexion del servicio BCServices: " + stackTraceToString(a));
			} else {
				log.error("Error AxisFault Subscriber: ", a);
				SysMessage.error("Error AxisFault de conexion del servicio BCServices", null);
				guardarExcepcion(idAuditoria, "Error AxisFault de conexion del servicio BCServices: " + stackTraceToString(a));
			}
		} catch (ServiceException e) {
			log.error("Error de servicio al conectarse al servicio BCServices Subscriber: ", e);
			SysMessage.error("Error de servicio al conectarse al servicio BCServices", null);
			guardarExcepcion(idAuditoria, "Error de servicio al conectarse al servicio BCServices: " + stackTraceToString(e));
		} catch (RemoteException e) {
			log.error("Error remoto al conectarse al servicio BCServices Subscriber: ", e);
			SysMessage.error("Error remoto al conectarse al servicio BCServices", null);
			guardarExcepcion(idAuditoria, "Error remoto al conectarse al servicio BCServices: " + stackTraceToString(e));
		} catch (Exception e) {
			log.error("Error al consultar Subscriber", e);
			SysMessage.error("Error al consultar", null);
			guardarExcepcion(idAuditoria, "Error al consultar: " + stackTraceToString(e));
		}
	}

	// @SuppressWarnings("unused")
	@SuppressFBWarnings(value = "DLS_DEAD_LOCAL_STORE")
	public void cargarOferta() {
		log.debug("Ingresando a cargar Oferta");
		long idAuditoria = 0;
		try {

			rootO = new DefaultTreeNode(new Oferta("Root", "", "", "", "", ""), null);

			if (login == null || login.trim().isEmpty()) {
				SysMessage.warn("Usuario no válido", null);
				return;
			}

			if (getIsdn() != null && !getIsdn().trim().isEmpty()) {
				idAuditoria = guardarAuditoria(Calendar.getInstance(), this.login, this.ip, Parametros.vistaOferta, Parametros.comandoOferta, this.isdn, null);
				if (idAuditoria <= 0) {
					SysMessage.error("Error al guardar log de auditoria", null);
					return;
				}

				disableIncidente = false;
				vistaIncidente = DescriptorBitacora.OFERTAS;
				try {
					controlerBitacora.accion(DescriptorBitacora.OFERTAS, "Se hizo consulta de ofertas para el numero: " + isdn);
				} catch (Exception e) {
					log.error("Error al guardar bitacora en el sistema: ", e);
					SysMessage.error("Error al guardar bitacora", null);
				}

				String wsdlLoc = Parametros.cugWsdl;

				URL url = new URL(UtilUrl.getIp(wsdlLoc));

				long ini = System.currentTimeMillis();

				if (url.getProtocol().equalsIgnoreCase("https")) {
					int puerto = url.getPort();
					String host = url.getHost();
					if (host.equalsIgnoreCase("localhost")) {
						host = "127.0.0.1";
					}
					validarCertificado(Parametros.cugPathKeystore, host + ":" + puerto);
				}

				NodoServicio<com.huawei.www.bme.cbsinterface.bcservices.BcServicsBindingStub> portNodo = ServicioCallingCircle.initPort(wsdlLoc, "OFERTAS" + login, Parametros.cugTimeOut);

				if (portNodo != null && portNodo.getPort() != null) {

					synchronized (portNodo) {

						try {

							com.huawei.www.bme.cbsinterface.bcservices.BcServicsBindingStub port = portNodo.getPort();

							com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo ownershipInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo();
							ownershipInfo.setBEID(Parametros.cugOwnerShipInfoId);
							com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo accessSecurity = new com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo();
							accessSecurity.setLoginSystemCode(Parametros.cugLoginSystemCode);
							accessSecurity.setPassword(Parametros.cugPassword);
							com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo operatorInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo();
							operatorInfo.setOperatorID(Parametros.cugOperatorId);
							com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat timeFormat = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat();
							timeFormat.setTimeType(Parametros.cugTimeType);

							SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
							String messageSeq = format.format(new Date());

							com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderAdditionalProperty[] additionalProperty = null;
							com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader requestHeader = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader(Parametros.cugVersion, "", messageSeq, ownershipInfo, accessSecurity, operatorInfo, "", "", timeFormat, additionalProperty);

							com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestMsg queryCustomerInfoRequestMsg = new com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestMsg();
							queryCustomerInfoRequestMsg.setRequestHeader(requestHeader);
							com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequest queryCustomerInfoRequest = new com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequest();
							com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestQueryObj queryObj = new com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestQueryObj();

							QueryCustomerInfoRequestQueryObjSubAccessCode subAccessCode = new QueryCustomerInfoRequestQueryObjSubAccessCode();
							subAccessCode.setPrimaryIdentity(isdn);
							queryObj.setSubAccessCode(subAccessCode);
							queryCustomerInfoRequest.setQueryObj(queryObj);
							queryCustomerInfoRequestMsg.setQueryCustomerInfoRequest(queryCustomerInfoRequest);
							com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoResultMsg queryCustomerInfo = port.queryCustomerInfo(queryCustomerInfoRequestMsg);

							long fin = System.currentTimeMillis();
							Long Con = null;
							if (portNodo.isPrimeraVez()) {
								Con = portNodo.getTiempoConexion();
							}
							log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio BCServices: " + (fin - ini) + " milisegundos");

							if (Parametros.saveRequestResponse) {
								try {
									saveXml(isdn, getLogin(), wsdlLoc, "queryCustomerInfo", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoria, Con, (fin - ini));
								} catch (Exception e) {
									log.warn("No se logro registrar los request y response del servicio BCServices, queryCustomerInfo: ", e);
								}
							}

							if (queryCustomerInfo != null && queryCustomerInfo.getResultHeader() != null) {
								if (queryCustomerInfo.getResultHeader().getResultCode().trim().equals("0")) {
									com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoResult queryCustomerInfoResult = queryCustomerInfo.getQueryCustomerInfoResult();

									if (queryCustomerInfoResult != null) {
										QueryCustomerInfoResultSubscriber subscriber = queryCustomerInfoResult.getSubscriber();

										if (subscriber != null) {
											Oferta ofertaPrimaria = new Oferta();

											// ------ OFERTAS PRIMARIAS
											// -------
											POfferingInst primaryOffering = subscriber.getPrimaryOffering();

											if (primaryOffering != null) {

												TreeNode primaria = new DefaultTreeNode(new Oferta("Oferta: Primaria", "", "", "", "", "#e2e2e2"), rootO);
												primaria.setExpanded(true);

												OfferingKey offeringKey = primaryOffering.getOfferingKey();

												if (offeringKey != null) {

													if (offeringKey.getOfferingID() != null) {

														VcOffering offering = getOffering(offeringKey.getOfferingID().longValue() + "");
														if (offering != null && offering.getEnabled() == 1) {
															ofertaPrimaria.setOferta(offering.getName());
															ofertaPrimaria.setTipo(offering.getType());
														} else {
															ofertaPrimaria.setOferta(offeringKey.getOfferingID().toString());
															ofertaPrimaria.setTipo("");
														}
													}
												}
												if (primaryOffering.getStatus() != null) {
													ofertaPrimaria.setEstado(hashStatus.get(primaryOffering.getStatus().trim()));
													if (ofertaPrimaria.getEstado() == null) {
														ofertaPrimaria.setEstado(primaryOffering.getStatus().trim());
													}
												}

												if (primaryOffering.getBundledFlag() != null) {
													ofertaPrimaria.setBundled(hashBundledFlag.get(primaryOffering.getBundledFlag().trim()));
													if (ofertaPrimaria.getBundled() == null) {
														ofertaPrimaria.setBundled(primaryOffering.getBundledFlag().trim());
													}
												}

												if (primaryOffering.getOfferingClass() != null) {
													ofertaPrimaria.setClaseOferta(hashOfferingClass.get(primaryOffering.getOfferingClass().trim()));
													if (ofertaPrimaria.getClaseOferta() == null) {
														ofertaPrimaria.setClaseOferta(primaryOffering.getOfferingClass().trim());
													}
												}
												@SuppressWarnings("unused")
												TreeNode tnPrimaria = new DefaultTreeNode(ofertaPrimaria, primaria);
											} else {
												log.warn("No existe oferta primaria para: " + isdn);
											}

											// -------- OFERTAS SUPLEMENTARIAS
											// ---------

											QueryCustomerInfoResultSubscriberSupplementaryOffering[] supplementaryOffering = subscriber.getSupplementaryOffering();

											if (supplementaryOffering != null && supplementaryOffering.length > 0) {

												TreeNode suplementaria = new DefaultTreeNode(new Oferta("Ofertas: Suplementarias", "", "", "", "", "#e2e2e2"), rootO);
												suplementaria.setExpanded(true);

												for (QueryCustomerInfoResultSubscriberSupplementaryOffering item : supplementaryOffering) {
													Oferta ofertaSuplementaria = new Oferta();

													OfferingKey offeringKey = item.getOfferingKey();
													if (offeringKey != null) {

														if (offeringKey.getOfferingID() != null) {

															VcOffering offering = getOffering(offeringKey.getOfferingID().longValue() + "");
															if (offering != null && offering.getEnabled() == 1) {
																ofertaSuplementaria.setOferta(offering.getName());
																ofertaSuplementaria.setTipo(offering.getType());
															} else {
																ofertaSuplementaria.setOferta(offeringKey.getOfferingID().toString());
																ofertaSuplementaria.setTipo("");
															}
														}
													}

													if (item.getStatus() != null) {
														ofertaSuplementaria.setEstado(hashStatus.get(item.getStatus().trim()));
														if (ofertaSuplementaria.getEstado() == null) {
															ofertaSuplementaria.setEstado(item.getStatus().trim());
														}
													}

													if (item.getBundledFlag() != null) {
														ofertaSuplementaria.setBundled(hashBundledFlag.get(item.getBundledFlag().trim()));
														if (ofertaSuplementaria.getBundled() == null) {
															ofertaSuplementaria.setBundled(item.getBundledFlag().trim());
														}
													}

													if (item.getOfferingClass() != null) {
														ofertaSuplementaria.setClaseOferta(hashOfferingClass.get(item.getOfferingClass().trim()));
														if (ofertaSuplementaria.getClaseOferta() == null) {
															ofertaSuplementaria.setClaseOferta(item.getOfferingClass().trim());
														}
													}
													@SuppressWarnings("unused")
													TreeNode tnSuplementaria = new DefaultTreeNode(ofertaSuplementaria, suplementaria);
													// log.debug(tnSuplementaria.toString());

												}
											} else {
												log.warn("No existen ofertas suplementarias para: " + isdn);
											}

										} else {
											log.warn("Subcriber no encontrado");
											SysMessage.warn("Subcriber no encontrado", null);
											guardarExcepcion(idAuditoria, "Subcriber no encontrado");
										}

									} else {
										log.warn("[isdn: " + getIsdn() + "] queryCustomerInfoResult resuelto a nulo");
									}
								} else {
									log.info("[isdn: " + getIsdn() + "] queryCustomerInfo ResultCode: " + queryCustomerInfo.getResultHeader().getResultCode() + ", ResultDesc: " + queryCustomerInfo.getResultHeader().getResultDesc());
									SysMessage.warn("Servicio BCServices, queryCustomerInfo Code: " + queryCustomerInfo.getResultHeader().getResultCode() + ", Desc: " + queryCustomerInfo.getResultHeader().getResultDesc(), null);
									guardarExcepcion(idAuditoria, "Servicio BCServices, queryCustomerInfo Code: " + queryCustomerInfo.getResultHeader().getResultCode() + ", Desc: " + queryCustomerInfo.getResultHeader().getResultDesc());

								}

							} else {
								if (queryCustomerInfo != null) {
									log.warn("[isdn: " + getIsdn() + "] queryCustomerInfo ResultHeader resuelto a nulo");
								} else {
									log.warn("[isdn: " + getIsdn() + "] queryCustomerInfo queryCustomerInfo resuelto a nulo");
								}
							}

							// INICIO querySubLifeCycle

							com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequest querySubLifeCycleRequest = new com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequest(subAccessCode);
							com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequestMsg querySubLifeCycleRequestMsg = new com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequestMsg(requestHeader, querySubLifeCycleRequest);

							com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleResultMsg querySubLifeCycle = port.querySubLifeCycle(querySubLifeCycleRequestMsg);

							long fin2 = System.currentTimeMillis();
							Long Con2 = null;
							if (portNodo.isPrimeraVez()) {
								Con2 = portNodo.getTiempoConexion();
							}
							log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio BCServices, querySubLifeCycle: " + (fin2 - ini) + " milisegundos");
							if (Parametros.saveRequestResponse) {
								try {
									saveXml(isdn, getLogin(), wsdlLoc, "querySubLifeCycle", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoria, Con2, (fin2 - ini));
								} catch (Exception e) {
									log.warn("No se logro registrar los request y response del servicio BCServices, querySubLifeCycle:", e);
								}
							}

							if (querySubLifeCycle != null && querySubLifeCycle.getResultHeader() != null) {
								if (querySubLifeCycle.getResultHeader().getResultCode().trim().equals("0")) {
									com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleResult querySubLifeCycleResult = querySubLifeCycle.getQuerySubLifeCycleResult();

									if (querySubLifeCycleResult != null) {
										log.debug("querySubLifeCycle.getResultHeader: " + querySubLifeCycle.getResultHeader());
									} else {
										log.warn("[isdn: " + getIsdn() + "] QuerySubLifeCycleResult resuelto a nulo");
									}
								} else {
									log.info("[isdn: " + getIsdn() + "] querySubLifeCycle ResultCode: " + querySubLifeCycle.getResultHeader().getResultCode() + ", ResultDesc: " + querySubLifeCycle.getResultHeader().getResultDesc());
									SysMessage.warn("Servicio BCServices, querySubLifeCycle Code: " + querySubLifeCycle.getResultHeader().getResultCode() + ", Desc: " + querySubLifeCycle.getResultHeader().getResultDesc(), null);
									guardarExcepcion(idAuditoria, "Servicio BCServices, querySubLifeCycle Code: " + querySubLifeCycle.getResultHeader().getResultCode() + ", Desc: " + querySubLifeCycle.getResultHeader().getResultDesc());
								}
							} else {
								if (querySubLifeCycle != null) {
									log.warn("[isdn: " + getIsdn() + "] querySubLifeCycle ResultHeader resuelto a nulo");
								} else {
									log.warn("[isdn: " + getIsdn() + "] querySubLifeCycle QuerySubLifeCycle resuelto a nulo");
								}
							}

							// FIN

						} finally {
							portNodo.setEnUso(false);
							portNodo.setFechaFin(new Date());
						}
					}

				} else {
					log.error("[isdn: " + getIsdn() + "] Port BCServices resuelto a nulo");
				}

				SysMessage.info("Consulta finalizada.", null);
			} else {
				SysMessage.warn("Nro de Cuenta no valido", null);
			}
		} catch (AxisFault a) {
			String msg = a.getMessage();
			msg = msg.toLowerCase();
			if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase()) || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase()) || msg.contains("UnknownHostException".toLowerCase()) || msg.contains("No route to host".toLowerCase())
					|| msg.contains("Connection refused".toLowerCase())) {
				log.error("Error de conexion al servicio BCServices Oferta: ", a);
				SysMessage.error("Error de conexion del servicio BCServices", null);
				guardarExcepcion(idAuditoria, "Error de conexion del servicio BCServices: " + stackTraceToString(a));
			} else {
				log.error("Error AxisFault Oferta: ", a);
				SysMessage.error("Error AxisFault de conexion del servicio BCServices", null);
				guardarExcepcion(idAuditoria, "Error AxisFault de conexion del servicio BCServices: " + stackTraceToString(a));
			}

		} catch (ServiceException e) {
			log.error("Error de servicio al conectarse al servicio BCServices Oferta: ", e);
			SysMessage.error("Error de servicio al conectarse al servicio BCServices", null);
			guardarExcepcion(idAuditoria, "Error de servicio al conectarse al servicio BCServices: " + stackTraceToString(e));
		} catch (RemoteException e) {
			log.error("Error remoto al conectarse al servicio BCServices Oferta: ", e);
			SysMessage.error("Error remoto al conectarse al servicio BCServices", null);
			guardarExcepcion(idAuditoria, "Error remoto al conectarse al servicio BCServices: " + stackTraceToString(e));
		} catch (Exception e) {
			log.error("Error al consultar Ofertas: ", e);
			SysMessage.error("Error al consultar", null);
			guardarExcepcion(idAuditoria, "Error al consultar: " + stackTraceToString(e));
		}
		// Consulta QuerySubLifeCycle

	}

	private VcOffering getOffering(String id) {
		try {
			Object find = blOffering.find(id, VcOffering.class);
			if (find != null)
				return (VcOffering) find;
		} catch (Exception e) {
			log.error("Error al obtener offering: ", e);
		}
		return null;
	}

	private void cargarHashPaymentMode() {
		try {
			hashPaymentMode = new HashMap<String, String>();
			String states = Parametros.ofertaPaymentMode;
			StringTokenizer st = new StringTokenizer(states, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				try {
					StringTokenizer stt = new StringTokenizer(nextToken.trim(), ":");
					String key = stt.nextToken();
					String value = stt.nextToken();
					hashPaymentMode.put(key.trim(), value.trim());
				} catch (Exception e) {
					e.getMessage();
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash Payment Mode: ", e);
			SysMessage.error("Error al cargar hash Payment Mode: " + e.getMessage(), null);
		}
	}

	private void cargarHashStatus() {
		try {
			hashStatus = new HashMap<String, String>();
			String states = Parametros.ofertaStatus;
			StringTokenizer st = new StringTokenizer(states, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				try {
					StringTokenizer stt = new StringTokenizer(nextToken.trim(), ":");
					String key = stt.nextToken();
					String value = stt.nextToken();
					hashStatus.put(key.trim(), value.trim());
				} catch (Exception e) {
					e.getMessage();
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash Status: ", e);
			SysMessage.error("Error al cargar hash Status: " + e.getMessage(), null);
		}
	}

	private void cargarHashBundledFlag() {
		try {
			hashBundledFlag = new HashMap<String, String>();
			String states = Parametros.ofertaBundledFlag;
			StringTokenizer st = new StringTokenizer(states, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				try {
					StringTokenizer stt = new StringTokenizer(nextToken.trim(), ":");
					String key = stt.nextToken();
					String value = stt.nextToken();
					hashBundledFlag.put(key.trim(), value.trim());
				} catch (Exception e) {
					e.getMessage();
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash Bundled Flag: ", e);
			SysMessage.error("Error al cargar hash Bundled Flag: " + e.getMessage(), null);
		}
	}

	private void cargarHashOfferingClass() {
		try {
			hashOfferingClass = new HashMap<String, String>();
			String states = Parametros.ofertaOfferingClass;
			StringTokenizer st = new StringTokenizer(states, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				try {
					StringTokenizer stt = new StringTokenizer(nextToken.trim(), ":");
					String key = stt.nextToken();
					String value = stt.nextToken();
					hashOfferingClass.put(key.trim(), value.trim());
				} catch (Exception e) {
					e.getMessage();
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash Offering Class: ", e);
			SysMessage.error("Error al cargar hash Offering Class: " + e.getMessage(), null);
		}
	}

	private void cargarHashStatusDetail(String path) {
		try {
			hashStatusDetail = new HashMap<String, String>();
			ArrayList<StatusDetail> list = (new ToXml()).readObject(path);
			if (list != null && !list.isEmpty()) {
				for (StatusDetail sd : list) {
					hashStatusDetail.put(sd.getStatus() + "|" + sd.getRblacklistStatus() + "|" + sd.getCurrentStatusIndex(), sd.getDetail());
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash Status detail: ", e);
			SysMessage.error("Error al cargar hash Status detail: " + e.getMessage(), null);
		}
	}

	private void cargarListPosition() {
		try {
			listPositions = new ArrayList<>();
			String states = Parametros.ofertaStatusDetailPositions;
			StringTokenizer st = new StringTokenizer(states, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				if (nextToken != null)
					listPositions.add(Integer.parseInt(nextToken.trim()));
			}
		} catch (Exception e) {
			log.error("Error al cargar lista de posiciones: ", e);
			SysMessage.error("Error al cargar lista de posiciones: " + e.getMessage(), null);
		}
	}

	private void cargarHashDaysExpired(String path) {
		try {
			hashDaysExpired = new HashMap<String, String>();
			ArrayList<DaysExpired> list = (new ToXml()).readObject(path);
			if (list != null && !list.isEmpty()) {
				for (DaysExpired sd : list) {
					hashDaysExpired.put(sd.getBilletera(), sd.getDaysExpired());
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash Days Expired: ", e);
			SysMessage.error("Error al cargar hash Days Expired: " + e.getMessage(), null);
		}
	}

	private void cargarListBilleteraExpired() {
		try {
			listBilleteraExpired = new ArrayList<>();
			String states = Parametros.billeteraExpired;
			StringTokenizer st = new StringTokenizer(states, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				if (nextToken != null)
					listBilleteraExpired.add(String.valueOf(nextToken.trim()));
			}
		} catch (Exception e) {
			log.error("Error al cargar lista de billeteras expiradas: ", e);
			SysMessage.error("Error al cargar lista de billeteras expiradas: " + e.getMessage(), null);
		}
	}

	public void onTabChange(TabChangeEvent event) {
		title = event.getTab().getTitle();
		String id = event.getTab().getId();
		title = id;
		CodigoAuditoria = 0;
		disableIncidente = true;
		vistaIncidente = null;
		log.debug("tab = " + title);

		if (id.equals("tab1")) { // HISTORIAL TRANSACCIONES

			all = false;
			cargarCheckbox();

			try {
				// AccountHistory ah = new AccountHistory();
				// ah.eliminar("WHERE " + AccountHistory._ISDN + " = '" + isdn +
				// "' AND " + AccountHistory._USUARIO + " = '" + getLogin() +
				// "'");
				blAccountHistory.eliminarHistorial(isdn, login);
			} catch (Exception e) {
				log.error("Error al eliminar registros temporales, isdn: " + isdn + ", login: " + getLogin(), e);
			}

			voucher = new Voucher();
			listVoucherOperation = new ArrayList<SimpleVoucherOperation>();

			callingCircles = new ArrayList<>();
			model = new LazyCarDataModel(new ArrayList<History>(), isdn, getLogin());
			listBalanceDetail = new ArrayList<>();
			propertys = new ArrayList<>();
		}
		if (id.equals("tab2")) { // BILLETERAS

			voucher = new Voucher();
			listVoucherOperation = new ArrayList<SimpleVoucherOperation>();

			callingCircles = new ArrayList<>();
			model = new LazyCarDataModel(new ArrayList<History>(), isdn, getLogin());
			listBalanceDetail = new ArrayList<>();
			propertys = new ArrayList<>();

			cargarBilleteras();
		}
		if (id.equals("tab3")) { // OFERTAS
			voucher = new Voucher();
			listVoucherOperation = new ArrayList<SimpleVoucherOperation>();

			callingCircles = new ArrayList<>();
			model = new LazyCarDataModel(new ArrayList<History>(), isdn, getLogin());
			listBalanceDetail = new ArrayList<>();
			propertys = new ArrayList<>();

			cargarOferta();
		}
		if (id.equals("tab4")) { // FAVORITOS
			voucher = new Voucher();
			listVoucherOperation = new ArrayList<SimpleVoucherOperation>();

			callingCircles = new ArrayList<>();
			model = new LazyCarDataModel(new ArrayList<History>(), isdn, getLogin());
			listBalanceDetail = new ArrayList<>();
			propertys = new ArrayList<>();

			CargarFavorito();
		}
		if (id.equals("tab5")) { // CUG
			// <p:ajax event="tabChange" listener="#{ccwsBean.onTabChange}"
			// onstart="PF('dialogTab').show();"
			// oncomplete="PF('dialogTab').hide();" process="@this"
			// update=":form_tab:isdn" />
			// RequestContext context = RequestContext.getCurrentInstance();
			// context.execute("PF('dialogCug').show();");

			// RequestContext.getCurrentInstance().openDialog("dialogCug");
			// RequestContext.getCurrentInstance().execute("dialogCug.show()");

			voucher = new Voucher();
			listVoucherOperation = new ArrayList<SimpleVoucherOperation>();

			callingCircles = new ArrayList<>();
			model = new LazyCarDataModel(new ArrayList<History>(), isdn, getLogin());
			listBalanceDetail = new ArrayList<>();
			propertys = new ArrayList<>();

			cargarCug();
			// context.execute("PF('dialogCug').hide();");
		}
		if (id.equals("tab6")) { // CALLING CIRCLE
			// saveLog(this.TAB_CALLING_CIRCLE, "", isdn, this.PLATAFORMA,
			// login);
			circleName = "";
			voucher = new Voucher();
			listVoucherOperation = new ArrayList<SimpleVoucherOperation>();

			callingCircles = new ArrayList<>();
			model = new LazyCarDataModel(new ArrayList<History>(), isdn, getLogin());
			listBalanceDetail = new ArrayList<>();
			propertys = new ArrayList<>();
		}
		if (id.equals("tab7")) { // VOUCHER
			// saveLog(this.TAB_VOUCHER, "", isdn, this.PLATAFORMA, login);
			batchNumber = "";
			serialNumber = "";
			voucher = new Voucher();
			listVoucherOperation = new ArrayList<SimpleVoucherOperation>();

			callingCircles = new ArrayList<>();
			model = new LazyCarDataModel(new ArrayList<History>(), isdn, getLogin());
			listBalanceDetail = new ArrayList<>();
			propertys = new ArrayList<>();
		}
	}

	public void paginar() {
		log.debug("Ingresando a paginar....");
		model = new LazyCarDataModel(new ArrayList<History>(), isdn, getLogin());
	}

	public class LazyCarDataModel extends LazyDataModel<History> {
		private static final long serialVersionUID = 1L;

		private List<History> datasource;
		private String lazyIsdn;
		private String lazyLogin;

		public LazyCarDataModel(List<History> datasource, String lazyIsdn, String lazyLogin) {
			this.datasource = datasource;
			this.lazyIsdn = lazyIsdn;
			this.lazyLogin = lazyLogin;
		}

		@Override
		public History getRowData(String rowKey) {
			for (History item : datasource) {
				if (item.getId().equals(rowKey))
					return item;
			}

			return null;
		}

		@Override
		public Object getRowKey(History item) {
			return item.getId();
		}

		@Override
		public List<History> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
			// log.info("first:" + first + " pageSize:" + pageSize + "
			// paramString:" + sortField + " sortOrder:" + sortOrder.toString()
			// + " paramMap:" + filters.toString());

			// datasource = accountHistoryDao.findByPage(first, pageSize,
			// sortField, sortOrder, filters, lazyIsdn, lazyLogin);
			// getModel().setRowCount(accountHistoryDao.count(first, pageSize,
			// sortField, sortOrder, filters, lazyIsdn, lazyLogin));

			datasource = blAccountHistory.findByPage(first, pageSize, sortField, sortOrder, filters, lazyIsdn, lazyLogin);
			getModel().setRowCount(blAccountHistory.count(first, pageSize, sortField, sortOrder, filters, lazyIsdn, lazyLogin));
			return datasource;
		}

		public List<History> getDatasource() {
			return datasource;
		}

		public void setDatasource(List<History> datasource) {
			this.datasource = datasource;
		}
	}

	public void refresh() {
		// log.debug(".......refresh, PollStop: " + controlButton.getPollStop()
		// + ", disableButton: " + controlButton.getDisabled());
		// log.debug(".......controlButton.getFinalizado: " +
		// controlButton.getFinalizado() + ", disableButton: " + disableButton);
		try {
			if (controlButton.getFinalizado()) {
				RequestContext reqCtx = RequestContext.getCurrentInstance();
				reqCtx.execute("PF('poll').stop();");
				disableButton = false;
				// log.debug("+++++++++++++++++++++++++++++++++++ stop poll");
				SysMessage.info("Consulta finalizada", null);
				if (controlButton.getErrores() != null && controlButton.getErrores().size() > 0) {
					List<String> errores = controlButton.getErrores();
					for (String error : errores) {
						SysMessage.error(error, null);
					}
					controlButton.setErrores(null);
				}
			}
		} catch (Exception e) {
			log.error("Error: " + e.getMessage());
		}
	}

	public void refreshTelnet() {
		// log.debug(".......refresh, PollStop: " + controlButton.getPollStop()
		// + ", disableButton: " + controlButton.getDisabled());
		// log.debug(".......controlButton.getFinalizado: " +
		// controlButton.getFinalizado() + ", disableButton: " + disableButton);
		// log.debug("Refresh Telnet: ");
		try {
			if (controlButtonTelnet.getFinalizado()) {
				RequestContext reqCtx = RequestContext.getCurrentInstance();
				reqCtx.execute("PF('pollTelnet').stop();");
				disableButton = false;
				// log.debug("+++++++++++++++++++++++++++++++++++ stop poll");
				SysMessage.info("Consulta finalizada", null);
				if (controlButtonTelnet.getErrores() != null && controlButtonTelnet.getErrores().size() > 0) {
					List<String> errores = controlButtonTelnet.getErrores();
					for (String error : errores) {
						SysMessage.warn(error, null);
					}
					controlButtonTelnet.setErrores(null);
				}
			}
		} catch (Exception e) {
			log.error("Error: " + e.getMessage());
		}
	}

	private boolean existeCheckboxSeleccionado(String label) {
		if (listCheckboxSelected != null) {
			for (Checkbox reg : listCheckboxSelected) {
				if (reg.getLabel().trim().equals(label.trim()))
					return true;
			}
		}
		return false;
	}

	public void validarIsdn() {
		log.debug("ingresando a validar nro: " + isdn);
		try {
			if (readInput) {
				return;
			}
			if (nro != null) {
				if (UtilNumber.esNroTigo(nro)) {
					isdn = nro;
					setDisableTab(false);
					disableButton = false;
					readInput = true;
					if (disabledRadioBetweenDay) {
						disabledRadioDay = false;
					} else {
						disabledRadioDay = true;
					}
					// FacesContext.getCurrentInstance().addMessage("messageCuenta",
					// new FacesMessage(FacesMessage.SEVERITY_INFO, "Info!",
					// "Isdn valido"));
					SysMessage.info("Isdn valido", null);
					// Cargar datos suscriber
					CargarSubscriber();

					if (title != null && title.equals("tab2")) { // BILLETERAS
						cargarBilleteras();
					}
					if (title != null && title.equals("tab3")) { // OFERTA
						cargarOferta();
					}
					if (title != null && title.equals("tab4")) { // FAVORITO
						CargarFavorito();
					}
					if (title != null && title.equals("tab5")) { // CUG
						cargarCug();
					}
				} else {
					isdn = "";
					disableButton = true;
					setDisableTab(true);
					// FacesContext.getCurrentInstance().addMessage("messageCuenta",
					// new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!",
					// "Isdn no valido"));
					SysMessage.warn("Isdn no valido", null);
				}

			} else {
				isdn = "";
				disableButton = true;
				setDisableTab(true);
				// FacesContext.getCurrentInstance().addMessage("messageCuenta",
				// new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!",
				// "Isdn no valido"));
				SysMessage.warn("Isdn no valido", null);
			}

		} catch (Exception e) {
			log.error("Error al validar isdn: ", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void lanzarHilos() {
		log.debug("Ingresando a lanzar hilos ..... isdn: " + isdn + ", login: " + login);

		try {
			model = new LazyCarDataModel(new ArrayList<History>(), isdn, getLogin());
			propertys = new ArrayList<>();
			listBalanceDetail = new ArrayList<>();

			try {
				blAccountHistory.eliminarHistorial(isdn, login);
			} catch (Exception e) {
				log.error("Error al eliminar registros temporales, isdn: " + isdn + ", login: " + getLogin(), e);
			}

			StringBuilder sb = new StringBuilder();
			if (listCheckboxSelected != null && listCheckboxSelected.length > 0) {
				for (Checkbox ch : listCheckboxSelected) {
					sb.append(" - " + ch.getLabel());
				}
			}
			log.debug("valueRadioDay " + valueRadioDay + ", valueRadioBetweenDay: " + valueRadioBetweenDay + ", listCheckboxSelected: " + sb.toString());

			if (listCheckboxSelected == null || listCheckboxSelected.length == 0) {
				SysMessage.warn("Seleccione por lo menos un History Type", null);
				return;
			}
			Calendar fechaInicio = Calendar.getInstance();
			Calendar fechaFin = Calendar.getInstance();
			Calendar fechaActual = Calendar.getInstance();

			if (isdn == null || isdn.trim().isEmpty()) {
				SysMessage.warn("Isdn no válido", null);
				return;
			}
			if (login == null || login.trim().isEmpty()) {
				SysMessage.warn("Usuario no válido", null);
				return;
			}
			if (valueRadioDay.equals("History For")) {
				if (daySelected != null && daySelected.intValue() <= Parametros.historialForDays) {
					fechaInicio.add(Calendar.DATE, -daySelected.intValue());

					fechaInicio.set(Calendar.HOUR_OF_DAY, 0);
					fechaInicio.set(Calendar.MINUTE, 0);
					fechaInicio.set(Calendar.SECOND, 0);
					fechaInicio.set(Calendar.MILLISECOND, 0);

					fechaFin.set(Calendar.HOUR_OF_DAY, 23);
					fechaFin.set(Calendar.MINUTE, 59);
					fechaFin.set(Calendar.SECOND, 59);
					fechaFin.set(Calendar.MILLISECOND, 0);

				} else {
					SysMessage.warn("Solo puede consultar de los ultimos " + Parametros.historialForDays + " dias", null);
					return;
				}
			} else {
				if (valueRadioBetweenDay.equals("Between Day") && (fromDate == null || toDate == null)) {
					SysMessage.warn("Fechas no válidas", null);
					return;
				} else {
					fechaInicio.setTimeInMillis(fromDate.getTime());

					fechaFin.setTimeInMillis(toDate.getTime());

					if (fechaFin.getTimeInMillis() < fechaInicio.getTimeInMillis()) {
						SysMessage.warn("La fecha From tiene que ser menor o igual que la fecha To", null);
						return;
					}

					fechaActual.add(Calendar.DATE, -Parametros.historialBetweenDay);

					fechaInicio.set(Calendar.HOUR_OF_DAY, 0);
					fechaInicio.set(Calendar.MINUTE, 0);
					fechaInicio.set(Calendar.SECOND, 0);
					fechaInicio.set(Calendar.MILLISECOND, 0);

					fechaFin.set(Calendar.HOUR_OF_DAY, 0);
					fechaFin.set(Calendar.MINUTE, 0);
					fechaFin.set(Calendar.SECOND, 0);
					fechaFin.set(Calendar.MILLISECOND, 0);

					fechaActual.set(Calendar.HOUR_OF_DAY, 0);
					fechaActual.set(Calendar.MINUTE, 0);
					fechaActual.set(Calendar.SECOND, 0);
					fechaActual.set(Calendar.MILLISECOND, 0);

					fechaFin.set(Calendar.HOUR_OF_DAY, 23);
					fechaFin.set(Calendar.MINUTE, 59);
					fechaFin.set(Calendar.SECOND, 59);
					fechaFin.set(Calendar.MILLISECOND, 0);

					log.debug("fechaActual: " + UtilDate.dateToString(fechaActual.getTime(), Parametros.fechaFormatWs));

					if (!(fechaInicio.getTimeInMillis() > fechaActual.getTimeInMillis() && fechaFin.getTimeInMillis() > fechaActual.getTimeInMillis())) {
						SysMessage.warn("Solo puede consultar los ultimos " + Parametros.historialBetweenDay + " dias", null);
						return;
					}
					Calendar hoy = Calendar.getInstance();
					hoy.set(Calendar.HOUR_OF_DAY, 23);
					hoy.set(Calendar.MINUTE, 59);
					hoy.set(Calendar.SECOND, 59);
					hoy.set(Calendar.MILLISECOND, 0);

					if (fechaFin.getTimeInMillis() > hoy.getTimeInMillis()) {
						SysMessage.warn("Fecha Fin no válido", null);
						return;
					}
				}
			}

			String startDate = UtilDate.dateToString(fechaInicio.getTime(), Parametros.fechaFormatWs);
			String endDate = UtilDate.dateToString(fechaFin.getTime(), Parametros.fechaFormatWs);

			log.debug("...... FECHA INICIO: " + startDate + ", FECHA FIN: " + endDate);

			int nroHilos = 0;
			List<VcHilo> lista = blHilo.findAll();

			// List<Hilo> lista = new Hilo().lista("WHERE " + Hilo._ACTIVO + " =
			// 'Y'");

			int comando = 0;
			if (lista != null) {
				for (VcHilo hilo : lista) {

					// if
					// (hashCheckBox.containsKey(hilo.getName_checkbox().trim()))
					// {
					if (existeCheckboxSeleccionado(hilo.getNameCheckbox().trim())) {
						nroHilos++;
						comando = comando + hilo.getComando();
					}
				}

			}
			log.debug("nroHilos: " + nroHilos + ", totalComando: " + comando);
			// AccountHistory ah = new AccountHistory();
			// ah.eliminar("WHERE " + AccountHistory._ISDN + " = '" + isdn + "'
			// AND " + AccountHistory._USUARIO + " = '" + getLogin() + "'");

			try {
				blAccountHistory.eliminarHistorial(isdn, login);
			} catch (Exception e) {
				log.error("Error al eliminar registros temporales, isdn: " + isdn + ", login: " + getLogin(), e);
			}

			boolean conIteracion = Parametros.historialConIteracion;
			if (nroHilos <= 0) {
				return;
			}

			List<VcLogAdicional> adicionales = new ArrayList<>();
			VcLogAdicional ad1 = new VcLogAdicional();
			ad1.setCodigo("Fecha Inicio");
			ad1.setValor(UtilDate.dateToString(fechaInicio.getTime(), Parametros.fechaFormatPage));
			adicionales.add(ad1);

			VcLogAdicional ad2 = new VcLogAdicional();
			ad2.setCodigo("Fecha Fin");
			ad2.setValor(UtilDate.dateToString(fechaFin.getTime(), Parametros.fechaFormatPage));
			adicionales.add(ad2);

			idAuditoriaHistorial = guardarAuditoria(Calendar.getInstance(), this.login, this.ip, Parametros.vistaHistorial, comando, this.isdn, adicionales);

			if (idAuditoriaHistorial <= 0) {
				SysMessage.error("Error al guardar log de auditoria", null);
				return;
			}

			disableIncidente = false;
			vistaIncidente = DescriptorBitacora.HISTORIAL;
			try {
				controlerBitacora.accion(DescriptorBitacora.HISTORIAL, "Se hizo consulta de Historial, isdn: " + isdn + ", fecha inicio: " + ad1.getValor() + ", fecha fin: " + ad2.getValor());
			} catch (Exception e) {
				log.error("Error al guardar bitacora en el sistema: ", e);
				SysMessage.error("Error al guardar bitacora", null);
			}

			disableButton = true;

			// Inicio Consulta al QueryCustomerInfo y al QuerySublifeCycle

			try {

				String wsdlLoc = Parametros.cugWsdl;

				URL url = new URL(UtilUrl.getIp(wsdlLoc));

				long ini = System.currentTimeMillis();

				if (url.getProtocol().equalsIgnoreCase("https")) {
					int puerto = url.getPort();
					String host = url.getHost();
					if (host.equalsIgnoreCase("localhost")) {
						host = "127.0.0.1";
					}
					validarCertificado(Parametros.cugPathKeystore, host + ":" + puerto);
				}

				NodoServicio<com.huawei.www.bme.cbsinterface.bcservices.BcServicsBindingStub> portNodo = ServicioCallingCircle.initPort(wsdlLoc, "CCC" + login, Parametros.cugTimeOut);

				if (portNodo != null && portNodo.getPort() != null) {

					synchronized (portNodo) {

						try {

							com.huawei.www.bme.cbsinterface.bcservices.BcServicsBindingStub port = portNodo.getPort();

							com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo ownershipInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo();
							ownershipInfo.setBEID(Parametros.cugOwnerShipInfoId);
							com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo accessSecurity = new com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo();
							accessSecurity.setLoginSystemCode(Parametros.cugLoginSystemCode);
							accessSecurity.setPassword(Parametros.cugPassword);
							com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo operatorInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo();
							operatorInfo.setOperatorID(Parametros.cugOperatorId);
							com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat timeFormat = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat();
							timeFormat.setTimeType(Parametros.cugTimeType);

							SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
							String messageSeq = format.format(new Date());

							com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderAdditionalProperty[] additionalProperty = null;
							com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader requestHeader = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader(Parametros.cugVersion, "", messageSeq, ownershipInfo, accessSecurity, operatorInfo, "", "", timeFormat, additionalProperty);

							com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestMsg queryCustomerInfoRequestMsg = new com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestMsg();
							queryCustomerInfoRequestMsg.setRequestHeader(requestHeader);
							com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequest queryCustomerInfoRequest = new com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequest();
							com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestQueryObj queryObj = new com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestQueryObj();

							QueryCustomerInfoRequestQueryObjSubAccessCode subAccessCode = new QueryCustomerInfoRequestQueryObjSubAccessCode();
							subAccessCode.setPrimaryIdentity(isdn);
							queryObj.setSubAccessCode(subAccessCode);
							queryCustomerInfoRequest.setQueryObj(queryObj);
							queryCustomerInfoRequestMsg.setQueryCustomerInfoRequest(queryCustomerInfoRequest);
							com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoResultMsg queryCustomerInfo = port.queryCustomerInfo(queryCustomerInfoRequestMsg);

							long fin = System.currentTimeMillis();
							Long Con = null;
							if (portNodo.isPrimeraVez()) {
								Con = portNodo.getTiempoConexion();
							}
							log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio BCServices: " + (fin - ini) + " milisegundos");

							if (Parametros.saveRequestResponse) {
								try {
									saveXml(isdn, getLogin(), wsdlLoc, "queryCustomerInfo", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoriaHistorial, Con, (fin - ini));
								} catch (Exception e) {
									log.warn("No se logro registrar los request y response del servicio BCServices, queryCustomerInfo:", e);
								}
							}

							if (queryCustomerInfo != null && queryCustomerInfo.getResultHeader() != null) {
								if (queryCustomerInfo.getResultHeader().getResultCode().trim().equals("0")) {
									com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoResult queryCustomerInfoResult = queryCustomerInfo.getQueryCustomerInfoResult();

									if (queryCustomerInfoResult != null) {
										log.debug("queryCustomerInfo.getResultHeader: " + queryCustomerInfo.getResultHeader());
									} else {
										log.warn("[isdn: " + getIsdn() + "] queryCustomerInfoResult resuelto a nulo");
									}
								} else {
									log.info("[isdn: " + getIsdn() + "] queryCustomerInfo ResultCode: " + queryCustomerInfo.getResultHeader().getResultCode() + ", ResultDesc: " + queryCustomerInfo.getResultHeader().getResultDesc());
									SysMessage.warn("Servicio BCServices, queryCustomerInfo Code: " + queryCustomerInfo.getResultHeader().getResultCode() + ", Desc: " + queryCustomerInfo.getResultHeader().getResultDesc(), null);
									guardarExcepcion(idAuditoriaHistorial, "Servicio BCServices, queryCustomerInfo Code: " + queryCustomerInfo.getResultHeader().getResultCode() + ", Desc: " + queryCustomerInfo.getResultHeader().getResultDesc());
								}
							} else {
								if (queryCustomerInfo != null) {
									log.warn("[isdn: " + getIsdn() + "] queryCustomerInfo ResultHeader resuelto a nulo");
								} else {
									log.warn("[isdn: " + getIsdn() + "] queryCustomerInfo queryCustomerInfo resuelto a nulo");
								}
							}

							// INICIO querySubLifeCycle

							com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequest querySubLifeCycleRequest = new com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequest(subAccessCode);
							com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequestMsg querySubLifeCycleRequestMsg = new com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequestMsg(requestHeader, querySubLifeCycleRequest);

							com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleResultMsg querySubLifeCycle = port.querySubLifeCycle(querySubLifeCycleRequestMsg);

							long fin2 = System.currentTimeMillis();
							Long Con2 = null;
							if (portNodo.isPrimeraVez()) {
								Con2 = portNodo.getTiempoConexion();
							}
							log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio BCServices, querySubLifeCycle: " + (fin2 - ini) + " milisegundos");
							if (Parametros.saveRequestResponse) {
								try {
									saveXml(isdn, getLogin(), wsdlLoc, "querySubLifeCycle", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoriaHistorial, Con2, (fin2 - ini));
								} catch (Exception e) {
									log.warn("No se logro registrar los request y response del servicio BCServices, querySubLifeCycle:", e);
								}
							}

							if (querySubLifeCycle != null && querySubLifeCycle.getResultHeader() != null) {
								if (querySubLifeCycle.getResultHeader().getResultCode().trim().equals("0")) {
									com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleResult querySubLifeCycleResult = querySubLifeCycle.getQuerySubLifeCycleResult();

									if (querySubLifeCycleResult != null) {
										log.debug("querySubLifeCycle.getResultHeader: " + querySubLifeCycle.getResultHeader());
									} else {
										log.warn("[isdn: " + getIsdn() + "] QuerySubLifeCycleResult resuelto a nulo");
									}
								} else {
									log.info("[isdn: " + getIsdn() + "] querySubLifeCycle ResultCode: " + querySubLifeCycle.getResultHeader().getResultCode() + ", ResultDesc: " + querySubLifeCycle.getResultHeader().getResultDesc());
									SysMessage.warn("Servicio BCServices, querySubLifeCycle Code: " + querySubLifeCycle.getResultHeader().getResultCode() + ", Desc: " + querySubLifeCycle.getResultHeader().getResultDesc(), null);
									guardarExcepcion(idAuditoriaHistorial, "Servicio BCServices, querySubLifeCycle Code: " + querySubLifeCycle.getResultHeader().getResultCode() + ", Desc: " + querySubLifeCycle.getResultHeader().getResultDesc());
								}
							} else {
								if (querySubLifeCycle != null) {
									log.warn("[isdn: " + getIsdn() + "] querySubLifeCycle ResultHeader resuelto a nulo");
								} else {
									log.warn("[isdn: " + getIsdn() + "] querySubLifeCycle QuerySubLifeCycle resuelto a nulo");
								}
							}

							// FIN

						} finally {
							portNodo.setEnUso(false);
							portNodo.setFechaFin(new Date());
						}
					}

				} else {
					log.error("[isdn: " + getIsdn() + "] Port BCServices resuelto a nulo");
				}

			} catch (

					AxisFault a) {
				String msg = a.getMessage();
				msg = msg.toLowerCase();
				if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase()) || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase()) || msg.contains("UnknownHostException".toLowerCase()) || msg.contains("No route to host".toLowerCase())
						|| msg.contains("Connection refused".toLowerCase())) {
					log.error("Error de conexion al servicio BCServices Subscriber: ", a);
					SysMessage.error("Error de conexion del servicio BCServices", null);
					guardarExcepcion(idAuditoriaHistorial, "Error de conexion del servicio BCServices: " + stackTraceToString(a));
				} else {
					log.error("Error AxisFault Subscriber: ", a);
					SysMessage.error("Error AxisFault de conexion del servicio BCServices", null);
					guardarExcepcion(idAuditoriaHistorial, "Error AxisFault de conexion del servicio BCServices: " + stackTraceToString(a));
				}
			} catch (ServiceException e) {
				log.error("Error de servicio al conectarse al servicio BCServices Subscriber: ", e);
				SysMessage.error("Error de servicio al conectarse al servicio BCServices", null);
				guardarExcepcion(idAuditoriaHistorial, "Error de servicio al conectarse al servicio BCServices: " + stackTraceToString(e));
			} catch (RemoteException e) {
				log.error("Error remoto al conectarse al servicio BCServices Subscriber: ", e);
				SysMessage.error("Error remoto al conectarse al servicio BCServices", null);
				guardarExcepcion(idAuditoriaHistorial, "Error remoto al conectarse al servicio BCServices: " + stackTraceToString(e));
			} catch (Exception e) {
				log.error("Error al consultar Subscriber", e);
				SysMessage.error("Error al consultar", null);
				guardarExcepcion(idAuditoriaHistorial, "Error al consultar: " + stackTraceToString(e));
			}

			// Fin Consulta al QueryCustomerInfo y al QuerySublifeCycle

			try {
				RequestContext reqCtx = RequestContext.getCurrentInstance();
				reqCtx.execute("PF('poll').start();");
			} catch (Exception e) {
				log.error("Error: " + e.getMessage());
			}

			Control control = new Control();
			control.setNroThreads(nroHilos);
			control.setNroFinalizadosPrimeraIteracion(nroHilos);
			control.setTotalCdrNum(new BigInteger(Parametros.historialTotalCdrNum));
			control.setFetchRowNum(new BigInteger(Parametros.historialFetchRowNum));
			control.setIdAuditoria(idAuditoriaHistorial);

			controlButton.setDisabled(true);
			controlButton.setPollStop(false);
			controlButton.setFinalizado(false);
			controlButton.setErrores(new ArrayList<String>());

			for (VcHilo hilo : lista) {

				if (existeCheckboxSeleccionado(hilo.getNameCheckbox().trim())) {
					log.debug(hilo.getNameType());

					if (hilo.getNameHistory() != null && hilo.getNameHistory().trim().equals("QUERY_CDR")) {

						ImportadorCDR at = new ImportadorCDR(hilo.getIdHilo(), control, controlButton);
						at.setIsdn(isdn);
						at.setLogin(getLogin());
						at.setFechaInicio(startDate);
						at.setFechaFin(endDate);
						at.setHashFlowType(hashFlowType);
						at.setHashServiceCategory(hashServiceCategory);
						at.setHashServiceType(hashServiceType);
						at.setHashRoamFlag(hashRoamFlag);
						at.setHashSpecialNumber(hashSpecialNumber);
						at.setBlConsulta(blConsulta);
						at.setBlAccountHistory(blAccountHistory);
						at.setBlAuditoria(blAuditoria);
						at.setBlHilo(blHilo);
						at.setBlExcepcion(blExcepcion);

						at.setServiceCategory(null);
						if (hilo.getServiceCategory() != null) {
							if (!hilo.getServiceCategory().trim().isEmpty())
								at.setServiceCategory(hilo.getServiceCategory().trim());
						}

						at.setFlowType(null);
						if (hilo.getFlowType() != null) {
							if (!hilo.getFlowType().trim().isEmpty())
								at.setFlowType(hilo.getFlowType().trim());
						}

						at.start();
						log.debug("idThread: " + hilo.getIdHilo() + " - " + hilo.getNameType() + " - " + hilo.getNameCheckbox() + " iniciado !!");
					}
					if (hilo.getNameHistory() != null && hilo.getNameHistory().trim().equals("ADJUSTMENT")) {
						ImportadorAjustes at = new ImportadorAjustes(hilo.getIdHilo(), control, controlButton);
						at.setIsdn(isdn);
						at.setLogin(getLogin());
						at.setFechaInicio(startDate);
						at.setFechaFin(endDate);
						at.setHashAdjustmentChannelId(hashAdjustmentChannelId);
						at.setBlConsulta(blConsulta);
						at.setBlAccountHistory(blAccountHistory);
						at.setBlAuditoria(blAuditoria);
						at.setBlHilo(blHilo);
						at.setBlExcepcion(blExcepcion);

						at.start();
						log.debug("idThread: " + hilo.getIdHilo() + " - " + hilo.getNameType() + " - " + hilo.getNameCheckbox() + " iniciado !!");
					}
					if (hilo.getNameHistory() != null && hilo.getNameHistory().trim().equals("RECHARGE")) {
						ImportadorRecargas at = new ImportadorRecargas(hilo.getIdHilo(), control, controlButton);
						at.setIsdn(isdn);
						at.setLogin(getLogin());
						at.setFechaInicio(startDate);
						at.setFechaFin(endDate);
						at.setHashRechargeType(hashRechargeType);
						at.setHashRechargeChannelId(hashRechargeChannelId);
						at.setBlConsulta(blConsulta);
						at.setBlAccountHistory(blAccountHistory);
						at.setBlAuditoria(blAuditoria);
						at.setBlHilo(blHilo);
						at.setBlExcepcion(blExcepcion);

						at.start();
						log.debug("idThread: " + hilo.getIdHilo() + " - " + hilo.getNameType() + " - " + hilo.getNameCheckbox() + " iniciado !!");
					}
				}
			}

			log.debug("CON ITERACION: " + conIteracion);

			if (conIteracion) {
				boolean fin = false;

				while (!fin) {
					try {
						Thread.sleep(1000);
						if (control.getNroFinalizadosPrimeraIteracion().intValue() <= 0) {
							fin = true;
							log.debug("FINALIZARON TODOS LOS HILOS LA PRIMERA ITERACION: " + fin);
						}
					} catch (InterruptedException e) {
						log.error("ERROR", e);
					}
				}
			} else {
				boolean fin = false;

				while (!fin) {
					try {
						Thread.sleep(1000);
						if (control.getNroThreads().intValue() <= 0) {
							fin = true;
							log.debug("FINALIZARON LOS TODOS LOS HILOS: " + fin);
						}
					} catch (InterruptedException e) {
						log.error("ERROR", e);
					}
				}
			}
			log.debug("FIN DE LANZAMIENTOS DE HILOS");

		} catch (Exception e) {
			log.error("Error al lanzar hilos: ", e);
		}
	}

	// cambiar para billeteras
	public void cargarBilleteras() {
		log.warn("Ingresando a cargar billeteras");
		long idAuditoria = 0;
		String balanceExpireTime = "";
		try {

			balances = new ArrayList<Balance>();
			lifeCycles = new ArrayList<LifeCycle>();

			if (login == null || login.trim().isEmpty()) {
				SysMessage.warn("Usuario no válido", null);
				return;
			}

			if (isdn != null && !isdn.trim().isEmpty()) {
				// LIFECICLESTATUS
				try {
					idAuditoria = guardarAuditoria(Calendar.getInstance(), this.login, this.ip, Parametros.vistaBilletera, Parametros.comandoBilletera, this.isdn, null);

					if (idAuditoria <= 0) {
						SysMessage.error("Error al guardar log de auditoria", null);
						return;
					}
					disableIncidente = false;
					vistaIncidente = DescriptorBitacora.BILLETERAS;
					try {
						controlerBitacora.accion(DescriptorBitacora.BILLETERAS, "Se hizo consulta de Billeteras, isdn: " + isdn);
					} catch (Exception e) {
						log.error("Error al guardar bitacora en el sistema: ", e);
						SysMessage.error("Error al guardar bitacora", null);
					}

					String wsdlLoc = Parametros.cugWsdl;

					URL url = new URL(UtilUrl.getIp(wsdlLoc));
					long ini = System.currentTimeMillis();

					if (url.getProtocol().equalsIgnoreCase("https")) {
						int puerto = url.getPort();
						String host = url.getHost();
						if (host.equalsIgnoreCase("localhost")) {
							host = "127.0.0.1";
						}
						validarCertificado(Parametros.cugPathKeystore, host + ":" + puerto);
					}

					NodoServicio<com.huawei.www.bme.cbsinterface.bcservices.BcServicsBindingStub> portNodo = ServicioCallingCircle.initPort(wsdlLoc, "CUG" + login, Parametros.cugTimeOut);

					if (portNodo != null && portNodo.getPort() != null) {

						synchronized (portNodo) {
							try {
								com.huawei.www.bme.cbsinterface.bcservices.BcServicsBindingStub port = portNodo.getPort();

								com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo ownershipInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo();
								ownershipInfo.setBEID(Parametros.cugOwnerShipInfoId);
								com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo accessSecurity = new com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo();
								accessSecurity.setLoginSystemCode(Parametros.cugLoginSystemCode);
								accessSecurity.setPassword(Parametros.cugPassword);
								com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo operatorInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo();
								operatorInfo.setOperatorID(Parametros.cugOperatorId);
								com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat timeFormat = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat();
								timeFormat.setTimeType(Parametros.cugTimeType);

								SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
								String messageSeq = format.format(new Date());

								com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderAdditionalProperty[] additionalProperty = null;
								com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader requestHeader = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader(Parametros.cugVersion, "", messageSeq, ownershipInfo, accessSecurity, operatorInfo, "", "", timeFormat, additionalProperty);

								com.huawei.www.bme.cbsinterface.bccommon.SubAccessCode subAccessCode = new com.huawei.www.bme.cbsinterface.bccommon.SubAccessCode();
								subAccessCode.setPrimaryIdentity(getIsdn());

								com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequest querySubLifeCycleRequest = new com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequest(subAccessCode);
								com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequestMsg querySubLifeCycleRequestMsg = new com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequestMsg(requestHeader, querySubLifeCycleRequest);

								com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleResultMsg querySubLifeCycle = port.querySubLifeCycle(querySubLifeCycleRequestMsg);

								long fin = System.currentTimeMillis();
								Long Con = null;
								if (portNodo.isPrimeraVez()) {
									Con = portNodo.getTiempoConexion();
								}
								log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio BCServices, querySubLifeCycle: " + (fin - ini) + " milisegundos");

								if (Parametros.saveRequestResponse) {
									try {
										saveXml(isdn, getLogin(), wsdlLoc, "querySubLifeCycle", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoria, Con, (fin - ini));
									} catch (Exception e) {
										log.warn("No se logro registrar los request y response del servicio BCServices, querySubLifeCycle: ", e);
									}
								}

								if (querySubLifeCycle != null && querySubLifeCycle.getResultHeader() != null) {
									if (querySubLifeCycle.getResultHeader().getResultCode().trim().equals("0")) {

										com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleResult querySubLifeCycleResult = querySubLifeCycle.getQuerySubLifeCycleResult();
										if (querySubLifeCycleResult != null) {
											com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleResultLifeCycleStatus[] lifeCycleStatus = querySubLifeCycleResult.getLifeCycleStatus();
											String currentStatusIndex = querySubLifeCycleResult.getCurrentStatusIndex();
											// com.huawei.www.bme.cbsinterface.bbservices.QueryFreeUnitResultFreeUnitItem[]
											// queryFreeUnitResult =
											// queryFreeUnit.getQueryFreeUnitResult();
											if (lifeCycleStatus != null) {

												// log.debug("querySubLifeCycleResult:
												// length: " +
												// lifeCycleStatus.length);

												for (com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleResultLifeCycleStatus item : lifeCycleStatus) {

													Date effectiveTime = UtilDate.stringToDate(item.getStatusExpireTime().trim(), Parametros.fechaFormatWs);
													String statusExpireTime = UtilDate.dateToString(effectiveTime, Parametros.fechaFormatPage);
													LifeCycle lif = new LifeCycle();
													lif.setStatusName(item.getStatusName());
													lif.setStatusIndex(item.getStatusIndex());
													lif.setStatusExpireTime(statusExpireTime);
													getLifeCycles().add(lif);

													if (currentStatusIndex.equals(item.getStatusIndex()))
														balanceExpireTime = statusExpireTime;

													if (balanceExpireTime.isEmpty())
														log.warn("Expire time para CORE_BALANCE no resuelto");

												}

											}
										} else {
											log.warn("[isdn: " + getIsdn() + "] QuerySubLifeCycleResult resuelto a nulo");
										}
									} else {
										log.info("[isdn: " + getIsdn() + "] querySubLifeCycle ResultCode: " + querySubLifeCycle.getResultHeader().getResultCode() + ", ResultDesc: " + querySubLifeCycle.getResultHeader().getResultDesc());
										SysMessage.warn("Servicio BCServices, querySubLifeCycle Code: " + querySubLifeCycle.getResultHeader().getResultCode() + ", Desc: " + querySubLifeCycle.getResultHeader().getResultDesc(), null);
										guardarExcepcion(idAuditoria, "Servicio BCServices, querySubLifeCycle Code: " + querySubLifeCycle.getResultHeader().getResultCode() + ", Desc: " + querySubLifeCycle.getResultHeader().getResultDesc());
									}
								} else {
									if (querySubLifeCycle != null) {
										log.warn("[isdn: " + getIsdn() + "] querySubLifeCycle ResultHeader resuelto a nulo");
									} else {
										log.warn("[isdn: " + getIsdn() + "] querySubLifeCycle QuerySubLifeCycle resuelto a nulo");
									}
								}
							} finally {
								portNodo.setEnUso(false);
								portNodo.setFechaFin(new Date());
							}
						}

					} else {
						log.error("[isdn: " + getIsdn() + "] Port BCServices resuelto a nulo");
					}

				} catch (AxisFault a) {
					String msg = a.getMessage();
					msg = msg.toLowerCase();
					if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase()) || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase()) || msg.contains("UnknownHostException".toLowerCase()) || msg.contains("No route to host".toLowerCase())
							|| msg.contains("Connection refused".toLowerCase())) {
						log.error("Error de conexion al servicio BCServices SubLifeCycle: ", a);
						SysMessage.error("Error de conexion del servicio BCServices", null);
						guardarExcepcion(idAuditoria, "Error de conexion al servicio BCServices: " + stackTraceToString(a));
					} else {
						log.error("Error AxisFault SubLifeCycle: ", a);
						SysMessage.error("Error AxisFault de conexion al servicio BCServices", null);
						guardarExcepcion(idAuditoria, "Error AxisFault de conexion al servicio BCServices: " + stackTraceToString(a));
					}
				} catch (ServiceException e) {
					log.error("Error de servicio al conectarse al servicio BCServices SubLifeCycle: ", e);
					SysMessage.error("Error de servicio al conectarse al servicio BCServices", null);
					guardarExcepcion(idAuditoria, "Error de servicio al conectarse al servicio BCServices: " + stackTraceToString(e));
				} catch (RemoteException e) {
					log.error("Error remoto al conectarse al servicio BCServices SubLifeCycle: ", e);
					SysMessage.error("Error remoto al conectarse al servicio BCServices", null);
					guardarExcepcion(idAuditoria, "Error remoto al conectarse al servicio BCServices: " + stackTraceToString(e));
				} catch (Exception e) {
					log.error("Error al consultar BCServices SubLifeCycle", e);
					SysMessage.error("Error al consultar BCServices", null);
					guardarExcepcion(idAuditoria, "Error al consultar BCServices:" + stackTraceToString(e));
				}

				// BILLETERAS BALANCE
				try {
					String wsdlLoc = Parametros.billeteraWsdl;

					URL url = new URL(UtilUrl.getIp(wsdlLoc));

					long ini = System.currentTimeMillis();

					if (url.getProtocol().equalsIgnoreCase("https")) {
						int puerto = url.getPort();
						String host = url.getHost();
						if (host.equalsIgnoreCase("localhost")) {
							host = "127.0.0.1";
						}
						validarCertificado(Parametros.billeteraPathKeystore, host + ":" + puerto);
					}

					NodoServicio<com.huawei.www.bme.cbsinterface.arservices.ArServicsBindingStub> portNodo = ServicioBilletera.initPort(wsdlLoc, "BILLETERA" + login, Parametros.billeteraTimeOut);

					if (portNodo != null && portNodo.getPort() != null) {

						synchronized (portNodo) {

							try {

								com.huawei.www.bme.cbsinterface.arservices.ArServicsBindingStub port = portNodo.getPort();

								com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo ownershipInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo();
								ownershipInfo.setBEID(Parametros.billeteraOwnerShipInfoId);
								com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo accessSecurity = new com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo();
								accessSecurity.setLoginSystemCode(Parametros.billeteraLoginSystemCode);
								accessSecurity.setPassword(Parametros.billeteraPassword);
								com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo operatorInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo();
								operatorInfo.setOperatorID(Parametros.billeteraOperatorId);
								com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat timeFormat = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat();
								timeFormat.setTimeType(Parametros.billeteraTimeType);
								SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
								String messageSeq = format.format(new Date());

								com.huawei.cbs.ar.wsservice.arcommon.SubAccessCode subAccessCode = new com.huawei.cbs.ar.wsservice.arcommon.SubAccessCode();
								subAccessCode.setPrimaryIdentity(isdn);

								com.huawei.www.bme.cbsinterface.arservices.QueryBalanceRequestQueryObjAcctAccessCode acctAccessCode = null;
								com.huawei.www.bme.cbsinterface.arservices.QueryBalanceRequestQueryObj queryObj = new com.huawei.www.bme.cbsinterface.arservices.QueryBalanceRequestQueryObj(subAccessCode, acctAccessCode);
								// String balanceType = "";
								com.huawei.www.bme.cbsinterface.arservices.QueryBalanceRequest queryBalanceRequest = new com.huawei.www.bme.cbsinterface.arservices.QueryBalanceRequest(queryObj, null);

								com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderAdditionalProperty[] additionalProperty = null;
								String version = Parametros.billeteraVersion;
								com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader requestHeader = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader(version, "", messageSeq, ownershipInfo, accessSecurity, operatorInfo, "", "", timeFormat, additionalProperty);

								com.huawei.www.bme.cbsinterface.arservices.QueryBalanceRequestMsg queryBalanceRequestMsg = new com.huawei.www.bme.cbsinterface.arservices.QueryBalanceRequestMsg(requestHeader, queryBalanceRequest);
								com.huawei.www.bme.cbsinterface.arservices.QueryBalanceResultMsg queryBalance = port.queryBalance(queryBalanceRequestMsg);

								long fin = System.currentTimeMillis();
								Long Con = null;
								if (portNodo.isPrimeraVez()) {
									Con = portNodo.getTiempoConexion();
								}
								log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio ARServices, queryBalance: " + (fin - ini) + " milisegundos");

								if (Parametros.saveRequestResponse) {
									try {
										saveXml(isdn, getLogin(), wsdlLoc, "queryBalance", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoria, Con, (fin - ini));
									} catch (Exception e) {
										log.warn("No se logro registrar los request y response del servicio ARServices, queryBalance: ", e);
									}
								}

								if (queryBalance != null && queryBalance.getResultHeader() != null) {
									if (queryBalance.getResultHeader().getResultCode().trim().equals("0")) {

										com.huawei.www.bme.cbsinterface.arservices.QueryBalanceResultAcctList[] queryBalanceResult = queryBalance.getQueryBalanceResult();
										if (queryBalanceResult != null && queryBalanceResult.length > 0) {
											long reservationAmountCoreBalance = 0;
											long amountCoreBalance = 0;
											ArrayList<Balance> listCoreBalance= new ArrayList<Balance>();
											Balance headerCoreBalance=null;
											boolean isCoreBalance=false;
											boolean isCoreBalance2=false;
											for (com.huawei.www.bme.cbsinterface.arservices.QueryBalanceResultAcctList queryBalanceResultAcctList : queryBalanceResult) {
												// String acctKey =
												// queryBalanceResultAcctList.getAcctKey();
												String verAccKey = queryBalanceResultAcctList.getAcctKey();
												String palabrabuscada = Parametros.billeteraBuscarPrefijo;
												int resultado = verAccKey.indexOf(palabrabuscada);
												if (resultado != -1) {
													verAccKey = hashAcckey.get("1");
												} else {
													verAccKey = hashAcckey.get("2");
												}

												id = 1;
												com.huawei.cbs.ar.wsservice.arcommon.AcctBalance[] balanceResult = queryBalanceResultAcctList.getBalanceResult();
												if (balanceResult != null && balanceResult.length > 0) {
													for (com.huawei.cbs.ar.wsservice.arcommon.AcctBalance acctBalance : balanceResult) {
														int statusExpired = 0;// 1:corbalance,2:daysexpired
														String daysExpired = "";
														Balance balance = new Balance();
														balance.setId(id);
														// b.setColor("CABECERA");
														balance.setColor("#e2e2e2");

														id++;

														balance.setAcctKey(verAccKey);
														String balanceTypeDescription = "";
														if (acctBalance.getBalanceType() != null)
															balanceTypeDescription = hashBalanceType.get(acctBalance.getBalanceType().trim());
														if (balanceTypeDescription != null) {
															balance.setBalanceType(balanceTypeDescription);
														} else {
															if (acctBalance.getBalanceType() != null)
																balance.setBalanceType(acctBalance.getBalanceType().trim());
														}

														// Validar Fecha
														// Expiracion
														if (acctBalance.getBalanceType() != null) {
															if (acctBalance.getBalanceType().equals(Parametros.billeteraCorebalanceBalanceType)) {
																statusExpired = 1;
															} else {
																if (hashDaysExpired.get(acctBalance.getBalanceType()) != null) {
																	statusExpired = 2;
																	daysExpired = hashDaysExpired.get(acctBalance.getBalanceType());
																}
															}
														}

														balance.setBalanceTypeName(acctBalance.getBalanceTypeName());
														//balance.setBalanceTypeName("CORE_BALANCE + CORE_BALANCE_2");
														double total = UtilNumber.redondear(((double) acctBalance.getTotalAmount() / (double) Parametros.billeteraDivisor), Parametros.billeteraNroDecimales);
														balance.setAmount(UtilNumber.doubleToString(total, Parametros.billeteraNroDecimales));
														long reservationAmount = 0;

														String unitType = "";
														ArrayList<Balance> listFreeUnit = new ArrayList<Balance>();
														com.huawei.cbs.ar.wsservice.arcommon.AcctBalanceBalanceDetail[] balanceDetail = acctBalance.getBalanceDetail();

														if (balanceDetail != null && balanceDetail.length > 0) {
															for (com.huawei.cbs.ar.wsservice.arcommon.AcctBalanceBalanceDetail item : balanceDetail) {
																Date date = UtilDate.stringToDate(item.getExpireTime().trim(), Parametros.fechaFormatWs);
																Balance bal = new Balance();
																bal.setId(id);
																id++;
																bal.setAcctKey("");
																//acctBalance.getBalanceTypeName()
																//bal.setBalanceType("<b>DETAIL</b>");
																bal.setBalanceType(acctBalance.getBalanceTypeName());
																bal.setBalanceTypeName(item.getBalanceInstanceID() + "");

																double total1 = UtilNumber.redondear((double) item.getAmount() / (double) Parametros.billeteraDivisor, Parametros.billeteraNroDecimales);
																bal.setAmount(UtilNumber.doubleToString(total1, Parametros.billeteraNroDecimales));

																double total2 = UtilNumber.redondear((double) item.getReservationAmount() / (double) Parametros.billeteraDivisor, Parametros.billeteraNroDecimales);
																bal.setReservationAmount(UtilNumber.doubleToString(total2, Parametros.billeteraNroDecimales));

																bal.setTotalBalance(UtilNumber.doubleToString((total1 - total2), Parametros.billeteraNroDecimales));

																if (acctBalance.getCurrencyID() != null) {
																	String cd = getCurrencyDescription(acctBalance.getCurrencyID().intValue());
																	if (cd != null) {
																		bal.setUnitType(cd);
																		unitType = cd;
																	} else
																		bal.setUnitType(acctBalance.getCurrencyID().intValue() + "");
																}

																Date effectiveTime = UtilDate.stringToDate(item.getEffectiveTime().trim(), Parametros.fechaFormatWs);
																bal.setEffectiveTime(UtilDate.dateToString(effectiveTime, Parametros.fechaFormatPage));

																if (Parametros.billeteraValidarFechaExpiracion) {
																	if (UtilDate.dateToString(date, "HH:mm:ss").equals("00:00:00")) {
																		Calendar restar = Calendar.getInstance();
																		restar.setTime(date);
																		restar.add(Calendar.SECOND, -1);

																		bal.setExpireTime(UtilDate.dateToString(restar.getTime(), Parametros.fechaFormatPage));

																	} else {
																		bal.setExpireTime(UtilDate.dateToString(date, Parametros.fechaFormatPage));
																	}
																} else {
																	bal.setExpireTime(UtilDate.dateToString(date, Parametros.fechaFormatPage));
																}

																// expireTime
																if (statusExpired == 1) {
																	bal.setStatusExpireTime(balanceExpireTime);
																} else {
																	if (statusExpired == 2) {
																		Date f = UtilDate.stringToDate(bal.getExpireTime(), Parametros.fechaFormatPage);
																		f = UtilDate.sumarRestarDiasFecha(f, Integer.valueOf(daysExpired));
																		bal.setStatusExpireTime(UtilDate.dateToString(f, Parametros.fechaFormatPage));
																	}
																}
																if(acctBalance.getBalanceTypeName().contains("CORE_BALANCE")) {
																	reservationAmountCoreBalance = reservationAmountCoreBalance + item.getReservationAmount();
																	amountCoreBalance = amountCoreBalance + item.getAmount();
																	listCoreBalance.add(bal);
																}else {
																	reservationAmount = reservationAmount + item.getReservationAmount();
																	listFreeUnit.add(bal);
																}
															}
														}

														total = acctBalance.getTotalAmount() - reservationAmount;

														double redondear = UtilNumber.redondear(((double) reservationAmount / (double) Parametros.billeteraDivisor), Parametros.billeteraNroDecimales);
														balance.setReservationAmount(UtilNumber.doubleToString(redondear, Parametros.billeteraNroDecimales));

														double total3 = UtilNumber.redondear(((double) total / (double) Parametros.billeteraDivisor), Parametros.billeteraNroDecimales);
														balance.setTotalBalance(UtilNumber.doubleToString(total3, Parametros.billeteraNroDecimales));
														balance.setExpireTime("");
														balance.setUnitType(unitType);
														if(acctBalance.getBalanceTypeName().contains("CORE_BALANCE")) {
															if( headerCoreBalance==null) {
																headerCoreBalance = balance;
															}
															if(acctBalance.getBalanceTypeName().equals("CORE_BALANCE")){
																isCoreBalance=true;
															}
															if(acctBalance.getBalanceTypeName().equals("CORE_BALANCE_2")){
																isCoreBalance2=true;
															}
														}else {
															balances.add(balance);

															for (Balance item : listFreeUnit) {
																balances.add(item);
															}
														}
													}
												}
											}

											if(headerCoreBalance!=null) {

												double redondear = UtilNumber.redondear(((double) reservationAmountCoreBalance / (double) Parametros.billeteraDivisor), Parametros.billeteraNroDecimales);
												headerCoreBalance.setReservationAmount(UtilNumber.doubleToString(redondear, Parametros.billeteraNroDecimales));
												double total2 = UtilNumber.redondear((((double) amountCoreBalance - redondear) / (double) Parametros.billeteraDivisor), Parametros.billeteraNroDecimales);
												headerCoreBalance.setTotalBalance(UtilNumber.doubleToString(total2, Parametros.billeteraNroDecimales));
												double total3 = UtilNumber.redondear(((double) amountCoreBalance / (double) Parametros.billeteraDivisor), Parametros.billeteraNroDecimales);
												headerCoreBalance.setAmount(UtilNumber.doubleToString(total3, Parametros.billeteraNroDecimales));
												String balanceTypeName=isCoreBalance? (isCoreBalance2?"CORE_BALANCE + CORE_BALANCE_2":"CORE_BALANCE"):"CORE_BALANCE_2";
												headerCoreBalance.setBalanceTypeName(balanceTypeName);
												balances.add(headerCoreBalance);
												for (Balance item : listCoreBalance) {
													balances.add(item);
												}
											}
										}
									} else {
										log.info("[isdn: " + getIsdn() + "] queryBalance ResultCode: " + queryBalance.getResultHeader().getResultCode() + ", ResultDesc: " + queryBalance.getResultHeader().getResultDesc());
										SysMessage.warn("Servicio ARServices, queryBalance Code: " + queryBalance.getResultHeader().getResultCode() + ", Desc: " + queryBalance.getResultHeader().getResultDesc(), null);
										guardarExcepcion(idAuditoria, "Servicio ARServices, queryBalance Code: " + queryBalance.getResultHeader().getResultCode() + ", Desc: " + queryBalance.getResultHeader().getResultDesc());
									}
								} else {
									if (queryBalance != null) {
										log.warn("[isdn: " + getIsdn() + "] queryBalance queryBalance resuelto a nulo");
									} else {
										log.warn("[isdn: " + getIsdn() + "] queryBalance ResultHeader resuelto a nulo");
									}
								}
							} finally {
								portNodo.setEnUso(false);
								portNodo.setFechaFin(new Date());
							}
						}
					} else {
						log.error("[isdn: " + getIsdn() + "] Port ARServices billeteras resuelto a nulo");
					}

				} catch (AxisFault a) {
					String msg = a.getMessage();
					msg = msg.toLowerCase();
					if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase()) || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase()) || msg.contains("UnknownHostException".toLowerCase()) || msg.contains("No route to host".toLowerCase())
							|| msg.contains("Connection refused".toLowerCase())) {
						log.error("Error de conexion al servicio ARServices billeteras: ", a);
						SysMessage.error("Error de conexion del servicio ARServices", null);
						guardarExcepcion(idAuditoria, "Error de conexion al servicio ARServices: " + stackTraceToString(a));
					} else {
						log.error("Error AxisFault Billerera: ", a);
						SysMessage.error("Error AxisFault de conexion al servicio ARServices", null);
						guardarExcepcion(idAuditoria, "Error AxisFault de conexion al servicio ARServices: " + stackTraceToString(a));
					}

				} catch (ServiceException e) {
					log.error("Error de servicio al conectarse al servicio ARServices billeteras: ", e);
					SysMessage.error("Error de servicio al conectarse al servicio ARServices", null);
					guardarExcepcion(idAuditoria, "Error de servicio al conectarse al servicio ARServices: " + stackTraceToString(e));
				} catch (RemoteException e) {
					log.error("Error remoto al conectarse al servicio ARServices billeteras: ", e);
					SysMessage.error("Error remoto al conectarse al servicio ARServices", null);
					guardarExcepcion(idAuditoria, "Error remoto al conectarse al servicio ARServices: " + stackTraceToString(e));
				} catch (Exception e) {
					log.error("Error al consultar ARServices billeteras", e);
					SysMessage.error("Error al consultar ARServices", null);
					guardarExcepcion(idAuditoria, "Error al consultar ARServices: " + stackTraceToString(e));
				}

				// ------------- BILLETERAS FREEUNIT NO
				// EXPIRADAS----------------

				try {

					String wsdlLoc = Parametros.historialWsdl;

					URL url = new URL(UtilUrl.getIp(wsdlLoc));
					long ini = System.currentTimeMillis();

					if (url.getProtocol().equalsIgnoreCase("https")) {
						int puerto = url.getPort();
						String host = url.getHost();
						if (host.equalsIgnoreCase("localhost")) {
							host = "127.0.0.1";
						}
						validarCertificado(Parametros.historialPathKeystore, host + ":" + puerto);
					}

					NodoServicio<com.huawei.www.bme.cbsinterface.bbservices.BbServicsBindingStub> portNodo = ServicioHistorial.initPort(wsdlLoc, "FREEUNIT" + login, Parametros.historialTimeOut);

					if (portNodo != null && portNodo.getPort() != null) {

						synchronized (portNodo) {
							try {
								com.huawei.www.bme.cbsinterface.bbservices.BbServicsBindingStub port = portNodo.getPort();

								com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo ownershipInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo();
								ownershipInfo.setBEID(Parametros.historialOwnerShipInfoId);
								com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo accessSecurity = new com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo();
								accessSecurity.setLoginSystemCode(Parametros.historialLoginSystemCode);
								accessSecurity.setPassword(Parametros.historialPassword);
								com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo operatorInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo();
								operatorInfo.setOperatorID(Parametros.historialOperatorId);
								com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat timeFormat = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat();
								timeFormat.setTimeType(Parametros.historialTimeType);
								SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
								String messageSeq = format.format(new Date());

								com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderAdditionalProperty[] additionalProperty = null;

								com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader requestHeader = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader(Parametros.historialVersion, "", messageSeq, ownershipInfo, accessSecurity, operatorInfo, "", "", timeFormat, additionalProperty);
								com.huawei.www.bme.cbsinterface.bbservices.QueryFreeUnitRequestMsg queryFreeUnitRequestMsg = new com.huawei.www.bme.cbsinterface.bbservices.QueryFreeUnitRequestMsg();
								queryFreeUnitRequestMsg.setRequestHeader(requestHeader);
								com.huawei.www.bme.cbsinterface.bbservices.QueryFreeUnitRequest queryFreeUnitRequest = new com.huawei.www.bme.cbsinterface.bbservices.QueryFreeUnitRequest();
								com.huawei.www.bme.cbsinterface.bbservices.QueryFreeUnitRequestQueryObj queryObj = new com.huawei.www.bme.cbsinterface.bbservices.QueryFreeUnitRequestQueryObj();
								com.huawei.www.bme.cbsinterface.bbcommon.SubAccessCode subAccessCode = new com.huawei.www.bme.cbsinterface.bbcommon.SubAccessCode();
								subAccessCode.setPrimaryIdentity(isdn);
								queryObj.setSubAccessCode(subAccessCode);
								queryFreeUnitRequest.setQueryObj(queryObj);

								queryFreeUnitRequestMsg.setQueryFreeUnitRequest(queryFreeUnitRequest);
								com.huawei.www.bme.cbsinterface.bbservices.QueryFreeUnitResultMsg queryFreeUnit = port.queryFreeUnit(queryFreeUnitRequestMsg);

								long fin = System.currentTimeMillis();
								Long Con = null;
								if (portNodo.isPrimeraVez()) {
									Con = portNodo.getTiempoConexion();
								}
								log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio BBServices Billetera, queryFreeUnit: " + (fin - ini) + " milisegundos");

								if (Parametros.saveRequestResponse) {
									try {
										saveXml(isdn, getLogin(), wsdlLoc, "queryFreeUnit", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoria, Con, (fin - ini));
									} catch (Exception e) {
										log.warn("No se logro registrar los request y response del servicio BBServices, queryFreeUnit: ", e);
									}
								}

								if (queryFreeUnit != null && queryFreeUnit.getResultHeader() != null) {
									if (queryFreeUnit.getResultHeader().getResultCode().trim().equals("0")) {

										// com.huawei.www.bme.cbsinterface.bbservices.QueryFreeUnitResult
										// queryFreeUnitResult2 =
										// queryFreeUnit.getQueryFreeUnitResult();
										// if (queryFreeUnitResult2 != null) {
										// com.huawei.www.bme.cbsinterface.bbservices.QueryFreeUnitResultFreeUnitItem[]
										// queryFreeUnitResult =
										// queryFreeUnitResult2.getFreeUnitItem();

										com.huawei.www.bme.cbsinterface.bbservices.QueryFreeUnitResultFreeUnitItem[] queryFreeUnitResult = queryFreeUnit.getQueryFreeUnitResult();
										if (queryFreeUnitResult != null) {

											log.debug("queryFreeUnitResult: length: " + queryFreeUnitResult.length);

											for (com.huawei.www.bme.cbsinterface.bbservices.QueryFreeUnitResultFreeUnitItem item : queryFreeUnitResult) {
												// todas las no expiradas
												// menos las que estan
												// configuradas en la lista
												// de no expirados
												if (!(listBilleteraExpired.contains(item.getFreeUnitType()))) {
													int statusExpired = 0;// 1:
													// corebalance,2:para
													// las
													// daysbalance
													String daysExpired = "";

													Balance bal = new Balance();
													bal.setId(id);
													// bal.setColor("CABECERA");
													bal.setColor("#e2e2e2");

													id++;
													bal.setAcctKey("FREEUNIT");
													if (item.getFreeUnitType() != null)
														bal.setBalanceType(hashFreeUnit.get(item.getFreeUnitType().trim()));
													bal.setBalanceTypeName(item.getFreeUnitTypeName());

													// SE SOBREESCRIBE SI NO
													// SE ENCUENTRA EN LA
													// TABLA FREEUNIT
													// MEASURE
													bal.setAmount(UtilNumber.doubleToString(item.getTotalUnusedAmount(), Parametros.billeteraNroDecimales));

													// bal.setTotalBalance(bal.getAmount());
													bal.setUnitType(item.getMeasureUnitName());

													if (item.getFreeUnitType() != null) {
														if (item.getFreeUnitType().equals(Parametros.billeteraCorebalanceBalanceType)) {
															statusExpired = 1;
														} else {
															if (hashDaysExpired.get(item.getFreeUnitType()) != null) {
																statusExpired = 2;
																daysExpired = hashDaysExpired.get(item.getFreeUnitType());
															}
														}
													}
													double reservationAmountSuma = 0;
													double totalSuma = 0;

													if (item.getMeasureUnit() != null) {
														FreeUnitMeasure freeUnitMeasure = hashFreeUnitMeasure.get(item.getMeasureUnit().trim());
														if (freeUnitMeasure != null) {
															double total = UtilNumber.redondear((double) item.getTotalUnusedAmount() / freeUnitMeasure.getDividendo(), Parametros.billeteraNroDecimales);
															bal.setAmount(UtilNumber.doubleToString(total, Parametros.billeteraNroDecimales));
															bal.setTotalBalance(UtilNumber.doubleToString(total, Parametros.billeteraNroDecimales));
															bal.setUnitType(freeUnitMeasure.getUnidad());
															totalSuma = total;
														} else {
															totalSuma = item.getTotalUnusedAmount();
														}
													} else {
														totalSuma = item.getTotalUnusedAmount();
													}

													ArrayList<Balance> listFreeUnit = new ArrayList<Balance>();
													com.huawei.www.bme.cbsinterface.bbservices.QueryFreeUnitResultFreeUnitItemFreeUnitItemDetail[] freeUnitDetails = item.getFreeUnitItemDetail();
													if (freeUnitDetails != null && freeUnitDetails.length > 0) {

														log.debug(item.getFreeUnitTypeName() + ", freeUnitDetails.size: " + freeUnitDetails.length);

														for (com.huawei.www.bme.cbsinterface.bbservices.QueryFreeUnitResultFreeUnitItemFreeUnitItemDetail itemsdetail : freeUnitDetails) {
															Balance b = new Balance();
															b.setId(id);
															id++;
															b.setAcctKey("");
															b.setBalanceType("<b>DETAIL</b>");
															b.setBalanceTypeName(itemsdetail.getFreeUnitInstanceID() + "");

															// SE
															// SOBREESCRIBE
															// SI NO SE
															// ENCUENTRA EN
															// LA TABLA
															// FREEUNIT
															// MEASURE
															b.setAmount(UtilNumber.doubleToString(itemsdetail.getCurrentAmount(), Parametros.billeteraNroDecimales));
															b.setReservationAmount(UtilNumber.doubleToString(itemsdetail.getReservationAmount(), Parametros.billeteraNroDecimales));
															b.setTotalBalance(UtilNumber.doubleToString(itemsdetail.getCurrentAmount() + itemsdetail.getReservationAmount(), Parametros.billeteraNroDecimales));
															b.setUnitType(item.getMeasureUnitName());

															if (item.getMeasureUnit() != null) {
																FreeUnitMeasure freeUnitMeasure = hashFreeUnitMeasure.get(item.getMeasureUnit().trim());
																if (freeUnitMeasure != null) {
																	double total = UtilNumber.redondear((double) itemsdetail.getCurrentAmount() / freeUnitMeasure.getDividendo(), Parametros.billeteraNroDecimales);
																	b.setAmount(UtilNumber.doubleToString(total, Parametros.billeteraNroDecimales));
																	double total2 = UtilNumber.redondear((double) itemsdetail.getReservationAmount() / freeUnitMeasure.getDividendo(), Parametros.billeteraNroDecimales);
																	b.setReservationAmount(UtilNumber.doubleToString(total2, Parametros.billeteraNroDecimales));

																	b.setTotalBalance(UtilNumber.doubleToString(total - total2, Parametros.billeteraNroDecimales));
																	b.setUnitType(freeUnitMeasure.getUnidad());
																	reservationAmountSuma = reservationAmountSuma + total2;
																}
															}

															Date effectiveTime = UtilDate.stringToDate(itemsdetail.getEffectiveTime().trim(), Parametros.fechaFormatWs);
															b.setEffectiveTime(UtilDate.dateToString(effectiveTime, Parametros.fechaFormatPage));

															Date date = UtilDate.stringToDate(itemsdetail.getExpireTime().trim(), Parametros.fechaFormatWs);

															if (Parametros.billeteraValidarFechaExpiracion) {
																if (UtilDate.dateToString(date, "HH:mm:ss").equals("00:00:00")) {
																	Calendar restar = Calendar.getInstance();
																	restar.setTime(date);
																	restar.add(Calendar.SECOND, -1);

																	b.setExpireTime(UtilDate.dateToString(restar.getTime(), Parametros.fechaFormatPage));

																} else {
																	b.setExpireTime(UtilDate.dateToString(date, Parametros.fechaFormatPage));
																}
															} else {
																b.setExpireTime(UtilDate.dateToString(date, Parametros.fechaFormatPage));
															}

															// expireTime
															if (statusExpired == 1) {
																b.setStatusExpireTime(balanceExpireTime);
															} else {
																if (statusExpired == 2) {
																	Date f = UtilDate.stringToDate(b.getExpireTime(), Parametros.fechaFormatPage);
																	f = UtilDate.sumarRestarDiasFecha(f, Integer.valueOf(daysExpired));
																	b.setStatusExpireTime(UtilDate.dateToString(f, Parametros.fechaFormatPage));
																}
															}
															listFreeUnit.add(b);
														}
													}

													bal.setReservationAmount(UtilNumber.doubleToString(reservationAmountSuma, Parametros.billeteraNroDecimales));
													bal.setTotalBalance(UtilNumber.doubleToString(totalSuma - reservationAmountSuma, Parametros.billeteraNroDecimales));

													balances.add(bal);

													for (Balance itemsde : listFreeUnit) {
														balances.add(itemsde);
													}

												}
											}
										} else {
											log.warn("[isdn: " + getIsdn() + "] queryFreeUnitResult resuelto a nulo");
										}
										// }
									} else {
										log.info("[isdn: " + getIsdn() + "] queryFreeUnit ResultCode: " + queryFreeUnit.getResultHeader().getResultCode() + ", ResultDesc: " + queryFreeUnit.getResultHeader().getResultDesc());
										SysMessage.warn("Servicio BBServices, queryFreeUnit, Code: " + queryFreeUnit.getResultHeader().getResultCode() + ", Desc: " + queryFreeUnit.getResultHeader().getResultDesc(), null);
										guardarExcepcion(idAuditoria, "Servicio BBServices, queryFreeUnit, Code: " + queryFreeUnit.getResultHeader().getResultCode() + ", Desc: " + queryFreeUnit.getResultHeader().getResultDesc());
									}
								} else {
									if (queryFreeUnit != null) {
										log.warn("[isdn: " + getIsdn() + "] queryFreeUnit ResultHeader resuelto a nulo");
									} else {
										log.warn("[isdn: " + getIsdn() + "] queryFreeUnit queryFreeUnit resuelto a nulo");
									}
								}
							} finally {
								portNodo.setEnUso(false);
								portNodo.setFechaFin(new Date());
							}
						}

					} else {
						log.error("[isdn: " + getIsdn() + "] Port BBServices Billetera FREEUNIT resuelto a nulo");
					}

				} catch (AxisFault a) {
					String msg = a.getMessage();
					msg = msg.toLowerCase();
					if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase()) || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase()) || msg.contains("UnknownHostException".toLowerCase()) || msg.contains("No route to host".toLowerCase())
							|| msg.contains("Connection refused".toLowerCase())) {
						log.error("Error de conexion al servicio BBServices Billetera FREEUNIT: ", a);
						SysMessage.error("Error de conexion del servicio BBServices", null);
						guardarExcepcion(idAuditoria, "Error de conexion al servicio BBServices: " + stackTraceToString(a));
					} else {
						log.error("Error AxisFault: ", a);
						SysMessage.error("Error AxisFault de conexion al servicio BBServices", null);
						guardarExcepcion(idAuditoria, "Error AxisFault de conexion al servicio BBServices: " + stackTraceToString(a));
					}
				} catch (ServiceException e) {
					log.error("Error de servicio al conectarse al servicio BBServices Billetera FREEUNIT: ", e);
					SysMessage.error("Error de servicio al conectarse al servicio BBServices", null);
					guardarExcepcion(idAuditoria, "Error de servicio al conectarse al servicio BBServices: " + stackTraceToString(e));
				} catch (RemoteException e) {
					log.error("Error remoto al conectarse al servicio BBServices Billetera FREEUNIT : ", e);
					SysMessage.error("Error remoto al conectarse al servicio BBServices", null);
					guardarExcepcion(idAuditoria, "Error remoto al conectarse al servicio BBServices: " + stackTraceToString(e));
				} catch (Exception e) {
					log.error("Error al consultar BBServices Billetera FREEUNIT", e);
					SysMessage.error("Error al consultar BBServices", null);
					guardarExcepcion(idAuditoria, "Error al consultar BBServices: " + stackTraceToString(e));
				}

				// ------------- BILLETERAS FREEUNIT EXPIRADAS----------------

				try {

					String wsdlLoc = Parametros.historialWsdl;

					URL url = new URL(UtilUrl.getIp(wsdlLoc));
					long ini = System.currentTimeMillis();

					if (url.getProtocol().equalsIgnoreCase("https")) {
						int puerto = url.getPort();
						String host = url.getHost();
						if (host.equalsIgnoreCase("localhost")) {
							host = "127.0.0.1";
						}
						validarCertificado(Parametros.historialPathKeystore, host + ":" + puerto);
					}

					NodoServicio<com.huawei.www.bme.cbsinterface.bbservices.BbServicsBindingStub> portNodo = ServicioHistorial.initPort(wsdlLoc, "FREEUNIT" + login, Parametros.historialTimeOut);

					if (portNodo != null && portNodo.getPort() != null) {

						synchronized (portNodo) {
							try {
								com.huawei.www.bme.cbsinterface.bbservices.BbServicsBindingStub port = portNodo.getPort();

								com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo ownershipInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo();
								ownershipInfo.setBEID(Parametros.historialOwnerShipInfoId);
								com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo accessSecurity = new com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo();
								accessSecurity.setLoginSystemCode(Parametros.historialLoginSystemCode);
								accessSecurity.setPassword(Parametros.historialPassword);
								com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo operatorInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo();
								operatorInfo.setOperatorID(Parametros.historialOperatorId);
								com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat timeFormat = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat();
								timeFormat.setTimeType(Parametros.historialTimeType);
								SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
								String messageSeq = format.format(new Date());

								com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderAdditionalProperty[] additionalProperty = null;

								com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader requestHeader = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader(Parametros.historialVersion, "", messageSeq, ownershipInfo, accessSecurity, operatorInfo, "", "", timeFormat, additionalProperty);
								com.huawei.www.bme.cbsinterface.bbservices.QueryFreeUnitRequestMsg queryFreeUnitRequestMsg = new com.huawei.www.bme.cbsinterface.bbservices.QueryFreeUnitRequestMsg();
								queryFreeUnitRequestMsg.setRequestHeader(requestHeader);
								com.huawei.www.bme.cbsinterface.bbservices.QueryFreeUnitRequest queryFreeUnitRequest = new com.huawei.www.bme.cbsinterface.bbservices.QueryFreeUnitRequest();
								com.huawei.www.bme.cbsinterface.bbservices.QueryFreeUnitRequestQueryObj queryObj = new com.huawei.www.bme.cbsinterface.bbservices.QueryFreeUnitRequestQueryObj();
								com.huawei.www.bme.cbsinterface.bbcommon.SubAccessCode subAccessCode = new com.huawei.www.bme.cbsinterface.bbcommon.SubAccessCode();
								subAccessCode.setPrimaryIdentity(isdn);
								queryObj.setSubAccessCode(subAccessCode);
								queryFreeUnitRequest.setQueryObj(queryObj);

								com.huawei.www.bme.cbsinterface.bbcommon.SimpleProperty sp = new com.huawei.www.bme.cbsinterface.bbcommon.SimpleProperty(Parametros.billeteraFreeunitAdditionalPropertyCode, Parametros.billeteraFreeunitAdditionalPropertyValue);

								com.huawei.www.bme.cbsinterface.bbcommon.SimpleProperty[] simpleAditionalProperty = new com.huawei.www.bme.cbsinterface.bbcommon.SimpleProperty[1];
								simpleAditionalProperty[0] = sp;
								queryFreeUnitRequest.setAdditionalProperty(simpleAditionalProperty);

								queryFreeUnitRequestMsg.setQueryFreeUnitRequest(queryFreeUnitRequest);
								com.huawei.www.bme.cbsinterface.bbservices.QueryFreeUnitResultMsg queryFreeUnit = port.queryFreeUnit(queryFreeUnitRequestMsg);

								long fin = System.currentTimeMillis();

								Long Con = null;
								if (portNodo.isPrimeraVez()) {
									Con = portNodo.getTiempoConexion();
								}
								log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio BBServices Billetera FREEUNIT EXPIRED, queryFreeUnit: " + (fin - ini) + " milisegundos");

								if (Parametros.saveRequestResponse) {
									try {
										saveXml(isdn, getLogin(), wsdlLoc, "queryFreeUnit", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoria, Con, (fin - ini));
									} catch (Exception e) {
										log.warn("No se logro registrar los request y response del servicio BBServices, queryFreeUnit: ", e);
									}
								}

								if (queryFreeUnit != null && queryFreeUnit.getResultHeader() != null) {
									if (queryFreeUnit.getResultHeader().getResultCode().trim().equals("0")) {

										// com.huawei.www.bme.cbsinterface.bbservices.QueryFreeUnitResult
										// queryFreeUnitResult2 =
										// queryFreeUnit.getQueryFreeUnitResult();
										// if (queryFreeUnitResult2 != null) {
										// com.huawei.www.bme.cbsinterface.bbservices.QueryFreeUnitResultFreeUnitItem[]
										// queryFreeUnitResult =
										// queryFreeUnitResult2.getFreeUnitItem();

										com.huawei.www.bme.cbsinterface.bbservices.QueryFreeUnitResultFreeUnitItem[] queryFreeUnitResult = queryFreeUnit.getQueryFreeUnitResult();

										if (queryFreeUnitResult != null) {

											log.debug("queryFreeUnitResult: length: " + queryFreeUnitResult.length);

											for (com.huawei.www.bme.cbsinterface.bbservices.QueryFreeUnitResultFreeUnitItem item : queryFreeUnitResult) {
												// todas las no expiradas
												// menos las que estan
												// configuradas en la lista
												// de no expirados
												if ((listBilleteraExpired.contains(item.getFreeUnitType()))) {
													int statusExpired = 0;// 1:
													// corebalance,2:para
													// las
													// daysbalance
													String daysExpired = "";

													Balance bal = new Balance();
													bal.setId(id);
													// bal.setColor("CABECERA");
													bal.setColor("#e2e2e2");

													id++;
													bal.setAcctKey("FREEUNIT");
													if (item.getFreeUnitType() != null)
														bal.setBalanceType(hashFreeUnit.get(item.getFreeUnitType().trim()));
													bal.setBalanceTypeName(item.getFreeUnitTypeName());

													// SE SOBREESCRIBE SI NO
													// SE ENCUENTRA EN LA
													// TABLA FREEUNIT
													// MEASURE
													bal.setAmount(UtilNumber.doubleToString(item.getTotalUnusedAmount(), Parametros.billeteraNroDecimales));

													// bal.setTotalBalance(bal.getAmount());
													bal.setUnitType(item.getMeasureUnitName());

													if (item.getFreeUnitType() != null) {
														if (item.getFreeUnitType().equals(Parametros.billeteraCorebalanceBalanceType)) {
															statusExpired = 1;
														} else {
															if (hashDaysExpired.get(item.getFreeUnitType()) != null) {
																statusExpired = 2;
																daysExpired = hashDaysExpired.get(item.getFreeUnitType());
															}
														}
													}
													double reservationAmountSuma = 0;
													double totalSuma = 0;

													if (item.getMeasureUnit() != null) {
														FreeUnitMeasure freeUnitMeasure = hashFreeUnitMeasure.get(item.getMeasureUnit().trim());
														if (freeUnitMeasure != null) {
															double total = UtilNumber.redondear((double) item.getTotalUnusedAmount() / freeUnitMeasure.getDividendo(), Parametros.billeteraNroDecimales);
															bal.setAmount(UtilNumber.doubleToString(total, Parametros.billeteraNroDecimales));
															bal.setTotalBalance(UtilNumber.doubleToString(total, Parametros.billeteraNroDecimales));
															bal.setUnitType(freeUnitMeasure.getUnidad());
															totalSuma = total;
														} else {
															totalSuma = item.getTotalUnusedAmount();
														}
													} else {
														totalSuma = item.getTotalUnusedAmount();
													}

													ArrayList<Balance> listFreeUnit = new ArrayList<Balance>();
													com.huawei.www.bme.cbsinterface.bbservices.QueryFreeUnitResultFreeUnitItemFreeUnitItemDetail[] freeUnitDetails = item.getFreeUnitItemDetail();
													if (freeUnitDetails != null && freeUnitDetails.length > 0) {

														log.debug(item.getFreeUnitTypeName() + ", freeUnitDetails.size: " + freeUnitDetails.length);

														for (com.huawei.www.bme.cbsinterface.bbservices.QueryFreeUnitResultFreeUnitItemFreeUnitItemDetail itemsdetail : freeUnitDetails) {
															Balance b = new Balance();
															b.setId(id);
															id++;
															b.setAcctKey("");
															b.setBalanceType("<b>DETAIL</b>");
															b.setBalanceTypeName(itemsdetail.getFreeUnitInstanceID() + "");

															// SE
															// SOBREESCRIBE
															// SI NO SE
															// ENCUENTRA EN
															// LA TABLA
															// FREEUNIT
															// MEASURE
															b.setAmount(UtilNumber.doubleToString(itemsdetail.getCurrentAmount(), Parametros.billeteraNroDecimales));
															b.setReservationAmount(UtilNumber.doubleToString(itemsdetail.getReservationAmount(), Parametros.billeteraNroDecimales));
															b.setTotalBalance(UtilNumber.doubleToString(itemsdetail.getCurrentAmount() + itemsdetail.getReservationAmount(), Parametros.billeteraNroDecimales));
															b.setUnitType(item.getMeasureUnitName());

															if (item.getMeasureUnit() != null) {
																FreeUnitMeasure freeUnitMeasure = hashFreeUnitMeasure.get(item.getMeasureUnit().trim());
																if (freeUnitMeasure != null) {
																	double total = UtilNumber.redondear((double) itemsdetail.getCurrentAmount() / freeUnitMeasure.getDividendo(), Parametros.billeteraNroDecimales);
																	b.setAmount(UtilNumber.doubleToString(total, Parametros.billeteraNroDecimales));
																	double total2 = UtilNumber.redondear((double) itemsdetail.getReservationAmount() / freeUnitMeasure.getDividendo(), Parametros.billeteraNroDecimales);
																	b.setReservationAmount(UtilNumber.doubleToString(total2, Parametros.billeteraNroDecimales));

																	b.setTotalBalance(UtilNumber.doubleToString(total - total2, Parametros.billeteraNroDecimales));
																	b.setUnitType(freeUnitMeasure.getUnidad());
																	reservationAmountSuma = reservationAmountSuma + total2;
																}
															}

															Date effectiveTime = UtilDate.stringToDate(itemsdetail.getEffectiveTime().trim(), Parametros.fechaFormatWs);
															b.setEffectiveTime(UtilDate.dateToString(effectiveTime, Parametros.fechaFormatPage));

															Date date = UtilDate.stringToDate(itemsdetail.getExpireTime().trim(), Parametros.fechaFormatWs);

															if (Parametros.billeteraValidarFechaExpiracion) {
																if (UtilDate.dateToString(date, "HH:mm:ss").equals("00:00:00")) {
																	Calendar restar = Calendar.getInstance();
																	restar.setTime(date);
																	restar.add(Calendar.SECOND, -1);

																	b.setExpireTime(UtilDate.dateToString(restar.getTime(), Parametros.fechaFormatPage));

																} else {
																	b.setExpireTime(UtilDate.dateToString(date, Parametros.fechaFormatPage));
																}
															} else {
																b.setExpireTime(UtilDate.dateToString(date, Parametros.fechaFormatPage));
															}

															// expireTime
															if (statusExpired == 1) {
																b.setStatusExpireTime(balanceExpireTime);
															} else {
																if (statusExpired == 2) {
																	Date f = UtilDate.stringToDate(b.getExpireTime(), Parametros.fechaFormatPage);
																	f = UtilDate.sumarRestarDiasFecha(f, Integer.valueOf(daysExpired));
																	b.setStatusExpireTime(UtilDate.dateToString(f, Parametros.fechaFormatPage));
																}
															}
															listFreeUnit.add(b);
														}
													}

													bal.setReservationAmount(UtilNumber.doubleToString(reservationAmountSuma, Parametros.billeteraNroDecimales));
													bal.setTotalBalance(UtilNumber.doubleToString(totalSuma - reservationAmountSuma, Parametros.billeteraNroDecimales));

													balances.add(bal);

													for (Balance itemsde : listFreeUnit) {
														balances.add(itemsde);
													}

												}
											}
										} else {
											log.warn("[isdn: " + getIsdn() + "] queryFreeUnitResult resuelto a nulo");
										}
										// }
									} else {
										log.info("[isdn: " + getIsdn() + "] queryFreeUnit ResultCode: " + queryFreeUnit.getResultHeader().getResultCode() + ", ResultDesc: " + queryFreeUnit.getResultHeader().getResultDesc());
										SysMessage.warn("Servicio BBServices, queryFreeUnit, Code: " + queryFreeUnit.getResultHeader().getResultCode() + ", Desc: " + queryFreeUnit.getResultHeader().getResultDesc(), null);
										guardarExcepcion(idAuditoria, "Servicio BBServices, queryFreeUnit, Code: " + queryFreeUnit.getResultHeader().getResultCode() + ", Desc: " + queryFreeUnit.getResultHeader().getResultDesc());
									}
								} else {
									if (queryFreeUnit != null) {
										log.warn("[isdn: " + getIsdn() + "] queryFreeUnit ResultHeader resuelto a nulo");
									} else {
										log.warn("[isdn: " + getIsdn() + "] queryFreeUnit queryFreeUnit resuelto a nulo");
									}
								}
							} finally {
								portNodo.setEnUso(false);
								portNodo.setFechaFin(new Date());
							}
						}

					} else {
						log.error("[isdn: " + getIsdn() + "] Port BBServices Billetera FREEUNIT EXPIRED resuelto a nulo");
					}

				} catch (AxisFault a) {
					String msg = a.getMessage();
					msg = msg.toLowerCase();
					if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase()) || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase()) || msg.contains("UnknownHostException".toLowerCase()) || msg.contains("No route to host".toLowerCase())
							|| msg.contains("Connection refused".toLowerCase())) {
						log.error("Error de conexion al servicio BBServices Billetera FREEUNIT EXPIRED: ", a);
						SysMessage.error("Error de conexion del servicio BBServices", null);
						guardarExcepcion(idAuditoria, "Error de conexion al servicio BBServices: " + stackTraceToString(a));
					} else {
						log.error("Error AxisFault: ", a);
						SysMessage.error("Error AxisFault de conexion al servicio BBServices", null);
						guardarExcepcion(idAuditoria, "Error AxisFault de conexion al servicio BBServices: " + stackTraceToString(a));
					}
				} catch (ServiceException e) {
					log.error("Error de servicio al conectarse al servicio BBServices Billetera FREEUNIT EXPIRED: ", e);
					SysMessage.error("Error de servicio al conectarse al servicio BBServices", null);
					guardarExcepcion(idAuditoria, "Error de servicio al conectarse al servicio BBServices: " + stackTraceToString(e));
				} catch (RemoteException e) {
					log.error("Error remoto al conectarse al servicio BBServices Billetera FREEUNIT EXPIRED: ", e);
					SysMessage.error("Error remoto al conectarse al servicio BBServices", null);
					guardarExcepcion(idAuditoria, "Error remoto al conectarse al servicio BBServices: " + stackTraceToString(e));
				} catch (Exception e) {
					log.error("Error al consultar BBServices Billetera FREEUNIT EXPIRED", e);
					SysMessage.error("Error al consultar BBServices", null);
					guardarExcepcion(idAuditoria, "Error al consultar BBServices: " + stackTraceToString(e));
				}

				// CONSULTA AL SERVICIO QUERYCUSTOMERINFO
				try {
					String wsdlLoc = Parametros.cugWsdl;

					URL url = new URL(UtilUrl.getIp(wsdlLoc));

					long ini = System.currentTimeMillis();

					if (url.getProtocol().equalsIgnoreCase("https")) {
						int puerto = url.getPort();
						String host = url.getHost();
						if (host.equalsIgnoreCase("localhost")) {
							host = "127.0.0.1";
						}
						validarCertificado(Parametros.cugPathKeystore, host + ":" + puerto);
					}

					NodoServicio<com.huawei.www.bme.cbsinterface.bcservices.BcServicsBindingStub> portNodo = ServicioCallingCircle.initPort(wsdlLoc, "QUERYCUSTOMERINFO" + login, Parametros.cugTimeOut);

					if (portNodo != null && portNodo.getPort() != null) {

						synchronized (portNodo) {

							try {

								com.huawei.www.bme.cbsinterface.bcservices.BcServicsBindingStub port = portNodo.getPort();

								com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo ownershipInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo();
								ownershipInfo.setBEID(Parametros.cugOwnerShipInfoId);
								com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo accessSecurity = new com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo();
								accessSecurity.setLoginSystemCode(Parametros.cugLoginSystemCode);
								accessSecurity.setPassword(Parametros.cugPassword);
								com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo operatorInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo();
								operatorInfo.setOperatorID(Parametros.cugOperatorId);
								com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat timeFormat = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat();
								timeFormat.setTimeType(Parametros.cugTimeType);

								SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
								String messageSeq = format.format(new Date());

								com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderAdditionalProperty[] additionalProperty = null;
								com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader requestHeader = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader(Parametros.cugVersion, "", messageSeq, ownershipInfo, accessSecurity, operatorInfo, "", "", timeFormat, additionalProperty);

								com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestMsg queryCustomerInfoRequestMsg = new com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestMsg();
								queryCustomerInfoRequestMsg.setRequestHeader(requestHeader);
								com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequest queryCustomerInfoRequest = new com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequest();
								com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestQueryObj queryObj = new com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestQueryObj();

								QueryCustomerInfoRequestQueryObjSubAccessCode subAccessCode = new QueryCustomerInfoRequestQueryObjSubAccessCode();
								subAccessCode.setPrimaryIdentity(isdn);
								queryObj.setSubAccessCode(subAccessCode);
								queryCustomerInfoRequest.setQueryObj(queryObj);
								queryCustomerInfoRequestMsg.setQueryCustomerInfoRequest(queryCustomerInfoRequest);
								com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoResultMsg queryCustomerInfo = port.queryCustomerInfo(queryCustomerInfoRequestMsg);

								long fin = System.currentTimeMillis();
								Long Con = null;
								if (portNodo.isPrimeraVez()) {
									Con = portNodo.getTiempoConexion();
								}
								log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio BCServices, queryCustomerInfo: " + (fin - ini) + " milisegundos");

								if (Parametros.saveRequestResponse) {
									try {
										saveXml(isdn, getLogin(), wsdlLoc, "queryCustomerInfo", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoria, Con, (fin - ini));
									} catch (Exception e) {
										log.warn("No se logro registrar los request y response del servicio BCServices, queryCustomerInfo: ", e);
									}
								}

								if (queryCustomerInfo != null && queryCustomerInfo.getResultHeader() != null) {
									if (queryCustomerInfo.getResultHeader().getResultCode().trim().equals("0")) {
										com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoResult queryCustomerInfoResult = queryCustomerInfo.getQueryCustomerInfoResult();

										if (queryCustomerInfoResult != null) {
											log.debug("queryCustomerInfo.getResultHeader: " + queryCustomerInfo.getResultHeader());
										} else {
											log.warn("[isdn: " + getIsdn() + "] queryCustomerInfoResult resuelto a nulo");
										}
									} else {
										log.info("[isdn: " + getIsdn() + "] queryCustomerInfo ResultCode: " + queryCustomerInfo.getResultHeader().getResultCode() + ", ResultDesc: " + queryCustomerInfo.getResultHeader().getResultDesc());
										SysMessage.warn("Servicio BCServices, queryCustomerInfo Code: " + queryCustomerInfo.getResultHeader().getResultCode() + ", Desc: " + queryCustomerInfo.getResultHeader().getResultDesc(), null);
										guardarExcepcion(idAuditoria, "Servicio BCServices, queryCustomerInfo Code: " + queryCustomerInfo.getResultHeader().getResultCode() + ", Desc: " + queryCustomerInfo.getResultHeader().getResultDesc());
									}
								} else {
									if (queryCustomerInfo != null) {
										log.warn("[isdn: " + getIsdn() + "] queryCustomerInfo ResultHeader resuelto a nulo");
									} else {
										log.warn("[isdn: " + getIsdn() + "] queryCustomerInfo queryCustomerInfo resuelto a nulo");
									}
								}

							} finally {
								portNodo.setEnUso(false);
								portNodo.setFechaFin(new Date());
							}
						}

					} else {
						log.error("[isdn: " + getIsdn() + "] Port BCServices resuelto a nulo");
					}

				} catch (AxisFault a) {
					String msg = a.getMessage();
					msg = msg.toLowerCase();
					if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase()) || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase()) || msg.contains("UnknownHostException".toLowerCase()) || msg.contains("No route to host".toLowerCase())
							|| msg.contains("Connection refused".toLowerCase())) {
						log.error("Error de conexion al servicio BCServices QueryCustomerInfo: ", a);
						SysMessage.error("Error de conexion del servicio BCServices", null);
						guardarExcepcion(idAuditoria, "Error de conexion al servicio BCServices: " + stackTraceToString(a));
					} else {
						log.error("Error AxisFault Numero Favorito: ", a);
						SysMessage.error("Error AxisFault de conexion del servicio BCServices", null);
						guardarExcepcion(idAuditoria, "Error AxisFault de conexion del servicio BCServices: " + stackTraceToString(a));
					}
				} catch (ServiceException e) {
					log.error("Error de servicio al conectarse al servicio BCServices Numero Favorito: ", e);
					SysMessage.error("Error de servicio al conectarse al servicio BCServices", null);
					guardarExcepcion(idAuditoria, "Error de servicio al conectarse al servicio BCServices: " + stackTraceToString(e));
				} catch (RemoteException e) {
					log.error("Error remoto al conectarse al servicio BCServices Numero Favorito: ", e);
					SysMessage.error("Error remoto al conectarse al servicio BCServices", null);
					guardarExcepcion(idAuditoria, "Error remoto al conectarse al servicio BCServices: " + stackTraceToString(e));
				}
				// FIN DE CONSULTA
			} else {
				SysMessage.warn("Isdn no válido", null);
			}

			SysMessage.info("Consulta finalizada.", null);

		} catch (Exception e) {
			log.error("Error al consultar billeteras", e);
			SysMessage.error("Error al consultar billeteras: " + e.getMessage(), null);
			guardarExcepcion(idAuditoria, "Error al consultar billeteras: " + stackTraceToString(e));
		}

	}

	public String stackTraceToString(Throwable e) {
		CharArrayWriter sw = new CharArrayWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		pw.close();

		return sw.toString(); // stack trace a
	}

	private long guardarAuditoria(Calendar fechaConsulta, String usuario, String ip, String vista, int comando, String isdn, List<VcLogAdicional> adicionales) {
		try {

			VcLogAuditoria item = new VcLogAuditoria();
			item.setFechaConsulta(fechaConsulta);
			item.setIsdn(isdn);
			item.setUsuario(usuario);
			item.setIp(ip);
			item.setVista(vista);
			item.setComando(comando);
			item.setAdicional("Y");
			item.setFechaCreacion(Calendar.getInstance());
			if (adicionales == null || adicionales.size() == 0) {
				item.setAdicional("N");
			}

			blAuditoria.guardarAuditoria(item, adicionales);
			CodigoAuditoria = item.getIdLogAuditoria();
			return item.getIdLogAuditoria();

		} catch (Exception e) {
			log.error("Error al guardar auditoria: ", e);
		}

		return -1;
	}

	public void cargarCallingCircle() {
		log.debug("Ingresando a cargar calling circle");
		long idAuditoria = 0;
		try {

			callingCircles = new ArrayList<CallingCircle>();

			if (login == null || login.trim().isEmpty()) {
				SysMessage.warn("Usuario no válido", null);
				return;
			}

			if (circleName != null && !circleName.trim().isEmpty()) {

				List<VcLogAdicional> adicionales = new ArrayList<>();
				VcLogAdicional ad = new VcLogAdicional();
				ad.setCodigo("Group Key");
				ad.setValor(circleName);
				adicionales.add(ad);
				idAuditoria = guardarAuditoria(Calendar.getInstance(), this.login, this.ip, Parametros.vistaCallingCircle, Parametros.comandoCallingCircle, "N/A", adicionales);

				if (idAuditoria <= 0) {
					SysMessage.error("Error al guardar log de auditoria", null);
					return;
				}

				disableIncidente = false;
				vistaIncidente = DescriptorBitacora.CALLING_CIRCLE;
				try {
					controlerBitacora.accion(DescriptorBitacora.CALLING_CIRCLE, "Se hizo consulta de Calling Circle, Group Key: " + circleName);
				} catch (Exception e) {
					log.error("Error al guardar bitacora en el sistema: ", e);
					SysMessage.error("Error al guardar bitacora", null);
				}

				String wsdlLoc = Parametros.cugWsdl;

				URL url = new URL(UtilUrl.getIp(wsdlLoc));
				long ini = System.currentTimeMillis();

				if (url.getProtocol().equalsIgnoreCase("https")) {
					int puerto = url.getPort();
					String host = url.getHost();
					if (host.equalsIgnoreCase("localhost")) {
						host = "127.0.0.1";
					}
					validarCertificado(Parametros.cugPathKeystore, host + ":" + puerto);
				}

				NodoServicio<com.huawei.www.bme.cbsinterface.bcservices.BcServicsBindingStub> portNodo = ServicioCallingCircle.initPort(wsdlLoc, "CUG" + login, Parametros.cugTimeOut);

				if (portNodo != null && portNodo.getPort() != null) {

					synchronized (portNodo) {
						try {
							com.huawei.www.bme.cbsinterface.bcservices.BcServicsBindingStub port = portNodo.getPort();

							com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo ownershipInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo();
							ownershipInfo.setBEID(Parametros.cugOwnerShipInfoId);
							com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo accessSecurity = new com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo();
							accessSecurity.setLoginSystemCode(Parametros.cugLoginSystemCode);
							accessSecurity.setPassword(Parametros.cugPassword);
							com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo operatorInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo();
							operatorInfo.setOperatorID(Parametros.cugOperatorId);
							com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat timeFormat = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat();
							timeFormat.setTimeType(Parametros.cugTimeType);

							SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
							String messageSeq = format.format(new Date());

							com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderAdditionalProperty[] additionalProperty = null;
							com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader requestHeader = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader(Parametros.cugVersion, "", messageSeq, ownershipInfo, accessSecurity, operatorInfo, "", "", timeFormat, additionalProperty);

							com.huawei.www.bme.cbsinterface.bccommon.SubAccessCode subAccessCode = new com.huawei.www.bme.cbsinterface.bccommon.SubAccessCode();
							subAccessCode.setPrimaryIdentity(getIsdn());

							com.huawei.www.bme.cbsinterface.bccommon.SubGroupAccessCode subGroupAccessCode = new com.huawei.www.bme.cbsinterface.bccommon.SubGroupAccessCode();
							subGroupAccessCode.setSubGroupKey(circleName);

							BigInteger totalNumber = new BigInteger("0");
							BigInteger beginRowNum = new BigInteger("0");
							BigInteger fetchRowNum = new BigInteger(Parametros.callingCircleFetchRowNum);
							com.huawei.www.bme.cbsinterface.bcservices.QueryGroupMemberListRequest queryGroupMemberListRequest = new com.huawei.www.bme.cbsinterface.bcservices.QueryGroupMemberListRequest(subGroupAccessCode, totalNumber, beginRowNum, fetchRowNum);
							com.huawei.www.bme.cbsinterface.bcservices.QueryGroupMemberListRequestMsg queryGroupMemberListRequestMsg = new com.huawei.www.bme.cbsinterface.bcservices.QueryGroupMemberListRequestMsg(requestHeader, queryGroupMemberListRequest);

							com.huawei.www.bme.cbsinterface.bcservices.QueryGroupMemberListResultMsg queryGroupMemberList = port.queryGroupMemberList(queryGroupMemberListRequestMsg);

							long fin = System.currentTimeMillis();
							Long Con = null;
							if (portNodo.isPrimeraVez()) {
								Con = portNodo.getTiempoConexion();
							}
							log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio BCServices, queryGroupMemberList: " + (fin - ini) + " milisegundos");

							if (Parametros.saveRequestResponse) {
								try {
									saveXml("N/A", getLogin(), wsdlLoc, "queryGroupMemberList", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoria, Con, (fin - ini));
								} catch (Exception e) {
									log.warn("No se logro registrar los request y response del servicio BCServices, queryGroupMemberList: ", e);
								}
							}

							if (queryGroupMemberList != null && queryGroupMemberList.getResultHeader() != null) {
								if (queryGroupMemberList.getResultHeader().getResultCode().trim().equals("0")) {

									com.huawei.www.bme.cbsinterface.bcservices.QueryGroupMemberListResultGroupMemberList[] groupMemberList = queryGroupMemberList.getQueryGroupMemberListResult().getGroupMemberList();

									if (groupMemberList != null && groupMemberList.length > 0) {
										for (com.huawei.www.bme.cbsinterface.bcservices.QueryGroupMemberListResultGroupMemberList items : groupMemberList) {
											CallingCircle cc = new CallingCircle();
											List<CallingCircleProperties> callingCirclesproperties = new ArrayList<CallingCircleProperties>();
											cc.setprimaryIdentity(items.getPrimaryIdentity());

											com.huawei.www.bme.cbsinterface.bccommon.SimpleProperty[] memberProperty = items.getMemberProperty();
											for (com.huawei.www.bme.cbsinterface.bccommon.SimpleProperty simpleProperty : memberProperty) {
												CallingCircleProperties cp = new CallingCircleProperties();
												cp.setCode(simpleProperty.getCode());
												cp.setValue(simpleProperty.getValue());
												callingCirclesproperties.add(cp);
											}
											cc.setCallingCirclesProperties(callingCirclesproperties);
											callingCircles.add(cc);
										}
									}
								} else {
									log.info("[isdn: " + getIsdn() + "] queryGroupMemberList ResultCode: " + queryGroupMemberList.getResultHeader().getResultCode() + ", ResultDesc: " + queryGroupMemberList.getResultHeader().getResultDesc());
									SysMessage.warn("Servicio BCServices, queryGroupMemberList Code: " + queryGroupMemberList.getResultHeader().getResultCode() + ", Desc: " + queryGroupMemberList.getResultHeader().getResultDesc(), null);
									guardarExcepcion(idAuditoria, "Servicio BCServices, queryGroupMemberList Code: " + queryGroupMemberList.getResultHeader().getResultCode() + ", Desc: " + queryGroupMemberList.getResultHeader().getResultDesc());
								}
							} else {
								if (queryGroupMemberList != null) {
									log.warn("[isdn: " + getIsdn() + "] queryGroupMemberList queryGroupMemberList resuelto a nulo");
								} else {
									log.warn("[isdn: " + getIsdn() + "] queryGroupMemberList ResultHeader resuelto a nulo");
								}
							}

							// INICIO querySubLifeCycle

							com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequest querySubLifeCycleRequest = new com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequest(subAccessCode);
							com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequestMsg querySubLifeCycleRequestMsg = new com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequestMsg(requestHeader, querySubLifeCycleRequest);

							com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleResultMsg querySubLifeCycle = port.querySubLifeCycle(querySubLifeCycleRequestMsg);

							long fin2 = System.currentTimeMillis();
							Long Con2 = null;
							if (portNodo.isPrimeraVez()) {
								Con2 = portNodo.getTiempoConexion();
							}
							log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio BCServices, querySubLifeCycle: " + (fin2 - ini) + " milisegundos");
							if (Parametros.saveRequestResponse) {
								try {
									saveXml(isdn, getLogin(), wsdlLoc, "querySubLifeCycle", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoria, Con2, (fin2 - ini));
								} catch (Exception e) {
									log.warn("No se logro registrar los request y response del servicio BCServices, querySubLifeCycle:", e);
								}
							}

							if (querySubLifeCycle != null && querySubLifeCycle.getResultHeader() != null) {
								if (querySubLifeCycle.getResultHeader().getResultCode().trim().equals("0")) {
									com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleResult querySubLifeCycleResult = querySubLifeCycle.getQuerySubLifeCycleResult();

									if (querySubLifeCycleResult != null) {
										log.debug("querySubLifeCycle.getResultHeader: " + querySubLifeCycle.getResultHeader());
									} else {
										log.warn("[isdn: " + getIsdn() + "] QuerySubLifeCycleResult resuelto a nulo");
									}
								} else {
									log.info("[isdn: " + getIsdn() + "] querySubLifeCycle ResultCode: " + querySubLifeCycle.getResultHeader().getResultCode() + ", ResultDesc: " + querySubLifeCycle.getResultHeader().getResultDesc());
									SysMessage.warn("Servicio BCServices, querySubLifeCycle Code: " + querySubLifeCycle.getResultHeader().getResultCode() + ", Desc: " + querySubLifeCycle.getResultHeader().getResultDesc(), null);
									guardarExcepcion(idAuditoria, "Servicio BCServices, querySubLifeCycle Code: " + querySubLifeCycle.getResultHeader().getResultCode() + ", Desc: " + querySubLifeCycle.getResultHeader().getResultDesc());
								}
							} else {
								if (querySubLifeCycle != null) {
									log.warn("[isdn: " + getIsdn() + "] querySubLifeCycle ResultHeader resuelto a nulo");
								} else {
									log.warn("[isdn: " + getIsdn() + "] querySubLifeCycle QuerySubLifeCycle resuelto a nulo");
								}
							}

							// FIN

							// INICIO QUERYCUSTOMERINFO

							com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestMsg queryCustomerInfoRequestMsg = new com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestMsg();
							queryCustomerInfoRequestMsg.setRequestHeader(requestHeader);
							com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequest queryCustomerInfoRequest = new com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequest();
							com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestQueryObj queryObj = new com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestQueryObj();

							QueryCustomerInfoRequestQueryObjSubAccessCode subAccessCode2 = new QueryCustomerInfoRequestQueryObjSubAccessCode();
							subAccessCode2.setPrimaryIdentity(isdn);
							queryObj.setSubAccessCode(subAccessCode2);
							queryCustomerInfoRequest.setQueryObj(queryObj);
							queryCustomerInfoRequestMsg.setQueryCustomerInfoRequest(queryCustomerInfoRequest);
							com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoResultMsg queryCustomerInfo = port.queryCustomerInfo(queryCustomerInfoRequestMsg);

							if (queryCustomerInfo != null) {
								log.debug("queryCustomerInfo.getResultHeader: " + queryCustomerInfo.getResultHeader());
							}

							long fin3 = System.currentTimeMillis();
							Long Con3 = null;
							if (portNodo.isPrimeraVez()) {
								Con3 = portNodo.getTiempoConexion();
							}
							log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio BCServices, queryCustomerInfo: " + (fin3 - ini) + " milisegundos");

							if (Parametros.saveRequestResponse) {
								try {
									saveXml(isdn, getLogin(), wsdlLoc, "queryCustomerInfo", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoria, Con3, (fin3 - ini));
								} catch (Exception e) {
									log.warn("No se logro registrar los request y response del servicio BCServices, queryCustomerInfo: ", e);
								}
							}

							if (queryCustomerInfo != null && queryCustomerInfo.getResultHeader() != null) {
								if (queryCustomerInfo.getResultHeader().getResultCode().trim().equals("0")) {
									com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoResult queryCustomerInfoResult = queryCustomerInfo.getQueryCustomerInfoResult();

									if (queryCustomerInfoResult != null) {
										log.debug("queryCustomerInfo.getResultHeader: " + queryCustomerInfo.getResultHeader());
									} else {
										log.warn("[isdn: " + getIsdn() + "] queryCustomerInfoResult resuelto a nulo");
									}
								} else {
									log.info("[isdn: " + getIsdn() + "] queryCustomerInfo ResultCode: " + queryCustomerInfo.getResultHeader().getResultCode() + ", ResultDesc: " + queryCustomerInfo.getResultHeader().getResultDesc());
									SysMessage.warn("Servicio BCServices, queryCustomerInfo Code: " + queryCustomerInfo.getResultHeader().getResultCode() + ", Desc: " + queryCustomerInfo.getResultHeader().getResultDesc(), null);
									guardarExcepcion(idAuditoria, "Servicio BCServices, queryCustomerInfo Code: " + queryCustomerInfo.getResultHeader().getResultCode() + ", Desc: " + queryCustomerInfo.getResultHeader().getResultDesc());
								}
							} else {
								if (queryCustomerInfo != null) {
									log.warn("[isdn: " + getIsdn() + "] queryCustomerInfo ResultHeader resuelto a nulo");
								} else {
									log.warn("[isdn: " + getIsdn() + "] queryCustomerInfo queryCustomerInfo resuelto a nulo");
								}
							}

							// FIN QUERYCUSTOMERINFO
						} finally {
							portNodo.setEnUso(false);
							portNodo.setFechaFin(new Date());
						}
					}
				} else {
					log.error("[isdn: " + getIsdn() + "] Port Calling Circle resuelto a nulo");
				}

				SysMessage.info("Consulta finalizada.", null);

			} else {
				SysMessage.warn("Circle Name no valido", null);
			}

		} catch (AxisFault a) {
			String msg = a.getMessage();
			msg = msg.toLowerCase();
			if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase()) || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase()) || msg.contains("UnknownHostException".toLowerCase()) || msg.contains("No route to host".toLowerCase())
					|| msg.contains("Connection refused".toLowerCase())) {
				log.error("conexion del servicio BCServices Calling Circle: ", a);
				SysMessage.error("Error de conexion del servicio BCServices", null);
				guardarExcepcion(idAuditoria, "conexion del servicio BCServices: " + stackTraceToString(a));
			} else {
				log.error("Error AxisFault Calling Circle: ", a);
				SysMessage.error("Error AxisFault de conexion al servicio BCServices", null);
				guardarExcepcion(idAuditoria, "Error AxisFault de conexion al servicio BCServices: " + stackTraceToString(a));
			}
		} catch (ServiceException e) {
			callingCircles = new ArrayList<CallingCircle>();
			log.error("Error de servicio al conectarse al servicio BCServices Calling Circle: ", e);
			SysMessage.error("Error de servicio al conectarse al servicio BCServices", null);
			guardarExcepcion(idAuditoria, "Error de servicio al conectarse al servicio BCServices: " + stackTraceToString(e));
		} catch (RemoteException e) {
			callingCircles = new ArrayList<CallingCircle>();
			log.error("Error remoto al conectarse al servicio BCServices Calling Circle: ", e);
			SysMessage.error("Error remoto al conectarse al servicio BCServices", null);
			guardarExcepcion(idAuditoria, "Error remoto al conectarse al servicio BCServices: " + stackTraceToString(e));
		} catch (Exception e) {
			callingCircles = new ArrayList<CallingCircle>();
			log.error("Error al consultar BCServices Calling Circle", e);
			SysMessage.error("Error al consultar BCServices", null);
			guardarExcepcion(idAuditoria, "Error al consultar BCServices: " + stackTraceToString(e));
		}
	}

	public void onRowSelect(SelectEvent event) {
		setCallingCirclesProperties(new ArrayList<CallingCircleProperties>());
		List<CallingCircleProperties> list = getSelectedPrimaryIdentity().getCallingCirclesProperties();
		for (CallingCircleProperties item : list) {
			CallingCircleProperties cp = new CallingCircleProperties();
			cp.setCode(item.getCode());
			cp.setValue(item.getValue());
			getCallingCirclesProperties().add(cp);
		}
	}

	public void onRowSelectHistory() {
		log.debug("onRowSelectHistory.....");

		try {

			if (selectedHistory != null) {
				log.debug(selectedHistory.toString());
				if (getIsdn() != null && !getIsdn().trim().isEmpty()) {
					// FacesContext.getCurrentInstance().addMessage("messageHistorial",
					// new FacesMessage(FacesMessage.SEVERITY_INFO, "Info!", "Id
					// Historial: " + selectedHistory.getId()));
					propertys = new ArrayList<Property>();
					listBalanceDetail = new ArrayList<BalanceDetail>();

					if (selectedHistory.getCategoria().equals("Adjustment")) { // ADJUSTMENT

						propertys.add(new Property("Category", selectedHistory.getCategory(), ""));
						propertys.add(new Property("PrimaryIdentity", selectedHistory.getPrimaryIdentity(), ""));
						Date date = UtilDate.stringToDate(selectedHistory.getTradeTime(), Parametros.fechaFormatWs);
						propertys.add(new Property("TradeTime", UtilDate.dateToString(date, Parametros.fechaFormatPage), ""));
						propertys.add(new Property("Remark", selectedHistory.getRemark(), ""));

						String channel = hashAdjustmentChannelId.get(selectedHistory.getChannelId());
						if (channel != null)
							propertys.add(new Property("ChannelID", channel, ""));
						else
							propertys.add(new Property("ChannelID", selectedHistory.getChannelId(), ""));

						propertys.add(new Property("TransID", selectedHistory.getTransId(), ""));
						propertys.add(new Property("ExtTransID", selectedHistory.getExTransId(), ""));

						String additionalProperty = selectedHistory.getAjAdditionalProperty();
						if (additionalProperty != null && !additionalProperty.isEmpty()) {
							ToXml xml = new ToXml();
							@SuppressWarnings("unchecked")
							ArrayList<Nota> list = (ArrayList<Nota>) xml.xmlToObject(additionalProperty);
							if (list != null) {
								for (Nota nota : list) {
									propertys.add(new Property(nota.getName(), nota.getValue(), ""));
								}
							}
						}

						cargarBalanceDetailAdjustment();

					} else if (selectedHistory.getCategoria().equals("Recharge")) { // RECHARGE
						propertys.add(new Property("Category", selectedHistory.getCategoria(), ""));
						Date date = UtilDate.stringToDate(selectedHistory.getReTradeTime(), Parametros.fechaFormatWs);
						propertys.add(new Property("TradeTime", UtilDate.dateToString(date, Parametros.fechaFormatPage), ""));
						propertys.add(new Property("PrimaryIdentity", selectedHistory.getRePrimaryIdentity(), ""));

						if (hashRechargeType.get(selectedHistory.getRechargeType()) != null)
							propertys.add(new Property("RechargeType", hashRechargeType.get(selectedHistory.getRechargeType()), ""));
						else
							propertys.add(new Property("RechargeType", selectedHistory.getRechargeType(), ""));

						if (hashRechargeChannelId.get(selectedHistory.getRechargeChannelId()) != null)
							propertys.add(new Property("RechargeChannelID", hashRechargeChannelId.get(selectedHistory.getRechargeChannelId()), ""));
						else
							propertys.add(new Property("RechargeChannelID", selectedHistory.getRechargeChannelId(), ""));

						if (selectedHistory.getRechargeAmount() != null) {
							if (!selectedHistory.getRechargeAmount().trim().isEmpty()) {
								double total = UtilNumber.redondear((double) (new Long(selectedHistory.getRechargeAmount()) / (double) Parametros.historialDivisor), Parametros.historialNroDecimales);
								propertys.add(new Property("RechargeAmount", UtilNumber.doubleToString(total, Parametros.historialNroDecimales), ""));
							} else
								propertys.add(new Property("RechargeAmount", "", ""));
						}
						if (selectedHistory.getCurrencyId() != null) {
							String cd = getCurrencyDescription(Integer.parseInt(selectedHistory.getCurrencyId().trim()));
							if (cd != null)
								propertys.add(new Property("CurrencyID", cd, ""));
							else
								propertys.add(new Property("CurrencyID", selectedHistory.getCurrencyId(), ""));
						}

						propertys.add(new Property("CardSequence", selectedHistory.getCardSequence(), ""));
						propertys.add(new Property("RechargeReason", selectedHistory.getRechargeReason(), ""));

						if (hashResultCode.get(selectedHistory.getResultCode()) != null)
							propertys.add(new Property("ResultCode", hashResultCode.get(selectedHistory.getResultCode()), ""));
						else
							propertys.add(new Property("ResultCode", selectedHistory.getResultCode(), ""));

						propertys.add(new Property("TransID", selectedHistory.getReTransId(), ""));
						propertys.add(new Property("ExtTransID", selectedHistory.getReExTransId(), ""));
						propertys.add(new Property("ExtRechargeType", selectedHistory.getExRechargeType(), ""));
						propertys.add(new Property("RechargeTax", selectedHistory.getRechargeTax(), ""));
						propertys.add(new Property("RechargePenalty", selectedHistory.getRechargePenalty(), ""));

						if (hashReversalFlag.get(selectedHistory.getReversalFlag()) != null)
							propertys.add(new Property("ReversalFlag", hashReversalFlag.get(selectedHistory.getReversalFlag()), ""));
						else
							propertys.add(new Property("ReversalFlag", selectedHistory.getReversalFlag(), ""));

						propertys.add(new Property("ReversalReason", selectedHistory.getReversalReason(), ""));

						Date dateTrade = UtilDate.stringToDate(selectedHistory.getReversalTime(), Parametros.fechaFormatWs);
						propertys.add(new Property("ReversalTime", UtilDate.dateToString(dateTrade, Parametros.fechaFormatPage), ""));

						String additionalProperty = selectedHistory.getReAdditionalProperty();
						if (additionalProperty != null && !additionalProperty.isEmpty()) {
							ToXml xml = new ToXml();
							@SuppressWarnings("unchecked")
							ArrayList<Nota> list = (ArrayList<Nota>) xml.xmlToObject(additionalProperty);
							if (list != null) {
								for (Nota nota : list) {
									propertys.add(new Property(nota.getName(), nota.getValue(), ""));
								}
							}
						}

						cargarBalanceDetailRecharge();

					} else { // QUERY CDR
						propertys.add(new Property("CdrSeq", selectedHistory.getCdrSecuencia(), ""));
						propertys.add(new Property("ServiceCategory", selectedHistory.getCategoria(), ""));
						propertys.add(new Property("SeriveType", selectedHistory.getTipoServicio(), ""));
						propertys.add(new Property("ServiceTypeName", selectedHistory.getDescripcionServicio(), ""));
						propertys.add(new Property("OtherNumber", selectedHistory.getOtherNumber(), ""));
						propertys.add(new Property("FlowType", selectedHistory.getTipoFlujo(), ""));
						propertys.add(new Property("RoamFlag", selectedHistory.getRoamFlag(), ""));
						propertys.add(new Property("CallingCellID", selectedHistory.getCallingCellId(), ""));
						propertys.add(new Property("CalledCellID", selectedHistory.getCalledCellId(), ""));
						propertys.add(new Property("SpecialNumberIndicator", selectedHistory.getSpecialNumber(), ""));
						propertys.add(new Property("StartTime", selectedHistory.getStartDate(), ""));
						propertys.add(new Property("EndTime", selectedHistory.getEndDate(), ""));

						propertys.add(new Property("RefundIndicator", selectedHistory.getRefundIndicator(), ""));
						// String colorCeleste = "celeste";
						// String colorVerde = "verde";
						String colorCeleste = "#7d9afb";
						String colorVerde = "#64e674";

						boolean existFreeUnit = false;
						if (selectedHistory.getMeasureUnitCdr() != null) {
							FreeUnitMeasure freeUnitMeasure = hashFreeUnitMeasure.get(selectedHistory.getMeasureUnitCdr().intValue() + "");

							if (freeUnitMeasure != null) {
								existFreeUnit = true;
								double total = 0;
								// if (selectedHistory.getActualVolume() != null
								// &&
								// !selectedHistory.getActualVolume().trim().isEmpty())
								// {
								// log.debug("ActualVolume encontrado");
								// total = UtilNumber.redondear((double) new
								// Long(selectedHistory.getActualVolume()) /
								// freeUnitMeasure.getDividendo(),
								// Parametros.historialNroDecimales);
								// propertys.add(new Property("ActualVolume",
								// UtilNumber.doubleToString(total,
								// Parametros.historialNroDecimales),
								// colorCeleste));
								// } else {
								// log.warn("ActualVolume no encontrado");
								// propertys.add(new Property("ActualVolume",
								// "", colorCeleste));
								// }

								if (selectedHistory.getRatingVolume() != null && !selectedHistory.getRatingVolume().trim().isEmpty()) {
									log.debug("RatingVolume encontrado");
									total = UtilNumber.redondear((double) new Long(selectedHistory.getRatingVolume()) / freeUnitMeasure.getDividendo(), Parametros.historialNroDecimales);
									propertys.add(new Property("RatingVolume", UtilNumber.doubleToString(total, Parametros.historialNroDecimales), colorCeleste));
								} else {
									log.warn("RatingVolume no encontrado");
									propertys.add(new Property("RatingVolume", "", colorCeleste));
								}

								if (selectedHistory.getFreeVolume() != null && !selectedHistory.getFreeVolume().trim().isEmpty()) {
									log.debug("FreeUnitVolume encontrado");
									total = UtilNumber.redondear((double) new Long(selectedHistory.getFreeVolume()) / freeUnitMeasure.getDividendo(), Parametros.historialNroDecimales);
									propertys.add(new Property("FreeUnitVolume", UtilNumber.doubleToString(total, Parametros.historialNroDecimales), colorCeleste));
								} else {
									log.warn("FreeUnitVolume no encontrado");
									propertys.add(new Property("FreeUnitVolume", "", colorCeleste));
								}

								if (selectedHistory.getActualChargeAmt() != null && !selectedHistory.getActualChargeAmt().trim().isEmpty()) {
									long ratingVolume = 0;
									if (selectedHistory.getRatingVolume() != null && !selectedHistory.getRatingVolume().trim().isEmpty()) {
										ratingVolume = new Long(selectedHistory.getRatingVolume());
									}
									long freeVolume = 0;
									if (selectedHistory.getFreeVolume() != null && !selectedHistory.getFreeVolume().trim().isEmpty()) {
										freeVolume = new Long(selectedHistory.getFreeVolume());
									}
									long resta = ratingVolume - freeVolume;
									double rvfv = UtilNumber.redondear((double) resta / freeUnitMeasure.getDividendo(), Parametros.historialNroDecimales);
									propertys.add(new Property("BalanceVolume", UtilNumber.doubleToString(rvfv, Parametros.historialNroDecimales), colorCeleste));
								} else {
									propertys.add(new Property("BalanceVolume", "", colorCeleste));
								}

								propertys.add(new Property("MeasureUnit", freeUnitMeasure.getUnidad(), colorCeleste));

							} else {
								log.warn("No se encontro en hashFreeUnitMeasure el cdr: " + selectedHistory.getMeasureUnitCdr().intValue());

								// propertys.add(new Property("ActualVolume",
								// selectedHistory.getActualVolume(),
								// colorCeleste));
								// propertys.add(new Property("RatingVolume",
								// selectedHistory.getRatingVolume(),
								// colorCeleste));
								// propertys.add(new Property("FreeUnitVolume",
								// selectedHistory.getFreeVolume(),
								// colorCeleste));
								// propertys.add(new Property("MeasureUnit",
								// selectedHistory.getMeasureUnitCdr().intValue()
								// + "", colorCeleste));
							}

						}
						if (!existFreeUnit) {
							log.warn("No existe freeUnitMeasure se van a llenar los datos vacios");
							// propertys.add(new Property("ActualVolume",
							// selectedHistory.getActualVolume(),
							// colorCeleste));
							propertys.add(new Property("RatingVolume", selectedHistory.getRatingVolume(), colorCeleste));
							propertys.add(new Property("FreeVolume", selectedHistory.getFreeVolume(), colorCeleste));
							propertys.add(new Property("BalanceVolume", "", colorCeleste));
							if (selectedHistory.getMeasureUnitCdr() != null)
								propertys.add(new Property("MeasureUnit", selectedHistory.getMeasureUnitCdr().intValue() + "", colorCeleste));
							propertys.add(new Property("CurrentAmount", "", colorCeleste));
						}

						if (selectedHistory.getActualChargeAmt() != null && !selectedHistory.getActualChargeAmt().trim().isEmpty()) {
							double total = UtilNumber.redondear((double) new Long(selectedHistory.getActualChargeAmt()) / (double) Parametros.historialDivisor, Parametros.historialNroDecimales);
							propertys.add(new Property("ActualChargeAmt", UtilNumber.doubleToString(total, Parametros.historialNroDecimales), colorVerde));
						} else
							propertys.add(new Property("ActualChargeAmt", "", colorVerde));

						if (selectedHistory.getFreeChargeAmt() != null && !selectedHistory.getFreeChargeAmt().trim().isEmpty()) {
							double total = UtilNumber.redondear((double) new Long(selectedHistory.getFreeChargeAmt()) / (double) Parametros.historialDivisor, Parametros.historialNroDecimales);
							propertys.add(new Property("FreeChargeAmt", UtilNumber.doubleToString(total, Parametros.historialNroDecimales), colorVerde));
						} else
							propertys.add(new Property("FreeChargeAmt", "", colorVerde));

						propertys.add(new Property("TaxCode", selectedHistory.getActualTaxCode(), colorVerde));

						if (selectedHistory.getActualTaxAmt() != null && !selectedHistory.getActualTaxAmt().trim().isEmpty()) {
							double total = UtilNumber.redondear((double) new Long(selectedHistory.getActualTaxAmt()) / (double) Parametros.historialDivisor, Parametros.historialNroDecimales);
							propertys.add(new Property("TaxAmt", UtilNumber.doubleToString(total, Parametros.historialNroDecimales), colorVerde));
						} else
							propertys.add(new Property("TaxAmt", "", colorVerde));

						if (selectedHistory.getCurrencyIdCdr() != null && !selectedHistory.getCurrencyIdCdr().trim().isEmpty()) {
							String cd = getCurrencyDescription(Integer.parseInt(selectedHistory.getCurrencyIdCdr()));
							if (cd != null)
								propertys.add(new Property("CurrencyID", cd, colorVerde));
							else
								propertys.add(new Property("CurrencyID", selectedHistory.getCurrencyIdCdr(), colorVerde));
						} else
							propertys.add(new Property("CurrencyID", "", colorVerde));

						String additionalProperty = selectedHistory.getAdditionalProperty();
						if (additionalProperty != null && !additionalProperty.isEmpty()) {
							ToXml xml = new ToXml();
							@SuppressWarnings("unchecked")
							ArrayList<Nota> list = (ArrayList<Nota>) xml.xmlToObject(additionalProperty);
							if (list != null) {
								for (Nota nota : list) {
									propertys.add(new Property(nota.getName(), nota.getValue(), ""));
								}
							}
						}
						cargarBalanceDetailCDR();
					}

				} else {
					SysMessage.warn("Nro de Cuenta no valido", null);
				}
			} else {
				log.warn("selectedHistory is null");
				SysMessage.warn("Item Historial no seleccionado", null);
			}

		} catch (Exception e) {
			log.error("Error al mostrar propertys de historial: ", e);
			SysMessage.error("Error al obtener datos: " + e.getMessage(), null);
		}

	}

	public void onRowUnselectHistory() {
		log.debug("onRowUnselectHistory.....");
		selectedHistory = null;
		propertys = new ArrayList<Property>();
		setListBalanceDetail(new ArrayList<BalanceDetail>());
	}

	public void cargarCug() {
		log.debug("Ingresando a cargar cug");
		long idAuditoria = 0;
		try {

			cug = new Cug();
			if (login == null || login.trim().isEmpty()) {
				SysMessage.warn("Usuario no válido", null);
				return;
			}

			if (getIsdn() != null && !getIsdn().trim().isEmpty()) {

				idAuditoria = guardarAuditoria(Calendar.getInstance(), this.login, this.ip, Parametros.vistaCug, Parametros.comandoCug, this.isdn, null);
				if (idAuditoria <= 0) {
					SysMessage.error("Error al guardar log de auditoria", null);
					return;
				}

				disableIncidente = false;
				vistaIncidente = DescriptorBitacora.CUG;
				try {
					controlerBitacora.accion(DescriptorBitacora.CUG, "Se hizo consulta de Cug, isdn: " + isdn);
				} catch (Exception e) {
					log.error("Error al guardar bitacora en el sistema: ", e);
					SysMessage.error("Error al guardar bitacora", null);
				}

				String wsdlLoc = Parametros.cugWsdl;

				URL url = new URL(UtilUrl.getIp(wsdlLoc));
				long ini = System.currentTimeMillis();

				if (url.getProtocol().equalsIgnoreCase("https")) {
					int puerto = url.getPort();
					String host = url.getHost();
					if (host.equalsIgnoreCase("localhost")) {
						host = "127.0.0.1";
					}
					validarCertificado(Parametros.cugPathKeystore, host + ":" + puerto);
				}

				NodoServicio<com.huawei.www.bme.cbsinterface.bcservices.BcServicsBindingStub> portNodo = ServicioCallingCircle.initPort(wsdlLoc, "CUG" + login, Parametros.cugTimeOut);

				if (portNodo != null && portNodo.getPort() != null) {

					synchronized (portNodo) {
						try {
							com.huawei.www.bme.cbsinterface.bcservices.BcServicsBindingStub port = portNodo.getPort();

							com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo ownershipInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo();
							ownershipInfo.setBEID(Parametros.cugOwnerShipInfoId);
							com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo accessSecurity = new com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo();
							accessSecurity.setLoginSystemCode(Parametros.cugLoginSystemCode);
							accessSecurity.setPassword(Parametros.cugPassword);
							com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo operatorInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo();
							operatorInfo.setOperatorID(Parametros.cugOperatorId);
							com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat timeFormat = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat();
							timeFormat.setTimeType(Parametros.cugTimeType);

							SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
							String messageSeq = format.format(new Date());

							com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderAdditionalProperty[] additionalProperty = null;
							com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader requestHeader = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader(Parametros.cugVersion, "", messageSeq, ownershipInfo, accessSecurity, operatorInfo, "", "", timeFormat, additionalProperty);

							com.huawei.www.bme.cbsinterface.bccommon.SubAccessCode subAccessCode = new com.huawei.www.bme.cbsinterface.bccommon.SubAccessCode();
							subAccessCode.setPrimaryIdentity(getIsdn());
							String subGroupType = null;

							com.huawei.www.bme.cbsinterface.bcservices.QueryGroupListBySubscriberRequest queryGroupListBySubscriberRequest = new com.huawei.www.bme.cbsinterface.bcservices.QueryGroupListBySubscriberRequest(subAccessCode, subGroupType);
							com.huawei.www.bme.cbsinterface.bcservices.QueryGroupListBySubscriberRequestMsg queryGroupListBySubscriberRequestMsg = new com.huawei.www.bme.cbsinterface.bcservices.QueryGroupListBySubscriberRequestMsg(requestHeader, queryGroupListBySubscriberRequest);

							com.huawei.www.bme.cbsinterface.bcservices.QueryGroupListBySubscriberResultMsg queryGroupListBySubscriber = port.queryGroupListBySubscriber(queryGroupListBySubscriberRequestMsg);

							long fin = System.currentTimeMillis();
							Long Con = null;
							if (portNodo.isPrimeraVez()) {
								Con = portNodo.getTiempoConexion();
							}
							log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio BCServices, queryGroupListBySubscriber: " + (fin - ini) + " milisegundos");

							if (Parametros.saveRequestResponse) {
								try {
									saveXml(isdn, getLogin(), wsdlLoc, "queryGroupListBySubscriber", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoria, Con, (fin - ini));
								} catch (Exception e) {
									log.warn("No se logro registrar los request y response del servicio BCServices, queryGroupListBySubscriber: ", e);
								}
							}

							if (queryGroupListBySubscriber != null && queryGroupListBySubscriber.getResultHeader() != null) {
								if (queryGroupListBySubscriber.getResultHeader().getResultCode().trim().equals("0")) {
									com.huawei.www.bme.cbsinterface.bcservices.QueryGroupListBySubscriberResultGroupList[] queryGroupListBySubscriberResult = queryGroupListBySubscriber.getQueryGroupListBySubscriberResult();
									if (queryGroupListBySubscriberResult != null) {
										if (queryGroupListBySubscriberResult.length > 0) {
											com.huawei.www.bme.cbsinterface.bcservices.QueryGroupListBySubscriberResultGroupList queryGroupListBySubscriberResultGroupList = queryGroupListBySubscriberResult[0];
											if (queryGroupListBySubscriberResultGroupList != null) {
												cug.setGroupKey(queryGroupListBySubscriberResultGroupList.getSubGroupKey());
												cug.setGroupCode(queryGroupListBySubscriberResultGroupList.getSubGroupCode());
												cug.setShortDialCode(queryGroupListBySubscriberResultGroupList.getMemberShortNo());
												if (queryGroupListBySubscriberResultGroupList.getMemberTypeCode() != null)
													cug.setCugMemberTypeCode(hashMemberTypeCode.get(queryGroupListBySubscriberResultGroupList.getMemberTypeCode().trim()));

											} else {
												log.warn("[isdn: " + getIsdn() + "] queryGroupListBySubscriberResultGroupList resuelto a nulo");
											}
										}
									} else {
										log.warn("[isdn: " + getIsdn() + "] queryGroupListBySubscriber ResultHeader resuelto a nulo");
										SysMessage.info("Datos no encontrados", null);
									}
								} else {
									log.info("[isdn: " + getIsdn() + "] queryGroupListBySubscriber ResultCode: " + queryGroupListBySubscriber.getResultHeader().getResultCode() + ", ResultDesc: " + queryGroupListBySubscriber.getResultHeader().getResultDesc());
									SysMessage.warn("Servicio BCServices, queryGroupListBySubscriber Code: " + queryGroupListBySubscriber.getResultHeader().getResultCode() + ", Desc: " + queryGroupListBySubscriber.getResultHeader().getResultDesc(), null);
								}
							} else {
								if (queryGroupListBySubscriber != null) {
									log.warn("[isdn: " + getIsdn() + "] queryGroupListBySubscriber queryGroupListBySubscriber resuelto a nulo");
								} else {
									log.warn("[isdn: " + getIsdn() + "] queryGroupListBySubscriber ResultHeader resuelto a nulo");
								}
							}

							if (cug.getGroupKey() != null) {

								com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestMsg queryCustomerInfoRequestMsg = new com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestMsg();
								queryCustomerInfoRequestMsg.setRequestHeader(requestHeader);
								com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequest queryCustomerInfoRequest = new com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequest();
								com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestQueryObj queryObj = new com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoRequestQueryObj();
								com.huawei.www.bme.cbsinterface.bccommon.SubGroupAccessCode subGroupAccessCode = new com.huawei.www.bme.cbsinterface.bccommon.SubGroupAccessCode();
								subGroupAccessCode.setSubGroupKey(cug.getGroupKey().trim());
								queryObj.setSubGroupAccessCode(subGroupAccessCode);
								queryCustomerInfoRequest.setQueryObj(queryObj);
								queryCustomerInfoRequestMsg.setQueryCustomerInfoRequest(queryCustomerInfoRequest);
								com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoResultMsg queryCustomerInfo = port.queryCustomerInfo(queryCustomerInfoRequestMsg);

								if (Parametros.saveRequestResponse) {
									try {
										saveXml("N/A", getLogin(), wsdlLoc, "queryCustomerInfo", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoria, Con, (fin - ini));
									} catch (Exception e) {
										log.warn("No se logro registrar los request y response del servicio BCServices, queryCustomerInfo: ", e);
									}
								}

								if (queryCustomerInfo != null && queryCustomerInfo.getResultHeader() != null) {
									if (queryCustomerInfo.getResultHeader().getResultCode().trim().equals("0")) {
										com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoResult queryCustomerInfoResult = queryCustomerInfo.getQueryCustomerInfoResult();
										if (queryCustomerInfoResult != null) {
											com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoResultSubGroup subGroup = queryCustomerInfoResult.getSubGroup();
											if (subGroup != null) {
												com.huawei.www.bme.cbsinterface.bcservices.QueryCustomerInfoResultSubGroupSubGroupInfo subGroupInfo = subGroup.getSubGroupInfo();
												if (subGroupInfo != null) {
													com.huawei.www.bme.cbsinterface.bccommon.SubGroupBasicInfo subGroupBasicInfo = subGroupInfo.getSubGroupBasicInfo();
													if (subGroupBasicInfo != null) {
														cug.setCugName(subGroupBasicInfo.getSubGroupName());
													} else {
														log.warn("[isdn: " + getIsdn() + "] subGroupBasicInfo resuelto a nulo");
													}

												} else {
													log.warn("[isdn: " + getIsdn() + "] subGroupInfo resuelto a nulo");
												}
											} else {
												log.warn("[isdn: " + getIsdn() + "] subGroup resuelto a nulo");
											}
										} else {
											log.warn("[isdn: " + getIsdn() + "] queryCustomerInfoResult resuelto a nulo");
										}
									} else {
										log.info("[isdn: " + getIsdn() + "] ResultCode: " + queryCustomerInfo.getResultHeader().getResultCode() + ", ResultDesc: " + queryCustomerInfo.getResultHeader().getResultDesc());
										SysMessage.warn("Servicio BCServices, queryCustomerInfo Code: " + queryCustomerInfo.getResultHeader().getResultCode() + ", Desc: " + queryCustomerInfo.getResultHeader().getResultDesc(), null);
										guardarExcepcion(idAuditoria, "Servicio BCServices, queryCustomerInfo Code: " + queryCustomerInfo.getResultHeader().getResultCode() + ", Desc: " + queryCustomerInfo.getResultHeader().getResultDesc());
									}

								} else {
									if (queryCustomerInfo != null) {
										log.warn("[isdn: " + getIsdn() + "] queryCustomerInfo queryCustomerInfo resuelto a nulo");
									} else {
										log.warn("[isdn: " + getIsdn() + "] queryCustomerInfo ResultHeader resuelto a nulo");
									}
								}
							}
							// INICIO querySubLifeCycle

							com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequest querySubLifeCycleRequest = new com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequest(subAccessCode);
							com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequestMsg querySubLifeCycleRequestMsg = new com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleRequestMsg(requestHeader, querySubLifeCycleRequest);

							com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleResultMsg querySubLifeCycle = port.querySubLifeCycle(querySubLifeCycleRequestMsg);

							long fin2 = System.currentTimeMillis();
							Long Con2 = null;
							if (portNodo.isPrimeraVez()) {
								Con2 = portNodo.getTiempoConexion();
							}
							log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio BCServices, querySubLifeCycle: " + (fin2 - ini) + " milisegundos");
							if (Parametros.saveRequestResponse) {
								try {
									saveXml(isdn, getLogin(), wsdlLoc, "querySubLifeCycle", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoria, Con2, (fin2 - ini));
								} catch (Exception e) {
									log.warn("No se logro registrar los request y response del servicio BCServices, querySubLifeCycle:", e);
								}
							}

							if (querySubLifeCycle != null && querySubLifeCycle.getResultHeader() != null) {
								if (querySubLifeCycle.getResultHeader().getResultCode().trim().equals("0")) {
									com.huawei.www.bme.cbsinterface.bcservices.QuerySubLifeCycleResult querySubLifeCycleResult = querySubLifeCycle.getQuerySubLifeCycleResult();

									if (querySubLifeCycleResult != null) {
										log.debug("querySubLifeCycle.getResultHeader: " + querySubLifeCycle.getResultHeader());
									} else {
										log.warn("[isdn: " + getIsdn() + "] QuerySubLifeCycleResult resuelto a nulo");
									}
								} else {
									log.info("[isdn: " + getIsdn() + "] querySubLifeCycle ResultCode: " + querySubLifeCycle.getResultHeader().getResultCode() + ", ResultDesc: " + querySubLifeCycle.getResultHeader().getResultDesc());
									SysMessage.warn("Servicio BCServices, querySubLifeCycle Code: " + querySubLifeCycle.getResultHeader().getResultCode() + ", Desc: " + querySubLifeCycle.getResultHeader().getResultDesc(), null);
									guardarExcepcion(idAuditoria, "Servicio BCServices, querySubLifeCycle Code: " + querySubLifeCycle.getResultHeader().getResultCode() + ", Desc: " + querySubLifeCycle.getResultHeader().getResultDesc());
								}
							} else {
								if (querySubLifeCycle != null) {
									log.warn("[isdn: " + getIsdn() + "] querySubLifeCycle ResultHeader resuelto a nulo");
								} else {
									log.warn("[isdn: " + getIsdn() + "] querySubLifeCycle QuerySubLifeCycle resuelto a nulo");
								}
							}

							// FIN
						} finally {
							portNodo.setEnUso(false);
							portNodo.setFechaFin(new Date());
						}
					}
				} else {
					log.error("[isdn: " + getIsdn() + "] Port BCServices resuelto a nulo");
				}

			} else {
				SysMessage.warn("Nro de Cuenta no valido", null);
			}
		} catch (AxisFault a) {
			String msg = a.getMessage();
			msg = msg.toLowerCase();
			if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase()) || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase()) || msg.contains("UnknownHostException".toLowerCase()) || msg.contains("No route to host".toLowerCase())
					|| msg.contains("Connection refused".toLowerCase())) {
				log.error("Error de conexion al servicio BCServices Cug: ", a);
				SysMessage.error("Error de conexion del servicio BCServices", null);
				guardarExcepcion(idAuditoria, "Error de conexion al servicio BCServices: " + stackTraceToString(a));
			} else {
				log.error("Error AxisFault Cug: ", a);
				SysMessage.error("Error AxisFault de conexion del servicio BCServices", null);
				guardarExcepcion(idAuditoria, "Error AxisFault de conexion del servicio BCServices: " + stackTraceToString(a));
			}
		} catch (ServiceException e) {
			cug = new Cug();
			log.error("Error de servicio al conectarse al servicio BCServices Cug: ", e);
			SysMessage.error("Error de servicio al conectarse al servicio BCServices", null);
			guardarExcepcion(idAuditoria, "Error de servicio al conectarse al servicio BCServices: " + stackTraceToString(e));
		} catch (RemoteException e) {
			cug = new Cug();
			log.error("Error remoto al conectarse al servicio BCServices Cug: ", e);
			SysMessage.error("Error remoto al conectarse al servicio BCServices", null);
			guardarExcepcion(idAuditoria, "Error remoto al conectarse al servicio BCServices: " + stackTraceToString(e));
		} catch (Exception e) {
			cug = new Cug();
			log.error("Error al consultar Cug", e);
			SysMessage.error("Error al consultar", null);
			guardarExcepcion(idAuditoria, "Error al consultar: " + stackTraceToString(e));
		}

	}

	private void saveXml(String isdn, String login, String servicio, String metodo, String request, String response, long idAuditoria, Long tiempoConexion, Long tiempoRespuesta) {
		try {
			VcConsulta c = new VcConsulta();
			c.setFechaCreacion(Calendar.getInstance());
			c.setIsdn(isdn);
			c.setLogin(login);
			c.setServicio(servicio);
			c.setMetodo(metodo);
			c.setRequest(request);
			c.setResponse(response);
			c.setTiempoConexion(tiempoConexion);
			c.setTiempoRespuesta(tiempoRespuesta);

			Object find = blAuditoria.find(idAuditoria, VcLogAuditoria.class);
			c.setVcLogAuditoria((VcLogAuditoria) find);

			blConsulta.save(c);
		} catch (Exception e) {
			log.warn("No se registro en bd el request y response: ", e);
		}
	}

	public void saveXmlRed(long idAuditoria, String isdn, String login, String ip, String tipo, String trama) {
		try {
			VcConsultaRed R = new VcConsultaRed();
			R.setFechaCreacion(Calendar.getInstance());
			R.setIsdn(isdn);
			R.setLogin(login);
			R.setIp(ip);
			R.setTipo(tipo);
			R.setTrama(trama);

			Object find = blAuditoria.find(idAuditoria, VcLogAuditoria.class);
			R.setVcLogAuditoria((VcLogAuditoria) find);

			blConsultaRed.save(R);
		} catch (Exception e) {
			log.warn("No se registro en bd la trama: ", e);
		}
	}

	public void cargarVoucher() {
		log.debug("Ingresando a cargar voucher");
		try {

			voucher = new Voucher();
			listVoucherOperation = new ArrayList<SimpleVoucherOperation>();

			if (login == null || login.trim().isEmpty()) {
				SysMessage.warn("Usuario no válido", null);
				return;
			}

			if (batchNumber != null && !batchNumber.trim().isEmpty()) {
				if (serialNumber != null && !serialNumber.trim().isEmpty()) {

					List<VcLogAdicional> adicionales = new ArrayList<>();
					VcLogAdicional ad1 = new VcLogAdicional();
					ad1.setCodigo("Batch ID");
					ad1.setValor(batchNumber);
					VcLogAdicional ad2 = new VcLogAdicional();
					ad2.setCodigo("Serial No");
					ad2.setValor(serialNumber);
					adicionales.add(ad1);
					adicionales.add(ad2);

					long idAuditoria = guardarAuditoria(Calendar.getInstance(), this.login, this.ip, Parametros.vistaVoucher, Parametros.comandoVoucher, "N/A", adicionales);

					if (idAuditoria <= 0) {
						SysMessage.error("Error al guardar log de auditoria", null);
						return;
					}

					disableIncidente = false;
					vistaIncidente = DescriptorBitacora.VOUCHER;
					try {
						controlerBitacora.accion(DescriptorBitacora.VOUCHER, "Se hizo consulta de Voucher, Batch Number: " + batchNumber + ", Serial Number: " + serialNumber);
					} catch (Exception e) {
						log.error("Error al guardar bitacora en el sistema: ", e);
						SysMessage.error("Error al guardar bitacora", null);
					}

					String wsdlLoc = Parametros.voucherWsdl;

					URL url = new URL(UtilUrl.getIp(wsdlLoc));
					long ini = System.currentTimeMillis();

					if (url.getProtocol().equalsIgnoreCase("https")) {
						int puerto = url.getPort();
						String host = url.getHost();
						if (host.equalsIgnoreCase("localhost")) {
							host = "127.0.0.1";
						}
						validarCertificado(Parametros.voucherPathKeystore, host + ":" + puerto);
					}

					NodoServicio<com.huawei.www.bme.cbsinterface.uvcservices.UvcServicsBindingStub> portNodo = ServicioVoucher.initPort(wsdlLoc, "VOUCHER" + login, Parametros.voucherTimeOut);

					if (portNodo != null && portNodo.getPort() != null) {

						synchronized (portNodo) {
							try {
								com.huawei.www.bme.cbsinterface.uvcservices.UvcServicsBindingStub port = portNodo.getPort();

								com.huawei.www.bme.cbsinterface.uvcheader.OwnershipInfo ownershipInfo = new com.huawei.www.bme.cbsinterface.uvcheader.OwnershipInfo();
								ownershipInfo.setBEID(Integer.parseInt(Parametros.voucherOwnerShipInfoId));
								com.huawei.www.bme.cbsinterface.uvcheader.SecurityInfo accessSecurity = new com.huawei.www.bme.cbsinterface.uvcheader.SecurityInfo();
								accessSecurity.setLoginSystemCode(Parametros.voucherLoginSystemCode);
								accessSecurity.setPassword(Parametros.voucherPassword);
								com.huawei.www.bme.cbsinterface.uvcheader.OperatorInfo operatorInfo = new com.huawei.www.bme.cbsinterface.uvcheader.OperatorInfo();
								operatorInfo.setOperatorID(Parametros.voucherOperatorId);
								com.huawei.www.bme.cbsinterface.uvcheader.RequestHeaderTimeFormat timeFormat = new com.huawei.www.bme.cbsinterface.uvcheader.RequestHeaderTimeFormat();
								timeFormat.setTimeType(Parametros.voucherTimeType);

								SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
								String messageSeq = format.format(new Date());

								com.huawei.www.bme.cbsinterface.uvcheader.RequestHeader requestHeader = new com.huawei.www.bme.cbsinterface.uvcheader.RequestHeader(Parametros.voucherVersion, "", messageSeq, ownershipInfo, accessSecurity, operatorInfo, "", timeFormat);

								com.huawei.www.bme.cbsinterface.uvcservices.QueryVoucherRequest queryVoucherRequest = new com.huawei.www.bme.cbsinterface.uvcservices.QueryVoucherRequest(batchNumber + serialNumber);
								com.huawei.www.bme.cbsinterface.uvcservices.QueryVoucherRequestMsg queryVoucherRequestMsg = new com.huawei.www.bme.cbsinterface.uvcservices.QueryVoucherRequestMsg(requestHeader, queryVoucherRequest);

								com.huawei.www.bme.cbsinterface.uvcservices.QueryVoucherResultMsg queryVoucher = port.queryVoucher(queryVoucherRequestMsg);

								long fin = System.currentTimeMillis();
								Long Con = null;
								if (portNodo.isPrimeraVez()) {
									Con = portNodo.getTiempoConexion();
								}
								log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio UvcServices, queryVoucher: " + (fin - ini) + " milisegundos");

								if (Parametros.saveRequestResponse) {
									try {
										saveXml("N/A", getLogin(), wsdlLoc, "queryVoucher", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoria, Con, (fin - ini));
									} catch (Exception e) {
										log.warn("No se logro registrar los request y response del servicio UvcServices, queryVoucher: ", e);
									}
								}

								if (queryVoucher != null && queryVoucher.getResultHeader() != null) {
									if (queryVoucher.getResultHeader().getResultCode().trim().equals("0")) {
										com.huawei.www.bme.cbsinterface.uvcservices.QueryVoucherResult queryVoucherResult = queryVoucher.getQueryVoucherResult();
										if (queryVoucherResult != null) {
											com.huawei.www.bme.cbsinterface.uvccommon.VoucherInfoType voucherInfo = queryVoucherResult.getVoucherInfo();

											if (voucherInfo != null) {
												voucher.setServiceProvider(Parametros.voucherServiceProvider);

												if (voucherInfo.getFaceValue() != null) {

													double total = UtilNumber.redondear(((double) voucherInfo.getFaceValue() / (double) Parametros.voucherDivisor), Parametros.voucherNroDecimales);
													voucher.setFaceValue(UtilNumber.doubleToString(total, Parametros.voucherNroDecimales));
													// voucher.setFaceValue(voucherInfo.getFaceValue().toString());
												}

												if (voucherInfo.getHotCardFlag() != null)
													voucher.setCardState(hashCardState.get(voucherInfo.getHotCardFlag().toString()));
												if (voucherInfo.getCurrency() != null)
													voucher.setCurrency(getCurrencyDescription(voucherInfo.getCurrency().intValue()));
												if (voucherInfo.getRechargeNumber() != null)
													voucher.setRechargeNumber(voucherInfo.getRechargeNumber());
												if (voucherInfo.getCardCosID() != null)
													voucher.setCardCosId(voucherInfo.getCardCosID());
												if (voucherInfo.getCardCosName() != null)
													voucher.setCardCosName(voucherInfo.getCardCosName());
												/// dateCardStar

												if (voucherInfo.getCardStartDate() != null) {
													Date dateCardStart = UtilDate.stringToDate(voucherInfo.getCardStartDate().trim(), "yyyyMMdd");
													// log.warn("UtilDate.dateToString(dateCardStart,
													// dd-MM-yyyy): " +
													// UtilDate.dateToString(dateCardStart,
													// "dd-MM-yyyy"));
													voucher.setCardStartDate(UtilDate.dateToString(dateCardStart, "dd-MM-yyyy"));
												}
												/// dateExpiration =
												/// CardStopDate
												if (voucherInfo.getCardStopDate() != null) {
													Date dateExpiration = UtilDate.stringToDate(voucherInfo.getCardStopDate().trim(), "yyyyMMdd");
													voucher.setExpirationDate(UtilDate.dateToString(dateExpiration, "dd-MM-yyyy"));
												}
												/// Date used = TradeTime
												if (voucherInfo.getTradeTime() != null) {
													Date dateUsed = UtilDate.stringToDate(voucherInfo.getTradeTime().trim(), Parametros.fechaFormatWs);
													voucher.setDateUsed(UtilDate.dateToString(dateUsed, Parametros.fechaFormatPage));
												}

												com.huawei.www.bme.cbsinterface.uvccommon.SimpleVoucherOperationType[] simpleVoucherOperation = queryVoucherResult.getSimpleVoucherOperation();
												if (simpleVoucherOperation != null && simpleVoucherOperation.length > 0) {

													for (int i = 0; i < simpleVoucherOperation.length; i++) {

														try {

															com.huawei.www.bme.cbsinterface.uvccommon.SimpleVoucherOperationType simpleVoucherOperationType = simpleVoucherOperation[i];
															if (simpleVoucherOperationType != null) {
																Date date = UtilDate.stringToDate(simpleVoucherOperationType.getOperationDate(), Parametros.fechaFormatWs);
																String operationDate = UtilDate.dateToString(date, Parametros.fechaFormatPage);
																String operationType = "";

																if (simpleVoucherOperationType.getOperationType() != null) {
																	operationType = hashOperationType.get(simpleVoucherOperationType.getOperationType().trim());
																	listVoucherOperation.add(new SimpleVoucherOperation(operationType, operationDate, simpleVoucherOperationType.getOperationReason()));
																}
															}
														} catch (Exception e) {
															log.error("Error al cargar lista de operaciones voucher: ", e);
														}
													}
												}

											} else {
												log.warn("[isdn: " + getIsdn() + "] queryVoucher, voucherInfo resuelto a nulo");
											}
										} else {
											log.warn("[isdn: " + getIsdn() + "] queryVoucher, queryVoucherResult resuelto a nulo");
										}
									} else {
										log.info("[isdn: " + getIsdn() + "] queryVoucher ResultCode: " + queryVoucher.getResultHeader().getResultCode() + ", ResultDesc: " + queryVoucher.getResultHeader().getResultDesc());
										SysMessage.warn("Servicio UvcServices, queryVoucher ResultCode: " + queryVoucher.getResultHeader().getResultCode() + ", ResultDesc: " + queryVoucher.getResultHeader().getResultDesc(), null);
									}
								} else {
									if (queryVoucher != null) {
										log.warn("[isdn: " + getIsdn() + "] queryVoucher queryVoucher resuelto a nulo");
									} else {
										log.warn("[isdn: " + getIsdn() + "] queryVoucher ResultHeader resuelto a nulo");
									}
								}
							} finally {
								portNodo.setEnUso(false);
								portNodo.setFechaFin(new Date());
							}
						}
					} else {
						log.error("[isdn: " + getIsdn() + "] Port UvcServices voucher resuelto a nulo");
					}

					SysMessage.info("Consulta finalizada.", null);

				} else {
					SysMessage.warn("El campo Serial Number no es válido", null);
				}
			} else {
				SysMessage.warn("El campo Batch Number no es válido", null);
			}

		} catch (AxisFault a) {
			String msg = a.getMessage();
			msg = msg.toLowerCase();
			if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase()) || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase()) || msg.contains("UnknownHostException".toLowerCase()) || msg.contains("No route to host".toLowerCase())
					|| msg.contains("Connection refused".toLowerCase())) {
				log.error("Error de conexion al servicio UvcServices billeteras: ", a);
				SysMessage.error("Error de conexion del servicio Voucher", null);
			} else {
				log.error("Error AxisFault: ", a);
				SysMessage.error("Error AxisFault de conexion al servicio UvcServices: " + a.getMessage(), null);
			}
		} catch (ServiceException e) {
			voucher = new Voucher();
			log.error("Error de servicio al conectarse al servicio UvcServices: ", e);
			SysMessage.error("Error de servicio al conectarse al servicio UvcServices", null);
		} catch (RemoteException e) {
			voucher = new Voucher();
			log.error("Error remoto al conectarse al servicio UvcServices: ", e);
			SysMessage.error("Error remoto al conectarse al servicio UvcServices", null);
		} catch (Exception e) {
			voucher = new Voucher();
			log.error("Error al consultar UvcServices", e);
			SysMessage.error("Error al consultar UvcServices", null);
		}
	}

	// cambiar para historico
	private void cargarBalanceDetailRecharge() {
		try {

			String wsdlLoc = Parametros.billeteraWsdl;

			URL url = new URL(UtilUrl.getIp(wsdlLoc));

			long ini = System.currentTimeMillis();

			if (url.getProtocol().equalsIgnoreCase("https")) {
				int puerto = url.getPort();
				String host = url.getHost();
				if (host.equalsIgnoreCase("localhost")) {
					host = "127.0.0.1";
				}
				validarCertificado(Parametros.billeteraPathKeystore, host + ":" + puerto);
			}

			NodoServicio<com.huawei.www.bme.cbsinterface.arservices.ArServicsBindingStub> portNodo = ServicioBilletera.initPort(wsdlLoc, "BILLETERA_RECHARGE" + login, Parametros.billeteraTimeOut);

			if (portNodo != null && portNodo.getPort() != null) {

				synchronized (portNodo) {
					try {
						com.huawei.www.bme.cbsinterface.arservices.ArServicsBindingStub port = portNodo.getPort();

						com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo ownershipInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo();
						ownershipInfo.setBEID(Parametros.billeteraOwnerShipInfoId);
						com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo accessSecurity = new com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo();
						accessSecurity.setLoginSystemCode(Parametros.billeteraLoginSystemCode);
						accessSecurity.setPassword(Parametros.billeteraPassword);
						com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo operatorInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo();
						operatorInfo.setOperatorID(Parametros.billeteraOperatorId);
						com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat timeFormat = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat();
						timeFormat.setTimeType(Parametros.billeteraTimeType);
						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
						String messageSeq = format.format(new Date());

						com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderAdditionalProperty[] additionalProperty = null;
						String version = Parametros.billeteraVersion;
						com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader requestHeader = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader(version, "", messageSeq, ownershipInfo, accessSecurity, operatorInfo, "", "", timeFormat, additionalProperty);

						com.huawei.www.bme.cbsinterface.arservices.QueryRechargeLogRequestMsg queryRechargeLogRequestMsg = new com.huawei.www.bme.cbsinterface.arservices.QueryRechargeLogRequestMsg();
						queryRechargeLogRequestMsg.setRequestHeader(requestHeader);
						com.huawei.www.bme.cbsinterface.arservices.QueryRechargeLogRequest queryRechargeLogRequest = new com.huawei.www.bme.cbsinterface.arservices.QueryRechargeLogRequest();
						queryRechargeLogRequest.setTotalRowNum(0);
						queryRechargeLogRequest.setBeginRowNum(0);
						queryRechargeLogRequest.setFetchRowNum(1000);
						queryRechargeLogRequest.setStartTime(selectedHistory.getReTradeTime());
						queryRechargeLogRequest.setEndTime(selectedHistory.getReTradeTime());

						com.huawei.www.bme.cbsinterface.arservices.QueryRechargeLogRequestQueryObj queryObj = new com.huawei.www.bme.cbsinterface.arservices.QueryRechargeLogRequestQueryObj();
						com.huawei.cbs.ar.wsservice.arcommon.SubAccessCode subAccessCode = new com.huawei.cbs.ar.wsservice.arcommon.SubAccessCode();
						subAccessCode.setPrimaryIdentity(selectedHistory.getIsdn());
						queryObj.setSubAccessCode(subAccessCode);
						queryRechargeLogRequest.setQueryObj(queryObj);

						queryRechargeLogRequestMsg.setQueryRechargeLogRequest(queryRechargeLogRequest);
						com.huawei.www.bme.cbsinterface.arservices.QueryRechargeLogResultMsg queryRechargeLog = port.queryRechargeLog(queryRechargeLogRequestMsg);

						long fin = System.currentTimeMillis();
						Long Con = null;
						if (portNodo.isPrimeraVez()) {
							Con = portNodo.getTiempoConexion();
						}
						log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio ARServices historial: " + (fin - ini) + " milisegundos");

						if (Parametros.saveRequestResponse) {
							try {
								saveXml(isdn, getLogin(), wsdlLoc, "queryRechargeLog", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoriaHistorial, Con, (fin - ini));
							} catch (Exception e) {
								log.warn("No se logro registrar los request y response del servicio: ", e);
							}
						}

						if (queryRechargeLog != null && queryRechargeLog.getResultHeader() != null) {
							if (queryRechargeLog.getResultHeader().getResultCode().trim().equals("0")) {

								com.huawei.www.bme.cbsinterface.arservices.QueryRechargeLogResult queryRechargeLogResult = queryRechargeLog.getQueryRechargeLogResult();

								if (queryRechargeLogResult != null) {

									com.huawei.www.bme.cbsinterface.arservices.QueryRechargeLogResultRechargeInfo[] rechargeInfo = queryRechargeLogResult.getRechargeInfo();

									if (rechargeInfo != null && rechargeInfo.length > 0) {
										ArrayList<BalanceDetail> listCoreBalance= new ArrayList<BalanceDetail>();
										double chargeAmount=0;
										boolean isCoreBalance=false;
										boolean isCoreBalance2=false;
										for (com.huawei.www.bme.cbsinterface.arservices.QueryRechargeLogResultRechargeInfo item : rechargeInfo) {

											if (item.getExtTransID() != null && selectedHistory.getReExTransId() != null && item.getExtTransID().trim().equals(selectedHistory.getReExTransId().trim())) {

												com.huawei.cbs.ar.wsservice.arcommon.BalanceChgInfo[] balanceChgInfo = item.getBalanceChgInfo();

												if (balanceChgInfo != null) {

													for (com.huawei.cbs.ar.wsservice.arcommon.BalanceChgInfo reg : balanceChgInfo) {
														BalanceDetail bd = new BalanceDetail();
														bd.setBalanceTypeName(reg.getBalanceTypeName());

														String currencyDescription = "";
														if (reg.getCurrencyID() != null) {
															currencyDescription = getCurrencyDescription(reg.getCurrencyID().intValue());
															if (currencyDescription == null)
																currencyDescription = reg.getCurrencyID().intValue() + "";
														}
														bd.setCurrencyId(currencyDescription);

														long total = reg.getNewBalanceAmt() - reg.getOldBalanceAmt();
														double redondear = UtilNumber.redondear(((double) total / (double) Parametros.historialDivisor), Parametros.historialNroDecimales);
														bd.setChargeAmount(UtilNumber.doubleToString(redondear, Parametros.historialNroDecimales));

														bd.setChargeCode(reg.getBalanceType());
														bd.setChargeCodeName("");

														if(bd.getBalanceTypeName().contains("CORE_BALANCE")){

															if(bd.getChargeAmount()!=null  && !bd.getChargeAmount().isEmpty()) {
																chargeAmount += Double.valueOf(bd.getChargeAmount().replaceAll(",", "."));
															}

															if(bd.getBalanceTypeName().equals("CORE_BALANCE")){
																isCoreBalance=true;
															}
															if(bd.getBalanceTypeName().equals("CORE_BALANCE_2")){
																isCoreBalance2=true;
															}
															listCoreBalance.add(bd);
														}else {
															getListBalanceDetail().add(bd);
														}
													}
												}

												com.huawei.www.bme.cbsinterface.arservices.QueryRechargeLogResultRechargeInfoRechargeBonus rechargeBonus = item.getRechargeBonus();

												if (rechargeBonus != null) {
													com.huawei.www.bme.cbsinterface.arservices.QueryRechargeLogResultRechargeInfoRechargeBonusFreeUnitItemList[] freeUnitItemList = rechargeBonus.getFreeUnitItemList();

													if (freeUnitItemList != null) {

														for (com.huawei.www.bme.cbsinterface.arservices.QueryRechargeLogResultRechargeInfoRechargeBonusFreeUnitItemList reg : freeUnitItemList) {
															BalanceDetail bd = new BalanceDetail();
															bd.setBalanceTypeName(reg.getFreeUnitTypeName());

															if (reg.getBonusAmt() != null)
																bd.setChargeAmount(UtilNumber.doubleToString(reg.getBonusAmt(), Parametros.historialNroDecimales));
															bd.setCurrencyId(reg.getMeasureUnit());

															if (reg.getMeasureUnit() != null) {
																FreeUnitMeasure freeUnitMeasure = hashFreeUnitMeasure.get(reg.getMeasureUnit().trim());
																if (freeUnitMeasure != null) {
																	if (reg.getBonusAmt() != null) {
																		double total = UtilNumber.redondear((double) reg.getBonusAmt() / freeUnitMeasure.getDividendo(), Parametros.historialNroDecimales);
																		bd.setChargeAmount(UtilNumber.doubleToString(total, Parametros.historialNroDecimales));
																	}
																	bd.setCurrencyId(freeUnitMeasure.getUnidad());
																}
															}
															Date date = UtilDate.stringToDate(reg.getExpireTime().trim(), Parametros.fechaFormatWs);
															bd.setChargeCode(UtilDate.dateToString(date, Parametros.fechaFormatPage));
															bd.setChargeCodeName("RechargeBonus");

															if(bd.getBalanceTypeName().contains("CORE_BALANCE")){

																if(bd.getChargeAmount()!=null  && !bd.getChargeAmount().isEmpty()) {
																	chargeAmount += Double.valueOf(bd.getChargeAmount().replaceAll(",", "."));
																}

																if(bd.getBalanceTypeName().equals("CORE_BALANCE")){
																	isCoreBalance=true;
																}
																if(bd.getBalanceTypeName().equals("CORE_BALANCE_2")){
																	isCoreBalance2=true;
																}
																listCoreBalance.add(bd);
															}else {
																getListBalanceDetail().add(bd);
															}
														}
													}

													com.huawei.www.bme.cbsinterface.arservices.QueryRechargeLogResultRechargeInfoRechargeBonusBalanceList[] balanceList = rechargeBonus.getBalanceList();
													if (balanceList != null) {

														for (com.huawei.www.bme.cbsinterface.arservices.QueryRechargeLogResultRechargeInfoRechargeBonusBalanceList reg : balanceList) {
															BalanceDetail bd = new BalanceDetail();
															bd.setBalanceTypeName(reg.getBalanceTypeName());

															double redondear = UtilNumber.redondear(((double) reg.getBonusAmt() / (double) Parametros.historialDivisor), Parametros.historialNroDecimales);
															bd.setChargeAmount(UtilNumber.doubleToString(redondear, Parametros.historialNroDecimales));

															String currencyDescription = "";
															if (reg.getCurrencyID() != null) {
																currencyDescription = getCurrencyDescription(reg.getCurrencyID().intValue());
																if (currencyDescription == null)
																	currencyDescription = reg.getCurrencyID().intValue() + "";
															}
															bd.setCurrencyId(currencyDescription);
															Date date = UtilDate.stringToDate(reg.getExpireTime().trim(), Parametros.fechaFormatWs);
															bd.setChargeCode(UtilDate.dateToString(date, Parametros.fechaFormatPage));
															bd.setChargeCodeName("RechargeBonus");

															if(bd.getBalanceTypeName().contains("CORE_BALANCE")){
																if(bd.getChargeAmount()!=null  && !bd.getChargeAmount().isEmpty()) {
																	chargeAmount += Double.valueOf(bd.getChargeAmount().replaceAll(",", "."));
																}
																if(bd.getBalanceTypeName().equals("CORE_BALANCE")){
																	isCoreBalance=true;
																}
																if(bd.getBalanceTypeName().equals("CORE_BALANCE_2")){
																	isCoreBalance2=true;
																}
																listCoreBalance.add(bd);
															}else {
																getListBalanceDetail().add(bd);
															}
														}
													}
												}
											} else {
												if (item.getExtTransID() == null) {
													log.warn("rechargeInfoItem.ExtTransID resuelto a nulo");
												} else {
													log.warn("selectedHistory.getReExTransId resuelto a nulo");
												}
											}
										}
										String textAlign="left";
										if(listCoreBalance.size()>1) {

											BalanceDetail bd = new BalanceDetail();
											String balanceTypeName=isCoreBalance? (isCoreBalance2?"CORE_BALANCE + CORE_BALANCE_2":"CORE_BALANCE"):"CORE_BALANCE_2";
											bd.setBalanceTypeName(balanceTypeName);
											bd.setColor("#e2e2e2");
											bd.setCurrencyId(listCoreBalance.get(0).getCurrencyId());
											bd.setChargeCode(listCoreBalance.get(0).getChargeCode());
											bd.setChargeCodeName(listCoreBalance.get(0).getChargeCodeName());

											bd.setChargeAmount(UtilNumber.doubleToString(chargeAmount, Parametros.billeteraNroDecimales));
											textAlign="right";
											getListBalanceDetail().add(bd);

										}
										for (BalanceDetail itemsde : listCoreBalance) {
											itemsde.setTextAlign(textAlign);
											getListBalanceDetail().add(itemsde);
										}
									}

								} else {
									log.warn("[isdn: " + getIsdn() + "] queryAdjustLogResult resuelto a nulo");
								}
							} else {
								log.info("[isdn: " + getIsdn() + "] ResultCode: " + queryRechargeLog.getResultHeader().getResultCode() + ", ResultDesc: " + queryRechargeLog.getResultHeader().getResultDesc());
								SysMessage.warn("Servicio ARServices, queryRechargeLog Code: " + queryRechargeLog.getResultHeader().getResultCode() + ", ResultDesc: " + queryRechargeLog.getResultHeader().getResultDesc(), null);
								guardarExcepcion(idAuditoriaHistorial, "Servicio ARServices, queryRechargeLog Code: " + queryRechargeLog.getResultHeader().getResultCode() + ", ResultDesc: " + queryRechargeLog.getResultHeader().getResultDesc());
							}
						} else {
							if (queryRechargeLog != null) {
								log.warn("[isdn: " + getIsdn() + "] ResultHeader resuelto a nulo");
							} else {
								log.warn("[isdn: " + getIsdn() + "] queryRechargeLog resuelto a nulo");
							}
						}
					} finally {
						portNodo.setEnUso(false);
						portNodo.setFechaFin(new Date());
					}
				}
			} else {
				log.error("[isdn: " + getIsdn() + "] Port ARServices Historial resuelto a nulo");
			}
		} catch (AxisFault a) {
			String msg = a.getMessage();
			msg = msg.toLowerCase();
			if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase()) || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase()) || msg.contains("UnknownHostException".toLowerCase()) || msg.contains("No route to host".toLowerCase())
					|| msg.contains("Connection refused".toLowerCase())) {
				log.error("Error de conexion al servicio ARServices historial: ", a);
				SysMessage.error("Error de conexion del servicio ARServices", null);
				guardarExcepcion(idAuditoriaHistorial, "Error de conexion al servicio ARServices: " + stackTraceToString(a));
			} else {
				log.error("Error AxisFault historial: ", a);
				SysMessage.error("Error AxisFault de conexion al servicio ARServices", null);
				guardarExcepcion(idAuditoriaHistorial, "Error AxisFault de conexion al servicio ARServices: " + stackTraceToString(a));
			}
		} catch (ServiceException e) {
			log.error("Error de servicio al conectarse al servicio ARServices historial: ", e);
			SysMessage.error("Error de servicio al conectarse al servicio ARServices", null);
			guardarExcepcion(idAuditoriaHistorial, "Error de servicio al conectarse al servicio ARServices: " + stackTraceToString(e));
		} catch (RemoteException e) {
			log.error("Error remoto al conectarse al servicio ARServices historial: ", e);
			SysMessage.error("Error remoto al conectarse al servicio ARServices", null);
			guardarExcepcion(idAuditoriaHistorial, "Error remoto al conectarse al servicio ARServices: " + stackTraceToString(e));
		} catch (Exception e) {
			log.error("Error al consultar ARServices historial", e);
			SysMessage.error("Error al consultar ARServices", null);
			guardarExcepcion(idAuditoriaHistorial, "Error al consultar ARServices: " + stackTraceToString(e));
		}

	}
	// cambiar para historico
	private void cargarBalanceDetailAdjustment() {

		log.debug("Ingresando a cargar detalle del adjustment");
		try {

			String wsdlLoc = Parametros.billeteraWsdl;

			URL url = new URL(UtilUrl.getIp(wsdlLoc));
			long ini = System.currentTimeMillis();

			if (url.getProtocol().equalsIgnoreCase("https")) {
				int puerto = url.getPort();
				String host = url.getHost();
				if (host.equalsIgnoreCase("localhost")) {
					host = "127.0.0.1";
				}
				validarCertificado(Parametros.billeteraPathKeystore, host + ":" + puerto);
			}

			NodoServicio<com.huawei.www.bme.cbsinterface.arservices.ArServicsBindingStub> portNodo = ServicioBilletera.initPort(wsdlLoc, "BILLETERA_ADJUSTMENT" + login, Parametros.billeteraTimeOut);

			if (portNodo != null && portNodo.getPort() != null) {

				synchronized (portNodo) {
					try {
						com.huawei.www.bme.cbsinterface.arservices.ArServicsBindingStub port = portNodo.getPort();

						com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo ownershipInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo();
						ownershipInfo.setBEID(Parametros.billeteraOwnerShipInfoId);
						com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo accessSecurity = new com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo();
						accessSecurity.setLoginSystemCode(Parametros.billeteraLoginSystemCode);
						accessSecurity.setPassword(Parametros.billeteraPassword);
						com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo operatorInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo();
						operatorInfo.setOperatorID(Parametros.billeteraOperatorId);
						com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat timeFormat = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat();
						timeFormat.setTimeType(Parametros.billeteraTimeType);
						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
						String messageSeq = format.format(new Date());

						com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderAdditionalProperty[] additionalProperty = null;
						String version = Parametros.billeteraVersion;
						com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader requestHeader = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader(version, "", messageSeq, ownershipInfo, accessSecurity, operatorInfo, "", "", timeFormat, additionalProperty);

						com.huawei.www.bme.cbsinterface.arservices.QueryAdjustLogRequestMsg queryAdjustLogRequestMsg = new com.huawei.www.bme.cbsinterface.arservices.QueryAdjustLogRequestMsg();
						queryAdjustLogRequestMsg.setRequestHeader(requestHeader);
						com.huawei.www.bme.cbsinterface.arservices.QueryAdjustLogRequest queryAdjustLogRequest = new com.huawei.www.bme.cbsinterface.arservices.QueryAdjustLogRequest();
						queryAdjustLogRequest.setTotalRowNum(new BigInteger("0"));
						queryAdjustLogRequest.setBeginRowNum(new BigInteger("0"));
						queryAdjustLogRequest.setFetchRowNum(new BigInteger("1000"));

						queryAdjustLogRequest.setStartTime(selectedHistory.getTradeTime());
						queryAdjustLogRequest.setEndTime(selectedHistory.getTradeTime());

						com.huawei.www.bme.cbsinterface.arservices.QueryAdjustLogRequestQueryObj queryObj = new com.huawei.www.bme.cbsinterface.arservices.QueryAdjustLogRequestQueryObj();
						com.huawei.cbs.ar.wsservice.arcommon.SubAccessCode subAccessCode = new com.huawei.cbs.ar.wsservice.arcommon.SubAccessCode();
						subAccessCode.setPrimaryIdentity(isdn);
						queryObj.setSubAccessCode(subAccessCode);
						queryAdjustLogRequest.setQueryObj(queryObj);

						queryAdjustLogRequestMsg.setQueryAdjustLogRequest(queryAdjustLogRequest);
						com.huawei.www.bme.cbsinterface.arservices.QueryAdjustLogResultMsg queryAdjustLog = port.queryAdjustLog(queryAdjustLogRequestMsg);

						long fin = System.currentTimeMillis();
						Long Con = null;
						if (portNodo.isPrimeraVez()) {
							Con = portNodo.getTiempoConexion();
						}
						log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio ARServices queryAdjustLog: " + (fin - ini) + " milisegundos");

						if (Parametros.saveRequestResponse) {
							try {
								saveXml(isdn, getLogin(), wsdlLoc, "queryAdjustLog", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoriaHistorial, Con, (fin - ini));
							} catch (Exception e) {
								log.warn("No se logro registrar los request y response del servicio ARServices, queryAdjustLog: ", e);
							}
						}

						if (queryAdjustLog != null && queryAdjustLog.getResultHeader() != null) {
							if (queryAdjustLog.getResultHeader().getResultCode().trim().equals("0")) {

								com.huawei.www.bme.cbsinterface.arservices.QueryAdjustLogResult queryAdjustLogResult = queryAdjustLog.getQueryAdjustLogResult();
								if (queryAdjustLogResult != null) {

									com.huawei.www.bme.cbsinterface.arservices.QueryAdjustLogResultAdjustInfo[] adjustInfo = queryAdjustLogResult.getAdjustInfo();
									if (adjustInfo != null && adjustInfo.length > 0) {
										ArrayList<BalanceDetail> listCoreBalance= new ArrayList<BalanceDetail>();
										double chargeAmount=0;
										boolean isCoreBalance=false;
										boolean isCoreBalance2=false;

										for (com.huawei.www.bme.cbsinterface.arservices.QueryAdjustLogResultAdjustInfo itemInfo : adjustInfo) {
											if (itemInfo.getTradeTime().equals(selectedHistory.getTradeTime()) && selectedHistory.getTransId().equals(itemInfo.getTransID() + "")) {

												com.huawei.www.bme.cbsinterface.arservices.QueryAdjustLogResultAdjustInfoBalanceAdjustmentInfo[] balanceAdjustmentInfo = itemInfo.getBalanceAdjustmentInfo();

												if (balanceAdjustmentInfo != null) {
													for (com.huawei.www.bme.cbsinterface.arservices.QueryAdjustLogResultAdjustInfoBalanceAdjustmentInfo detail : balanceAdjustmentInfo) {
														BalanceDetail bd = new BalanceDetail();
														bd.setBalanceTypeName(detail.getBalanceTypeName());

														if (detail.getAdjustmentAmt() != null) {
															double redondear = UtilNumber.redondear(((double) detail.getAdjustmentAmt() / (double) Parametros.billeteraDivisor), Parametros.billeteraNroDecimales);
															bd.setChargeAmount(UtilNumber.doubleToString(redondear, Parametros.billeteraNroDecimales));
														}

														if (detail.getCurrencyID() != null)
															bd.setCurrencyId(getCurrencyDescription(detail.getCurrencyID().intValue()));
														if (detail.getAdjustmentType() != null)
															bd.setChargeCode(hashAdjustmentType.get(detail.getAdjustmentType().trim()));

														Date date = UtilDate.stringToDate(detail.getExpireTime(), Parametros.fechaFormatWs);
														bd.setChargeCodeName(UtilDate.dateToString(date, Parametros.fechaFormatPage));

														if(bd.getBalanceTypeName().contains("CORE_BALANCE")){
															if(bd.getChargeAmount()!=null  && !bd.getChargeAmount().isEmpty()) {
																chargeAmount += Double.valueOf(bd.getChargeAmount().replaceAll(",", "."));
															}
															if(bd.getBalanceTypeName().equals("CORE_BALANCE")){
																isCoreBalance=true;
															}
															if(bd.getBalanceTypeName().equals("CORE_BALANCE_2")){
																isCoreBalance2=true;
															}
															listCoreBalance.add(bd);
														}else {
															getListBalanceDetail().add(bd);
														}
													}
												}

												com.huawei.www.bme.cbsinterface.arservices.QueryAdjustLogResultAdjustInfoFreeUnitAdjustmentInfo[] freeUnitAdjustmentInfo = itemInfo.getFreeUnitAdjustmentInfo();
												if (freeUnitAdjustmentInfo != null) {

													for (com.huawei.www.bme.cbsinterface.arservices.QueryAdjustLogResultAdjustInfoFreeUnitAdjustmentInfo detail : freeUnitAdjustmentInfo) {
														BalanceDetail bd = new BalanceDetail();
														bd.setBalanceTypeName(detail.getFreeUnitTypeName());

														if (detail.getAdjustmentAmt() != null) { // el
															// monto
															// de
															// FreeUnitAdjustmentInfo
															// ->
															// AdjustmentAmt
															// lo
															// redondea
															// a
															// N
															// decimales
															bd.setChargeAmount(UtilNumber.doubleToString(detail.getAdjustmentAmt(), Parametros.billeteraNroDecimales));
														}

														if (detail.getMeasureUnit() != null) {
															FreeUnitMeasure freeUnitMeasure = hashFreeUnitMeasure.get(detail.getMeasureUnit().intValue() + "");
															// if
															// (selectedHistory.getMeasureUnit()
															// != null) {
															// FreeUnitMeasure
															// freeUnitMeasure =
															// hashFreeUnitMeasure.get(selectedHistory.getMeasureUnit().intValue()
															// + "");
															if (freeUnitMeasure != null) {
																log.debug("detail.getAdjustmentAmt: " + detail.getAdjustmentAmt() + ", " + freeUnitMeasure);
																if (detail.getAdjustmentAmt() != null) {
																	double total = UtilNumber.redondear((double) detail.getAdjustmentAmt() / freeUnitMeasure.getDividendo(), Parametros.billeteraNroDecimales);
																	bd.setChargeAmount(UtilNumber.doubleToString(total, Parametros.billeteraNroDecimales));

																	bd.setCurrencyId(freeUnitMeasure.getUnidad());
																} else {
																	log.debug("detail.getAdjustmentAmt resuelto a nulo");
																}
															} else {
																log.debug("freeUnitMeasure resuelto a nulo con ID: " + detail.getMeasureUnit().intValue());
															}
														} else {
															// log.debug("selectedHistory.getMeasureUnit
															// resuelto a
															// nulo");
															log.debug("QueryAdjustLogResultAdjustInfoFreeUnitAdjustmentInfo.getMeasureUnit resuelto a nulo");
														}

														// if
														// (detail.getMeasureUnit()
														// != null)
														// la unidad de medida
														// FreeUnitAdjustmentInfo
														// -> MeasureUnit lo
														// obtiene de
														// currency.xml
														// bd.setCurrencyId(getCurrencyDescription(detail.getMeasureUnit().intValue()));
														if (detail.getAdjustmentType() != null)
															// el valor de
															// ChargeCode
															// FreeUnitAdjustmentInfo
															// -> AdjustmentType
															// lo obtiene de
															// properties
															// HISTORIAL.ADJUSTMENT.TYPE
															// (1:CR, 2:DR)
															bd.setChargeCode(hashAdjustmentType.get(detail.getAdjustmentType().trim()));

														Date date = UtilDate.stringToDate(detail.getExpireTime(), Parametros.fechaFormatWs);
														bd.setChargeCodeName(UtilDate.dateToString(date, Parametros.fechaFormatPage));

														if(bd.getBalanceTypeName().contains("CORE_BALANCE")){
															if(bd.getChargeAmount()!=null  && !bd.getChargeAmount().isEmpty()) {
																chargeAmount += Double.valueOf(bd.getChargeAmount().replaceAll(",", "."));
															}
															if(bd.getBalanceTypeName().equals("CORE_BALANCE")){
																isCoreBalance=true;
															}
															if(bd.getBalanceTypeName().equals("CORE_BALANCE_2")){
																isCoreBalance2=true;
															}
															listCoreBalance.add(bd);
														}else {
															getListBalanceDetail().add(bd);
														}
													}
												} else {
													log.warn("freeUnitAdjustmentInfo resuelto a nulo");
												}
											}
										}
										String textAlign="left";
										if(listCoreBalance.size()>1) {

											BalanceDetail bd = new BalanceDetail();
											String balanceTypeName=isCoreBalance? (isCoreBalance2?"CORE_BALANCE + CORE_BALANCE_2":"CORE_BALANCE"):"CORE_BALANCE_2";
											bd.setBalanceTypeName(balanceTypeName);
											bd.setColor("#e2e2e2");
											bd.setCurrencyId(listCoreBalance.get(0).getCurrencyId());
											bd.setChargeCode(listCoreBalance.get(0).getChargeCode());
											bd.setChargeCodeName(listCoreBalance.get(0).getChargeCodeName());
											bd.setChargeAmount(UtilNumber.doubleToString(chargeAmount, Parametros.billeteraNroDecimales));
											textAlign="right";
											getListBalanceDetail().add(bd);

										}
										for (BalanceDetail itemsde : listCoreBalance) {
											itemsde.setTextAlign(textAlign);
											getListBalanceDetail().add(itemsde);
										}
									}

								} else {
									log.warn("[isdn: " + getIsdn() + "] queryAdjustLogResult resuelto a nulo");
								}

							} else {
								log.info("[isdn: " + getIsdn() + "] queryAdjustLog ResultCode: " + queryAdjustLog.getResultHeader().getResultCode() + ", ResultDesc: " + queryAdjustLog.getResultHeader().getResultDesc());
								SysMessage.warn("Servicio ARServices, queryAdjustLog Code: " + queryAdjustLog.getResultHeader().getResultCode() + ", Desc: " + queryAdjustLog.getResultHeader().getResultDesc(), null);
								guardarExcepcion(idAuditoriaHistorial, "Servicio ARServices, queryAdjustLog Code: " + queryAdjustLog.getResultHeader().getResultCode() + ", Desc: " + queryAdjustLog.getResultHeader().getResultDesc());
							}
						} else {
							if (queryAdjustLog != null) {
								log.warn("[isdn: " + getIsdn() + "] queryAdjustLog ResultHeader resuelto a nulo");
							} else {
								log.warn("[isdn: " + getIsdn() + "] queryAdjustLog queryAdjustLog resuelto a nulo");
							}
						}
					} finally {
						portNodo.setEnUso(false);
						portNodo.setFechaFin(new Date());
					}
				}
			} else {
				log.error("[isdn: " + getIsdn() + "] Port ARServices historial resuelto a nulo");
			}

		} catch (AxisFault a) {
			String msg = a.getMessage();
			msg = msg.toLowerCase();
			if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase()) || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase()) || msg.contains("UnknownHostException".toLowerCase()) || msg.contains("No route to host".toLowerCase())
					|| msg.contains("Connection refused".toLowerCase())) {
				log.error("Error de conexion al servicio ARServices historial: ", a);
				SysMessage.error("Error de conexion del servicio ARServices", null);
				guardarExcepcion(idAuditoriaHistorial, "Error de conexion al servicio ARServices: " + stackTraceToString(a));
			} else {
				log.error("Error AxisFault: ", a);
				SysMessage.error("Error AxisFault de conexion al servicio ARServices", null);
				guardarExcepcion(idAuditoriaHistorial, "Error AxisFault de conexion al servicio ARServices: " + stackTraceToString(a));
			}
		} catch (ServiceException e) {
			log.error("Error de servicio al conectarse al servicio ARServices historial: ", e);
			SysMessage.error("Error de servicio al conectarse al servicio ARServices", null);
			guardarExcepcion(idAuditoriaHistorial, "Error de servicio al conectarse al servicio ARServices: " + stackTraceToString(e));
		} catch (RemoteException e) {
			log.error("Error remoto al conectarse al servicio ARServices historial: ", e);
			SysMessage.error("Error remoto al conectarse al servicio ARServices", null);
			guardarExcepcion(idAuditoriaHistorial, "Error remoto al conectarse al servicio ARServices: " + stackTraceToString(e));
		} catch (Exception e) {
			log.error("Error al consultar ARServices historial", e);
			SysMessage.error("Error al consultar ARServices", null);
			guardarExcepcion(idAuditoriaHistorial, "Error al consultar ARServices: " + stackTraceToString(e));
		}
	}
	// cambiar para historico
	private void cargarBalanceDetailCDR() {

		log.debug("Ingresando a cargar balance details");
		try {
			setListBalanceDetail(new ArrayList<BalanceDetail>());

			String wsdlLoc = Parametros.historialWsdl;

			URL url = new URL(UtilUrl.getIp(wsdlLoc));
			long ini = System.currentTimeMillis();

			if (url.getProtocol().equalsIgnoreCase("https")) {
				int puerto = url.getPort();
				String host = url.getHost();
				if (host.equalsIgnoreCase("localhost")) {
					host = "127.0.0.1";
				}
				validarCertificado(Parametros.historialPathKeystore, host + ":" + puerto);
			}

			NodoServicio<com.huawei.www.bme.cbsinterface.bbservices.BbServicsBindingStub> portNodo = ServicioHistorial.initPort(wsdlLoc, "BALANCE_DETAIL" + login, Parametros.historialTimeOut);

			if (portNodo != null && portNodo.getPort() != null) {

				synchronized (portNodo) {
					try {
						com.huawei.www.bme.cbsinterface.bbservices.BbServicsBindingStub port = portNodo.getPort();

						com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo ownershipInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo();
						ownershipInfo.setBEID(Parametros.historialOwnerShipInfoId);
						com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo accessSecurity = new com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo();
						accessSecurity.setLoginSystemCode(Parametros.historialLoginSystemCode);
						accessSecurity.setPassword(Parametros.historialPassword);
						com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo operatorInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo();
						operatorInfo.setOperatorID(Parametros.historialOperatorId);
						com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat timeFormat = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat();
						timeFormat.setTimeType(Parametros.historialTimeType);
						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
						String messageSeq = format.format(new Date());

						com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderAdditionalProperty[] additionalProperty = null;
						com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader requestHeader = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader(Parametros.historialVersion, "", messageSeq, ownershipInfo, accessSecurity, operatorInfo, "", "", timeFormat, additionalProperty);
						com.huawei.www.bme.cbsinterface.bbservices.QueryCDRDetailRequestMsg queryCDRDetailRequestMsg = new com.huawei.www.bme.cbsinterface.bbservices.QueryCDRDetailRequestMsg();
						queryCDRDetailRequestMsg.setRequestHeader(requestHeader);
						com.huawei.www.bme.cbsinterface.bbservices.QueryCDRDetailRequest queryCDRDetailRequest = new com.huawei.www.bme.cbsinterface.bbservices.QueryCDRDetailRequest();

						queryCDRDetailRequest.setPrimaryIdentity(isdn);
						queryCDRDetailRequest.setCdrSeq(selectedHistory.getCdrSecuencia());

						// Date date =
						// UtilDate.stringToDate(selectedHistory.getStartDate(),
						// Parametros.fechaFormatPage);
						// queryCDRDetailRequest.setStartTime(UtilDate.dateToString(date,
						// Parametros.fechaFormatWs));
						queryCDRDetailRequest.setStartTime(selectedHistory.getStartTime());
						queryCDRDetailRequest.setEndTime(selectedHistory.getEndTime());
						queryCDRDetailRequest.setBillCycleID(selectedHistory.getBillCycleId());

						queryCDRDetailRequestMsg.setQueryCDRDetailRequest(queryCDRDetailRequest);
						com.huawei.www.bme.cbsinterface.bbservices.QueryCDRDetailResultMsg queryCDRDetail = port.queryCDRDetail(queryCDRDetailRequestMsg);

						long fin = System.currentTimeMillis();
						Long Con = null;
						if (portNodo.isPrimeraVez()) {
							Con = portNodo.getTiempoConexion();
						}
						log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio BBServices Historial: " + (fin - ini) + " milisegundos");

						if (Parametros.saveRequestResponse) {
							try {
								saveXml(isdn, getLogin(), wsdlLoc, "queryCDRDetail", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoriaHistorial, Con, (fin - ini));
							} catch (Exception e) {
								log.warn("No se logro registrar los request y response del servicio BBServices, queryCDRDetail: ", e);
							}
						}

						if (queryCDRDetail != null && queryCDRDetail.getResultHeader() != null) {
							if (queryCDRDetail.getResultHeader().getResultCode().trim().equals("0")) {

								com.huawei.www.bme.cbsinterface.bbservices.QueryCDRDetailResult queryCDRDetailResult = queryCDRDetail.getQueryCDRDetailResult();
								if (queryCDRDetailResult != null) {
									com.huawei.www.bme.cbsinterface.bbservices.QueryCDRDetailResultCDRInfo cdrInfo = queryCDRDetailResult.getCDRInfo();

									com.huawei.www.bme.cbsinterface.bbservices.QueryCDRDetailResultCDRInfoVolumeInfoFreeUnitList[] volumeInfo = cdrInfo.getVolumeInfo();
									ArrayList<BalanceDetail> listCoreBalance= new ArrayList<BalanceDetail>();
									double chargeAmount=0;
									double currentAmount=0;
									boolean isCoreBalance=false;
									boolean isCoreBalance2=false;
									if (volumeInfo != null && volumeInfo.length > 0) {

										for (com.huawei.www.bme.cbsinterface.bbservices.QueryCDRDetailResultCDRInfoVolumeInfoFreeUnitList reg : volumeInfo) {

											com.huawei.www.bme.cbsinterface.bccustomizedservices.QueryBalFUTypeResultBalFUTypeInfoList balFuType = obtenerBalFUType(reg.getFreeUnitType());

											BalanceDetail bd = new BalanceDetail();
											bd.setChargeCode(reg.getFreeUnitId() + "");
											bd.setBalanceTypeName(reg.getFreeUnitTypeName());

											if (balFuType != null) {
												bd.setBalanceTypeName(balFuType.getBalanceFreeUnitName());

												FreeUnitMeasure freeUnitMeasure = hashFreeUnitMeasure.get(balFuType.getMeasureUnit().intValue() + "");
												if (freeUnitMeasure != null) {
													double total = UtilNumber.redondear((double) reg.getAmount() / freeUnitMeasure.getDividendo(), Parametros.historialNroDecimales);
													bd.setChargeAmount(UtilNumber.doubleToString(total, Parametros.historialNroDecimales));
													bd.setCurrencyId(freeUnitMeasure.getUnidad());

													if (enabledColumnCurrentAmount) { // CURRENT_AMOUNT - CHANGE_REQUEST reg.getCurrentAmount
														if (reg.getCurrentAmount() != null) {
															double totalCurrentAmount = UtilNumber.redondear((double) reg.getCurrentAmount() / freeUnitMeasure.getDividendo(), Parametros.historialNroDecimales);
															bd.setCurrentAmount(UtilNumber.doubleToString(totalCurrentAmount, Parametros.historialNroDecimales));
														} else {
															log.warn("CurrentAmount resuelto a nulo");
															bd.setCurrentAmount("");
														}
													}
												} else {
													bd.setChargeAmount(reg.getAmount() + "");
													bd.setCurrencyId(balFuType.getMeasureUnitName());

													if (enabledColumnCurrentAmount) { // CURRENT_AMOUNT -CHANGE_REQUEST reg.getCurrentAmount
														bd.setCurrentAmount(reg.getCurrentAmount() + "");
													}
												}

											} else {
												bd.setChargeAmount(reg.getAmount() + "");
												bd.setCurrencyId("");

											}

											if (reg.getFreeUnitType() != null) {
												bd.setChargeCodeName(reg.getFreeUnitType());

												String type = hashFreeUnit.get(reg.getFreeUnitType().trim());
												if (type != null) {
													bd.setChargeCodeName(type);
												}
											} else
												bd.setChargeCodeName("");
											if(bd.getBalanceTypeName().contains("CORE_BALANCE")){
												if(bd.getChargeAmount()!=null  && !bd.getChargeAmount().isEmpty()) {
													chargeAmount += Double.valueOf(bd.getChargeAmount().replaceAll(",", "."));
												}
												if(bd.getCurrentAmount()!=null  && !bd.getCurrentAmount().isEmpty()) {
													currentAmount += Double.valueOf(bd.getCurrentAmount().replaceAll(",", "."));
												}
												if(bd.getBalanceTypeName().equals("CORE_BALANCE")){
													isCoreBalance=true;
												}
												if(bd.getBalanceTypeName().equals("CORE_BALANCE_2")){
													isCoreBalance2=true;
												}
												listCoreBalance.add(bd);
											}else {
												getListBalanceDetail().add(bd);
											}
										}

									} else {
										log.warn("[isdn: " + getIsdn() + "] queryCDRDetail volumeInfo resuelto a nulo o vacio");
									}

									com.huawei.www.bme.cbsinterface.bbservices.QueryCDRDetailResultCDRInfoChargeDetail[] chargeDetail = cdrInfo.getChargeDetail();

									if (chargeDetail != null && chargeDetail.length > 0) {
										for (com.huawei.www.bme.cbsinterface.bbservices.QueryCDRDetailResultCDRInfoChargeDetail detail : chargeDetail) {
											BalanceDetail bd = new BalanceDetail();
											bd.setBalanceTypeName(detail.getBalanceTypeName());

											if (enabledColumnCurrentAmount) { // CURRENT_AMOUNT - CHANGE_REQUEST detail.getCurrentAmount
												if (detail.getCurrentAmount() != null) {
													double totalCurrentAmount = UtilNumber.redondear((double) detail.getCurrentAmount() / (double) Parametros.historialDivisor, Parametros.historialNroDecimales);
													bd.setCurrentAmount(UtilNumber.doubleToString(totalCurrentAmount, Parametros.historialNroDecimales));
												} else {
													log.warn("CurrentAmount resuelto a nulo");
													bd.setCurrentAmount("");
												}
											}

											double total = UtilNumber.redondear((double) detail.getChargeAmount() / (double) Parametros.historialDivisor, Parametros.historialNroDecimales);

											bd.setChargeAmount(UtilNumber.doubleToString(total, Parametros.historialNroDecimales));

											if (detail.getCurrencyID() != null) {
												String cd = getCurrencyDescription(detail.getCurrencyID().intValue());
												if (cd != null) {
													bd.setCurrencyId(cd);
												} else
													bd.setCurrencyId(detail.getCurrencyID().intValue() + "");
											}

											bd.setChargeCode(detail.getChargeCode());
											bd.setChargeCodeName(detail.getChargeCodeName());
											if(bd.getBalanceTypeName().contains("CORE_BALANCE")){
												if(bd.getChargeAmount()!=null  && !bd.getChargeAmount().isEmpty()) {
													chargeAmount += Double.valueOf(bd.getChargeAmount().replaceAll(",", "."));
												}
												if(bd.getCurrentAmount()!=null  && !bd.getCurrentAmount().isEmpty()) {
													currentAmount += Double.valueOf(bd.getCurrentAmount().replaceAll(",", "."));
												}
												if(bd.getBalanceTypeName().equals("CORE_BALANCE")){
													isCoreBalance=true;
												}
												if(bd.getBalanceTypeName().equals("CORE_BALANCE_2")){
													isCoreBalance2=true;
												}
												listCoreBalance.add(bd);
											}else {
												getListBalanceDetail().add(bd);
											}
										}
									} else {
										log.warn("[isdn: " + getIsdn() + "] chargeDetail resuelto a nulo o lista vacia");
									}
									String textAlign="left";
									if(listCoreBalance.size()>1) {

										BalanceDetail bd = new BalanceDetail();
										String balanceTypeName=isCoreBalance? (isCoreBalance2?"CORE_BALANCE + CORE_BALANCE_2":"CORE_BALANCE"):"CORE_BALANCE_2";
										bd.setBalanceTypeName(balanceTypeName);
										bd.setColor("#e2e2e2");
										bd.setCurrencyId(listCoreBalance.get(0).getCurrencyId());
										bd.setChargeCode(listCoreBalance.get(0).getChargeCode());
										bd.setChargeCodeName(listCoreBalance.get(0).getChargeCodeName());
										bd.setChargeAmount(UtilNumber.doubleToString(chargeAmount, Parametros.billeteraNroDecimales));
										bd.setCurrentAmount(UtilNumber.doubleToString(currentAmount, Parametros.billeteraNroDecimales));
										textAlign="right";
										getListBalanceDetail().add(bd);

									}
									for (BalanceDetail itemsde : listCoreBalance) {
										itemsde.setTextAlign(textAlign);
										getListBalanceDetail().add(itemsde);
									}
								} else {
									log.warn("[isdn: " + getIsdn() + "] queryCDRDetail queryCDRDetailResult resuelto a nulo");
								}
							} else {
								log.info("[isdn: " + getIsdn() + "] queryCDRDetail ResultCode: " + queryCDRDetail.getResultHeader().getResultCode() + ", ResultDesc: " + queryCDRDetail.getResultHeader().getResultDesc());
								SysMessage.warn("Servicio BBServices, queryCDRDetail Code: " + queryCDRDetail.getResultHeader().getResultCode() + ", Desc: " + queryCDRDetail.getResultHeader().getResultDesc(), null);
								guardarExcepcion(idAuditoriaHistorial, "Servicio BBServices, queryCDRDetail Code: " + queryCDRDetail.getResultHeader().getResultCode() + ", Desc: " + queryCDRDetail.getResultHeader().getResultDesc());
							}
						} else {
							if (queryCDRDetail != null) {
								log.warn("[isdn: " + getIsdn() + "] queryCDRDetail queryCDRDetail resuelto a nulo");
							} else {
								log.warn("[isdn: " + getIsdn() + "] queryCDRDetail ResultHeader resuelto a nulo");
							}
						}
					} finally {
						portNodo.setEnUso(false);
						portNodo.setFechaFin(new Date());
					}
				}
			} else {
				log.error("[isdn: " + getIsdn() + "] Port BBServices Historial resuelto a nulo");
			}

		} catch (AxisFault a) {
			String msg = a.getMessage();
			msg = msg.toLowerCase();
			if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase()) || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase()) || msg.contains("UnknownHostException".toLowerCase()) || msg.contains("No route to host".toLowerCase())
					|| msg.contains("Connection refused".toLowerCase())) {
				log.error("Error de conexion al servicio BBServices Historial: ", a);
				SysMessage.error("Error de conexion del servicio BBServices", null);
				guardarExcepcion(idAuditoriaHistorial, "Error de conexion al servicio BBServices: " + stackTraceToString(a));
			} else {
				log.error("Error AxisFault Historial: ", a);
				SysMessage.error("Error AxisFault de conexion al servicio BBServices", null);
				guardarExcepcion(idAuditoriaHistorial, "Error AxisFault de conexion al servicio BBServices: " + stackTraceToString(a));
			}
		} catch (ServiceException e) {
			log.error("Error de servicio al conectarse al servicio BBServices Historial: ", e);
			SysMessage.error("Error de servicio al conectarse al servicio BBServices", null);
			guardarExcepcion(idAuditoriaHistorial, "Error de servicio al conectarse al servicio BBServices: " + stackTraceToString(e));
		} catch (RemoteException e) {
			log.error("Error remoto al conectarse al servicio BBServices Historial: ", e);
			SysMessage.error("Error remoto al conectarse al servicio BBServices", null);
			guardarExcepcion(idAuditoriaHistorial, "Error remoto al conectarse al servicio BBServices: " + stackTraceToString(e));
		} catch (Exception e) {
			log.error("Error al consultar BBServices Historial", e);
			SysMessage.error("Error al consultar BBServices", null);
			guardarExcepcion(idAuditoriaHistorial, "Error al consultar BBServices: " + stackTraceToString(e));
		}

	}

	private com.huawei.www.bme.cbsinterface.bccustomizedservices.QueryBalFUTypeResultBalFUTypeInfoList obtenerBalFUType(String balanceFreeUnitType) {
		log.debug("Ingresando a cargar bal FU type");
		try {

			String wsdlLoc = Parametros.customizedWsdl;

			URL url = new URL(UtilUrl.getIp(wsdlLoc));
			long ini = System.currentTimeMillis();

			if (url.getProtocol().equalsIgnoreCase("https")) {
				int puerto = url.getPort();
				String host = url.getHost();
				if (host.equalsIgnoreCase("localhost")) {
					host = "127.0.0.1";
				}
				validarCertificado(Parametros.customizedPathKeystore, host + ":" + puerto);
			}

			NodoServicio<com.huawei.www.bme.cbsinterface.bccustomizedservices.BCCustomizedServicesBindingStub> portNodo = ServicioCustomized.initPort(wsdlLoc, "CUSTOMIZED" + login, Parametros.customizedTimeOut);

			if (portNodo != null && portNodo.getPort() != null) {

				synchronized (portNodo) {
					try {
						com.huawei.www.bme.cbsinterface.bccustomizedservices.BCCustomizedServicesBindingStub port = portNodo.getPort();

						com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo ownershipInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OwnershipInfo();
						ownershipInfo.setBEID(Parametros.customizedOwnerShipInfoId);
						com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo accessSecurity = new com.huawei.www.bme.cbsinterface.cbscommon.SecurityInfo();
						accessSecurity.setLoginSystemCode(Parametros.customizedLoginSystemCode);
						accessSecurity.setPassword(Parametros.customizedPassword);
						com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo operatorInfo = new com.huawei.www.bme.cbsinterface.cbscommon.OperatorInfo();
						operatorInfo.setOperatorID(Parametros.customizedOperatorId);
						com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat timeFormat = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeaderTimeFormat();
						timeFormat.setTimeType(Parametros.customizedTimeType);

						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
						String messageSeq = format.format(new Date());

						com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader requestHeader = new com.huawei.www.bme.cbsinterface.cbscommon.RequestHeader(Parametros.customizedVersion, "", messageSeq, ownershipInfo, accessSecurity, operatorInfo, "", "", timeFormat, null);

						com.huawei.www.bme.cbsinterface.bccustomizedservices.QueryBalFUTypeRequest queryBalFUTypeRequest = new com.huawei.www.bme.cbsinterface.bccustomizedservices.QueryBalFUTypeRequest(balanceFreeUnitType, "");
						com.huawei.www.bme.cbsinterface.bccustomizedservices.QueryBalFUTypeRequestMsg queryBalFUTypeRequestMsg = new com.huawei.www.bme.cbsinterface.bccustomizedservices.QueryBalFUTypeRequestMsg(requestHeader, queryBalFUTypeRequest);
						com.huawei.www.bme.cbsinterface.bccustomizedservices.QueryBalFUTypeResultMsg queryBalFUType = port.queryBalFUType(queryBalFUTypeRequestMsg);

						long fin = System.currentTimeMillis();
						Long Con = null;
						if (portNodo.isPrimeraVez()) {
							Con = portNodo.getTiempoConexion();
						}
						log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio BCCustomizedServices, queryBalFUType: " + (fin - ini) + " milisegundos");

						if (Parametros.saveRequestResponse) {
							try {
								saveXml("N/A", getLogin(), wsdlLoc, "queryBalFUType", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoriaHistorial, Con, (fin - ini));
							} catch (Exception e) {
								log.warn("No se logro registrar los request y response del servicio BCCustomizedServices, queryBalFUType: ", e);
							}
						}

						if (queryBalFUType != null && queryBalFUType.getResultHeader() != null) {
							if (queryBalFUType.getResultHeader().getResultCode().trim().equals("0")) {

								com.huawei.www.bme.cbsinterface.bccustomizedservices.QueryBalFUTypeResultBalFUTypeInfoList[] queryBalFUTypeResult = queryBalFUType.getQueryBalFUTypeResult();
								if (queryBalFUTypeResult != null && queryBalFUTypeResult.length > 0) {
									if (queryBalFUTypeResult.length > 1)
										log.warn("Se obtuvo mas de un Balance FU Type");
									return queryBalFUTypeResult[0];
								}
							} else {
								log.info("[isdn: " + getIsdn() + "] queryBalFUType ResultCode: " + queryBalFUType.getResultHeader().getResultCode() + ", ResultDesc: " + queryBalFUType.getResultHeader().getResultDesc());
								SysMessage.warn("Servicio BCCustomized, queryBalFUType ResultCode: " + queryBalFUType.getResultHeader().getResultCode() + ", ResultDesc: " + queryBalFUType.getResultHeader().getResultDesc(), null);
								guardarExcepcion(idAuditoriaHistorial, "Servicio BCCustomized, queryBalFUType ResultCode: " + queryBalFUType.getResultHeader().getResultCode() + ", ResultDesc: " + queryBalFUType.getResultHeader().getResultDesc());
							}
						} else {
							if (queryBalFUType != null) {
								log.warn("[isdn: " + getIsdn() + "] queryBalFUType queryBalFUType resuelto a nulo");
							} else {
								log.warn("[isdn: " + getIsdn() + "] queryBalFUType ResultHeader resuelto a nulo");
							}
						}
					} finally {
						portNodo.setEnUso(false);
						portNodo.setFechaFin(new Date());
					}
				}
			} else {
				log.error("[isdn: " + getIsdn() + "] Port BCCustomized resuelto a nulo");
			}

		} catch (AxisFault a) {
			String msg = a.getMessage();
			msg = msg.toLowerCase();
			if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase()) || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase()) || msg.contains("UnknownHostException".toLowerCase()) || msg.contains("No route to host".toLowerCase())
					|| msg.contains("Connection refused".toLowerCase())) {
				log.error("Error de conexion al servicio BCCustomized queryBalFUType: ", a);
				SysMessage.error("Error de conexion del servicio BCCustomized", null);
				guardarExcepcion(idAuditoriaHistorial, "Error de conexion al servicio BCCustomized queryBalFUType: " + stackTraceToString(a));
			} else {
				log.error("Error AxisFault: ", a);
				SysMessage.error("Error AxisFault de conexion al servicio BCCustomized", null);
				guardarExcepcion(idAuditoriaHistorial, "Error AxisFault de conexion al servicio BCCustomized: " + stackTraceToString(a));
			}
		} catch (ServiceException e) {
			voucher = new Voucher();
			log.error("Error de servicio al conectarse al servicio BCCustomized queryBalFUType: ", e);
			SysMessage.error("Error de servicio al conectarse al servicio BCCustomized", null);
			guardarExcepcion(idAuditoriaHistorial, "Error de servicio al conectarse al servicio BCCustomized queryBalFUType: " + stackTraceToString(e));
		} catch (RemoteException e) {
			voucher = new Voucher();
			log.error("Error remoto al conectarse al servicio BCCustomized queryBalFUType: ", e);
			SysMessage.error("Error remoto al conectarse al servicio BCCustomized", null);
			guardarExcepcion(idAuditoriaHistorial, "Error remoto al conectarse al servicio BCCustomized queryBalFUType: " + stackTraceToString(e));
		} catch (Exception e) {
			voucher = new Voucher();
			log.error("Error al consultar BCCustomized queryBalFUType", e);
			SysMessage.error("Error al consultar BCCustomized", null);
			guardarExcepcion(idAuditoriaHistorial, "Error al consultar BCCustomized queryBalFUType: " + stackTraceToString(e));
		}
		return null;

	}

	private void validarCertificado(String pathKeystore, String ipPort) {
		try {
			// Properties sysProperties = System.getProperties();
			log.debug("Ingresando a validar certificado pathKeystore: " + pathKeystore + ", ipPort: " + ipPort);

			System.setProperty("javax.net.ssl.trustStore", pathKeystore);
			System.setProperty("java.protocol.handler.pkgs", ipPort);

			HostnameVerifier hv = new HostnameVerifier() {
				public boolean verify(String urlHostName, SSLSession session) {
					// System.out.println("Warning: URL Host: " + urlHostName +
					// " vs. " + session.getPeerHost());
					return true;
				}
			};

			// trustAllHttpsCertificates();
			HttpsURLConnection.setDefaultHostnameVerifier(hv);
		} catch (Exception e) {
			log.error("Error al validar certificado: ", e);
		}
	}

	List<String> errores = new ArrayList<>();

	public void guardarIncidente() {
		log.debug("[saveVCIncidente]: Ingresando..");

		try {
			if (CodigoAuditoria > 0) {
				VcLogAuditoria find = (VcLogAuditoria) blAuditoria.find(CodigoAuditoria, VcLogAuditoria.class);

				if (disableIncidente && find != null) {
					SysMessage.warn(Parametros.mensajeValidacionIncidente, null);
					return;
				}
				VcIncidente item = new VcIncidente();
				item.setFechaIncidente(Calendar.getInstance());
				item.setAtencion(false);
				item.setUsuario(this.login);
				item.setVcLogAuditoria(find);
				String str = blIncidente.validar(item, true);
				if (!str.isEmpty()) {
					SysMessage.warn(str, null);
					return;
				}
				blIncidente.save(item);
				// ThreadTelnetRun t = new ThreadTelnetRun(CodigoAuditoria,
				// isdn,
				// getLogin(),blAuditoria,blExcepcion,blConsultaRed,servidorBL,blConexionTelnet);
				// t.start();

				try {
					RequestContext reqCtx = RequestContext.getCurrentInstance();
					reqCtx.execute("PF('pollTelnet').start();");
				} catch (Exception e) {
					log.error("Error: " + e.getMessage());
				}

				controlButtonTelnet.setDisabled(true);
				controlButtonTelnet.setPollStop(false);
				controlButtonTelnet.setFinalizado(false);
				controlButtonTelnet.setErrores(new ArrayList<String>());

				Thread t = new Thread(new Runnable() {

					@Override
					public void run() {
						boolean live = true;

						while (live) {
							try {
								log.debug("Conectando... ");

								ConexionElementosRed(CodigoAuditoria, isdn, getLogin());

								live = false;// finish the thread
							} catch (Exception e) {
								log.warn("Error al conectar el hilo telnet: ", e);

							} finally {
								live = false;
								log.debug("Finalizo el hilo: Elementos Red.");

								controlButtonTelnet.setDisabled(false);
								controlButtonTelnet.setFinalizado(true);
								log.debug("FINALIZARON TODOS LOS HILOS DE ELEMENTOS RED controlButton.disabled: " + controlButtonTelnet.getDisabled());
								try {
									Thread.sleep(3000);
								} catch (Exception e2) {
									e2.getMessage();
								}
								controlButtonTelnet.setPollStop(true);
								log.debug("FINALIZARON TODOS LOS HILOS DE ELEMENTOS RED controlButton.PollStop: " + controlButtonTelnet.getPollStop());
							}
						}

					}

				});
				t.start();

				controlerBitacora.accion(this.vistaIncidente, "Se adiciono " + DescriptorBitacora.INCIDENTE + " con Id: " + String.valueOf(item.getIdIncidente()));
				SysMessage.info("Incidente Nro. " + String.valueOf(item.getIdIncidente()) + ": " + Parametros.mensajeReporteIncidente, null);
				disableIncidente = true;
				vistaIncidente = null;
			} else {
				SysMessage.warn(Parametros.mensajeValidacionIncidente, null);
				return;
			}
		} catch (Exception e) {
			log.error("Error al guardar incidente: ", e);
			SysMessage.error("Fallo al guardar en la Base de Datos.", null);
		}

	}

	public void guardarExcepcion(long IdAuditoria, String exepcion) {
		log.debug("[saveVCExcepcion]: Ingresando..");
		VcLogExcepcion item = new VcLogExcepcion();
		VcLogAuditoria find;
		try {
			find = (VcLogAuditoria) blAuditoria.find(IdAuditoria, VcLogAuditoria.class);
			item.setFechaCreacion(Calendar.getInstance());
			item.setExcepcion(exepcion);
			item.setVcLogAuditoria(find);

			blExcepcion.save(item);
		} catch (Exception e) {
			log.error("Error al guardar Excepcion: ", e);
		}
	}

	public void ConexionElementosRed(long IdAuditoria, String isdn, String login) {

		log.debug("Entrando a elementos de red");
		try {
			String idServidorPadre = "0";
			lanzandoHilos(IdAuditoria, isdn, login, idServidorPadre, "");
		} catch (Exception e) {
			log.error("error en conexion de elementos de red: ", e);
			guardarExcepcion(IdAuditoria, "error en conexion de elementos de red: " + stackTraceToString(e));
			controlButtonTelnet.getErrores().add("error en conexion de elementos de red. ");
		}

	}

	public void lanzandoHilos(long IdAuditoria, String isdn, String login, String idServidorPadre, String tramaPadre) {
		log.debug("Iniciando con la consulta...");
		try {
			List<VcServidor> elementosRed = servidorBL.findAllActive(idServidorPadre);
			if (elementosRed == null || elementosRed.isEmpty()) {
				log.debug("No se encontraron ELEMENTO DE RED hijos de: " + idServidorPadre);
				if (idServidorPadre.equals("0"))
					controlButtonTelnet.getErrores().add("No se encontraron ELEMENTO DE RED hijos de: " + idServidorPadre);
			} else {
				log.debug("Entro para la conexion:");
				ControlTelnet control = new ControlTelnet();
				control.setCantidadHilos(elementosRed.size());
				String trama = "";
				if (!tramaPadre.isEmpty())
					trama = tramaPadre.replaceAll(" ", "");
				for (VcServidor item : elementosRed) {
					List<VcConfigElementoRed> listExcepcion = ExcepcionBL.findAllExcepciones(item.getIdServidor());
					List<VcConexionTelnet> listaNodosTelnet = new ArrayList<>();

					if (item.getKeyCodServidor() == null || item.getKeyCodServidor().isEmpty() || trama.isEmpty()) {
						log.debug("No existe keycodServidor");
						listaNodosTelnet = blConexionTelnet.findAllActive(item.getIdServidor());

					} else {
						String key = item.getKeyCodServidor() + "=";
						String codServer = trama.substring(trama.indexOf(key) + key.length(), trama.indexOf(key) + key.length() + 11);
						log.debug("El codigo key: " + key + ": " + codServer);

						try {
							listaNodosTelnet = blConexionTelnet.findAllActiveOrderByCodServidor(item.getIdServidor(), codServer);

						} catch (Exception e) {
							// TODO Auto-generated catch block
							log.error("Finalizo los hilos de elementos red de la conexion: ", e);
							guardarExcepcion(IdAuditoria, "Finalizo los hilos de elementos red de la conexion: " + stackTraceToString(e));
						}
					}
					if (listaNodosTelnet.size() == 0) {
						log.debug("No se encuentran mas nodos para este elemento de red de la conexion: " + item.getNombreServidor());
					}
					Log.debug("Se inicio el Hilo de la conexion");
					ThreadTelnet telnet = new ThreadTelnet(item, control, isdn, listaNodosTelnet, controlButtonTelnet, listExcepcion);
					telnet.start();
				}
				while (!control.finalizaron()) {
					// log.debug("Aun no finalizaron los hilos.");
					try {
						Thread.sleep(2000);
					} catch (Exception e) {
						// log.error("Finalizo los hilos: ", e);
						guardarExcepcion(IdAuditoria, "Finalizo los hilos de elementos red de la conexion: " + stackTraceToString(e));
					}
				}
				log.debug("Han finalizado a los nodos hijos de elementos de red de la conexion: " + idServidorPadre);
				if (control.getSuccessConnections().size() == 0) {
					log.debug("No se encontro ninguna trama exitosa para los nodos hijos de elementos red: " + idServidorPadre);

				} else {
					// SAVE TRAMA HERE
					for (VcConexionTelnet nodo : control.getSuccessConnections()) {
						saveXmlRed(IdAuditoria, isdn, login, nodo.getIp(), nodo.getNombreElementoRed(), nodo.getEstado());
						log.debug("TRAMA:" + nodo.getEstado());
					}
				}
				// launching the sons threads of actual elemento Red
				for (VcServidor item : elementosRed) {
					// getting trama in each elementoRed
					String tramaSuccess = "";
					if (control.getSuccessConnections().size() != 0)
						for (VcConexionTelnet nodo : control.getSuccessConnections()) {
							if (nodo.getVcServidor().getIdServidor() == item.getIdServidor()) {
								tramaSuccess = nodo.getEstado();
								break;
							}
						}
					idServidorPadre = String.valueOf(item.getIdServidor());
					lanzandoHilos(IdAuditoria, isdn, login, idServidorPadre, tramaSuccess);
				}
				for (String ExcepcionServidor : control.getExcepcionServidor()) {
					guardarExcepcion(IdAuditoria, ExcepcionServidor);
				}
				for (String Error : control.getErrores()) {
					guardarExcepcion(IdAuditoria, Error);
				}
			}
		} catch (Exception e) {
			log.error("Error al lanzar hilos de elementos red conexion: ", e);
			guardarExcepcion(IdAuditoria, "Error al lanzar hilos de elementos red conexion: " + stackTraceToString(e));
			controlButtonTelnet.getErrores().add("Error al lanzar hilos de elementos red conexion");
		}
	}

	private String getCurrencyDescription(int currencyId) {
		String description = "";
		description = hashCurrency.get(currencyId + "");
		return description;
	}

	public List<Persona> getPersonas() {
		return personas;
	}

	public void setPersonas(List<Persona> personas) {
		this.personas = personas;
	}

	public List<CallingCircle> getCallingCircles() {
		return callingCircles;
	}

	public void setCallingCircles(List<CallingCircle> callingCircles) {
		this.callingCircles = callingCircles;
	}

	public List<Balance> getBalances() {
		return balances;
	}

	public void setBalances(List<Balance> balances) {
		this.balances = balances;
	}

	public Balance getSelectedBalance() {
		return selectedBalance;
	}

	public void setSelectedBalance(Balance selectedBalance) {
		this.selectedBalance = selectedBalance;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public List<Property> getPropertys() {
		return propertys;
	}

	public void setPropertys(List<Property> propertys) {
		this.propertys = propertys;
	}

	public String getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Voucher getVoucher() {
		return voucher;
	}

	public void setVoucher(Voucher voucher) {
		this.voucher = voucher;
	}

	public Cug getCug() {
		return cug;
	}

	public void setCug(Cug cug) {
		this.cug = cug;
	}

	public String getCircleName() {
		return circleName;
	}

	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}

	public String getIsdn() {
		return isdn;
	}

	public void setIsdn(String isdn) {
		this.isdn = isdn;
	}

	public String getCircleNamePool() {
		return circleNamePool;
	}

	public void setCircleNamePool(String circleNamePool) {
		this.circleNamePool = circleNamePool;
	}

	public ArrayList<SimpleVoucherOperation> getListVoucherOperation() {
		return listVoucherOperation;
	}

	public void setListVoucherOperation(ArrayList<SimpleVoucherOperation> listVoucherOperation) {
		this.listVoucherOperation = listVoucherOperation;
	}

	public CallingCircle getSelectedPrimaryIdentity() {
		return selectedPrimaryIdentity;
	}

	public void setSelectedPrimaryIdentity(CallingCircle selectedPrimaryIdentity) {
		this.selectedPrimaryIdentity = selectedPrimaryIdentity;
	}

	public List<CallingCircleProperties> getCallingCirclesProperties() {
		return callingCirclesProperties;
	}

	public void setCallingCirclesProperties(List<CallingCircleProperties> callingCirclesProperties) {
		this.callingCirclesProperties = callingCirclesProperties;
	}

	public ControlButton getControlButton() {
		return controlButton;
	}

	public void setControlButton(ControlButton controlButton) {
		this.controlButton = controlButton;
	}

	public String getValueRadioDay() {
		return valueRadioDay;
	}

	public void setValueRadioDay(String valueRadioDay) {
		this.valueRadioDay = valueRadioDay;
	}

	public String getValueRadioBetweenDay() {
		return valueRadioBetweenDay;
	}

	public void setValueRadioBetweenDay(String valueRadioBetweenDay) {
		this.valueRadioBetweenDay = valueRadioBetweenDay;
	}

	public boolean isDisabledRadioDay() {
		return disabledRadioDay;
	}

	public void setDisabledRadioDay(boolean disabledRadioDay) {
		this.disabledRadioDay = disabledRadioDay;
	}

	public boolean isDisabledRadioBetweenDay() {
		return disabledRadioBetweenDay;
	}

	public void setDisabledRadioBetweenDay(boolean disabledRadioBetweenDay) {
		this.disabledRadioBetweenDay = disabledRadioBetweenDay;
	}

	// public List<History> getListHistory() {
	// return listHistory;
	// }
	//
	// public void setListHistory(List<History> listHistory) {
	// this.listHistory = listHistory;
	// }

	public History getSelectedHistory() {
		return selectedHistory;
	}

	public void setSelectedHistory(History selectedHistory) {
		this.selectedHistory = selectedHistory;
	}

	public Integer getDaySelected() {
		return daySelected;
	}

	public void setDaySelected(Integer daySelected) {
		this.daySelected = daySelected;
	}

	public LazyDataModel<History> getModel() {
		return model;
	}

	public void setModel(LazyDataModel<History> model) {
		this.model = model;
	}

	public ArrayList<BalanceDetail> getListBalanceDetail() {
		return listBalanceDetail;
	}

	public void setListBalanceDetail(ArrayList<BalanceDetail> listBalanceDetail) {
		this.listBalanceDetail = listBalanceDetail;
	}

	public List<Checkbox> getListCheckbox() {
		return listCheckbox;
	}

	public void setListCheckbox(List<Checkbox> listCheckbox) {
		this.listCheckbox = listCheckbox;
	}

	public Checkbox[] getListCheckboxSelected() {
		return listCheckboxSelected;
	}

	public void setListCheckboxSelected(Checkbox[] listCheckboxSelected) {
		this.listCheckboxSelected = listCheckboxSelected;
	}

	public CheckboxService getCheckboxService() {
		return checkboxService;
	}

	public void setCheckboxService(CheckboxService checkboxService) {
		this.checkboxService = checkboxService;
	}

	public String getLogin() {
		return login;
	}

	public boolean isDisableTab() {
		return disableTab;
	}

	public void setDisableTab(boolean disableTab) {
		this.disableTab = disableTab;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getNro() {
		return nro;
	}

	public void setNro(String nro) {
		this.nro = nro;
	}

	public boolean isReadInput() {
		return readInput;
	}

	public void setReadInput(boolean readInput) {
		this.readInput = readInput;
	}

	public boolean isDisableButton() {
		return disableButton;
	}

	public void setDisableButton(boolean disableButton) {
		this.disableButton = disableButton;
	}

	public boolean isAll() {
		return all;
	}

	public void setAll(boolean all) {
		this.all = all;
	}

	public int getMaxDias() {
		return maxDias;
	}

	public void setMaxDias(int maxDias) {
		this.maxDias = maxDias;
	}

	public List<LifeCycle> getLifeCycles() {
		return lifeCycles;
	}

	public void setLifeCycles(List<LifeCycle> lifeCycles) {
		this.lifeCycles = lifeCycles;
	}

	public String getBlackList() {
		return blackList;
	}

	public void setBlackList(String blackList) {
		this.blackList = blackList;
	}

	public TreeNode getRootF() {
		return rootF;
	}

	public void setRootF(TreeNode rootF) {
		this.rootF = rootF;
	}

	public String getTipoSubscriber() {
		return tipoSubscriber;
	}

	public void setTipoSubscriber(String tipoSubscriber) {
		this.tipoSubscriber = tipoSubscriber;
	}

	public TreeNode getRootO() {
		return rootO;
	}

	public void setRootO(TreeNode rootO) {
		this.rootO = rootO;
	}

	public boolean isDisableIncidente() {
		return disableIncidente;
	}

	public void setDisableIncidente(boolean disableIncidente) {
		this.disableIncidente = disableIncidente;
	}

	public String getStatusDetail() {
		return statusDetail;
	}

	public void setStatusDetail(String statusDetail) {
		this.statusDetail = statusDetail;
	}

	public Enum getVistaIncidente() {
		return vistaIncidente;
	}

	public void setVistaIncidente(Enum vistaIncidente) {
		this.vistaIncidente = vistaIncidente;
	}

	public ControlButton getControlButtonTelnet() {
		return controlButtonTelnet;
	}

	public void setControlButtonTelnet(ControlButton controlButtonTelnet) {
		this.controlButtonTelnet = controlButtonTelnet;
	}

	public boolean isEnabledColumnCurrentAmount() {
		return enabledColumnCurrentAmount;
	}

	public void setEnabledColumnCurrentAmount(boolean enabledColumnCurrentAmount) {
		this.enabledColumnCurrentAmount = enabledColumnCurrentAmount;
	}

}
