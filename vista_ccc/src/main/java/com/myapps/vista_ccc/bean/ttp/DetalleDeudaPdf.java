package com.myapps.vista_ccc.bean.ttp;

public class DetalleDeudaPdf {

	private String producto;
	private String fecha;
	private String monto;
	private String comision;
	private String pagado;
	private String deuda;
	private String idServicio;
	private String servicio;

	public DetalleDeudaPdf() {
		super();
	}

	public DetalleDeudaPdf(String producto, String fecha, String monto, String comision, String pagado, String deuda,
			String idServicio, String nombreServicio) {
		super();
		this.producto = producto;
		this.fecha = fecha;
		this.monto = monto;
		this.comision = comision;
		this.pagado = pagado;
		this.deuda = deuda;
		this.idServicio = idServicio;
		this.servicio = nombreServicio;
	}

	public String getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(String idServicio) {
		this.idServicio = idServicio;
	}

	public String getServicio() {
		return servicio;
	}

	public void setServicio(String servicio) {
		this.servicio = servicio;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public String getComision() {
		return comision;
	}

	public void setComision(String comision) {
		this.comision = comision;
	}

	public String getPagado() {
		return pagado;
	}

	public void setPagado(String pagado) {
		this.pagado = pagado;
	}

	public String getDeuda() {
		return deuda;
	}

	public void setDeuda(String deuda) {
		this.deuda = deuda;
	}

	@Override
	public String toString() {
		return "DetalleDeudaPdf [producto=" + producto + ", fecha=" + fecha + ", monto=" + monto + ", comision="
				+ comision + ", pagado=" + pagado + ", deuda=" + deuda + "]";
	}

}
