package com.myapps.vista_ccc.bean.ttp;

import com.myapps.CrypAES256;
import com.myapps.detalle_navegacion.util.UtilDate;
import com.myapps.detalle_navegacion.util.UtilFile;
import com.myapps.user.bean.ControlerBitacora;
import com.myapps.user.ldap.DescriptorBitacora;
import com.myapps.vista_ccc.bean.ttp.dao.ParametroDao;
import com.myapps.vista_ccc.bean.ttp.model.TtpPropertieEntity;
import com.myapps.vista_ccc.business.ConsultaBL;
import com.myapps.vista_ccc.business.LogAuditoriaBL;
import com.myapps.vista_ccc.business.LogExcepcionBL;
import com.myapps.vista_ccc.entity.VcConsulta;
import com.myapps.vista_ccc.entity.VcLogAdicional;
import com.myapps.vista_ccc.entity.VcLogAuditoria;
import com.myapps.vista_ccc.entity.VcLogExcepcion;
import com.myapps.vista_ccc.servicio.NodoServicio;
import com.myapps.vista_ccc.servicio.Servicio;
import com.myapps.vista_ccc.servicio.ServicioFacturarDeuda;
import com.myapps.vista_ccc.servicio.ServicioPendingDebtsQuantity;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.SysMessage;
import com.myapps.vista_ccc.util.UtilNumber;
import com.myapps.vista_ccc.util.UtilUrl;
import com.myapps.www.WSFacturarDeuda.FacturarDeudaRequest;
import com.myapps.www.WSFacturarDeuda.GenerarFacturaResponse;
import com.myapps.www.WSFacturarDeuda.WSFacturarDeudaSoapBindingStub;
import com.myapps.www.WSPendingDebtsDetail.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.apache.axis.AxisFault;
import org.apache.log4j.Logger;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.rpc.ServiceException;
import java.io.*;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@ManagedBean
@ViewScoped
public class DeudaBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(DeudaBean.class);

	@Inject
	private LogAuditoriaBL blAuditoria;

	@Inject
	private LogExcepcionBL blExcepcion;

	@Inject
	private ControlerBitacora controlerBitacora;

	@Inject
	private ConsultaBL blConsulta;

	@Inject
	private ParametroDao parametroDao;

	private String login;
	private String ip;
	private long idAuditoriaHistorial;
	private long CodigoAuditoria;

	private String isdn;
	private Integer cantidadRegistros;
	private List<Deuda> listaDeuda;
	private List<Deuda> listaDeudaFiltered;
	private List<Deuda> listaDeudaPostPago;
	private Deuda deudaSeleccionada;
	private String patronFechaDeuda;
	private String patronFechaDetalleDeuda;

	private List<DetalleDeuda> listaDetalleDeuda;
	private List<DetalleDeuda> listaDetalleDeudaFiltered;

	private CNS cns;
	private String msgErrorCrearCNS;

	private List<String> estadosValidosCrearCNS;
	private List<String> estadosValidosDeudaCero;
	// private boolean lineaYaTieneCNS;
	private boolean habilitadoBotonCrearCNS;
	private boolean habilitarDialogMsg;
	private boolean habilitarDialogCns;

	private HashMap<String, String> hashEstadoColores;
	private HashMap<String, String> hashEstadosInternoPantalla;
	private HashMap<String, String> hashEstadosInternoCorte;
	// private HashMap<String, String> hashPropietarioLineas;

	private boolean respuestaSatisfactoriaCrearCns;
	private String mensajeConfirmacionCrearCns;

	private transient StreamedContent file;
	private List<TtpPropertieEntity> parametros;
	private HashMap<String, String> mapaParametros;

	@PostConstruct
	public void init() {
		try {
			inicializando();
		} catch (Exception e) {
			log.error("Error al iniciar, " + e);
		}
	}

	public void inicializando() {
		try {
			log.debug("iniciando deudas ttp");
			mapaParametros = parametroDao.getMapAllParameters();
			Servicio.setPagina(this.getClass().getSimpleName());
			Servicio.eliminarConexionesInactivas(this.getClass().getSimpleName());
			obtenerUsuario();
			cargarIp();

			setListaDeuda(new ArrayList<Deuda>());
			setListaDeudaPostPago(new ArrayList<Deuda>());
			cantidadRegistros = 5;
			// patronFechaDeuda = Parametros.patronFechaDeuda;
			patronFechaDetalleDeuda = Parametros.patronFechaDetalleDeuda;

			setDeudaSeleccionada(null);

			habilitadoBotonCrearCNS = true;
			habilitarDialogMsg = false;
			habilitarDialogCns = false;
			respuestaSatisfactoriaCrearCns = false;
			setMensajeConfirmacionCrearCns(Parametros.deudaTtpMensajeConfirmacionCrearCns);

			cargarListaEstadosValidosCrearCNS();
			cargarHashEstadoColores();


			// cargarHashPropietarioLineas();

		} catch (Exception e) {
			log.error("Error al iniciar, ", e);
		}
	}

	public void buscar(String vista) {

		try {
			cargarHashEstadosInternoPantalla(vista);
			cargarHashEstadosInternoCorte(vista);
			habilitadoBotonCrearCNS = true;
			habilitarDialogCns = false;
			habilitarDialogMsg = false;
			setDeudaSeleccionada(null);
			setListaDeuda(new ArrayList<Deuda>());
			setListaDeudaPostPago(new ArrayList<Deuda>());
			listaDetalleDeuda = new ArrayList<>();

			if (login == null || login.trim().isEmpty()) {
				SysMessage.warn("Usuario no válido", null);
				return;
			}
			if (isdn == null || isdn.trim().isEmpty()) {
				SysMessage.warn("Isdn no válido", null);
				return;
			}
			if (!UtilNumber.esNroTigo(isdn)) {
				SysMessage.warn("Isdn no válido", null);
				return;
			}
			try {

				idAuditoriaHistorial = guardarAuditoria(Calendar.getInstance(), this.login, this.ip,
						Parametros.vistaDeudaTtp, Parametros.comandoDeudaTtp, this.isdn, null);

				if (idAuditoriaHistorial <= 0) {
					SysMessage.error("Error al guardar log de auditoria", null);
					return;
				}

				// disableIncidente=false;

				try {
					controlerBitacora.accion(DescriptorBitacora.DEUDAS_TTP,
							"Se realizó la busqueda de deudas TTP de la linea: " + isdn + ", cantidad de registros: "
									+ cantidadRegistros);
				} catch (Exception e) {
					log.error("Error al guardar bitacora en el sistema: ", e);
					SysMessage.error("Error al guardar bitacora", null);
				}

				obtenerDeudas();

			} catch (AxisFault a) {
				String msg = a.getMessage();
				msg = msg.toLowerCase();
				if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase())
						|| msg.contains("Bad Request".toLowerCase())
						|| msg.contains("Connection timed out".toLowerCase())
						|| msg.contains("UnknownHostException".toLowerCase())
						|| msg.contains("No route to host".toLowerCase())
						|| msg.contains("Connection refused".toLowerCase())) {
					log.error("Error de conexion al servicio PendingDebtsQuantity: ", a);
					SysMessage.error("Error de conexion del servicio PendingDebtsQuantity", null);
					guardarExcepcion(idAuditoriaHistorial,
							"Error de conexion al servicio PendingDebtsQuantity: " + stackTraceToString(a));
				} else {
					log.error("Error AxisFault PendingDebtsQuantity: ", a);
					SysMessage.error("Error AxisFault de conexion al servicio PendingDebtsQuantity", null);
					guardarExcepcion(idAuditoriaHistorial,
							"Error AxisFault de conexion al servicio PendingDebtsQuantity: " + stackTraceToString(a));
				}
			} catch (ServiceException e) {
				log.error("Error de servicio al conectarse al servicio PendingDebtsQuantity: ", e);
				SysMessage.error("Error de servicio al conectarse al servicio PendingDebtsQuantity", null);
				guardarExcepcion(idAuditoriaHistorial,
						"Error de servicio al conectarse al servicio PendingDebtsQuantity: " + stackTraceToString(e));
			} catch (RemoteException e) {
				log.error("Error remoto al conectarse al servicio PendingDebtsQuantity: ", e);
				SysMessage.error("Error remoto al conectarse al servicio PendingDebtsQuantity", null);
				guardarExcepcion(idAuditoriaHistorial,
						"Error remoto al conectarse al servicio PendingDebtsQuantity: " + stackTraceToString(e));
			} catch (Exception e) {
				log.error("Error al consultar PendingDebtsQuantity", e);
				SysMessage.error("Error al consultar PendingDebtsQuantity", null);
				guardarExcepcion(idAuditoriaHistorial,
						"Error al consultar PendingDebtsQuantity:" + stackTraceToString(e));
			}

			SysMessage.info("Consulta finalizada.", null);

		} catch (Exception e) {
			log.error("Error al realizar busqueda: ", e);
			SysMessage.error("Error al realizar busqueda: " + e.getMessage(), null);
			guardarExcepcion(idAuditoriaHistorial, "Error al realizar busqueda:" + stackTraceToString(e));
		}

	}

	public void aceptarDialogMensaje() {
		log.debug("aceptarDialogMensaje");
		habilitarDialogMsg = false;
		habilitarDialogCns = false;
	}

	public void crearCNS() {
		habilitarDialogCns = false;
		habilitarDialogMsg = false;
		respuestaSatisfactoriaCrearCns = false;
		log.debug("Crear CNS");
		try {

			if (login == null || login.trim().isEmpty()) {
				SysMessage.warn("Usuario no válido", null);
				return;
			}
			if (isdn == null || isdn.trim().isEmpty()) {
				SysMessage.warn("Isdn no válido", null);
				return;
			}

			try {
				if (deudaSeleccionada != null) {
					if (estadosValidosCrearCNS.contains(deudaSeleccionada.getEstado())) { // consul // al

						// servicio
						// if (deudaSeleccionada.getNumero() == null &&
						// deudaSeleccionada.getNumero().isEmpty()) {

						msgErrorCrearCNS = "";
						// cns = new CNS("CNS", "3C", "4021");
						cns = new CNS();
						generarFactura();
						if (respuestaSatisfactoriaCrearCns) {
							habilitarDialogCns = true;
						} else {
							deudaSeleccionada = null;
						}
						try {
							controlerBitacora.accion(DescriptorBitacora.DEUDAS_TTP,
									"Se creó un CNS para la linea: " + isdn);
						} catch (Exception e) {
							log.error("Error al guardar bitacora en el sistema: ", e);
							SysMessage.error("Error al guardar bitacora", null);
						}

						habilitadoBotonCrearCNS = true;
						// deudaSeleccionada = null;
						// } else {
						// msgErrorCrearCNS = Parametros.mensajeLineaConCNS;
						// habilitarDialogMsg = true;
						// }
					} else { // muestra mensaje parametrizable de error
						msgErrorCrearCNS = Parametros.mensajeLineaErrorEstadoNoValido;
						habilitarDialogMsg = true;
					}
				}
			} catch (AxisFault a) {
				String msg = a.getMessage();
				msg = msg.toLowerCase();
				if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase())
						|| msg.contains("Bad Request".toLowerCase())
						|| msg.contains("Connection timed out".toLowerCase())
						|| msg.contains("UnknownHostException".toLowerCase())
						|| msg.contains("No route to host".toLowerCase())
						|| msg.contains("Connection refused".toLowerCase())) {
					log.error("Error de conexion al servicio FacturarDeuda: ", a);
					SysMessage.error("Error de conexion del servicio FacturarDeuda", null);
					guardarExcepcion(idAuditoriaHistorial,
							"Error de conexion al servicio FacturarDeuda: " + stackTraceToString(a));
				} else {
					log.error("Error AxisFault FacturarDeuda: ", a);
					SysMessage.error("Error AxisFault de conexion al servicio FacturarDeuda", null);
					guardarExcepcion(idAuditoriaHistorial,
							"Error AxisFault de conexion al servicio FacturarDeuda: " + stackTraceToString(a));
				}
			} catch (ServiceException e) {
				log.error("Error de servicio al conectarse al servicio FacturarDeuda: ", e);
				SysMessage.error("Error de servicio al conectarse al servicio FacturarDeuda", null);
				guardarExcepcion(idAuditoriaHistorial,
						"Error de servicio al conectarse al servicio FacturarDeuda: " + stackTraceToString(e));
			} catch (RemoteException e) {
				log.error("Error remoto al conectarse al servicio FacturarDeuda: ", e);
				SysMessage.error("Error remoto al conectarse al servicio FacturarDeuda", null);
				guardarExcepcion(idAuditoriaHistorial,
						"Error remoto al conectarse al servicio FacturarDeuda: " + stackTraceToString(e));
			} catch (Exception e) {
				log.error("Error al consultar FacturarDeuda", e);
				SysMessage.error("Error al consultar FacturarDeuda", null);
				guardarExcepcion(idAuditoriaHistorial, "Error al consultar FacturarDeuda:" + stackTraceToString(e));
			}

			SysMessage.info("Consulta finalizada.", null);

		} catch (Exception e) {
			log.error("Error al crear CNS: ", e);
			SysMessage.error("Error al crear CNS: " + e.getMessage(), null);
			guardarExcepcion(idAuditoriaHistorial, "Error al crear CNS:" + stackTraceToString(e));
		}
	}

	public void aceptarCrearCNS() {
		log.debug("Ingresando a obtener nuevamente la lista de deudas");
		try {
			habilitarDialogCns = false;
			habilitarDialogMsg = false;
			listaDeuda = new ArrayList<>();
			listaDetalleDeuda = new ArrayList<>();
			habilitadoBotonCrearCNS = true;
			deudaSeleccionada = null;

			obtenerDeudas();

		} catch (AxisFault a) {
			String msg = a.getMessage();
			msg = msg.toLowerCase();
			if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase())
					|| msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase())
					|| msg.contains("UnknownHostException".toLowerCase())
					|| msg.contains("No route to host".toLowerCase())
					|| msg.contains("Connection refused".toLowerCase())) {
				log.error("Error de conexion al servicio PendingDebtsQuantity: ", a);
				SysMessage.error("Error de conexion del servicio PendingDebtsQuantity", null);
				guardarExcepcion(idAuditoriaHistorial,
						"Error de conexion al servicio PendingDebtsQuantity: " + stackTraceToString(a));
			} else {
				log.error("Error AxisFault PendingDebtsQuantity: ", a);
				SysMessage.error("Error AxisFault de conexion al servicio PendingDebtsQuantity", null);
				guardarExcepcion(idAuditoriaHistorial,
						"Error AxisFault de conexion al servicio PendingDebtsQuantity: " + stackTraceToString(a));
			}
		} catch (ServiceException e) {
			log.error("Error de servicio al conectarse al servicio PendingDebtsQuantity: ", e);
			SysMessage.error("Error de servicio al conectarse al servicio PendingDebtsQuantity", null);
			guardarExcepcion(idAuditoriaHistorial,
					"Error de servicio al conectarse al servicio PendingDebtsQuantity: " + stackTraceToString(e));
		} catch (RemoteException e) {
			log.error("Error remoto al conectarse al servicio PendingDebtsQuantity: ", e);
			SysMessage.error("Error remoto al conectarse al servicio PendingDebtsQuantity", null);
			guardarExcepcion(idAuditoriaHistorial,
					"Error remoto al conectarse al servicio PendingDebtsQuantity: " + stackTraceToString(e));
		} catch (Exception e) {
			log.error("Error al consultar PendingDebtsQuantity", e);
			SysMessage.error("Error al consultar PendingDebtsQuantity", null);
			guardarExcepcion(idAuditoriaHistorial, "Error al consultar PendingDebtsQuantity:" + stackTraceToString(e));
		}

		SysMessage.info("Consulta finalizada.", null);
	}

	public void aceptarDetalleDeuda() {

	}

	public String formatDateToString(Date date) {
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String fechaString = format.format(date);
		return fechaString;
	}

	public void exportar(String tipoReporte, String tipoLinea) {
		log.debug("Ingresando a exportar tipoReporte: " + tipoReporte);
		String formato = "pdf";
		try {
			boolean tieneCns = false;
			boolean tieneDetalle = false;
			boolean centrar = false;
			obtenerUsuario();

			// if (deudaSeleccionada.getTipo() != null &&
			// !deudaSeleccionada.getTipo().trim().isEmpty() &&
			// deudaSeleccionada.getListaDetalle() != null &&
			// !deudaSeleccionada.getListaDetalle().isEmpty()) {

			// Preparing parameters
			log.info("Usuario y fecha " + this.login + " " + new Date().toString());
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("titulo", Parametros.reporteTitulo);
			params.put("cnsTipo", deudaSeleccionada.getTipo());
			params.put("cnsSerie", deudaSeleccionada.getSerie());
			params.put("cnsNumero", deudaSeleccionada.getNumero());
			params.put("usuario", this.login);
			params.put("fechaDeGeneracion", this.formatDateToString(new Date()));
			// params.put("total",
			// UtilNumber.doubleToString(deudaSeleccionada.getTotalDetalleDeuda(),
			// ","));
			params.put("total",
					UtilNumber.doubleToString(deudaSeleccionada.getTotalDetalleDeuda(), Parametros.deudaTtpNroDecimales,
							Parametros.deudaTtpLocaleLenguage, Parametros.deudaTtpLocaleCountry));
			params.put("habilitadoTabla", "true");
			params.put("linea", isdn);
			params.put("estado", deudaSeleccionada.getEstadoPantalla());
			params.put("corte", deudaSeleccionada.getCorte());

			if (tipoReporte.equals("DETALLE_DEUDA_CNS")) {
				params.put("habilitado", "true");
				params.put("cnsTipo", cns.getTipo());
				params.put("cnsSerie", cns.getSerie());
				params.put("cnsNumero", cns.getNumero());
				params.put("habilitadoCuenta", "true");
				tieneCns = true;
			} else if (tipoReporte.equals("DETALLE_DEUDA")) {
				params.put("habilitado", "false");
				params.put("habilitadoCuenta", "false");
			} else {
				if (deudaSeleccionada.getTipo() != null && !deudaSeleccionada.getTipo().trim().isEmpty()) {
					params.put("habilitado", "true");
					tieneCns = true;
				} else {
					params.put("habilitado", "false");
					centrar = true;
				}
				params.put("habilitadoCuenta", "true");
			}

			List<DetalleDeudaPdf> listDetalleDeudaPdf = new ArrayList<>();
			if (deudaSeleccionada.getListaDetalle() != null && !deudaSeleccionada.getListaDetalle().isEmpty()) {
				tieneDetalle = true;
				List<DetalleDeuda> listaDetalle = deudaSeleccionada.getListaDetalle();
				for (DetalleDeuda item : listaDetalle) {
					DetalleDeudaPdf ddp = new DetalleDeudaPdf();
					log.info("Servicio ");
					ddp.setProducto(item.getProducto());
					ddp.setServicio(item.getNombreService());
					ddp.setIdServicio(String.valueOf(item.getIdServicio()));
					ddp.setFecha(UtilDate.dateToString(item.getFecha(), Parametros.patronFechaDetalleDeuda));
					ddp.setMonto(UtilNumber.doubleToString(item.getMonto(), Parametros.deudaTtpNroDecimales,
							Parametros.deudaTtpLocaleLenguage, Parametros.deudaTtpLocaleCountry));
					ddp.setComision(UtilNumber.doubleToString(item.getComision(), Parametros.deudaTtpNroDecimales,
							Parametros.deudaTtpLocaleLenguage, Parametros.deudaTtpLocaleCountry));
					ddp.setPagado(UtilNumber.doubleToString(item.getPagado(), Parametros.deudaTtpNroDecimales,
							Parametros.deudaTtpLocaleLenguage, Parametros.deudaTtpLocaleCountry));
					ddp.setDeuda(UtilNumber.doubleToString(item.getDeuda(), Parametros.deudaTtpNroDecimales,
							Parametros.deudaTtpLocaleLenguage, Parametros.deudaTtpLocaleCountry));
					listDetalleDeudaPdf.add(ddp);
				}
			} else {
				params.put("habilitadoTabla", "false");
				DetalleDeudaPdf ddp = new DetalleDeudaPdf();
				ddp.setProducto("");
				ddp.setFecha("");
				ddp.setMonto("");
				ddp.setComision("");
				ddp.setPagado("");
				ddp.setDeuda("");
				listDetalleDeudaPdf.add(ddp);
			}

			log.debug("tieneCns: " + tieneCns);
			log.debug("tieneDetalle: " + tieneDetalle);

			if (!tieneCns && !tieneDetalle) {
				log.warn("[isdn: " + isdn + "] No se tiene información suficiente para generar reporte");
				SysMessage.info("No se tiene información suficiente para generar reporte", null);
				return;
			}

			try {
				controlerBitacora.accion(DescriptorBitacora.DEUDAS_TTP,
						"Se generó reporte de deudas para la linea: " + isdn);
			} catch (Exception e) {
				log.error("Error al guardar bitacora en el sistema: ", e);
				SysMessage.error("Error al guardar bitacora", null);
			}
			// --------------- EXPORT PDF - EXCEL ------------------
			FacesContext faces = FacesContext.getCurrentInstance();
			ServletContext context = (ServletContext) faces.getExternalContext().getContext();
			String pathFile = "";
			if(tipoLinea.equals("prepago")){
				pathFile = context.getRealPath("/WEB-INF/template/detalle_deuda.jrxml");
			}else{
				pathFile = context.getRealPath("/WEB-INF/template/detalle_deudaPostpago.jrxml");
			}
			String logo = context.getRealPath("/WEB-INF/template/logo_tigo.png");
			params.put("logo", logo);

			// String pathFile = "C:/Users/Jose
			// Luis/Desktop/configuracion/pie/report.jrxml";
			// -- custom
			String source = UtilFile.fileToString(pathFile);
			// log.debug(source);
			InputStream inputReport = new ByteArrayInputStream(source.getBytes("UTF-8"));
			JasperDesign jasperDesign = JRXmlLoader.load(inputReport);
			// JasperDesign jasperDesign = JRXmlLoader.load(new File(pathFile));

			if (centrar) {
				jasperDesign.getTitle().getElementByKey("frameCuenta").setX(150);
			}
			// JasperDesign jasperDesign = JRXmlLoader.load(pathFile);
			JasperReport reporte = JasperCompileManager.compileReport(jasperDesign);

			JasperPrint jasperPrint;
			jasperPrint = JasperFillManager.fillReport(reporte, params,
					new JRBeanCollectionDataSource(listDetalleDeudaPdf));

			// PDF
			// JasperExportManager.exportReportToPdfFile(jasperPrint,
			// "E:/simple_report.pdf");
			// exporting to excel file
			// String name = "report_excel" + (new Date()).getTime() + ".xls";
			// String path = "E:/" + name;// context.getRealPath("/WEB-INF/" +
			// name);
			// File destFile = new File(path);
			// boolean fvar = destFile.createNewFile();
			// if (fvar) {
			// JRXlsxExporter exportador = new JRXlsxExporter();
			// exportador.setParameter(JRExporterParameter.JASPER_PRINT,
			// jasperPrint);
			// exportador.setParameter(JRExporterParameter.OUTPUT_FILE,
			// destFile);
			// exportador.exportReport();
			// }

			String contextPath = context.getRealPath(File.separator);
			// log.debug("contextPath: " + contextPath);
			String name = "deuda_" + (new Date()).getTime() + (formato.equals("pdf") ? ".pdf" : ".xlsx");
			// log.debug("name: " + name);

			if (formato.equals("pdf")) {
				JasperExportManager.exportReportToPdfFile(jasperPrint, contextPath + name);
			} else {
				JRXlsxExporter exportador = new JRXlsxExporter();
				// exportador.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET,
				// Boolean.TRUE);
				// este parametro es para decirle que todo el reporte se genera
				// en
				// una sola hoja de excel
				// exportador.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET,
				// false);
				// con este le decimos que detecte el tipo de celda.
				// exportador.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE,
				// true);
				// El siguiente es para decirle que no ignore los bordes de las
				// celdas, y el ultimo es para que no use un fondo blanco, con
				// estos
				// dos parametros jasperreport genera la hoja de excel con los
				// bordes de las celdas.
				// exportador.setParameter(JRXlsExporterParameter.IS_IGNORE_CELL_BORDER,
				// false);
				// exportador.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND,
				// false);
				exportador.setParameter(net.sf.jasperreports.engine.JRExporterParameter.JASPER_PRINT, jasperPrint);
				exportador.setParameter(net.sf.jasperreports.engine.JRExporterParameter.OUTPUT_FILE,
						new File(contextPath + name));
				exportador.exportReport();
			}

			byte[] bytesFile = getBytesFromFile(new File(contextPath + name));
			// HttpServletResponse hsr = (HttpServletResponse)
			// faces.getExternalContext().getResponse();
			// hsr.setContentType("application/octet-stream");
			// // hsr.setContentType("application/pdf");
			// hsr.setContentLength(bytesFile.length);
			//
			// hsr.setHeader("Content-disposition", "attachment; filename=\"" +
			// name + "\"");
			// ServletOutputStream out;
			// out = hsr.getOutputStream();
			// out.write(bytesFile);
			// out.close();

			InputStream stream = new ByteArrayInputStream(bytesFile);
			file = new DefaultStreamedContent(stream, "application/octet-stream", name);

			try {
				File f = new File(contextPath + name);
				boolean delete = f.delete();
				log.debug("[isdn: " + isdn + "] Resultado de eliminar archivo " + contextPath + name + ": " + delete);
			} catch (Exception e) {
				log.warn("Error al eliminar archivo: ", e);
			}
			faces.responseComplete();

			log.debug("Exportador finalizado.....");
		} catch (IOException e) {
			log.error("Error al exportar pdf: ", e);

			guardarExcepcion(idAuditoriaHistorial, "Error al exportar pdf: " + e.getMessage());
			SysMessage.error("Error al exportar pdf: " + e.getLocalizedMessage(), null);
		} catch (JRException e) {
			guardarExcepcion(idAuditoriaHistorial, "Error al exportar pdf: " + e.getMessage());
			SysMessage.error("Error al exportar pdf: " + e.getLocalizedMessage(), null);
		}
	}

	private void obtenerDeudas() throws MalformedURLException, Exception {
		String wsdlLoc = Parametros.gestorDeudasWsdl;

		long ini = System.currentTimeMillis();

		// URL url = new URL(UtilUrl.getIp(wsdlLoc));
		// if (url.getProtocol().equalsIgnoreCase("https")) {
		// int puerto = url.getPort();
		// String host = url.getHost();
		// if (host.equalsIgnoreCase("localhost")) {
		// host = "127.0.0.1";
		// }
		// validarCertificado(Parametros.gestorDeudasPathKeystore, host + ":" +
		// puerto);
		// }
		NodoServicio<WSPendingDebtsDetailSoapBindingStub> portNodo;
		log.info("Gerenrando Port para consumir servicio Deuda Detallada TTP Opcion: {"
				+ Parametros.opcionConsumoServicioDeudaDetallada + "}");
		if (Parametros.opcionConsumoServicioDeudaDetallada == 1) {
			log.info("Consumir de servicio Deuda Detallada TTP Opcion: {"
					+ Parametros.opcionConsumoServicioDeudaDetallada + "} SIN CREDENDIALES");
			portNodo = ServicioPendingDebtsQuantity.initPort(wsdlLoc, "GestorDeudas" + login,
					Parametros.gestorDeudasTimeOut, "", "");
		} else {
			log.info("Consumir de servicio Deuda Detallada TTP Opcion: {"
					+ Parametros.opcionConsumoServicioDeudaDetallada + "} CON CREDENDIALES");
			log.info("Usuario encriptado: " + Parametros.userServiceDDTTP + ", contraseña encriptada: "
					+ Parametros.passServiceDDTTP);
			CrypAES256 cryp = new CrypAES256();
			log.info("cryp creado");
			String userDeCryp = cryp.decrypt(Parametros.userServiceDDTTP, "credencialesTTP");
			String passDeCryp = cryp.decrypt(Parametros.passServiceDDTTP, "credencialesTTP");
			log.info("Usuario desencriptado: ****, password desencriptado: ****");
			portNodo = ServicioPendingDebtsQuantity.initPort(wsdlLoc, "GestorDeudas" + login,
					Parametros.gestorDeudasTimeOut, userDeCryp, passDeCryp);
		}

		if (portNodo != null && portNodo.getPort() != null) {

			synchronized (portNodo) {

				try {

					WSPendingDebtsDetailSoapBindingStub port = portNodo.getPort();
					String consumerID = "";
					String transactionID = "";
					String country = null;
					String correlationID = "";
					GeneralConsumerInformation generalConsumerInformation = new GeneralConsumerInformation(consumerID,
							transactionID, country, correlationID);
					com.myapps.www.WSPendingDebtsDetail.RequestHeader requestHeader = new com.myapps.www.WSPendingDebtsDetail.RequestHeader(
							generalConsumerInformation);

					String msisdn = isdn;
					String processCode = "";
					AdditionalParameters additionalParameters = null;
					com.myapps.www.WSPendingDebtsDetail.RequestBody requestBody = new com.myapps.www.WSPendingDebtsDetail.RequestBody(
							msisdn, processCode, additionalParameters);

					GetPendingDebtsDetailRequest getPendingDebtsQuantityRequest = new GetPendingDebtsDetailRequest(
							requestHeader, requestBody);
					GetPendingDetailResponseType response = (GetPendingDetailResponseType) port
							.getPendingDebtsDetail(getPendingDebtsQuantityRequest);

					long fin = System.currentTimeMillis();
					Long Con = null;
					if (portNodo.isPrimeraVez()) {
						Con = portNodo.getTiempoConexion();
					}
					log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio PendingDebtsQuantity: "
							+ (fin - ini) + " milisegundos");

					if (Parametros.saveRequestResponse) {
						try {
							saveXml(isdn, login, wsdlLoc, "getPendingDebtsQuantity",
									port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(),
									port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoriaHistorial,
									Con, (fin - ini));
						} catch (Exception e) {
							log.warn(
									"No se logro registrar los request y response del servicio PendingDebtsQuantity, getPendingDebtsQuantity:",
									e);
						}
					}

					if (response != null) {

						DebtsType[] debtsTypes = response.getResponseBody();
						if (debtsTypes != null && debtsTypes.length > 0) {
							for (DebtsType item : debtsTypes) {
								Deuda deudaPostPago = new Deuda();
								Deuda deuda = new Deuda();
								if(item.getEstadoDeuda().equals("PENDIENTE_CARGO") || item.getEstadoDeuda().equals("PAGADO_CARGO")
										|| item.getEstadoDeuda().equals("GENERADO_CARGO") || item.getEstadoDeuda().equals("GENERADO_PARCIAL")){
									deudaPostPago.setId(item.getIdDebt() + "");
									deudaPostPago.setColor(hashEstadoColores.get(item.getEstadoDeuda()));

									// if (item.getNumber() != null) {
									// deuda.setPropietarioLinea(hashPropietarioLineas.get(item.getNumber().getRoutingNumberHolder()
									// + ""));
									// deuda.setOrigen(hashPropietarioLineas.get(item.getNumber().getRoutingNumberBegin()
									// + ""));
									// deuda.setDestino(hashPropietarioLineas.get(item.getNumber().getRoutingNumberEnd()
									// + ""));
									// } else {
									// log.warn("[isdn: " + getIsdn() + "] Number
									// resuelto a nulo");
									// }

									deudaPostPago.setEstado(item.getEstadoDeuda());
									deudaPostPago.setFechaPortatacion(item.getFechaPortacion().getTime());
									deudaPostPago.setFechaEstado(item.getFechaEstado().getTime());
									if (item.getFechaVencimiento() != null)
										deudaPostPago.setFechaVencimiento(item.getFechaVencimiento().getTime());
									deudaPostPago.setCorte(item.getEstadoDeuda());

									if (item.getEstadoDeuda() != null) {
										String pantalla = hashEstadosInternoPantalla.get(item.getEstadoDeuda().trim());
										if (pantalla != null) {
											deudaPostPago.setEstadoPantalla(pantalla);
										}
										String corte = hashEstadosInternoCorte.get(item.getEstadoDeuda().trim());
										if (corte != null) {
											deudaPostPago.setCorte(corte);
										}
									} else {
										log.warn("[isdn: " + getIsdn() + "] Estado de la deuda resuelto a nulo");
									}

									// if (item.getFechaDeuda() != null) {
									// deuda.setFecha(item.getFechaDeuda().getTime());
									// }
									// deuda.setBloqueo(item.isBloqueado() ? "SI" :
									// "NO");

									if (item.getCns() != null) {
										deudaPostPago.setTipo(item.getCns().getTipoCns());
										deudaPostPago.setSerie(item.getCns().getSerie());
										deudaPostPago.setNumero(item.getCns().getNumero());
									} else {
										log.warn("[isdn: " + getIsdn() + "] CNS resuelto a nulo");
									}

									DetalleDeudasType[] detalleDeudas = item.getDetalleDeudas();
									List<DetalleDeuda> listaDetalle = new ArrayList<>();
//								double totalDetalleDeuda = 0;

									if (detalleDeudas != null && detalleDeudas.length > 0) {
										for (DetalleDeudasType itemDeuda : detalleDeudas) {
											DetalleDeuda dd = new DetalleDeuda();
											dd.setSortBy("1");
											dd.setProducto(itemDeuda.getSelectText());

											if (itemDeuda.getPurchaseDate() != null) {
												dd.setFecha(itemDeuda.getPurchaseDate().getTime());
											}
											dd.setMonto(itemDeuda.getLoanAmount());
											dd.setComision(itemDeuda.getLoanComission());
											dd.setPagado(itemDeuda.getCharges());
											dd.setIdServicio(itemDeuda.getServiceId());
											dd.setNombreService(itemDeuda.getServiceName());
											dd.setDeuda(Math.round(itemDeuda.getDebtRemainder() * Math.pow(10, 2))
													/ Math.pow(10, 2));
//										totalDetalleDeuda = totalDetalleDeuda + dd.getDeuda();

											listaDetalle.add(dd);
										}
									}
									if (validarEstado(Parametros.estadoValidoDeudaCero, item.getEstadoDeuda()) >= 0) {
										log.debug("Deuda resuelta a 0");
										deudaPostPago.setDeuda(0);
									}

									else {
										log.debug("Deuda recuperada: " + item.getMontoDeuda());
										deudaPostPago.setDeuda(
												Math.round(item.getMontoDeuda() * Math.pow(10, 2)) / Math.pow(10, 2));
									}
//								deuda.setDeuda(item.getMontoDeuda());
									deudaPostPago.setListaDetalle(listaDetalle);
									deudaPostPago.setTotalDetalleDeuda(deudaPostPago.getDeuda());
									log.debug("Deuda Cargada, Id Deuda: " + deudaPostPago.getId() + ", Msisdn: "
											+ item.getNumber().getMsisdn() + ", Monto Dedua: " + deudaPostPago.getDeuda());
									listaDeudaPostPago.add(deudaPostPago);
								}else {
								deuda.setId(item.getIdDebt() + "");
								deuda.setColor(hashEstadoColores.get(item.getEstadoDeuda()));

								// if (item.getNumber() != null) {
								// deuda.setPropietarioLinea(hashPropietarioLineas.get(item.getNumber().getRoutingNumberHolder()
								// + ""));
								// deuda.setOrigen(hashPropietarioLineas.get(item.getNumber().getRoutingNumberBegin()
								// + ""));
								// deuda.setDestino(hashPropietarioLineas.get(item.getNumber().getRoutingNumberEnd()
								// + ""));
								// } else {
								// log.warn("[isdn: " + getIsdn() + "] Number
								// resuelto a nulo");
								// }

								deuda.setEstado(item.getEstadoDeuda());
								deuda.setFechaPortatacion(item.getFechaPortacion().getTime());
								deuda.setFechaEstado(item.getFechaEstado().getTime());
								if (item.getFechaVencimiento() != null)
									deuda.setFechaVencimiento(item.getFechaVencimiento().getTime());
								deuda.setCorte(item.getEstadoDeuda());

								if (item.getEstadoDeuda() != null) {
									String pantalla = hashEstadosInternoPantalla.get(item.getEstadoDeuda().trim());
									if (pantalla != null) {
										deuda.setEstadoPantalla(pantalla);
									}
									String corte = hashEstadosInternoCorte.get(item.getEstadoDeuda().trim());
									if (corte != null) {
										deuda.setCorte(corte);
									}
								} else {
									log.warn("[isdn: " + getIsdn() + "] Estado de la deuda resuelto a nulo");
								}

								// if (item.getFechaDeuda() != null) {
								// deuda.setFecha(item.getFechaDeuda().getTime());
								// }
								// deuda.setBloqueo(item.isBloqueado() ? "SI" :
								// "NO");

								if (item.getCns() != null) {
									deuda.setTipo(item.getCns().getTipoCns());
									deuda.setSerie(item.getCns().getSerie());
									deuda.setNumero(item.getCns().getNumero());
								} else {
									log.warn("[isdn: " + getIsdn() + "] CNS resuelto a nulo");
								}

								DetalleDeudasType[] detalleDeudas = item.getDetalleDeudas();
								List<DetalleDeuda> listaDetalle = new ArrayList<>();
//								double totalDetalleDeuda = 0;

								if (detalleDeudas != null && detalleDeudas.length > 0) {
									for (DetalleDeudasType itemDeuda : detalleDeudas) {
										DetalleDeuda dd = new DetalleDeuda();
										dd.setSortBy("1");
										dd.setProducto(itemDeuda.getSelectText());

										if (itemDeuda.getPurchaseDate() != null) {
											dd.setFecha(itemDeuda.getPurchaseDate().getTime());
										}
										dd.setMonto(itemDeuda.getLoanAmount());
										dd.setComision(itemDeuda.getLoanComission());
										dd.setPagado(itemDeuda.getCharges());
										dd.setIdServicio(itemDeuda.getServiceId());
										dd.setNombreService(itemDeuda.getServiceName());
										dd.setDeuda(Math.round(itemDeuda.getDebtRemainder() * Math.pow(10, 2))
												/ Math.pow(10, 2));
//										totalDetalleDeuda = totalDetalleDeuda + dd.getDeuda();

										listaDetalle.add(dd);
									}
								}
								if (validarEstado(Parametros.estadoValidoDeudaCero, item.getEstadoDeuda()) >= 0) {
									log.debug("Deuda resuelta a 0");
									deuda.setDeuda(0);
								}

								else {
									log.debug("Deuda recuperada: " + item.getMontoDeuda());
									deuda.setDeuda(
											Math.round(item.getMontoDeuda() * Math.pow(10, 2)) / Math.pow(10, 2));
								}
//								deuda.setDeuda(item.getMontoDeuda());
								deuda.setListaDetalle(listaDetalle);
								deuda.setTotalDetalleDeuda(deuda.getDeuda());
								log.debug("Deuda Cargada, Id Deuda: " + deuda.getId() + ", Msisdn: "
										+ item.getNumber().getMsisdn() + ", Monto Dedua: " + deuda.getDeuda());
								listaDeuda.add(deuda);
								}
							}
						} else {
							log.warn("[isdn: " + getIsdn() + "] DebtsType[] resuelto a nulo");
						}
					} else {
						log.warn("[isdn: " + getIsdn() + "] GetPendingDebtsQuantityResponseType resuelto a nulo");
					}

					habilitarDialogMsg = false;

					if (response.getResponseHeader() != null) {
						if (response.getResponseHeader().getServiceCode() != null) {
							log.debug("[isdn: " + getIsdn() + "] Service code: "
									+ response.getResponseHeader().getServiceCode() + ", Localized message: "
									+ response.getResponseHeader().getLocalizedMessage());
							if (response.getResponseHeader().getServiceCode().equals("20")) {
								habilitarDialogMsg = true;
								msgErrorCrearCNS = Parametros.mensajeLineaNoPortada;
							} else if (response.getResponseHeader().getServiceCode().equals("21")) {
								habilitarDialogMsg = true;
								msgErrorCrearCNS = Parametros.mensajeLineaSinDeuda;
							} else if (response.getResponseHeader().getServiceCode().equals("22")) {
								habilitarDialogMsg = true;
								msgErrorCrearCNS = Parametros.mensajeLineaNoEncontrada;
							} else if (response.getResponseHeader().getServiceCode().equals("00")) {
								// exitoso
							} else {
								msgErrorCrearCNS = "Service code: " + response.getResponseHeader().getServiceCode()
										+ ", Localized message: " + response.getResponseHeader().getLocalizedMessage();
								SysMessage.warn(msgErrorCrearCNS, null);
							}
						} else {
							log.warn("[isdn: " + getIsdn() + "] ResponseHeader.ServiceCode resuelto a nulo");
						}
					} else {
						log.warn("[isdn: " + getIsdn() + "] ResponseHeader resuelto a nulo");
					}
				} finally {
					portNodo.setEnUso(false);
					portNodo.setFechaFin(new Date());
				}
			}

		} else {
			log.error("[isdn: " + getIsdn() + "] Port PendingDebtsQuantity resuelto a nulo");
		}
	}

	private void generarFactura() throws MalformedURLException, Exception {
		String wsdlLoc = Parametros.facturarDeudaWsdl;

		long ini = System.currentTimeMillis();

		// URL url = new URL(UtilUrl.getIp(wsdlLoc));
		// if (url.getProtocol().equalsIgnoreCase("https")) {
		// int puerto = url.getPort();
		// String host = url.getHost();
		// if (host.equalsIgnoreCase("localhost")) {
		// host = "127.0.0.1";
		// }
		// validarCertificado(Parametros.gestorDeudasPathKeystore, host + ":" +
		// puerto);
		// }

		NodoServicio<WSFacturarDeudaSoapBindingStub> portNodo;

		log.info("Gerenrando Port para consumir servicio Deuda Detallada TTP Opcion: {"
				+ Parametros.opcionConsumoServicioFacturarDeuda + "}");
		if (Parametros.opcionConsumoServicioFacturarDeuda == 1) {
			log.info("Consumir de servicio Facturar Deuda TTP Opcion: {"
					+ Parametros.opcionConsumoServicioDeudaDetallada + "} SIN CREDENDIALES");
			portNodo = ServicioFacturarDeuda.initPort(wsdlLoc, "FACTURAR_DEUDA" + login,
					Parametros.facturarDeudaTimeOut, "", "");
		} else {
			log.info("Consumir de servicio Facturar Deuda TTP Opcion: {" + Parametros.opcionConsumoServicioFacturarDeuda
					+ "} CON CREDENDIALES");
			log.info("Usuario encriptado: " + Parametros.userServiceFaturarTTP + ", contraseña encriptada: "
					+ Parametros.passServiceFacturarTTP);
			CrypAES256 cryp = new CrypAES256();
			log.info("cryp creado");
			String userDeCryp2 = cryp.decrypt(Parametros.userServiceFaturarTTP, "userDDTTP");
			String passDeCryp2 = cryp.decrypt(Parametros.passServiceFacturarTTP, "passDDTTP");
			log.info("Usuario desencriptado: ****, password desencriptado: ****");
			portNodo = ServicioFacturarDeuda.initPort(wsdlLoc, "FACTURAR_DEUDA" + login,
					Parametros.facturarDeudaTimeOut, userDeCryp2, passDeCryp2);
		}

		if (portNodo != null && portNodo.getPort() != null) {

			synchronized (portNodo) {
				try {

					WSFacturarDeudaSoapBindingStub port = portNodo.getPort();

					com.myapps.www.WSFacturarDeuda.RequestHeader requestHeader = new com.myapps.www.WSFacturarDeuda.RequestHeader();
					int idDeuda = Integer.parseInt(deudaSeleccionada.getId());
					int msisdn = Integer.parseInt(isdn);
					double amountDebt = Math.round(deudaSeleccionada.getDeuda() * Math.pow(10, 2)) / Math.pow(10, 2);

					com.myapps.www.WSFacturarDeuda.RequestBody requestBody = new com.myapps.www.WSFacturarDeuda.RequestBody(
							idDeuda, msisdn, amountDebt);
					log.debug("Deuda seleccionada para generar CNS -> Id Deuda: " + idDeuda + ", Msisdn: " + msisdn
							+ ", Monto Deuda: " + amountDebt);
					FacturarDeudaRequest facturarDeudaRequest = new FacturarDeudaRequest(requestHeader, requestBody);
					GenerarFacturaResponse response = port.facturarDeuda(facturarDeudaRequest);

					long fin = System.currentTimeMillis();
					Long Con = null;
					if (portNodo.isPrimeraVez()) {
						Con = portNodo.getTiempoConexion();
					}
					log.debug("Tiempo de respuesta del servicio FacturarDeuda, facturarDeuda: " + (fin - ini)
							+ " milisegundos");

					if (Parametros.saveRequestResponse) {
						try {
							saveXml(isdn, login, wsdlLoc, "facturarDeuda",
									port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(),
									port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoriaHistorial,
									Con, (fin - ini));
						} catch (Exception e) {
							log.warn(
									"No se logro registrar los request y response del servicio FacturarDeuda, facturarDeuda: ",
									e);
						}
					}

					if (response != null) {

						if (response.getResponseHeader() != null) {

							if (response.getResponseHeader().getServiceCode() != null) {
								log.info("Code response genarar CNS " + response.getResponseHeader().getServiceCode()
										+ ", " + response.getResponseHeader().getLocalizedMessage());
								if (response.getResponseHeader().getServiceCode().trim().equals("00")) {
									if (response.getResponseBody().getCns() != null) {
										cns.setNumero(response.getResponseBody().getCns().getNumero());
										cns.setSerie(response.getResponseBody().getCns().getSerie());
										cns.setTipo(response.getResponseBody().getCns().getTipo());
										respuestaSatisfactoriaCrearCns = true;
									} else {
										log.warn("[isdn: " + getIsdn() + "] CNS resuelto a nulo");
									}
								} else if (response.getResponseHeader().getServiceCode().trim().equals("24")) {
									habilitarDialogMsg = true;
									msgErrorCrearCNS = Parametros.mensajeLineaConMontoNoValido;
								} else if (response.getResponseHeader().getServiceCode().trim().equals("25")) {

									msgErrorCrearCNS = Parametros.mensajeLineaConCNS;
									log.info("mensaje emergente " + msgErrorCrearCNS);
									habilitarDialogMsg = true;
								} else if (response.getResponseHeader().getServiceCode().trim().equals("26")) {
									habilitarDialogMsg = true;
									msgErrorCrearCNS = Parametros.mensajeLineaDeudaNoCorresponde;
								} else {
									respuestaSatisfactoriaCrearCns = false;
									SysMessage.warn("INFO: " + response.getResponseHeader().getServiceCode() + ", "
											+ response.getResponseHeader().getLocalizedMessage(), null);
									// response.getResponseHeader().getLocalizedMessage(),
									// null);
								}

							} else {
								log.warn("[isdn: " + getIsdn() + "] LocalizedMessage resuelto a nulo");
							}
						} else {
							log.warn("[isdn: " + getIsdn() + "] ResponseHeader resuelto a nulo");
						}
					} else {
						log.warn("[isdn: " + getIsdn() + "] generarFacturaResonse resuelto a nulo");
					}

				} finally {
					portNodo.setEnUso(false);
					portNodo.setFechaFin(new Date());
				}
			}
		} else {
			log.error("[isdn: " + getIsdn() + "] Port FacturarDeuda Historial resuelto a nulo");
		}
	}

	public void onRowSelectRadio() {
		habilitadoBotonCrearCNS = false;
		listaDetalleDeuda = new ArrayList<>();
		if (deudaSeleccionada != null) {
			log.debug("Deuda TTP (Tigo Te Presta) selecciona: " + deudaSeleccionada);
			// if (deudaSeleccionada.getListaDetalle() != null) {
			// listaDetalleDeuda = deudaSeleccionada.getListaDetalle();
			// }
		}
	}

	public void verDetalle() {
		log.debug(deudaSeleccionada);

		listaDetalleDeuda = new ArrayList<>();

		if (deudaSeleccionada != null) {
			listaDetalleDeuda = deudaSeleccionada.getListaDetalle();
		}
		// for (int i = 0; i < 100; i++) {
		// listaDetalleDeuda.add(new DetalleDeuda("producto" + i,
		// UtilDate.randomDate(),
		// com.myapps.vista_ccc.util.UtilNumber.randomDouble(0, 100, 2),
		// com.myapps.vista_ccc.util.UtilNumber.randomDouble(0, 100, 2),
		// com.myapps.vista_ccc.util.UtilNumber.randomDouble(0, 100, 2)));
		// }
	}

	// public void onSelect(Deuda deuda) {
	// setDeudaSeleccionada(deuda);
	// deuda.setSeleccionado(deuda.getId());
	// log.debug("ITEM SELECCIONADO: " + deudaSeleccionada);
	//
	// if (getListaDeuda() != null) {
	// for (Deuda item : getListaDeuda()) {
	// if (!item.getId().equals(deuda.getId())) {
	// item.setSeleccionado("false");
	// }
	// }
	// }
	// }

	public void cargarListaEstadosValidosCrearCNS() {
		try {
			estadosValidosCrearCNS = new ArrayList<>();
			String states = Parametros.estadosValidosParaCrearCns;
			StringTokenizer st = new StringTokenizer(states, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				if (nextToken != null) {
					estadosValidosCrearCNS.add(nextToken.trim());
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar lista de estados validos para crear CNS: ", e);
			SysMessage.error("Error al cargar lista de estados validos para crear CNS: " + e.getMessage(), null);
		}
	}

	public void cargarListaEstadosDeudaCero() {
		try {
			estadosValidosDeudaCero = new ArrayList<>();
			String states = Parametros.estadoValidoDeudaCero;
			StringTokenizer st = new StringTokenizer(states, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				if (nextToken != null) {
					estadosValidosDeudaCero.add(nextToken.trim());
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar lista de estados validos para crear CNS: ", e);
			SysMessage.error("Error al cargar lista de estados validos para crear CNS: " + e.getMessage(), null);
		}
	}

	private void cargarHashEstadoColores() {
		try {
			hashEstadoColores = new HashMap<String, String>();
			String states = Parametros.estadosColores;
			StringTokenizer st = new StringTokenizer(states, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				try {
					StringTokenizer stt = new StringTokenizer(nextToken.trim(), ":");
					String key = stt.nextToken();
					String value = stt.nextToken();
					log.debug(key + ":" + value);
					hashEstadoColores.put(key.trim(), value.trim());
				} catch (Exception e) {
					// e.getMessage();
					log.error("Error al cargar token Estado Colores: ", e);
					SysMessage.error("Error al cargar token Estado Colores: " + e.getMessage(), null);
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash Estado Colores: ", e);
			SysMessage.error("Error al cargar hash Estado Colores: " + e.getMessage(), null);
		}
	}

	private void cargarHashEstadosInternoPantalla(String vista) {
		try {
			hashEstadosInternoPantalla = new HashMap<String, String>();
			String states = "";
			if(vista.equals("prepago")){
				states = Parametros.estadosInternoPantalla;
			}else{
				states = mapaParametros.get("ESTADOS.INTERNO.PANTALLA");
			}

			StringTokenizer st = new StringTokenizer(states, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				try {
					StringTokenizer stt = new StringTokenizer(nextToken.trim(), ":");
					String key = stt.nextToken();
					String value = stt.nextToken();
					log.debug(key + ":" + value);
					hashEstadosInternoPantalla.put(key.trim(), value.trim());
				} catch (Exception e) {
					log.error("Error al cargar token Estados Interno Pantalla: ", e);
					SysMessage.error("Error al cargar token Estados Interno Pantalla: " + e.getMessage(), null);
					// e.getMessage();
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash Estados Interno Pantalla: ", e);
			SysMessage.error("Error al cargar hash Estados Interno Pantalla: " + e.getMessage(), null);
		}
	}

	private void cargarHashEstadosInternoCorte(String vista) {
		try {
			hashEstadosInternoCorte = new HashMap<String, String>();

			String states = "";
			if(vista.equals("prepago")){
				states = Parametros.estadosInternoCorte;
			}else{
				states = mapaParametros.get("ESTADOS.INTERNO.CORTE");
			}
			StringTokenizer st = new StringTokenizer(states, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				try {
					StringTokenizer stt = new StringTokenizer(nextToken.trim(), ":");
					String key = stt.nextToken();
					String value = stt.nextToken();
					log.debug(key + ":" + value);
					hashEstadosInternoCorte.put(key.trim(), value.trim());
				} catch (Exception e) {
					log.error("Error al cargar token Estados Interno Pantalla: ", e);
					SysMessage.error("Error al cargar token Estados Interno Pantalla: " + e.getMessage(), null);
					// e.getMessage();
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash Estados Interno Pantalla: ", e);
			SysMessage.error("Error al cargar hash Estados Interno Pantalla: " + e.getMessage(), null);
		}
	}

	// private void cargarHashPropietarioLineas() {
	// try {
	// hashPropietarioLineas = new HashMap<String, String>();
	// String states = Parametros.propietarioLinea;
	// StringTokenizer st = new StringTokenizer(states, ",");
	// while (st.hasMoreTokens()) {
	// String nextToken = st.nextToken();
	// try {
	// StringTokenizer stt = new StringTokenizer(nextToken.trim(), ":");
	// String key = stt.nextToken();
	// String value = stt.nextToken();
	// hashPropietarioLineas.put(key.trim(), value.trim());
	// } catch (Exception e) {
	// e.getMessage();
	// }
	// }
	// } catch (Exception e) {
	// log.error("Error al cargar hash Propietario Lineas: ", e);
	// SysMessage.error("Error al cargar hash Propietario Lineas: " +
	// e.getMessage(), null);
	// }
	// }

	public String doubleToString(double number) {
		return UtilNumber.doubleToString(number, Parametros.deudaTtpNroDecimales, Parametros.deudaTtpLocaleLenguage,
				Parametros.deudaTtpLocaleCountry);
	}

	private void obtenerUsuario() {
		try {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
					.getRequest();
			this.login = (String) request.getSession().getAttribute("TEMP$USER_NAME");
			// log.info("login: " + login);
		} catch (Exception e) {
			log.error("Error al obtener usuario: ", e);
		}
	}

	private void cargarIp() {
		try {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
					.getRequest();
			ip = UtilUrl.getClientIp(request);
		} catch (Exception e) {
			log.error("Error al obtener ip: ", e);
		}
	}

	public void guardarExcepcion(long IdAuditoria, String exepcion) {
		log.debug("[saveVCExcepcion]: Ingresando..");
		VcLogExcepcion item = new VcLogExcepcion();
//		VcLogAuditoria find;
		try {
			VcLogAuditoria find = (VcLogAuditoria) blAuditoria.find(IdAuditoria, VcLogAuditoria.class);
			item.setFechaCreacion(Calendar.getInstance());
			item.setExcepcion(exepcion);
			if (find != null)
				item.setVcLogAuditoria(find);

			blExcepcion.save(item);
		} catch (Exception e) {
			log.error("Error al guardar Excepcion: ", e);
		}
	}

	public String stackTraceToString(Throwable e) {
		CharArrayWriter sw = new CharArrayWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		pw.close();

		return sw.toString(); // stack trace a
	}

	private long guardarAuditoria(Calendar fechaConsulta, String usuario, String ip, String vista, int comando,
			String isdn, List<VcLogAdicional> adicionales) {
		try {

			VcLogAuditoria item = new VcLogAuditoria();
			item.setFechaConsulta(fechaConsulta);
			item.setIsdn(isdn);
			item.setUsuario(usuario);
			item.setIp(ip);
			item.setVista(vista);
			item.setComando(comando);
			item.setAdicional("Y");
			item.setFechaCreacion(Calendar.getInstance());
			if (adicionales == null || adicionales.size() == 0) {
				item.setAdicional("N");
			}

			blAuditoria.guardarAuditoria(item, adicionales);
			CodigoAuditoria = item.getIdLogAuditoria();
			return item.getIdLogAuditoria();

		} catch (Exception e) {
			log.error("Error al guardar auditoria: ", e);
		}

		return -1;
	}

	private void validarCertificado(String pathKeystore, String ipPort) {
		try {
			// Properties sysProperties = System.getProperties();
			log.debug("Ingresando a validar certificado pathKeystore: " + pathKeystore + ", ipPort: " + ipPort);

			System.setProperty("javax.net.ssl.trustStore", pathKeystore);
			System.setProperty("java.protocol.handler.pkgs", ipPort);

			HostnameVerifier hv = new HostnameVerifier() {
				public boolean verify(String urlHostName, SSLSession session) {
					// System.out.println("Warning: URL Host: " + urlHostName +
					// " vs. " + session.getPeerHost());
					return true;
				}
			};

			// trustAllHttpsCertificates();
			HttpsURLConnection.setDefaultHostnameVerifier(hv);
		} catch (Exception e) {
			log.error("Error al validar certificado: ", e);
		}
	}

	private void saveXml(String isdn, String login, String servicio, String metodo, String request, String response,
			long idAuditoria, Long tiempoConexion, Long tiempoRespuesta) {
		try {
			VcConsulta c = new VcConsulta();
			c.setFechaCreacion(Calendar.getInstance());
			c.setIsdn(isdn);
			c.setLogin(login);
			c.setServicio(servicio);
			c.setMetodo(metodo);
			c.setRequest(request);
			c.setResponse(response);
			c.setTiempoConexion(tiempoConexion);
			c.setTiempoRespuesta(tiempoRespuesta);

			Object find = blAuditoria.find(idAuditoria, VcLogAuditoria.class);
			if (find != null)
				c.setVcLogAuditoria((VcLogAuditoria) find);

			blConsulta.save(c);
		} catch (Exception e) {
			log.warn("No se registro en bd el request y response: ", e);
		}
	}

	public static byte[] getBytesFromFile(File file) {
		log.debug("Ingresando a obtener bytes de archivo");
		InputStream is = null;
		byte[] bytes = null;
		try {
			is = new FileInputStream(file);

			// Get the size of the file
			long length = file.length();

			if (length > Integer.MAX_VALUE) {
				// File is too large
			}

			// Create the byte array to hold the data
			bytes = new byte[(int) length];

			// Read in the bytes
			int offset = 0;
			int numRead = 0;
			while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
				offset += numRead;
			}

			// Ensure all the bytes have been read in
			if (offset < bytes.length) {
				throw new IOException("Could not completely read file " + file.getName());
			}

			// Close the input stream and return bytes
			// is.close();

		} catch (FileNotFoundException e) {
			log.error("No existe el archivo = " + file.getName(), e);
		} catch (IOException e) {
			log.error("Error obteniendo bytes del archivo = " + file.getName(), e);
		} catch (Exception e) {
			log.error("Error obteniendo bytes del archivo = " + file.getName(), e);
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
				}
			}
		}
		return bytes;
	}

	public static int validarEstado(String listaValidar, String parametroValidar) {
		List<String> lista = Arrays.asList(listaValidar.split(","));
		int validarEstado = -1;
		if (parametroValidar != null) {
			for (int j = 0; j < lista.size(); j++) {
				if (parametroValidar.equals(lista.get(j).trim())) {
					validarEstado = j;
					break;
				}
			}
		}
		return validarEstado;
	}

	private String obtenerColor(String estado) {
		String color = "";
		if (hashEstadoColores != null) {
			color = hashEstadoColores.get(estado);
			if (color != null) {
				return color;
			} else {
				color = "";
			}
		}
		return color;
	}

	public String getIsdn() {
		return isdn;
	}

	public void setIsdn(String isdn) {
		this.isdn = isdn;
	}

	public Integer getCantidadRegistros() {
		return cantidadRegistros;
	}

	public void setCantidadRegistros(Integer cantidadRegistros) {
		this.cantidadRegistros = cantidadRegistros;
	}

	public Deuda getDeudaSeleccionada() {
		return deudaSeleccionada;
	}

	public void setDeudaSeleccionada(Deuda deudaSeleccionada) {
		this.deudaSeleccionada = deudaSeleccionada;
	}

	public CNS getCns() {
		return cns;
	}

	public void setCns(CNS cns) {
		this.cns = cns;
	}

	public String getMsgErrorCrearCNS() {
		return msgErrorCrearCNS;
	}

	public void setMsgErrorCrearCNS(String msgErrorCrearCNS) {
		this.msgErrorCrearCNS = msgErrorCrearCNS;
	}

	public boolean isHabilitadoBotonCrearCNS() {
		return habilitadoBotonCrearCNS;
	}

	public void setHabilitadoBotonCrearCNS(boolean habilitadoBotonCrearCNS) {
		this.habilitadoBotonCrearCNS = habilitadoBotonCrearCNS;
	}

	public List<DetalleDeuda> getListaDetalleDeuda() {
		return listaDetalleDeuda;
	}

	public void setListaDetalleDeuda(List<DetalleDeuda> listaDetalleDeuda) {
		this.listaDetalleDeuda = listaDetalleDeuda;
	}

	public List<DetalleDeuda> getListaDetalleDeudaFiltered() {
		return listaDetalleDeudaFiltered;
	}

	public void setListaDetalleDeudaFiltered(List<DetalleDeuda> listaDetalleDeudaFiltered) {
		this.listaDetalleDeudaFiltered = listaDetalleDeudaFiltered;
	}

	public List<Deuda> getListaDeudaFiltered() {
		return listaDeudaFiltered;
	}

	public void setListaDeudaFiltered(List<Deuda> listaDeudaFiltered) {
		this.listaDeudaFiltered = listaDeudaFiltered;
	}

	public List<Deuda> getListaDeuda() {
		return listaDeuda;
	}

	public void setListaDeuda(List<Deuda> listaDeuda) {
		this.listaDeuda = listaDeuda;
	}

	public String getPatronFechaDeuda() {
		return patronFechaDeuda;
	}

	public void setPatronFechaDeuda(String patronFechaDeuda) {
		this.patronFechaDeuda = patronFechaDeuda;
	}

	public String getPatronFechaDetalleDeuda() {
		return patronFechaDetalleDeuda;
	}

	public void setPatronFechaDetalleDeuda(String patronFechaDetalleDeuda) {
		this.patronFechaDetalleDeuda = patronFechaDetalleDeuda;
	}

	public boolean isHabilitarDialogMsg() {
		return habilitarDialogMsg;
	}

	public void setHabilitarDialogMsg(boolean habilitarDialogMsg) {
		this.habilitarDialogMsg = habilitarDialogMsg;
	}

	public boolean isHabilitarDialogCns() {
		return habilitarDialogCns;
	}

	public void setHabilitarDialogCns(boolean habilitarDialogCns) {
		this.habilitarDialogCns = habilitarDialogCns;
	}

	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}

	public String getMensajeConfirmacionCrearCns() {
		return mensajeConfirmacionCrearCns;
	}

	public void setMensajeConfirmacionCrearCns(String mensajeConfirmacionCrearCns) {
		this.mensajeConfirmacionCrearCns = mensajeConfirmacionCrearCns;
	}

	public List<Deuda> getListaDeudaPostPago() {
		return listaDeudaPostPago;
	}

	public void setListaDeudaPostPago(List<Deuda> listaDeudaPostPago) {
		this.listaDeudaPostPago = listaDeudaPostPago;
	}
}
