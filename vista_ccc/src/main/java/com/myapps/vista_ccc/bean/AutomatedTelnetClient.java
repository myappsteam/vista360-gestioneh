package com.myapps.vista_ccc.bean;

import com.myapps.vista_ccc.util.Parametros;
import org.apache.commons.net.telnet.TelnetClient;
import org.apache.log4j.Logger;

import java.io.DataInputStream;
import java.io.PrintStream;

//import com.myapps.vista_ccc.business.ExcepcionElementosRedBL;
//import com.myapps.vista_ccc.entity.VcConexionTelnet;
//import com.myapps.vista_ccc.entity.VcConfigElementoRed;
//import com.myapps.vista_ccc.util.Parametros;

public class AutomatedTelnetClient {
	public static Logger log = Logger.getLogger(AutomatedTelnetClient.class);
	private TelnetClient telnet = new TelnetClient();

	private PrintStream out;
	private String dpto;

	public AutomatedTelnetClient(String server, String puerto, String dpto) {
		try {
			this.dpto = dpto;
			// Connect to the specified server
			telnet.connect(server, Integer.parseInt(puerto));

			log.debug("Conexion Exitosa");
			// Get input and output stream references

			out = new PrintStream(telnet.getOutputStream(), true, "UTF-8");

			log.debug("Reading up to prompt.");
			Thread.sleep(Integer.parseInt(Parametros.esperaRespuestaElementosRed));
			readUntil();

			log.debug("Prompt found. Ready.");
			// Advance to a prompt
		} catch (Exception e) {
			log.error("No se pudo mostrar la repuesta del servidor de la conexion Telnet: ", e);
		}
	}

	public  String readUntil() {
		DataInputStream in = new DataInputStream(telnet.getInputStream());
		StringBuffer sb = new StringBuffer();
		try {
			if (telnet.getInputStream().available() == 0) {
				return "connection_not_complete";
			}

			
			int available = in.available();
			log.debug("available: " + available);
			if (available == 0)
				return "";

			int index = 1;
			char ch = (char) in.read();
			while (index < available) {

				index++;
				// System.out.print(ch);

				sb.append(ch);
				ch = (char) in.read();
			}
			sb.append(ch);

			log.debug("------------------------INICIO RESPUESTA SERVIDOR " + dpto + "----------------------------------");
			log.debug(sb.toString());
			log.debug("----------------------- FIN RESPUESTA SERVIDOR " + dpto + "----------------------------------");
			return sb.toString();
		} catch (Exception e) {
			log.error("No pudo leer: ", e);
			return "";
		}
	}

	public void write(String value) {
		try {
			out.println(value);
			out.flush();

			log.debug("_______ COMANDO: " + value);
		} catch (Exception e) {
			log.error("No pudo escribir de la conexion Telnet: ", e);
		}
	}

	// Se creo este writeAutenticacion para no escribir el comando que lleva el user y el password
	public void writeAutenticacion(String value) {
		try {
			out.println(value);
			out.flush();

		} catch (Exception e) {
			log.error("No pudo escribir de la conexion Telnet: ", e);
		}
	}

	public  String sendCommand(String command) {
		String result = "";
		boolean isComplete = false;// flag for bucle, continue reading if the buffer is not complete
		int intents = Integer.parseInt(Parametros.nroReintentosConexionTelnet);// counter, the bucle finish if the intents are 0
		try {
			write(command);
			while (!isComplete) {
				log.debug("intentos =" + intents);
				if (intents != 0) {
					Thread.sleep(Integer.parseInt(Parametros.esperaRespuestaElementosRed));
				}
				result = readUntil();
				if (!result.equals("connection_not_complete")) {
					isComplete = true;
					
					break;
				}
				intents--;
				if (intents == 0) {
					isComplete = true;
					result = "";
					break;
				}
			}
			return result;
		} catch (Exception e) {
			log.error("No pudo enviar el comando de la conexion Telnet: ", e);
		}
		return null;
	}

	public String sendCommandSingleLine(String command) {
		String result = "";
		boolean isComplete = false;// flag for bucle, continue reading if the buffer is not complete
		int intents = Integer.parseInt(Parametros.nroReintentosConexionTelnet);// counter, the bucle finish if the intents are 0
		try {
			write(command);
			while (!isComplete) {
				log.debug("intentos =" + intents);
				if (intents != 0) {
					Thread.sleep(Integer.parseInt(Parametros.esperaRespuestaElementosRedGGSN));
				}
				result = readUntil();
				if (!result.equals("connection_not_complete")) {
					isComplete = true;
					break;
				}
				intents--;
				if (intents == 0) {
					isComplete = true;
					result = "";
					break;
				}
			}
			return result;
		} catch (Exception e) {
			log.error("No pudo enviar el comando de la conexion Telnet: ", e);
		}
		return null;
	}

	public String sendCommandAutenticacion(String command) {
		String result = "";
		boolean isComplete = false;// flag for bucle, continue reading if the buffer is not complete
		int intents = Integer.parseInt(Parametros.nroReintentosConexionTelnet);// counter, the bucle finish if the intents are 0
		try {
			writeAutenticacion(command);
			while (!isComplete) {
				log.debug("intentos =" + intents);
				if (intents != 0) {
					Thread.sleep(Integer.parseInt(Parametros.esperaRespuestaElementosRed));
				}
				result = readUntil();
				if (!result.equals("connection_not_complete")) {
					isComplete = true;
					break;
				}
				intents--;
				if (intents == 0) {
					isComplete = true;
					result = "";
					break;
				}

			}
			return result;
		} catch (Exception e) {
			log.error("No pudo enviar el comando de la conexion Telnet: ", e);
		}
		return null;
	}

	// Se creo este enviar comandoAutenticacion para no imprimir el comando que lleva el user y el password
	// public String sendCommandAutenticacion(String command) {
	//
	// try {
	// writeAutenticacion(command);
	// Thread.sleep(Integer.parseInt(Parametros.esperaRespuestaElementosRed));
	// return readUntil();
	// } catch (Exception e) {
	// log.error("No pudo enviar el comando: " , e);
	// }
	// return null;
	// }

	public void disconnect() {
		try {
			telnet.disconnect();
		} catch (Exception e) {
			log.error("No se pudo desconectar de la conexion telnet: ", e);
		}
	}

	public static void main(String[] args) {

		AutomatedTelnetClient telnet = new AutomatedTelnetClient("172.25.0.80", "23", "Yeri");

		telnet.sendCommandAutenticacion("vista360");
		telnet.sendCommandAutenticacion("T3st*2016");

		telnet.sendCommand("display pdpcontext msisdn  “59177390166”");

		telnet.disconnect();

		// try {

//		 ControlTelnet control = new ControlTelnet();
//		 control.setCantidadHilos(2);
//		 ThreadTelnet telnet0 = new ThreadTelnet("0", ThreadTelnet.HLR_SCZ, control, "77901425");
//		 telnet0.start();
//		 ThreadTelnet telnet1 = new ThreadTelnet("1", ThreadTelnet.HLR_CBB, control, "77901425");
//		 telnet1.start();
//		 while (!control.finalizaron()) {
//		 log.debug("Aun no finalizaron los hilos.");
//		 try {
//		 Thread.sleep(2000);
//		 } catch (Exception e) {
//		 log.error(e.getMessage());//
//		 }
//		 }
//		 log.debug("Ha finalizado la consulta al HLR");
//		
//		 } catch (Exception e) {
//		 e.printStackTrace();
//		 }
	}
}