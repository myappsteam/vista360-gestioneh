package com.myapps.vista_ccc.bean.model;

import java.io.Serializable;

public class Oferta implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String oferta;
	private String tipo;
	private String estado;
	private String bundled;
	private String claseOferta;
	private String color;

	public Oferta() {
		super();
	}

	public Oferta(String oferta, String tipo, String estado, String bundled, String claseOferta, String color) {
		super();
		this.oferta = oferta;
		this.tipo = tipo;
		this.estado = estado;
		this.bundled = bundled;
		this.claseOferta = claseOferta;
		this.color = color;
	}

	public String getOferta() {
		return oferta;
	}

	public void setOferta(String oferta) {
		this.oferta = oferta;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getBundled() {
		return bundled;
	}

	public void setBundled(String bundled) {
		this.bundled = bundled;
	}

	public String getClaseOferta() {
		return claseOferta;
	}

	public void setClaseOferta(String claseOferta) {
		this.claseOferta = claseOferta;
	}

	@Override
	public String toString() {
		return "Oferta [oferta=" + oferta + ", tipo=" + tipo + ", estado=" + estado + ", bundled=" + bundled + ", claseOferta=" + claseOferta + "]";
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

}
