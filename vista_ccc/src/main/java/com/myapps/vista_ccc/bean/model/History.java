package com.myapps.vista_ccc.bean.model;

import java.io.Serializable;

public class History implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;
	private String isdn;
	private String categoria;
	private String tipoFlujo;
	private String tipoServicio;
	private String descripcionServicio;
	private String startDate;
	private String cdrSecuencia;

	// QUERY CDR
	private String otherNumber;
	private String roamFlag;
	private String callingCellId;
	private String calledCellId;
	private String specialNumber;
	private String endDate;
	private String billCycleId;
	private Integer measureUnit;

	private String startTime;
	private String endTime;

	private String refundIndicator;
	private String actualVolume;
	private String ratingVolume;
	private String freeVolume;
	private Integer measureUnitCdr;

	private String actualChargeAmt;
	private String freeChargeAmt;
	private String currencyIdCdr;

	private String actualTaxCode;
	private String actualTaxAmt;
	 private String additionalProperty;

	// AJUSTES
	private String category;
	private String primaryIdentity;
	private String tradeTime;
	private String remark;
	private String channelId;
	private String transId;
	private String exTransId;
	private String ajAdditionalProperty;

	// RECARGAS
	private String reCategory;
	private String reTradeTime;
	private String rePrimaryIdentity;
	private String rechargeType;
	private String rechargeChannelId;
	private String rechargeAmount;
	private String currencyId;
	private String cardSequence;
	private String rechargeReason;
	private String resultCode;
	private String reTransId;
	private String reExTransId;
	private String exRechargeType;
	private String rechargeTax;
	private String rechargePenalty;
	private String reversalFlag;
	private String reversalReason;
	private String reversalTime;
	private String reAdditionalProperty;

	public History() {
		super();
	}

	public History(String id, String isdn, String categoria, String tipoFlujo, String tipoServicio, String descripcionServicio, String startDate, String cdrSecuencia, String otherNumber, String roamFlag, String callingCellId, String calledCellId, String specialNumber, String endDate, String billCycleId, Integer measureUnit, String startTime, String endTime, String refundIndicator,
			String actualVolume, String ratingVolume, String freeVolume, Integer measureUnitCdr, String actualChargeAmt, String freeChargeAmt, String currencyIdCdr, String actualTaxCode, String actualTaxAmt, String category, String primaryIdentity, String tradeTime, String remark, String channelId, String transId, String exTransId, String additionalProperty, String reCategory, String reTradeTime,
			String rePrimaryIdentity, String rechargeType, String rechargeChannelId, String rechargeAmount, String currencyId, String cardSequence, String rechargeReason, String resultCode, String reTransId, String reExTransId, String exRechargeType, String rechargeTax, String rechargePenalty, String reversalFlag, String reversalReason, String reversalTime, String reAdditionalProperty) {
		super();
		this.id = id;
		this.isdn = isdn;
		this.categoria = categoria;
		this.tipoFlujo = tipoFlujo;
		this.tipoServicio = tipoServicio;
		this.descripcionServicio = descripcionServicio;
		this.startDate = startDate;
		this.cdrSecuencia = cdrSecuencia;
		this.otherNumber = otherNumber;
		this.roamFlag = roamFlag;
		this.callingCellId = callingCellId;
		this.calledCellId = calledCellId;
		this.specialNumber = specialNumber;
		this.endDate = endDate;
		this.billCycleId = billCycleId;
		this.measureUnit = measureUnit;
		this.startTime = startTime;
		this.endTime = endTime;
		this.refundIndicator = refundIndicator;
		this.actualVolume = actualVolume;
		this.ratingVolume = ratingVolume;
		this.freeVolume = freeVolume;
		this.measureUnitCdr = measureUnitCdr;
		this.actualChargeAmt = actualChargeAmt;
		this.freeChargeAmt = freeChargeAmt;
		this.currencyIdCdr = currencyIdCdr;
		this.actualTaxCode = actualTaxCode;
		this.actualTaxAmt = actualTaxAmt;
		this.category = category;
		this.primaryIdentity = primaryIdentity;
		this.tradeTime = tradeTime;
		this.remark = remark;
		this.channelId = channelId;
		this.transId = transId;
		this.exTransId = exTransId;
		this.reCategory = reCategory;
		this.reTradeTime = reTradeTime;
		this.rePrimaryIdentity = rePrimaryIdentity;
		this.rechargeType = rechargeType;
		this.rechargeChannelId = rechargeChannelId;
		this.rechargeAmount = rechargeAmount;
		this.currencyId = currencyId;
		this.cardSequence = cardSequence;
		this.rechargeReason = rechargeReason;
		this.resultCode = resultCode;
		this.reTransId = reTransId;
		this.reExTransId = reExTransId;
		this.exRechargeType = exRechargeType;
		this.rechargeTax = rechargeTax;
		this.rechargePenalty = rechargePenalty;
		this.reversalFlag = reversalFlag;
		this.reversalReason = reversalReason;
		this.reversalTime = reversalTime;
		this.reAdditionalProperty = reAdditionalProperty;
	}

	public String getIsdn() {
		return isdn;
	}

	public void setIsdn(String isdn) {
		this.isdn = isdn;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getTipoFlujo() {
		return tipoFlujo;
	}

	public void setTipoFlujo(String tipoFlujo) {
		this.tipoFlujo = tipoFlujo;
	}

	public String getTipoServicio() {
		return tipoServicio;
	}

	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}

	public String getDescripcionServicio() {
		return descripcionServicio;
	}

	public void setDescripcionServicio(String descripcionServicio) {
		this.descripcionServicio = descripcionServicio;
	}

	public String getCdrSecuencia() {
		return cdrSecuencia;
	}

	public void setCdrSecuencia(String cdrSecuencia) {
		this.cdrSecuencia = cdrSecuencia;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOtherNumber() {
		return otherNumber;
	}

	public void setOtherNumber(String otherNumber) {
		this.otherNumber = otherNumber;
	}

	public String getRoamFlag() {
		return roamFlag;
	}

	public void setRoamFlag(String roamFlag) {
		this.roamFlag = roamFlag;
	}

	public String getCallingCellId() {
		return callingCellId;
	}

	public void setCallingCellId(String callingCellId) {
		this.callingCellId = callingCellId;
	}

	public String getCalledCellId() {
		return calledCellId;
	}

	public void setCalledCellId(String calledCellId) {
		this.calledCellId = calledCellId;
	}

	public String getSpecialNumber() {
		return specialNumber;
	}

	public void setSpecialNumber(String specialNumber) {
		this.specialNumber = specialNumber;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getBillCycleId() {
		return billCycleId;
	}

	public void setBillCycleId(String billCycleId) {
		this.billCycleId = billCycleId;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Integer getMeasureUnit() {
		return measureUnit;
	}

	public void setMeasureUnit(Integer measureUnit) {
		this.measureUnit = measureUnit;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getPrimaryIdentity() {
		return primaryIdentity;
	}

	public void setPrimaryIdentity(String primaryIdentity) {
		this.primaryIdentity = primaryIdentity;
	}

	public String getTradeTime() {
		return tradeTime;
	}

	public void setTradeTime(String tradeTime) {
		this.tradeTime = tradeTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getTransId() {
		return transId;
	}

	public void setTransId(String transId) {
		this.transId = transId;
	}

	public String getExTransId() {
		return exTransId;
	}

	public void setExTransId(String exTransId) {
		this.exTransId = exTransId;
	}

	public String getRechargeType() {
		return rechargeType;
	}

	public void setRechargeType(String rechargeType) {
		this.rechargeType = rechargeType;
	}

	public String getRechargeChannelId() {
		return rechargeChannelId;
	}

	public void setRechargeChannelId(String rechargeChannelId) {
		this.rechargeChannelId = rechargeChannelId;
	}

	public String getRechargeAmount() {
		return rechargeAmount;
	}

	public void setRechargeAmount(String rechargeAmount) {
		this.rechargeAmount = rechargeAmount;
	}

	public String getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}

	public String getCardSequence() {
		return cardSequence;
	}

	public void setCardSequence(String cardSequence) {
		this.cardSequence = cardSequence;
	}

	public String getRechargeReason() {
		return rechargeReason;
	}

	public void setRechargeReason(String rechargeReason) {
		this.rechargeReason = rechargeReason;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getExRechargeType() {
		return exRechargeType;
	}

	public void setExRechargeType(String exRechargeType) {
		this.exRechargeType = exRechargeType;
	}

	public String getRechargeTax() {
		return rechargeTax;
	}

	public void setRechargeTax(String rechargeTax) {
		this.rechargeTax = rechargeTax;
	}

	public String getRechargePenalty() {
		return rechargePenalty;
	}

	public void setRechargePenalty(String rechargePenalty) {
		this.rechargePenalty = rechargePenalty;
	}

	public String getReversalFlag() {
		return reversalFlag;
	}

	public void setReversalFlag(String reversalFlag) {
		this.reversalFlag = reversalFlag;
	}

	public String getReversalReason() {
		return reversalReason;
	}

	public void setReversalReason(String reversalReason) {
		this.reversalReason = reversalReason;
	}

	public String getReversalTime() {
		return reversalTime;
	}

	public void setReversalTime(String reversalTime) {
		this.reversalTime = reversalTime;
	}

	public String getReCategory() {
		return reCategory;
	}

	public void setReCategory(String reCategory) {
		this.reCategory = reCategory;
	}

	public String getReTradeTime() {
		return reTradeTime;
	}

	public void setReTradeTime(String reTradeTime) {
		this.reTradeTime = reTradeTime;
	}

	public String getRePrimaryIdentity() {
		return rePrimaryIdentity;
	}

	public void setRePrimaryIdentity(String rePrimaryIdentity) {
		this.rePrimaryIdentity = rePrimaryIdentity;
	}

	public String getReTransId() {
		return reTransId;
	}

	public void setReTransId(String reTransId) {
		this.reTransId = reTransId;
	}

	public String getReExTransId() {
		return reExTransId;
	}

	public void setReExTransId(String reExTransId) {
		this.reExTransId = reExTransId;
	}

	public String getRefundIndicator() {
		return refundIndicator;
	}

	public void setRefundIndicator(String refundIndicator) {
		this.refundIndicator = refundIndicator;
	}

	public String getActualVolume() {
		return actualVolume;
	}

	public void setActualVolume(String actualVolume) {
		this.actualVolume = actualVolume;
	}

	public String getRatingVolume() {
		return ratingVolume;
	}

	public void setRatingVolume(String ratingVolume) {
		this.ratingVolume = ratingVolume;
	}

	public String getFreeVolume() {
		return freeVolume;
	}

	public void setFreeVolume(String freeVolume) {
		this.freeVolume = freeVolume;
	}

	public Integer getMeasureUnitCdr() {
		return measureUnitCdr;
	}

	public void setMeasureUnitCdr(Integer measureUnitCdr) {
		this.measureUnitCdr = measureUnitCdr;
	}

	public String getActualChargeAmt() {
		return actualChargeAmt;
	}

	public void setActualChargeAmt(String actualChargeAmt) {
		this.actualChargeAmt = actualChargeAmt;
	}

	public String getFreeChargeAmt() {
		return freeChargeAmt;
	}

	public void setFreeChargeAmt(String freeChargeAmt) {
		this.freeChargeAmt = freeChargeAmt;
	}

	public String getCurrencyIdCdr() {
		return currencyIdCdr;
	}

	public void setCurrencyIdCdr(String currencyIdCdr) {
		this.currencyIdCdr = currencyIdCdr;
	}

	public String getActualTaxCode() {
		return actualTaxCode;
	}

	public void setActualTaxCode(String actualTaxCode) {
		this.actualTaxCode = actualTaxCode;
	}

	public String getActualTaxAmt() {
		return actualTaxAmt;
	}

	public void setActualTaxAmt(String actualTaxAmt) {
		this.actualTaxAmt = actualTaxAmt;
	}

	public String getReAdditionalProperty() {
		return reAdditionalProperty;
	}

	public void setReAdditionalProperty(String reAdditionalProperty) {
		this.reAdditionalProperty = reAdditionalProperty;
	}

	@Override
	public String toString() {
		return "History [id=" + id + ", isdn=" + isdn + ", categoria=" + categoria + ", tipoFlujo=" + tipoFlujo + ", tipoServicio=" + tipoServicio + ", descripcionServicio=" + descripcionServicio + ", startDate=" + startDate + ", cdrSecuencia=" + cdrSecuencia + ", otherNumber=" + otherNumber + ", roamFlag=" + roamFlag + ", callingCellId=" + callingCellId + ", calledCellId=" + calledCellId
				+ ", specialNumber=" + specialNumber + ", endDate=" + endDate + ", billCycleId=" + billCycleId + ", measureUnit=" + measureUnit + ", startTime=" + startTime + ", endTime=" + endTime + ", refundIndicator=" + refundIndicator + ", actualVolume=" + actualVolume + ", ratingVolume=" + ratingVolume + ", freeVolume=" + freeVolume + ", measureUnitCdr=" + measureUnitCdr
				+ ", actualChargeAmt=" + actualChargeAmt + ", freeChargeAmt=" + freeChargeAmt + ", currencyIdCdr=" + currencyIdCdr + ", actualTaxCode=" + actualTaxCode + ", actualTaxAmt=" + actualTaxAmt + ", category=" + category + ", primaryIdentity=" + primaryIdentity + ", tradeTime=" + tradeTime + ", remark=" + remark + ", channelId=" + channelId + ", transId=" + transId + ", exTransId="
				+ exTransId + ", reCategory=" + reCategory + ", reTradeTime=" + reTradeTime + ", rePrimaryIdentity=" + rePrimaryIdentity + ", rechargeType=" + rechargeType + ", rechargeChannelId=" + rechargeChannelId + ", rechargeAmount=" + rechargeAmount + ", currencyId=" + currencyId + ", cardSequence=" + cardSequence + ", rechargeReason=" + rechargeReason + ", resultCode=" + resultCode
				+ ", reTransId=" + reTransId + ", reExTransId=" + reExTransId + ", exRechargeType=" + exRechargeType + ", rechargeTax=" + rechargeTax + ", rechargePenalty=" + rechargePenalty + ", reversalFlag=" + reversalFlag + ", reversalReason=" + reversalReason + ", reversalTime=" + reversalTime + "]";
	}

	public String getAjAdditionalProperty() {
		return ajAdditionalProperty;
	}

	public void setAjAdditionalProperty(String ajAdditionalProperty) {
		this.ajAdditionalProperty = ajAdditionalProperty;
	}

	public String getAdditionalProperty() {
		return additionalProperty;
	}

	public void setAdditionalProperty(String additionalProperty) {
		this.additionalProperty = additionalProperty;
	}

}
