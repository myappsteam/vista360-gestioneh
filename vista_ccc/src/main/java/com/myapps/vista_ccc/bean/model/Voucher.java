package com.myapps.vista_ccc.bean.model;

import java.io.Serializable;

public class Voucher implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String faceValue;
	private String currency;
	private String cardStartDate;
	private String rechargeNumber;
	private String cardCosId;
	private String cardCosName;
	private String cardState;
	private String serviceProvider;
	private String cosChangeFlag;
	private String usedBy;
	private String expirationDate;
	private String dateUsed;

	public Voucher() {
		super();
	}

	public Voucher(String faceValue, String currency, String cardStartDate, String rechargeNumber, String cardCosId, String cardCosName, String cardState, String serviceProvider, String cosChangeFlag, String usedBy, String expirationDate, String dateUsed) {
		super();
		this.faceValue = faceValue;
		this.currency = currency;
		this.cardStartDate = cardStartDate;
		this.rechargeNumber = rechargeNumber;
		this.cardCosId = cardCosId;
		this.cardCosName = cardCosName;
		this.cardState = cardState;
		this.serviceProvider = serviceProvider;
		this.cosChangeFlag = cosChangeFlag;
		this.usedBy = usedBy;
		this.expirationDate = expirationDate;
		this.dateUsed = dateUsed;
	}

	public String getCardState() {
		return cardState;
	}

	public void setCardState(String cardState) {
		this.cardState = cardState;
	}

	public String getCardStartDate() {
		return cardStartDate;
	}

	public void setCardStartDate(String cardStartDate) {
		this.cardStartDate = cardStartDate;
	}

	public String getRechargeNumber() {
		return rechargeNumber;
	}

	public void setRechargeNumber(String rechargeNumber) {
		this.rechargeNumber = rechargeNumber;
	}

	public String getCardCosId() {
		return cardCosId;
	}

	public void setCardCosId(String cardCosId) {
		this.cardCosId = cardCosId;
	}

	public String getCardCosName() {
		return cardCosName;
	}

	public void setCardCosName(String cardCosName) {
		this.cardCosName = cardCosName;
	}

	public String getFaceValue() {
		return faceValue;
	}

	public void setFaceValue(String faceValue) {
		this.faceValue = faceValue;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getServiceProvider() {
		return serviceProvider;
	}

	public void setServiceProvider(String serviceProvider) {
		this.serviceProvider = serviceProvider;
	}

	public String getCosChangeFlag() {
		return cosChangeFlag;
	}

	public void setCosChangeFlag(String cosChangeFlag) {
		this.cosChangeFlag = cosChangeFlag;
	}

	public String getUsedBy() {
		return usedBy;
	}

	public void setUsedBy(String usedBy) {
		this.usedBy = usedBy;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getDateUsed() {
		return dateUsed;
	}

	public void setDateUsed(String dateUsed) {
		this.dateUsed = dateUsed;
	}

	@Override
	public String toString() {
		return "Voucher [faceValue=" + faceValue + ", currency=" + currency + ", cardStartDate=" + cardStartDate + ", rechargeNumber=" + rechargeNumber + ", cardCosId=" + cardCosId + ", cardCosName=" + cardCosName + ", cardState=" + cardState + ", serviceProvider=" + serviceProvider + ", cosChangeFlag=" + cosChangeFlag + ", usedBy=" + usedBy + ", expirationDate=" + expirationDate + ", dateUsed="
				+ dateUsed + "]";
	}

}
