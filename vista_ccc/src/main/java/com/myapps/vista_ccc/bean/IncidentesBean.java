package com.myapps.vista_ccc.bean;

import com.myapps.user.bean.ControlerBitacora;
import com.myapps.user.business.UsuarioBL;
import com.myapps.user.ldap.DescriptorBitacora;
import com.myapps.user.model.MuUsuario;
import com.myapps.vista_ccc.bean.model.Incidente;
import com.myapps.vista_ccc.business.*;
import com.myapps.vista_ccc.entity.*;
import com.myapps.vista_ccc.servicio.Servicio;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.SysMessage;
import com.myapps.vista_ccc.util.UtilDate;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;

@ManagedBean
@ViewScoped
public class IncidentesBean implements Serializable {

	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(IncidentesBean.class);

	@Inject
	private IncidenteBL incidenteBL;

	@Inject
	private UsuarioBL userBL;

	@Inject
	private LogAuditoriaBL logAuditoriaBL;

	@Inject
	private ConsultaBL consultaBL;
	
	@Inject
	private ConsultaRedBL consultaRedBL;

	@Inject
	private LogAdicionalBL logAdicionalBL;

	@Inject
	private LogExcepcionBL logExepcionBL;

	@Inject
	private ControlerBitacora controlerBitacora;

	private VcIncidente vcinc;
	private Date fechainc;
	private Date fechafin;
	private String usuario;
	private String vista;
	private String atencion;
	private String idi;
	private List<SelectItem> selectItems;
	private String select;
	private List<SelectItem> selectItems2;
	private String select2;
	private List<SelectItem> selectItems3;
	private String select3;
	private boolean controlFilter;

	private LazyDataModel<Incidente> model;

	public LazyDataModel<Incidente> getModel() {
		return model;
	}

	public void setModel(LazyDataModel<Incidente> model) {
		this.model = model;
	}

	public List<SelectItem> getSelectItems3() {
		return selectItems3;
	}

	public void setSelectItems3(List<SelectItem> selectItems3) {
		this.selectItems3 = selectItems3;
	}

	public String getSelect3() {
		return select3;
	}

	public void setSelect3(String select3) {
		this.select3 = select3;
	}

	private List<VcIncidente> listIncidente;

	@PostConstruct
	public void init() {
		try {
			Servicio.setPagina(this.getClass().getSimpleName());
			Servicio.eliminarConexionesInactivas(this.getClass().getSimpleName());

			cargarListUsuarios();
			cargarListVistas();
			controlFilter = false;
			atencion = "";
			vista = "";
			usuario = "";
			select = "";
			select2 = "";
			idi = "";
			fechainc = null;
			fechafin = null;
			paginar();
		} catch (Exception e) {
			log.error("[init] Fallo en el init.", e);
		}
	}

	public List<SelectItem> getSelectItems() {
		return selectItems;
	}

	public void setSelectItems(List<SelectItem> selectItems) {
		this.selectItems = selectItems;
	}

	public String getSelect() {
		return select;
	}

	public void setSelect(String select) {
		this.select = select;
	}

	private void cargarListUsuarios() {
		try {
			selectItems = new ArrayList<SelectItem>();
			selectItems.add(new SelectItem("", "Todo"));
			List<MuUsuario> listaUser = userBL.getUsuario();
			for (MuUsuario user : listaUser) {
				SelectItem sel = new SelectItem(user.getLogin(), user.getLogin());
				selectItems.add(sel);
			}
		} catch (Exception e) {
			log.error("Error al cargar lista de Usuarios: ", e);
			SysMessage.error("Error al cargar lista de Usuarios: " + e.getMessage(), null);
		}
	}

	private void cargarListVistas() {
		try {
			selectItems2 = new ArrayList<SelectItem>();
			String states = Parametros.incidenteVistas;
			selectItems2.add(new SelectItem("", "Todo"));
			StringTokenizer st = new StringTokenizer(states, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				if (nextToken != null) {
					SelectItem sel = new SelectItem(nextToken.trim(), nextToken.trim());
					selectItems2.add(sel);
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar lista de vistas: ", e);
			SysMessage.error("Error al cargar lista de vistas: " + e.getMessage(), null);
		}
	}

	public void Buscar() {
		controlFilter = true;
		RequestContext requestContext = RequestContext.getCurrentInstance();
		requestContext.execute("PF('orderDataTable').clearFilters()");

		String busqueda = "";
		if (!idi.isEmpty())
			busqueda = busqueda + ", Nro Incidente: " + idi;

		if (atencion.equals(""))
			busqueda = busqueda + ", Atencion: " + "Todos";
		if (atencion.equals("0"))
			busqueda = busqueda + ", Atencion: " + "No Atendido";
		if (atencion.equals("1"))
			busqueda = busqueda + ", Atencion: " + "Atendido";

		if (select.equals(""))
			busqueda = busqueda + ", Usuario: " + "Todos";
		else
			busqueda = busqueda + ", Usuario: " + select;

		if (select2.equals(""))
			busqueda = busqueda + ", Vista: " + "Todos";
		else
			busqueda = busqueda + ", Vista: " + select2;

		if (fechainc != null)
			busqueda = busqueda + ", Fecha Inicio: " + UtilDate.dateToString(fechainc, Parametros.fechaFormatPage);

		if (fechafin != null)
			busqueda = busqueda + ", Fecha Fin: " + UtilDate.dateToString(fechafin, Parametros.fechaFormatPage);

		log.debug("filtro busqueda Incidente: " + busqueda);

		model = new LazyCarDataModel(new ArrayList<Incidente>(), select, select2, fechainc, fechafin, atencion, idi, controlFilter);

		controlerBitacora.accion(DescriptorBitacora.INCIDENTE, "Se hizo la busqueda de Incidente" + busqueda);

	}

	public void editIncidente(Incidente item) {
		if (item.getAtendido()) {
			SysMessage.warn("Este registro ya fue atendido", null);
			return;
		}
		Long id = Long.parseLong(item.getId());

		try {
			vcinc = (VcIncidente) incidenteBL.find(id, VcIncidente.class);
			if (vcinc.getAtencion()) {
				SysMessage.warn("Este registro ya fue atendido", null);
				return;
			}
			vcinc.setAtencion(true);
			vcinc.setFechaAtencion(Calendar.getInstance());
			incidenteBL.update(vcinc);
			controlerBitacora.accion(DescriptorBitacora.INCIDENTE, "Se modifico: " + DescriptorBitacora.INCIDENTE + " con Id: " + String.valueOf(item.getId()));

			item.setAtendido(true);
			item.setFechaAtencion(vcinc.getFechaAtencion().getTime());
			SysMessage.info("Se guardo correctamente", null);
		} catch (Exception e) {
			log.error("[saveIncidente] error al momento de modificar: " + item.getId() + " " + e);
			SysMessage.error("Fallo al guardar en la Base de Datos.", null);
		}
	}

	public void limpiar() {
		select = "";
		select2 = "";
		fechainc = null;
		fechafin = null;
		atencion = "";
		idi = "";
		controlFilter = false;
		model = new LazyCarDataModel(new ArrayList<Incidente>(), select, select2, fechainc, fechafin, atencion, idi, controlFilter);
	}

	public void paginar() {
		log.debug("Ingresando a paginar....");

		model = new LazyCarDataModel(new ArrayList<Incidente>(), select, select2, fechainc, fechafin, atencion, idi, controlFilter);
	}

	public class LazyCarDataModel extends LazyDataModel<Incidente> {
		private static final long serialVersionUID = 1L;

		private List<Incidente> datasource;
		String usuario;
		String vista;
		Date FechaIni;
		Date FechaFin;
		String atencion;
		String id;
		boolean controlFilter;

		public LazyCarDataModel(List<Incidente> datasource, String usuario, String vista, Date FechaIni, Date FechaFin, String atencion, String id, boolean controlFilter) {
			this.datasource = datasource;
			this.usuario = usuario;
			this.vista = vista;
			this.FechaIni = fechainc;
			this.FechaFin = fechafin;
			this.atencion = atencion;
			this.id = id;
			this.controlFilter = controlFilter;
		}

		@Override
		public Incidente getRowData(String rowKey) {
			for (Incidente item : datasource) {
				if (item.getId().equals(rowKey))
					return item;
			}

			return null;
		}

		@Override
		public Object getRowKey(Incidente item) {
			return item.getId();
		}

		@Override
		public List<Incidente> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
			// log.debug("First: " + first + ", pageSize: " + pageSize + ", sortField: "+ sortField + ", SortOrder: " + SortOrde
			log.info("first:" + first + " pageSize:" + pageSize + "paramString:" + sortField + " sortOrder:" + sortOrder.toString() + " paramMap:" + filters.toString());
			if (controlFilter) {
				datasource = incidenteBL.findByPage(first, pageSize, sortField, sortOrder, filters, usuario, vista, FechaIni, FechaFin, atencion, id);
				getModel().setRowCount(incidenteBL.count(first, pageSize, sortField, sortOrder, filters, usuario, vista, FechaIni, FechaFin, atencion, id));
			} else {
				datasource = new ArrayList<Incidente>();
				getModel().setRowCount(0);
			}
			return datasource;
		}

		public List<Incidente> getDatasource() {
			return datasource;
		}

		public void setDatasource(List<Incidente> datasource) {
			this.datasource = datasource;
		}
	}

	public String getAtencion() {
		return atencion;
	}

	public void setAtencion(String atencion) {
		this.atencion = atencion;
	}

	public Date getFechafin() {
		return fechafin;
	}

	public void setFechafin(Date fechafin) {
		this.fechafin = fechafin;
	}

	public Date getFechainc() {
		return fechainc;
	}

	public void setFechainc(Date fechainc) {
		this.fechainc = fechainc;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getVista() {
		return vista;
	}

	public void setVista(String vista) {
		this.vista = vista;
	}

	public List<VcIncidente> getListIncidente() {
		return listIncidente;
	}

	public void setListIncidente(List<VcIncidente> listIncidente) {
		this.listIncidente = listIncidente;
	}

	public String getIdi() {
		return idi;
	}

	public void setIdi(String idi) {
		this.idi = idi;
	}

	public String getSelect2() {
		return select2;
	}

	public void setSelect2(String select2) {
		this.select2 = select2;
	}

	public List<SelectItem> getSelectItems2() {
		return selectItems2;
	}

	public void setSelectItems2(List<SelectItem> selectItems2) {
		this.selectItems2 = selectItems2;
	}

	public void exportExcel(Incidente au) throws Throwable {
		Row row = null;
		Row rowAdicional = null;
		Row rowConsulta = null;
		Row rowErrores = null;
		Row rowConsultaRed = null;
		Cell cellAuditoria = null;
		Cell cellAdicional = null;
		Cell cellConsulta = null;
		Cell cellErrores = null;
		Cell cellConsultaRed = null;

		File temp = null;
		InputStream stream = null;
		OutputStream os = null;

		try {
			if (au.getIdAuditoria() == null || au.getIdAuditoria().isEmpty()) {
				log.info("Nro de Auditoria no valido");
				SysMessage.error("Numero de auditoria no valido.", null);
				return;
			}

			long idAuditoria = Long.parseLong(au.getIdAuditoria());

			HSSFWorkbook wb = new HSSFWorkbook();

			// HSSFPalette palette = wb.getCustomPalette();
			// HSSFColor myColor = palette.findSimilarColor(64,126,178);
			// // get the palette index of that color
			// short palIndex = myColor.getIndex();
			//
			HSSFCellStyle styleHeader = (HSSFCellStyle) wb.createCellStyle();
			HSSFFont fontHeader = (HSSFFont) wb.createFont();
			fontHeader.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			styleHeader.setWrapText(true);
			styleHeader.setFont(fontHeader);
			styleHeader.setFillForegroundColor(IndexedColors.LIGHT_BLUE.index);
			styleHeader.setFillPattern(CellStyle.SOLID_FOREGROUND);
			styleHeader.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			styleHeader.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
			styleHeader.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
			styleHeader.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
			styleHeader.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
			styleHeader.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

			HSSFCellStyle styleRow = (HSSFCellStyle) wb.createCellStyle();
			HSSFFont fontRow = (HSSFFont) wb.createFont();
			fontRow.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
			styleRow.setFont(fontRow);
			styleRow.setWrapText(true);
			styleRow.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
			styleRow.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
			styleRow.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
			styleRow.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
			styleRow.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

			// AUDITORIAS
			Sheet sheet = wb.createSheet("Registro Auditoria");
			row = sheet.createRow((short) 0);

			cellAuditoria = row.createCell(0);
			cellAuditoria.setCellValue("Log Auditoria");
			cellAuditoria.setCellStyle(styleHeader);

			cellAuditoria = row.createCell(1);
			cellAuditoria.setCellValue("Fecha Consulta");
			cellAuditoria.setCellStyle(styleHeader);

			cellAuditoria = row.createCell(2);
			cellAuditoria.setCellValue("Usuario");
			cellAuditoria.setCellStyle(styleHeader);

			cellAuditoria = row.createCell(3);
			cellAuditoria.setCellValue("Ip");
			cellAuditoria.setCellStyle(styleHeader);

			cellAuditoria = row.createCell(4);
			cellAuditoria.setCellValue("Vista");
			cellAuditoria.setCellStyle(styleHeader);

			cellAuditoria = row.createCell(5);
			cellAuditoria.setCellValue("Comando");
			cellAuditoria.setCellStyle(styleHeader);

			cellAuditoria = row.createCell(6);
			cellAuditoria.setCellValue("ISDN");
			cellAuditoria.setCellStyle(styleHeader);

			cellAuditoria = row.createCell(7);
			cellAuditoria.setCellValue("Adicional");
			cellAuditoria.setCellStyle(styleHeader);

			List<VcLogAuditoria> auditorias = logAuditoriaBL.findAll(idAuditoria);
			if (auditorias != null) {
				for (int k = 0; k < auditorias.size(); k++) {
					row = sheet.createRow((short) k + 1);
					VcLogAuditoria item = auditorias.get(k);

					cellAuditoria = row.createCell(0);
					cellAuditoria.setCellValue(String.valueOf((item.getIdLogAuditoria())));
					cellAuditoria.setCellStyle(styleRow);

					cellAuditoria = row.createCell(1);
					cellAuditoria.setCellValue(UtilDate.dateToString(item.getFechaConsulta().getTime(), Parametros.fechaFormatPage));
					cellAuditoria.setCellStyle(styleRow);

					cellAuditoria = row.createCell(2);
					if (item.getUsuario() != null)
						cellAuditoria.setCellValue(item.getUsuario());
					else
						cellAuditoria.setCellValue("");

					cellAuditoria.setCellStyle(styleRow);

					cellAuditoria = row.createCell(3);
					if (item.getIp() != null)
						cellAuditoria.setCellValue(item.getIp());
					else
						cellAuditoria.setCellValue("");
					cellAuditoria.setCellStyle(styleRow);

					cellAuditoria = row.createCell(4);
					if (item.getVista() != null)
						cellAuditoria.setCellValue(item.getVista());
					else
						cellAuditoria.setCellValue("");
					cellAuditoria.setCellStyle(styleRow);

					cellAuditoria = row.createCell(5);
					if (item.getComando() != null)
						cellAuditoria.setCellValue(String.valueOf(item.getComando()));
					else
						cellAuditoria.setCellValue("");
					cellAuditoria.setCellStyle(styleRow);

					cellAuditoria = row.createCell(6);
					if (item.getIsdn() != null)
						cellAuditoria.setCellValue(item.getIsdn());
					else
						cellAuditoria.setCellValue("");
					cellAuditoria.setCellStyle(styleRow);

					cellAuditoria = row.createCell(7);
					if (item.getAdicional() != null)
						cellAuditoria.setCellValue(item.getAdicional());
					else
						cellAuditoria.setCellValue("");
					cellAuditoria.setCellStyle(styleRow);
				}
			}
			// VC LOG ADICIONAL
			Sheet sheetAdicional = wb.createSheet("Datos Adicionales");
			rowAdicional = sheetAdicional.createRow((short) 0);

			// AUDITORIAS
			cellAdicional = rowAdicional.createCell(0);
			cellAdicional.setCellValue("Id Adicional");
			cellAdicional.setCellStyle(styleHeader);

			cellAdicional = rowAdicional.createCell(1);
			cellAdicional.setCellValue("Id Auditoria");
			cellAdicional.setCellStyle(styleHeader);

			cellAdicional = rowAdicional.createCell(2);
			cellAdicional.setCellValue("Codigo");
			cellAdicional.setCellStyle(styleHeader);

			cellAdicional = rowAdicional.createCell(3);
			cellAdicional.setCellValue("Valor");
			cellAdicional.setCellStyle(styleHeader);

			cellAdicional = rowAdicional.createCell(4);
			cellAdicional.setCellValue("Fecha Creacion");
			cellAdicional.setCellStyle(styleHeader);

			List<VcLogAdicional> adicionales = logAdicionalBL.findAll(idAuditoria);
			// for (DBData[] temp : tabularData) {
			if (adicionales != null) {
				for (int k = 0; k < adicionales.size(); k++) {
					rowAdicional = sheetAdicional.createRow((short) k + 1);
					VcLogAdicional item = adicionales.get(k);

					cellAdicional = rowAdicional.createCell(0);
					cellAdicional.setCellValue(String.valueOf((item.getIdLogAdicional())));
					cellAdicional.setCellStyle(styleRow);

					cellAdicional = rowAdicional.createCell(1);
					cellAdicional.setCellValue(String.valueOf((item.getVcLogAuditoria().getIdLogAuditoria())));
					cellAdicional.setCellStyle(styleRow);

					cellAdicional = rowAdicional.createCell(2);
					if (item.getCodigo() != null)
						cellAdicional.setCellValue(item.getCodigo());
					else
						cellAdicional.setCellValue("");
					cellAdicional.setCellStyle(styleRow);

					cellAdicional = rowAdicional.createCell(3);
					if (item.getValor() != null)
						cellAdicional.setCellValue(item.getValor());
					else
						cellAdicional.setCellValue("");
					cellAdicional.setCellStyle(styleRow);

					cellAdicional = rowAdicional.createCell(4);
					cellAdicional.setCellValue(UtilDate.dateToString(item.getFechaCreacion().getTime(), Parametros.fechaFormatPage));
					cellAdicional.setCellStyle(styleRow);
				}
			}

			// VCCONSULTA

			Sheet sheetConsulta = wb.createSheet("Consulta CBS");
			rowConsulta = sheetConsulta.createRow((short) 0);
			sheetConsulta.setColumnWidth(7, 100 * 280);
			sheetConsulta.setColumnWidth(8, 100 * 280);

			cellConsulta = rowConsulta.createCell(0);
			cellConsulta.setCellValue("Id Consulta");
			cellConsulta.setCellStyle(styleHeader);

			cellConsulta = rowConsulta.createCell(1);
			cellConsulta.setCellValue("Id Auditoria");
			cellConsulta.setCellStyle(styleHeader);

			cellConsulta = rowConsulta.createCell(2);
			cellConsulta.setCellValue("Id Hilo");
			cellConsulta.setCellStyle(styleHeader);

			cellConsulta = rowConsulta.createCell(3);
			cellConsulta.setCellValue("Isdn");
			cellConsulta.setCellStyle(styleHeader);

			cellConsulta = rowConsulta.createCell(4);
			cellConsulta.setCellValue("Login");
			cellConsulta.setCellStyle(styleHeader);

			cellConsulta = rowConsulta.createCell(5);
			cellConsulta.setCellValue("Servicio");
			cellConsulta.setCellStyle(styleHeader);

			cellConsulta = rowConsulta.createCell(6);
			cellConsulta.setCellValue("Metodo");
			cellConsulta.setCellStyle(styleHeader);

			cellConsulta = rowConsulta.createCell(7);
			cellConsulta.setCellValue("Request");
			cellConsulta.setCellStyle(styleHeader);

			cellConsulta = rowConsulta.createCell(8);
			cellConsulta.setCellValue("Response");
			cellConsulta.setCellStyle(styleHeader);

			cellConsulta = rowConsulta.createCell(9);
			cellConsulta.setCellValue("Fecha Creacion");
			cellConsulta.setCellStyle(styleHeader);

			cellConsulta = rowConsulta.createCell(10);
			cellConsulta.setCellValue("Tiempo Conexion");
			cellConsulta.setCellStyle(styleHeader);

			cellConsulta = rowConsulta.createCell(11);
			cellConsulta.setCellValue("Tiempo Respuesta");
			cellConsulta.setCellStyle(styleHeader);

			List<VcConsulta> consultas = consultaBL.findAll(idAuditoria);
			if (consultas != null) {
				int nroRow=0;
				for (int k = 0; k < consultas.size(); k++) {
					rowConsulta = sheetConsulta.createRow((short) nroRow + 1);
					rowConsulta.setHeight((short) 2000);
					VcConsulta item = consultas.get(k);
					int nroRowAddReq = 0;
					int nroRowAddRes = 0;

					cellConsulta = rowConsulta.createCell(0);
					cellConsulta.setCellValue(String.valueOf((item.getIdConsulta())));
					cellConsulta.setCellStyle(styleRow);

					cellConsulta = rowConsulta.createCell(1);
					cellConsulta.setCellValue(String.valueOf(item.getVcLogAuditoria().getIdLogAuditoria()));
					cellConsulta.setCellStyle(styleRow);

					cellConsulta = rowConsulta.createCell(2);
					if (item.getVcHilo() != null)
						cellConsulta.setCellValue(String.valueOf(item.getVcHilo().getIdHilo()));
					else
						cellConsulta.setCellValue("");
					cellConsulta.setCellStyle(styleRow);

					cellConsulta = rowConsulta.createCell(3);
					if (item.getIsdn() != null)
						cellConsulta.setCellValue(item.getIsdn());
					else
						cellConsulta.setCellValue("");
					cellConsulta.setCellStyle(styleRow);

					cellConsulta = rowConsulta.createCell(4);
					if (item.getLogin() != null)
						cellConsulta.setCellValue(item.getLogin());
					else
						cellConsulta.setCellValue("");
					cellConsulta.setCellStyle(styleRow);

					cellConsulta = rowConsulta.createCell(5);
					cellConsulta.setCellValue(item.getServicio());
					cellConsulta.setCellStyle(styleRow);

					cellConsulta = rowConsulta.createCell(6);
					cellConsulta.setCellValue(item.getMetodo());
					cellConsulta.setCellStyle(styleRow);

					cellConsulta = rowConsulta.createCell(7);
					if (item.getRequest() != null) {
						if (item.getRequest().length() >= 32760) {
							nroRowAddReq = (int) (Math.ceil((double)item.getRequest().length() / (double)32760))-1;
							cellConsulta.setCellValue(item.getRequest().substring(0, 32760));
						} else
							cellConsulta.setCellValue(item.getRequest());
					} else {
						cellConsulta.setCellValue("");
					}
					cellConsulta.setCellStyle(styleRow);

					cellConsulta = rowConsulta.createCell(8);
					if (item.getResponse() != null) {
						if (item.getResponse().length() >= 32760) {
							nroRowAddRes = (int) (Math.ceil((double)item.getResponse().length() / (double)32760))-1;
							cellConsulta.setCellValue(item.getResponse().substring(0, 32760));
						} else
							cellConsulta.setCellValue(item.getResponse());
					} else {
						cellConsulta.setCellValue("");
					}
					cellConsulta.setCellStyle(styleRow);

					cellConsulta = rowConsulta.createCell(9);
					cellConsulta.setCellValue(UtilDate.dateToString(item.getFechaCreacion().getTime(), Parametros.fechaFormatPage));
					cellConsulta.setCellStyle(styleRow);

					cellConsulta = rowConsulta.createCell(10);
					if (item.getTiempoConexion() != null)
						cellConsulta.setCellValue(String.valueOf(item.getTiempoConexion()));
					else
						cellConsulta.setCellValue("");
					cellConsulta.setCellStyle(styleRow);

					cellConsulta = rowConsulta.createCell(11);
					if (item.getTiempoRespuesta() != null)
						cellConsulta.setCellValue(String.valueOf(item.getTiempoRespuesta()));
					else
						cellConsulta.setCellValue("");
					cellConsulta.setCellStyle(styleRow);

					if (nroRowAddReq > 0 || nroRowAddRes > 0) {
						int cantRow = 0;
						
						if (nroRowAddReq < nroRowAddRes)
							cantRow = nroRowAddRes;
						else
							cantRow = nroRowAddReq;
						for (int i = 1; i <= cantRow; i++) {
							nroRow++;
							rowConsulta = sheetConsulta.createRow((short) nroRow + 1);
							rowConsulta.setHeight((short) 2000);

							cellConsulta = rowConsulta.createCell(0);
							cellConsulta.setCellValue("");
							cellConsulta.setCellStyle(styleRow);

							cellConsulta = rowConsulta.createCell(1);
							cellConsulta.setCellValue("");
							cellConsulta.setCellStyle(styleRow);

							cellConsulta = rowConsulta.createCell(2);
							cellConsulta.setCellValue("");
							cellConsulta.setCellStyle(styleRow);

							cellConsulta = rowConsulta.createCell(3);
							cellConsulta.setCellValue("");
							cellConsulta.setCellStyle(styleRow);

							cellConsulta = rowConsulta.createCell(4);
							cellConsulta.setCellValue("");
							cellConsulta.setCellStyle(styleRow);

							cellConsulta = rowConsulta.createCell(5);
							cellConsulta.setCellValue("");
							cellConsulta.setCellStyle(styleRow);

							cellConsulta = rowConsulta.createCell(6);
							cellConsulta.setCellValue("");
							cellConsulta.setCellStyle(styleRow);

							cellConsulta = rowConsulta.createCell(7);
							if (i <= nroRowAddReq) {
								int lfin = (i * 32760) + 32760;
								if (lfin > item.getRequest().length())
									lfin = item.getRequest().length();
								cellConsulta.setCellValue(item.getRequest().substring((i * 32760), lfin));
							}
							cellConsulta.setCellStyle(styleRow);

							cellConsulta = rowConsulta.createCell(8);
							if (i <= nroRowAddRes) {
								int lfin = (i * 32760) + 32760;
								if (lfin > item.getResponse().length())
									lfin = item.getResponse().length();
								cellConsulta.setCellValue(item.getResponse().substring((i * 32760), lfin));
							}
							cellConsulta.setCellStyle(styleRow);

							cellConsulta = rowConsulta.createCell(9);
							cellConsulta.setCellValue("");
							cellConsulta.setCellStyle(styleRow);

							cellConsulta = rowConsulta.createCell(10);
							cellConsulta.setCellValue("");
							cellConsulta.setCellStyle(styleRow);

							cellConsulta = rowConsulta.createCell(11);
							cellConsulta.setCellValue("");
							cellConsulta.setCellStyle(styleRow);

						}
					}
					nroRow++;
				}
			}
			
			// VC Consulta Red
			Sheet sheetConsultaRed = wb.createSheet("Elementos Red");
			rowConsultaRed = sheetConsultaRed.createRow((short) 0);

			sheetConsultaRed.setColumnWidth(6, 100 * 200);
			// AUDITORIAS
			cellConsultaRed = rowConsultaRed.createCell(0);
			cellConsultaRed.setCellValue("Id Conexion");
			cellConsultaRed.setCellStyle(styleHeader);
			
			cellErrores = rowConsultaRed.createCell(1);
			cellErrores.setCellValue("Id Auditoria");
			cellErrores.setCellStyle(styleHeader);

			cellConsultaRed = rowConsultaRed.createCell(2);
			cellConsultaRed.setCellValue("Isdn");
			cellConsultaRed.setCellStyle(styleHeader);

			cellConsultaRed = rowConsultaRed.createCell(3);
			cellConsultaRed.setCellValue("Login");
			cellConsultaRed.setCellStyle(styleHeader);
			
			cellConsultaRed = rowConsultaRed.createCell(4);
			cellConsultaRed.setCellValue("Ip");
			cellConsultaRed.setCellStyle(styleHeader);
			
			cellConsultaRed = rowConsultaRed.createCell(5);
			cellConsultaRed.setCellValue("Tipo");
			cellConsultaRed.setCellStyle(styleHeader);
			
			cellConsultaRed = rowConsultaRed.createCell(6);
			cellConsultaRed.setCellValue("Trama");
			cellConsultaRed.setCellStyle(styleHeader);
			
			cellConsultaRed = rowConsultaRed.createCell(7);
			cellConsultaRed.setCellValue("Fecha Creacion");
			cellConsultaRed.setCellStyle(styleHeader);
			

			List<VcConsultaRed> consultaRed = consultaRedBL.findAll(idAuditoria);
			if (consultaRed != null) {
				int nroRow=0;
				for (int k = 0; k < consultaRed.size(); k++) {
					rowConsultaRed = sheetConsultaRed.createRow((short) nroRow + 1);
					rowConsultaRed.setHeight((short) 2000);
					VcConsultaRed item = consultaRed.get(k);
					int nroRowAdd = 0;
					cellConsultaRed = rowConsultaRed.createCell(0);
					cellConsultaRed.setCellValue(String.valueOf((item.getIdConsulta())));
					cellConsultaRed.setCellStyle(styleRow);

					cellConsultaRed = rowConsultaRed.createCell(1);
					cellConsultaRed.setCellValue(String.valueOf((item.getVcLogAuditoria().getIdLogAuditoria())));
					cellConsultaRed.setCellStyle(styleRow);
					
					cellConsultaRed = rowConsultaRed.createCell(2);
					if (item.getIsdn() != null)
						cellConsultaRed.setCellValue(item.getIsdn());
					else
						cellConsultaRed.setCellValue("");
					cellConsultaRed.setCellStyle(styleRow);

					cellConsultaRed = rowConsultaRed.createCell(3);
					if (item.getLogin() != null)
						cellConsultaRed.setCellValue(item.getLogin());
					else
						cellConsultaRed.setCellValue("");
					cellConsultaRed.setCellStyle(styleRow);
					
					cellConsultaRed = rowConsultaRed.createCell(4);
					cellConsultaRed.setCellValue(String.valueOf(item.getIp()));
					cellConsultaRed.setCellStyle(styleRow);
					
					cellConsultaRed = rowConsultaRed.createCell(5);
					cellConsultaRed.setCellValue(String.valueOf(item.getTipo()));
					cellConsultaRed.setCellStyle(styleRow);
					
					
					
					cellConsultaRed = rowConsultaRed.createCell(6);
					if (item.getTrama() != null) {
						if (item.getTrama().length() >= 5000) {
							nroRowAdd = (int) (Math.ceil((double)item.getTrama().length() / (double)5000))-1;
							cellConsultaRed.setCellValue(item.getTrama().substring(0, 5000));
						} else
							cellConsultaRed.setCellValue(item.getTrama());
					} else {
						cellConsultaRed.setCellValue("");
					}
					cellConsultaRed.setCellStyle(styleRow);
					
					cellConsultaRed = rowConsultaRed.createCell(7);
					cellConsultaRed.setCellValue(UtilDate.dateToString(item.getFechaCreacion().getTime(), Parametros.fechaFormatPage));
					cellConsultaRed.setCellStyle(styleRow);
					
					if (nroRowAdd > 0) {
						for (int i = 1; i <= nroRowAdd; i++) {
							nroRow++;
							rowConsultaRed = sheetConsultaRed.createRow((short) nroRow + 1);
							rowConsultaRed.setHeight((short) 1000);
							
							
							cellConsultaRed = rowConsultaRed.createCell(0);
							cellConsultaRed.setCellValue("");
							cellConsultaRed.setCellStyle(styleRow);

							cellConsultaRed = rowConsultaRed.createCell(1);
							cellConsultaRed.setCellValue("");
							cellConsultaRed.setCellStyle(styleRow);

							cellConsultaRed = rowConsultaRed.createCell(2);
							cellConsultaRed.setCellValue("");
							cellConsultaRed.setCellStyle(styleRow);
							
							cellConsultaRed = rowConsultaRed.createCell(3);
							cellConsultaRed.setCellValue("");
							cellConsultaRed.setCellStyle(styleRow);
							
							cellConsultaRed = rowConsultaRed.createCell(4);
							cellConsultaRed.setCellValue("");
							cellConsultaRed.setCellStyle(styleRow);
							
							cellConsultaRed = rowConsultaRed.createCell(5);
							cellConsultaRed.setCellValue("");
							cellConsultaRed.setCellStyle(styleRow);
							
							cellConsultaRed = rowConsultaRed.createCell(6);
							if (i <= nroRowAdd) {
								int lfin = (i * 5000) + 5000;
								if (lfin > item.getTrama().length())
									lfin = item.getTrama().length();
								cellConsultaRed.setCellValue(item.getTrama().substring((i * 5000), lfin));
							}
							cellConsultaRed.setCellStyle(styleRow);
							
							
							cellConsultaRed = rowConsultaRed.createCell(7);
							cellConsultaRed.setCellValue("");
							cellConsultaRed.setCellStyle(styleRow);

							
						}
					}
					nroRow++;
				}
			}
			
			//Fin consultaRed
			
			
			// VC LOG EXCEPCIONES
			Sheet sheetErrores = wb.createSheet("Log ExcepcionGenerica");
			rowErrores = sheetErrores.createRow((short) 0);

			sheetErrores.setColumnWidth(3, 100 * 280);
			// AUDITORIAS
			cellErrores = rowErrores.createCell(0);
			cellErrores.setCellValue("Id Excepcion");
			cellErrores.setCellStyle(styleHeader);

			cellErrores = rowErrores.createCell(1);
			cellErrores.setCellValue("Id Auditoria");
			cellErrores.setCellStyle(styleHeader);

			cellErrores = rowErrores.createCell(2);
			cellErrores.setCellValue("Fecha Creacion");
			cellErrores.setCellStyle(styleHeader);

			cellErrores = rowErrores.createCell(3);
			cellErrores.setCellValue("Excepcion");
			cellErrores.setCellStyle(styleHeader);

			List<VcLogExcepcion> errores = logExepcionBL.findAll(idAuditoria);
			if (errores != null) {
				int nroRow=0;
				for (int k = 0; k < errores.size(); k++) {
					rowErrores = sheetErrores.createRow((short) nroRow + 1);
					rowErrores.setHeight((short) 1000);
					VcLogExcepcion item = errores.get(k);
					int nroRowAdd = 0;
					cellErrores = rowErrores.createCell(0);
					cellErrores.setCellValue(String.valueOf((item.getIdLogExcepcion())));
					cellErrores.setCellStyle(styleRow);

					cellErrores = rowErrores.createCell(1);
					cellErrores.setCellValue(String.valueOf((item.getVcLogAuditoria().getIdLogAuditoria())));
					cellErrores.setCellStyle(styleRow);

					cellErrores = rowErrores.createCell(2);
					cellErrores.setCellValue(UtilDate.dateToString(item.getFechaCreacion().getTime(), Parametros.fechaFormatPage));
					cellErrores.setCellStyle(styleRow);

					cellErrores = rowErrores.createCell(3);
					if (item.getExcepcion() != null) {
						if (item.getExcepcion().length() >= 32760){
							nroRowAdd = (int) (Math.ceil((double)item.getExcepcion().length() / (double)32760))-1;
							cellErrores.setCellValue(item.getExcepcion().substring(0, 32760));
						}
						else
							cellErrores.setCellValue(item.getExcepcion());

					} else
						cellErrores.setCellValue("");
					cellErrores.setCellStyle(styleRow);
					
					
					if (nroRowAdd > 0) {
						for (int i = 1; i <= nroRowAdd; i++) {
							nroRow++;
							rowErrores = sheetErrores.createRow((short) nroRow + 1);
							rowErrores.setHeight((short) 1000);
							
							
							cellErrores = rowErrores.createCell(0);
							cellErrores.setCellValue("");
							cellErrores.setCellStyle(styleRow);

							cellErrores = rowErrores.createCell(1);
							cellErrores.setCellValue("");
							cellErrores.setCellStyle(styleRow);

							cellErrores = rowErrores.createCell(2);
							cellErrores.setCellValue("");
							cellErrores.setCellStyle(styleRow);

							cellErrores = rowErrores.createCell(3);
							int lfin = (i * 32760) + 32760;
							if (lfin > item.getExcepcion().length())
								lfin = item.getExcepcion().length();
							cellConsulta.setCellValue(item.getExcepcion().substring((i * 32760), lfin));
							cellErrores.setCellStyle(styleRow);
						}
					}
					nroRow++;
				}
			}
		

			// String excelFileName = "datosAuditoria.xls";
			temp = File.createTempFile("pattern" + (new Date()).getTime(), ".suffix");

			FileOutputStream fos = new FileOutputStream(temp);

			for (int j = 0; j < 8; j++) {
				sheet.autoSizeColumn((short) j);
			}

			for (int j = 0; j < 12; j++) {
				if (j != 7 && j != 8)
					sheetConsulta.autoSizeColumn((short) j);
			}

			for (int j = 0; j < 5; j++) {
				sheetAdicional.autoSizeColumn((short) j);
			}

			for (int j = 0; j < 3; j++) {
				sheetErrores.autoSizeColumn((short) j);
			}
			
			for (int j = 0; j < 8; j++) {
				sheetConsultaRed.autoSizeColumn((short) j);
			}
			wb.write(fos);
			fos.flush();
			fos.close();

			String filename = "Incidente Nro. " + au.getId() + " - " + UtilDate.dateToString(new Date(), "yyyyMMdd HHmmss") + ".xls";
			stream = new BufferedInputStream(new FileInputStream(temp));

			FacesContext fc = FacesContext.getCurrentInstance();
			HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
			response.setContentType("application/vnd.ms-excel");
			response.addHeader("Content-disposition", "attachment; filename=\"" + filename + "\"");
			os = response.getOutputStream();
			byte[] buffer = new byte[1024];
			int read = stream.read(buffer);

			while (read >= 0) {
				if (read > 0) {
					os.write(buffer, 0, read);
				}
				read = stream.read(buffer);
			}

			log.info("Se hizo exportacion a excel para el incidente:" + au.getIdAuditoria() + ", con " + auditorias.size() + " registros de auditoria, " + adicionales.size() + " datos adicionales, "
					+ consultas.size() + " consultas CBS, " + errores.size() + " Logs de ExcepcionGenerica");
			controlerBitacora.accion(DescriptorBitacora.INCIDENTE, "Se hizo exportacion a excel de Incidente con  ID INCIDENTE: " + au.getId() + ", ID AUDITORIA: " + au.getIdAuditoria());
		} catch (Exception e) {
			log.error("Error al exportar Incidentes a excel:" + e);
			SysMessage.error("Error al exportar Incidentes a excel:" + e.getLocalizedMessage(), null);
		} finally {
			if (temp != null) {
				temp.deleteOnExit();
			}
			if (stream != null) {
				try {
					stream.close();
				} catch (IOException e) {
				}
			}
			if (os != null) {
				try {
					os.close();
				} catch (IOException e) {
				}
			}
			FacesContext.getCurrentInstance().responseComplete();
			FacesContext.getCurrentInstance().renderResponse();
		}
	}

}
