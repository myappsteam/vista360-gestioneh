package com.myapps.vista_ccc.bean.model;

import java.io.Serializable;

public class DaysExpired implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String billetera;
	private String daysExpired;
	
	public DaysExpired() {
		super();
	}
	
	public DaysExpired(String billetera, String daysExpired) {
		super();
		this.billetera = billetera;
		this.daysExpired = daysExpired;
	}
	
	public String getBilletera() {
		return billetera;
	}

	public void setBilletera(String billetera) {
		this.billetera = billetera;
	}

	public String getDaysExpired() {
		return daysExpired;
	}

	public void setDaysExpired(String daysExpired) {
		this.daysExpired = daysExpired;
	}
	
	@Override
	public String toString() {
		return "daysExpired [billetera=" + billetera + ", daysExpired=" + daysExpired + "]";
	}

	
}
