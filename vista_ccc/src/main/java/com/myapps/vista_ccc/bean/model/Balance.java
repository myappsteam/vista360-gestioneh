package com.myapps.vista_ccc.bean.model;

import java.io.Serializable;

public class Balance implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	///// Header Balance
	private int id;
	private String color;
	private String acctKey;
	private String balanceType;
	private String balanceTypeName;
	private String amount;
	private String reservationAmount;
	private String totalBalance;
	private String unitType;
	private String effectiveTime;
	private String expireTime;
	private String statusExpireTime;


	public Balance() {
		super();
	}

	public Balance(int id, String acctKey, String balanceType, String balanceTypeName, String amount, String reservationAmount, String totalBalance, String unitType, String effectiveTime, String expireTime, String statusExpireTime) {
		super();
		this.id = id;
		this.acctKey = acctKey;
		this.balanceType = balanceType;
		this.balanceTypeName = balanceTypeName;
		this.amount = amount;
		this.reservationAmount = reservationAmount;
		this.totalBalance = totalBalance;
		this.unitType = unitType;
		this.effectiveTime = effectiveTime;
		this.expireTime = expireTime;
		this.statusExpireTime=statusExpireTime;
	}

	public String getAcctKey() {
		return acctKey;
	}

	public void setAcctKey(String acctKey) {
		this.acctKey = acctKey;
	}

	public String getBalanceType() {
		return balanceType;
	}

	public void setBalanceType(String balanceType) {
		this.balanceType = balanceType;
	}

	public String getBalanceTypeName() {
		return balanceTypeName;
	}

	public void setBalanceTypeName(String balanceTypeName) {
		this.balanceTypeName = balanceTypeName;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getReservationAmount() {
		return reservationAmount;
	}

	public void setReservationAmount(String reservationAmount) {
		this.reservationAmount = reservationAmount;
	}

	public String getTotalBalance() {
		return totalBalance;
	}

	public void setTotalBalance(String totalBalance) {
		this.totalBalance = totalBalance;
	}

	public String getUnitType() {
		return unitType;
	}

	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}

	public String getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(String expireTime) {
		this.expireTime = expireTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEffectiveTime() {
		return effectiveTime;
	}

	public void setEffectiveTime(String effectiveTime) {
		this.effectiveTime = effectiveTime;
	}

	@Override
	public String toString() {
		return "Balance [id=" + id + ", acctKey=" + acctKey + ", balanceType=" + balanceType + ", balanceTypeName=" + balanceTypeName + ", amount=" + amount + ", reservationAmount=" + reservationAmount + ", totalBalance=" + totalBalance + ", unitType=" + unitType + ", effectiveTime=" + effectiveTime + ", expireTime=" + expireTime + "]";
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getStatusExpireTime() {
		return statusExpireTime;
	}

	public void setStatusExpireTime(String statusExpireTime) {
		this.statusExpireTime = statusExpireTime;
	}
}
