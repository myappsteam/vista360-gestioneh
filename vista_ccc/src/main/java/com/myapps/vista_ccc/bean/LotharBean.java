package com.myapps.vista_ccc.bean;

import com.myapps.vista_ccc.servicio.Servicio;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.SysMessage;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;

@ManagedBean
@ViewScoped
public class LotharBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(LotharBean.class);

	private String pageCallingCircleConsultar;
	private String pageCallingCircleCrearGrupo;
	private String pageCallingCircleBorrarGrupo;
	private String pageCallingCircleAgregarMiembro;
	private String pageCallingCircleCambiarTarifaMiembro;
	private String pageCallingCircleBorrarMiembro;

	private String pageCugConsultar;
	private String pageCugCrearGrupo;
	private String pageCugBorrarGrupo;
	private String pageCugAgregarMiembro;
	private String pageCugCambiarTipoMiembro;
	private String pageCugBorrarMiembro;

	@PostConstruct
	public void init() {
		iniciar();
	}

	public void iniciar() {
		try {
			Servicio.setPagina(this.getClass().getSimpleName());
			Servicio.eliminarConexionesInactivas(this.getClass().getSimpleName());

			// setPageCallingCircleConsultar("http://172.29.84.22:8080/calling-circle-admin/querycallingcircle.html?usuario=maitaj");
			// <iframe id="imagepgframe" name="imagepgframe" frameborder="0" scrolling="auto" src="https://localhost:8444/tomcat_test/">

			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			String login = (String) request.getSession().getAttribute("TEMP$USER_NAME");
			// login = "maitaj";

			String url = Parametros.lotharProtocolo + "://" + Parametros.lotharIp + ":" + Parametros.lotharPuerto + "/" + Parametros.lotharContexto + "/";

			pageCallingCircleConsultar = (url + Parametros.lotharCallingCirclePageConsultar).replace("[user]", login);
			pageCallingCircleCrearGrupo = (url + Parametros.lotharCallingCirclePageCrearGrupo).replace("[user]", login);
			pageCallingCircleBorrarGrupo = (url + Parametros.lotharCallingCirclePageBorrarGrupo).replace("[user]", login);
			pageCallingCircleAgregarMiembro = (url + Parametros.lotharCallingCirclePageAgregarMiembro).replace("[user]", login);
			pageCallingCircleCambiarTarifaMiembro = (url + Parametros.lotharCallingCirclePageCambiarTarifaMiembro).replace("[user]", login);
			pageCallingCircleBorrarMiembro = (url + Parametros.lotharCallingCirclePageBorrarMiembro).replace("[user]", login);

			pageCugConsultar = (url + Parametros.lotharCugPageConsultar).replace("[user]", login);
			pageCugCrearGrupo = (url + Parametros.lotharCugPageCrearGrupo).replace("[user]", login);
			pageCugBorrarGrupo = (url + Parametros.lotharCugPageBorrarGrupo).replace("[user]", login);
			pageCugAgregarMiembro = (url + Parametros.lotharCugPageAgregarMiembro).replace("[user]", login);
			pageCugCambiarTipoMiembro = (url + Parametros.lotharCugPageCambiarTipoMiembro).replace("[user]", login);
			pageCugBorrarMiembro = (url + Parametros.lotharCugPageBorrarMiembro).replace("[user]", login);

			log.debug("pageCallingCircleConsultar: " + pageCallingCircleConsultar);
			log.debug("pageCallingCircleCrearGrupo: " + pageCallingCircleCrearGrupo);
			log.debug("pageCallingCircleBorrarGrupo: " + pageCallingCircleBorrarGrupo);
			log.debug("pageCallingCircleAgregarMiembro: " + pageCallingCircleAgregarMiembro);
			log.debug("pageCallingCircleCambiarTarifaMiembro: " + pageCallingCircleCambiarTarifaMiembro);
			log.debug("pageCallingCircleBorrarMiembro: " + pageCallingCircleBorrarMiembro);

			log.debug("pageCugConsultar: " + pageCugConsultar);
			log.debug("pageCugCrearGrupo: " + pageCugCrearGrupo);
			log.debug("pageCugBorrarGrupo: " + pageCugBorrarGrupo);
			log.debug("pageCugAgregarMiembro: " + pageCugAgregarMiembro);
			log.debug("pageCugCambiarTipoMiembro: " + pageCugCambiarTipoMiembro);
			log.debug("pageCugBorrarMiembro: " + pageCugBorrarMiembro);

		} catch (Exception e) {
			log.error("Error al iniciar: ", e);
			SysMessage.error("Error al iniciar: " + e.getMessage(), null);
		}
	}

	public String getPageCallingCircleConsultar() {
		return pageCallingCircleConsultar;
	}

	public void setPageCallingCircleConsultar(String pageCallingCircleConsultar) {
		this.pageCallingCircleConsultar = pageCallingCircleConsultar;
	}

	public String getPageCallingCircleCrearGrupo() {
		return pageCallingCircleCrearGrupo;
	}

	public void setPageCallingCircleCrearGrupo(String pageCallingCircleCrearGrupo) {
		this.pageCallingCircleCrearGrupo = pageCallingCircleCrearGrupo;
	}

	public String getPageCallingCircleBorrarGrupo() {
		return pageCallingCircleBorrarGrupo;
	}

	public void setPageCallingCircleBorrarGrupo(String pageCallingCircleBorrarGrupo) {
		this.pageCallingCircleBorrarGrupo = pageCallingCircleBorrarGrupo;
	}

	public String getPageCallingCircleAgregarMiembro() {
		return pageCallingCircleAgregarMiembro;
	}

	public void setPageCallingCircleAgregarMiembro(String pageCallingCircleAgregarMiembro) {
		this.pageCallingCircleAgregarMiembro = pageCallingCircleAgregarMiembro;
	}

	public String getPageCallingCircleCambiarTarifaMiembro() {
		return pageCallingCircleCambiarTarifaMiembro;
	}

	public void setPageCallingCircleCambiarTarifaMiembro(String pageCallingCircleCambiarTarifaMiembro) {
		this.pageCallingCircleCambiarTarifaMiembro = pageCallingCircleCambiarTarifaMiembro;
	}

	public String getPageCallingCircleBorrarMiembro() {
		return pageCallingCircleBorrarMiembro;
	}

	public void setPageCallingCircleBorrarMiembro(String pageCallingCircleBorrarMiembro) {
		this.pageCallingCircleBorrarMiembro = pageCallingCircleBorrarMiembro;
	}

	public String getPageCugConsultar() {
		return pageCugConsultar;
	}

	public void setPageCugConsultar(String pageCugConsultar) {
		this.pageCugConsultar = pageCugConsultar;
	}

	public String getPageCugCrearGrupo() {
		return pageCugCrearGrupo;
	}

	public void setPageCugCrearGrupo(String pageCugCrearGrupo) {
		this.pageCugCrearGrupo = pageCugCrearGrupo;
	}

	public String getPageCugBorrarGrupo() {
		return pageCugBorrarGrupo;
	}

	public void setPageCugBorrarGrupo(String pageCugBorrarGrupo) {
		this.pageCugBorrarGrupo = pageCugBorrarGrupo;
	}

	public String getPageCugAgregarMiembro() {
		return pageCugAgregarMiembro;
	}

	public void setPageCugAgregarMiembro(String pageCugAgregarMiembro) {
		this.pageCugAgregarMiembro = pageCugAgregarMiembro;
	}

	public String getPageCugCambiarTipoMiembro() {
		return pageCugCambiarTipoMiembro;
	}

	public void setPageCugCambiarTipoMiembro(String pageCugCambiarTipoMiembro) {
		this.pageCugCambiarTipoMiembro = pageCugCambiarTipoMiembro;
	}

	public String getPageCugBorrarMiembro() {
		return pageCugBorrarMiembro;
	}

	public void setPageCugBorrarMiembro(String pageCugBorrarMiembro) {
		this.pageCugBorrarMiembro = pageCugBorrarMiembro;
	}

}
