package com.myapps.vista_ccc.bean.model;

import java.io.Serializable;

public class Property implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String propertyName;
	private String propertyValue;
	private String color;

	public Property() {
		super();
	}

	public Property(String propertyName, String propertyValue, String color) {
		super();
		this.propertyName = propertyName;
		this.propertyValue = propertyValue;
		this.color = color;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getPropertyValue() {
		return propertyValue;
	}

	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

}
