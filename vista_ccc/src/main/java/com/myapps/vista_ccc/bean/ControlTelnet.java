package com.myapps.vista_ccc.bean;

import com.myapps.vista_ccc.entity.VcConexionTelnet;

import java.util.ArrayList;
import java.util.List;



public class ControlTelnet {
	private int cantidadHilos;
	
	private String trama;
	private String elementoRedExitoso;
	private String ipExitoso;
	private String nombreElementoRed;
	private List<String> Msjnull;


	

	private List<VcConexionTelnet> successConnections = new ArrayList<VcConexionTelnet>();
	private List<String> errores = new ArrayList<String>();
	
	
	private List<String> ExcepcionServidor = new ArrayList<String>();

	
	
	public List<String> getExcepcionServidor() {
		return ExcepcionServidor;
	}

	public void setExcepcionServidor(List<String> excepcionServidor) {
		ExcepcionServidor = excepcionServidor;
	}

	public List<String> getMsjnull() {
		return Msjnull;
	}

	public void setMsjnull(List<String> msjnull) {
		Msjnull = msjnull;
	}
	public List<String> getErrores() {
		return errores;
	}

	public void setErrores(List<String> errores) {
		this.errores = errores;
	}

	public List<VcConexionTelnet> getSuccessConnections() {
		return successConnections;
	}

	public void setSuccessConnections(List<VcConexionTelnet> successConnections) {
		this.successConnections = successConnections;
	}

	public String getNombreElementoRed() {
		return nombreElementoRed;
	}

	public void setNombreElementoRed(String nombreElementoRed) {
		this.nombreElementoRed = nombreElementoRed;
	}

	public String getIpExitoso() {
		return ipExitoso;
	}

	public void setIpExitoso(String ipExitoso) {
		this.ipExitoso = ipExitoso;
	}

	public String getElementoRedExitoso() {
		return elementoRedExitoso;
	}

	public void setElementoRedExitoso(String elementoRedExitoso) {
		this.elementoRedExitoso = elementoRedExitoso;
	}

	public ControlTelnet() {
		super();
	}

	public synchronized void finalizar() {
		cantidadHilos--;
	}

	public ControlTelnet(int cantidadHilos) {
		super();
		this.cantidadHilos = cantidadHilos;
	}

	public int getCantidadHilos() {
		return cantidadHilos;
	}

	public void setCantidadHilos(int cantidadHilos) {
		this.cantidadHilos = cantidadHilos;
	}

	public boolean finalizaron() {
		if (cantidadHilos == 0)
			return true;
		else
			return false;
	}

	public String getTrama() {
		return trama;
	}

	public void setTrama(String trama) {
		this.trama = trama;
	}

	

	
}
