package com.myapps.vista_ccc.bean.ttp;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Deuda implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;
	// private String propietarioLinea;
	// private String origen;
	// private String destino;
	private String estado;
	private Date fechaEstado;
	private Date fechaPortatacion;
	private Date fechaVencimiento;
	private String estadoPantalla;
	private double deuda;
	// private Date fecha;
	private String corte;
	private String tipo;
	private String serie;
	private String numero;
	private String color;
	private List<DetalleDeuda> listaDetalle;
	private double totalDetalleDeuda;

	public Deuda() {
		super();
	}

	public Deuda(String id, String estado, Date fechaEstado, Date fechaPortatacion, Date fechaVencimiento,
			String estadoPantalla, double deuda, String corte, String tipo, String serie, String numero, String color,
			List<DetalleDeuda> listaDetalle, double totalDetalleDeuda) {
		super();
		this.id = id;
		this.estado = estado;
		this.fechaEstado = fechaEstado;
		this.fechaPortatacion = fechaPortatacion;
		this.fechaVencimiento = fechaVencimiento;
		this.estadoPantalla = estadoPantalla;
		this.deuda = deuda;
		this.corte = corte;
		this.tipo = tipo;
		this.serie = serie;
		this.numero = numero;
		this.color = color;
		this.listaDetalle = listaDetalle;
		this.totalDetalleDeuda = totalDetalleDeuda;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEstado() {
		return estado;
	}

	public Date getFechaEstado() {
		return fechaEstado;
	}

	public void setFechaEstado(Date fechaEstado) {
		this.fechaEstado = fechaEstado;
	}

	public Date getFechaPortatacion() {
		return fechaPortatacion;
	}

	public void setFechaPortatacion(Date fechaPortatacion) {
		this.fechaPortatacion = fechaPortatacion;
	}

	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCorte() {
		return corte;
	}

	public void setCorte(String corte) {
		this.corte = corte;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public double getDeuda() {
		return deuda;
	}

	public void setDeuda(double deuda) {
		this.deuda = deuda;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public List<DetalleDeuda> getListaDetalle() {
		return listaDetalle;
	}

	public void setListaDetalle(List<DetalleDeuda> listaDetalle) {
		this.listaDetalle = listaDetalle;
	}

	public double getTotalDetalleDeuda() {
		return totalDetalleDeuda;
	}

	public void setTotalDetalleDeuda(double totalDetalleDeuda) {
		this.totalDetalleDeuda = totalDetalleDeuda;
	}

	public String getEstadoPantalla() {
		return estadoPantalla;
	}

	public void setEstadoPantalla(String estadoPantalla) {
		this.estadoPantalla = estadoPantalla;
	}

	@Override
	public String toString() {
		return "Deuda [id=" + id + ", estado=" + estado + ", estadoPantalla=" + estadoPantalla + ", deuda=" + deuda
				+ ", corte=" + corte + ", tipo=" + tipo + ", serie=" + serie + ", numero=" + numero + ", color=" + color
				+ ", totalDetalleDeuda=" + totalDetalleDeuda + "]";
	}

}
