package com.myapps.vista_ccc.bean.model;

import java.io.Serializable;
import java.util.Date;

public class Incidente implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;
	private String idAuditoria;
	private Date fechaIncidente;
	private Date fechaAtencion;
	private boolean atendido;
	private String usuario;
	private String vista;

	public Incidente() {
		super();
	}

	public Incidente(String id, String idAuditoria, Date fechaIncidente, Date fechaAtencion, boolean atendido, String usuario, String vista) {
		super();
		this.id = id;
		this.idAuditoria = idAuditoria;
		this.fechaIncidente = fechaIncidente;
		this.fechaAtencion = fechaAtencion;
		this.atendido = atendido;
		this.usuario = usuario;
		this.vista = vista;
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdAuditoria() {
		return idAuditoria;
	}

	public void setIdAuditoria(String idAuditoria) {
		this.idAuditoria = idAuditoria;
	}

	public Date getFechaIncidente() {
		return fechaIncidente;
	}

	public void setFechaIncidente(Date fechaIncidente) {
		this.fechaIncidente = fechaIncidente;
	}

	public Date getFechaAtencion() {
		return fechaAtencion;
	}

	public void setFechaAtencion(Date fechaAtencion) {
		this.fechaAtencion = fechaAtencion;
	}

	public boolean getAtendido() {
		return atendido;
	}

	public void setAtendido(boolean atendido) {
		this.atendido = atendido;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getVista() {
		return vista;
	}

	public void setVista(String vista) {
		this.vista = vista;
	}

	// @Override
	// public String toString() {
	// return "History [id=" + id + ", isdn=" + isdn + ", categoria=" + categoria + ", tipoFlujo=" + tipoFlujo + ", tipoServicio=" + tipoServicio + ", descripcionServicio=" + descripcionServicio + ",
	// startDate=" + startDate + ", cdrSecuencia=" + cdrSecuencia + ", otherNumber=" + otherNumber + ", roamFlag=" + roamFlag + ", callingCellId=" + callingCellId + ", calledCellId=" + calledCellId
	// + ", specialNumber=" + specialNumber + ", endDate=" + endDate + ", billCycleId=" + billCycleId + ", measureUnit=" + measureUnit + ", startTime=" + startTime + ", endTime=" + endTime + ",
	// refundIndicator=" + refundIndicator + ", actualVolume=" + actualVolume + ", ratingVolume=" + ratingVolume + ", freeVolume=" + freeVolume + ", measureUnitCdr=" + measureUnitCdr
	// + ", actualChargeAmt=" + actualChargeAmt + ", freeChargeAmt=" + freeChargeAmt + ", currencyIdCdr=" + currencyIdCdr + ", actualTaxCode=" + actualTaxCode + ", actualTaxAmt=" + actualTaxAmt + ",
	// category=" + category + ", primaryIdentity=" + primaryIdentity + ", tradeTime=" + tradeTime + ", remark=" + remark + ", channelId=" + channelId + ", transId=" + transId + ", exTransId="
	// + exTransId + ", reCategory=" + reCategory + ", reTradeTime=" + reTradeTime + ", rePrimaryIdentity=" + rePrimaryIdentity + ", rechargeType=" + rechargeType + ", rechargeChannelId=" +
	// rechargeChannelId + ", rechargeAmount=" + rechargeAmount + ", currencyId=" + currencyId + ", cardSequence=" + cardSequence + ", rechargeReason=" + rechargeReason + ", resultCode=" + resultCode
	// + ", reTransId=" + reTransId + ", reExTransId=" + reExTransId + ", exRechargeType=" + exRechargeType + ", rechargeTax=" + rechargeTax + ", rechargePenalty=" + rechargePenalty + ",
	// reversalFlag=" + reversalFlag + ", reversalReason=" + reversalReason + ", reversalTime=" + reversalTime + "]";
	// }

}
