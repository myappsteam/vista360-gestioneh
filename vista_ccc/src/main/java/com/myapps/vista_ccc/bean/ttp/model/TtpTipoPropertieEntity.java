package com.myapps.vista_ccc.bean.ttp.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by michael on 7/4/2020.
 */
@Entity
@Table(name = "TTP_TIPO_PROPERTIE")
public class TtpTipoPropertieEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    private long idTipoPropertie;
    private String nombre;
    private String descripcion;

    @Id
    @Column(name = "ID_TIPO_PROPERTIE")
    public long getIdTipoPropertie() {
        return idTipoPropertie;
    }

    public void setIdTipoPropertie(long idTipoPropertie) {
        this.idTipoPropertie = idTipoPropertie;
    }

    @Basic
    @Column(name = "NOMBRE")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "DESCRIPCION")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TtpTipoPropertieEntity that = (TtpTipoPropertieEntity) o;

        if (idTipoPropertie != that.idTipoPropertie) return false;
        if (nombre != null ? !nombre.equals(that.nombre) : that.nombre != null) return false;
        if (descripcion != null ? !descripcion.equals(that.descripcion) : that.descripcion != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (idTipoPropertie ^ (idTipoPropertie >>> 32));
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        result = 31 * result + (descripcion != null ? descripcion.hashCode() : 0);
        return result;
    }
}
