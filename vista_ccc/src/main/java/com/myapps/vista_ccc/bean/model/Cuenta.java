package com.myapps.vista_ccc.bean.model;

import java.io.Serializable;

public class Cuenta implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String nroCuenta;
	private String codigoCliente;
	private String nombre;
	private String apellido;
	private String nroIdentidad;
	private String tipoPersona;
	private String nroContrato;
	private String planComercial;
	private String planConsumo;
	private String estadoCuenta;
	private String planComercialSva;
	private String planConsumoSva;
	private String estadoCuentaSva;
	private String cos;
	private String cosEstado;

	public Cuenta() {
		super();
	}

	public Cuenta(String nroCuenta, String codigoCliente, String nombre, String apellido, String nroIdentidad, String tipoPersona, String nroContrato, String planComercial, String planConsumo, String estadoCuenta, String planComercialSva, String planConsumoSva, String estadoCuentaSva, String cos, String cosEstado) {
		super();
		this.nroCuenta = nroCuenta;
		this.codigoCliente = codigoCliente;
		this.nombre = nombre;
		this.apellido = apellido;
		this.nroIdentidad = nroIdentidad;
		this.tipoPersona = tipoPersona;
		this.nroContrato = nroContrato;
		this.planComercial = planComercial;
		this.planConsumo = planConsumo;
		this.estadoCuenta = estadoCuenta;
		this.planComercialSva = planComercialSva;
		this.planConsumoSva = planConsumoSva;
		this.estadoCuentaSva = estadoCuentaSva;
		this.cos = cos;
		this.cosEstado = cosEstado;
	}

	public String getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(String nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public String getCodigoCliente() {
		return codigoCliente;
	}

	public void setCodigoCliente(String codigoCliente) {
		this.codigoCliente = codigoCliente;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNroIdentidad() {
		return nroIdentidad;
	}

	public void setNroIdentidad(String nroIdentidad) {
		this.nroIdentidad = nroIdentidad;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getNroContrato() {
		return nroContrato;
	}

	public void setNroContrato(String nroContrato) {
		this.nroContrato = nroContrato;
	}

	public String getPlanComercial() {
		return planComercial;
	}

	public void setPlanComercial(String planComercial) {
		this.planComercial = planComercial;
	}

	public String getPlanConsumo() {
		return planConsumo;
	}

	public void setPlanConsumo(String planConsumo) {
		this.planConsumo = planConsumo;
	}

	public String getEstadoCuenta() {
		return estadoCuenta;
	}

	public void setEstadoCuenta(String estadoCuenta) {
		this.estadoCuenta = estadoCuenta;
	}

	public String getPlanComercialSva() {
		return planComercialSva;
	}

	public void setPlanComercialSva(String planComercialSva) {
		this.planComercialSva = planComercialSva;
	}

	public String getPlanConsumoSva() {
		return planConsumoSva;
	}

	public void setPlanConsumoSva(String planConsumoSva) {
		this.planConsumoSva = planConsumoSva;
	}

	public String getEstadoCuentaSva() {
		return estadoCuentaSva;
	}

	public void setEstadoCuentaSva(String estadoCuentaSva) {
		this.estadoCuentaSva = estadoCuentaSva;
	}

	public String getCos() {
		return cos;
	}

	public void setCos(String cos) {
		this.cos = cos;
	}

	public String getCosEstado() {
		return cosEstado;
	}

	public void setCosEstado(String cosEstado) {
		this.cosEstado = cosEstado;
	}

	@Override
	public String toString() {
		return "Cuenta [nroCuenta=" + nroCuenta + ", codigoCliente=" + codigoCliente + ", nombre=" + nombre + ", apellido=" + apellido + ", nroIdentidad=" + nroIdentidad + ", tipoPersona=" + tipoPersona + ", nroContrato=" + nroContrato + ", planComercial=" + planComercial + ", planConsumo=" + planConsumo + ", estadoCuenta=" + estadoCuenta + ", planComercialSva=" + planComercialSva
				+ ", planConsumoSva=" + planConsumoSva + ", estadoCuentaSva=" + estadoCuentaSva + ", cos=" + cos + ", cosEstado=" + cosEstado + "]";
	}

}
