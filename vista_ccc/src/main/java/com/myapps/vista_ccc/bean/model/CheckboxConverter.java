package com.myapps.vista_ccc.bean.model;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.io.Serializable;
import java.util.List;

@FacesConverter(value = "checkboxConverter", forClass = Checkbox.class)
public class CheckboxConverter implements Serializable, Converter {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
		// TODO Se llama al guardar
		try {

			if (value != null && value.trim().length() > 0) {

				CheckboxService service = (CheckboxService) fc.getExternalContext().getApplicationMap().get("checkboxService");
				List<Checkbox> list = service.getListCheckbox();
				for (Checkbox cb : list) {
					if (cb.getId().toString().equals(value)) {
						return cb;
					}
				}

				return null;

			} else {
				return null;
			}

		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent uic, Object object) {
		// TODO se llama al inicio, cuando se construye el componente
		if (object != null) {
			if (!object.toString().equals("-1") && !object.toString().trim().isEmpty()) {
				return String.valueOf(((Checkbox) object).getId());
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

}
