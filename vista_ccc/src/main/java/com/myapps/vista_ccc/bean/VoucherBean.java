package com.myapps.vista_ccc.bean;

import com.myapps.user.bean.ControlerBitacora;
import com.myapps.user.ldap.DescriptorBitacora;
import com.myapps.vista_ccc.bean.model.Currency;
import com.myapps.vista_ccc.bean.model.SimpleVoucherOperation;
import com.myapps.vista_ccc.bean.model.Voucher;
import com.myapps.vista_ccc.business.ConsultaBL;
import com.myapps.vista_ccc.business.IncidenteBL;
import com.myapps.vista_ccc.business.LogAuditoriaBL;
import com.myapps.vista_ccc.business.LogExcepcionBL;
import com.myapps.vista_ccc.entity.*;
import com.myapps.vista_ccc.servicio.NodoServicio;
import com.myapps.vista_ccc.servicio.Servicio;
import com.myapps.vista_ccc.servicio.ServicioVoucher;
import com.myapps.vista_ccc.util.*;
import org.apache.axis.AxisFault;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.servlet.http.HttpServletRequest;
import javax.xml.rpc.ServiceException;
import java.io.CharArrayWriter;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.URL;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.*;


@ManagedBean
@ViewScoped
public class VoucherBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(VoucherBean.class);


	@Inject
	private LogAuditoriaBL blAuditoria;

	@Inject
	private ConsultaBL blConsulta;
	
	@Inject
	private IncidenteBL blIncidente;
	
	@Inject
	private LogExcepcionBL blExcepcion;

	@Inject
	private ControlerBitacora controlerBitacora;

	private boolean disabledButton;
	
	private String login;
	private String ip;
	private long CodigoAuditoria;
	private boolean disableIncidente;
	
	// VOUCHER
	private String batchNumber;
	private String serialNumber;
	private Voucher voucher;
	private ArrayList<SimpleVoucherOperation> listVoucherOperation;
	private HashMap<String, String> hashOperationType;
	private HashMap<String, String> hashCardState;
	private HashMap<String, String> hashCurrency;

	@PostConstruct
	public void init() {
		try {
			// log.debug("iniciaando
			// postconstruct....................................................");
			inicializando();
			//controlButton = new ControlButton(false, false, true);

		} catch (Exception e) {
			log.error("Error al iniciar, " + e);
		}
	}

	public void inicializando() {
		//log.debug("iniciando voucher");
		try {
			Servicio.setPagina(this.getClass().getSimpleName());
			Servicio.eliminarConexionesInactivas(this.getClass().getSimpleName());

			voucher = new Voucher();

			disabledButton = false;
			
			CodigoAuditoria=0;
			disableIncidente=true;
			
			listVoucherOperation = new ArrayList<SimpleVoucherOperation>();
			batchNumber = "";
			serialNumber = "";
			cargarIp();
			obtenerUsuario();
			cargarHashCardStates();
			cargarHashOperationTypes();
			loadCurrency(Parametros.configFile + "currency.xml");
		} catch (Exception e) {
			log.error("Error al iniciar, ", e);
		}
	}


	private void obtenerUsuario() {
		try {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			this.login = (String) request.getSession().getAttribute("TEMP$USER_NAME");
			log.info("login: " + login);
		} catch (Exception e) {
			log.error("Error al obtener usuario: ", e);
		}
	}

	public void limpiar() {
		voucher = new Voucher();
		listVoucherOperation = new ArrayList<SimpleVoucherOperation>();
		batchNumber = "";
		serialNumber = "";
		
		CodigoAuditoria=0;
		disableIncidente=true;
	}

	private void cargarIp() {
		try {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();

			ip = UtilUrl.getClientIp(request);

		} catch (Exception e) {
			log.error("Error al obtener ip: ", e);
		}
	}

	private long guardarAuditoria(Calendar fechaConsulta, String usuario, String ip, String vista, int comando, String isdn, List<VcLogAdicional> adicionales) {
		try {

			VcLogAuditoria item = new VcLogAuditoria();
			item.setFechaConsulta(fechaConsulta);
			item.setIsdn(isdn);
			item.setUsuario(usuario);
			item.setIp(ip);
			item.setVista(vista);
			item.setComando(comando);
			item.setAdicional("Y");
			item.setFechaCreacion(Calendar.getInstance());
			if (adicionales == null || adicionales.size() == 0) {
				item.setAdicional("N");
			}

			blAuditoria.guardarAuditoria(item, adicionales);
			CodigoAuditoria = item.getIdLogAuditoria();
			return item.getIdLogAuditoria();

		} catch (Exception e) {
			log.error("Error al guardar auditoria: ", e);
		}

		return -1;
	}
	
	private void saveXml(String isdn, String login, String servicio, String metodo, String request, String response, long idAuditoria, Long tiempoConexion, Long tiempoRespuesta) {
		try {
			VcConsulta c = new VcConsulta();
			c.setFechaCreacion(Calendar.getInstance());
			c.setIsdn(isdn);
			c.setLogin(login);
			c.setServicio(servicio);
			c.setMetodo(metodo);
			c.setRequest(request);
			c.setResponse(response);
			c.setTiempoConexion(tiempoConexion);
			c.setTiempoRespuesta(tiempoRespuesta);

			
			Object find = blAuditoria.find(idAuditoria, VcLogAuditoria.class);
			c.setVcLogAuditoria((VcLogAuditoria) find);

			blConsulta.save(c);
		} catch (Exception e) {
			log.warn("No se registro en bd el request y response: ", e);
		}
	}

	public void cargarVoucher() {
		log.debug("Iniciando Busqueda");
		long idAuditoria=0;
		try {

			voucher = new Voucher();
			listVoucherOperation = new ArrayList<SimpleVoucherOperation>();

			if (login == null || login.trim().isEmpty()) {
				SysMessage.warn("Usuario no válido", null);
				return;
			}

			if (batchNumber != null && !batchNumber.trim().isEmpty()) {
				if (serialNumber != null && !serialNumber.trim().isEmpty()) {
					
					List<VcLogAdicional> adicionales = new ArrayList<>();
					VcLogAdicional ad1 = new VcLogAdicional();
					ad1.setCodigo("Batch ID");
					ad1.setValor(batchNumber);
					VcLogAdicional ad2 = new VcLogAdicional();
					ad2.setCodigo("Serial No");
					ad2.setValor(serialNumber);
					adicionales.add(ad1);
					adicionales.add(ad2);
					
					disabledButton = true;
					
					idAuditoria = guardarAuditoria(Calendar.getInstance(), this.login, this.ip, Parametros.vistaVoucher, Parametros.comandoVoucher, "N/A", adicionales);

					if (idAuditoria <= 0) {
						SysMessage.error("Error al guardar log de auditoria", null);
						return;
					}
					
					disableIncidente=false;
					
					try {
						controlerBitacora.accion(DescriptorBitacora.VOUCHER, "Se hizo busqueda de Voucher, Batch Number: " + batchNumber + ", Serial Number: " + serialNumber);
					} catch (Exception e) {
						log.error("Error al guardar bitacora en el sistema: ", e);
						SysMessage.error("Error al guardar bitacora", null);
					}

					String wsdlLoc = Parametros.voucherWsdl;

					URL url = new URL(UtilUrl.getIp(wsdlLoc));
					long ini = System.currentTimeMillis();

					if (url.getProtocol().equalsIgnoreCase("https")) {
						int puerto = url.getPort();
						String host = url.getHost();
						if (host.equalsIgnoreCase("localhost")) {
							host = "127.0.0.1";
						}
						validarCertificado(Parametros.voucherPathKeystore, host + ":" + puerto);
					}

					NodoServicio<com.huawei.www.bme.cbsinterface.uvcservices.UvcServicsBindingStub> portNodo = ServicioVoucher.initPort(wsdlLoc, "VOUCHER" + login, Parametros.voucherTimeOut);

					if (portNodo != null && portNodo.getPort() != null) {

						synchronized (portNodo) {
							try {
								com.huawei.www.bme.cbsinterface.uvcservices.UvcServicsBindingStub port = portNodo.getPort();

									com.huawei.www.bme.cbsinterface.uvcheader.OwnershipInfo ownershipInfo = new com.huawei.www.bme.cbsinterface.uvcheader.OwnershipInfo();
									ownershipInfo.setBEID(Integer.parseInt(Parametros.voucherOwnerShipInfoId));
									com.huawei.www.bme.cbsinterface.uvcheader.SecurityInfo accessSecurity = new com.huawei.www.bme.cbsinterface.uvcheader.SecurityInfo();
									accessSecurity.setLoginSystemCode(Parametros.voucherLoginSystemCode);
									accessSecurity.setPassword(Parametros.voucherPassword);
									com.huawei.www.bme.cbsinterface.uvcheader.OperatorInfo operatorInfo = new com.huawei.www.bme.cbsinterface.uvcheader.OperatorInfo();
									operatorInfo.setOperatorID(Parametros.voucherOperatorId);
									com.huawei.www.bme.cbsinterface.uvcheader.RequestHeaderTimeFormat timeFormat = new com.huawei.www.bme.cbsinterface.uvcheader.RequestHeaderTimeFormat();
									timeFormat.setTimeType(Parametros.voucherTimeType);

									SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
									String messageSeq = format.format(new Date());

									com.huawei.www.bme.cbsinterface.uvcheader.RequestHeader requestHeader = new com.huawei.www.bme.cbsinterface.uvcheader.RequestHeader(Parametros.voucherVersion, "", messageSeq, ownershipInfo, accessSecurity, operatorInfo, "", timeFormat);

									com.huawei.www.bme.cbsinterface.uvcservices.QueryVoucherRequest queryVoucherRequest = new com.huawei.www.bme.cbsinterface.uvcservices.QueryVoucherRequest(batchNumber + serialNumber);
									com.huawei.www.bme.cbsinterface.uvcservices.QueryVoucherRequestMsg queryVoucherRequestMsg = new com.huawei.www.bme.cbsinterface.uvcservices.QueryVoucherRequestMsg(requestHeader, queryVoucherRequest);

									com.huawei.www.bme.cbsinterface.uvcservices.QueryVoucherResultMsg queryVoucher = port.queryVoucher(queryVoucherRequestMsg);

									long fin = System.currentTimeMillis();
									Long Con = null;
									if (portNodo.isPrimeraVez()){
										Con=portNodo.getTiempoConexion();
									}
									log.debug("Tiempo de respuesta del servicio UvcServices, queryVoucher: " + (fin - ini) + " milisegundos");

									if (Parametros.saveRequestResponse) {
										try {
											saveXml("N/A", getLogin(), wsdlLoc, "queryVoucher", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoria,Con,(fin - ini));
										} catch (Exception e) {
											log.warn("No se logro registrar los request y response del servicio UvcServices, queryVoucher: ", e);
										}
									}

									if (queryVoucher != null && queryVoucher.getResultHeader() != null) {
										if (queryVoucher.getResultHeader().getResultCode().trim().equals("0")) {
											com.huawei.www.bme.cbsinterface.uvcservices.QueryVoucherResult queryVoucherResult = queryVoucher.getQueryVoucherResult();
											if (queryVoucherResult != null) {
												com.huawei.www.bme.cbsinterface.uvccommon.VoucherInfoType voucherInfo = queryVoucherResult.getVoucherInfo();

												if (voucherInfo != null) {
													voucher.setServiceProvider(Parametros.voucherServiceProvider);

													if (voucherInfo.getFaceValue() != null) {

														double total = UtilNumber.redondear(((double) voucherInfo.getFaceValue() / (double) Parametros.voucherDivisor), Parametros.voucherNroDecimales);
														voucher.setFaceValue(UtilNumber.doubleToString(total, Parametros.voucherNroDecimales));
														// voucher.setFaceValue(voucherInfo.getFaceValue().toString());
													}

													if (voucherInfo.getHotCardFlag() != null)
														voucher.setCardState(hashCardState.get(voucherInfo.getHotCardFlag().toString()));
													if (voucherInfo.getCurrency() != null)
														voucher.setCurrency(getCurrencyDescription(voucherInfo.getCurrency().intValue()));
													if (voucherInfo.getRechargeNumber() != null)
														voucher.setRechargeNumber(voucherInfo.getRechargeNumber());
													if (voucherInfo.getCardCosID() != null)
														voucher.setCardCosId(voucherInfo.getCardCosID());
													if (voucherInfo.getCardCosName() != null)
														voucher.setCardCosName(voucherInfo.getCardCosName());

													if (voucherInfo.getCardStartDate() != null) {
														Date dateCardStart = UtilDate.stringToDate(voucherInfo.getCardStartDate().trim(), "yyyyMMdd");
											
														voucher.setCardStartDate(UtilDate.dateToString(dateCardStart, "dd-MM-yyyy"));
													}
													/// dateExpiration = CardStopDate
													if (voucherInfo.getCardStopDate() != null) {
														Date dateExpiration = UtilDate.stringToDate(voucherInfo.getCardStopDate().trim(), "yyyyMMdd");
														voucher.setExpirationDate(UtilDate.dateToString(dateExpiration, "dd-MM-yyyy"));
													}
													/// Date used = TradeTime
													if (voucherInfo.getTradeTime() != null) {
														Date dateUsed = UtilDate.stringToDate(voucherInfo.getTradeTime().trim(), Parametros.fechaFormatWs);
														voucher.setDateUsed(UtilDate.dateToString(dateUsed, Parametros.fechaFormatPage));
													}

													com.huawei.www.bme.cbsinterface.uvccommon.SimpleVoucherOperationType[] simpleVoucherOperation = queryVoucherResult.getSimpleVoucherOperation();
													if (simpleVoucherOperation != null && simpleVoucherOperation.length > 0) {

														for (int i = 0; i < simpleVoucherOperation.length; i++) {

															try {

																com.huawei.www.bme.cbsinterface.uvccommon.SimpleVoucherOperationType simpleVoucherOperationType = simpleVoucherOperation[i];
																if (simpleVoucherOperationType != null) {
																	Date date = UtilDate.stringToDate(simpleVoucherOperationType.getOperationDate(), Parametros.fechaFormatWs);
																	String operationDate = UtilDate.dateToString(date, Parametros.fechaFormatPage);
																	String operationType = "";

																	if (simpleVoucherOperationType.getOperationType() != null) {
																		operationType = hashOperationType.get(simpleVoucherOperationType.getOperationType().trim());
																		listVoucherOperation.add(new SimpleVoucherOperation(operationType, operationDate, simpleVoucherOperationType.getOperationReason()));
																	}
																}
															} catch (Exception e) {
																log.error("Error al cargar lista de operaciones voucher: ", e);
															}
														}
													}

												} else {
													log.warn("QueryVoucher, voucherInfo resuelto a nulo");
												}
											} else {
												log.warn("QueryVoucher, queryVoucherResult resuelto a nulo");
											}
										} else {
											log.info("QueryVoucher ResultCode: " + queryVoucher.getResultHeader().getResultCode() + ", ResultDesc: " + queryVoucher.getResultHeader().getResultDesc());
											SysMessage.warn("Servicio UvcServices, queryVoucher ResultCode: " + queryVoucher.getResultHeader().getResultCode() + ", ResultDesc: " + queryVoucher.getResultHeader().getResultDesc(), null);
											guardarExcepcion(idAuditoria,"Servicio UvcServices, queryVoucher ResultCode: " + queryVoucher.getResultHeader().getResultCode() + ", ResultDesc: " + queryVoucher.getResultHeader().getResultDesc());
										}
									} else {
										if (queryVoucher != null) {
											log.warn("QueryVoucher queryVoucher resuelto a nulo");
										} else {
											log.warn("QueryVoucher ResultHeader resuelto a nulo");
										}
									}
							} finally {
								portNodo.setEnUso(false);
								portNodo.setFechaFin(new Date());
							}
						}
					} else {
						log.error("Port UvcServices voucher resuelto a nulo");
					}

					SysMessage.info("Consulta finalizada.", null);

				} else {
					SysMessage.warn("El campo Serial Number no es válido", null);
				}
			} else {
				SysMessage.warn("El campo Batch Number no es válido", null);
			}

		} catch (AxisFault a) {
			String msg = a.getMessage();
			msg = msg.toLowerCase();
			if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase()) || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase()) || msg.contains("UnknownHostException".toLowerCase())
					|| msg.contains("No route to host".toLowerCase()) || msg.contains("Connection refused".toLowerCase())) {
				log.error("Error de conexion al servicio UvcServices Voucher: ", a);
				SysMessage.error("Error de conexion del servicio UvcServices", null);
				guardarExcepcion(idAuditoria,"Error de conexion al servicio UvcServices: " + stackTraceToString(a));
			} else {
				log.error("Error AxisFault Voucher: ", a);
				SysMessage.error("Error AxisFault de conexion al servicio UvcServices", null);
				guardarExcepcion(idAuditoria,"Error AxisFault de conexion al servicio UvcServices: " + stackTraceToString(a));
			}
		
		} catch (ServiceException e) {
			voucher = new Voucher();
			log.error("Error de servicio al conectarse al servicio UvcServices Voucher: ", e);
			SysMessage.error("Error de servicio al conectarse al servicio UvcServices", null);
			guardarExcepcion(idAuditoria,"Error de servicio al conectarse al servicio UvcServices" + stackTraceToString(e));
		} catch (RemoteException e) {
			voucher = new Voucher();
			log.error("Error remoto al conectarse al servicio UvcServices Voucher: ", e);
			SysMessage.error("Error remoto al conectarse al servicio UvcServices", null);
			guardarExcepcion(idAuditoria,"Error remoto al conectarse al servicio UvcServices Voucher: " + stackTraceToString(e));
		} catch (Exception e) {
			voucher = new Voucher();
			log.error("Error al consultar UvcServices", e);
			SysMessage.error("Error al consultar UvcServices", null);
			guardarExcepcion(idAuditoria,"Error al consultar UvcServices: " + stackTraceToString(e));
		}
		
	}

	public void guardarIncidente() {
		log.debug("[saveVCIncidente]: Ingresando..");

		try {
			if (CodigoAuditoria > 0) {
				VcLogAuditoria find = (VcLogAuditoria) blAuditoria.find(CodigoAuditoria, VcLogAuditoria.class);

				if (disableIncidente && find != null) {
					SysMessage.warn(Parametros.mensajeValidacionIncidente, null);
					return;
				}
				VcIncidente item = new VcIncidente();
				item.setFechaIncidente(Calendar.getInstance());
				item.setAtencion(false);
				item.setUsuario(this.login);
				item.setVcLogAuditoria(find);
				String str = blIncidente.validar(item, true);
				if (!str.isEmpty()) {
					SysMessage.warn(str, null);
					return;
				}
				blIncidente.save(item);
				controlerBitacora.accion(DescriptorBitacora.VOUCHER, "Se adiciono "+ DescriptorBitacora.INCIDENTE +" con Id: " + String.valueOf(item.getIdIncidente()));
				SysMessage.info("Incidente Nro. " + String.valueOf(item.getIdIncidente()) +": " + Parametros.mensajeReporteIncidente, null);
				disableIncidente = true;
			} else {
				SysMessage.warn(Parametros.mensajeValidacionIncidente, null);
				return;
			}
		} catch (Exception e) {
			log.error("Error al guardar incidente: ", e);
			SysMessage.error("Fallo al guardar en la Base de Datos.", null);
		}

	}
	
	public void guardarExcepcion(long IdAuditoria, String exepcion){
		log.debug("[saveVCExcepcion]: Ingresando..");
		VcLogExcepcion item = new VcLogExcepcion();
		VcLogAuditoria find;
		try {
			find = (VcLogAuditoria) blAuditoria.find(IdAuditoria, VcLogAuditoria.class);
			item.setFechaCreacion(Calendar.getInstance());
			item.setExcepcion(exepcion);
			item.setVcLogAuditoria(find);
			
			blExcepcion.save(item);
		} catch (Exception e) {
			log.error("Error al guardar Excepcion: ", e);
		}
	}
	
	public String stackTraceToString(Throwable e) {
		CharArrayWriter sw = new CharArrayWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		pw.close();
		
	    return sw.toString(); // stack trace a
	}
	
	private void cargarHashCardStates() {
		try {
			hashCardState = new HashMap<String, String>();
			String states = Parametros.voucherCardState;
			StringTokenizer st = new StringTokenizer(states, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				try {
					StringTokenizer stt = new StringTokenizer(nextToken.trim(), ":");
					String key = stt.nextToken();
					String value = stt.nextToken();
					hashCardState.put(key.trim(), value.trim());
				} catch (Exception e) {
					e.getMessage();
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash card states: ", e);
			SysMessage.error("Error al cargar hash card states: " + e.getMessage(), null);
		}
	}

	public boolean isDisableIncidente() {
		return disableIncidente;
	}

	public void setDisableIncidente(boolean disableIncidente) {
		this.disableIncidente = disableIncidente;
	}
	
	
	private void cargarHashOperationTypes() {
		try {
			hashOperationType = new HashMap<String, String>();
			String operations = Parametros.voucherOperationType;
			StringTokenizer st = new StringTokenizer(operations, ",");
			while (st.hasMoreTokens()) {
				String nextToken = st.nextToken();
				try {
					StringTokenizer stt = new StringTokenizer(nextToken.trim(), ":");
					String key = stt.nextToken();
					String value = stt.nextToken();
					// log.debug("key: " + key + ", value: " + value);
					hashOperationType.put(key.trim(), value.trim());
				} catch (Exception e) {
					e.getMessage();
				}
			}
		} catch (Exception e) {
			log.error("Error al cargar hash operation types: ", e);
			SysMessage.error("Error al cargar hash operation types: " + e.getMessage(), null);
		}
	}


	private void validarCertificado(String pathKeystore, String ipPort) {
		try {
			// Properties sysProperties = System.getProperties();
			log.debug("Ingresando a validar certificado pathKeystore: " + pathKeystore + ", ipPort: " + ipPort);

			System.setProperty("javax.net.ssl.trustStore", pathKeystore);
			System.setProperty("java.protocol.handler.pkgs", ipPort);

			HostnameVerifier hv = new HostnameVerifier() {
				public boolean verify(String urlHostName, SSLSession session) {
					// System.out.println("Warning: URL Host: " + urlHostName +
					// " vs. " + session.getPeerHost());
					return true;
				}
			};

			// trustAllHttpsCertificates();
			HttpsURLConnection.setDefaultHostnameVerifier(hv);
		} catch (Exception e) {
			log.error("Error al validar certificado: ", e);
		}
	}

	private String getCurrencyDescription(int currencyId) {
		String description = "";
		description = hashCurrency.get(currencyId + "");
		return description;
	}

	private void loadCurrency(String path) {
		log.debug("Ingresando a cargar monedas");
		try {
			hashCurrency = new HashMap<String, String>();

			ArrayList<Currency> listCurrency = (new ToXml()).readObject(path);
			if (listCurrency != null) {
				for (Currency cu : listCurrency) {
					hashCurrency.put(cu.getId().intValue() + "", cu.getDescription().trim());
				}
			}
			// log.debug("hash map value: " + currencys.get("1049"));
		} catch (Exception e) {
			log.error("Error al cargar archivo de monedas: ", e);
			SysMessage.error("Error al cargar archivo de monedas: " + e.getMessage(), null);
		}
	}
	
	public String getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Voucher getVoucher() {
		return voucher;
	}

	public void setVoucher(Voucher voucher) {
		this.voucher = voucher;
	}

	public ArrayList<SimpleVoucherOperation> getListVoucherOperation() {
		return listVoucherOperation;
	}

	public void setListVoucherOperation(ArrayList<SimpleVoucherOperation> listVoucherOperation) {
		this.listVoucherOperation = listVoucherOperation;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
	
	public boolean isDisabledButton() {
		return disabledButton;
	}

	public void setDisabledButton(boolean disabledButton) {
		this.disabledButton = disabledButton;
	}

}
