package com.myapps.vista_ccc.bean.model;

import java.io.Serializable;


public class LifeCycle implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String statusName;
	private String statusIndex;
	private String statusExpireTime;
	
	public LifeCycle() {
		super();
	}

	public LifeCycle(String statusName, String statusIndex, String statusExpireTime) {
		super();
		this.statusName = statusName;
		this.statusIndex = statusIndex;
		this.statusExpireTime = statusExpireTime;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getStatusIndex() {
		return statusIndex;
	}

	public void setStatusIndex(String statusIndex) {
		this.statusIndex = statusIndex;
	}

	public String getStatusExpireTime() {
		return statusExpireTime;
	}

	public void setStatusExpireTime(String statusExpireTime) {
		this.statusExpireTime = statusExpireTime;
	}
}
