package com.myapps.vista_ccc.bean.model;

import myapps.org.soap.mnppdaportacion.DetallePortacion;

import java.io.Serializable;

public class DetallePortacionPDF implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DetallePortacion detallePortacion;	
	private RespaldoPortacion respaldo;
			
	public DetallePortacion getDetallePortacion() {
		return detallePortacion;
	}
	
	public void setDetallePortacion(DetallePortacion detallePortacion) {
		this.detallePortacion = detallePortacion;
	}
	
	public RespaldoPortacion getRespaldo() {
		return respaldo;
	}
	
	public void setRespaldo(RespaldoPortacion respaldo) {
		this.respaldo = respaldo;
	}	
}
