package com.myapps.vista_ccc.bean.ttp;

import java.io.Serializable;
import java.util.Date;

public class DetalleDeuda implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String producto;
	private Date fecha;
	private double monto;
	private double comision;
	private double pagado;
	private double deuda;
	private long idServicio;
	private String nombreService;
	private String sortBy;

	public DetalleDeuda() {
		super();
	}

	public DetalleDeuda(String producto, Date fecha, double monto, double comision, double pagado, double deuda,
			String sortBy, long idServicio, String nombreService) {
		super();
		this.producto = producto;
		this.fecha = fecha;
		this.monto = monto;
		this.comision = comision;
		this.pagado = pagado;
		this.deuda = deuda;
		this.sortBy = sortBy;
		this.idServicio = idServicio;
		this.nombreService = nombreService;
	}

	public long getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(long idServicio) {
		this.idServicio = idServicio;
	}

	public String getNombreService() {
		return nombreService;
	}

	public void setNombreService(String nombreService) {
		this.nombreService = nombreService;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public double getDeuda() {
		return deuda;
	}

	public void setDeuda(double deuda) {
		this.deuda = deuda;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public double getPagado() {
		return pagado;
	}

	public void setPagado(double pagado) {
		this.pagado = pagado;
	}

	@Override
	public String toString() {
		return "DetalleDeuda [producto=" + producto + ", fecha=" + fecha + ", monto=" + monto + ", comision=" + comision
				+ ", pagado=" + pagado + ", deuda=" + deuda + ", sortBy=" + sortBy + "]";
	}

}
