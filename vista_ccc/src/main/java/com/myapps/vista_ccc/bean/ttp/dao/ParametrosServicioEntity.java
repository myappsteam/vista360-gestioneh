package com.myapps.vista_ccc.bean.ttp.dao;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "TTP_PARAMETROS_SERVICIOS")
public class ParametrosServicioEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@SequenceGenerator(name = "TTP_PARAMETROS_SERVICIOS_TRID_GENERATOR", sequenceName = "SEQ_TTP_PARAMETROS_SERVICIOS", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TTP_PARAMETROS_SERVICIOS_TRID_GENERATOR")
	@Column(name = "ID_PARAMETRO_SERVICIO")
	private long idParametrosServicio;
	@Column(name = "CODE_SERVICE_BCCS")
	private String codeServiceBCCS;
	@Column(name = "SERVICE_ID")
	private String serviceId;
	@Column(name = "SERVICE_NAME")
	private String serviceName;
	@Column(name = "ESTADO")
	private int estado;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_CREACION")
	private Date fechaCreacion;
	@Column(name = "CREADOR_POR")
	private String creadoPor;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_MODIFICACION")
	private Date fechaModificacion;
	@Column(name = "MODIFICADO_POR")
	private String modificadoPor;

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getCreadoPor() {
		return creadoPor;
	}

	public void setCreadoPor(String creadoPor) {
		this.creadoPor = creadoPor;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getModificadoPor() {
		return modificadoPor;
	}

	public void setModificadoPor(String modificadoPor) {
		this.modificadoPor = modificadoPor;
	}

	public long getIdParametrosServicio() {
		return idParametrosServicio;
	}

	public void setIdParametrosServicio(long idParametrosServicio) {
		this.idParametrosServicio = idParametrosServicio;
	}

	public String getCodeServiceBCCS() {
		return codeServiceBCCS;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public void setCodeServiceBCCS(String codeServiceBCCS) {
		this.codeServiceBCCS = codeServiceBCCS;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	@Override
	public String toString() {
		return "ParametrosServicioEntity [idParametrosServicio=" + idParametrosServicio + ", codeServiceBCCS="
				+ codeServiceBCCS + ", serviceId=" + serviceId + ", serviceName=" + serviceName + "]";
	}

}
