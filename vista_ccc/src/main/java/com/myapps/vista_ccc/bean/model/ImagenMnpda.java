package com.myapps.vista_ccc.bean.model;

import java.io.Serializable;

public class ImagenMnpda implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer portacionId;
	private String nombre;
	private String contBase64;

	public ImagenMnpda() {
		super();
		this.nombre = "";

	}

	public Integer getPortacionId() {
		return portacionId;
	}

	public void setPortacionId(Integer portacionId) {
		this.portacionId = portacionId;
	}

	public String getNombre() {
		return nombre;
	}

	public String getContBase64() {
		return contBase64;
	}

	public void setContBase64(String contBase64) {
		this.contBase64 = contBase64;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
