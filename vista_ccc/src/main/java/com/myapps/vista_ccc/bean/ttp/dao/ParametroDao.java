package com.myapps.vista_ccc.bean.ttp.dao;

import com.myapps.vista_ccc.bean.ttp.model.TtpPropertieEntity;
import com.myapps.vista_ccc.dao.MasterDao;
import com.myapps.vista_ccc.dao.MasterDaoInterface;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * Created by michael on 9/4/2020.
 */
public class ParametroDao implements Serializable, MasterDaoInterface {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Inject
    private MasterDao dao;
    @PersistenceContext(unitName = "vista_ccc")
    private transient EntityManager em;

    @Override
    public String validar(Object entidad, boolean nuevo) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void save(Object entidad) throws Exception {
        // TODO Auto-generated method stub
        dao.save(entidad);
    }

    @Override
    public void remove(Object entidad) throws Exception {
        // TODO Auto-generated method stub
        dao.remove(entidad);
    }

    @Override
    public void update(Object entidad) throws Exception {
        dao.update(entidad);
    }

    @Override
    public Object find(Object key, Class clase) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List findAll() throws Exception {
        String sql = "SELECT p FROM TtpPropertieEntity p";
        return dao.findAllQuery(TtpPropertieEntity.class, sql, null);
    }

    public List<ParametrosServicioEntity> findByCodigoAndId(ParametrosServicioEntity p) {
        Query q = em.createQuery(
                "FROM ParametrosServicioEntity p WHERE (p.codeServiceBCCS = ?1 OR p.serviceId = ?2)  AND p.estado = 1");
        q.setParameter(1, p.getCodeServiceBCCS());
        q.setParameter(2, p.getServiceId());
        List<ParametrosServicioEntity> listas = (List<ParametrosServicioEntity>) q.getResultList();
        return listas;
    }

    public HashMap<String, String> getMapAllParameters() {
        HashMap<String, String> p = new HashMap<>();
        Query q = em.createQuery("SELECT p FROM TtpPropertieEntity p");
        List<TtpPropertieEntity> ps = (List<TtpPropertieEntity>) q.getResultList();
        for (TtpPropertieEntity item : ps) {
            p.put(item.getNombre(), item.getValor());
        }
        return p;
    }

    public List<TtpPropertieEntity> findByIdTipoParametro(Long idTipoParametro) {
        Query q = em.createQuery(
                "FROM TtpPropertieEntity p WHERE p.ttpTipoPropertieEntity.idTipoPropertie = ?1");
        q.setParameter(1, idTipoParametro);
        List<TtpPropertieEntity> listas = (List<TtpPropertieEntity>) q.getResultList();
        return listas;
    }
}
