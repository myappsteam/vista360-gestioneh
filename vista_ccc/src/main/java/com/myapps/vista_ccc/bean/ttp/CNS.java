package com.myapps.vista_ccc.bean.ttp;

import java.io.Serializable;

public class CNS implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String tipo;
	private String serie;
	private String numero;

	public CNS() {
		super();
	}

	public CNS(String tipo, String serie, String numero) {
		super();
		this.tipo = tipo;
		this.serie = serie;
		this.numero = numero;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	@Override
	public String toString() {
		return "CNS [tipo=" + tipo + ", serie=" + serie + ", numero=" + numero + "]";
	}

}
