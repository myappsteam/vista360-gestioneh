package com.myapps.vista_ccc.bean;


import com.jcraft.jsch.*;
import com.myapps.vista_ccc.util.Parametros;
import org.apache.log4j.Logger;

import java.io.*;
import java.nio.charset.Charset;

//import com.jcraft.jsch.ChannelExec;
//import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;
//import java.io.BufferedReader;
//import java.io.InputStreamReader;

public class SSHConnector {
	public static Logger log = Logger.getLogger(SSHConnector.class);
	/**
	 * Constante que representa un enter.
	 */
//	private static final String ENTER_KEY = "\n";
	/**
	 * Sesi&oacute;n SSH establecida.
	 */
	Session session = null;
	Channel channel;
	private String dpto;
	private OutputStream out;

	String error = "";

	static ControlTelnet control = new ControlTelnet();

	/**
	 * Establece una conexi&oacute;n SSH.
	 *
	 * @param username
	 *            Nombre de usuario.
	 * @param password
	 *            Contrase&ntilde;a.
	 * @param host
	 *            Host a conectar.
	 * @param port
	 *            Puerto del Host.
	 *
	 * @throws JSchException
	 *             Cualquier error al establecer conexi&oacute;n SSH.
	 * @throws IllegalAccessException
	 *             Indica que ya existe una conexi&oacute;n SSH establecida.
	 */
	public SSHConnector(String username, String password, String host, int port, String dpto, ControlTelnet controlSSH) {
		try {
			this.dpto = dpto;
			if (this.session == null || !this.session.isConnected()) {
				JSch jsch = new JSch();

				this.session = jsch.getSession(username, host, port);
				
//				Thread.sleep(Integer.parseInt(Parametros.esperaRespuestaElementosRed));

				this.session.setPassword(password);

				// Parametro para no validar key de conexion.
				this.session.setConfig("StrictHostKeyChecking", "no");
				this.session.setTimeout(10000);
				this.session.connect();
				// Abrimos un canal SSH. Es como abrir una consola.

				channel = session.openChannel("shell");
				
				channel.setInputStream(null);
				channel.setOutputStream(null);
				
				((ChannelShell) channel).setPtyType("vt102");
				
				channel.connect(3000);
				
				log.debug("Conexion Establecida SSH = "+ channel.isConnected()+ ", del servidor: "+dpto);
				

			}
		} catch (Exception e) {
			log.error("Problema de conexion SSH: ", e);
			error = "Problema de conexion SSH del servidor " + dpto + ": " + stackTraceToString(e);
			controlSSH.getErrores().add(error);
		}
	}

	public String stackTraceToString(Throwable e) {
		CharArrayWriter sw = new CharArrayWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		pw.close();

		return sw.toString(); // stack trace a
	}

	/**
	 * Ejecuta un comando SSH.
	 *
	 * @param command
	 *            Comando SSH a ejecutar.
	 **/

	public String executeCommand(String command) {
		log.debug("Ejecutando comando SSH");
		String result = "";
		boolean isComplete = false;// flag for bucle, continue reading if the buffer is not complete
		int intents = Integer.parseInt(Parametros.nroReintentosConexionTelnet);// counter, the bucle finish if the intents are 0
		try {
			if (this.session != null && this.session.isConnected()) {

				while (!isComplete) {
					log.debug("intentos =" + intents);
					if (intents != 0) {
						Thread.sleep(Integer.parseInt(Parametros.esperaRespuestaElementosRed));
					}

				    out = channel.getOutputStream();

					
					
					out.write((command).getBytes(Charset.forName("UTF-8")));
					out.write(("\n").getBytes(Charset.forName("UTF-8")));
					out.flush();

					result = readOutput2(channel);
					if (!result.equals("connection_not_complete")) {
						isComplete = true;
						break;
					}
					intents--;
					if (intents == 0) {
						isComplete = true;
						result = "";
						break;
					}
				}
				return result;
			}

		} catch (Exception e) {
			log.error("No pudo enviar el comando de la conexion SSH: ", e);
		}
		return null;
	}
	
	public String executeCommandSingleLine(String command) {
		log.debug("Ejecutando comando SSH");
		String result = "";
		boolean isComplete = false;// flag for bucle, continue reading if the buffer is not complete
		int intents = Integer.parseInt(Parametros.nroReintentosConexionTelnet);// counter, the bucle finish if the intents are 0
		try {
			if (this.session != null && this.session.isConnected()) {

				while (!isComplete) {
					log.debug("intentos =" + intents);
					if (intents != 0) {
						Thread.sleep(Integer.parseInt(Parametros.esperaRespuestaElementosRedGGSNSsh));
					}

				    out = channel.getOutputStream();

					
					
					out.write((command).getBytes(Charset.forName("UTF-8")));
					out.write(("\n").getBytes(Charset.forName("UTF-8")));
					out.flush();

					result = readOutput2(channel);
					if (!result.equals("connection_not_complete")) {
						isComplete = true;
						break;
					}
					intents--;
					if (intents == 0) {
						isComplete = true;
						result = "";
						break;
					}
				}
				return result;
			}

		} catch (Exception e) {
			log.error("No pudo enviar el comando de la conexion SSH: ", e);
		}
		return null;
	}
	
	public String readOutput2(Channel channel) throws IOException {
		try {
			StringBuilder sb = new StringBuilder();
			InputStream in = channel.getInputStream();//6031

			byte[] tmp = new byte[1024];
			while (true) {
//				Thread.sleep(500);
				Thread.sleep(Integer.parseInt(Parametros.esperaRespuestaElementosRedSsh));
				if (in.available() == 0) {
					return "connection_not_complete";
				}
//				log.debug("Cantidad de caracteres: "+ in.available());
				while (in.available() > 0) {

					int i = in.read(tmp, 0, 1024);
					if (i < 0) {
						break;
					}
					sb.append(new String(tmp, 0, i,Charset.forName("UTF-8")));
				
				}
				if (in.available() <= 0) {

//					System.out.println("exit-status: " + channel.getExitStatus());
					in.close();
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			
			
//			System.out.println(sb.toString());
			

			return sb.toString();
		} catch (Exception e) {
			log.error("No pudo leer de la conexion SSH: ", e);
			return "";
		}
	}
	/**
	 * Cierra la sesi&oacute;n SSH.
	 */
	public final void disconnect() {
		try {
			out.close();
			channel.isClosed();
			channel.disconnect();
			this.session.disconnect();
			log.debug("Conexion cerrada SSH = "+channel.isClosed()+ ", del servidor: "+dpto);

		} catch (Exception e) {
			log.error("No se pudo desconectar la conexion SSH: ", e);
		}
	}

	public static void main(String[] args) throws JSchException {

//		 final String USERNAME = "repo";
//		 final String HOST = "172.29.84.14";
//		 final int PORT = 22;
//		 final String PASSWORD = "repo123";
//		 SSHConnector sshConnector = new SSHConnector(USERNAME, PASSWORD, HOST, PORT,"La Paz",control);
//		 sshConnector.executeCommand("ls");
//		 sshConnector.executeCommand("ls");
//
//		 final String USERNAME = "sshvista360";
//		 final String HOST = "73.24.0.211";
//		 final int PORT = 22;
//		 final String PASSWORD = "v15t4.360";
//		 SSHConnector sshConnector = new SSHConnector(USERNAME, PASSWORD, HOST, PORT,"La Paz",control);
//		 String user = "vista360";
//		 String pass = "vista2017.2";
//		 String numero = "59177635310";
//		 sshConnector.executeCommand("LGI:HLRSN=1,OPNAME=\""+user+"\",PWD=\""+pass+"\";");
//		 sshConnector.executeCommand("LST DYNSUB: ISDN=\""+numero+"\"; ");
//		 
//		 sshConnector.disconnect();
		
		
//		final String USERNAME = "vistaccc";
//		 final String HOST = "172.31.93.170";
//		 final int PORT = 22;
//		 final String PASSWORD = "ccc5364";
//		 
//		 final String UsuarioMSC ="MMLUser1";
//		 final String PasswordMSC ="Tigo@123";
//		 final String tipo = "MSC-CBB";
//		 String numero = "77390320";
//		 
//		 SSHConnector sshConnector = new SSHConnector(USERNAME, PASSWORD, HOST, PORT,"La Paz",control);
//		 sshConnector.executeCommand("openssl s_client -port 31114 -host 172.25.0.100 -ssl3 -quiet -crlf");
//		 sshConnector.executeCommand("LGI:OP=\""+UsuarioMSC+"\",PWD=\""+PasswordMSC+"\";");
//		 sshConnector.executeCommand("REG NE:NAME=\""+tipo+"\";");
//		 sshConnector.executeCommand("USE ME:MEID=5;");
//		 sshConnector.executeCommand("DSP USRINF: UNT=MSISDN, D=K'591"+numero+";");
//		 
//		 sshConnector.disconnect();
	}
}
