package com.myapps.vista_ccc.bean.ttp;

import com.myapps.user.bean.ControlerBitacora;
import com.myapps.user.ldap.DescriptorBitacora;
import com.myapps.vista_ccc.bean.ttp.dao.ParametroServiceDeudaDao;
import com.myapps.vista_ccc.bean.ttp.dao.ParametrosServicioEntity;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@ManagedBean
@ViewScoped
public class ParametrosBean implements Serializable {

	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(ParametrosBean.class);
	private List<ParametrosServicioEntity> parametros;
	private List<ParametrosServicioEntity> filteredParametros;
	private String codigoBCCS;
	private String idServicio;
	private String nombreServicio;
	private ParametrosServicioEntity selectedParametro;
	@Inject
	private ParametroServiceDeudaDao parametroServiceDao;
	@Inject
	private ControlerBitacora controlerBitacora;

	@SuppressWarnings("unchecked")
	@PostConstruct
	public void cargarDatos() {
//		this.limpiarDatos();
		try {
			controlerBitacora.accion(DescriptorBitacora.PARAMETROS_SERVICIOS_BCCS_DEUDAS_TTP,
					"Cargar parametros de servicios BCCS deudas TTP con estado valido");
			this.parametros = this.parametroServiceDao.findAll();
			this.filteredParametros = parametros;
			log.info("Parametros de Servicios Tigo Te Presta cargados correctamente.");
		} catch (Exception e) {
			log.error("Error al consultar tabla de parametros de servicios " + e);
		}
	}

	public String hidden() {
		return "PF('editParametroDialog').hide()";
	}

	public void registrarParametro() {
		log.info("Captara de parametros: {Codigo BCCS: " + this.codigoBCCS + ", Servicio ID: " + this.idServicio
				+ ", Nombre Servicio: " + this.nombreServicio);
		if (this.validarDatos(this.codigoBCCS, this.idServicio, this.nombreServicio)) {
			try {
				ParametrosServicioEntity parametroService = new ParametrosServicioEntity();
				parametroService.setCodeServiceBCCS(this.codigoBCCS);
				parametroService.setServiceId(this.idServicio);
				parametroService.setServiceName(this.nombreServicio);
				parametroService.setIdParametrosServicio(0);
				parametroService.setFechaCreacion(this.formatDate(new Date()));
				parametroService.setCreadoPor(
						DescriptorBitacora.PARAMETROS_SERVICIOS_BCCS_DEUDAS_TTP + " - " + obtenerUsuario());
				parametroService.setEstado(1);
				if (this.verificarExistenciaParametros(parametroService)) {
					controlerBitacora.insert(DescriptorBitacora.PARAMETROS_SERVICIOS_BCCS_DEUDAS_TTP,
							parametroService.getCodeServiceBCCS(), parametroService.getServiceName());
					parametroServiceDao.save(parametroService);
					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage(FacesMessage.SEVERITY_INFO, "OK", "Registro Exitoso."));
					this.cargarDatos();
					this.limpiarDatos();
				}
			} catch (Exception e) {
				log.error("Error al registrar datos de parametros de servicio tigo te presta " + e.getMessage(), e);
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"Error",
						"No se pudo almacenar los datos por un error interno, comuniquese con el administrador."));
			}
		} else {
			log.info("Algunos Campos se encuentran vacios.");
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_WARN, "IMPORTANTE", "Debe ingresar todos los campos."));
//			cargarDatos();
		}

	}

	public boolean verificarExistenciaParametros(ParametrosServicioEntity parametroService) {
		boolean res = false;
		List<ParametrosServicioEntity> parametrosServiceExistentes = this.parametroServiceDao
				.findByCodigoAndId(parametroService);
		if (parametrosServiceExistentes.size() <= 0) {
			res = true;
		} else {
			log.warn("Ya existe un parametro con el mismo Codigo BCCS o Service Id : "
					+ parametrosServiceExistentes.size());
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_WARN, "IMPORTANTE",
							"Ya existe un parametro registrado con el mismo Codigo BCCS: "
									+ parametroService.getCodeServiceBCCS() + " o Service Id: "
									+ parametroService.getServiceId()));
		}
		return res;
	}

	public boolean verificarExistenciaParametrosEditar(ParametrosServicioEntity parametroService) {
		boolean res = true;
		List<ParametrosServicioEntity> parametrosServiceExistentes = this.parametroServiceDao
				.findByCodigoAndId(parametroService);
		if (parametrosServiceExistentes.size() > 0) {
			for (ParametrosServicioEntity pS : parametrosServiceExistentes) {
				if (pS.getIdParametrosServicio() != parametroService.getIdParametrosServicio()) {
					res = false;
					log.warn("Ya existe un parametro con el mismo Codigo BCCS o Service Id : "
							+ parametrosServiceExistentes);
					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage(FacesMessage.SEVERITY_WARN, "IMPORTANTE",
									"Ya existe un parametro registrado con el mismo Codigo BCCS: "
											+ parametroService.getCodeServiceBCCS() + " o Service Id: "
											+ parametroService.getServiceId()));
					break;
				}
			}
		}
		return res;
	}

	private String obtenerUsuario() {
		String login = "";
		try {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
					.getRequest();
			login = (String) request.getSession().getAttribute("TEMP$USER_NAME");
			// log.info("login: " + login);
		} catch (Exception e) {
			log.error("Error al obtener usuario: ", e);
		}
		return login;
	}

	public void eleminarParametro(ParametrosServicioEntity p) {

		log.info("Parametro a elminar service id: " + p.getServiceId() + ", codigo bccs: " + p.getCodeServiceBCCS()
				+ ", id parametro: " + p.getIdParametrosServicio());
		try {
			ParametrosServicioEntity perametroEntity = p;
			perametroEntity.setEstado(0);
			perametroEntity.setFechaModificacion(this.formatDate(new Date()));
			perametroEntity.setModificadoPor(
					DescriptorBitacora.PARAMETROS_SERVICIOS_BCCS_DEUDAS_TTP + " - " + obtenerUsuario());
			controlerBitacora.update(DescriptorBitacora.PARAMETROS_SERVICIOS_BCCS_DEUDAS_TTP,
					perametroEntity.getCodeServiceBCCS(), perametroEntity.getServiceName());
			this.parametroServiceDao.update(perametroEntity);
			log.info("Eliminacion de Parametros de Servicio Gestor Deudas TTP {Codigo BCCS: "
					+ perametroEntity.getCodeServiceBCCS() + ", Servicio ID: " + perametroEntity.getServiceId()
					+ ", Nombre Servicio: " + perametroEntity.getServiceName() + "}");
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "OK",
							"Parametro con: Codigo BCCS: " + perametroEntity.getCodeServiceBCCS() + " y ID Servucio: "
									+ perametroEntity.getServiceId() + ", Eleminado Correctamente."));
			this.cargarDatos();
		} catch (Exception e) {
			log.error("NO se pudo actualizar parametro " + e.getMessage(), e);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR ",
					"NO se pudo actualizar parametro, comuniquese con el administrador."));
		}
	}

	public Date formatDate(Date imputDate) throws Exception {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if (imputDate != null) {
			String fechaString = format.format(imputDate);
			Date miFecha = format.parse(fechaString);
			return miFecha;
		}

		return null;
	}

	public boolean validarDatos(String codigoBCCS, String idServicio, String nombreServivio) {
		boolean res = false;
		if (!codigoBCCS.trim().isEmpty() && !idServicio.trim().isEmpty() && !nombreServivio.trim().isEmpty())
			res = true;
		return res;
	}

	public void limpiarDatos() {
		this.codigoBCCS = "";
		this.idServicio = "";
		this.nombreServicio = "";
	}

	public void selectEditarParametro(ParametrosServicioEntity p) {
		this.selectedParametro = p;
	}

	public void editarParametro() {
		log.info("Editando parametro de servicio " + this.selectedParametro.getCodeServiceBCCS());
		if (this.validarDatos(selectedParametro.getCodeServiceBCCS(), selectedParametro.getServiceId(),
				selectedParametro.getServiceName())) {
			try {
				ParametrosServicioEntity perametroEntity = this.selectedParametro;
				perametroEntity.setFechaModificacion(this.formatDate(new Date()));
				perametroEntity.setModificadoPor(
						DescriptorBitacora.PARAMETROS_SERVICIOS_BCCS_DEUDAS_TTP + " - " + obtenerUsuario());
				if (this.verificarExistenciaParametrosEditar(perametroEntity)) {
					controlerBitacora.insert(DescriptorBitacora.PARAMETROS_SERVICIOS_BCCS_DEUDAS_TTP,
							perametroEntity.getCodeServiceBCCS(), perametroEntity.getServiceName());
					this.parametroServiceDao.update(perametroEntity);
					log.info("Edicion de Parametros de Servicio Gestor Deudas TTP {Codigo BCCS: "
							+ perametroEntity.getCodeServiceBCCS() + ", Servicio ID: " + perametroEntity.getServiceId()
							+ ", Nombre Servicio: " + perametroEntity.getServiceName() + "}");
					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage(FacesMessage.SEVERITY_INFO, "OK",
									"Parametro con: Codigo BCCS: " + perametroEntity.getCodeServiceBCCS()
											+ " y ID Servucio: " + perametroEntity.getServiceId()
											+ ", Actualizado Correctamente."));
					RequestContext rc = RequestContext.getCurrentInstance();
					rc.execute("PF('editParametroDialog').hide()");
					this.cargarDatos();
					this.limpiarDatos();
				}
			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"ERROR ", "NO se pudo actualizar parametro, comuniquese con el administrador."));
			}
		} else {
			log.info("Algunos Campos se encuentran vacios.");
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_WARN, "IMPORTANTE", "Debe ingresar todos los campos."));
//			cargarDatos();
		}
	}

	public void cancelEditarParametro() {
		ParametrosServicioEntity perametroEntity = this.selectedParametro;
		log.info("Edicion de Parametros de Servicio Gestor Deudas TTP Cancelada.");
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "IMPORTANTE",
				"Actualizacion Cancelada." + perametroEntity.getServiceName()));
		cargarDatos();
	}

	public List<ParametrosServicioEntity> getParametros() {
		return parametros;
	}

	public void setParametros(List<ParametrosServicioEntity> parametros) {
		this.parametros = parametros;
	}

	public String getCodigoBCCS() {
		return codigoBCCS;
	}

	public void setCodigoBCCS(String codigoBCCS) {
		this.codigoBCCS = codigoBCCS;
	}

	public String getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(String idServicio) {
		this.idServicio = idServicio;
	}

	public String getNombreServicio() {
		return nombreServicio;
	}

	public List<ParametrosServicioEntity> getFilteredParametros() {
		return filteredParametros;
	}

	public void setFilteredParametros(List<ParametrosServicioEntity> filteredParametros) {
		this.filteredParametros = filteredParametros;
	}

	public void setNombreServicio(String nombreServicio) {
		this.nombreServicio = nombreServicio;
	}

	public ParametrosServicioEntity getSelectedParametro() {
		return selectedParametro;
	}

	public void setSelectedParametro(ParametrosServicioEntity selectedParametro) {
		this.selectedParametro = selectedParametro;
	}

}
