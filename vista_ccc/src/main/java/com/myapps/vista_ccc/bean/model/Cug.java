package com.myapps.vista_ccc.bean.model;

import java.io.Serializable;

public class Cug implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String CugName;
	private String groupKey;
	private String groupCode;
	private String shortDialCode;
	private String cugMemberTypeCode;

	public Cug() {
		super();
	}

	
	public Cug(String cugName, String groupKey, String groupCode, String shortDialCode, String cugMemberTypeCode) {
		super();
		CugName = cugName;
		this.groupKey = groupKey;
		this.groupCode = groupCode;
		this.shortDialCode = shortDialCode;
		this.cugMemberTypeCode = cugMemberTypeCode;
	}


	public String getCugName() {
		return CugName;
	}

	public void setCugName(String cugName) {
		CugName = cugName;
	}

	public String getGroupKey() {
		return groupKey;
	}

	public void setGroupKey(String groupKey) {
		this.groupKey = groupKey;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getShortDialCode() {
		return shortDialCode;
	}

	public void setShortDialCode(String shortDialCode) {
		this.shortDialCode = shortDialCode;
	}

	public String getCugMemberTypeCode() {
		return cugMemberTypeCode;
	}

	public void setCugMemberTypeCode(String cugMemberTypeCode) {
		this.cugMemberTypeCode = cugMemberTypeCode;
	}


	@Override
	public String toString() {
		return "Cug [CugName=" + CugName + ", groupKey=" + groupKey + ", groupCode=" + groupCode + ", shortDialCode=" + shortDialCode + ", cugMemberTypeCode=" + cugMemberTypeCode + "]";
	}
	
	

	

}
