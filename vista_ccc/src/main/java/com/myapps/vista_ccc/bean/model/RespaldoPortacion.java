package com.myapps.vista_ccc.bean.model;

import org.apache.log4j.Logger;
import org.primefaces.model.StreamedContent;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

public class RespaldoPortacion implements Serializable {

	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(RespaldoPortacion.class);
	private String portacionID;
	private transient StreamedContent StreamedContentPDF;
	private transient StreamedContent StreamedContentDeclaracion;
	private List<ImagenMnpda> listaImagenes;

	public StreamedContent getStreamedContentPDF() {
		try {
			if (StreamedContentPDF != null) {
				StreamedContentPDF.getStream().reset();
			}
		} catch (IOException e) {
			e.printStackTrace();
			log.error("ERROR: " + e.getMessage());
		}
		return StreamedContentPDF;
	}

	public void setStreamedContentPDF(StreamedContent streamedContentPDF) {
		StreamedContentPDF = streamedContentPDF;
	}

	public List<ImagenMnpda> getListaImagenes() {
		return listaImagenes;
	}

	public void setListaImagenes(List<ImagenMnpda> listaImagenes) {
		this.listaImagenes = listaImagenes;
	}

	public String getPortacionID() {
		return portacionID;
	}

	public void setPortacionID(String portacionID) {
		this.portacionID = portacionID;
	}

	public StreamedContent getStreamedContentDeclaracion() {
		try {
			if (StreamedContentDeclaracion != null) {
				StreamedContentDeclaracion.getStream().reset();
			}
		} catch (IOException e) {
			e.printStackTrace();
			log.error("ERROR: " + e.getMessage());
		}
		return StreamedContentDeclaracion;
	}

	public void setStreamedContentDeclaracion(StreamedContent streamedContentDeclaracion) {
		StreamedContentDeclaracion = streamedContentDeclaracion;
	}

}
