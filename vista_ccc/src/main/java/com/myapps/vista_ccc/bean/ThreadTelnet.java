package com.myapps.vista_ccc.bean;

import com.myapps.vista_ccc.entity.VcConexionTelnet;
import com.myapps.vista_ccc.entity.VcConfigElementoRed;
import com.myapps.vista_ccc.entity.VcServidor;
import com.myapps.vista_ccc.thread.ControlButton;
import com.myapps.vista_ccc.util.Parametros;
import org.apache.log4j.Logger;

import java.io.CharArrayWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class ThreadTelnet extends Thread {
	public static Logger log = Logger.getLogger(ThreadTelnet.class);
	private boolean live;

	private VcServidor elementoRed;
	private String idHilo;
	private ControlTelnet control;
	private String isdn;
	List<VcConexionTelnet> lista;
	List<VcConfigElementoRed> listExcepcion;
	private ControlButton controlButton;

	public ThreadTelnet(VcServidor vcservidor, ControlTelnet control, String isdn, List<VcConexionTelnet> lista, ControlButton controlButton, List<VcConfigElementoRed> listExcepcion) {
		this.idHilo = String.valueOf(vcservidor.getIdServidor());
		this.elementoRed = vcservidor;
		this.control = control;
		this.controlButton = controlButton;
		this.isdn = isdn;
		this.lista = lista;
		this.listExcepcion = listExcepcion;

		control.setElementoRedExitoso(String.valueOf(elementoRed.getIdServidor()));
	}

	public void run() {
		live = true;
		while (live) {
			try {

				// OBTENER el GSSN y el MSC
				log.debug("Ejecuntando el hilo de conexion");

				if (lista == null || lista.isEmpty()) {
					log.debug("No se encuentran nodos para el elemento de red conexion Telnet: " + elementoRed.getNombreServidor());
					lista = new ArrayList<>();
				}

				String elementoRedResult = "";
				for (VcConexionTelnet nodo : lista) {
					String resultNodo = null;
					if (nodo.getTipoConexion().equals("ssh")) {
						log.debug("Entro para la conexion ssh");
						if (elementoRed.getTipoConexion() == 1)
							resultNodo = comandos4(nodo);
						if (elementoRed.getTipoConexion() == 2)
							resultNodo = comandos5(nodo);
						if (elementoRed.getTipoConexion() == 3)
							resultNodo = comandos6(nodo);
						if (resultNodo != null && !resultNodo.isEmpty()) {
							log.debug("Se encontro una trama exitosa para el elemento de red conexion Ssh: " + elementoRed.getNombreServidor());
							live = false;
							elementoRedResult = resultNodo;
							break;
						}
					} else {
						log.debug("Entro para la conexion telnet");
						if (elementoRed.getTipoConexion() == 1)
							resultNodo = comandos1(nodo);
						if (elementoRed.getTipoConexion() == 2)
							resultNodo = comandos2(nodo);
						if (elementoRed.getTipoConexion() == 3)
							resultNodo = comandos3(nodo);
						if (resultNodo != null && !resultNodo.isEmpty()) {
							log.debug("Se encontro una trama exitosa para el elemento de red conexion Telnet: " + elementoRed.getNombreServidor());
							live = false;
							elementoRedResult = resultNodo;
							break;
						}
					}
				}
				if (elementoRedResult == null || elementoRedResult.isEmpty()) {
					log.debug("No se encontro una trama exitosa para el elemento de red: " + elementoRed.getNombreServidor());
				}
				live = false;// finish the thread
			} catch (Exception e) {
				live = false;
				log.warn("Error al conectar el hilo: ", e);
				control.getErrores().add("Error al conectar el hilo: "+ stackTraceToString(e));
			} finally {
				control.finalizar();
				live = false;
				log.debug("finalizo el hilo elementos red conexion: " + idHilo);
			}
		}
	}
	public String stackTraceToString(Throwable e) {
		CharArrayWriter sw = new CharArrayWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		pw.close();

		return sw.toString(); // stack trace a
	}
	public String comandos1(VcConexionTelnet nodo) throws Exception {

		log.debug("conexión con nodo: " + nodo.getCodServidor() + " / " + nodo.getIdConexion());
		AutomatedTelnetClient telnet = new AutomatedTelnetClient(nodo.getIp(), String.valueOf(nodo.getPuerto()), nodo.getNombreElementoRed());
//		log.debug("Conectando...");
		String autenticacion = "";

		if (nodo.getUsuarioComando() != null && nodo.getPasswordComando() != null) {
			String cadena = Parametros.cnxTelnetHlrComandUno.replace("[user]", nodo.getUsuarioComando());
			cadena = cadena.replace("[password]", nodo.getPasswordComando());
			autenticacion = telnet.sendCommandAutenticacion(cadena);

			if (autenticacion == null || autenticacion.isEmpty()) {
				log.error("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexion)");
				controlButton.getErrores().add("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
				control.getErrores().add("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
				telnet.disconnect();
				return "";
			}
		}
		String error = "";
		// Para obtener GGSN
		// Para verificar si la trama contiene los datos para el GGSN y MSC
		String CondicionParada  = telnet.sendCommandSingleLine(Parametros.cnxTelnetGGSNComandEnter);
		String trama1 = telnet.sendCommand(Parametros.cnxTelnetHlrComandDos.replace("[isdn]", isdn));
		
		

		for (VcConfigElementoRed prueba : listExcepcion) {
			if (autenticacion.contains(prueba.getExcepcionServidor())) {
				log.error("Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + autenticacion);
				error = "Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + autenticacion;
				control.getErrores().add(error);
				telnet.disconnect();
				control.setElementoRedExitoso("No exitoso");
				return "";
			}
		}

		if (trama1 == null || trama1.isEmpty()) {
			log.error("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexion)");
			controlButton.getErrores().add("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
			control.getErrores().add("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
			telnet.disconnect();
			return "";
		}

		for (VcConfigElementoRed prueba : listExcepcion) {
			if (trama1.contains(prueba.getExcepcionServidor())) {
				log.error("Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + trama1);
				error = "Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + trama1;
				control.getErrores().add(error);
				control.setElementoRedExitoso("No exitoso");
				telnet.disconnect();
				return "";
			}
		}

		if (trama1.contains("MscNum") || trama1.contains("SgsnNum") || trama1.contains("IMEI")) {
			String tram2 = "";
			while (!tram2.equals("Elemento de red finalizado con exito")) {
				tram2 = telnet.sendCommandSingleLine(Parametros.cnxTelnetGGSNComandEnter);
//				if (tram2 == null || tram2.isEmpty()) {
				if (tram2.equals(CondicionParada)) {
					break;
				} else {
						trama1 = trama1.concat(tram2);
				}

			}
		
			telnet.disconnect();
			nodo.setEstado(trama1);// Reusamos la variable estado del objeto para guardar la trama
			control.getSuccessConnections().add(nodo);
			return trama1;
		} else {
			String tram2 = "";
			while (!tram2.equals("Elemento de red finalizado con exito")) {
				tram2 = telnet.sendCommandSingleLine(Parametros.cnxTelnetGGSNComandEnter);
//				if (tram2 == null || tram2.isEmpty()) {
				if (tram2.equals(CondicionParada)) {
					break;
				} else {
						trama1 = trama1.concat(tram2);
				}

			}
			nodo.setEstado(trama1);
			log.error("No se encontro el GGSN ni el MSC en  " + nodo.getNombreElementoRed());
			error = "No se encontro el GGSN ni el MSC en  " + nodo.getNombreElementoRed();
			control.getErrores().add(error);
			telnet.disconnect();
			control.setElementoRedExitoso("No exitoso");
			control.getSuccessConnections().add(nodo);
			return trama1;// En caso de que los datos no se encuentren la trama es
							// descartada
		}

	}

	public String comandos2(VcConexionTelnet nodo) throws Exception {
		log.debug("Conexión con nodo: " + nodo.getCodServidor() + " / " + nodo.getIdConexion());
		AutomatedTelnetClient telnet = new AutomatedTelnetClient(nodo.getIp(), String.valueOf(nodo.getPuerto()), nodo.getNombreElementoRed());
//		log.debug("Conectando...");

		String autenticacion = "";
		String user = "";
		if (nodo.getUsuarioComando() != null && nodo.getPasswordComando() != null) {
			user = telnet.sendCommandAutenticacion(nodo.getUsuarioComando());
			autenticacion = telnet.sendCommandAutenticacion(nodo.getPasswordComando());
			// controlButton.getErrores().add("El servidor no esta respondiendo. " + user);
			if (user == null || user.isEmpty()) {
				log.error("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
				controlButton.getErrores().add("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
				control.getErrores().add("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
				telnet.disconnect();
				return "";
			}
			if (autenticacion == null || autenticacion.isEmpty()) {
				log.error("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
				controlButton.getErrores().add("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
				control.getErrores().add("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
				telnet.disconnect();
				return "";
			}
		}

		telnet.sendCommand(Parametros.cnxTelnetGGSNComandCero);
		String CondicionParada = telnet.sendCommand(Parametros.cnxTelnetGGSNComandEnter);
		log.debug("Obtener Nombre Servidor: " + CondicionParada);
		String trama = telnet.sendCommand(Parametros.cnxTelnetGGSNComandUno.replace("[isdn]", isdn));
		String error = "";

		for (VcConfigElementoRed prueba : listExcepcion) {
			if (user.contains(prueba.getExcepcionServidor())) {
				log.error("Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + user);
				error = "Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + user;
				control.getErrores().add(error);
				telnet.disconnect();
				control.setElementoRedExitoso("No exitoso");
				return "";
			}
		}

		for (VcConfigElementoRed prueba : listExcepcion) {
			if (autenticacion.contains(prueba.getExcepcionServidor())) {
				log.error("Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + autenticacion);
				error = "Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + autenticacion;
				control.getErrores().add(error);
				telnet.disconnect();
				control.setElementoRedExitoso("No exitoso");
				return "";
			}
		}

		if (trama == null || trama.isEmpty()) {
			log.error("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
			controlButton.getErrores().add("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
			control.getErrores().add("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
			telnet.disconnect();
			return "";
		}

		for (VcConfigElementoRed prueba : listExcepcion) {
			if (trama.contains(prueba.getExcepcionServidor())) {
				log.error("Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + trama);
				error = "Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + trama;
				control.getErrores().add(error);
				telnet.disconnect();
				control.setElementoRedExitoso("No exitoso");
				return "";
			}
		}


		String tram2 = "";
		while (!tram2.contains("Elemento de red finalizado con exito")) {
			tram2 = telnet.sendCommandSingleLine(Parametros.cnxTelnetGGSNComandEnter);
			log.debug("RESPUESTA: " + tram2);
			if (tram2.equals(CondicionParada)) {
				break;
			} else {
				trama = trama.concat(tram2);
			}
		}
		
		telnet.disconnect();
		nodo.setEstado(trama);// Reusamos la variable estado del objeto para guardar la trama
		control.getSuccessConnections().add(nodo);
		return trama;
	}

	public String comandos3(VcConexionTelnet nodo) throws Exception {
		log.debug("conexión con nodo: " + nodo.getCodServidor() + " / " + nodo.getIdConexion());
		AutomatedTelnetClient telnet = new AutomatedTelnetClient(nodo.getIp(), String.valueOf(nodo.getPuerto()), nodo.getNombreElementoRed());
//		log.debug("Conectando...");

		String autenticacion = "";
		if (nodo.getUsuarioComando() != null && nodo.getPasswordComando() != null) {
			String cadena = Parametros.cnxTelnetMscComandUno.replace("[user]", nodo.getUsuarioComando());
			cadena = cadena.replace("[password]", nodo.getPasswordComando());
			autenticacion = telnet.sendCommandAutenticacion(cadena);
			if (autenticacion == null || autenticacion.isEmpty()) {
				log.error("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexion)");
				controlButton.getErrores().add("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
				control.getErrores().add("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
				telnet.disconnect();
				return "";
			}
		}

		String trama0 = telnet.sendCommand(Parametros.cnxTelnetMscComandDos);
		String CondicionParada  = telnet.sendCommandSingleLine(Parametros.cnxTelnetGGSNComandEnter);
		String trama = telnet.sendCommand(Parametros.cnxTelnetMscComandTres.replace("[isdn]", isdn));
		String error = "";

		for (VcConfigElementoRed prueba : listExcepcion) {
			if (autenticacion.contains(prueba.getExcepcionServidor())) {
				log.error("Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + autenticacion);
				error = "Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + autenticacion;
				control.getErrores().add(error);
				telnet.disconnect();
				control.setElementoRedExitoso("No exitoso");
				return "";
			}
		}

		if (trama0 == null || trama0.isEmpty() || trama == null || trama.isEmpty()) {
			log.error("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
			controlButton.getErrores().add("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
			control.getErrores().add("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
			telnet.disconnect();
			return "";
		}

		for (VcConfigElementoRed prueba : listExcepcion) {
			if (trama.contains(prueba.getExcepcionServidor())) {
				log.error("Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + trama);
				error = "Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + trama;
				control.getErrores().add(error);
				telnet.disconnect();
				control.setElementoRedExitoso("No exitoso");
				return "";
			}
		}

		for (VcConfigElementoRed prueba : listExcepcion) {
			if (trama0.contains(prueba.getExcepcionServidor())) {
				log.error("Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + trama0);
				error = "Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + trama0;
				control.getErrores().add(error);
				telnet.disconnect();
				control.setElementoRedExitoso("No exitoso");
				return "";
			}
		}

		String tram2 = "";
		while (!tram2.equals("Elemento de red finalizado con exito")) {
			tram2 = telnet.sendCommandSingleLine(Parametros.cnxTelnetGGSNComandEnter);
			if (tram2.equals(CondicionParada)) {
				break;
			} else {
				trama = trama.concat(tram2);
			}

		}

		telnet.disconnect();
		nodo.setEstado(trama);// Reusamos la variable estado del objeto para guardar la trama
		control.getSuccessConnections().add(nodo);
		return trama;
	}

	public String comandos4(VcConexionTelnet nodo) throws Exception {

		log.debug("conexión con nodo: " + nodo.getCodServidor() + " / " + nodo.getIdConexion());
		SSHConnector ssh = new SSHConnector(nodo.getUsuarioSsh(), nodo.getPasswordSsh(), nodo.getIp(), nodo.getPuerto(), nodo.getNombreElementoRed(), control);
//		log.debug("Conectando...");
		String autenticacion = "";
		String error = "";
		// Para obtener GGSN

		if (nodo.getUsuarioComando() != null && nodo.getPasswordComando() != null) {
			String cadena = Parametros.cnxTelnetHlrComandUno.replace("[user]", nodo.getUsuarioComando());
			cadena = cadena.replace("[password]", nodo.getPasswordComando());
			autenticacion = ssh.executeCommand(cadena);
//			log.debug(autenticacion);

			if (autenticacion == null || autenticacion.isEmpty()) {
				log.error("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexion)");
				controlButton.getErrores().add("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
				control.getErrores().add("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
				ssh.disconnect();
				return "";
			}
		}

		// Para verificar si la trama contiene los datos para el GGSN y MSC

		String trama1 = ssh.executeCommand(Parametros.cnxTelnetHlrComandDos.replace("[isdn]", isdn));
		log.debug("------------------------INICIO RESPUESTA SERVIDOR "+nodo.getNombreElementoRed()+"----------------------------------");
		log.debug(trama1);
		log.debug("----------------------- FIN REPUESTA SERVIDOR "+nodo.getNombreElementoRed()+"---------------------------------");
		

		for (VcConfigElementoRed prueba : listExcepcion) {
			if (autenticacion.contains(prueba.getExcepcionServidor())) {
				log.error("Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + autenticacion);
				error = "Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + autenticacion;
				control.getErrores().add(error);
				ssh.disconnect();
				control.setElementoRedExitoso("No exitoso");
				return "";
			}
		}

		if (trama1 == null || trama1.isEmpty()) {
			log.error("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexion)");
			controlButton.getErrores().add("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
			control.getErrores().add("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
			ssh.disconnect();
			return "";
		}

		for (VcConfigElementoRed prueba : listExcepcion) {
			if (trama1.contains(prueba.getExcepcionServidor())) {
				log.error("Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + trama1);
				error = "Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + trama1;
				control.getErrores().add(error);
				control.setElementoRedExitoso("No exitoso");
				ssh.disconnect();
				return "";
			}
		}

		if (trama1.contains("MscNum") || trama1.contains("SgsnNum") || trama1.contains("IMEI")) {
			ssh.disconnect();
			nodo.setEstado(trama1);// Reusamos la variable estado del objeto para guardar la trama
			control.getSuccessConnections().add(nodo);
			return trama1;
		} else {
			nodo.setEstado(trama1);
			log.error("No se encontro el GGSN ni el MSC en  " + nodo.getNombreElementoRed());
			error = "No se encontro el GGSN ni el MSC en  " + nodo.getNombreElementoRed();
			control.getErrores().add(error);
			ssh.disconnect();
			control.setElementoRedExitoso("No exitoso");
			control.getSuccessConnections().add(nodo);
			return trama1;// En caso de que los datos no se encuentren la trama es
							// descartada
		}
	}

	public String comandos5(VcConexionTelnet nodo) throws Exception {
		log.debug("Conexión con nodo: " + nodo.getCodServidor() + " / " + nodo.getIdConexion());
		SSHConnector ssh = new SSHConnector(nodo.getUsuarioSsh(), nodo.getPasswordSsh(), nodo.getIp(), nodo.getPuerto(), nodo.getNombreElementoRed(), control);
//		log.debug("Conectando...");
		
		ssh.executeCommand(Parametros.cnxTelnetGGSNComandCero);
		String CondicionParada = ssh.executeCommand(Parametros.cnxTelnetGGSNComandEnter);
		log.debug("Obtener Nombre Servidor: " + CondicionParada);
		String trama = ssh.executeCommand(Parametros.cnxTelnetGGSNComandUno.replace("[isdn]", isdn));
		String error = "";
		log.debug("------------------------INICIO RESPUESTA SERVIDOR "+nodo.getNombreElementoRed()+"----------------------------------");
		log.debug(trama);
		log.debug("----------------------- FIN REPUESTA SERVIDOR "+nodo.getNombreElementoRed()+"---------------------------------");
		
		if (trama == null || trama.isEmpty()) {
			log.error("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
			controlButton.getErrores().add("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
			control.getErrores().add("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
			ssh.disconnect();
			return "";
		}

		for (VcConfigElementoRed prueba : listExcepcion) {
			if (trama.contains(prueba.getExcepcionServidor())) {
				log.error("Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + trama);
				error = "Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + trama;
				control.getErrores().add(error);
				ssh.disconnect();
				control.setElementoRedExitoso("No exitoso");
				return "";
			}
		}

		
		String tram2 = "";
		while (!tram2.contains("Elemento de red finalizado con exito")) {
			tram2 = ssh.executeCommandSingleLine(Parametros.cnxTelnetGGSNComandEnter);
			log.debug("------------------------INICIO RESPUESTA SERVIDOR "+nodo.getNombreElementoRed()+"----------------------------------");
			log.debug("RESPUESTA: " + tram2);
			log.debug("----------------------- FIN REPUESTA SERVIDOR "+nodo.getNombreElementoRed()+"---------------------------------");
			if (tram2 == null || tram2.equals(CondicionParada) ) {
				break;
			} else {
				trama = trama.concat(tram2);
			}
		}
		
		ssh.disconnect();
		nodo.setEstado(trama);// Reusamos la variable estado del objeto para guardar la trama
		control.getSuccessConnections().add(nodo);
		return trama;
	}

	public String comandos6(VcConexionTelnet nodo) throws Exception {
		log.debug("conexión con nodo: " + nodo.getCodServidor() + " / " + nodo.getIdConexion());
		SSHConnector ssh = new SSHConnector(nodo.getUsuarioSsh(), nodo.getPasswordSsh(), nodo.getIp(), nodo.getPuerto(), nodo.getNombreElementoRed(), control);
//		log.debug("Conectando...");
		String ComandSSL = ssh.executeCommand(Parametros.cnxSshMscComandCuatro);
		log.debug("------------------------INICIO RESPUESTA SERVIDOR "+nodo.getNombreElementoRed()+"----------------------------------");
		log.debug(ComandSSL);
		log.debug("----------------------- FIN REPUESTA SERVIDOR "+nodo.getNombreElementoRed()+"---------------------------------");
		String autenticacion = "";
		if (nodo.getUsuarioComando() != null && nodo.getPasswordComando() != null) {
			String cadena = Parametros.cnxSshMscComandUno.replace("[user]", nodo.getUsuarioComando());
			cadena = cadena.replace("[password]", nodo.getPasswordComando());
			autenticacion = ssh.executeCommand(cadena);
			if (autenticacion == null || autenticacion.isEmpty()) {
				log.error("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexion)");
				controlButton.getErrores().add("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
				control.getErrores().add("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
				ssh.disconnect();
				return "";
			}
		}
		String ComandSSL2 = ssh.executeCommand(Parametros.cnxSshMscComandCinco.replace("[tipoServidor]", nodo.getNombreElementoRed()));
		log.debug("------------------------INICIO RESPUESTA SERVIDOR "+nodo.getNombreElementoRed()+"----------------------------------");
		log.debug(ComandSSL2);
		log.debug("----------------------- FIN REPUESTA SERVIDOR "+nodo.getNombreElementoRed()+"---------------------------------");
//		String trama0 = ssh.executeCommand(Parametros.cnxTelnetMscComandDos);
		String trama = ssh.executeCommand(Parametros.cnxTelnetMscComandTres.replace("[isdn]", isdn));
		
		log.debug("------------------------INICIO RESPUESTA SERVIDOR "+nodo.getNombreElementoRed()+"----------------------------------");
		log.debug(trama);
		log.debug("----------------------- FIN REPUESTA SERVIDOR "+nodo.getNombreElementoRed()+"---------------------------------");
		
		String error = "";

		for (VcConfigElementoRed prueba : listExcepcion) {
			if (autenticacion.contains(prueba.getExcepcionServidor())) {
				log.error("Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + autenticacion);
				error = "Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + autenticacion;
				control.getErrores().add(error);
				ssh.disconnect();
				control.setElementoRedExitoso("No exitoso");
				return "";
			}
		}

		if (trama == null || trama.isEmpty()) {
			log.error("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
			controlButton.getErrores().add("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
			control.getErrores().add("El servidor no esta respondiendo. " + nodo.getNombreElementoRed() + "(Problema de conexión)");
			ssh.disconnect();
			return "";
		}

		for (VcConfigElementoRed prueba : listExcepcion) {
			if (trama.contains(prueba.getExcepcionServidor())) {
				log.error("Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + trama);
				error = "Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + trama;
				control.getErrores().add(error);
				ssh.disconnect();
				control.setElementoRedExitoso("No exitoso");
				return "";
			}
		}

		for (VcConfigElementoRed prueba : listExcepcion) {
			if (ComandSSL.contains(prueba.getExcepcionServidor())) {
				log.error("Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + ComandSSL);
				error = "Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + ComandSSL;
				control.getErrores().add(error);
				ssh.disconnect();
				control.setElementoRedExitoso("No exitoso");
				return "";
			}
		}
		
		for (VcConfigElementoRed prueba : listExcepcion) {
			if (ComandSSL2.contains(prueba.getExcepcionServidor())) {
				log.error("Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + ComandSSL2);
				error = "Excepcion del Elemento Red: " + nodo.getNombreElementoRed() + "\n" + ComandSSL2;
				control.getErrores().add(error);
				ssh.disconnect();
				control.setElementoRedExitoso("No exitoso");
				return "";
			}
		}

		ssh.disconnect();
		nodo.setEstado(trama);// Reusamos la variable estado del objeto para guardar la trama
		control.getSuccessConnections().add(nodo);
		return trama;
	}
}