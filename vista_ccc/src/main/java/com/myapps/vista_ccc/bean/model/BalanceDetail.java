package com.myapps.vista_ccc.bean.model;

import java.io.Serializable;

public class BalanceDetail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String color;
	private String textAlign;
	private String balanceTypeName;
	private String chargeAmount;
	private String currencyId;
	private String chargeCode;
	private String chargeCodeName;
	private String currentAmount;

	public BalanceDetail() {
		super();
	}

	public BalanceDetail(String balanceTypeName, String chargeAmount, String currencyId, String chargeCode, String chargeCodeName) {
		super();
		this.balanceTypeName = balanceTypeName;
		this.chargeAmount = chargeAmount;
		this.currencyId = currencyId;
		this.chargeCode = chargeCode;
		this.chargeCodeName = chargeCodeName;
		this.currentAmount = "";
	}

	public String getTextAlign() {
		return textAlign;
	}

	public void setTextAlign(String textAlign) {
		this.textAlign = textAlign;
	}
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getBalanceTypeName() {
		return balanceTypeName;
	}

	public void setBalanceTypeName(String balanceTypeName) {
		this.balanceTypeName = balanceTypeName;
	}

	public String getChargeAmount() {
		return chargeAmount;
	}

	public void setChargeAmount(String chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	public String getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public String getChargeCodeName() {
		return chargeCodeName;
	}

	public void setChargeCodeName(String chargeCodeName) {
		this.chargeCodeName = chargeCodeName;
	}

	public String getCurrentAmount() {
		return currentAmount;
	}

	public void setCurrentAmount(String currentAmount) {
		this.currentAmount = currentAmount;
	}

	@Override
	public String toString() {
		return "BalanceDetail [balanceTypeName=" + balanceTypeName + ", chargeAmount=" + chargeAmount + ", currencyId=" + currencyId + ", chargeCode=" + chargeCode + ", chargeCodeName=" + chargeCodeName + ", currentAmount=" + currentAmount + "]";
	}

}
