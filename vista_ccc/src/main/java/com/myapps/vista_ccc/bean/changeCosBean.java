package com.myapps.vista_ccc.bean;

import com.myapps.user.bean.ControlerBitacora;
import com.myapps.user.ldap.DescriptorBitacora;
import com.myapps.vista_ccc.servicio.NodoServicio;
import com.myapps.vista_ccc.servicio.ServicioChangeCos;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.SysMessage;
import com.myapps.vista_ccc.util.UtilNumber;
import org.apache.axis.AxisFault;
import org.apache.log4j.Logger;
import org.myapps.cc.ws.*;
import org.myapps.changecost.ChangeCostWSSoapBindingStub;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.xml.rpc.ServiceException;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

@ManagedBean
@ViewScoped
public class changeCosBean implements Serializable {

	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(changeCosBean.class);

	private String isdn;
	private boolean disable;
	private String login;
	private String planCos;
	private String descripcion;
	@Inject
	private ControlerBitacora controlerBitacora;

	private ArrayList<ChangeCosRollbackEntity> listRollBack;
	private ArrayList<ChangeCosHistoricoEntity> listaHistorico;

	@PostConstruct
	public void init() {
		try {
			this.login = "";
			this.disable = true;
			this.planCos = "";
			this.descripcion = "";
			this.listaHistorico = new ArrayList<>();
			this.listRollBack = new ArrayList<>();
			obtenerUsuario();
		} catch (Exception e) {
			log.error("Error al iniciar, " + e);
		}
	}

	public void buscar() {
		log.debug("ingresando a vefificar tarifa rebajada para nro: " + isdn);
		controlerBitacora.accion(DescriptorBitacora.TARIFA_REBAJADA, "Se consulto la linea: " + isdn);
		try {
			if (isdn != null && UtilNumber.esNroTigo(isdn)) {
				consumirChangeCosProcess(isdn);
			} else {
				isdn = "";
				disable = true;
				SysMessage.warn("Isdn no valido", null);
			}

		} catch (Exception e) {
			log.error("Error al verificar tarifa rebajada para nro:" + isdn);
		}

	}

	public void desuscribir() {
		log.debug("ingresando a vefificar tarifa rebajada para nro: " + isdn);
		try {
			if (isdn != null && UtilNumber.esNroTigo(isdn)) {
				consumirChangeCosRollBack(isdn);
			} else {
				isdn = "";
				disable = true;
				SysMessage.warn("Isdn no valido", null);
			}

		} catch (Exception e) {
			log.error("Error al verificar tarifa rebajada para nro:" + isdn);
		}

	}

	public String getIsdn() {
		return isdn;
	}

	public void setIsdn(String isdn) {
		this.isdn = isdn;
	}

	public boolean isDisable() {
		return disable;
	}

	public void setDisable(boolean disable) {
		this.disable = disable;
	}

	public String getPlanCos() {
		return planCos;
	}

	public void setPlanCos(String planCos) {
		this.planCos = planCos;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public ArrayList<ChangeCosRollbackEntity> getListRollBack() {
		return listRollBack;
	}

	public void setListRollBack(ArrayList<ChangeCosRollbackEntity> listRollBack) {
		this.listRollBack = listRollBack;
	}

	public ArrayList<ChangeCosHistoricoEntity> getListaHistorico() {
		return listaHistorico;
	}

	public void setListaHistorico(ArrayList<ChangeCosHistoricoEntity> listaHistorico) {
		this.listaHistorico = listaHistorico;
	}

	private ArrayList<ChangeCosHistoricoEntity> ToArrayList(ChangeCosHistoricoEntity[] vector) {
		ArrayList<ChangeCosHistoricoEntity> list = new ArrayList<>();
		if (vector.length > 0) {
			for (int i = 0; i < vector.length; i++) {
				list.add(vector[i]);
			}
		}
		return list;
	}

	private ArrayList<ChangeCosRollbackEntity> ToArrayList(ChangeCosRollbackEntity[] vector) {
		ArrayList<ChangeCosRollbackEntity> list = new ArrayList<>();
		if (vector.length > 0) {
			for (int i = 0; i < vector.length; i++) {
				list.add(vector[i]);
			}
		}
		return list;
	}

	private void obtenerUsuario() {
		try {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			this.login = (String) request.getSession().getAttribute("TEMP$USER_NAME");
			// log.info("login: " + login);
		} catch (Exception e) {
			log.error("Error al obtener usuario: ", e);
		}
	}

	public static String dateToTimeString(Calendar fecha) {
		try {
			DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");// ("yyyy-MM-dd");
			// 01/01/0001 00:00:00
			return formatter.format(fecha.getTime());
		} catch (Exception e) {
			return null;
		}
	}

	public void consumirChangeCosRollBack(String isdn) {
		String wsdlLoc = Parametros.chcWsdl;
		try {
			if (isdn != null && !isdn.trim().isEmpty()) {
				long ini = System.currentTimeMillis();
				NodoServicio<ChangeCostWSSoapBindingStub> portNodo = ServicioChangeCos.initPort(wsdlLoc, "CHC" + login, Parametros.chcTimeOut);

				if (portNodo != null && portNodo.getPort() != null) {
					synchronized (portNodo) {
						try {
							ChangeCostWSSoapBindingStub port = portNodo.getPort();
							ResponseRollback response = port.processRollback(isdn);
							long fin = System.currentTimeMillis();
							log.debug("[isdn: " + isdn + "] Tiempo de respuesta del servicio ChangeCostService: " + (fin - ini) + " milisegundos");
							if (response != null && response.getCodeReponse() > 0) {
								SysMessage.info(response.getDecripcion(), null);
							}
						} finally {
							portNodo.setEnUso(false);
							portNodo.setFechaFin(new Date());
						}
					}
				}
			}
		} catch (AxisFault a) {
			String msg = a.getMessage();
			msg = msg.toLowerCase();
			if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase()) || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase())
					|| msg.contains("UnknownHostException".toLowerCase()) || msg.contains("No route to host".toLowerCase()) || msg.contains("Connection refused".toLowerCase())) {
				log.error("Error de conexion al servicio ChangeCos: ", a);
				SysMessage.error("Error de conexion del servicio ChangeCos", null);
				// guardarExcepcion(idAuditoria,
				// "Error de conexion al servicio ARServices: " +
				// stackTraceToString(a));
			} else {
				log.error("Error AxisFault Billerera: ", a);
				SysMessage.error("Error AxisFault de conexion al servicio ChangeCos", null);
				// guardarExcepcion(idAuditoria,
				// "Error AxisFault de conexion al servicio
				// ARServices:
				// " + stackTraceToString(a));
			}

		} catch (ServiceException e) {
			log.error("Error de servicio al conectarse al servicio ChangeCos: ", e);
			SysMessage.error("Error de servicio al conectarse al servicio ChangeCos", null);
			// guardarExcepcion(idAuditoria,
			// "Error de servicio al conectarse al servicio
			// ARServices: " + stackTraceToString(e));
		} catch (RemoteException e) {
			log.error("Error remoto al conectarse al servicio ChangeCos: ", e);
			SysMessage.error("Error remoto al conectarse al servicio ChangeCos", null);
			// guardarExcepcion(idAuditoria,
			// "Error remoto al conectarse al servicio ARServices: "
			// + stackTraceToString(e));
		} catch (Exception e) {
			log.error("Error al consultar ChangeCos ", e);
			SysMessage.error("Error al consultar ChangeCos", null);
			// guardarExcepcion(idAuditoria, "Error al consultar
			// ARServices: " + stackTraceToString(e));
		}
	}

	public void consumirChangeCosProcess(String isdn) {
		String wsdlLoc = Parametros.chcWsdl;
		try {
			if (isdn != null && !isdn.trim().isEmpty()) {
				long ini = System.currentTimeMillis();
				NodoServicio<ChangeCostWSSoapBindingStub> portNodo = ServicioChangeCos.initPort(wsdlLoc, "CHC" + login, Parametros.chcTimeOut);

				if (portNodo != null && portNodo.getPort() != null) {
					synchronized (portNodo) {
						try {
							ChangeCostWSSoapBindingStub port = portNodo.getPort();
							Response response = port.process(isdn);
							long fin = System.currentTimeMillis();
							log.debug("[isdn: " + isdn + "] Tiempo de respuesta del servicio ChangeCostService: " + (fin - ini) + " milisegundos");

							if (response != null && response.getCodeReponse() == 1) {
								TarifaRebajada tarifaRebajada = response.getTarifaRebajada();
								if (tarifaRebajada != null) {
									this.disable = false;
									SysMessage.info("Tiene Tarifa Rebajada la cuenta: " + isdn, null);

									this.planCos = tarifaRebajada.getPlanCos();
									this.descripcion = tarifaRebajada.getPlanCosDescripcion();
									ChangeCosHistoricoEntity[] vecHistorico = tarifaRebajada.getChangeCosHistorico();
									ChangeCosRollbackEntity[] vecRollback = tarifaRebajada.getChangeCosRollback();

									listaHistorico = ToArrayList(vecHistorico);
									listRollBack = ToArrayList(vecRollback);
								}
							} else {
								SysMessage.info("No tiene Tarifa Rebajada la cuenta: " + isdn, null);
							}
						} finally {
							portNodo.setEnUso(false);
							portNodo.setFechaFin(new Date());
						}
					}
				}
			}
		} catch (AxisFault a) {
			String msg = a.getMessage();
			msg = msg.toLowerCase();
			if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase()) || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase())
					|| msg.contains("UnknownHostException".toLowerCase()) || msg.contains("No route to host".toLowerCase()) || msg.contains("Connection refused".toLowerCase())) {
				log.error("Error de conexion al servicio ChangeCos: ", a);
				SysMessage.error("Error de conexion del servicio ChangeCos", null);
				// guardarExcepcion(idAuditoria,
				// "Error de conexion al servicio ARServices: " +
				// stackTraceToString(a));
			} else {
				log.error("Error AxisFault Billerera: ", a);
				SysMessage.error("Error AxisFault de conexion al servicio ChangeCos", null);
				// guardarExcepcion(idAuditoria,
				// "Error AxisFault de conexion al servicio
				// ARServices:
				// " + stackTraceToString(a));
			}

		} catch (ServiceException e) {
			log.error("Error de servicio al conectarse al servicio ChangeCos: ", e);
			SysMessage.error("Error de servicio al conectarse al servicio ChangeCos", null);
			// guardarExcepcion(idAuditoria,
			// "Error de servicio al conectarse al servicio
			// ARServices: " + stackTraceToString(e));
		} catch (RemoteException e) {
			log.error("Error remoto al conectarse al servicio ChangeCos: ", e);
			SysMessage.error("Error remoto al conectarse al servicio ChangeCos", null);
			// guardarExcepcion(idAuditoria,
			// "Error remoto al conectarse al servicio ARServices: "
			// + stackTraceToString(e));
		} catch (Exception e) {
			log.error("Error al consultar ChangeCos ", e);
			SysMessage.error("Error al consultar ChangeCos", null);
			// guardarExcepcion(idAuditoria, "Error al consultar
			// ARServices: " + stackTraceToString(e));
		}

	}

}
