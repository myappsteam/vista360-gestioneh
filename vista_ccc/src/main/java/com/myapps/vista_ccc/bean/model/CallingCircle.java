package com.myapps.vista_ccc.bean.model;

import java.io.Serializable;
import java.util.List;

public class CallingCircle implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String primaryIdentity;
	private List<CallingCircleProperties> callingCirclesProperties;

	public CallingCircle() {
		super();
	}

	public CallingCircle(String primaryIdentity) {
		super();
		this.primaryIdentity = primaryIdentity;

	}

	public String getprimaryIdentity() {
		return primaryIdentity;
	}

	public void setprimaryIdentity(String primaryIdentity) {
		this.primaryIdentity = primaryIdentity;
	}

	@Override
	public String toString() {
		return "CallingCircle [primaryIdentity=" + primaryIdentity + "]";
	}

	public List<CallingCircleProperties> getCallingCirclesProperties() {
		return callingCirclesProperties;
	}

	public void setCallingCirclesProperties(List<CallingCircleProperties> callingCirclesProperties) {
		this.callingCirclesProperties = callingCirclesProperties;
	}

}
