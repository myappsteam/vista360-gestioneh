package com.myapps.vista_ccc.bean.model;

import java.io.Serializable;

public class CallingCircleProperties implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String code;
	private String value;

	public CallingCircleProperties() {
		super();
	}

	public CallingCircleProperties(String code, String value) {
		super();
		this.code = code;
		this.value = value;

	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "CallingCircleProperties [code=" + code + ", value=" + value + "]";
	}

}
