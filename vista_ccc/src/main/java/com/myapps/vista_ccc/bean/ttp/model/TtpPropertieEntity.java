package com.myapps.vista_ccc.bean.ttp.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by michael on 8/4/2020.
 */
@Entity
@Table(name = "TTP_PROPERTIE")
public class TtpPropertieEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    private long idPropertie;
    private String nombre;
    private String tipo;
    private String valor;
    private String descripcion;
    private TtpTipoPropertieEntity ttpTipoPropertieEntity;

    @Id
    @Column(name = "ID_PROPERTIE")
    public long getIdPropertie() {
        return idPropertie;
    }

    public void setIdPropertie(long idPropertie) {
        this.idPropertie = idPropertie;
    }

    @Basic
    @Column(name = "NOMBRE")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "TIPO")
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Basic
    @Column(name = "VALOR")
    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    @Basic
    @Column(name = "DESCRIPCION")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @ManyToOne
    @JoinColumn(name = "TIPO_PARAMETRO", referencedColumnName = "ID_TIPO_PROPERTIE", nullable = false)
    public TtpTipoPropertieEntity getTtpTipoPropertieEntity() {
        return ttpTipoPropertieEntity;
    }

    public void setTtpTipoPropertieEntity(TtpTipoPropertieEntity ttpTipoPropertieEntity) {
        this.ttpTipoPropertieEntity = ttpTipoPropertieEntity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TtpPropertieEntity that = (TtpPropertieEntity) o;

        if (idPropertie != that.idPropertie) return false;
        if (nombre != null ? !nombre.equals(that.nombre) : that.nombre != null) return false;
        if (tipo != null ? !tipo.equals(that.tipo) : that.tipo != null) return false;
        if (valor != null ? !valor.equals(that.valor) : that.valor != null) return false;
        if (descripcion != null ? !descripcion.equals(that.descripcion) : that.descripcion != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (idPropertie ^ (idPropertie >>> 32));
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        result = 31 * result + (tipo != null ? tipo.hashCode() : 0);
        result = 31 * result + (valor != null ? valor.hashCode() : 0);
        result = 31 * result + (descripcion != null ? descripcion.hashCode() : 0);
        return result;
    }
}
