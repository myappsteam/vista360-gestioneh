package com.myapps.vista_ccc.bean.model;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ManagedBean(name = "checkboxService", eager = true)
@ApplicationScoped
public class CheckboxService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<Checkbox> listCheckbox;

	@PostConstruct
	public void init() {
		setListCheckbox(new ArrayList<Checkbox>());
	}

	public List<Checkbox> getListCheckbox() {
		return listCheckbox;
	}

	public void setListCheckbox(List<Checkbox> listCheckbox) {
		this.listCheckbox = listCheckbox;
	}
}
