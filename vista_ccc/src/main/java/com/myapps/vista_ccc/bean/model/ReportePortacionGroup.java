package com.myapps.vista_ccc.bean.model;

import myapps.org.soap.mnppdaportacion.ClienteType;

import java.io.Serializable;
import java.util.List;

public class ReportePortacionGroup implements Serializable{
	
	private static final long serialVersionUID = 1L;
		
	private ClienteType clienteType;
	private List<DetallePortacionPDF> listaDetallePortacionPDF;
			
	public ClienteType getClienteType() {
		return clienteType;
	}

	public List<DetallePortacionPDF> getListaDetallePortacionPDF() {
		return listaDetallePortacionPDF;
	}

	public void setListaDetallePortacionPDF(List<DetallePortacionPDF> listaDetallePortacionPDF) {
		this.listaDetallePortacionPDF = listaDetallePortacionPDF;
	}

	public void setClienteType(ClienteType clienteType) {
		this.clienteType = clienteType;
	}

}
