package com.myapps.vista_ccc.bean.model;

import java.io.Serializable;

public class FreeUnitMeasure implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String measureUnit;
	private String measureUnitName;
	private double dividendo;
	private String unidad;

	public FreeUnitMeasure() {
		super();
	}

	public FreeUnitMeasure(String measureUnit, String measureUnitName, double dividendo, String unidad) {
		super();
		this.measureUnit = measureUnit;
		this.measureUnitName = measureUnitName;
		this.dividendo = dividendo;
		this.unidad = unidad;
	}

	public String getMeasureUnit() {
		return measureUnit;
	}

	public void setMeasureUnit(String measureUnit) {
		this.measureUnit = measureUnit;
	}

	public String getMeasureUnitName() {
		return measureUnitName;
	}

	public void setMeasureUnitName(String measureUnitName) {
		this.measureUnitName = measureUnitName;
	}

	public double getDividendo() {
		return dividendo;
	}

	public void setDividendo(double dividendo) {
		this.dividendo = dividendo;
	}

	public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	@Override
	public String toString() {
		return "FreeUnitMeasure [measureUnit=" + measureUnit + ", measureUnitName=" + measureUnitName + ", dividendo=" + dividendo + ", unidad=" + unidad + "]";
	}

}
