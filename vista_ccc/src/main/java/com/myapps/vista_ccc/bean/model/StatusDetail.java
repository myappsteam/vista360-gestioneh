package com.myapps.vista_ccc.bean.model;

import java.io.Serializable;

public class StatusDetail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String status;
	private String detail;
	private String currentStatusIndex;
	private String rblacklistStatus;
	
	
	public String getCurrentStatusIndex() {
		return currentStatusIndex;
	}

	public void setCurrentStatusIndex(String currentStatusIndex) {
		this.currentStatusIndex = currentStatusIndex;
	}

	public String getRblacklistStatus() {
		return rblacklistStatus;
	}

	public void setRblacklistStatus(String rblacklistStatus) {
		this.rblacklistStatus = rblacklistStatus;
	}

	

	public StatusDetail() {
		super();
	}

	public StatusDetail(String status, String detail,String rblacklistStatus, String currentStatusIndex) {
		super();
		this.status = status;
		this.detail = detail;
		this.rblacklistStatus = rblacklistStatus;
		this.currentStatusIndex = currentStatusIndex;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status= status;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	@Override
	public String toString() {
		return "StatusDetail [status=" + status + ", rblacklistStatus=" + rblacklistStatus + ", currentStatusIndex=" + currentStatusIndex + ", detail=" + detail + "]";
	}

}
