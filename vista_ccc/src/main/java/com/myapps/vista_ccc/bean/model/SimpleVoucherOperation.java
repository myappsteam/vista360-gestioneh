package com.myapps.vista_ccc.bean.model;

import java.io.Serializable;

public class SimpleVoucherOperation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String operationType;
	private String operationDate;
	private String operationReason;

	public SimpleVoucherOperation() {
		super();
	}

	public SimpleVoucherOperation(String operationType, String operationDate, String operationReason) {
		super();
		this.operationType = operationType;
		this.operationDate = operationDate;
		this.operationReason = operationReason;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public String getOperationDate() {
		return operationDate;
	}

	public void setOperationDate(String operationDate) {
		this.operationDate = operationDate;
	}

	public String getOperationReason() {
		return operationReason;
	}

	public void setOperationReason(String operationReason) {
		this.operationReason = operationReason;
	}

	@Override
	public String toString() {
		return "SimpleVoucherOperation [operationType=" + operationType + ", operationDate=" + operationDate + ", operationReason=" + operationReason + "]";
	}

}
