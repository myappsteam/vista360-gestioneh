package com.myapps.vista_ccc.bean.model;

import java.io.Serializable;

public class NumeroFavorito implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String serialNo;
	private String favorito;
	private String tipo;
	private String grupo;
	private String color;

	public NumeroFavorito() {
		super();
	}

	public NumeroFavorito(String serialNo, String favorito, String tipo, String grupo, String color) {
		super();
		this.serialNo = serialNo;
		this.favorito = favorito;
		this.tipo = tipo;
		this.grupo = grupo;
		this.color = color;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getFavorito() {
		return favorito;
	}

	public void setFavorito(String favorito) {
		this.favorito = favorito;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	@Override
	public String toString() {
		return "NumeroFavorito [serialNo=" + serialNo + ", favorito=" + favorito + ", tipo=" + tipo + ", grupo=" + grupo + "]";
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

}
