package com.myapps.vista_ccc.bean;

import com.myapps.user.bean.ControlerBitacora;
import com.myapps.user.ldap.DescriptorBitacora;
import com.myapps.vista_ccc.bean.model.DetallePortacionPDF;
import com.myapps.vista_ccc.bean.model.ImagenMnpda;
import com.myapps.vista_ccc.bean.model.ReportePortacionGroup;
import com.myapps.vista_ccc.bean.model.RespaldoPortacion;
import com.myapps.vista_ccc.servicio.NodoServicio;
import com.myapps.vista_ccc.servicio.ServicioMnpdaPortacion;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.SysMessage;
import com.myapps.vista_ccc.util.UtilDate;
import com.myapps.vista_ccc.util.UtilNumber;
import myapps.org.soap.mnppdaportacion.*;
import org.apache.axis.AxisFault;
import org.apache.commons.net.util.Base64;
import org.apache.log4j.Logger;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.xml.rpc.ServiceException;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@ManagedBean
@ViewScoped
public class portacionmnpda implements Serializable{
	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(portacionmnpda.class);
	
	private String login;
	private String nroDocumento;
	private String nroCuenta;
	private List<ReportePortacionGroup> listPortaciones;
	
	private RespaldoPortacion respaldoSelected;		
	private boolean visibleDialogResp = false;
	
	@Inject
	private ControlerBitacora controlerBitacora;
				
	@PostConstruct
	public void init() {
		try {
			this.login = "";	
			this.nroCuenta = "";
			this.nroDocumento="";
			this.respaldoSelected = new RespaldoPortacion();
			obtenerUsuario();
			listPortaciones = new ArrayList<>();	
		} catch (Exception e) {
			log.error("Error al iniciar, " + e);
		}
	}
		
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
			
	public String getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public String getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(String nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	
	public List<ReportePortacionGroup> getListPortaciones() {
		return listPortaciones;
	}

	public void setListPortaciones(List<ReportePortacionGroup> listPortaciones) {
		this.listPortaciones = listPortaciones;
	}
		
	public RespaldoPortacion getRespaldoSelected() {
		return respaldoSelected;
	}

	public void setRespaldoSelected(RespaldoPortacion respaldoSelected) {
		this.respaldoSelected = respaldoSelected;
	}

	public boolean isVisibleDialogResp() {
		return visibleDialogResp;
	}

	public void setVisibleDialogResp(boolean visibleDialogResp) {
		this.visibleDialogResp = visibleDialogResp;
	}

	private void obtenerUsuario() {
		try {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			this.login = (String) request.getSession().getAttribute("TEMP$USER_NAME");						
		} catch (Exception e) {
			log.error("Error al obtener usuario: ", e);
		}
	}
	
	public void limpiarNroCuenta(AjaxBehaviorEvent event) {		  			
		((UIOutput)event.getSource()).setValue("");		
	}

	public void limpiarNroDoc(AjaxBehaviorEvent event) {		  			
		((UIOutput)event.getSource()).setValue("");		
	}
	
	public void  limpiar() {
		this.nroCuenta = "";
		this.nroDocumento = "";
		this.listPortaciones = new ArrayList<>();
		this.visibleDialogResp = false;
		this.respaldoSelected = new RespaldoPortacion();
	}
	
	public void buscar() {						
		log.debug("ingresando a vefificar MNPDA-Portacion para Nro Cuenta: " + this.nroCuenta +" - Nro Doc: " + this.nroDocumento);
		controlerBitacora.accion(DescriptorBitacora.CONSULTA_MNPDA, "Se hizo consulta del Nro Cuenta: " + this.nroCuenta +" - Nro Doc: " + this.nroDocumento);
		this.listPortaciones = new ArrayList<>();
		
		if(validar(this.nroDocumento, this.nroCuenta)){ 			
			if(!"".equals(this.nroDocumento)){
				consumirMnpdaServices(this.nroDocumento,true);						
			}else{
				consumirMnpdaServices(this.nroCuenta,false);				
			}			
		}		
	}
	
	private Boolean validar(String nroDoc, String nroCuenta){
		boolean sw = true;	
		
		if( ("".equals(nroDoc.trim()) && "".equals(nroCuenta.trim()))){			
			SysMessage.warn("Ingrese un criterio de busqueda ", null);			
			sw=false;
		}else if((nroCuenta != null && !nroCuenta.equals("")) && !UtilNumber.esNroTigo(nroCuenta) && nroDoc.equals("")){
			log.debug("ingresando a vefificar portacion mnpda para nro: " + nroCuenta);
			this.nroCuenta = "";			
			SysMessage.warn("Nro cuenta no valido", null);		
			sw=false;
		}	
		
		return sw;		
	}
	
	public String mostrarRespaldo() {		
		visibleDialogResp = true;
		controlerBitacora.accion(DescriptorBitacora.CONSULTA_MNPDA, "Ingreso a ver respaldo de la Portacion ID: " + respaldoSelected.getPortacionID());
		return "";
	}
		
	public String focusing(){
		if(this.nroCuenta.equals("")){
			return "itNroDoc";
		}else{
			return "itIsdn";	
		}		
	}	
	
	public void  consumirMnpdaServices(String valor, boolean isNroDoc) {
		String wsdlLoc = Parametros.mnpdaWsdl;
		try {					
			if (valor != null && !valor.trim().isEmpty()) {
				long ini = System.currentTimeMillis();
				NodoServicio<MNPPortacionPortStub> portNodo = ServicioMnpdaPortacion.initPort(wsdlLoc, "MNPDA" + login, Parametros.mnpdaTimeOut);
				
				if (portNodo != null && portNodo.getPort() != null) {
					synchronized (portNodo) {
						try {
							MNPPortacionPortStub port = portNodo.getPort();
							
							RequestBodySolicitudPortacionType requestBody =  new RequestBodySolicitudPortacionType();																							
							if(isNroDoc){
								requestBody.setNrodoc(valor);
								requestBody.setTelefono("");
							}else{
								requestBody.setTelefono(valor);
								requestBody.setNrodoc("");
							}
							
							RequestHeader requestHeader = new RequestHeader();
							requestHeader.setConsumerID(Parametros.mnpdaComsumerId);
							requestHeader.setCorrelationID(Parametros.mnpdaCorrelationId);
							requestHeader.setCountry(Parametros.mnpdaCountry);
							requestHeader.setTransactionID(Parametros.mnpdaTransaccionId);
							
							SolicitudPortacionRequestType type = new SolicitudPortacionRequestType();
							type.setRequestHeader(requestHeader);
							type.setRequestBody(requestBody);
							
							SolicitudPortacionResponseType response = port.getSolicitudesPortacion(type);	
							
							long fin = System.currentTimeMillis();
							log.debug("[valor: " + valor + "] Tiempo de respuesta del servicio mnpdaPortacion: " + (fin - ini) + " milisegundos");
							
							if (response != null && response.getCodeResponse() == 1 ) {
								ReportePortacionGroup rGroup = new ReportePortacionGroup();
								
								ClienteType clientType= response.getDatosCliente();						
																	
								if(response.getPortaciones()!=null){
									
									DetallePortacion detallePortacion[] = response.getPortaciones();																	
									List<DetallePortacionPDF> childrens = new ArrayList<>();
									
									for (int i = 0; i < detallePortacion.length; i++) {
										DetallePortacion detalle= detallePortacion[i];										
										String fecha =  UtilDate.stringToCalendarXML(detalle.getFechaPortacion());									
										detalle.setFechaPortacion(fecha);
										
										DetallePortacionPDF detallePortacionPDF = new DetallePortacionPDF();
										detallePortacionPDF.setDetallePortacion(detalle);																																							
										RespaldoPortacion pdfDoc = obtenerPdfRespaldos(detalle.getPortacionCuentaId());	
									
																										
										detallePortacionPDF.setRespaldo(pdfDoc);
										
										childrens.add(detallePortacionPDF);	
									}										
									rGroup.setClienteType(clientType);
									rGroup.setListaDetallePortacionPDF(childrens);		
									listPortaciones.add(rGroup);
									SysMessage.info("Busqueda satisfactoria: " + valor , null);
								}else{
									SysMessage.info("No cuenta con datos de portación para: " + valor , null);
								}
							}else{
								SysMessage.warn("Error al consultar el servicio de MNPDA-Portacion " + valor, null);
							}
						}finally{
							portNodo.setEnUso(false);
							portNodo.setFechaFin(new Date());
						}
					}					
				}
			}			
		} catch (AxisFault a) {
			String msg = a.getMessage();
			msg = msg.toLowerCase();
			if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase()) || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase())
					|| msg.contains("UnknownHostException".toLowerCase()) || msg.contains("No route to host".toLowerCase()) || msg.contains("Connection refused".toLowerCase())) {
				log.error("Error de conexion al servicio MNPDA-Portacion: ", a);
				SysMessage.error("Error de conexion del servicio MNPDA-Portacion", null);				
			} else {
				log.error("Error AxisFault Billetera: ", a);
				SysMessage.error("Error AxisFault de conexion al servicio MNPDA-Portacion", null);				
			}
		} catch (ServiceException e) {
			log.error("Error de servicio al conectarse al servicio MNPDA-Portacion: ", e);
			SysMessage.error("Error de servicio al conectarse al servicio MNPDA-Portacion", null);			
		} catch (RemoteException e) {
			log.error("Error remoto al conectarse al servicio MNPDA-Portacion: ", e);
			SysMessage.error("Error remoto al conectarse al servicio MNPDA-Portacion", null);			
		} catch (Exception e) {
			log.error("Error al consultar MNPDA-Portacion ", e);
			SysMessage.error("Error al consultar MNPDA-Portacion", null);			
		}		
	}
	
	public RespaldoPortacion obtenerPdfRespaldos (String portacionID) {
		String wsdlLoc = Parametros.mnpdaWsdl;
		RespaldoPortacion respaldoPortacion = null;
		respaldoPortacion = new RespaldoPortacion();
		try {
			if (portacionID != null && !portacionID.trim().isEmpty()) {
				long ini = System.currentTimeMillis();
				NodoServicio<MNPPortacionPortStub> portNodo = ServicioMnpdaPortacion.initPort(wsdlLoc, "MNPDA" + login, Parametros.mnpdaTimeOut);
				
				if (portNodo != null && portNodo.getPort() != null) {
					synchronized (portNodo) {
						try {
							MNPPortacionPortStub port = portNodo.getPort();							
							//RequestBodySolicitudPortacionType requestBody =  new RequestBodySolicitudPortacionType();																																					
							RequestHeader requestHeader = new RequestHeader();
							requestHeader.setConsumerID(Parametros.mnpdaComsumerId);
							requestHeader.setCorrelationID(Parametros.mnpdaCorrelationId);
							requestHeader.setCountry(Parametros.mnpdaCountry);
							requestHeader.setTransactionID(Parametros.mnpdaTransaccionId);
							
							RequestBodySocitudPDFType body =  new RequestBodySocitudPDFType();
							body.setPortacionCuentaId(portacionID);
							
							SolicitudPDFRequestType type = new SolicitudPDFRequestType();
							
							type.setRequestHeader(requestHeader);
							type.setRequestBody(body);
														
							SolicitudPDFResponseType response = port.getPDFSolicitud(type);	
							
							long fin = System.currentTimeMillis();
							log.debug("[valor: " + portacionID + "] Tiempo de respuesta del servicio mnpdaPortacion: " + (fin - ini) + " milisegundos");
							
							if (response != null && response.getCodeResponse() ==1 ) {		
								log.info("Obtener getReportePDF Portacion ID: " + portacionID  + " - "+ response.getCodeDescripcion());
												
								if(response.getPdfReporte() != null){
									log.info("Obtuvo el Reporte de portacion Exitoso: " + portacionID);
									String pdfBase64 = response.getPdfReporte();								
									byte[] bytes =Base64.decodeBase64(pdfBase64);								
									InputStream stream = new ByteArrayInputStream(bytes);
									//InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/resources/MU_CBS.pdf");								
									StreamedContent streamedContentPdf = new DefaultStreamedContent(stream, "application/pdf", "formulario.pdf");
									respaldoPortacion.setStreamedContentPDF(streamedContentPdf);
								}
								
								if(response.getPdfReporteDeclaracion() != null){
									log.info("Obtuvo el Reporte de Declarion exitoso Exitoso: " + portacionID);
									String pdfBase64 = response.getPdfReporteDeclaracion();								
									byte[] bytes =Base64.decodeBase64(pdfBase64);								
									InputStream stream = new ByteArrayInputStream(bytes);																
									StreamedContent streamedContentDeclaracion = new DefaultStreamedContent(stream, "application/pdf", "declaracion.pdf");																		
									respaldoPortacion.setStreamedContentDeclaracion(streamedContentDeclaracion);
								}
																						
								if(response.getAttachments() != null){
									log.info("Obtuvo los Resplado Exitoso: " + portacionID);
									List<ImagenMnpda> listaImagenes = new ArrayList<>();									
									AttachmentType[] attch = response.getAttachments();																																								
									for(int i = 0; i< attch.length; i++){
										ImagenMnpda imag= new ImagenMnpda();
										
										String nombreImg = attch[i].getNombre();																																											
									    String contBase64 = "data:image/png;base64," + attch[i].getContenido();
										
										imag.setPortacionId(Integer.parseInt(portacionID));
										imag.setNombre(nombreImg);
										imag.setContBase64(contBase64);

										listaImagenes.add(imag);
									}
									respaldoPortacion.setListaImagenes(listaImagenes);									
									log.info("");																
								}															
							}else{
								SysMessage.warn("No se encuentra el reporte para la portación: " + portacionID, null);
							}
						}finally{
							portNodo.setEnUso(false);
							portNodo.setFechaFin(new Date());							
						}
					}					
				}
			}
			
		} catch (AxisFault a) {
			String msg = a.getMessage();
			msg = msg.toLowerCase();
			if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase()) || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase())
					|| msg.contains("UnknownHostException".toLowerCase()) || msg.contains("No route to host".toLowerCase()) || msg.contains("Connection refused".toLowerCase())) {
				log.error("Error de conexion al servicio MNPDA-Portacion: ", a);
				SysMessage.error("Error de conexion del servicio MNPDA-Portacion", null);
			} else {
				log.error("Error AxisFault Billerera: ", a);
				SysMessage.error("Error AxisFault de conexion al servicio MNPDA-Portacion", null);
			}

		} catch (ServiceException e) {
			log.error("Error de servicio al conectarse al servicio ChangeCos: ", e);
			SysMessage.error("Error de servicio al conectarse al servicio MNPDA-Portacion", null);
		} catch (RemoteException e) {
			log.error("Error remoto al conectarse al servicio MNPDA-Portacion: ", e);
			SysMessage.error("Error remoto al conectarse al servicio MNPDA-Portacion", null);
		} catch (Exception e) {
			log.error("Error al consultar ChangeCos ", e);
			SysMessage.error("Error al consultar MNPDA-Portacion", null);	
		}
		
		respaldoPortacion.setPortacionID(portacionID);
		return respaldoPortacion;
	}
	
			
}
