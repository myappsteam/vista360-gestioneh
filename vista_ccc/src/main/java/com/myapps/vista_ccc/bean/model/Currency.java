package com.myapps.vista_ccc.bean.model;

import java.io.Serializable;

public class Currency implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String code;
	private String description;

	public Currency() {
		super();
	}

	public Currency(Integer id, String code, String description) {
		super();
		this.id = id;
		this.code = code;
		this.description = description;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Currency [id=" + id + ", code=" + code + ", description=" + description + "]";
	}

}
