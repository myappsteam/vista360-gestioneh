package com.myapps.vista_ccc.dao;

import com.myapps.vista_ccc.entity.VcIncidente;
import com.myapps.vista_ccc.entity.VcLogAdicional;
import com.myapps.vista_ccc.entity.VcLogAuditoria;

import javax.annotation.Resource;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import java.io.Serializable;
import java.util.*;






@Named
public class MasterDao implements Serializable {

	private static final long serialVersionUID = 1L;

	@PersistenceContext(unitName = "vista_ccc")
	private transient EntityManager entityManager;

	@Resource
	private transient UserTransaction transaction;

	public void save(Object entidad) throws Exception {
		transaction.begin();
		entityManager.joinTransaction();
		try {
			entityManager.persist(entidad);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
			throw e;
		}
	}

	public void remove(Object entidad) throws Exception {
		transaction.begin();
		entityManager.joinTransaction();
		try {
			entityManager.remove(entityManager.contains(entidad) ? entidad : entityManager.merge(entidad));
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
			throw e;
		}
	}

	public void update(Object entidad) throws Exception {
		transaction.begin();
		entityManager.joinTransaction();
		try {
			entityManager.merge(entidad);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
			throw e;
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object find(Object key, Class clase) throws Exception {
		try {
			return entityManager.find(clase, key);
		} catch (Exception e) {
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public VcIncidente getIncidente(long param) {

		String consulta = "SELECT i FROM VcIncidente i WHERE i.vcLogAuditoria.idLogAuditoria = :logAuditoria";
		Query qu = entityManager.createQuery(consulta).setParameter("logAuditoria", param);
		List<VcIncidente> lista = qu.getResultList();
		return lista.isEmpty() ? null : lista.get(0);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List findAllQuery(Class clase, String sql, ParameterQuery parameterQuery) throws Exception {
		Map<String, Object> parametros = parameterQuery != null ? parameterQuery.getParametros() : null;
		try {
			Query query = entityManager.createQuery(sql, clase);
			if (parametros != null) {
				List<String> keys = new ArrayList<String>(parametros.keySet());
				for (String key : keys) {
					query.setParameter(key, parametros.get(key));
				}
			}
			return query.getResultList();
		} catch (Exception e) {
			throw e;
		}
	}

	@SuppressWarnings({ "rawtypes" })
	public List findAllNativeQuery(String sql, ParameterQuery parameterQuery) throws Exception {
		Map<String, Object> parametros = parameterQuery != null ? parameterQuery.getParametros() : null;
		try {
			Query query = entityManager.createNativeQuery(sql);
			if (parametros != null) {
				List<String> keys = new ArrayList<String>(parametros.keySet());
				for (String key : keys) {
					query.setParameter(key, parametros.get(key));
				}
			}
			return query.getResultList();
		} catch (Exception e) {
			throw e;
		}
	}

	@SuppressWarnings({ "rawtypes" })
	public List findAllNativeQuery(Class clase, String sql, ParameterQuery parameterQuery) throws Exception {
		Map<String, Object> parametros = parameterQuery != null ? parameterQuery.getParametros() : null;
		try {
			Query query = entityManager.createNativeQuery(sql, clase);
			if (parametros != null) {
				List<String> keys = new ArrayList<String>(parametros.keySet());
				for (String key : keys) {
					query.setParameter(key, parametros.get(key));
				}
			}
			return query.getResultList();
		} catch (Exception e) {
			throw e;
		}
	}

	public void executeUpdateNativeQuery(String sql, ParameterQuery parameterQuery) throws Exception {
		Map<String, Object> parametros = parameterQuery != null ? parameterQuery.getParametros() : null;
		transaction.begin();
		entityManager.joinTransaction();
		try {
			Query query = entityManager.createNativeQuery(sql);
			if (parametros != null) {
				List<String> keys = new ArrayList<String>(parametros.keySet());
				for (String key : keys) {
					query.setParameter(key, parametros.get(key));
				}
			}
			query.executeUpdate();
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
			throw e;
		}
	}

	public List findAllNativeQuery(Class clase, String sql, ParameterQuery parameterQuery, int first, int pageSize) throws Exception {
		Map<String, Object> parametros = parameterQuery != null ? parameterQuery.getParametros() : null;
		try {
			Query query = entityManager.createNativeQuery(sql, clase);
			if (parametros != null) {
				List<String> keys = new ArrayList<String>(parametros.keySet());
				for (String key : keys) {
					query.setParameter(key, parametros.get(key));
				}
			}
			if (first > 0) {
				query.setFirstResult(first);
			}
			if (pageSize > 0) {
				query.setMaxResults(pageSize);
			}
			return query.getResultList();
		} catch (Exception e) {
			throw e;
		}
	}

	public void guardarAuditoria(VcLogAuditoria entidad, List<VcLogAdicional> entidades) throws Exception {
		transaction.begin();
		entityManager.joinTransaction();
		try {
			entityManager.persist(entidad);

			if (entidades != null && entidades.size() > 0) {
				for (VcLogAdicional entity : entidades) {
					entity.setVcLogAuditoria(entidad);
					entity.setFechaCreacion(Calendar.getInstance());
					entityManager.persist(entity);
				}
			}

			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<VcIncidente> getIncidente(String usuario,String vista,Date FechaIni,Date FechaFin,String atencion,String id) {

		String consulta = "SELECT v.*, VCL.VISTA FROM VC_INCIDENTE v, VC_LOG_AUDITORIA VCL WHERE V.ID_LOG_AUDITORIA = VCL.ID_LOG_AUDITORIA AND v.usuario like :usuario AND VCL.VISTA like :vista AND v.atencion like :atencion and v.id_incidente like :id ";
		if(FechaIni!=null)
			consulta=consulta + " AND v.FECHA_INCIDENTE >= :FechaIni";
		if(FechaFin!=null)
			consulta=consulta+ " AND v.FECHA_INCIDENTE <= :FechaFin ";
		
		Query qu = entityManager.createNativeQuery(consulta,VcIncidente.class);
		qu.setParameter("usuario",usuario);
		qu.setParameter("vista", vista);
		qu.setParameter("atencion", atencion);
		qu.setParameter("id", id);
		
		if(FechaIni!=null)
			qu.setParameter("FechaIni",FechaIni);
		if(FechaFin!=null)
		qu.setParameter("FechaFin",FechaFin);
		
		return qu.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<VcLogAuditoria> getList() {
		return entityManager.createQuery("SELECT v FROM VcLogAuditoria v").getResultList();
	}
	
}
