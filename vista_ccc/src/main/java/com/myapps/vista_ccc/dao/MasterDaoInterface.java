package com.myapps.vista_ccc.dao;

import java.util.List;

public interface MasterDaoInterface {

	public String validar(Object entidad, boolean nuevo);

	public void save(Object entidad) throws Exception;

	public void remove(Object entidad) throws Exception;

	public void update(Object entidad) throws Exception;

	@SuppressWarnings({ "rawtypes" })
	public Object find(Object key, Class clase) throws Exception;

	@SuppressWarnings("rawtypes")
	public List findAll() throws Exception;

}
