package com.myapps.vista_ccc.dao;

import java.util.HashMap;
import java.util.Map;

public class ParameterQuery {

	private Map<String, Object> parametros;

	private ParameterQuery() {
		this.parametros = new HashMap<String, Object>();
	}

	public void put(String nombre, Object valor) {
		parametros.put(nombre, valor);
	}

	public Map<String, Object> getParametros() {
		return parametros;
	}

	public static ParameterQuery getParameterQuery() {
		return new ParameterQuery();
	}

}
