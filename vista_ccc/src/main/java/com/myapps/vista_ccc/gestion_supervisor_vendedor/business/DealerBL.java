package com.myapps.vista_ccc.gestion_supervisor_vendedor.business;


import com.myapps.vista_ccc.gestion_eh.dao.VcDealerDAO;
import com.myapps.vista_ccc.gestion_eh.entity.VcDealerEntity;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
public class DealerBL implements Serializable {

    private static final long serialVersionUID = 1L;
    private static Logger log = Logger.getLogger(com.myapps.vista_ccc.gestion_eh.business.VcDealerZonasBL.class);


    @Inject
    private VcDealerDAO dao;

    public void save(VcDealerEntity nodo) throws Exception {
        dao.save(nodo);
    }

    public VcDealerEntity getVcDealerZona(long idNodo) throws Exception {
        return dao.get(idNodo);
    }

    public void update(VcDealerEntity nodo) throws Exception {
        dao.update(nodo);
    }

    public List<VcDealerEntity> getList() throws Exception {
        return dao.getList();
    }

}
