package com.myapps.vista_ccc.gestion_supervisor_vendedor.bean;

import org.primefaces.model.TreeNode;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;

@ManagedBean
@ViewScoped
public class RoleFormGestionSupVen implements Serializable {
    private static final long serialVersionUID = 1L;
    private TreeNode roles;
    private TreeNode[] nodosSeleccionados;



    public TreeNode getRoles() {
        return roles;
    }

    public void setRoles(TreeNode roles) {
        this.roles = roles;
    }

    public TreeNode[] getNodosSeleccionados() {
        return nodosSeleccionados;
    }

    public void setNodosSeleccionados(TreeNode[] nodosSeleccionados) {
        this.nodosSeleccionados = nodosSeleccionados;
    }
}
