package com.myapps.vista_ccc.gestion_supervisor_vendedor.model;

import java.io.Serializable;

public class gestionDealerModel implements Serializable {

    private long idNombre;

    private String nombre;

    private long nivel;


    public gestionDealerModel(long idNombre, String nombre, long nivel) {
        this.idNombre = idNombre;
        this.nombre = nombre;
        this.nivel = nivel;
    }
    public gestionDealerModel() {

    }

    public long getIdNombre() {
        return idNombre;
    }

    public void setIdNombre(long idNombre) {
        this.idNombre = idNombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public long getNivel() {
        return nivel;
    }

    public void setNivel(long nivel) {
        this.nivel = nivel;
    }

}
