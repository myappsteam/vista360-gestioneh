package com.myapps.vista_ccc.gestion_supervisor_vendedor.dao;

import com.myapps.user.model.MuGrupoAd;
import com.myapps.user.model.MuUsuario;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.List;

@Named
public class GestionSupVenDAO implements Serializable {
    private static final long serialVersionUID = 1L;

    @PersistenceContext(unitName = "vista_ccc")
    private transient EntityManager entityManager;

    @SuppressWarnings("unchecked")
    public MuUsuario gestionSupVenLoginmUsuario(String login) {
        Query query;
        String sql = "SELECT M.* FROM MU_USUARIO M WHERE M.LOGIN = :login AND M.ESTADO = 1";
        query = entityManager.createNativeQuery(sql, MuUsuario.class);
        query.setParameter("login", login);
        query.getResultList();
        if (query.getResultList().isEmpty()) {
            return null;
        } else {
            return (MuUsuario) query.getSingleResult();
        }
    }

    @SuppressWarnings("unchecked")
    public MuGrupoAd gestionSupVenLoginGrupUserAD(String nameGroup) {
        Query query;
        String sql = "SELECT G.* FROM MU_GRUPO_AD G WHERE G.NOMBRE = :nameGroup AND G.ESTADO = 1";
        query = entityManager.createNativeQuery(sql, MuGrupoAd.class);
        query.setParameter("nameGroup", nameGroup);
        if (query.getResultList().isEmpty()) {
            return null;
        } else {
            return (MuGrupoAd) query.getSingleResult();
        }
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> rolSupVenPadre(long usuarioID) {
        Query query;
        String sql = "SELECT VD.ID_VC_DEALER AS ID_DEALER, VD.NOMBRE, VD.ESTADO FROM VC_PERMISOS_SUPERVISOR VPS, VC_SUPERVISOR_DEALER_ZONAS VSDZ, VC_DEALER_ZONAS VDZ INNER JOIN VC_DEALER VD ON VDZ.ID_VC_DEALER = VD.ID_VC_DEALER WHERE VPS.USUARIO_ID = :usuarioID AND VPS.ID_PERMISOS_SUPERVISOR = VSDZ.ID_PERMISOS_SUPERVISOR AND VPS.ESTADO = 1 AND VD.ESTADO = 1";
        query = entityManager.createNativeQuery(sql);
        query.setParameter("usuarioID", usuarioID);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> rolSupVenHijo(long usuarioID, long rolId) {
        Query query;
        String sql = "SELECT VZ.ID_VC_ZONAS AS ID_ZONAS, VZ.NOMBRE, VZ.ESTADO FROM VC_PERMISOS_SUPERVISOR VPS, VC_SUPERVISOR_DEALER_ZONAS VSDZ, VC_DEALER_ZONAS VDZ INNER JOIN VC_ZONAS VZ ON VDZ.ID_VC_ZONAS = VZ.ID_VC_ZONAS WHERE VPS.USUARIO_ID = :usuarioID AND VPS.ID_PERMISOS_SUPERVISOR = VSDZ.ID_PERMISOS_SUPERVISOR AND VPS.ESTADO = 1 AND VZ.ESTADO = 1";
        query = entityManager.createNativeQuery(sql);
        query.setParameter("usuarioID", usuarioID);
        return query.getResultList();
    }
}
