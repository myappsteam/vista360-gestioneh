package com.myapps.vista_ccc.gestion_supervisor_vendedor.bean;

import com.myapps.user.bean.ControlerBitacora;
import com.myapps.user.business.UsuarioBL;
import com.myapps.user.ldap.DescriptorBitacora;
import com.myapps.user.model.MuGrupoAd;
import com.myapps.user.model.MuUsuario;
import com.myapps.vista_ccc.gestion_eh.business.VcDealerZonasBL;
import com.myapps.vista_ccc.gestion_eh.business.VcPermisosSupervisorBL;
import com.myapps.vista_ccc.gestion_eh.business.VcSupervisorDealerZOnasBL;
import com.myapps.vista_ccc.gestion_eh.entity.VcDealerEntity;
import com.myapps.vista_ccc.gestion_eh.entity.VcDealerZonasEntity;
import com.myapps.vista_ccc.gestion_eh.entity.VcPermisosSupervisorEntity;
import com.myapps.vista_ccc.gestion_eh.entity.VcSupervisorDealerZonasEntity;
import com.myapps.vista_ccc.gestion_supervisor_vendedor.business.ZonasBL;
import com.myapps.vista_ccc.gestion_supervisor_vendedor.dao.GestionSupVenDAO;
import com.myapps.vista_ccc.gestion_supervisor_vendedor.model.gestionDealerModel;
import com.myapps.vista_ccc.reset_pin.model.gson.QueryUser;
import com.myapps.vista_ccc.servicio.Servicio;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.SysMessage;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.List;
import java.util.Map;

@ManagedBean
@ViewScoped
public class GestionSupervisorVendedorBean implements Serializable {
    @Inject
    private GestionSupVenDAO gestionSupVenDAO;
    @Inject
    private VcPermisosSupervisorBL vcPermisosSupervisorBL;
    @Inject
    private com.myapps.vista_ccc.gestion_supervisor_vendedor.business.DealerBL dealerBL;
    @Inject
    private ZonasBL zonasBL;
    @Inject
    private VcDealerZonasBL vcDealerZonasBL;
    @Inject
    private VcSupervisorDealerZOnasBL vcSupervisorDealerZOnasBL;
    @Inject
    private UsuarioBL usuarioBL;
    @Inject
    private ControlerBitacora controlerBitacora;
    private QueryUser botonesDisable;
    private String nameUser;
    private TreeNode root;
    private String tipoUsuario;
    private TreeNode roles;
    private TreeNode[] nodosSeleccionados;
    public static Logger log = Logger.getLogger(GestionSupervisorVendedorBean.class);
    private MuUsuario muUsuario;
    private MuGrupoAd muGrupoAd;
    private static final String ERROR = "ERROR";
    private Map<String, String> mapResponseWhiteList;
    private String login;
    private String token;
    private boolean disable;
    private String nameGuardarBoton;

    @PostConstruct
    public void init() {
        this.nameGuardarBoton = "Guardar";
        this.disable= true;
        tipoUsuario = "muUsuario";
        obtenerUsuario();
        CargarTreeNode();
        botonesDisable = new QueryUser();
        Servicio.setPagina(this.getClass().getSimpleName());
        Servicio.eliminarConexionesInactivas(this.getClass().getSimpleName());
    }

    public void CargarTreeNode(){
        try {
            List<VcDealerEntity>  listvcDealer = dealerBL.getList();
            root = new DefaultTreeNode("Root", null);
            for(VcDealerEntity item :  listvcDealer ){
                TreeNode node = new DefaultTreeNode(new gestionDealerModel( item.getIdVcDealer(), item.getNombre(), (long) 1), root);
                node.setExpanded(true);
                List<VcDealerZonasEntity>  listvcZonas = vcDealerZonasBL.getListForDealer(item.getIdVcDealer());
                for(VcDealerZonasEntity item2 : listvcZonas){
                    TreeNode node2 = new DefaultTreeNode(new gestionDealerModel( item2.getIdDealerZonas() , item2.getVcZonasByIdVcZonas().getNombre(), (long) 2), node);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cargarTreeNodeDatosTabla2(long rolId, long idPermisoSupervisor){
        try {
            List<VcPermisosSupervisorEntity> vcPermisosSupList;
            if(tipoUsuario.equals("muUsuario")){
                vcPermisosSupList = vcPermisosSupervisorBL.getIdPermisoSupervisor(muUsuario.getUsuarioId());
            } else {
                vcPermisosSupList = vcPermisosSupervisorBL.getIdPermisoSupervisorGrupoID(muGrupoAd.getGrupoId());
            }
            List<VcSupervisorDealerZonasEntity> vcSupDZonas = vcSupervisorDealerZOnasBL.getIdSupVenDelete(idPermisoSupervisor);
            TreeNode node;
            TreeNode node2;
            List<VcDealerZonasEntity>  listvcZonas;
            List<VcDealerEntity>  listvcDealer = dealerBL.getList();
            root = new DefaultTreeNode("Root", null);
            for(VcDealerEntity item :  listvcDealer ){
                node = new DefaultTreeNode(new gestionDealerModel( item.getIdVcDealer(), item.getNombre(), rolId), root);
                node.setExpanded(true);
                listvcZonas = vcDealerZonasBL.getListForDealer(item.getIdVcDealer());
                for(VcDealerZonasEntity item2 : listvcZonas){
                    node2 = new DefaultTreeNode(new gestionDealerModel( item2.getIdDealerZonas() , item2.getVcZonasByIdVcZonas().getNombre(), 2), node);
                    node2.setExpanded(true);
                    for( VcSupervisorDealerZonasEntity itemSupDealerZonas : vcSupDZonas ){
                        //compara idZonas
                        for(VcPermisosSupervisorEntity itemPerSup : vcPermisosSupList){
                            if(tipoUsuario.equals("muUsuario")){
                                if(itemPerSup.getUsuarioId() == rolId && item2.getIdDealerZonas() == itemSupDealerZonas.getVcDealerZonasByIdDealerZonas().getIdDealerZonas() && itemSupDealerZonas.getVcPermisosSupervisorByIdPermisosSupervisor().getIdPermisosSupervisor() == itemPerSup.getIdPermisosSupervisor() ){
                                    node2.setSelected(true);
                                }
                            } else {
                                if(itemPerSup.getGrupoId() == rolId && item2.getIdDealerZonas() == itemSupDealerZonas.getVcDealerZonasByIdDealerZonas().getIdDealerZonas() && itemSupDealerZonas.getVcPermisosSupervisorByIdPermisosSupervisor().getIdPermisosSupervisor() == itemPerSup.getIdPermisosSupervisor() ){
                                    node2.setSelected(true);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e){
            log.error("ERROR GESTION_SUPERVISOR. AL CARGAR DATOS", e);
        }
    }

    public void consultarTablaUsuarios(){
        if (nameUser == null || nameUser.isEmpty()) {
            CargarTreeNode();
            mostrarMensajeGenerico(Parametros.gestionSupVenUsuarioRequerido, "WARN");
        } else if (tipoUsuario == null || tipoUsuario.isEmpty()) {
            CargarTreeNode();
            mostrarMensajeGenerico(Parametros.gestionSupVentipoUsuario, "WARN");
        } else {
            try {
                if(tipoUsuario.equals("muUsuario")){
                    muUsuario = gestionSupVenDAO.gestionSupVenLoginmUsuario(nameUser);
                    if( !validaQueryUser(muUsuario) ){
                        controlerBitacora.accion(DescriptorBitacora.GESTION_SUPERVISOR,
                                "Se realizó la busqueda en Gestion Supervisor con el usuario: " + muUsuario.getLogin());
                        VcPermisosSupervisorEntity dataPermisoSupervisor = vcPermisosSupervisorBL.getConsultaSupervisorPermisoBL(muUsuario.getUsuarioId());
                        if(dataPermisoSupervisor != null){
                            nameGuardarBoton = "Actualizar";
                            disable = false;
                            cargarTreeNodeDatosTabla2(muUsuario.getUsuarioId(), dataPermisoSupervisor.getIdPermisosSupervisor());
                        } else {
                            nameGuardarBoton = "Guardar";
                            disable = false;
                            CargarTreeNode();
                        }
                    } else {
                        CargarTreeNode();
                        nameGuardarBoton = "Guardar";
                        disable = true;
                        mostrarMensajeGenerico(Parametros.gestionSupVenUsuarioNoEncontrado, "WARN");
                    }
                } else if(tipoUsuario.equals("grupoUsuarioAd")){
                    muGrupoAd = gestionSupVenDAO.gestionSupVenLoginGrupUserAD(nameUser);
                    if( !validaQueryUser(muGrupoAd) ) {
                        controlerBitacora.accion(DescriptorBitacora.GESTION_SUPERVISOR,
                                "Se realizó la busqueda en Gestion Supervisor con el usuario: " + muGrupoAd.getNombre());
                        VcPermisosSupervisorEntity dataPermisoSupervisor = vcPermisosSupervisorBL.getConsultaSupervisorPermisoGrupoIDBL(muGrupoAd.getGrupoId());
                        if(dataPermisoSupervisor != null){
                            nameGuardarBoton = "Actualizar";
                            disable = false;
                            cargarTreeNodeDatosTabla2(muGrupoAd.getGrupoId(), dataPermisoSupervisor.getIdPermisosSupervisor());
                        } else {
                            nameGuardarBoton = "Guardar";
                            disable = false;
                            CargarTreeNode();
                        }
                    } else {
                        nameGuardarBoton = "Guardar";
                        CargarTreeNode();
                        disable = true;
                        mostrarMensajeGenerico(Parametros.gestionSupVenUsuarioNoEncontrado, "WARN");
                    }
                }
            } catch (Exception e) {
                log.error("ERROR GESTION_SUPERVISOR. AL CONSULTAR", e);
            }
        }
    }

    public void guardarVcDealerZonas(){
        try {
            eliminarVcDealerZonas(0);
            TreeNode[] nodesds = nodosSeleccionados;
            if(nodesds.length == 0){
                mostrarMensajeGenerico(Parametros.gestionSupVenSeleccionZona, "WARN");
            } else {
                VcPermisosSupervisorEntity vcPermisosSupervisorEntity = new VcPermisosSupervisorEntity();
                VcPermisosSupervisorEntity vcSupervisor;
                if(tipoUsuario.equals("grupoUsuarioAd")){
                    vcSupervisor = vcPermisosSupervisorBL.getConsultaSupervisorPermisoGrupoIDBL(muGrupoAd.getGrupoId());
                    vcPermisosSupervisorEntity.setGrupoId(muGrupoAd.getGrupoId());
                } else {
                    vcSupervisor = vcPermisosSupervisorBL.getSupervisorForUsuario(muUsuario.getUsuarioId(), null);
                    vcPermisosSupervisorEntity.setUsuarioId(muUsuario.getUsuarioId());
                }
                vcPermisosSupervisorEntity.setFecha(new Time(new Date().getTime()));
                vcPermisosSupervisorEntity.setAccion(Parametros.gestionSupVenNameAccion);
                vcPermisosSupervisorEntity.setEstado(true);

                if(vcSupervisor != null ){
                    vcPermisosSupervisorEntity.setIdPermisosSupervisor(vcSupervisor.getIdPermisosSupervisor());
                } else {
                    vcPermisosSupervisorBL.save(vcPermisosSupervisorEntity);
                }

                for(TreeNode nodos : nodosSeleccionados){
                    gestionDealerModel gestionDealerModel = (com.myapps.vista_ccc.gestion_supervisor_vendedor.model.gestionDealerModel) nodos.getData();
                    if( gestionDealerModel.getNivel() == 2 ){
                        VcSupervisorDealerZonasEntity vcDealerZonasEntity = new VcSupervisorDealerZonasEntity();
                        vcDealerZonasEntity.setEstado(true);
                        vcDealerZonasEntity.setVcDealerZonasByIdDealerZonas(vcDealerZonasBL.getVcDealerZona(gestionDealerModel.getIdNombre()));
                        vcDealerZonasEntity.setVcPermisosSupervisorByIdPermisosSupervisor(vcPermisosSupervisorEntity);
                        vcSupervisorDealerZOnasBL.save(vcDealerZonasEntity);
                    }
                }

                try {

                    if(tipoUsuario.equals("muUsuario")){
                        mostrarMensajeGenerico(Parametros.gestionSupVenGuardarCambios, "INFO");
                        controlerBitacora.accion(DescriptorBitacora.GESTION_SUPERVISOR,
                                "Se guardo los cambios de Gestion Supervisor del usuario: " + muUsuario.getLogin());
                    }
                    if(tipoUsuario.equals("grupoUsuarioAd")) {
                        mostrarMensajeGenerico(Parametros.gestionSupVenGuardarCambios, "INFO");
                        controlerBitacora.accion(DescriptorBitacora.GESTION_SUPERVISOR,
                                "Se guardo los cambios de Gestion Supervisor del usuario: " + muGrupoAd.getNombre());
                    }

                } catch (Exception e) {
                    log.error("Error al guardar bitacora en el sistema el metodo guardar: ", e);
                }
                nameGuardarBoton = "Guardar";
                disable = true;
                limpiarTodo();
            }
        } catch (Exception e){
            log.error("Error gestion supervisor: ", e);
        }
    }

    public void eliminarVcDealerZonas(int dataNumber){
        RequestContext.getCurrentInstance().execute("PF('carDelet').hide()");//para que se oculte el poppup
        if (nameUser == null || nameUser.isEmpty()) {
            CargarTreeNode();
            mostrarMensajeGenerico(Parametros.gestionSupVenUsuarioRequerido, "WARN");
        } else {
            try {
                VcPermisosSupervisorEntity dataPermisoSupervisor;
                if(tipoUsuario.equals("muUsuario")){
                    dataPermisoSupervisor = vcPermisosSupervisorBL.getConsultaSupervisorPermisoBL(muUsuario.getUsuarioId());
                } else {
                    dataPermisoSupervisor = vcPermisosSupervisorBL.getConsultaSupervisorPermisoGrupoIDBL(muGrupoAd.getGrupoId());
                }
                List<VcSupervisorDealerZonasEntity> dataDealerZona = vcSupervisorDealerZOnasBL.getIdSupVenDelete(dataPermisoSupervisor.getIdPermisosSupervisor());
                //vamos a ver posibles errores
//                VcPermisosSupervisorEntity vcPermisosSupervisorEntity = new VcPermisosSupervisorEntity();
                dataPermisoSupervisor.setIdPermisosSupervisor(dataPermisoSupervisor.getIdPermisosSupervisor());
                if(tipoUsuario.equals("muUsuario")){
                    dataPermisoSupervisor.setUsuarioId(dataPermisoSupervisor.getUsuarioId());
                } else {
                    dataPermisoSupervisor.setUsuarioId(dataPermisoSupervisor.getGrupoId());
                }
                dataPermisoSupervisor.setFecha(dataPermisoSupervisor.getFecha());
                dataPermisoSupervisor.setAccion(dataPermisoSupervisor.getAccion());
                dataPermisoSupervisor.setEstado(dataPermisoSupervisor.getEstado());
                VcSupervisorDealerZonasEntity vcDealerZonasEntity = new VcSupervisorDealerZonasEntity();
                for( VcSupervisorDealerZonasEntity itemList : dataDealerZona ){
                    vcDealerZonasEntity.setVcPermisosSupervisor(itemList.getVcPermisosSupervisor());
                    vcDealerZonasEntity.setVcPermisosSupervisorByIdPermisosSupervisor(itemList.getVcPermisosSupervisorByIdPermisosSupervisor());
                    vcDealerZonasEntity.setVcDealerZonasByIdDealerZonas(itemList.getVcDealerZonasByIdDealerZonas());
                    vcDealerZonasEntity.setEstado(itemList.getEstado());
                    vcSupervisorDealerZOnasBL.removeDataSupDealerZona(vcDealerZonasEntity);
                }
                if(dataNumber == 1 && tipoUsuario.equals("muUsuario")){
                    limpiarTodoDeshabilitarBoton();
                    mostrarMensajeGenerico(Parametros.gestionSupVenEliminarDealer, "INFO");
                }
                if(dataNumber == 1 && tipoUsuario.equals("grupoUsuarioAd")) {
                    limpiarTodoDeshabilitarBoton();
                    mostrarMensajeGenerico(Parametros.gestionSupVenEliminarDealer, "INFO");
                }
            } catch (Exception e){
                log.info("Error en gestion supervisor no pudo eliminar: ", e);
            }
            try {

                if(dataNumber == 1 && tipoUsuario.equals("muUsuario")){
                    controlerBitacora.accion(DescriptorBitacora.GESTION_SUPERVISOR,
                            "Se elimino de Gestion Supervisor las zonas del usuario: " + muUsuario.getLogin());
                }
                if(dataNumber == 1 && tipoUsuario.equals("grupoUsuarioAd")) {
                    controlerBitacora.accion(DescriptorBitacora.GESTION_SUPERVISOR,
                            "Se elimino de Gestion Supervisor las zonas del usuario: " + muGrupoAd.getNombre());
                }

            } catch (Exception e) {
                log.error("Error al guardar bitacora en el sistema en el metodo eliminar: ", e);
            }
        }
    }

    private boolean validaQueryUser(Object userID){
        if(userID == null || userID.equals("")){
            return true;
        }
        return false;
    }

    public void mostrarMensajeGenerico(String mensaje, String categoria) {
        switch (categoria) {
            case "INFO":
                SysMessage.info(mensaje, null);
                break;
            case "WARN":
                SysMessage.warn(mensaje, null);
                break;
            case ERROR:
                SysMessage.error(mensaje, null);
                break;
            default:
                SysMessage.error("Categoria no Definida " + mensaje, null);
                break;
        }
    }

    private void obtenerUsuario() {
        try {
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                    .getRequest();
            login = (String) request.getSession().getAttribute("TEMP$USER_NAME");
            log.info("Usuario: " + login + ", ingresando a vista de Gestion Supervisor.");
        } catch (Exception e) {
            log.error("Error al obtener usuario: ", e);
        }
    }

    public void limpiarTodo() {
        CargarTreeNode();
        this.nameUser = null;
        this.roles = null;
        this.botonesDisable = new QueryUser();
    }

    public void limpiarTodoDeshabilitarBoton() {
        CargarTreeNode();
        this.nameUser = null;
        this.roles = null;
        this.disable=true;
        this.nameGuardarBoton = "Guardar";
        this.botonesDisable = new QueryUser();
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public TreeNode getRoles() {
        return roles;
    }

    public TreeNode getRoot() {
        return root;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public void setRoles(TreeNode roles) {
        this.roles = roles;
    }

    public TreeNode[] getNodosSeleccionados() {
        return nodosSeleccionados;
    }

    public void setNodosSeleccionados(TreeNode[] nodosSeleccionados) {
        this.nodosSeleccionados = nodosSeleccionados;
    }

    public QueryUser getBotonesDisable() {
        return botonesDisable;
    }

    public void setBotonesDisable(QueryUser botonesDisable) {
        this.botonesDisable = botonesDisable;
    }

    public Map<String, String> getMapResponseWhiteList() {
        return mapResponseWhiteList;
    }

    public void setMapResponseWhiteList(Map<String, String> mapResponseWhiteList) {
        this.mapResponseWhiteList = mapResponseWhiteList;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isDisable() {
        return disable;
    }

    public void setDisable(boolean disable) {
        this.disable = disable;
    }

    public String getNameGuardarBoton() {
        return nameGuardarBoton;
    }

    public void setNameGuardarBoton(String nameGuardarBoton) {
        this.nameGuardarBoton = nameGuardarBoton;
    }
}