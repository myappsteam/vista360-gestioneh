package com.myapps.vista_ccc.gestion_supervisor_vendedor.business;

import com.myapps.vista_ccc.gestion_eh.dao.VcZonasDAO;
import com.myapps.vista_ccc.gestion_eh.entity.VcZonasEntity;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
public class ZonasBL implements Serializable {

    private static final long serialVersionUID = 1L;
    private static Logger log = Logger.getLogger(com.myapps.vista_ccc.gestion_eh.business.VcDealerZonasBL.class);


    @Inject
    private VcZonasDAO dao;

    public void save(VcZonasEntity nodo) throws Exception {
        dao.save(nodo);
    }

    public VcZonasEntity getVcDealerZona(long idNodo) throws Exception {
        return dao.get(idNodo);
    }

    public void update(VcZonasEntity nodo) throws Exception {
        dao.update(nodo);
    }

    public List<VcZonasEntity> getList() throws Exception {
        return dao.getList();
    }

    public List<VcZonasEntity> getListForDealer(long idDealer) throws Exception {
        return dao.getListPorDealer(idDealer);
    }

}
