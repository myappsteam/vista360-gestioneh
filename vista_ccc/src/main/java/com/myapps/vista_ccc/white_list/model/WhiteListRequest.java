package com.myapps.vista_ccc.white_list.model;

import com.google.gson.Gson;
import org.apache.log4j.Logger;

import java.io.Serializable;

public class WhiteListRequest implements Serializable {

	private static Logger logger = Logger.getLogger(WhiteListRequest.class);

	private static final long serialVersionUID = 1L;
	private String msisdn;
	private String notes;
	private String start_user;
	private String start_channel;
	private String end_user;
	private String end_channel;

	public static WhiteListRequest getInstanceForAdd(String msisdn, String notes, String start_user,
                                                     String start_channel) {
		WhiteListRequest whiteList = new WhiteListRequest();
		whiteList.setMsisdn(msisdn);
		whiteList.setNotes(notes);
		whiteList.setStart_user(start_user);
		whiteList.setStart_channel(start_channel);
		return whiteList;
	}

	public static WhiteListRequest getInitByIsdn(String isdn) {
		WhiteListRequest whiteList = new WhiteListRequest();
		whiteList.setMsisdn(isdn);
		return whiteList;
	}

	public static WhiteListRequest getInstanceForDelete(String msisdn, String notes, String end_user,
                                                        String end_channel) {
		WhiteListRequest whiteList = new WhiteListRequest();
		whiteList.setMsisdn(msisdn);
		whiteList.setNotes(notes);
		whiteList.setEnd_user(end_user);
		whiteList.setEnd_channel(end_channel);
		return whiteList;
	}

	public static String convertToJson(WhiteListRequest requestBody) {
		Gson gson = new Gson();
		String res = gson.toJson(requestBody);
		logger.info("Objecto JSON White List construido para request: " + res);
		return res;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getStart_user() {
		return start_user;
	}

	public void setStart_user(String start_user) {
		this.start_user = start_user;
	}

	public String getStart_channel() {
		return start_channel;
	}

	public void setStart_channel(String start_channel) {
		this.start_channel = start_channel;
	}

	@Override
	public String toString() {
		return "WhiteListRequest [msisdn=" + msisdn + ", notes=" + notes + ", start_user=" + start_user
				+ ", start_channel=" + start_channel + ", end_user=" + end_user + ", end_channel=" + end_channel + "]";
	}

	public String getEnd_user() {
		return end_user;
	}

	public void setEnd_user(String end_user) {
		this.end_user = end_user;
	}

	public String getEnd_channel() {
		return end_channel;
	}

	public void setEnd_channel(String end_channel) {
		this.end_channel = end_channel;
	}
}
