package com.myapps.vista_ccc.white_list.bean;

import com.myapps.user.bean.ControlerBitacora;
import com.myapps.user.ldap.DescriptorBitacora;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.SysMessage;
import com.myapps.vista_ccc.util.UtilNumber;
import com.myapps.vista_ccc.util.UtilUrl;
import com.myapps.vista_ccc.white_list.business.SmsSendBusiness;
import com.myapps.vista_ccc.white_list.commons.AuditoriaWhiteList;
import com.myapps.vista_ccc.white_list.error.HttpStatusException;
import com.myapps.vista_ccc.white_list.error.SmsSendException;
import com.myapps.vista_ccc.white_list.model.SmsSendResponseOk;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@ManagedBean
@ViewScoped
public class SmsSendBean implements Serializable {

	public int getRangoDiasConsultaLogs() {
		return rangoDiasConsultaLogs;
	}

	public void setRangoDiasConsultaLogs(int rangoDiasConsultaLogs) {
		this.rangoDiasConsultaLogs = rangoDiasConsultaLogs;
	}

	private static final long serialVersionUID = 1L;
	private static Logger log = Logger.getLogger(SmsSendBean.class);
	@Inject
	private SmsSendBusiness smsSenBussines;
	@Inject
	private ControlerBitacora controlerBitacora;
	private Date startDate;
	private Date endDate;
	private String msisdn;
	private String patternFecha;
	private transient List<SmsSendResponseOk> smsSendList;
	private int rangoDiasConsultaLogs;
	private String login;
	@Inject
	private AuditoriaWhiteList auditoria;
	private long idAuditoriaHistorial;
	private String ip;

	@PostConstruct
	public void init() {
		patternFecha = Parametros.patternFechaSendSms;
		rangoDiasConsultaLogs = Parametros.rangoDiasConsultaLogs;
		obtenerUsuario();
		cargarIp();
	}

	private void cargarIp() {
		try {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
					.getRequest();
			ip = UtilUrl.getClientIp(request);
		} catch (Exception e) {
			log.error("Error al obtener ip: ", e);
		}
	}

	private void obtenerUsuario() {
		try {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
					.getRequest();
			login = (String) request.getSession().getAttribute("TEMP$USER_NAME");
			log.info("Usuario: " + login + ", ingresando a vista de white list.");
		} catch (Exception e) {
			log.error("Error al obtener usuario: ", e);
		}
	}

	public boolean validarIsdn() {
		boolean validar = false;
		if (msisdn != null && !msisdn.trim().isEmpty() && UtilNumber.esNroTigo(msisdn))
			validar = true;
		else
			SysMessage.warn("Número de teléfono no válido", null);
		return validar;
	}

	public void listarSmsSend() {
		if (validarIsdn() && smsSenBussines.validarFecha(startDate, endDate)) {
			smsSendList = null;
			try {
				idAuditoriaHistorial = auditoria.guardarAuditoria(Calendar.getInstance(), login, ip,
						Parametros.vistaWhiteList, Parametros.comandoWhiteList, msisdn, null);
				smsSendList = smsSenBussines.obtenerSmsSend(msisdn, startDate, endDate, login, idAuditoriaHistorial);
				SysMessage.info("Consulta exitosa.", null);
			} catch (SmsSendException e) {
				log.info("Espuesta de servicio codigo: " + e.getResponseInfo().getCodigo() + ", mensaje: "
						+ e.getResponseInfo().getMensaje());
				SysMessage.info(e.getResponseInfo().getCodigo() + " : " + e.getResponseInfo().getMensaje(), null);
			} catch (HttpStatusException e) {
				log.error("Excepcion al consumir el servicio de SmsSend " + e.getMessage(), e);
				SysMessage.warn("No se pudo completar la consulta.", null);
			} catch (Exception e) {
				log.error("Error interno de aplicacion " + e.getMessage(), e);
				SysMessage.error("Problema de aplicacion, no se pudo completar la consulta.", null);
			}
		}

		try {
			controlerBitacora.accion(DescriptorBitacora.SEND_SMS,
					"Se realizó la busqueda de resporte SMSs enviados de linea: " + msisdn);
		} catch (Exception e) {
			log.error("Error al guardar bitacora en el sistema: ", e);
			SysMessage.error("Error al guardar bitacora", null);
		}
	}

	public void limpiar() {
		startDate = null;
		endDate = null;
		msisdn = "";
		smsSendList = null;
	}

	public boolean vaidarFechas() {
		return false;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getPatternFecha() {
		return patternFecha;
	}

	public void setPatternFecha(String patternFecha) {
		this.patternFecha = patternFecha;
	}

	public List<SmsSendResponseOk> getSmsSendList() {
		return smsSendList;
	}

	public void setSmsSendList(List<SmsSendResponseOk> smsSendList) {
		this.smsSendList = smsSendList;
	}

}
