package com.myapps.vista_ccc.white_list.business;

import com.google.gson.Gson;
import com.myapps.vista_ccc.white_list.commons.HttpMethod;
import com.myapps.vista_ccc.white_list.commons.WhiteListMethods;
import com.myapps.vista_ccc.white_list.error.HttpStatusAcceptException;
import com.myapps.vista_ccc.white_list.error.HttpStatusException;
import com.myapps.vista_ccc.white_list.error.WhiteListResponseException;
import com.myapps.vista_ccc.white_list.model.WhiteListRequest;
import com.myapps.vista_ccc.white_list.model.WhiteListResponse;
import com.myapps.vista_ccc.white_list.servicio.WhileListRestConsumer;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@SuppressWarnings("unchecked")
@Named
public class WhiteListBusiness implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(WhiteListBusiness.class);
	@Inject
	private WhileListRestConsumer clientRest;

	public List<WhiteListResponse> obtenerListDatosWhiteList(String msisdn, String usuario, long idAuditoriaHistorial)
			throws HttpStatusException, IOException, WhiteListResponseException {
		String urlMethod = new StringBuffer(WhiteListMethods.WHITE_LIST.getUri()).append(msisdn).toString();
		String whiteStringJson = "";
		try {
			whiteStringJson = clientRest.consumerWhiteList(WhiteListRequest.getInitByIsdn(msisdn), urlMethod, HttpMethod.GET, usuario,
					idAuditoriaHistorial);
		} catch (HttpStatusAcceptException e) {
			logger.info("Respuesta de servicio white_list deferente de OK...");
			throw new WhiteListResponseException(convertJsonToMap(e.getJson()), e.getCodigo());
		}
		return WhiteListResponse.convertJsonToListWhiteList(whiteStringJson);
	}

	public WhiteListResponse obtenerDatosWhiteList(String msisdn, String usuario, long idAuditoriaHistorial)
			throws HttpStatusException, IOException, WhiteListResponseException {
		String urlMethod = new StringBuffer(WhiteListMethods.WHITE_LIST.getUri()).append(msisdn).toString();
		String whiteStringJson = "";
		try {
			whiteStringJson = clientRest.consumerWhiteList(WhiteListRequest.getInitByIsdn(msisdn), urlMethod, HttpMethod.GET, usuario,
					idAuditoriaHistorial);
		} catch (HttpStatusAcceptException e) {
			throw new WhiteListResponseException(convertJsonToMap(e.getJson()), e.getCodigo());
		}
		return WhiteListResponse.convertJsonWhileList(whiteStringJson);
	}

	public Map<String, String> addWhiteList(WhiteListRequest requestBody, String usuario, long idAuditoriaHistorial)
			throws HttpStatusException, IOException, WhiteListResponseException {
		String urlMethod = WhiteListMethods.WHITE_ADD.getUri();
//		requestBody.setMsisdn(
//				new StringBuffer(Parametros.codigoRegionMsisdnWhiteList).append(requestBody.getMsisdn()).toString());
		logger.info("Ejecutando Metodo add white list...");
//		String jsonRequestBody = WhiteListRequest.convertToJson(requestBody);
		String whiteStringJson = "";
		try {
			whiteStringJson = clientRest.consumerWhiteList(requestBody, urlMethod, HttpMethod.POST, usuario,
					idAuditoriaHistorial);
		} catch (HttpStatusAcceptException e) {
			throw new WhiteListResponseException(convertJsonToMap(e.getJson()), e.getCodigo());
		}
		return convertJsonToMap(whiteStringJson);
	}

	public Map<String, String> deleteWhiteList(WhiteListRequest requestBody, String usuario, long idAuditoriaHistorial)
			throws HttpStatusException, IOException, WhiteListResponseException {
		String urlMethod = WhiteListMethods.WHITE_DELETE.getUri();
//		requestBody.setMsisdn(
//				new StringBuffer(Parametros.codigoRegionMsisdnWhiteList).append(requestBody.getMsisdn()).toString());
		String whiteStringJson = null;
		try {
			whiteStringJson = clientRest.consumerWhiteList(requestBody, urlMethod,
					HttpMethod.POST, usuario, idAuditoriaHistorial);
		} catch (HttpStatusAcceptException e) {
			throw new WhiteListResponseException(convertJsonToMap(e.getJson()), e.getCodigo());
		}
		return convertJsonToMap(whiteStringJson);
	}

	public Map<String, String> convertJsonToMap(String stringJson) {
		Gson gson = new Gson();
		Map<String, String> whiteList = gson.fromJson(stringJson, Map.class);
		logger.info("WhiteList ResponseBody: " + whiteList);
		return whiteList;
	}

	public List<Map<String, String>> convertJsonToListMap(String stringJson) {
		Gson gson = new Gson();
		List<Map<String, String>> whiteListDatos = gson.fromJson(stringJson, List.class);
		logger.info("Lista de datos WhiteList: " + whiteListDatos);
		return whiteListDatos;
	}

}
