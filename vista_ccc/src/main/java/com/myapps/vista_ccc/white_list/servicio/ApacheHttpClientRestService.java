package com.myapps.vista_ccc.white_list.servicio;

import com.myapps.vista_ccc.white_list.commons.HttpMethod;
import com.myapps.vista_ccc.white_list.commons.StatusResponse;
import com.myapps.vista_ccc.white_list.error.HttpStatusAcceptException;
import com.myapps.vista_ccc.white_list.error.HttpStatusException;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

@Named
public class ApacheHttpClientRestService implements Serializable {

	private static Logger logger = Logger.getLogger(ApacheHttpClientRestService.class);

	public synchronized String httpClientRest(HttpMethod httpMethod, String body, String url, String tokken,
                                              int timeout) throws IOException, HttpStatusAcceptException, HttpStatusException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
		//HttpResponse response;
		String responseBody;
		CloseableHttpResponse response;
		CloseableHttpClient httpClient = null;
//		CloseableHttpClient httpClient = HttpClientBuilder.create()
//				.setDefaultRequestConfig(generarConfiguracionRequest(timeout)).build();
		switch (httpMethod) {
		case GET:
			logger.info("Consume service REST httpMethod: " + HttpMethod.GET.toString());
			SSLConnectionSocketFactory scsf = new SSLConnectionSocketFactory(SSLContexts.custom().loadTrustMaterial(null, new TrustSelfSignedStrategy()).build(), NoopHostnameVerifier.INSTANCE);
			try{
				httpClient = HttpClientBuilder.create().setSSLSocketFactory(scsf).setDefaultRequestConfig(generarConfiguracionRequest(timeout)).build();
				response = httpClient.execute(prepararRequestGet(url, tokken));
				responseBody = validarResponse(response);
			}finally {
				httpClient.close();
				//response.close();
			}

			break;
		case POST:
			logger.info("Consume service POST httpMethod: " + body);
			try{
				httpClient = HttpClientBuilder.create().setDefaultRequestConfig(generarConfiguracionRequest(timeout)).build();
				response = httpClient.execute(prepararRequetPost(body, url, tokken));
				responseBody = validarResponse(response);
			}finally {
				httpClient.close();
			}
			break;
		default:
			//httpClient = null;
			//responseBody = null;
			response = null;
			responseBody = validarResponse(response);
			logger.info("Htttp Method no conocido");
		}
		//String responseBody = validarResponseBolean(response);
		//httpClient.close();
		//response.close();
		return responseBody;
	}

	public String validarResponse(HttpResponse response)
			throws HttpStatusAcceptException, IOException, HttpStatusException {
		if (response != null) {
			logger.info("Http Status Code: " + response.getStatusLine().getStatusCode());
			if (response.getStatusLine().getStatusCode() != StatusResponse.HTTP_OK.getCode()) {
				if (response.getStatusLine().getStatusCode() == StatusResponse.HTTP_NOT_FOUND.getCode()
						|| response.getStatusLine().getStatusCode() == StatusResponse.HTTP_CONFLICT.getCode()
						|| response.getStatusLine().getStatusCode() == StatusResponse.HTTP_ERROR.getCode()) {
					throw new HttpStatusAcceptException(EntityUtils.toString(response.getEntity()),
							response.getStatusLine().getStatusCode());
				} else if (response.getStatusLine().getStatusCode() == StatusResponse.HTTP_UNAUTHORIZED.getCode()) {
					throw new HttpStatusException(StatusResponse.HTTP_UNAUTHORIZED.getDescripcion(),
							StatusResponse.HTTP_UNAUTHORIZED);
				} else if (response.getStatusLine().getStatusCode() == StatusResponse.HTTP_FORBIDDEN.getCode()) {
					throw new HttpStatusException(StatusResponse.HTTP_FORBIDDEN.getDescripcion(),
							StatusResponse.HTTP_FORBIDDEN);
				} else {
					throw new HttpStatusException(response.getStatusLine().getReasonPhrase(),
							StatusResponse.HTTP_ERROR);
				}
			}
		} else {
			logger.error("No se pudo establecer una conexion con el servicio Rest Servicio resualto a Null.");
			throw new HttpStatusException(
					StatusResponse.HTTP_ERROR.getDescripcion() + "Causa: Servicio resulto a nulo.",
					StatusResponse.HTTP_ERROR);
		}
		return EntityUtils.toString(response.getEntity());
	}

	public RequestConfig generarConfiguracionRequest(int timeout) {
		return RequestConfig.custom().setConnectTimeout(timeout * 1000).setConnectionRequestTimeout(timeout * 1000)
				.setSocketTimeout(timeout * 1000).build();
	}

	public HttpGet prepararRequestGet(String url, String tokken) {
		HttpGet getRequest = new HttpGet(url);
		getRequest.addHeader("accept", "application/json");
		getRequest.addHeader("Authorization", "Basic " + tokken);
		return getRequest;
	}

	public HttpPost prepararRequetPost(String body, String url, String tokken) throws UnsupportedEncodingException {
		HttpPost postRequest = new HttpPost(url);
		postRequest.addHeader("content-type", "application/json");
		postRequest.addHeader("Authorization", "Basic " + tokken);
		StringEntity entity = new StringEntity(body);
		postRequest.setEntity(entity);
		return postRequest;
	}

}
