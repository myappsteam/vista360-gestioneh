package com.myapps.vista_ccc.white_list.business;

import com.google.gson.JsonSyntaxException;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.SysMessage;
import com.myapps.vista_ccc.util.UtilDate;
import com.myapps.vista_ccc.white_list.commons.HttpMethod;
import com.myapps.vista_ccc.white_list.commons.WhiteListMethods;
import com.myapps.vista_ccc.white_list.error.HttpStatusAcceptException;
import com.myapps.vista_ccc.white_list.error.HttpStatusException;
import com.myapps.vista_ccc.white_list.error.SmsSendException;
import com.myapps.vista_ccc.white_list.model.SmsSendResponseInfo;
import com.myapps.vista_ccc.white_list.model.SmsSendResponseOk;
import com.myapps.vista_ccc.white_list.servicio.SmsSendRestConsumer;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Named
public class SmsSendBusiness implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(SmsSendBusiness.class);
	@Inject
	private SmsSendRestConsumer consumerRest;

	public List<SmsSendResponseOk> obtenerSmsSend(String msisdn, Date startDate, Date endDate, String usuario,
                                                  long idAuditoriaHistoria) throws HttpStatusException, IOException, SmsSendException {
		String urlMethod = new StringBuffer().append(WhiteListMethods.SMS_SEND.getUri()).append("?msisdn=")
				.append(msisdn).append("&startDate=").append(UtilDate.dateToString(startDate, "dd-MM-yyyy"))
				.append("&endDate=").append(UtilDate.dateToString(endDate, "dd-MM-yyyy")).toString();
		String whiteStringJson = "";
		try {
			whiteStringJson = consumerRest.consumerWhiteList(msisdn, urlMethod, HttpMethod.GET, usuario,
					idAuditoriaHistoria);
		} catch (HttpStatusAcceptException e) {
			logger.info("Respuesta de sms_sent deferente de OK: " + e.getJson());
			throw new SmsSendException(SmsSendResponseInfo.getInstanceByJson(e.getJson()));
		}
		return convertJsonToListSmsSend(whiteStringJson);
	}

	public List<SmsSendResponseOk> convertJsonToListSmsSend(String json) {
		List<SmsSendResponseOk> list = new ArrayList<>();
		try {
			list = SmsSendResponseOk.getInstanceListByJson(json);
		} catch (JsonSyntaxException e) {
			logger.info("Respuesta de servicio diferente de OK...");
		}
//		try {
//			throw new SmsSendException(SmsSendResponseInfo.getInstanceByJson(json));
//		} catch (JsonSyntaxException e) {
//			logger.warn("Respuesta no conocida retornada por es el servicio Sms Send...");
//		}
		return list;
	}

	public boolean validarFecha(Date startDate, Date endDate) {
		Date fechaSistema = new Date();
		boolean validar = false;
		if (startDate != null && endDate != null) {
			if (endDate.before(fechaSistema)) {
				if (startDate.before(endDate) || startDate.equals(endDate)) {
					if (UtilDate.verificarRangoDeFechas(startDate, endDate, Parametros.rangoDiasConsultaLogs))
						validar = true;
					else
						SysMessage.warn("La cantidad de días entre fecha inicio y fecha fin, no debe exceder los "
								+ Parametros.rangoDiasConsultaLogs + " días.", null);
				} else {
					SysMessage.warn("La Fecha Inicio tiene que ser menor o igual que la Fecha Fin", null);
				}
			} else {
				SysMessage.warn("La Fecha Fin tiene que ser menor o igual que la Fecha Actual del Sistema", null);
			}
		} else {
			SysMessage.warn("Fechas no validas.", null);
		}
		return validar;
	}

}
