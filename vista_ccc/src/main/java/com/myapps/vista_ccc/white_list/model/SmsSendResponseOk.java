package com.myapps.vista_ccc.white_list.model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class SmsSendResponseOk {

	private String id;
	private String tlogID;
	private String startDate;
	private String msisdn;
	private String subscription;
	private String statusDetail;
	private String status;
	private String message;

	public static List<SmsSendResponseOk> getInstanceListByJson(String json) {
		Type listType = new TypeToken<List<SmsSendResponseOk>>() {
		}.getType();
		Gson gson = new Gson();
		return gson.fromJson(json, listType);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTlogID() {
		return tlogID;
	}

	public void setTlogID(String tlogID) {
		this.tlogID = tlogID;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getSubscription() {
		return subscription;
	}

	public void setSubscription(String subscription) {
		this.subscription = subscription;
	}

	public String getStatusDetail() {
		return statusDetail;
	}

	public void setStatusDetail(String statusDetail) {
		this.statusDetail = statusDetail;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "SmsSendResponseOk [id=" + id + ", tlogID=" + tlogID + ", startDate=" + startDate + ", msisdn=" + msisdn
				+ ", subscription=" + subscription + ", statusDetail=" + statusDetail + ", status=" + status
				+ ", message=" + message + "]";
	}
}
