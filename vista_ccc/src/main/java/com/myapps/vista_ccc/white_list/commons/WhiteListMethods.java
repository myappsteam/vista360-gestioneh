package com.myapps.vista_ccc.white_list.commons;

public enum WhiteListMethods {

	WHITE_LIST(1, "pkg_expiry/white_list/"),
	WHITE_ADD(2,"pkg_expiry/add_to_white_list"),
	WHITE_DELETE(3,"pkg_expiry/delete_from_white_list"),
	SMS_SEND(4,"api/v1/log/sms_sent");

	private int codigo;
	private String uri;

	WhiteListMethods(int codigo, String uri) {
		this.codigo = codigo;
		this.uri = uri;
	}

	public int getCodigo() {
		return codigo;
	}

	public String getUri() {
		return uri;
	}

}
