package com.myapps.vista_ccc.white_list.bean;

import com.myapps.user.bean.ControlerBitacora;
import com.myapps.user.ldap.ActiveDirectory;
import com.myapps.user.ldap.DescriptorBitacora;
import com.myapps.vista_ccc.util.*;
import com.myapps.vista_ccc.white_list.business.WhiteListBusiness;
import com.myapps.vista_ccc.white_list.commons.AuditoriaWhiteList;
import com.myapps.vista_ccc.white_list.commons.ResponseWhiteList;
import com.myapps.vista_ccc.white_list.error.HttpStatusException;
import com.myapps.vista_ccc.white_list.error.WhiteListResponseException;
import com.myapps.vista_ccc.white_list.model.WhiteListRequest;
import com.myapps.vista_ccc.white_list.model.WhiteListResponse;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

@ManagedBean
@ViewScoped
public class WhiteListBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger log = Logger.getLogger(WhiteListBean.class);

	@Inject
	private WhiteListBusiness whiteListBusiness;
	@Inject
	private ControlerBitacora controlerBitacora;
	private long idAuditoriaHistorial;
	private String ip;
	@Inject
	private AuditoriaWhiteList auditoria;
	private String msisdn;
	private String notas;
	private int opcionRadio;
	private WhiteListResponse whiteList;
	private Map<String, String> mapResponseWhiteList;
	private String login;
	private String claveRespuesta;
	private String valorRespuesta;

	@PostConstruct
	public void init() {
		obtenerUsuario();
		cargarIp();
	}

	private void cargarIp() {
		try {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
					.getRequest();
			ip = UtilUrl.getClientIp(request);
		} catch (Exception e) {
			log.error("Error al obtener ip: ", e);
		}
	}

	public boolean validarIsdn() {
		boolean validar = false;
		if (msisdn != null && !msisdn.trim().isEmpty() && UtilNumber.esNroTigo(msisdn))
			validar = true;
		else
			SysMessage.warn("Número de teléfono no válido", null);
		return validar;
	}

	public void buscarDatosPorNumero(int opcion) {
		if (opcion == 1) {
			limpiarDatos();
		}
		if (validarIsdn()) {
			try {
				idAuditoriaHistorial = auditoria.guardarAuditoria(Calendar.getInstance(), login, ip,
						Parametros.vistaWhiteList, Parametros.comandoWhiteList, msisdn, null);
				List<WhiteListResponse> listWhiteList = whiteListBusiness.obtenerListDatosWhiteList(msisdn, login,
						idAuditoriaHistorial);
				log.info("Lista recuperada: " + listWhiteList);
				whiteList = listWhiteList.get(0);

				whiteList.setCreated_date(
						UtilDate.stringToCalendarXML(whiteList.getCreated_date(), "MM/dd/yyyy HH:mm:ss"));
				whiteList.setUpdated_date(
						UtilDate.stringToCalendarXML(whiteList.getUpdated_date(), "MM/dd/yyyy HH:mm:ss"));
				whiteList.setStartUserFullName(obtenerNombreCompletroUsuario(whiteList.getStart_user()));
				whiteList.setEndUserFullName(obtenerNombreCompletroUsuario(whiteList.getEnd_user()));
				log.info("Datos de: " + msisdn + ", listados correctamente: " + whiteList);
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_INFO, "Info ", "Consulta finalizada correctamente."));
			} catch (WhiteListResponseException e) {
				mapResponseWhiteList = e.getResponse();
				mostrarMensaje(e.getResponse(), e.getCodigo(), 1);
			} catch (HttpStatusException e) {
				log.error("Problema con http client " + e.getMessage() + " " + e.getStatusResponse().getCode());
				SysMessage.error(e.getStatusResponse().getCode() + " " + e.getMessage(), null);

			} catch (Exception e) {
				log.error("No se pudo completar la consulta: " + e.getMessage(), e);
				SysMessage.error("Problema de aplicacion, no se pudo completar la consulta.", null);
			}
		}
		try {
			controlerBitacora.accion(DescriptorBitacora.WHITE_LIST,
					"Se realizó la busqueda en White List de linea: " + msisdn);
		} catch (Exception e) {
			log.error("Error al guardar bitacora en el sistema: ", e);
			SysMessage.error("Error al guardar bitacora", null);
		}
	}

	public void limpiarDatos() {
		whiteList = null;
		mapResponseWhiteList = null;
	}

	public void limpiarTodo() {
		limpiarDatos();
		msisdn = "";
		notas = "";
		opcionRadio = 0;
	}

	public void realizarAccion() {
		limpiarDatos();
		if (validarIsdn()) {
			if (opcionRadio == 1) {
				realizarOperacionAdd();
			} else if (opcionRadio == 2) {
				realizarOperacionDelete();
			} else {
				SysMessage.warn("Seleccione una opcion.", null);
			}
		}
	}

	public void realizarOperacionDelete() {
		WhiteListRequest newWhiteList = WhiteListRequest.getInstanceForDelete(msisdn, notas, login,
				Parametros.endChannelRequestWhiteList);
		log.info("Accion opcion {2} eliminar MSISDN: " + newWhiteList + " de White List.");
		eliminarMsisdn(newWhiteList);
	}

	public void realizarOperacionAdd() {
		WhiteListRequest newWhiteList = WhiteListRequest.getInstanceForAdd(msisdn, notas, login,
				Parametros.startChannelRequestWhiteList);
		log.info("Accion opcion {1} Adicionar MSISDN: " + newWhiteList + " a White List.");
		adicionarMsisdn(newWhiteList);
	}

	public void eliminarMsisdn(WhiteListRequest newWhiteList) {
		try {
			idAuditoriaHistorial = auditoria.guardarAuditoria(Calendar.getInstance(), login, ip,
					Parametros.vistaWhiteList, Parametros.comandoWhiteList, msisdn, null);
			mapResponseWhiteList = whiteListBusiness.deleteWhiteList(newWhiteList, login, idAuditoriaHistorial);
			log.info("Respuesta para accion delete_from_white_list: " + mapResponseWhiteList);
			buscarDatosPorNumero(2);
			mostrarMensaje(mapResponseWhiteList, 200, 3);
		} catch (WhiteListResponseException e) {
			mapResponseWhiteList = e.getResponse();
			mostrarMensaje(e.getResponse(), e.getCodigo(), 3);
		} catch (HttpStatusException e) {
			log.error("Problema con http client " + e.getMessage() + " " + e.getStatusResponse().getCode());
			SysMessage.error(e.getStatusResponse().getCode() + " " + e.getMessage(), null);
		} catch (Exception e) {
			log.error("Problema de aplicacion, no se pudo completar la consulta: " + e.getMessage(), e);
			SysMessage.error("Problema de aplicacion, no se pudo completar la consulta.", null);
		}
		try {
			controlerBitacora.accion(DescriptorBitacora.WHITE_LIST,
					"Se realizó accion delete_from_white_list, " + newWhiteList.getMsisdn());
		} catch (Exception e) {
			log.error("Error al guardar bitacora en el sistema: ", e);
			SysMessage.error("Error al guardar bitacora", null);
		}
	}

	public void adicionarMsisdn(WhiteListRequest newWhiteList) {
		try {
			idAuditoriaHistorial = auditoria.guardarAuditoria(Calendar.getInstance(), login, ip,
					Parametros.vistaWhiteList, Parametros.comandoWhiteList, msisdn, null);
			mapResponseWhiteList = whiteListBusiness.addWhiteList(newWhiteList, login, idAuditoriaHistorial);
			log.info("Respuesta para accion add_to_white_list: " + mapResponseWhiteList);
			buscarDatosPorNumero(2);
			mostrarMensaje(mapResponseWhiteList, 200, 2);
		} catch (WhiteListResponseException e) {
			mapResponseWhiteList = e.getResponse();
			mostrarMensaje(e.getResponse(), e.getCodigo(), 2);
		} catch (HttpStatusException e) {
			SysMessage.error(e.getStatusResponse().getCode() + " " + e.getMessage(), null);
			log.error("Problema con http client " + e.getMessage() + " " + e.getStatusResponse().getCode());
		} catch (Exception e) {
			SysMessage.error("Problema de aplicacion, no se pudo completar la consulta.", null);
			log.error("Problema de aplicacion, no se pudo completar la consulta: " + e.getMessage(), e);
		}
		try {
			controlerBitacora.accion(DescriptorBitacora.WHITE_LIST,
					"Se realizó accion add_to_white_list, " + newWhiteList.getMsisdn());
		} catch (Exception e) {
			log.error("Error al guardar bitacora en el sistema: ", e);
			SysMessage.error("Error al guardar bitacora", null);
		}
	}

	public void mostrarMensaje(Map<String, String> msjAccion, int codigo, int evento) {
		for (Map.Entry<String, String> accion : msjAccion.entrySet()) {
			log.info(accion.getKey() + " " + accion.getValue());
			claveRespuesta = accion.getKey();
			valorRespuesta = accion.getValue();
			if (evento == 1) {
				mostrarMensajeListar(codigo);
			} else if (evento == 2) {
				mostrarMensajeAdd(codigo, accion.getValue());
			} else if (evento == 3) {
				mostrarMensajeDelete(codigo, accion.getValue());
			}
		}
	}

	public void mostrarMensajeListar(int codigo) {
		if (codigo == ResponseWhiteList.RESPONSE_OK_200.getCodigo())
			SysMessage.info(ResponseWhiteList.RESPONSE_OK_200.getMensajeVista(), null);
		else
			mostrarMensajeGenerico(codigo);
	}

	public void mostrarMensajeAdd(int codigo, String mensaje) {
		if (codigo == ResponseWhiteList.RESPONSE_OK_200_ADD.getCodigo()
				&& mensaje.equals(ResponseWhiteList.RESPONSE_OK_200_ADD.getMensajeResponse()))
			SysMessage.info(ResponseWhiteList.RESPONSE_OK_200_ADD.getMensajeVista(), null);
		else if (codigo == ResponseWhiteList.RESPONSE_ERROR_409_ADD_YES.getCodigo()
				&& mensaje.equals(ResponseWhiteList.RESPONSE_ERROR_409_ADD_YES.getMensajeResponse()))
			SysMessage.warn(ResponseWhiteList.RESPONSE_ERROR_409_ADD_YES.getMensajeVista(), null);
		else
			mostrarMensajeGenerico(codigo);
	}

	public void mostrarMensajeDelete(int codigo, String mensaje) {
		if (codigo == ResponseWhiteList.RESPONSE_OK_200_DELETE.getCodigo()
				&& mensaje.equals(ResponseWhiteList.RESPONSE_OK_200_DELETE.getMensajeResponse()))
			SysMessage.info(ResponseWhiteList.RESPONSE_OK_200_DELETE.getMensajeVista(), null);
		else if (codigo == ResponseWhiteList.RESPONSE_ERROR_409_DELETE_YES.getCodigo()
				&& mensaje.equals(ResponseWhiteList.RESPONSE_ERROR_409_DELETE_YES.getMensajeResponse()))
			SysMessage.warn(ResponseWhiteList.RESPONSE_ERROR_409_DELETE_YES.getMensajeVista(), null);
		else
			mostrarMensajeGenerico(codigo);
	}

	public void mostrarMensajeGenerico(int codigo) {
		if (codigo == ResponseWhiteList.RESPONSE_ERROR_404.getCodigo())
			SysMessage.warn(ResponseWhiteList.RESPONSE_ERROR_404.getMensajeVista(), null);
		else if (codigo == ResponseWhiteList.RESPONSE_ERROR_409.getCodigo())
			SysMessage.warn(ResponseWhiteList.RESPONSE_ERROR_409.getMensajeVista(), null);
		else
			SysMessage.warn(ResponseWhiteList.RESPONSE_ERROR_500.getMensajeVista(), null);
	}

	private void obtenerUsuario() {
		try {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
					.getRequest();
			login = (String) request.getSession().getAttribute("TEMP$USER_NAME");
			log.info("Usuario: " + login + ", ingresando a vista de white list.");
		} catch (Exception e) {
			log.error("Error al obtener usuario: ", e);
		}
	}

	private String obtenerNombreCompletroUsuario(String login) {
		return ActiveDirectory.getNombreCompleto(login);
	}

	public String getClaveRespuesta() {
		return claveRespuesta;
	}

	public void setClaveRespuesta(String claveRespuesta) {
		this.claveRespuesta = claveRespuesta;
	}

	public String getValorRespuesta() {
		return valorRespuesta;
	}

	public void setValorRespuesta(String valorRespuesta) {
		this.valorRespuesta = valorRespuesta;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public int getOpcionRadio() {
		return opcionRadio;
	}

	public void setOpcionRadio(int opcionRadio) {
		this.opcionRadio = opcionRadio;
	}

	public WhiteListResponse getWhiteList() {
		return whiteList;
	}

	public void setWhiteList(WhiteListResponse whiteList) {
		this.whiteList = whiteList;
	}

	public String getNotas() {
		return notas;
	}

	public void setNotas(String notas) {
		this.notas = notas;
	}

	public Map<String, String> getMapResponseWhiteList() {
		return mapResponseWhiteList;
	}

	public void setMapResponseWhiteList(Map<String, String> mapResponseWhiteList) {
		this.mapResponseWhiteList = mapResponseWhiteList;
	}

}
