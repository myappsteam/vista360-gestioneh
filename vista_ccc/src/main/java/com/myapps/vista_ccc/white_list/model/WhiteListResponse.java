package com.myapps.vista_ccc.white_list.model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

public class WhiteListResponse implements Serializable {

	public String getEndUserFullName() {
		return endUserFullName;
	}

	public void setEndUserFullName(String endUserFullName) {
		this.endUserFullName = endUserFullName;
	}

	public String getStartUserFullName() {
		return startUserFullName;
	}

	public void setStartUserFullName(String startUserFullName) {
		this.startUserFullName = startUserFullName;
	}

	private static final long serialVersionUID = 1L;

	private String id;
	private String msisdn;
	private String state;
	private String notes;
	private String start_user;
	private String startUserFullName;
	private String end_user;
	private String endUserFullName;
	private String start_channel;
	private String end_channel;
	private String created_date;
	private String updated_date;

	public static WhiteListResponse convertJsonWhileList(String stringJson) {
		Gson gson = new Gson();
		return gson.fromJson(stringJson, WhiteListResponse.class);
	}

	public static List<WhiteListResponse> convertJsonToListWhiteList(String stringJson) {
		Type listType = new TypeToken<List<WhiteListResponse>>() {
		}.getType();
		Gson gson = new Gson();
		return gson.fromJson(stringJson, listType);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getStart_user() {
		return start_user;
	}

	public void setStart_user(String start_user) {
		this.start_user = start_user;
	}

	public String getEnd_user() {
		return end_user;
	}

	public void setEnd_user(String end_user) {
		this.end_user = end_user;
	}

	public String getStart_channel() {
		return start_channel;
	}

	public void setStart_channel(String start_channel) {
		this.start_channel = start_channel;
	}

	public String getEnd_channel() {
		return end_channel;
	}

	public void setEnd_channel(String end_channel) {
		this.end_channel = end_channel;
	}

	public String getCreated_date() {
		return created_date;
	}

	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}

	public String getUpdated_date() {
		return updated_date;
	}

	public void setUpdated_date(String updated_date) {
		this.updated_date = updated_date;
	}

	@Override
	public String toString() {
		return "WhiteListResponse [id=" + id + ", msisdn=" + msisdn + ", state=" + state + ", notes=" + notes
				+ ", start_user=" + start_user + ", end_user=" + end_user + ", start_channel=" + start_channel
				+ ", end_channel=" + end_channel + ", created_date=" + created_date + ", updated_date=" + updated_date
				+ "]";
	}

}
