package com.myapps.vista_ccc.white_list.commons;

public enum StatusResponse {

    HTTP_UNAUTHORIZED(401, "Cliente HTTP No Autorizado {Unauthorized}"),
    HTTP_FORBIDDEN(403, "Cliente HTTP Prohibido {Forbidden}"),
    HTTP_NOT_FOUND(404, "Not Found"),
    HTTP_CONFLICT(409, "Conflicto"),
    HTTP_INTERNAL_SERVER_ERROR(500, "Error interno de servidor."),
    HTTP_ERROR(500, "Error de aplicacion"),
    HTTP_OK(200, "OK"),
    CREATED(201, "Created"),
    NO_CONTENT(204, "No Content");

    private int code;
    private String descripcion;

    StatusResponse(int code, String descripcion) {
        this.code = code;
        this.descripcion = descripcion;
    }

    public int getCode() {
        return code;
    }

    public String getDescripcion() {
        return descripcion;
    }

}
