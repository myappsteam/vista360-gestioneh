package com.myapps.vista_ccc.white_list.model;

import com.google.gson.Gson;

public class AddWhileListResponse {

	private String ok;

	public static AddWhileListResponse convertOfJson(String json) {
		Gson gson = new Gson();
		return gson.fromJson(json, AddWhileListResponse.class);
	}

	public String getOk() {
		return ok;
	}

	public void setOk(String ok) {
		this.ok = ok;
	}

}
