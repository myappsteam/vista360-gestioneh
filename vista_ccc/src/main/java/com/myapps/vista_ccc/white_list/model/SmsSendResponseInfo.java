package com.myapps.vista_ccc.white_list.model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;

public class SmsSendResponseInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	private String mensaje;
	private String codigo;

	public static SmsSendResponseInfo getInstanceByJson(String json) {
		Type listType = new TypeToken<SmsSendResponseInfo>() {
		}.getType();
		Gson gson = new Gson();
		return gson.fromJson(json, listType);
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

}
