package com.myapps.vista_ccc.white_list.commons;

public enum HttpMethod {
	
	GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE;

}
