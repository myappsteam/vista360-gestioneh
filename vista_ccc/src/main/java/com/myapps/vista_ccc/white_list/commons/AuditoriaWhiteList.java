package com.myapps.vista_ccc.white_list.commons;

import com.myapps.vista_ccc.business.ConsultaBL;
import com.myapps.vista_ccc.business.LogAuditoriaBL;
import com.myapps.vista_ccc.business.LogExcepcionBL;
import com.myapps.vista_ccc.entity.VcConsulta;
import com.myapps.vista_ccc.entity.VcLogAdicional;
import com.myapps.vista_ccc.entity.VcLogAuditoria;
import com.myapps.vista_ccc.entity.VcLogExcepcion;
import com.myapps.vista_ccc.util.SysMessage;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.rpc.ServiceException;
import java.io.CharArrayWriter;
import java.io.PrintWriter;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.List;

@Named
public class AuditoriaWhiteList implements Serializable {

	public static Logger log = Logger.getLogger(AuditoriaWhiteList.class);
	@Inject
	private LogAuditoriaBL blAuditoria;
	@Inject
	private ConsultaBL blConsulta;
	@Inject
	private LogExcepcionBL blExcepcion;

	public long guardarAuditoria(Calendar fechaConsulta, String usuario, String ip, String vista, int comando,
                                 String isdn, List<VcLogAdicional> adicionales) {
		try {
			VcLogAuditoria item = new VcLogAuditoria();
			item.setFechaConsulta(fechaConsulta);
			item.setIsdn(isdn);
			item.setUsuario(usuario);
			item.setIp(ip);
			item.setVista(vista);
			item.setComando(comando);
			item.setAdicional("Y");
			item.setFechaCreacion(Calendar.getInstance());
			if (adicionales == null || adicionales.size() == 0) {
				item.setAdicional("N");
			}

			blAuditoria.guardarAuditoria(item, adicionales);
			return item.getIdLogAuditoria();

		} catch (Exception e) {
			log.error("Error al guardar auditoria: ", e);
		}

		return -1;
	}

	public void saveXml(String isdn, String login, String servicio, String metodo, String request, String response,
                        long idAuditoria, Long tiempoConexion, Long tiempoRespuesta) {
		try {
			VcConsulta c = new VcConsulta();
			c.setFechaCreacion(Calendar.getInstance());
			c.setIsdn(isdn);
			c.setLogin(login);
			c.setServicio(servicio);
			c.setMetodo(metodo);
			c.setRequest(request);
			c.setResponse(response);
			c.setTiempoConexion(tiempoConexion);
			c.setTiempoRespuesta(tiempoRespuesta);

			Object find = blAuditoria.find(idAuditoria, VcLogAuditoria.class);
			if (find != null)
				c.setVcLogAuditoria((VcLogAuditoria) find);

			blConsulta.save(c);
		} catch (Exception e) {
			log.warn("No se registro en bd el request y response: ", e);
		}
	}

	public void guardarClasificarError(Exception e, long idAuditoriaHistorial) {
		String msg = e.getMessage();
		msg = msg.toLowerCase();
		if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase())
				|| msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase())
				|| msg.contains("UnknownHostException".toLowerCase()) || msg.contains("No route to host".toLowerCase())
				|| msg.contains("Connection refused".toLowerCase())) {
			log.error("Error de conexion al servicio PendingDebtsQuantity: ", e);
			SysMessage.error("Error de conexion del servicio PendingDebtsQuantity", null);
			guardarExcepcion(idAuditoriaHistorial,
					"Error de conexion al servicio PendingDebtsQuantity: " + stackTraceToString(e));
		} else {
			log.error("Error AxisFault PendingDebtsQuantity: ", e);
			SysMessage.error("Error AxisFault de conexion al servicio PendingDebtsQuantity", null);
			guardarExcepcion(idAuditoriaHistorial,
					"Error AxisFault de conexion al servicio PendingDebtsQuantity: " + stackTraceToString(e));
		}
		if (e.getCause() instanceof ServiceException) {
			log.error("Error de servicio al conectarse al servicio PendingDebtsQuantity: ", e);
			SysMessage.error("Error de servicio al conectarse al servicio PendingDebtsQuantity", null);
			guardarExcepcion(idAuditoriaHistorial,
					"Error de servicio al conectarse al servicio PendingDebtsQuantity: " + stackTraceToString(e));
		} else if (e.getCause() instanceof RemoteException) {
			log.error("Error remoto al conectarse al servicio PendingDebtsQuantity: ", e);
			SysMessage.error("Error remoto al conectarse al servicio PendingDebtsQuantity", null);
			guardarExcepcion(idAuditoriaHistorial,
					"Error remoto al conectarse al servicio PendingDebtsQuantity: " + stackTraceToString(e));
		} else {
			log.error("Error al consultar PendingDebtsQuantity", e);
			SysMessage.error("Error al consultar PendingDebtsQuantity", null);
			guardarExcepcion(idAuditoriaHistorial, "Error al consultar PendingDebtsQuantity:" + stackTraceToString(e));
		}

		SysMessage.info("Consulta finalizada.", null);

	}

	public void guardarExcepcion(long IdAuditoria, String exepcion) {
		log.debug("[saveVCExcepcion]: Ingresando..");
		VcLogExcepcion item = new VcLogExcepcion();
//		VcLogAuditoria find;
		try {
			VcLogAuditoria find = (VcLogAuditoria) blAuditoria.find(IdAuditoria, VcLogAuditoria.class);
			item.setFechaCreacion(Calendar.getInstance());
			item.setExcepcion(exepcion);
			if (find != null)
				item.setVcLogAuditoria(find);

			blExcepcion.save(item);
		} catch (Exception e) {
			log.error("Error al guardar Excepcion: ", e);
		}
	}

	public String stackTraceToString(Throwable e) {
		CharArrayWriter sw = new CharArrayWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		pw.close();

		return sw.toString(); // stack trace a
	}

}
