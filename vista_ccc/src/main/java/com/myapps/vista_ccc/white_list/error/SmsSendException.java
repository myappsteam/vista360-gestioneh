package com.myapps.vista_ccc.white_list.error;

import com.myapps.vista_ccc.white_list.model.SmsSendResponseInfo;

public class SmsSendException extends Exception {

	private static final long serialVersionUID = 1L;
	private final transient SmsSendResponseInfo responseInfo;

	public SmsSendException(SmsSendResponseInfo responseInfo) {
		this.responseInfo = responseInfo;
	}

	public SmsSendResponseInfo getResponseInfo() {
		return responseInfo;
	}

}
