package com.myapps.vista_ccc.util;

import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import java.math.BigDecimal;
//import java.math.RoundingMode;

public class UtilNumber implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final Logger log = Logger.getLogger(UtilNumber.class);

	public static boolean esNroTigo(String isdn) {
		// log.info("Parametros.expresionRegularNroTigo: " +
		// Parametros.expresionRegularNroTigo);
		Pattern pattern = Pattern.compile(Parametros.expresionRegularNroTigo);
		// Pattern pat = Pattern.compile("^[0-9]{8}$");
		Matcher matcher = pattern.matcher(isdn);
		if (matcher.find()) {
			log.debug("nro=" + isdn + " es valido");
			return true;

		}
		log.debug("nro=" + isdn + " NO es valido");
		return false;
	}

	public static double redondear(double numero, int digitos) {

		// BigDecimal bd = new BigDecimal(numero);
		// bd.setScale(digitos, RoundingMode.HALF_UP);
		// return bd.doubleValue();

		int cifras = (int) Math.pow(10, digitos);
		return Math.rint(numero * cifras) / cifras;
	}

	public static String doubleToString(double number, int nroDecimales) {
		String result = "";
		// %n
		String format = "%." + nroDecimales + "f";
		try {
			// String country = System.getProperty("user.country");
			// String lan = System.getProperty("user.language");
			// log.debug("country: " + country);
			// log.debug("lan: " + lan);

			// result = String.format(Locale.US, format, number);
			Locale locale = new Locale("es", "BO");
			result = String.format(locale, format, number);
			// result = String.format(format, number);

		} catch (Exception e) {
			log.error("Error al convertir double a string: ", e);
		}
		return result;
	}

	public static void main(String[] args) {
		// System.out.println(UtilNumber.esNroTigo("75600608"));
		// double a = 794999;
		// // a = 795000;
		// double b = 1000000;
		//
		// double num = a / b;

		// System.out.println("result: " + UtilNumber.doubleToString(num, 7));
		//
		// num = -123456111.2028811871100000;
		// num = UtilNumber.redondear(num, 2);
		// System.out.println("result: " + UtilNumber.doubleToString(num, 2));
		//

		// System.out.println(UtilNumber.esNroTigo("710414512"));;
	}

	public static String doubleToString(double number, int nroDecimales, String language, String country) {
		String result = "";
		// %n
		String format = "%." + nroDecimales + "f";
		try {
			// result = String.format(Locale.US, format, number);
			Locale locale = new Locale(language, country);
			result = String.format(locale, format, number);

		} catch (Exception e) {
			log.error("Error al convertir double a string: ", e);
		}
		return result;
	}
}
