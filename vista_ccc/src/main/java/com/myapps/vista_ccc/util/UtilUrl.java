package com.myapps.vista_ccc.util;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UtilUrl implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final Logger log = Logger.getLogger(UtilUrl.class);

	public static String getIp(String dir) throws Exception {
		// log.debug("dir input: " + dir);
		String response = "";
		URL url = new URL(dir);
		InetAddress inetAddress = java.net.InetAddress.getByName(url.getHost());
		String ip = inetAddress.getHostAddress();
		response = dir.replace(url.getHost(), ip);
		// log.debug("dir output: " + response);
		return response;
	}

	public static synchronized String getClientIp(HttpServletRequest request) {
		String ip = request.getHeader("X-FORWARDED-FOR");
		// log.debug("X-Forwarded-For: " + ip);
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {

			ip = (String) request.getSession().getAttribute("TEMP$IP_CLIENT");
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
//				 ip = request.getRemoteAddr();
			}
			// log.debug("X-FORWARDED-FOR RESUELTO A NULO, REMOTE ADDRESS JAVA: " + ip);
		}
		// else {
		// log.debug("X-FORWARDED RESUELTO: " + ip);
		// }
		return ip;
	}

	public static boolean esTextoValido(String valor, String expresionRegular) {
		Pattern pattern = Pattern.compile(expresionRegular);
		Matcher matcher = pattern.matcher(valor);
		return matcher.find();
	}

}
