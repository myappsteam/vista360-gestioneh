package com.myapps.vista_ccc.util;

import org.apache.log4j.Logger;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class UtilDate implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(UtilDate.class);

	public static Date stringToDate(String fecha, String format) {
		Date date = null;
		try {
			DateFormat formatter;
			// yyyy-MM-dd HH:mm
			formatter = new SimpleDateFormat(format);
			date = (Date) formatter.parse(fecha);
		} catch (Exception e) {
			e.getMessage();
		}
		return date;
	}

	public static Date UTCToDate(String fecha, String format) {
		Date date = null;
		String fechaString = null;
		if(fecha != null && !fecha.trim().isEmpty()){
			try {
				DateFormat formatter;
				// yyyy-MM-dd HH:mm
				fechaString = conversionGMT(fecha);
				if(fechaString != null){
					formatter = new SimpleDateFormat(format);
					date = (Date) formatter.parse(fechaString);
				}
			} catch (Exception e) {
				logger.error("formato de fecha no valido "+ fecha +" Excepcion: "+ e.getMessage(), e);
			}
		}else{
			logger.warn("Fecha nulla o vacia");
		}

		return date;
	}

	public  static String conversionGMT(String fecha){
		SimpleDateFormat sdfgmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		sdfgmt.setTimeZone(TimeZone.getTimeZone("GMT"));

		SimpleDateFormat sdfmad = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		sdfmad.setTimeZone(TimeZone.getTimeZone("America/La_Paz"));

		String outdate = null;
		try {
			Date inptdate = (Date) sdfgmt.parse(fecha);
			outdate = sdfmad.format(inptdate);
		} catch (ParseException e) {e.printStackTrace();}

		/*System.out.println("GMT:\t\t" + sdfgmt.format(inptdate));
		System.out.println("America/La_Paz:\t" + sdfmad.format(inptdate));*/
		return outdate;
	}

	public static Date setHoraInicio(Date startDate){
		Calendar inicio = Calendar.getInstance();
		inicio.setTime(startDate);
		inicio.set(Calendar.HOUR_OF_DAY,0);
		inicio.set(Calendar.MINUTE,0);
		inicio.set(Calendar.SECOND,0);
		inicio.set(Calendar.MILLISECOND,0);
		return inicio.getTime();
	}

	public static Date setHoraFin(Date endDate){
		Calendar fin = Calendar.getInstance();
		fin.setTime(endDate);
		fin.set(Calendar.HOUR_OF_DAY,23);
		fin.set(Calendar.MINUTE,59);
		fin.set(Calendar.SECOND,59);
		fin.set(Calendar.MILLISECOND,999);
		return fin.getTime();
	}

	public static String dateToString(Date fecha, String format) {
		String date = "";
		try {
			DateFormat formatter = new SimpleDateFormat(format);
			date = formatter.format(fecha);
		} catch (Exception e) {
			e.getMessage();
		}
		return date;
	}

	public static Date sumarRestarDiasFecha(Date fecha, int dias) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha);
		calendar.add(Calendar.DAY_OF_YEAR, dias); 

		return calendar.getTime();	
	}
	
	public static String stringToCalendarXML(String fecha) {
        String resultado = "";		
            try {		     		    
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS");		    
                Date date = (Date) formatter.parse(fecha);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                DateFormat formatterr = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                return formatterr.format(calendar.getTime());

            } catch (ParseException e) {
                e.getMessage();
            }
        return resultado;
    }

	public static boolean verificarRangoDeFechas(Date inicio, Date fin, int diasPermitidos) {
		int dias = (int) ((fin.getTime() - inicio.getTime()) / 86400000);
		System.out.println("dias "+dias);
		return dias <= diasPermitidos;
	}

	public static String stringToCalendarXML(String fecha, String formatoInicial) {
		String resultado = "";
		try {
//                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS");
			DateFormat formatter = new SimpleDateFormat(formatoInicial);
			Date date = (Date) formatter.parse(fecha);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			DateFormat formatterr = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			resultado = formatterr.format(calendar.getTime());
		} catch (ParseException e) {
			logger.error("No se pudo convertir string a Calendar XML " + e.getMessage(), e);
		}
		return resultado;
	}

	// public static void main(String[] args) {
	// TODO Auto-generated method stub
	// Date fecha = UtilDate.stringToDate("20160627121601", "YYYYMMDDhhmmss");
	// Date fecha = UtilDate.stringToDate("20160627121601", "yyyyMMddHHmmss");
	// 2016-06-27 121601
	// Date fecha = new Date();

	// System.out.println(UtilDate.dateToString(fecha, "yyyyMMddHHmmss"));
	// System.out.println(UtilDate.dateToString(fecha, "dd-MM-yyyy HH:mm:ss"));
	// 28-12-2015 12:00:00

	// System.out.println(UtilDate.dateToString(fecha, "YYYYMMDDhhmmss"));
	// System.out.println(UtilDate.dateToString(fecha, "yyyyMMddHHmmss"));
	// 2016-12-36 2121201

	// System.out.println(UtilDate.dateToString(new Date(), "HH:mm:ss"));
	// }
	
	
}
