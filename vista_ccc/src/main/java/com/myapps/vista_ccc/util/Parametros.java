package com.myapps.vista_ccc.util;

import org.apache.log4j.Logger;

import javax.rmi.PortableRemoteObject;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;


public class Parametros {

    //public final static String configFile = "D:\\PROYECTOS\\";
    //public final static String configFile = "C:\\Users\\michael\\Documents\\";
    // QA O PRODUCCION /home/navafusr/myapps/vista_ccc

    // PRODUCCCION
    public final static String configFile = File.separator + "opt" + File.separator + "myapps" + File.separator + "vista_ccc" + File.separator;
   // public final static String configFile = "C:\\Users\\aniva\\Desktop\\tigoProject\\projecto\\PROJECTO_VASTRIX\\PROJECT_VASTRIX_MEJORADO\\VISTA360\\vista360\\";
    //public final static String configFile = "G:\\VISTA_360 gestionHE\\vista360-gestioneh\\";
//    public final static String configFile = "/home/navafusr/myapps/";
    //  public final static String configFile = "C:\\Users\\aniva\\Desktop\\tigoProject\\projectosVISTA_360\\vista360Nuevo\\";


    protected static final Properties prop = new Properties();
    public static Logger log = Logger.getLogger(Parametros.class);

    static {
        try {
            Parametros p = new Parametros();
            p.init();
        } catch (Exception e) {
            log.error("Error al cargar archivo de propiedades pathFile: " + configFile, e);
        }
    }

    public static String activeInitialContext;
    public static String activeProviderUrl;
    public static String activeSecurityAuthentication;
    public static String activeSecurityPrincipal;
    public static String activeSecurityCredentials;
    public static String activeSecurityUser;
    public static String activeDominio;
    public static Boolean consultarDirectorioActivo;
    public static String usuario;
    public static String password;
    public static int nroIntentos;
    public static int tiempoFuera;
    public static int timpoFueraPoolConexion;
    public static String dominioAccessOrigin;

    public static String tituloAplicacion;
    public static String tituloPiePagina;
    public static String expresionRegularTextoNormal;
    public static String expresionRegularUsuario;
    public static String expresionRegularPassword;
    public static String expresionRegularNroTigo;
    public static String mensajeValidacionUsuario;
    public static String mensajeValidacionPassword;
    public static String mensajeValidacionTextoNormal;
    public static String mensajeValidacionIp;

    public static String mensajeValidacionIncidente;
    public static String mensajeReporteIncidente;

    public static String fechaFormatWs;
    public static String fechaFormatPage;
    public static Boolean saveRequestResponse;

    public static String voucherWsdl;
    public static int voucherTimeOut;
    public static String voucherPathKeystore;
    public static String voucherVersion;
    public static String voucherLoginSystemCode;
    public static String voucherPassword;
    public static String voucherOperatorId;
    public static String voucherTimeType;
    public static String voucherOwnerShipInfoId;
    public static String voucherOperationType;
    public static String voucherCardState;
    public static String voucherServiceProvider;
    public static double voucherDivisor;
    public static int voucherNroDecimales;

    public static String cugWsdl;
    public static int cugTimeOut;
    public static String cugPathKeystore;
    public static String cugVersion;
    public static String cugLoginSystemCode;
    public static String cugPassword;
    public static String cugOperatorId;
    public static String cugTimeType;
    public static String cugOwnerShipInfoId;
    public static String cugMemberTypeCode;

    public static String callingCircleFetchRowNum;

    public static String billeteraWsdl;
    public static int billeteraTimeOut;
    public static String billeteraPathKeystore;
    public static String billeteraVersion;
    public static String billeteraLoginSystemCode;
    public static String billeteraPassword;
    public static String billeteraOperatorId;
    public static String billeteraTimeType;
    public static String billeteraOwnerShipInfoId;
    public static String billeterabalancetype;
    public static String billeterafreeunittype;
    public static double billeteraDivisor;
    public static int billeteraNroDecimales;
    public static String billeteraAccKey;
    public static String billeteraBuscarPrefijo;
    public static Boolean billeteraValidarFechaExpiracion;
    public static String billeteraFreeunitAdditionalPropertyCode;
    public static String billeteraFreeunitAdditionalPropertyValue;
    public static String billeteraExpired;
    public static String billeteraCorebalanceBalanceType;

    public static String historialWsdl;
    public static int historialTimeOut;
    public static String historialPathKeystore;
    public static String historialVersion;
    public static String historialLoginSystemCode;
    public static String historialPassword;
    public static String historialOperatorId;
    public static String historialTimeType;
    public static String historialOwnerShipInfoId;

    public static String historialServiceCategory;
    public static String historialFlowType;
    public static String historialServiceType;

    public static String historialTotalCdrNum;
    public static String historialFetchRowNum;
    public static Boolean historialConIteracion;
    public static String historialRoamFlag;
    public static String historialSpecialNumberIndicator;
    public static String historialAdjustmentType;
    public static String historialRechargeType;
    public static String historialRechargeChannelId;
    public static double historialDivisor;
    public static int historialNroDecimales;
    public static String historialResultCode;
    public static String historialReversalFlag;
    public static int historialHoraTemporal;
    public static String historialRefundIndicator;
    public static String historialAdjustmentChannelId;
    public static int historialForDays;
    public static int historialBetweenDay;

    public static String chcWsdl;
    public static int chcTimeOut;

    public static String mnpdaWsdl;
    public static int mnpdaTimeOut;
    public static String mnpdaComsumerId;
    public static String mnpdaTransaccionId;
    public static String mnpdaCorrelationId;
    public static String mnpdaCountry;

    // COMANDOS
    public static String vistaHistorial;
    public static String vistaCallingCircle;
    public static int comandoCallingCircle;
    public static String vistaCug;
    public static int comandoCug;
    public static String vistaBilletera;
    public static int comandoBilletera;
    public static String vistaVoucher;
    public static int comandoVoucher;
    public static String vistaOferta;
    public static int comandoOferta;
    public static String ofertaPaymentMode;
    public static String ofertaStatus;
    public static String ofertaBundledFlag;
    public static String ofertaOfferingClass;
    public static String ofertaStatusDetailPositions;
    public static String ofertaStatusDetailMsgNotFound;
    public static String ofertaStatusBlackListDescription;
    public static String vistaCCC;
    public static int comandoCCC;

    public static String vistaDetalleNavegacionPorcentajeConsumo;
    public static int comandoDetalleNavegacionPorcentajeConsumo;
    public static String vistaDetalleNavegacionTiempoVsConsumo;
    public static int comandoDetalleNavegacionTiempoVsConsumo;
    public static String vistaDetalleNavegacionTablaConsumo;
    public static int comandoDetalleNavegacionTablaConsumo;
    public static String vistaDetalleNavegacionAnchoBanda;
    public static int comandoDetalleNavegacionAnchoBanda;
    public static String vistaDetalleNavegacionExportar;
    public static int comandoDetalleNavegacionExportar;

    public static String numerosFavoritos;
    public static String vistaNumeroFavorito;
    public static int comandoNumeroFavorito;

    public static String numeroFavoritoFnSerial;
    public static String numeroFavoritoFnNumber;
    public static String numeroFavoritoFnType;
    public static String numeroFavoritoFnGroup;

    // LOTHAR
    public static String lotharProtocolo;
    public static String lotharIp;
    public static String lotharPuerto;
    public static String lotharContexto;
    public static String lotharCallingCirclePageConsultar;
    public static String lotharCallingCirclePageCrearGrupo;
    public static String lotharCallingCirclePageBorrarGrupo;
    public static String lotharCallingCirclePageAgregarMiembro;
    public static String lotharCallingCirclePageCambiarTarifaMiembro;
    public static String lotharCallingCirclePageBorrarMiembro;

    public static String lotharCugPageConsultar;
    public static String lotharCugPageCrearGrupo;
    public static String lotharCugPageBorrarGrupo;
    public static String lotharCugPageAgregarMiembro;
    public static String lotharCugPageCambiarTipoMiembro;
    public static String lotharCugPageBorrarMiembro;

    public static String customizedWsdl;
    public static int customizedTimeOut;
    public static String customizedPathKeystore;
    public static String customizedVersion;
    public static String customizedLoginSystemCode;
    public static String customizedPassword;
    public static String customizedOperatorId;
    public static String customizedTimeType;
    public static String customizedOwnerShipInfoId;

    public static String incidenteVistas;

    public static String cnxTelnetHlrComandUno;
    public static String cnxTelnetHlrComandDos;
    // public static String tramaContainHlr;

    public static String cnxTelnetGGSNComandCero;
    public static String cnxTelnetGGSNComandUno;
    public static String tramaContainGGSN;

    public static String cnxTelnetMscComandUno;
    public static String cnxTelnetMscComandDos;
    public static String cnxTelnetMscComandTres;
    public static String cnxSshMscComandUno;
    public static String cnxSshMscComandCuatro;
    public static String cnxSshMscComandCinco;
    // public static String tramaContainMsc;

    public static String esperaRespuestaElementosRed;
    public static String esperaRespuestaElementosRedGGSN;
    public static String cnxTelnetGGSNComandEnter;

    // public static String containValidacionExcepcionHlrUno;
    // public static String containValidacionExcepcionHlrDos;
    // public static String containValidacionExcepcionHlrTres;
    //
    // public static String containValidacionExcepcionGgsnUno;
    // public static String containValidacionExcepcionGgsnDos;
    // public static String containValidacionExcepcionGgsnTres;
    //
    // public static String containValidacionExcepcionMscUno;
    // public static String containValidacionExcepcionMscDos;
    // public static String containValidacionExcepcionMscTres;

    public static String nroReintentosConexionTelnet;

    public static String esperaRespuestaElementosRedSsh;
    public static String esperaRespuestaElementosRedGGSNSsh;

    public static String dsTusMegasPorAplicacion;
    public static String urlServletParametroNodos;

    public static String jndi;
    public static String urlProvider;

    public static boolean enabledColumnCurrentAmount;

    // DEUDAS TTP
    public static String mensajeLineaSinDeuda;
    public static String mensajeLineaConCNS;
    public static String mensajeLineaDeudaNoCorresponde;
    public static String mensajeLineaConMontoNoValido;
    public static String mensajeLineaNoPortada;
    public static String mensajeLineaErrorEstadoNoValido;
    public static String mensajeLineaNoEncontrada;
    public static String estadosValidosParaCrearCns;
    public static String vistaDeudaTtp;
    public static int comandoDeudaTtp;
    public static String estadosColores;
    public static String estadosInternoPantalla;
    public static String estadosInternoCorte;
    public static String reporteTitulo;

    public static String gestorDeudasWsdl;
    public static int gestorDeudasTimeOut;
    public static String gestorDeudasPathKeystore;
    public static String patronFechaDetalleDeuda;

    public static String facturarDeudaWsdl;
    public static int facturarDeudaTimeOut;
    public static String facturarDeudaPathKeystore;

    public static String deudaTtpLocaleLenguage;
    public static String deudaTtpLocaleCountry;
    public static int deudaTtpNroDecimales;
    public static String deudaTtpMensajeConfirmacionCrearCns;
    public static String estadoValidoDeudaCero;

    // TTP Version Genearacion de CNS con multiple detalle
    public static int opcionConsumoServicioFacturarDeuda;
    public static int opcionConsumoServicioDeudaDetallada;
    public static String userServiceFaturarTTP;
    public static String passServiceFacturarTTP;
    public static String userServiceDDTTP;
    public static String passServiceDDTTP;

    public static String urlServiceReporteBccs;
    public static String formatoFechaReporteBccs;
    public static int timeoutServiceResporteBccs;
    public static String usuarioServicioBCCS;
    public static String passwordServicioBCCS;
    public static String vistaReporteBCCS;
    public static int comandoReportesBCCS;
    public static int comandoCuentaMigrada;
    public static String urlCuentaMigrada;
    public static String tokenCuentaMigradaAuth;
    public static String estadoCuenta;
    public static int timeOutServiceCuentaMigrada;
    // Parametros change request Reportes BCCS

    public static String urlWhiteListService;
    public static String tokenWhiteListAuth;
    public static String patternFechaSendSms;
    public static int timeOutServiceWhiteList;
    public static String startChannelRequestWhiteList;
    public static String endChannelRequestWhiteList;
    public static int rangoDiasConsultaLogs;
    public static String urlSendSmsService;
    public static String tokenSendSmsAuth;
    public static int timeOutServiceSendSms;
    public static String vistaWhiteList;
    public static int comandoWhiteList;

    //DETALLE TRANSACCION
    public static String urlDetalleTransaccionService;
    public static String patternFechaDetalleTransaccion;
    public static int timeOutServiceDetalleTransaccion;
    public static int detalleRangoDiasConsultaLogs;
    public static String vistaDetalleTransaccion;
    public static int comandoDetalleTransaccion;
    public static String queryTargetIDDetalleTransaccion;
    public static String queryUsageEventsDetalleTransaccion;
    public static String queryRechargeEventsDetalleTransaccion;
    public static String queryAdjustmentEventsDetalleTransaccion;
    public static String DetalleEspecification;
    public static int DetalleNroDecimales;


    public static String restUrlToken;
    public static String restUrlParametrosToken;
    public static String restparametrosToken;
    public static String restMensajeErrorToken;
    public static int restTimeoutToken;


    public static String restUrlQueryUSer;
    public static String restUrlParametrosQueryUSer;
    public static String restMensajeEmptyQueryUSer;
    public static String restMensajeErrorQueryUSer;
    public static int restTimeoutQueryUSer;

    public static String restUrlResetPin;
    public static String restUrlParametrosResetPin;
    public static String restBodyResetPin;
    public static String restMensajeErrorResetPin;
    public static String restMensajeExitosoResetPin;
    public static int restTimeoutResetPin;
    public static boolean mostrarPin;


    public static String restUrlSMS;
    public static String restUrlBodySMS;
    public static String restMensajeSMS;
    public static int restTimeoutSMS;

    public static String restUrlEnableUser;
    public static String restUrlParametrosEnableUser;
    public static String restUrlBodyEnableUser;
    public static String restMensajeEnableUser;
    public static int restTimeoutEnableUser;

    public static String pinTipo;
    public static int pinLonguitud;
    public static int pinCola;


    public static String ehCategoria;
    public static String ehPermisos;
    public static String ehPrefijoTelefono;
    public static String ehSummaryMensajes;
    public static String ehIdentificadorSucursales;


    public static String ehExpresionCodigo;
    public static String ehExpresionCodigoBuscador;
    public static String ehExpresionCodigoMensajeError;
    public static String ehExpresionCodigoMensajeRequerido;

    public static String ehExpresionEmail;
    public static String ehExpresionEmailMensajeError;


    public static String ehExpresionNombres;
    public static String ehExpresionNombresMensajeError;
    public static String ehExpresionNombresMensajeRequerido;

    public static String ehExpresionApellidos;
    public static String ehExpresionApellidosMensajeError;
    public static String ehExpresionApellidosMensajeRequerido;

    public static String ehExpresionTelefono;
    public static String ehExpresionTelefonoMensajeError;
    public static String ehExpresionTelefonoMensajeRequerido;

    public static String ehExpresionLimite;
    public static String getEhExpresionLimiteMensajeError;
    public static String getEhExpresionLimiteMensajeRequerido;

    //QueryGroup
    public static String restUrlQueryGroup;
    public static String restUrlParametrosQueryGroup;
    public static String restMensajeEmptyQueryGroup;
    public static String restMensajeErrorQueryGroup;
    public static int restTimeoutQueryGroup;
    public static String restListaExcepcionesQueryGroup;
    public static String restListaAceptadosQueryGroup;

    //QueryUserGroup
    public static String restUrlQueryUserGroup;
    public static String restUrlParametrosQueryUserGroup;
    public static String restMensajeErrorQueryUserGroup;
    public static int restTimeoutQueryUserGroup;
    public static String restListaExcepcionesQueryUserGroup;
    public static String restListaAceptadosQueryUserGroup;


    //Create User
    public static String restUrlCreateUser;
    public static String restUrlParametrosCreateUser;
    public static String restUrlBodyCreateUser;
    public static String restMensajeCreateUser;
    public static String restMensajeErrorCreateUser;
    public static int restTimeoutCreateUser;
    public static String restListaExcepcionesCreateUser;
    public static String restListaAceptadosCreateUser;

    //Update User
    public static String restUrlUpdateUser;
    public static String restUrlParametrosUpdateUser;
    public static String restUrlBodyUpdateUser;
    public static String restMensajeUpdateUser;
    public static String restMensajeErrorUpdateUser;
    public static int restTimeoutUpdateUser;
    public static String restListaExcepcionesUpdateUser;
    public static String restListaAceptadosUpdateUser;


    //AddGroupUser
    public static String restUrlAddGroupUser;
    public static String restUrlParametrosAddGroupUser;
    public static String restMensajeAddGroupUser;
    public static String restMensajeErrorAddGroupUser;
    public static int restTimeoutAddGroupUser;
    public static String restListaExcepcionesAddGroupUser;
    public static String restListaAceptadosAddGroupUser;

    //DeleteGroupUser
    public static String restUrlDeleteGroupUser;
    public static String restUrlParametrosDeleteGroupUser;
    public static String restMensajeDeleteGroupUser;
    public static String restMensajeErrorDeleteGroupUser;
    public static int restTimeoutDeleteGroupUser;
    public static String restListaExcepcionesDeleteGroupUser;
    public static String restListaAceptadosDeleteGroupUser;

    //DeleteUser
    public static String restUrlDeleteUser;
    public static String restUrlParametrosDeleteUser;
    public static String restMensajeDeleteUser;
    public static String restMensajeErrorDeleteUser;
    public static int restTimeoutDeleteUser;
    public static String restListaExcepcionesDeleteUser;
    public static String restListaAceptadosDeleteUser;


    //GESTION SUPERVISORES VENDEDORES
    public static String gestionSupVenUsuarioRequerido;
    public static String gestionSupVentipoUsuario;
    public static String gestionSupVenUsuarioNoEncontrado;
    public static String gestionSupVenSeleccionZona;
    public static String gestionSupVenGuardarCambios;
    public static String gestionSupVenEliminarDealer;
    public static String gestionSupVenNameAccion;
    public static String gestionSupVenEliminarZonas;




    private static void init() {
        // InputStream inputStream = null;
        FileInputStream is = null;

        try { // Ubicar el archivo en el ClassPath actual
            Properties prop = new Properties();

            // URL location =
            // Parametros.class.getProtectionDomain().getCodeSource().getLocation();
            // File classes = new File(location.getPath());
            // File webinf = new File(classes.getParent());
            // File vistaccc = new File(webinf.getParent());
            // File deployments = new File(vistaccc.getParent());
            // File standalone = new File(deployments.getParent());

            // String strStandalone = standalone.getAbsolutePath().replace("\\",
            // File.separator) + Parametros.configFile;

            // inputStream = this.getClass().getResourceAsStream("/" +
            // Parametros.configFile);
            // is = new FileInputStream(strStandalone);

            is = new FileInputStream(Parametros.configFile + "vista_ccc.properties");
            //is = new FileInputStream(Parametros.configFile + "vista_360.properties");

            // if (inputStream != null) {
            if (is != null) {

                // prop.load(inputStream);
                prop.load(is);

                log.info("cargando parametros......");

                Parametros.urlDetalleTransaccionService = prop.getProperty("DETALLE.QGL.URL.SERVICE");
                Parametros.patternFechaDetalleTransaccion = prop.getProperty("DETALLE.PATTERN.FECHA.FOR_IMPUT");
                Parametros.timeOutServiceDetalleTransaccion = Integer.parseInt(prop.getProperty("DETALLE.SERVICE.TIMEOUT"));
                Parametros.detalleRangoDiasConsultaLogs = Integer.parseInt(prop.getProperty("DETALLE.RANGO.DIAS.CONSULTA_LOGS"));
                Parametros.vistaDetalleTransaccion = prop.getProperty("DETALLE.TRANSACCION.VISTA");
                Parametros.comandoDetalleTransaccion = Integer.parseInt(prop.getProperty("DETALLE.COMANDO"));
                Parametros.queryTargetIDDetalleTransaccion = prop.getProperty("DETALLE.QUERY.TARGETID");
                Parametros.queryUsageEventsDetalleTransaccion = prop.getProperty("DETALLE.QUERY.USAGEVENTS");
                Parametros.queryRechargeEventsDetalleTransaccion = prop.getProperty("DETALLE.QUERY.RECHARGEVENTS");
                Parametros.queryAdjustmentEventsDetalleTransaccion = prop.getProperty("DETALLE.QUERY.ADJUSTMENTEVENTS");
                Parametros.DetalleEspecification = prop.getProperty("DETALLE.ESPECIFICATION");
                Parametros.DetalleNroDecimales = Integer.parseInt(prop.getProperty("DETALLE.NRO.DECIMALES"));

                Parametros.urlWhiteListService = prop.getProperty("WHITELIST.REST.URL.SERVICE");
                Parametros.tokenWhiteListAuth = prop.getProperty("WHITELIST.TOKEN.AUTH_BASIC");
                Parametros.patternFechaSendSms = prop.getProperty("SEND_SMS.PATTERN.FECHA.FOR_IMPUT");
                Parametros.timeOutServiceWhiteList = Integer.parseInt(prop.getProperty("WHILELIST.SERVICE.TIMEOUT"));
                Parametros.startChannelRequestWhiteList = prop.getProperty("WHITELIST.SERVICE.REQUEST.START_CHANNEL");
                Parametros.endChannelRequestWhiteList = prop.getProperty("WHITELIST.SERVICE.REQUEST.END_CHANNEL");
                Parametros.rangoDiasConsultaLogs = Integer.parseInt(prop.getProperty("SEND_SMS.RANGO.DIAS.CONSULTA_LOGS"));
                Parametros.urlSendSmsService = prop.getProperty("SEND_SMS.REST.URL.SERVICE");
                Parametros.tokenSendSmsAuth = prop.getProperty("SEND_SMS.TOKEN.AUTH_BASIC");
                Parametros.timeOutServiceSendSms = Integer.parseInt(prop.getProperty("SEND_SMS.SERVICE.TIMEOUT"));
                Parametros.vistaWhiteList = prop.getProperty("WHITELIST.VISTA");
                Parametros.comandoWhiteList = Integer.parseInt(prop.getProperty("WHITELIST.COMANDO"));

                Parametros.comandoReportesBCCS = Integer.parseInt(prop.getProperty("REPORTE_BCCS.COMANDO"));
                Parametros.vistaReporteBCCS = prop.getProperty("REPORTE_BCCS.VISTA");
                Parametros.usuarioServicioBCCS = prop.getProperty("REPORTE_BCCS.USUARIO_SERVICIO");
                Parametros.passwordServicioBCCS = prop.getProperty("REPORTE_BCCS.PASSWORD_SERVICIO");
                Parametros.formatoFechaReporteBccs = prop.getProperty("REPORTE_BCCS.FORMATO_FECHA");
                Parametros.timeoutServiceResporteBccs = Integer
                        .parseInt(prop.getProperty("REPORTE_BCCS.TIMEOUT.WEB_SERVICE"));
                Parametros.urlServiceReporteBccs = prop.getProperty("REPORTE_BCCS.URL.WEB_SERVICE");
                Parametros.comandoCuentaMigrada = Integer.parseInt(prop.getProperty("REPORTE_BCCS.CUENTA_MIGRADA.COMANDO"));
                Parametros.urlCuentaMigrada = prop.getProperty("CUENTA_MIGRADA.REST.URL.SERVICE");
                Parametros.tokenCuentaMigradaAuth = prop.getProperty("CUENTA_MIGRADA.TOKEN.AUTH_BASIC");
                Parametros.estadoCuenta = prop.getProperty("CUENTA_MIGRADA.ESTADO");
                Parametros.timeOutServiceCuentaMigrada = Integer.parseInt(prop.getProperty("CUENTA_MIGRADA.SERVICE.TIMEOUT"));

                Parametros.activeInitialContext = prop.getProperty("DIRECTORIO_ACTIVO.initial_context_factory");
                Parametros.activeProviderUrl = prop.getProperty("DIRECTORIO_ACTIVO.provider_url");
                Parametros.activeSecurityAuthentication = prop.getProperty("DIRECTORIO_ACTIVO.security_authentication");
                Parametros.activeSecurityPrincipal = prop.getProperty("DIRECTORIO_ACTIVO.security_principal");
                Parametros.activeSecurityCredentials = prop.getProperty("DIRECTORIO_ACTIVO.security_credentials");
                Parametros.activeSecurityUser = prop.getProperty("DIRECTORIO_ACTIVO.security_user");
                Parametros.activeDominio = prop.getProperty("DIRECTORIO_ACTIVO.dominio");
                Parametros.consultarDirectorioActivo = Boolean.valueOf(prop.getProperty("CONSULTAR.DIRECTORIO.ACTIVO"));
                Parametros.usuario = prop.getProperty("USUARIO");
                Parametros.password = prop.getProperty("PASSWORD");
                Parametros.nroIntentos = Integer.parseInt(prop.getProperty("NRO.INTENTOS"));
                Parametros.tiempoFuera = Integer.parseInt(prop.getProperty("TIEMPO.FUERA"));
                Parametros.timpoFueraPoolConexion = Integer.parseInt(prop.getProperty("TIEMPO.FUERA.POOL.CONEXION"));

                Parametros.tituloAplicacion = prop.getProperty("TITULO.APLICACION");
                Parametros.tituloPiePagina = prop.getProperty("TITULO.PIE.PAGINA");
                Parametros.expresionRegularTextoNormal = prop.getProperty("EXPRESION.REGULAR.TEXTO.NORMAL");
                Parametros.expresionRegularUsuario = prop.getProperty("EXPRESION.REGULAR.USUARIO");
                Parametros.expresionRegularPassword = prop.getProperty("EXPRESION.REGULAR.PASSWORD");
                Parametros.expresionRegularNroTigo = prop.getProperty("EXPRESION.REGULAR.NRO.TIGO");
                Parametros.mensajeValidacionUsuario = prop.getProperty("MENSAJE.VALIDACION.USUARIO");
                Parametros.mensajeValidacionPassword = prop.getProperty("MENSAJE.VALIDACION.PASSWORD");
                Parametros.mensajeValidacionTextoNormal = prop.getProperty("MENSAJE.VALIDACION.TEXTO.NORMAL");
                Parametros.mensajeValidacionIp = prop.getProperty("MENSAJE.VALIDACION.IP");

                Parametros.mensajeValidacionIncidente = prop.getProperty("MENSAJE.VALIDACION.INCIDENTE");
                Parametros.mensajeReporteIncidente = prop.getProperty("MENSAJE.REPORTE.INCIDENTE");

                Parametros.fechaFormatWs = prop.getProperty("FECHA.FORMAT.WS");
                Parametros.fechaFormatPage = prop.getProperty("FECHA.FORMAT.PAGE");
                Parametros.saveRequestResponse = Boolean.valueOf(prop.getProperty("SAVE.REQUEST.RESPONSE"));

                Parametros.voucherWsdl = prop.getProperty("VOUCHER.WSDL");
                Parametros.voucherTimeOut = Integer.parseInt(prop.getProperty("VOUCHER.TIMEOUT"));
                Parametros.voucherPathKeystore = prop.getProperty("VOUCHER.PATH.KEYSTORE");
                Parametros.voucherVersion = prop.getProperty("VOUCHER.VERSION");
                Parametros.voucherLoginSystemCode = prop.getProperty("VOUCHER.LOGIN.SYSTEM.CODE");
                Parametros.voucherPassword = prop.getProperty("VOUCHER.PASSWORD");
                Parametros.voucherOperatorId = prop.getProperty("VOUCHER.OPERATOR.ID");
                Parametros.voucherTimeType = prop.getProperty("VOUCHER.TIME.TYPE");
                Parametros.voucherOwnerShipInfoId = prop.getProperty("VOUCHER.OWNER.SHIP.INFO.ID");
                Parametros.voucherOperationType = prop.getProperty("VOUCHER.OPERATION.TYPE");
                Parametros.voucherCardState = prop.getProperty("VOUCHER.CARD.STATE");
                Parametros.voucherServiceProvider = prop.getProperty("VOUCHER.SERVICE.PROVIDER");
                Parametros.voucherDivisor = Double.parseDouble(prop.getProperty("VOUCHER.DIVISOR"));
                Parametros.voucherNroDecimales = Integer.parseInt(prop.getProperty("VOUCHER.NRO.DECIMALES"));

                Parametros.cugWsdl = prop.getProperty("CUG.WSDL");
                Parametros.cugTimeOut = Integer.parseInt(prop.getProperty("CUG.TIMEOUT"));
                Parametros.cugPathKeystore = prop.getProperty("CUG.PATH.KEYSTORE");
                Parametros.cugVersion = prop.getProperty("CUG.VERSION");
                Parametros.cugLoginSystemCode = prop.getProperty("CUG.LOGIN.SYSTEM.CODE");
                Parametros.cugPassword = prop.getProperty("CUG.PASSWORD");
                Parametros.cugOperatorId = prop.getProperty("CUG.OPERATOR.ID");
                Parametros.cugTimeType = prop.getProperty("CUG.TIME.TYPE");
                Parametros.cugOwnerShipInfoId = prop.getProperty("CUG.OWNER.SHIP.INFO.ID");
                Parametros.cugMemberTypeCode = prop.getProperty("CUG.MEMBER.TYPE.CODE");

                Parametros.callingCircleFetchRowNum = prop.getProperty("CALLING.CIRCLE.FETCH.ROW.NUM");

                Parametros.billeteraWsdl = prop.getProperty("BILLETERA.WSDL");
                Parametros.billeteraTimeOut = Integer.parseInt(prop.getProperty("BILLETERA.TIMEOUT"));
                Parametros.billeteraPathKeystore = prop.getProperty("BILLETERA.PATH.KEYSTORE");
                Parametros.billeteraVersion = prop.getProperty("BILLETERA.VERSION");
                Parametros.billeteraLoginSystemCode = prop.getProperty("BILLETERA.LOGIN.SYSTEM.CODE");
                Parametros.billeteraPassword = prop.getProperty("BILLETERA.PASSWORD");
                Parametros.billeteraOperatorId = prop.getProperty("BILLETERA.OPERATOR.ID");
                Parametros.billeteraTimeType = prop.getProperty("BILLETERA.TIME.TYPE");
                Parametros.billeteraOwnerShipInfoId = prop.getProperty("BILLETERA.OWNER.SHIP.INFO.ID");
                Parametros.billeterabalancetype = prop.getProperty("BILLETERA.BALANCE.TYPE");
                Parametros.billeterafreeunittype = prop.getProperty("BILLETERA.FREE.UNIT.TYPE");
                Parametros.billeteraDivisor = Double.parseDouble(prop.getProperty("BILLETERA.DIVISOR"));
                Parametros.billeteraNroDecimales = Integer.parseInt(prop.getProperty("BILLETERA.NRO.DECIMALES"));
                Parametros.billeteraAccKey = prop.getProperty("BILLETERA.ACCKEY");
                Parametros.billeteraBuscarPrefijo = prop.getProperty("BILLETERA.BUSCAR.PREFIJO");
                Parametros.billeteraValidarFechaExpiracion = Boolean
                        .valueOf(prop.getProperty("BILLETERA.VALIDAR.FECHA.EXPIRACION"));
                Parametros.billeteraFreeunitAdditionalPropertyCode = prop
                        .getProperty("BILLETERA.FREEUNIT.ADDITIONAL.PROPERTY.CODE");
                Parametros.billeteraFreeunitAdditionalPropertyValue = prop
                        .getProperty("BILLETERA.FREEUNIT.ADDITIONAL.PROPERTY.VALUE");
                Parametros.billeteraExpired = prop.getProperty("BILLETERA.EXPIRED");
                Parametros.billeteraCorebalanceBalanceType = prop.getProperty("BILLETERA.COREBALANCE.BALANCETYPE");

                Parametros.historialWsdl = prop.getProperty("HISTORIAL.WSDL");
                Parametros.historialTimeOut = Integer.parseInt(prop.getProperty("HISTORIAL.TIMEOUT"));
                Parametros.historialPathKeystore = prop.getProperty("HISTORIAL.PATH.KEYSTORE");
                Parametros.historialVersion = prop.getProperty("HISTORIAL.VERSION");
                Parametros.historialLoginSystemCode = prop.getProperty("HISTORIAL.LOGIN.SYSTEM.CODE");
                Parametros.historialPassword = prop.getProperty("HISTORIAL.PASSWORD");
                Parametros.historialOperatorId = prop.getProperty("HISTORIAL.OPERATOR.ID");
                Parametros.historialTimeType = prop.getProperty("HISTORIAL.TIME.TYPE");
                Parametros.historialOwnerShipInfoId = prop.getProperty("HISTORIAL.OWNER.SHIP.INFO.ID");

                Parametros.historialServiceCategory = prop.getProperty("HISTORIAL.SERVICE.CATEGORY");
                Parametros.historialFlowType = prop.getProperty("HISTORIAL.FLOW.TYPE");
                Parametros.historialServiceType = prop.getProperty("HISTORIAL.SERVICE.TYPE");

                Parametros.historialTotalCdrNum = prop.getProperty("HISTORIAL.TOTAL.CDR.NUM");
                Parametros.historialFetchRowNum = prop.getProperty("HISTORIAL.FETCH.ROW.NUM");

                Parametros.historialConIteracion = Boolean.valueOf(prop.getProperty("HISTORIAL.CON.ITERACION"));
                Parametros.historialRoamFlag = prop.getProperty("HISTORIAL.ROAM.FLAG");
                Parametros.historialSpecialNumberIndicator = prop.getProperty("HISTORIAL.SPECIAL.NUMBER.INDICATOR");
                Parametros.historialAdjustmentType = prop.getProperty("HISTORIAL.ADJUSTMENT.TYPE");
                Parametros.historialRechargeType = prop.getProperty("HISTORIAL.RECHARGE.TYPE");
                Parametros.historialRechargeChannelId = prop.getProperty("HISTORIAL.RECHARGE.CHANNEL.ID");
                Parametros.historialDivisor = Double.parseDouble(prop.getProperty("HISTORIAL.DIVISOR"));
                Parametros.historialNroDecimales = Integer.parseInt(prop.getProperty("HISTORIAL.NRO.DECIMALES"));

                Parametros.historialResultCode = prop.getProperty("HISTORIAL.RESULT.CODE");
                Parametros.historialReversalFlag = prop.getProperty("HISTORIAL.REVERSAL.FLAG");
                Parametros.historialHoraTemporal = Integer.parseInt(prop.getProperty("HISTORIAL.HORA.TEMPORAL"));

                Parametros.historialRefundIndicator = prop.getProperty("HISTORIAL.REFUND.INDICATOR");
                Parametros.historialAdjustmentChannelId = prop.getProperty("HISTORIAL.ADJUSTMENT.CHANNEL.ID");

                Parametros.historialForDays = Integer.parseInt(prop.getProperty("HISTORIAL.FOR.DAYS"));
                Parametros.historialBetweenDay = Integer.parseInt(prop.getProperty("HISTORIAL.BETWEEN.DAY"));

                Parametros.chcWsdl = prop.getProperty("CHC.WSDL");
                Parametros.chcTimeOut = Integer.parseInt(prop.getProperty("CHC.TIMEOUT"));

                Parametros.mnpdaWsdl = prop.getProperty("MNPDA.WSDL");
                Parametros.mnpdaTimeOut = Integer.parseInt(prop.getProperty("MNPDA.TIMEOUT"));
                Parametros.mnpdaComsumerId = prop.getProperty("MNPDA.CONSUMER.ID");
                Parametros.mnpdaTransaccionId = prop.getProperty("MNPDA.TRANSACCION.ID");
                Parametros.mnpdaCorrelationId = prop.getProperty("MNPDA.CORRELATION.ID");
                Parametros.mnpdaCountry = prop.getProperty("MNPDA.COUNTRY");

                // comando
                Parametros.vistaHistorial = prop.getProperty("HISTORIAL.VISTA");
                Parametros.vistaCallingCircle = prop.getProperty("CALLING.CIRCLE.VISTA");
                Parametros.comandoCallingCircle = Integer.parseInt(prop.getProperty("CALLING.CIRCLE.COMANDO"));
                Parametros.vistaCug = prop.getProperty("CUG.VISTA");
                Parametros.comandoCug = Integer.parseInt(prop.getProperty("CUG.COMANDO"));
                Parametros.vistaBilletera = prop.getProperty("BILLETERA.VISTA");
                Parametros.comandoBilletera = Integer.parseInt(prop.getProperty("BILLETERA.COMANDO"));
                Parametros.vistaVoucher = prop.getProperty("VOUCHER.VISTA");
                Parametros.comandoVoucher = Integer.parseInt(prop.getProperty("VOUCHER.COMANDO"));
                Parametros.vistaOferta = prop.getProperty("OFERTA.VISTA");
                Parametros.comandoOferta = Integer.parseInt(prop.getProperty("OFERTA.COMANDO"));
                Parametros.ofertaPaymentMode = prop.getProperty("OFERTA.PAYMENT.MODE");
                Parametros.vistaCCC = prop.getProperty("CCC.VISTA");
                Parametros.comandoCCC = Integer.parseInt(prop.getProperty("CCC.COMANDO"));

                Parametros.vistaDetalleNavegacionPorcentajeConsumo = prop
                        .getProperty("DETALLE.NAVEGACION.GRAFICO.PORCENTAJE.CONSUMO.VISTA");
                Parametros.comandoDetalleNavegacionPorcentajeConsumo = Integer
                        .parseInt(prop.getProperty("DETALLE.NAVEGACION.GRAFICO.PORCENTAJE.CONSUMO.COMANDO"));
                Parametros.vistaDetalleNavegacionTiempoVsConsumo = prop
                        .getProperty("DETALLE.NAVEGACION.GRAFICO.TIEMPO.VS.CONSUMO.VISTA");
                Parametros.comandoDetalleNavegacionTiempoVsConsumo = Integer
                        .parseInt(prop.getProperty("DETALLE.NAVEGACION.GRAFICO.TIEMPO.VS.CONSUMO.COMANDO"));
                Parametros.vistaDetalleNavegacionTablaConsumo = prop
                        .getProperty("DETALLE.NAVEGACION.TABLA.CONSUMO.VISTA");
                Parametros.comandoDetalleNavegacionTablaConsumo = Integer
                        .parseInt(prop.getProperty("DETALLE.NAVEGACION.TABLA.CONSUMO.COMANDO"));
                Parametros.vistaDetalleNavegacionAnchoBanda = prop
                        .getProperty("DETALLE.NAVEGACION.GRAFICO.ANCHO.BANDA.VISTA");
                Parametros.comandoDetalleNavegacionAnchoBanda = Integer
                        .parseInt(prop.getProperty("DETALLE.NAVEGACION.GRAFICO.ANCHO.BANDA.COMANDO"));
                Parametros.vistaDetalleNavegacionExportar = prop.getProperty("DETALLE.NAVEGACION.EXPORTAR.VISTA");
                Parametros.comandoDetalleNavegacionExportar = Integer
                        .parseInt(prop.getProperty("DETALLE.NAVEGACION.EXPORTAR.COMANDO"));

                Parametros.ofertaStatus = prop.getProperty("OFERTA.STATUS");
                Parametros.ofertaBundledFlag = prop.getProperty("OFERTA.BUNDLED.FLAG");
                Parametros.ofertaOfferingClass = prop.getProperty("OFERTA.OFFERING.CLASS");

                Parametros.ofertaStatusDetailPositions = prop.getProperty("OFERTA.STATUS.DETAIL.POSITIONS");
                Parametros.ofertaStatusDetailMsgNotFound = prop.getProperty("OFERTA.STATUS.DETAIL.MSG.NOT.FOUND");
                Parametros.ofertaStatusBlackListDescription = prop.getProperty("OFERTA.STATUS.BLACKLIST.DESCRIPTION");

                Parametros.numerosFavoritos = prop.getProperty("NUMEROS.FAVORITOS.ID");
                Parametros.vistaNumeroFavorito = prop.getProperty("NUMERO.FAVORITO.VISTA");
                Parametros.comandoNumeroFavorito = Integer.parseInt(prop.getProperty("NUMERO.FAVORITO.COMANDO"));

                Parametros.numeroFavoritoFnSerial = prop.getProperty("NUMERO.FAVORITO.FN.SERIAL.NO");
                Parametros.numeroFavoritoFnNumber = prop.getProperty("NUMERO.FAVORITO.FN.NUMBER");
                Parametros.numeroFavoritoFnType = prop.getProperty("NUMERO.FAVORITO.FN.TYPE");
                Parametros.numeroFavoritoFnGroup = prop.getProperty("NUMERO.FAVORITO.FN.GROUP");

                // LOTHAR

                Parametros.lotharProtocolo = prop.getProperty("LOTHAR.PROTOCOLO");
                Parametros.lotharIp = prop.getProperty("LOTHAR.IP");
                Parametros.lotharPuerto = prop.getProperty("LOTHAR.PUERTO");
                Parametros.lotharContexto = prop.getProperty("LOTHAR.CONTEXTO");

                Parametros.lotharCallingCirclePageConsultar = prop.getProperty("LOTHAR.CALLING.CIRCLE.PAGE.CONSULTAR");
                Parametros.lotharCallingCirclePageCrearGrupo = prop
                        .getProperty("LOTHAR.CALLING.CIRCLE.PAGE.CREAR.GRUPO");
                Parametros.lotharCallingCirclePageBorrarGrupo = prop
                        .getProperty("LOTHAR.CALLING.CIRCLE.PAGE.BORRAR.GRUPO");
                Parametros.lotharCallingCirclePageAgregarMiembro = prop
                        .getProperty("LOTHAR.CALLING.CIRCLE.PAGE.AGREGAR.MIEMBRO");
                Parametros.lotharCallingCirclePageCambiarTarifaMiembro = prop
                        .getProperty("LOTHAR.CALLING.CIRCLE.PAGE.CAMBIAR.TARIFA.DE.MIEMBRO");
                Parametros.lotharCallingCirclePageBorrarMiembro = prop
                        .getProperty("LOTHAR.CALLING.CIRCLE.PAGE.BORRAR.MIEMBRO");

                Parametros.lotharCugPageConsultar = prop.getProperty("LOTHAR.CUG.PAGE.CONSULTAR");
                Parametros.lotharCugPageCrearGrupo = prop.getProperty("LOTHAR.CUG.PAGE.CREAR.GRUPO");
                Parametros.lotharCugPageBorrarGrupo = prop.getProperty("LOTHAR.CUG.PAGE.BORRAR.GRUPO");
                Parametros.lotharCugPageAgregarMiembro = prop.getProperty("LOTHAR.CUG.PAGE.AGREGAR.MIEMBRO");
                Parametros.lotharCugPageCambiarTipoMiembro = prop
                        .getProperty("LOTHAR.CUG.PAGE.CAMBIAR.TIPO.DE.MIEMBRO");
                Parametros.lotharCugPageBorrarMiembro = prop.getProperty("LOTHAR.CUG.PAGE.BORRAR.MIEMBRO");

                Parametros.customizedWsdl = prop.getProperty("CUSTOMIZED.WSDL");
                Parametros.customizedTimeOut = Integer.parseInt(prop.getProperty("CUSTOMIZED.TIMEOUT"));
                Parametros.customizedPathKeystore = prop.getProperty("CUSTOMIZED.PATH.KEYSTORE");
                Parametros.customizedVersion = prop.getProperty("CUSTOMIZED.VERSION");
                Parametros.customizedLoginSystemCode = prop.getProperty("CUSTOMIZED.LOGIN.SYSTEM.CODE");
                Parametros.customizedPassword = prop.getProperty("CUSTOMIZED.PASSWORD");
                Parametros.customizedOperatorId = prop.getProperty("CUSTOMIZED.OPERATOR.ID");
                Parametros.customizedTimeType = prop.getProperty("CUSTOMIZED.TIME.TYPE");
                Parametros.customizedOwnerShipInfoId = prop.getProperty("CUSTOMIZED.OWNER.SHIP.INFO.ID");

                Parametros.incidenteVistas = prop.getProperty("INCIDENTE.VISTAS");
                Parametros.dominioAccessOrigin = prop.getProperty("DOMINIO.ACCESS.ORIGIN");

                Parametros.cnxTelnetHlrComandUno = prop.getProperty("CNX.TELNET.HLR.COMAND.UNO");
                Parametros.cnxTelnetHlrComandDos = prop.getProperty("CNX.TELNET.HLR.COMAND.DOS");
                // Parametros.tramaContainHlr =
                // prop.getProperty("TRAMA.CONTAIN.HLR");

                Parametros.cnxTelnetGGSNComandCero = prop.getProperty("CNX.TELNET.GGSN.COMAND.CERO");
                Parametros.cnxTelnetGGSNComandUno = prop.getProperty("CNX.TELNET.GGSN.COMAND.UNO");
                Parametros.tramaContainGGSN = prop.getProperty("TRAMA.CONTAIN.GGSN");

                Parametros.cnxTelnetMscComandUno = prop.getProperty("CNX.TELNET.MSC.COMAND.UNO");
                Parametros.cnxTelnetMscComandDos = prop.getProperty("CNX.TELNET.MSC.COMAND.DOS");
                Parametros.cnxTelnetMscComandTres = prop.getProperty("CNX.TELNET.MSC.COMAND.TRES");

                Parametros.cnxSshMscComandUno = prop.getProperty("CNX.SSH.MSC.COMAND.UNO");
                Parametros.cnxSshMscComandCuatro = prop.getProperty("CNX.SSH.MSC.COMAND.CUATRO");
                Parametros.cnxSshMscComandCinco = prop.getProperty("CNX.SSH.MSC.COMAND.CINCO");

                // Parametros.tramaContainMsc =
                // prop.getProperty("TRAMA.CONTAIN.MSC");

                Parametros.esperaRespuestaElementosRed = prop.getProperty("ESPERA.RESPUESTA.ELEMENTOS.RED");

                Parametros.esperaRespuestaElementosRedGGSN = prop.getProperty("ESPERA.RESPUESTA.ELEMENTOS.RED.GGSN");
                Parametros.cnxTelnetGGSNComandEnter = prop.getProperty("CNX.TELNET.GGSN.COMAND.ENTER");

                Parametros.esperaRespuestaElementosRedSsh = prop.getProperty("ESPERA.RESPUESTA.ELEMENTOS.RED.SSH");
                Parametros.esperaRespuestaElementosRedGGSNSsh = prop
                        .getProperty("ESPERA.RESPUESTA.ELEMENTOS.RED.GGSN.SSH");

                // Validaciones de elementos Red
                // Parametros.containValidacionExcepcionHlrUno =
                // prop.getProperty("CONTAIN.VALIDACION.EXCEPCION.HLR.UNO");
                // Parametros.containValidacionExcepcionHlrDos =
                // prop.getProperty("CONTAIN.VALIDACION.EXCEPCION.HLR.DOS");
                // Parametros.containValidacionExcepcionHlrTres =
                // prop.getProperty("CONTAIN.VALIDACION.EXCEPCION.HLR.TRES");
                //
                // Parametros.containValidacionExcepcionGgsnUno =
                // prop.getProperty("CONTAIN.VALIDACION.EXCEPCION.GGSN.UNO");
                // Parametros.containValidacionExcepcionGgsnDos =
                // prop.getProperty("CONTAIN.VALIDACION.EXCEPCION.GGSN.DOS");
                // Parametros.containValidacionExcepcionGgsnTres =
                // prop.getProperty("CONTAIN.VALIDACION.EXCEPCION.GGSN.TRES");
                //
                // Parametros.containValidacionExcepcionMscUno =
                // prop.getProperty("CONTAIN.VALIDACION.EXCEPCION.MSC.UNO");
                // Parametros.containValidacionExcepcionMscDos =
                // prop.getProperty("CONTAIN.VALIDACION.EXCEPCION.MSC.DOS");
                // Parametros.containValidacionExcepcionMscTres =
                // prop.getProperty("CONTAIN.VALIDACION.EXCEPCION.MSC.TRES");

                Parametros.nroReintentosConexionTelnet = prop.getProperty("NRO.REINTENTOS.CONEXION.TELNET");

                Parametros.dsTusMegasPorAplicacion = prop.getProperty("DATASOURCE.NAME.JNI.TUS.MEGAS.POR.APLICACION");
                Parametros.urlServletParametroNodos = prop.getProperty("URL.SERVLET.PARAMETRO.NODOS");

                Parametros.jndi = prop.getProperty("JNDI");
                Parametros.urlProvider = prop.getProperty("URL.PROVIDER");

                Parametros.enabledColumnCurrentAmount = Boolean
                        .valueOf(prop.getProperty("HABILITAR.COLUMNA.CURRENT.AMOUNT"));
                // DEUDA TTP
                Parametros.enabledColumnCurrentAmount = Boolean
                        .valueOf(prop.getProperty("HABILITAR.COLUMNA.CURRENT.AMOUNT"));
                Parametros.mensajeLineaSinDeuda = prop.getProperty("MENSAJE.LINEA.SIN.DEUDA");
                Parametros.mensajeLineaConCNS = prop.getProperty("MENSAJE.LINEA.CON.CNS");
                Parametros.mensajeLineaDeudaNoCorresponde = prop.getProperty("MENSAJE.LINEA.CON.DEUDA.NO.CORRESPONDE");
                Parametros.mensajeLineaConMontoNoValido = prop.getProperty("MENSAJE.LINEA.CON.MONTO.NO.VALIDO");
                Parametros.mensajeLineaNoPortada = prop.getProperty("MENSAJE.LINEA.ERROR.NO.PORTADO");
                Parametros.mensajeLineaErrorEstadoNoValido = prop.getProperty("MENSAJE.LINEA.ERROR.ESTADO.NO.VALIDO");
                Parametros.mensajeLineaNoEncontrada = prop.getProperty("MENSAJE.LINEA.NO.ENCONTRADA");
                Parametros.estadosValidosParaCrearCns = prop.getProperty("ESTADOS.VALIDOS.PARA.CREAR.CNS");
                Parametros.vistaDeudaTtp = prop.getProperty("DEUDA.TTP.VISTA");
                Parametros.comandoDeudaTtp = Integer.parseInt(prop.getProperty("DEUDA.TTP.COMANDO").trim());
                Parametros.estadosColores = prop.getProperty("DEUDA.TTP.ESTADOS.COLOR");

                Parametros.estadosInternoPantalla = prop.getProperty("ESTADOS.INTERNO.PANTALLA");
                Parametros.estadosInternoCorte = prop.getProperty("ESTADOS.INTERNO.CORTE");

                Parametros.gestorDeudasWsdl = prop.getProperty("DEUDA.TTP.WSDL");
                Parametros.gestorDeudasTimeOut = Integer.parseInt(prop.getProperty("DEUDA.TTP.TIMEOUT"));
                Parametros.gestorDeudasPathKeystore = prop.getProperty("DEUDA.TTP.PATH.KEYSTORE");

                // Parametros.propietarioLinea =
                // prop.getProperty("DEUDA.TTP.PROPIETARIO.LINEA");
                // Parametros.patronFechaDeuda =
                // prop.getProperty("DEUDA.TTP.PATRON.FECHA.DEUDA");
                Parametros.patronFechaDetalleDeuda = prop.getProperty("DEUDA.TTP.PATRON.FECHA.DETALLE.DEUDA");

                Parametros.facturarDeudaWsdl = prop.getProperty("FACTURAR.DEUDA.WSDL");
                Parametros.facturarDeudaTimeOut = Integer.parseInt(prop.getProperty("FACTURAR.DEUDA.TIMEOUT"));
                Parametros.facturarDeudaPathKeystore = prop.getProperty("FACTURAR.DEUDA.PATH.KEYSTORE");
                Parametros.reporteTitulo = prop.getProperty("DEUDA.TTP.REPORTE.TITULO");

                Parametros.deudaTtpLocaleLenguage = prop.getProperty("DEUDA.TTP.LOCALE.LANGUAGE");
                Parametros.deudaTtpLocaleCountry = prop.getProperty("DEUDA.TTP.LOCALE.COUNTRY");
                Parametros.deudaTtpNroDecimales = Integer.parseInt(prop.getProperty("DEUDA.TTP.NRO.DECIMALES"));
                Parametros.deudaTtpMensajeConfirmacionCrearCns = prop
                        .getProperty("DEUDA.TTP.MENSAJE.CONFIRMACION.CREAR.CNS");
                Parametros.estadoValidoDeudaCero = prop.getProperty("ESTADO.VALIDO.DEUDA.CERO");
                Parametros.opcionConsumoServicioFacturarDeuda = Integer
                        .parseInt(prop.getProperty("OPCION.FACTURAR.DEUDA.SERVICE.OSB"));
                Parametros.opcionConsumoServicioDeudaDetallada = Integer
                        .parseInt(prop.getProperty("OPCION.DEUDA.DETALLADA.SERVICE.OSB"));
                userServiceFaturarTTP = prop.getProperty("USER.SERVICE.FACTURAR.TTP");
                passServiceFacturarTTP = prop.getProperty("PASSWORD.SERVICE.FACTURAR.TTP");
                userServiceDDTTP = prop.getProperty("USER.SERVICE.DEUDA_DETALLE.TTP");
                passServiceDDTTP = prop.getProperty("PASSWORD.SERVICE.DEUDA_DETALLE.TTP");


                Parametros.restUrlToken = prop.getProperty("REST.URL.TOKEN");
                Parametros.restUrlParametrosToken = prop.getProperty("REST.URL.PARAMETROS.TOKEN");
                Parametros.restparametrosToken = prop.getProperty("REST.PARAMETROS.TOKEN");
                Parametros.restTimeoutToken = Integer.valueOf(prop.getProperty("REST.TIMEOUT.TOKEN"));
                Parametros.restMensajeErrorToken = prop.getProperty("REST.MENSAJE.TOKEN");

                Parametros.restUrlQueryUSer = prop.getProperty("REST.URL.QUERYUSER");
                Parametros.restUrlParametrosQueryUSer = prop.getProperty("REST.URL.PARAMETROS.QUERYUSER");
                Parametros.restTimeoutQueryUSer = Integer.valueOf(prop.getProperty("REST.TIMEOUT.QUERYUSER"));
                Parametros.restMensajeEmptyQueryUSer = prop.getProperty("REST.MENSSAJE.EMPTY.QUERYUSER");
                Parametros.restMensajeErrorQueryUSer = prop.getProperty("REST.MENSAJE.ERROR.QUERYUSER");

                Parametros.restUrlResetPin = prop.getProperty("REST.URL.RESETPIN");
                Parametros.restUrlParametrosResetPin = prop.getProperty("REST.URL.PARAMETROS.RESETPIN");
                Parametros.restBodyResetPin = prop.getProperty("REST.BODY.RESETPIN");
                Parametros.restTimeoutResetPin = Integer.valueOf(prop.getProperty("REST.TIMEOUT.RESETPIN"));
                Parametros.restMensajeErrorResetPin = prop.getProperty("REST.MENSAJE.ERROR.RESETPIN");
                Parametros.restMensajeExitosoResetPin = prop.getProperty("REST.MENSAJE.EXITOSO.RESETPIN");
                Parametros.mostrarPin = Boolean.parseBoolean(prop.getProperty("REST.MOSTRAR.NUEVO.PIN.RESETPIN"));

                Parametros.restUrlSMS = prop.getProperty("REST.URL.SMS");
                Parametros.restUrlBodySMS = prop.getProperty("REST.BODY.SMS");
                Parametros.restTimeoutSMS = Integer.valueOf(prop.getProperty("REST.TIMEOUT.SMS"));
                Parametros.restMensajeSMS = prop.getProperty("REST.MENSAJE.SMS");


                Parametros.restUrlEnableUser = prop.getProperty("REST.URL.ENABLEUSER");
                Parametros.restUrlParametrosEnableUser = prop.getProperty("REST.URL.PARAMETROS.ENABLEUSER");
                Parametros.restUrlBodyEnableUser = prop.getProperty("REST.BODY.ENABLEUSER");
                Parametros.restTimeoutEnableUser = Integer.valueOf(prop.getProperty("REST.TIMEOUT.ENABLEUSER"));
                Parametros.restMensajeEnableUser = prop.getProperty("REST.MENSAJE.ENABLEUSER");

                Parametros.pinTipo = prop.getProperty("PIN.TIPO");
                Parametros.pinLonguitud = Integer.valueOf(prop.getProperty("PIN.LONGUITUD"));
                Parametros.pinCola = Integer.valueOf(prop.getProperty("PIN.COLA"));


                Parametros.restUrlQueryGroup = prop.getProperty("REST.URL.QUERYGROUP");
                Parametros.restUrlParametrosQueryGroup = prop.getProperty("REST.URL.PARAMETROS.QUERYGROUP");
                Parametros.restTimeoutQueryGroup = Integer.valueOf(prop.getProperty("REST.TIMEOUT.QUERYGROUP"));
                Parametros.restMensajeErrorQueryGroup = prop.getProperty("REST.MENSAJE.ERROR.QUERYGROUP");
                Parametros.restListaExcepcionesQueryGroup = prop.getProperty("REST.LISTA.EXCEPCIONES.QUERYGROUP");
                Parametros.restListaAceptadosQueryGroup = prop.getProperty("REST.LISTA.ACEPTADOS.QUERYGROUP");

                Parametros.restUrlQueryUserGroup = prop.getProperty("REST.URL.QUERYUSERGROUP");
                Parametros.restUrlParametrosQueryUserGroup = prop.getProperty("REST.URL.PARAMETROS.QUERYUSERGROUP");
                Parametros.restTimeoutQueryUserGroup = Integer.valueOf(prop.getProperty("REST.TIMEOUT.QUERYUSERGROUP"));
                Parametros.restMensajeErrorQueryUserGroup = prop.getProperty("REST.MENSAJE.ERROR.QUERYUSERGROUP");
                Parametros.restListaExcepcionesQueryUserGroup = prop.getProperty("REST.LISTA.EXCEPCIONES.QUERYUSERGROUP");
                Parametros.restListaAceptadosQueryUserGroup = prop.getProperty("REST.LISTA.ACEPTADOS.QUERYUSERGROUP");


                Parametros.restUrlCreateUser = prop.getProperty("REST.URL.CREATEUSER");
                Parametros.restUrlParametrosCreateUser = prop.getProperty("REST.URL.PARAMETROS.CREATEUSER");
                Parametros.restUrlBodyCreateUser = prop.getProperty("REST.BODY.CREATEUSER");
                Parametros.restTimeoutCreateUser = Integer.valueOf(prop.getProperty("REST.TIMEOUT.CREATEUSER"));
                Parametros.restMensajeCreateUser = prop.getProperty("REST.MENSAJE.CREATEUSER");
                Parametros.restMensajeErrorCreateUser = prop.getProperty("REST.MENSAJE.ERROR.CREATEUSER");
                Parametros.restListaExcepcionesCreateUser = prop.getProperty("REST.LISTA.EXCEPCIONES.CREATEUSER");
                Parametros.restListaAceptadosCreateUser = prop.getProperty("REST.LISTA.ACEPTADOS.CREATEUSER");


                Parametros.restUrlUpdateUser = prop.getProperty("REST.URL.UPDATEUSER");
                Parametros.restUrlParametrosUpdateUser = prop.getProperty("REST.URL.PARAMETROS.UPDATEUSER");
                Parametros.restUrlBodyUpdateUser = prop.getProperty("REST.BODY.UPDATEUSER");
                Parametros.restTimeoutUpdateUser = Integer.valueOf(prop.getProperty("REST.TIMEOUT.UPDATEUSER"));
                Parametros.restMensajeUpdateUser = prop.getProperty("REST.MENSAJE.UPDATEUSER");
                Parametros.restMensajeErrorUpdateUser = prop.getProperty("REST.MENSAJE.ERROR.UPDATEUSER");
                Parametros.restListaExcepcionesUpdateUser = prop.getProperty("REST.LISTA.EXCEPCIONES.UPDATEUSER");
                Parametros.restListaAceptadosUpdateUser = prop.getProperty("REST.LISTA.ACEPTADOS.UPDATEUSER");


                Parametros.restUrlAddGroupUser = prop.getProperty("REST.URL.ADDGROUPUSER");
                Parametros.restUrlParametrosAddGroupUser = prop.getProperty("REST.URL.PARAMETROS.ADDGROUPUSER");
                Parametros.restTimeoutAddGroupUser = Integer.valueOf(prop.getProperty("REST.TIMEOUT.ADDGROUPUSER"));
                Parametros.restMensajeAddGroupUser = prop.getProperty("REST.MENSAJE.ADDGROUPUSER");
                Parametros.restMensajeErrorAddGroupUser = prop.getProperty("REST.MENSAJE.ERROR.ADDGROUPUSER");
                Parametros.restListaExcepcionesAddGroupUser = prop.getProperty("REST.LISTA.EXCEPCIONES.ADDGROUPUSER");
                Parametros.restListaAceptadosAddGroupUser = prop.getProperty("REST.LISTA.ACEPTADOS.ADDGROUPUSER");


                Parametros.restUrlDeleteGroupUser = prop.getProperty("REST.URL.DELETEGROUPUSER");
                Parametros.restUrlParametrosDeleteGroupUser = prop.getProperty("REST.URL.PARAMETROS.DELETEGROUPUSER");
                Parametros.restTimeoutDeleteGroupUser = Integer.valueOf(prop.getProperty("REST.TIMEOUT.DELETEGROUPUSER"));
                Parametros.restMensajeDeleteGroupUser = prop.getProperty("REST.MENSAJE.DELETEGROUPUSER");
                Parametros.restMensajeErrorDeleteGroupUser = prop.getProperty("REST.MENSAJE.ERROR.DELETEGROUPUSER");
                Parametros.restListaExcepcionesDeleteGroupUser = prop.getProperty("REST.LISTA.EXCEPCIONES.DELETEGROUPUSER");
                Parametros.restListaAceptadosDeleteGroupUser = prop.getProperty("REST.LISTA.ACEPTADOS.DELETEGROUPUSER");


                Parametros.restUrlDeleteUser = prop.getProperty("REST.URL.REMOVEUSER");
                Parametros.restUrlParametrosDeleteUser = prop.getProperty("REST.URL.PARAMETROS.REMOVEUSER");
                Parametros.restTimeoutDeleteUser = Integer.valueOf(prop.getProperty("REST.TIMEOUT.REMOVEUSER"));
                Parametros.restMensajeDeleteUser = prop.getProperty("REST.MENSAJE.REMOVEUSER");
                Parametros.restMensajeErrorDeleteUser = prop.getProperty("REST.MENSAJE.ERROR.REMOVEUSER");
                Parametros.restListaExcepcionesDeleteUser = prop.getProperty("REST.LISTA.EXCEPCIONES.REMOVEUSER");
                Parametros.restListaAceptadosDeleteUser = prop.getProperty("REST.LISTA.ACEPTADOS.REMOVEUSER");


                Parametros.ehCategoria = prop.getProperty("EH.CATEGORIA");
                Parametros.ehPermisos = prop.getProperty("EH.PERMISOS");
                Parametros.ehPrefijoTelefono = prop.getProperty("EH.PREFIJO.TELFONO");
                Parametros.ehSummaryMensajes = prop.getProperty("EH.SUMMARY.MENSAJES");
                Parametros.ehIdentificadorSucursales = prop.getProperty("EH.IDENTIFICADOR.SUCURSALES");


                Parametros.ehExpresionCodigo = prop.getProperty("EH.EXPRESION.CODIGO");
                Parametros.ehExpresionCodigoBuscador = prop.getProperty("EH.EXPRESION.CODIGO.BUSCADOR");
                Parametros.ehExpresionCodigoMensajeError = prop.getProperty("EH.EXPRESION.CODIGO.MENSAJE.ERROR");
                Parametros.ehExpresionCodigoMensajeRequerido = prop.getProperty("EH.EXPRESION.CODIGO.MENSAJE.REQUERIDO");

                Parametros.ehExpresionNombres = prop.getProperty("EH.EXPRESION.NOMBRES");
                Parametros.ehExpresionNombresMensajeError = prop.getProperty("EH.EXPRESION.NOMBRES.MENSAJE.ERROR");
                Parametros.ehExpresionNombresMensajeRequerido = prop.getProperty("EH.EXPRESION.NOMBRES.MENSAJE.REQUERIDO");

                Parametros.ehExpresionApellidos = prop.getProperty("EH.EXPRESION.APELLIDOS");
                Parametros.ehExpresionApellidosMensajeError = prop.getProperty("EH.EXPRESION.APELLIDOS.MENSAJE.ERROR");
                Parametros.ehExpresionApellidosMensajeRequerido = prop.getProperty("EH.EXPRESION.APELLIDOS.MENSAJE.REQUERIDO");

                Parametros.ehExpresionEmail = prop.getProperty("EH.EXPRESION.EMAIL");
                Parametros.ehExpresionEmailMensajeError = prop.getProperty("EH.EXPRESION.EMAIL.MENSAJE.ERROR");

                Parametros.ehExpresionTelefono = prop.getProperty("EH.EXPRESION.TELEFONO");
                Parametros.ehExpresionTelefonoMensajeError = prop.getProperty("EH.EXPRESION.TELEFONO.MENSAJE.ERROR");
                Parametros.ehExpresionTelefonoMensajeRequerido = prop.getProperty("EH.EXPRESION.TELEFONO.MENSAJE.REQUERIDO");


                Parametros.ehExpresionLimite = prop.getProperty("EH.EXPRESION.LIMITE");
                Parametros.getEhExpresionLimiteMensajeError = prop.getProperty("EH.EXPRESION.LIMITE.MENSAJE.ERROR");
                Parametros.getEhExpresionLimiteMensajeRequerido = prop.getProperty("EH.EXPRESION.LIMITE.MENSAJE.REQUERIDO");


                log.info("fin cargando parametros......");

                //GESTION SUPERVISORES Y VENDEDORES
                Parametros.gestionSupVenUsuarioRequerido = prop.getProperty("GESTION.MENSAJE.WARN.USUARIO.REQUERIDO");
                Parametros.gestionSupVentipoUsuario = prop.getProperty("GESTION.MENSAJE.WARN.TIPO.USUARIO");
                Parametros.gestionSupVenUsuarioNoEncontrado = prop.getProperty("GESTION.MENSAJE.WARN.USUARIO.NO.ENCONTRADO");
                Parametros.gestionSupVenSeleccionZona = prop.getProperty("GESTION.MENSAJE.WARN.SELECCIONAR.ZONA");
                Parametros.gestionSupVenGuardarCambios = prop.getProperty("GESTION.MENSAJE.INFO.CAMBIOS.GUARDADOS");
                Parametros.gestionSupVenEliminarDealer = prop.getProperty("GESTION.MENSAJE.INFO.ELIMINAR.DEALER");
                Parametros.gestionSupVenNameAccion = prop.getProperty("GESTION.MENSAJE.NAME.ACCION");
                Parametros.gestionSupVenEliminarZonas = prop.getProperty("GESTION.MENSAJE.INFO.ELIMINAR.ZONAS");


            }
            // else {
            // log.error("No se encontro el archivo para cargar las
            // propiedades");
            // }

        } catch (IOException e) {
            log.error("Error al cargar archivo de propiedades: " + configFile, e);
        } catch (Exception e) {
            log.error("Error al cargar archivo de propiedades: " + configFile, e);
        } finally {
            try {
                if (is != null)
                    is.close();
            } catch (IOException e) {
                log.error("Error al cerrar archivo: ", e);
            }
        }
    }
	/*public static void main (String [ ] args) {
		Parametros p = new Parametros();
		p.init();
	}*/
}
