package com.myapps.user.dao;

import com.myapps.user.model.MuFormulario;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.util.List;

@Named
public class FormDAO implements Serializable {

	private static final long serialVersionUID = 1L;

	@PersistenceContext(unitName = "vista_ccc")
	private transient EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public List<MuFormulario> getList() {
		return entityManager.createQuery("SELECT us FROM MuFormulario us").getResultList();
	}
}
