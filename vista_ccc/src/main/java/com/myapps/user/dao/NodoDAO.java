package com.myapps.user.dao;

import com.myapps.user.model.VcNodo;

import javax.annotation.Resource;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import java.io.Serializable;
import java.util.List;

@Named
public class NodoDAO implements Serializable {

	private static final long serialVersionUID = 1L;

	@PersistenceContext(unitName = "vista_ccc")
	private transient EntityManager entityManager;

	@Resource
	private transient UserTransaction transaction;

	public void save(VcNodo dato) throws Exception {
		transaction.begin();
		entityManager.persist(dato);
		transaction.commit();
	}

	public VcNodo get(long id) throws Exception {
		return entityManager.find(VcNodo.class, id);
	}

	public void update(VcNodo dato) throws Exception {
		transaction.begin();
		entityManager.joinTransaction();
		try {
			entityManager.merge(dato);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
			throw e;
		}
	}

	public void remove(VcNodo dato) throws Exception {
		transaction.begin();
		entityManager.joinTransaction();
		try {
			entityManager.remove(entityManager.contains(dato) ? dato : entityManager.merge(dato));
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<VcNodo> getList() throws Exception {
		return entityManager.createQuery("SELECT us FROM VcNodo us").getResultList();
	}

	@SuppressWarnings("unchecked")
	public VcNodo getNodoLogin(String login) throws Exception {

		String consulta = "SELECT us FROM VcNodo us WHERE us.usuario = :login";
		Query qu = entityManager.createQuery(consulta).setParameter("login", login);
		List<VcNodo> lista = qu.getResultList();
		return lista.isEmpty() ? null : lista.get(0);

	}

}
