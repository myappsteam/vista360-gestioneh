package com.myapps.user.dao;

import com.myapps.user.model.MuBitacora;

import javax.annotation.Resource;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import java.io.Serializable;
import java.util.List;

@Named
public class BitacoraDAO implements Serializable {

	private static final long serialVersionUID = 1L;

	@PersistenceContext(unitName = "vista_ccc")
	private transient EntityManager entityManager;

	@Resource
	private transient UserTransaction transaction;

	public void save(MuBitacora dato) throws Exception {
		transaction.begin();
		entityManager.persist(dato);
		transaction.commit();
	}

	@SuppressWarnings("unchecked")
	public List<MuBitacora> listBitacora() {
		String sql = "FROM MuBitacora b ORDER BY b.fecha DESC";
		Query query = entityManager.createQuery(sql);
		query.setMaxResults(1000);
		return query.getResultList();
	}

}
