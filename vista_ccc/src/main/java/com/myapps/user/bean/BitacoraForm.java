package com.myapps.user.bean;

import com.myapps.user.business.BitacoraBL;
import com.myapps.user.model.MuBitacora;
import com.myapps.vista_ccc.servicio.Servicio;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;

@ManagedBean
@ViewScoped
public class BitacoraForm implements Serializable {

	private static final long serialVersionUID = 1L;

	public static Logger log = Logger.getLogger(BitacoraForm.class);

	@Inject
	private BitacoraBL logBl;

	private List<MuBitacora> listBitacora;

	@PostConstruct
	public void init() {
		try {
			listBitacora = logBl.listBitacora();
			
			Servicio.setPagina(this.getClass().getSimpleName());
			Servicio.eliminarConexionesInactivas(this.getClass().getSimpleName());
			
		} catch (Exception e) {
			log.error("[init] Fallo al iniciar el bean.", e);
		}
	}

	public List<MuBitacora> getListBitacora() {
		return listBitacora;
	}

	public void setListBitacora(List<MuBitacora> listBitacora) {
		this.listBitacora = listBitacora;
	}

}
