package com.myapps.user.bean;

import com.myapps.user.business.RoleBL;
import com.myapps.user.business.UsuarioBL;
import com.myapps.user.ldap.ActiveDirectory;
import com.myapps.user.ldap.DescriptorBitacora;
import com.myapps.user.model.MuRol;
import com.myapps.user.model.MuUsuario;
import com.myapps.vista_ccc.servicio.Servicio;
import com.myapps.vista_ccc.util.SysMessage;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ViewScoped
public class RoleUserForm implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private UsuarioBL userBL;

	@Inject
	private RoleBL rolBL;

	@Inject
	private ControlerBitacora controlerBitacora;

	private List<MuUsuario> listUser;

	private MuUsuario user = new MuUsuario();
	private String userId;
	private boolean edit;

	private List<SelectItem> selectItems;
	private String select;

	private static Logger log = Logger.getLogger(RoleUserForm.class);

	@PostConstruct
	public void init() {
		try {
			
			Servicio.setPagina(this.getClass().getSimpleName());
			Servicio.eliminarConexionesInactivas(this.getClass().getSimpleName());
			
			user = new MuUsuario();
			listUser = userBL.getUsers();
			fillSelectItems();
		} catch (Exception e) {
			log.error("init|Fallo al inicializar la clase. " + e.getMessage());
		}
	}

	private void fillSelectItems() {

		selectItems = new ArrayList<SelectItem>();
		selectItems.add(new SelectItem("-1", "Grupos_Rol"));
		List<MuRol> listaRol = rolBL.getRoles();
		for (MuRol role : listaRol) {
			SelectItem sel = new SelectItem(role.getRolId(), role.getNombre());
			selectItems.add(sel);
		}
	}

	public void saveUser() {
		int idRole = Integer.parseInt(select);
		if (idRole == -1) {
			SysMessage.warn("Campo rol es requerido", null);
			return;
		}

		String str = "";
		try {
			str = userBL.validate(user, userId);
		} catch (Exception e) {
			log.error("[saveUser] Fallo al intentar validar los parametros.", e);
			str = "error con la conexion a la BD o otro problema";
		}

		if (!str.isEmpty()) {
			SysMessage.warn(str, null);
			return;
		}
		
		String name = ActiveDirectory.getNombreCompleto(user.getLogin());
		if (name.trim().isEmpty()) {
			SysMessage.warn("No se pudo obtener el nombre del usuario", null);
			return;
		}

		if (!name.isEmpty())
			user.setNombre(name);

		try {
			user.setLogin(user.getLogin().toLowerCase());
			if (!edit) {
				userBL.saveUserRole(user, idRole);
				controlerBitacora.insert(DescriptorBitacora.USUARIO, user.getUsuarioId() + "", user.getLogin());
			} else {
				int id = Integer.parseInt(userId);

				user.setUsuarioId(id);
				userBL.updateUser(user, idRole);
				controlerBitacora.update(DescriptorBitacora.USUARIO, user.getUsuarioId() + "", user.getLogin());
			}
			listUser = userBL.getUsers();
			SysMessage.info("Se guardo correctamente.", null);
			newUser();
		} catch (Exception e) {
			log.error("[saveUser] usuario: " + user.getNombre() + ", Fallo al guardar el usuario", e);
			SysMessage.error("Fallo al guardar el usuario", null);
		}
	}

	public void editRoleUser() {
		String Idstr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("user_id");
		int id = Integer.parseInt(Idstr);
		userId = Idstr;
		edit = true;
		try {
			user = userBL.getUser(id);
			select = user.getMuRol().getRolId() + "";
		} catch (Exception e) {
			user = new MuUsuario();
			log.error("[editRoleUser] error al obtener el usuario:" + e);
		}
	}

	public String deleteRoleUser() {
		String Idstr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("user_id");
		int id = Integer.parseInt(Idstr);

		if (id == 1) {
			SysMessage.error("Este Usuario no se puede elimnar es usuario Interno.", null);
			return "";
		}

		try {
			user = userBL.getUser(id);
			userBL.deleteUser(id);
			controlerBitacora.delete(DescriptorBitacora.USUARIO, user.getUsuarioId() + "", user.getLogin());
			listUser = userBL.getUsers();
			newUser();
			SysMessage.info("Se elimino correctamente.", null);
		} catch (Exception e) {
			log.error("[deleteRoleUser]  error al eliminar el menu id:" + Idstr + "  " + e);
			SysMessage.error("Fallo al eliminar.", null);
		}
		return "";
	}

	public void newUser() {
		edit = false;
		user = new MuUsuario();
		select = "-1";
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Boolean getEdit() {
		return edit;
	}

	public void setEdit(Boolean edit) {
		this.edit = edit;
	}

	public MuUsuario getUser() {
		return user;
	}

	public void setUser(MuUsuario user) {
		this.user = user;
	}

	public List<SelectItem> getSelectItems() {
		return selectItems;
	}

	public void setSelectItems(List<SelectItem> selectItems) {
		this.selectItems = selectItems;
	}

	public String getSelect() {
		return select;
	}

	public void setSelect(String select) {
		this.select = select;
	}

	public List<MuUsuario> getListUser() {
		return listUser;
	}

	public void setListUser(List<MuUsuario> listUser) {
		this.listUser = listUser;
	}
}
