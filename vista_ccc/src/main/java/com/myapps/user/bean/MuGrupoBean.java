package com.myapps.user.bean;

import com.myapps.user.business.GrupoAdBL;
import com.myapps.user.business.RoleBL;
import com.myapps.user.ldap.ActiveDirectory;
import com.myapps.user.ldap.DescriptorBitacora;
import com.myapps.user.model.MuGrupoAd;
import com.myapps.user.model.MuRol;
import com.myapps.vista_ccc.servicio.Servicio;
import com.myapps.vista_ccc.util.SysMessage;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ViewScoped
public class MuGrupoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private GrupoAdBL groupBL;

	@Inject
	private RoleBL rolBL;

	@Inject
	private ControlerBitacora controlerBitacora;

	private List<MuGrupoAd> listGroup;

	private MuGrupoAd group = new MuGrupoAd();
	private String groupId;
	private boolean edit;

	private List<SelectItem> selectItems;
	private String select;

	public static Logger log = Logger.getLogger(MuGrupoBean.class);

	@PostConstruct
	public void init() {

		try {
			
			Servicio.setPagina(this.getClass().getSimpleName());
			Servicio.eliminarConexionesInactivas(this.getClass().getSimpleName());
			
			group = new MuGrupoAd();
			listGroup = groupBL.getGroups();
			fillSelectItems();
		} catch (Exception e) {
			log.error("init|Fallo al iniciar gestion de grupo",e);
		}

	}

	private void fillSelectItems() {

		selectItems = new ArrayList<SelectItem>();
		selectItems.add(new SelectItem("-1", "Grupos_Rol"));
		List<MuRol> listaRol = rolBL.getRoles();
		for (MuRol role : listaRol) {
			SelectItem sel = new SelectItem(role.getRolId(), role.getNombre());
			selectItems.add(sel);
		}
	}

	public String saveGroup() {
		int idRole = Integer.parseInt(select);
		if (idRole == -1) {
			SysMessage.warn("Seleccione un Rol.", null);
			return "";
		}

		String str = groupBL.validate(group, groupId);
		if (!str.isEmpty()) {
			SysMessage.warn(str, null);
			return "";
		}
		
		str = validarExisteEnAD(group.getNombre());
		if (!str.isEmpty()) {
			SysMessage.warn(str, null);
			return "";
		}

		try {
			if (!edit) {
				groupBL.saveGroupRole(group, idRole);
				controlerBitacora.insert(DescriptorBitacora.GRUPO, group.getGrupoId() + "", group.getNombre());
			} else {
				int id = Integer.parseInt(groupId);
				group.setGrupoId(id);
				groupBL.updateGroup(group, idRole);
				controlerBitacora.update(DescriptorBitacora.GRUPO, group.getGrupoId() + "", group.getNombre());
			}
			listGroup = groupBL.getGroups();
			SysMessage.info("Se guardo correctamente.", null);
			newGroup();
		} catch (Exception e) {
			log.error("[saveGroup] error al momento de modificar o guardar: " + group.getNombre() + " " + e);
			SysMessage.error("Error al guardar en la Base de Datos.", null);

		}
		return "";
	}

	public void editRoleGroup() {
		String Idstr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("groupId");
		int id = Integer.parseInt(Idstr);
		group = groupBL.getGroup(id);
		select = group.getMuRol().getRolId() + "";
		groupId = Idstr;
		edit = true;
	}

	public void deleteRoleGroup() {
		String Idstr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("groupId");
		int id = Integer.parseInt(Idstr);
		try {
			group = groupBL.getGroup(id);
			groupBL.deleteGroup(id);
			controlerBitacora.delete(DescriptorBitacora.GRUPO, group.getGrupoId() + "", group.getNombre());
			listGroup = groupBL.getGroups();
			newGroup();
			SysMessage.info("Se elimino correctamente.", null);
		} catch (Exception e) {
			log.error("[deleteRoleGroup]  error al eliminar el menu id:" + Idstr + "  " + e);
			SysMessage.error("Fallo al eliminar.", null);
		}
	}

	public void newGroup() {
		edit = false;
		group = new MuGrupoAd();
		select = "-1";
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String GroupId) {
		this.groupId = GroupId;
	}

	public Boolean getEdit() {
		return edit;
	}

	public void setEdit(Boolean edit) {
		this.edit = edit;
	}

	public MuGrupoAd getGroup() {
		return group;
	}

	public void setGroup(MuGrupoAd Group) {
		this.group = Group;
	}

	public List<SelectItem> getSelectItems() {
		return selectItems;
	}

	public void setSelectItems(List<SelectItem> selectItems) {
		this.selectItems = selectItems;
	}

	public String getSelect() {
		return select;
	}

	public void setSelect(String select) {
		this.select = select;
	}

	public List<MuGrupoAd> getListGroup() {
		return listGroup;
	}

	public void setListGroup(List<MuGrupoAd> listGroup) {
		this.listGroup = listGroup;
	}

	private String validarExisteEnAD(String nameNewGrupo) {
		try {
			if (!ActiveDirectory.validarGrupo(nameNewGrupo)) {
				return "No existe un grupo  '" + nameNewGrupo + "' en Active Directory";
			}
		} catch (Exception e) {
			log.error("[validarExisteEnAD] Error al intentar verificar si grupo existe en AD  nombre=" + nameNewGrupo);
			return "No se puede comprobar si este grupo existe en Active Directory";
		}
		return "";
	}

}
