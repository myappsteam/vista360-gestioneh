package com.myapps.user.ldap;

import com.myapps.vista_ccc.util.Parametros;
import org.apache.log4j.Logger;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.*;
import javax.naming.ldap.InitialLdapContext;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;


public class ActiveDirectory {

	public static int EXIT_USER = 1;
	public static int NOT_EXIT_USER = 2;
	public static int ERROR = 3;

	private static Logger log = Logger.getLogger(ActiveDirectory.class);

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static int validarUsuario(String usuario, String password) {
		if (usuario == null || usuario.trim().isEmpty() || password == null || password.trim().isEmpty()) {
			return NOT_EXIT_USER;
		}
		Hashtable env = new Hashtable();
		try {
//			System.out.println(Parametros.activeSecurityPrincipal + usuario);
			env.put(Context.INITIAL_CONTEXT_FACTORY, Parametros.activeInitialContext);
			env.put(Context.PROVIDER_URL, Parametros.activeProviderUrl);
			env.put(Context.SECURITY_AUTHENTICATION, Parametros.activeSecurityAuthentication);
			env.put(Context.SECURITY_PRINCIPAL, Parametros.activeSecurityPrincipal + usuario);
			env.put(Context.SECURITY_CREDENTIALS, password);
		} catch (Exception e) {
			log.error("usuario: " + usuario + ", Fallo al establecer conexion con LDAP", e);
			return ERROR;
		}
		try {
			InitialDirContext ctx = new InitialDirContext(env);
			ctx.close();
			return EXIT_USER;
		} catch (Exception e) {
			log.error("usuario: " + usuario + ", Fallo al establecer conexion con LDAP", e);
			return NOT_EXIT_USER;
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
	public static List<String> getListaGrupos(String usuario) {

		List<String> lGrupos = new ArrayList<String>();

		Hashtable env = new Hashtable();

		env.put("java.naming.factory.initial", Parametros.activeInitialContext);
		env.put("java.naming.provider.url", Parametros.activeProviderUrl);
		env.put("java.naming.security.authentication", Parametros.activeSecurityAuthentication);
		env.put("java.naming.security.principal", Parametros.activeSecurityPrincipal + Parametros.activeSecurityUser);
		env.put("java.naming.security.credentials", Parametros.activeSecurityCredentials);

		InitialLdapContext ctx = null;
		try {
			ctx = new InitialLdapContext(env, null);
			String searchBase = "DC=tigo,DC=net,DC=bo";
			SearchControls searchCtls = new SearchControls();
			searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			String returnAtts[] = { "memberOf" };
			String searchFilter = "(&(objectCategory=person)(objectClass=user)(mailNickname=" + usuario + "))";

			searchCtls.setReturningAttributes(returnAtts);

			NamingEnumeration answer = ctx.search(searchBase, searchFilter, searchCtls);
			int totalResults = 0;

			while (answer.hasMoreElements()) {
				SearchResult sr = (SearchResult) answer.next();
				Attributes attrs = sr.getAttributes();
				if (attrs != null) {
					for (NamingEnumeration ne = attrs.getAll(); ne.hasMore();) {
						Attribute attr = (Attribute) ne.next();
						String grupo;
						for (NamingEnumeration e = attr.getAll(); e.hasMore(); totalResults++) {
							grupo = e.next().toString().trim();
							grupo = grupo.substring(3, grupo.indexOf(",")).trim();
							lGrupos.add(grupo);
						}
					}
				}
			}

		} catch (Exception e) {
			log.error("usuario: " + usuario + ", Error al obtener el listado de grupos", e);
		} finally {
			try {
				ctx.close();
			} catch (Exception e2) {
				log.warn("usuario: " + usuario + ", Fallo al cerrar el InitialLdapContext", e2);
			}
		}
		return lGrupos;
	}

	@SuppressWarnings("rawtypes")
	public static String getNombreCompleto(String usuario) {

		Conexion conexion = new Conexion();
		conexion.setDominio(Parametros.activeDominio);  //CORREGIR REVISAR
		conexion.setUrl(Parametros.activeProviderUrl);
		conexion.setUsuario(Parametros.activeSecurityUser);
		conexion.setClave(Parametros.activeSecurityCredentials);

		Ldap ld = new Ldap(conexion);
		try {
			String[] returnAtts = { "cn", "sAMAccountName", "mail", "sn", "givenName", "initials" };
			Map mapa = ld.obtenerDatos(usuario, returnAtts);
			if (mapa.isEmpty()) {
				return "";
			}

			return String.valueOf(mapa.get("givenName") + " " + (String) mapa.get("sn")).trim();

		} catch (Exception e) {
			log.error("usuario: " + usuario + ", Fallo al obtener el nombre completo del usuario", e);
		}
		return "";
	}

	@SuppressWarnings("rawtypes")
	public static boolean validarGrupo(String grupo) {
		InitialDirContext dirC = null;
		NamingEnumeration answer = null;
		NamingEnumeration ae = null;

		try {
			Hashtable<String, String> env = new Hashtable<String, String>();
			env.put("java.naming.factory.initial", Parametros.activeInitialContext);
			env.put("java.naming.provider.url", Parametros.activeProviderUrl);
			env.put("java.naming.security.authentication", Parametros.activeSecurityAuthentication);
			env.put("java.naming.security.principal", Parametros.activeSecurityPrincipal + Parametros.activeSecurityUser);
			env.put("java.naming.security.credentials", Parametros.activeSecurityCredentials);

			dirC = new InitialDirContext(env);
			if (dirC != null) {
				String searchBase = "DC=tigo,DC=net,DC=bo";
				SearchControls searchCtls = new SearchControls();
				searchCtls.setSearchScope(2);
//				String searchFilter = "(objectclass=group)";
				String searchFilter = "(&(objectCategory=group)(!(groupType:1.2.840.113556.1.4.803:=2147483648)))";
				String[] returnAtts = { "cn" };
				searchCtls.setReturningAttributes(returnAtts);
				answer = dirC.search(searchBase, searchFilter, searchCtls);

				while (answer.hasMoreElements()) {
					SearchResult sr = (SearchResult) answer.next();
					Attributes attrs = sr.getAttributes();
					if (attrs != null) {
						for (ae = attrs.getAll(); ae.hasMore();) {
							Attribute attr = (Attribute) ae.next();
							if (attr.get().toString().equals(grupo)) {
								return true;
							}
						}

					}

				}

			}

		} catch (Exception e) {
			log.error("grupo: " + grupo + ", Fallo al validar el grupo", e);
		} finally {
			try {
				if (dirC != null)
					dirC.close();
			} catch (Exception e) {
				log.warn("grupo: " + grupo + ", Fallo al cerrar el contexto LDAP", e);
			}
		}
		return false;
	}

}