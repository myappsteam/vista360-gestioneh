package com.myapps.user.business;

import com.myapps.user.dao.NodoDAO;
import com.myapps.user.model.VcNodo;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
public class NodoBL implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private NodoDAO dao;

	public void save(VcNodo nodo) throws Exception {
		dao.save(nodo);
	}

	public VcNodo getUser(long idNodo) throws Exception {
		return dao.get(idNodo);
	}

	public void update(VcNodo nodo) throws Exception {
		dao.update(nodo);
	}

	public void remove(VcNodo nodo) throws Exception {
		dao.remove(nodo);
	}

	public List<VcNodo> getList() throws Exception {
		return dao.getList();
	}

	public VcNodo getNodoLogin(String login) throws Exception {
		return dao.getNodoLogin(login);
	}
}
