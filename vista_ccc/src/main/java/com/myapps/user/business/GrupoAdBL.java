package com.myapps.user.business;

import com.myapps.user.dao.GrupoAdDAO;
import com.myapps.user.model.MuGrupoAd;
import com.myapps.user.model.MuRol;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.UtilUrl;
import org.apache.log4j.Logger;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
@Named
public class GrupoAdBL implements Serializable {
    private static Logger logger = Logger.getLogger(GrupoAdBL.class);
    @Inject
    private GrupoAdDAO dao;

    public String validate(MuGrupoAd user, String idStr) {

        if (user == null || user.getNombre().trim().isEmpty()) {
            return "El campo Nombre esta Vacio";
        }

        if (!UtilUrl.esTextoValido(user.getNombre(), Parametros.expresionRegularTextoNormal)) {
            return "El campo Nombre no es valido: " + Parametros.mensajeValidacionTextoNormal;
        }

        if (user.getDetalle() != null && !user.getDetalle().trim().isEmpty()) {
            if (!UtilUrl.esTextoValido(user.getDetalle(), Parametros.expresionRegularTextoNormal)) {
                return "El campo Descripcion no es valido: " + Parametros.mensajeValidacionTextoNormal;
            }
        }

        MuGrupoAd usAux = dao.getGroupName(user.getNombre());
        if (usAux == null)
            return "";

        if (idStr != null && !idStr.isEmpty()) {
            int id = Integer.parseInt(idStr);
            if (id == usAux.getGrupoId())
                if (user.getNombre().equals(usAux.getNombre()))
                    return "";
        }

        return "este nombre de grupo ya existe";
    }

    public void saveGroupRole(MuGrupoAd group, int idRole) throws Exception {
        MuRol rol = new MuRol();
        rol.setRolId(idRole);
        group.setMuRol(rol);
        group.setEstado(true);
        dao.save(group);
    }

    public void updateGroup(MuGrupoAd group, int idRole) throws Exception {
        MuRol rol = new MuRol();
        rol.setRolId(idRole);

        MuGrupoAd groupAux = dao.get(group.getGrupoId());
        groupAux.setNombre(group.getNombre());
        groupAux.setDetalle(group.getDetalle());
        groupAux.setMuRol(rol);
        dao.update(groupAux);
    }

    public void deleteGroup(int idGroup) throws Exception {
        MuGrupoAd user = dao.get(idGroup);
        user.setEstado(false);
        dao.update(user);
    }

    public List<MuGrupoAd> getGroups() {
        return dao.getList();
    }

    public MuGrupoAd getGroup(int idGroup) {
        return dao.get(idGroup);
    }

    public MuGrupoAd getGroupName(String name) {
        return dao.getGroupName(name);
    }

    public MuGrupoAd obtenerGrupo() {
        MuGrupoAd grupoAd = null;
        try {
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            List<String> grupos = (List<String>) request.getSession().getAttribute("TEMP$GROUP_NAME");
            if (grupos != null) {
                for (String name : grupos) {
                    grupoAd = dao.getGroupName(name);
                    if (grupoAd != null) {
                        break;
                    }
                }

            }
        } catch (Exception e) {
            logger.error("Error al obtener Grupo" + e.getMessage());
        }

        return grupoAd;
    }
}