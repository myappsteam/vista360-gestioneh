package com.myapps.user.business;

import com.myapps.user.dao.GrupoAdDAO;
import com.myapps.user.dao.RoleDAO;
import com.myapps.user.dao.UsuarioDAO;
import com.myapps.user.model.MuGrupoAd;
import com.myapps.user.model.MuRol;
import com.myapps.user.model.MuRolFormulario;
import com.myapps.user.model.MuUsuario;
import com.myapps.vista_ccc.dao.MasterDao;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.UtilUrl;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
public class UsuarioBL implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private MasterDao masterDao;

    @Inject
    private UsuarioDAO dao;

    @Inject
    private RoleDAO rolDao;

    @Inject
    private GrupoAdDAO groupDao;

    public String validate(MuUsuario user, String idStr) throws Exception {

        if (user != null) {

            if (user.getLogin() != null && user.getLogin().trim().isEmpty()) {
                return "El campo Login esta Vacio";
            }

            if (user.getLogin() != null && !UtilUrl.esTextoValido(user.getLogin(), Parametros.expresionRegularTextoNormal)) {
                return "El campo Usuario no es valido: " + Parametros.mensajeValidacionTextoNormal;
            }

            MuUsuario usAux = dao.getUsuarioLogin(user.getLogin());
            if (usAux == null)
                return "";

            if (idStr != null && !idStr.isEmpty()) {
                int id = Integer.parseInt(idStr);
                if (id == usAux.getUsuarioId())
                    if (user.getLogin().equals(usAux.getLogin()))
                        return "";
            }
            return "Este login existe";
        } else
            return "Usuario nulo para validar";
    }

    public void saveUserRole(MuUsuario user, int idRole) throws Exception {
        MuRol rol = new MuRol();
        rol.setRolId(idRole);
        user.setMuRol(rol);
        user.setEstado(true);
        // dao.save(user);
        masterDao.save(user);
    }

    public void updateUser(MuUsuario user, int idRole) throws Exception {

        MuUsuario userAux = dao.get(user.getUsuarioId());
        userAux.setLogin(user.getLogin());
        userAux.setNombre(user.getNombre());
        MuRol rol = new MuRol();
        rol.setRolId(idRole);
        userAux.setMuRol(rol);
        dao.update(userAux);
    }

    public void deleteUser(int idUser) throws Exception {
        MuUsuario user = dao.get(idUser);
        user.setEstado(false);
        dao.update(user);
    }

    public List<MuUsuario> getUsers() throws Exception {
        return dao.getList();
    }

    public MuUsuario getUser(int idUser) throws Exception {
        return dao.get(idUser);
    }

    public List<String> getListFormUser(String login) throws Exception {

        long k = 1;
        MuUsuario user = dao.getUsuarioLogin(login);
        if (user != null)
            k = user.getMuRol().getRolId();

        List<MuRolFormulario> lista = rolDao.getRolFormularioUser(k);
        List<String> listaUrl = new ArrayList<String>();
        for (MuRolFormulario rolFormulario : lista) {
            listaUrl.add(rolFormulario.getMuFormulario().getUrl());
        }
        return listaUrl;
    }

    public List<MuRolFormulario> getRolFormularios(String login) throws Exception {
        long k = 1;
        MuUsuario user = dao.getUsuarioLogin(login);
        if (user != null)
            k = user.getMuRol().getRolId();

        return rolDao.getRolFormulario(k);

    }

    public long getIdRole(String login, List<Object> userGroups) {

        MuUsuario user = dao.getUsuarioLogin(login);

        if (user != null)
            return user.getMuRol().getRolId();

        for (Object object : userGroups) {
            MuGrupoAd gr = groupDao.getGroupName(object.toString());
            if (gr != null)
                return gr.getMuRol().getRolId();
        }

        return -1;
    }

    public List<MuUsuario> getUsuario() {
        return dao.getList2();
    }

    public MuUsuario obtenerUsuario() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String login = (String) request.getSession().getAttribute("TEMP$USER_NAME");
        return dao.getUsuarioLogin(login);
    }

}
