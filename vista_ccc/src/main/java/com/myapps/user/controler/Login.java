package com.myapps.user.controler;

import com.myapps.detalle_navegacion.servicio.ServicioDetalleNavegacion;
import com.myapps.user.dao.FormularioDAO;
import com.myapps.user.dao.NodoDAO;
import com.myapps.user.ldap.ActiveDirectory;
import com.myapps.user.model.MuFormulario;
import com.myapps.user.model.VcNodo;
import com.myapps.vista_ccc.bean.model.Nota;
import com.myapps.vista_ccc.servicio.*;
import com.myapps.vista_ccc.util.Parametros;
import com.myapps.vista_ccc.util.SysMessage;
import com.myapps.vista_ccc.util.UtilDate;
import com.myapps.vista_ccc.util.UtilUrl;
import org.apache.log4j.Logger;
import org.primefaces.model.menu.*;
import org.primefaces.util.Base64;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

@ManagedBean(name = "login")
@SessionScoped
public class Login implements Serializable {

    private static final long serialVersionUID = 1L;
    public static Logger log = Logger.getLogger(Login.class);

    @Inject
    ControlLoginImpl controlLogin;

    @Inject
    private FormularioDAO daoFormulario;

    @Inject
    private NodoDAO nodoDao;

    private List<Nota> listNota;
    private Nota notaSeleccionada;

    private String userId;

    private String password;
    private String idSession;

    private MenuModel model;

    private String ipClient;
    private String ipServer;
    private String user;
    private String date;
    private boolean collapsed;

    private String urlServlet;
    private String urlServletVolver;

    @PostConstruct
    public void init() {
        collapsed = false;
    }

    // public void onToogle() {
    // log.debug("........................... collapsed: " + collapsed);
    // }

    public Login() {
        this.userId = "";
        this.password = "";
        listNota = new ArrayList<>();
        listNota.add(new Nota(1, "Teléfono", "", 8));
        listNota.add(new Nota(2, "Código Tigo Star", "", 20));
        listNota.add(new Nota(3, "Nro Documento", "", 20));
        listNota.add(new Nota(4, "Nombre(s)", "", 150));
        listNota.add(new Nota(5, "Apellido", "", 150));
        getIps();
    }

    public String logout() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpSession sesion = request.getSession();
        sesion.setAttribute("TEMP$ACTION_MESSAGE_ID", "");
        sesion.setAttribute("TEMP$USER_NAME", "");
        sesion.setAttribute("TEMP$GROUP", "");
        sesion.invalidate();
        return "/";
    }

    public void getIps() {
        try {
            InetAddress ip = InetAddress.getLocalHost();
            ipServer = ip.getHostAddress();

            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            ipClient = UtilUrl.getClientIp(request);

            user = request.getSession().getAttribute("TEMP$USER_NAME") != null ? request.getSession().getAttribute("TEMP$USER_NAME") + "" : "nulo";

            setDate(UtilDate.dateToString(new Date(), "dd-MM-yyyy HH:mm:ss"));

            log.debug("IPS: ipClient: " + ipClient + ", ipServer: " + ipServer);

            // ------------------------------------------------------------------
            String scheme = "http";
            if (request.getScheme() != null && request.getScheme().toLowerCase().trim().equals("https")) {
                scheme = "https";
            }

            log.info("request.getLocalAddr: " + request.getLocalAddr());
            log.info("request.getServerName: " + request.getServerName());
            log.info("InetAddress.getLocalHost().getHostAddress: " + InetAddress.getLocalHost().getHostAddress());

            String ipNodo = request.getLocalAddr();
            if (ipNodo == null || ipNodo.equals("127.0.0.1") || ipNodo.trim().isEmpty()) {
                ipNodo = request.getServerName();
                if (ipNodo == null || ipNodo.equals("127.0.0.1") || ipNodo.trim().isEmpty()) {
                    ipNodo = InetAddress.getLocalHost().getHostAddress();
                }
            }

            urlServlet = scheme + "://" + ipNodo + ":" + request.getLocalPort() + request.getContextPath() + "/MyServlet";
            urlServletVolver = scheme + "://" + ipNodo + ":" + request.getLocalPort() + request.getContextPath() + "/Certificado";

            log.debug("urlServlet: " + urlServlet);
            log.debug("urlServletVolver: " + urlServletVolver);

            urlServlet = encriptUrl(urlServlet);
            urlServletVolver = encriptUrl(urlServletVolver);

            // ------------------------------------------------------------------

            Enumeration<String> headerNames = request.getHeaderNames();
            while (headerNames.hasMoreElements()) {
                String name = headerNames.nextElement();

                // System.out.println(name + " : " + request.getHeader(name));
                log.debug("VARIABLE CABECERA " + name + ": " + request.getHeader(name));
            }

        } catch (Exception e) {
            log.error("Error al obtener ips: ", e);
        }
    }

    public void liberar(String usuario, boolean logout) {

        try {
            if (logout) {
                log.debug("Liberando conexiones para el usuario: " + usuario + " por logout");
            } else {
                log.debug("Liberando conexiones para el usuario: " + usuario + " por inicio de sesion");
            }

            ServicioBilletera.liberar(usuario);
            ServicioCallingCircle.liberar(usuario);
            ServicioCustomized.liberar(usuario);
            ServicioHistorial.liberar(usuario);
            ServicioVoucher.liberar(usuario);
            ServicioDetalleNavegacion.liberar(usuario);

        } catch (Exception e) {
            log.error("Error al liberar conexiones de los hashmap: ", e);
        }
    }

    public String encriptUrl(String url) {
        if (url.contains("https"))
            url = url.replace("https", "A4853A");
        else
            url.replace("http", "A48A");

        url = url.replace("/", "A2FA");
        url = url.replace(".", "A2EA");
        url = url.replace(":", "A3AA");
        return url;
    }

    public void logoutSimple() {
        try {
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            String strIdUs = (String) request.getSession().getAttribute("TEMP$USER_NAME");
            // String address = request.getRemoteAddr();
            liberar(strIdUs, true);
            String address = ipClient;
            controlLogin.salirBitacora(strIdUs, address);

            VcNodo nodo = nodoDao.getNodoLogin(userId);
            if (nodo != null) {
                nodoDao.remove(nodo);
            } else {
                log.warn("Usuario no encontrado para logout: " + userId);
            }

            FacesContext.getCurrentInstance().getExternalContext().redirect(request.getContextPath() + "/Logout");

        } catch (Exception e) {
            log.error("Error al cerrar sesion: ", e);
        }
    }

    public void addNota() {
        listNota.add(new Nota((new Date()).getTime(), "", "", 150));
    }

    public void removeNota() {
        if (notaSeleccionada != null) {
            log.debug("nota eliminada: " + notaSeleccionada.toString());
            listNota.remove(notaSeleccionada);
            notaSeleccionada = null;
        } else {
            log.debug("nota null");
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public String verifyLogin() {
        log.debug("iniciando sesion por login: " + ipClient);
        try {

            String cuenta = userId.toLowerCase();

            log.debug("userId: " + userId + ", password: *** , ipClient: " + ipClient);

            try {
                ipClient = decryptBase64(ipClient);
            } catch (Exception e) {
                log.warn("ErrorIpDecoder: ", e);
            }

//			Map<String, String> map = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
//			for (Map.Entry<String, String> entry : map.entrySet()) {
//				log.debug("Clave: " + entry.getKey() + ": " + entry.getValue());
//			}

            // ---------------------------------------------------------------------
            if (ipClient == null || ipClient.trim().isEmpty() || ipClient.equals("ip")) {
                HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
                ipClient = request.getRemoteAddr();
                if (ipClient == null || ipClient.trim().isEmpty() || ipClient.equals("ip")) {
                    SysMessage.error(Parametros.mensajeValidacionIp, null);
                    log.debug("userId: " + userId + ", ipClient: " + ipClient);
//					ipClient = "172.31.86.54";
                    return "";
                }
            }
            ipClient = ipClient.trim();
            log.debug("userId: " + userId + ", ipClient: " + ipClient);
            // ---------------------------------------------------------------------

            String validacion = controlLogin.validateIngreso(cuenta, password);
            // String validacion = "";

            if (!validacion.isEmpty()) {
                SysMessage.error(validacion, null);
                return "";

            }

            boolean irActive = Parametros.consultarDirectorioActivo;
            int valid = 0;

            if (irActive) {
                valid = ActiveDirectory.validarUsuario(cuenta, password);
            } else {
                valid = controlLogin.existUser(cuenta, password);
                // valid = 1;
            }

            if (valid == ActiveDirectory.EXIT_USER) {


                List groups = null;
                if (irActive) {
                    groups = ActiveDirectory.getListaGrupos(cuenta);
                    // groups = controlLogin.getListaGrupo();
                } else {
                    groups = controlLogin.getListaGrupo();
                }

                if (groups != null && groups.size() > 0) {
                    long idRol = controlLogin.getIdRole(cuenta, groups);
                    // long idRol = 1;

                    log.debug("[usuario: " + userId + ", rolId:" + idRol + "] [Se obtuvo un rol]");

                    if (idRol > 0) {
                        // String remoteAddr = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRemoteAddr();
                        // log.debug("[usuario: " + userId + ", rolId:" + idRol + "] [Se obtuvo un rol]");
                        // log.debug("cliente ip: " + remoteAddr);

                        if (controlLogin.verificarIp(idRol, ipClient)) {

                            log.debug("[usuario: " + userId + ", rolId:" + idRol + "] login exitoso");

                            userId = userId.toLowerCase();

                            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();

                            if (request.getSession().getAttribute("TEMP$USER_NAME") != null) {
                                String user = (String) request.getSession().getAttribute("TEMP$USER_NAME");
                                if (user.equals(userId)) {
                                    log.debug("Ya inicio sesion ............................");
                                    return "/view/menu.xhtml";
                                }
                            }

                            request.getSession().setAttribute("TEMP$USER_NAME", userId);
                            request.getSession().setAttribute("TEMP$GROUP_NAME", groups);
                            request.getSession().setAttribute("TEMP$IP_CLIENT", ipClient.trim());
                            user = userId;

                            HttpServletRequest httpRequest = (HttpServletRequest) request;
                            idSession = httpRequest.getRequestedSessionId();

                            // remoteAddr = remoteAddr.replaceAll("\\.", "_");
                            controlLogin.addBitacoraLogin(userId, ipClient, idRol);
                            // controlLogin.reiniciarReintentos(userId);

                            cargarv2(idRol);

                            liberar(userId, false);

                            return "/view/menu.xhtml";
                        } else {
                            log.debug("[usuario: " + userId + ", rolId:" + idRol + "] login insatisfactorio por validacion de ip-rol");
                            // SysMessage.error("Su equipo no esta registrado para el Rol Asignado", null);
                            // return "";
                            validacion = controlLogin.controlErrorRol(cuenta, ipClient);
                            SysMessage.error(validacion, null);
                            return "";
                        }
                    } else {
                        log.debug("[usuario: " + userId + " usuario no registrado para ningun rol");
                        SysMessage.error("Usted no esta registrado para ningun Rol", null);
                        return "";
                    }
                } else {
                    log.debug("[usuario: " + userId + " usuario no tiene grupo de trabajo");
                    SysMessage.error("Usted no tiene grupo de trabajo", null);
                    return "";
                }
            } else {
                log.debug("[usuario: " + userId + " usuario no existe en el directorio activo");
                validacion = controlLogin.controlError(cuenta, ipClient);
                SysMessage.error(validacion, null);
                return "";
            }
        } catch (Exception e) {
            log.error("Error al iniciar sesion: ", e);
            SysMessage.error("Error al iniciar sesión: " + e.getMessage(), null);
            return "";
        }
    }

    private void cargarv2(long roleId) {
        model = new DefaultMenuModel();
        List<MuFormulario> lform = daoFormulario.findPadres(roleId);
        for (MuFormulario form : lform) {
            // model.addSubmenu((DefaultSubMenu) cargarv2(form, roleId));
            model.addElement((DefaultSubMenu) cargarv2(form, roleId));
        }

        DefaultSubMenu submenu = new DefaultSubMenu();
        submenu.setLabel("Opciones");

        // DefaultMenuItem manualInstalacion = new DefaultMenuItem();
        // manualInstalacion.setValue("Manual de Implementacion");
        // String pathManualInstalacion = "/vista_ccc/resources/MI.pdf";
        // String urlManualInstalacion = "window.open('" + pathManualInstalacion + "'); return false;";
        // manualInstalacion.setOnclick(urlManualInstalacion);
        // manualInstalacion.setIcon("ui-icon ui-icon-document");
        //
        // DefaultMenuItem manualOperacion = new DefaultMenuItem();
        // manualOperacion.setValue("Manual de Operacion");
        // String pathManualOperacion = "/vista_ccc/resources/MO.pdf";
        // String urlManualOperacion = "window.open('" + pathManualOperacion + "'); return false;";
        // manualOperacion.setOnclick(urlManualOperacion);
        // manualOperacion.setIcon("ui-icon ui-icon-document");

        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String contextPath = request.getContextPath();

        DefaultMenuItem manualUsuario = new DefaultMenuItem();
        manualUsuario.setValue("Manual de Usuario");
        // String pathManualUsuario = "/vista_ccc/resources/MU_CBS.pdf";
        String pathManualUsuario = contextPath + "/resources/MU_CBS.pdf";
        String urlManualUsuario = "window.open('" + pathManualUsuario + "'); return false;";
        manualUsuario.setOnclick(urlManualUsuario);
        manualUsuario.setIcon("ui-icon ui-icon-document");

        // submenu.addElement(manualInstalacion);
        // submenu.addElement(manualOperacion);
        submenu.addElement(manualUsuario);

        // item = new DefaultMenuItem();
        // item.setValue("Cerrar Sesión");
        // item.setIcon("ui-icon ui-icon-close");
        // item.setUrl("/Logout");
        // submenu.addElement(item);
        model.addElement(submenu);
    }

    private MenuElement cargarv2(MuFormulario form, long roleId) {
        List<MuFormulario> lHijos = daoFormulario.findHijos(form.getId(), roleId);
        if (lHijos != null && !lHijos.isEmpty()) {
            DefaultSubMenu submenu = new DefaultSubMenu();
            submenu.setLabel(form.getNombre());
            for (MuFormulario hijo : lHijos) {
                // submenu.getChildren().add(cargarv2(hijo, roleId));
                submenu.addElement(cargarv2(hijo, roleId));
            }
            return submenu;
        } else {
            DefaultMenuItem item = new DefaultMenuItem();
            item.setValue(form.getNombre());
            item.setUrl(form.getUrl());
            // item.setOnclick("cargar();");
            // item.setIcon("fa fa-ellipsis-v");
            item.setIcon("fa fa-external-link");
            return item;
        }
    }

    public String getExpresionRegularUsuario() {
        return Parametros.expresionRegularUsuario;
    }

    public String getMensajeValidacionUsuario() {
        return Parametros.mensajeValidacionUsuario;
    }

    public String getExpresionRegularPassword() {
        return Parametros.expresionRegularPassword;
    }

    public String getMensajeValidacionPassword() {
        return Parametros.mensajeValidacionPassword;
    }
    //
    // public String getExpresionRegularGeneral() {
    // return P.getParametro(ParametroID.EXPRESION_REGULAR_GENERAL).getValorCadena();
    // }
    //
    // public String getMensajeValidacionGeneral() {
    // return P.getParametro(ParametroID.MENSAJE_VALIDACION_GENERAL).getValorCadena();
    // }

    public String getTituloAplicacion() {
        return Parametros.tituloAplicacion;
    }

    public String getPiePagina() {
        return Parametros.tituloPiePagina;
    }

    private String decryptBase64(String valor) throws Exception {
        if (valor != null) {
            byte[] str = Base64.decode(valor);
            if (str != null)
                valor = new String(str, "UTF-8");
        }
        return valor;
    }

    // private String encryptBase64(String valor) throws Exception {
    // if (valor != null) {
    // byte[] utf8 = valor.getBytes("UTF8");
    //
    // valor = Base64.encodeToString(utf8, false);
    // }
    // return valor;
    // }
    // public static String getClientIpAddr(HttpServletRequest request) {
    // String ip = request.getHeader("X-Forwarded-For");
    // if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
    // ip = request.getHeader("Proxy-Client-IP");
    // }
    // if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
    // ip = request.getHeader("WL-Proxy-Client-IP");
    // }
    // if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
    // ip = request.getHeader("HTTP_CLIENT_IP");
    // }
    // if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
    // ip = request.getHeader("HTTP_X_FORWARDED_FOR");
    // }
    // if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
    // ip = request.getRemoteAddr();
    // }
    // return ip;
    // }

    public MenuModel getModel() {
        return model;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Nota> getListNota() {
        return listNota;
    }

    public void setListNota(List<Nota> listNota) {
        this.listNota = listNota;
    }

    public Nota getNotaSeleccionada() {
        return notaSeleccionada;
    }

    public void setNotaSeleccionada(Nota notaSeleccionada) {
        this.notaSeleccionada = notaSeleccionada;
    }

    public String getIpClient() {
        return ipClient;
    }

    public void setIpClient(String ipClient) {
        this.ipClient = ipClient;
    }

    public String getIpServer() {
        return ipServer;
    }

    public void setIpServer(String ipServer) {
        this.ipServer = ipServer;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isCollapsed() {
        return collapsed;
    }

    public void setCollapsed(boolean collapsed) {
        this.collapsed = collapsed;
    }

    public String getUrlServlet() {
        return urlServlet;
    }

    public void setUrlServlet(String urlServlet) {
        this.urlServlet = urlServlet;
    }

    public String getUrlServletVolver() {
        return urlServletVolver;
    }

    public void setUrlServletVolver(String urlServletVolver) {
        this.urlServletVolver = urlServletVolver;
    }

    public String getIdSession() {
        return idSession;
    }

    public void setIdSession(String idSession) {
        this.idSession = idSession;
    }

    // <p:panel id="panelNota">
    // <h:panelGrid columns="1">
    // <h:outputText value="IP CLIENT: #{login.ipClient}" />
    // <h:outputText value="IP SERVER: #{login.ipServer}" />
    // <h:outputText value="USER: #{login.user}" />
    // <h:outputText value="DATE: #{login.date}" />
    // <p:commandButton action="#{login.getIps()}" update=":formNota:panelNota" value="Refresh" icon="fa fa-refresh" />
    // </h:panelGrid>
    // </p:panel>
}
