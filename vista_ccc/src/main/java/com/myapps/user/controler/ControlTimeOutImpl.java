/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapps.user.controler;

import com.myapps.user.business.BitacoraBL;
import com.myapps.user.business.NodoBL;
import com.myapps.user.model.MuBitacora;
import com.myapps.user.model.VcNodo;
import com.myapps.vista_ccc.servicio.*;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author Marciano
 */
@Named
public class ControlTimeOutImpl implements Serializable {

	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(ControlTimeOutImpl.class);

	@Inject
	private BitacoraBL logBL;

	@Inject
	private NodoBL nodoBL;

	// private static Map<String, NodoClient> mapClient = new HashMap<String, NodoClient>();
	// private static Map<String, NodoIngresoUser> mapIngresClient = new HashMap<String, NodoIngresoUser>();

	// private static int timeOut = Parametros.tiempoFuera;
	// private int indexPath = 0;

	public ControlTimeOutImpl() {

	}

	// public void addRutaBase(String path) {
	// int k = path.lastIndexOf("/");
	// indexPath = k + 1;
	// }

	// public void addNodoClient(NodoClient nd) {
	// mapClient.put(nd.getUser(), nd);
	// }

	// public NodoClient getNodoClient(String user) {
	// return mapClient.get(user);
	// }

	public VcNodo getNodoClient2(String user) {
		try {
			return nodoBL.getNodoLogin(user);
		} catch (Exception e) {
			log.error("Error al obtener nodo cliente: ", e);
		}
		return null;
	}

	// public void createNewClient(String user, String addressIp) {
	//
	// NodoClient nd = new NodoClient();
	// nd.setUser(user);
	// nd.setAddressIp(addressIp);
	// nd.setTime(new Date().getTime());
	// mapClient.put(user, nd);
	// }

	// public void setDatos(String user, long d) {
	// NodoClient nd = mapClient.get(user);
	// nd.setTime(d);
	// }

	public void setDatos2(VcNodo nodo, long time) {
		nodo.setTime(time);
		try {
			nodoBL.update(nodo);
		} catch (Exception e) {
			log.error("Error al actualizar nodo cliente: ", e);
		}
	}

	// public void remove(String user) {
	// mapClient.remove(user);
	// }

	// public String getAddressIP(String user) {
	// if (mapClient.get(user) == null) {
	// return "";
	// } else {
	// return mapClient.get(user).getAddressIp();
	// }
	// }

	public String getAddressIP2(String user) {
		try {
			VcNodo nodo = nodoBL.getNodoLogin(user);
			if (nodo != null) {
				if (nodo.getIp() != null)
					return nodo.getIp();
			}
		} catch (Exception e) {
			e.getMessage();
		}
		return "";
	}

	public void registerOutTime(long timeMax) {

		Date d = new Date();
		long tp = d.getTime();
		try {
			List<VcNodo> list = nodoBL.getList();
			if (list != null) {
				for (VcNodo nodo : list) {

					long value = nodo.getTime();
					if ((tp - value) > timeMax) {

						liberar(nodo.getUsuario());

						MuBitacora logg = new MuBitacora();
						logg.setFormulario("");
						logg.setAccion("Salio del Sistema por expiracion de tiempo");
						logg.setUsuario(nodo.getUsuario());
						logg.setFecha(new Timestamp(value + timeMax));
						logg.setDireccionIp(nodo.getIp());
						logBL.save(logg);
						nodoBL.remove(nodo);

					}
				}
			}
		} catch (Exception e) {
			log.error("Error al eliminar clientes con timeout: ", e);
		}
	}

	// @SuppressWarnings("rawtypes")
	// public void clearUserCliente() {
	// int nroTime = timeOut;
	// long timeMax = ((long) nroTime * 60000l);
	// Iterator em = mapIngresClient.keySet().iterator();
	// Date d = new Date();
	// long tp = d.getTime();
	// try {
	//
	// while (em.hasNext()) {
	// String key = (String) em.next();
	// long value = mapIngresClient.get(key).getTime();
	// if ((tp - value) > timeMax) {
	// mapIngresClient.remove(key);
	// }
	// }
	// } catch (Exception e) {
	// }
	// }

	public VcNodo exisUserIngreso2(String user) {
		try {
			return nodoBL.getNodoLogin(user);
		} catch (Exception e) {
			log.error("Error al obtener nodo cliente: ", e);
		}
		return null;
	}

	// public boolean exisUserIngreso(String user) {
	//
	// return mapIngresClient.containsKey(user);
	// }

	public void insertUserIngreso2(String user, String ip) throws Exception {

		VcNodo nodo = new VcNodo();
		nodo.setFecha(Calendar.getInstance());
		nodo.setCount(1);
		nodo.setUsuario(user);
		nodo.setIp(ip);
		nodo.setTime(new Date().getTime());
		nodoBL.save(nodo);
	}

	// public void deleteUserIngreso(String user) {
	// mapIngresClient.remove(user);
	// }

	// public int countIntentoUserIngreso(String user) {
	// NodoIngresoUser nd = mapIngresClient.get(user);
	// return nd.getCount();
	// }

	// public void ingrementIntentoUser(String user) {
	// NodoIngresoUser nd = mapIngresClient.get(user);
	// nd.setDate(new Date());
	// nd.increment();
	// }

	// public NodoIngresoUser getNodoIngresoUser(String user) {
	// return mapIngresClient.get(user);
	// }

	// public boolean isPaginaUsuario(String user, String path) {
	//
	// NodoClient nd = getNodoClient(user);
	// String strPg = path.substring(indexPath);
	// return nd.existeUrl(strPg);
	//
	// }

	public void liberar(String usuario) {

		try {
			log.debug("Liberando conexiones por time out para el usuario: " + usuario);
			ServicioBilletera.liberar(usuario);
			ServicioCallingCircle.liberar(usuario);
			ServicioCustomized.liberar(usuario);
			ServicioHistorial.liberar(usuario);
			ServicioVoucher.liberar(usuario);
		} catch (Exception e) {
			log.error("Error al liberar conexiones de los hashmap: ", e);
		}
	}

}