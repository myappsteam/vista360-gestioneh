package com.myapps.user.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;


/**
 * The persistent class for the MU_ROL_IP database table.
 * 
 */
@Entity
@Table(name="MU_ROL_IP")
@NamedQuery(name="MuRolIp.findAll", query="SELECT m FROM MuRolIp m")
public class MuRolIp implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="MU_ROL_IP_IPID_GENERATOR", sequenceName="SEQ_MU_IP",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="MU_ROL_IP_IPID_GENERATOR")
	@Column(name="IP_ID")
	private long ipId;

	private boolean estado;

	@Column(name="FECHA_CREACION")
	private Calendar fechaCreacion;

	private String ip;
	private String ipF;

	public String getIpF() {
		return this.ipF;
	}

	public void setIpF(String ipF) {
		this.ipF = ipF;
	}

	//bi-directional many-to-one association to MuRol
	@ManyToOne
	@JoinColumn(name="ROL_ID")
	private MuRol muRol;

	public MuRolIp() {
	}

	public long getIpId() {
		return this.ipId;
	}

	public void setIpId(long ipId) {
		this.ipId = ipId;
	}

	public boolean getEstado() {
		return this.estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public Calendar getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Calendar fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public MuRol getMuRol() {
		return this.muRol;
	}

	public void setMuRol(MuRol muRol) {
		this.muRol = muRol;
	}

}