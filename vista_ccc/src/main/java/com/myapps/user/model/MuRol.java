package com.myapps.user.model;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the MU_ROL database table.
 * 
 */
@Entity
@Table(name="MU_ROL")
public class MuRol implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="MU_ROL_ROLID_GENERATOR", sequenceName="SEQ_MU_ROL", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="MU_ROL_ROLID_GENERATOR")
	@Column(name="ROL_ID")
	private long rolId;

	private String descripcion;

	private boolean estado;

	private String nombre;

	@Column(name="VALIDA_IP")
	private boolean validaIp;

	public MuRol() {
	}

	public long getRolId() {
		return this.rolId;
	}

	public void setRolId(long rolId) {
		this.rolId = rolId;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public boolean getEstado() {
		return this.estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean getValidaIp() {
		return this.validaIp;
	}

	public void setValidaIp(boolean validaIp) {
		this.validaIp = validaIp;
	}

}