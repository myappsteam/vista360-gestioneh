package com.myapps.user.filter;

import com.myapps.vista_ccc.util.Parametros;
import org.primefaces.util.Base64;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Servlet implementation class MyServlet
 */
@WebServlet("/MyServlet")
public class MyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MyServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());	
		// Set response content type
		// response.setContentType("text/html");
		response.setHeader("Access-Control-Allow-Origin", Parametros.dominioAccessOrigin);
		
		response.setHeader("X-XSS-Protection", "1; mode=block");
		response.setHeader("X-CONTENT-TYPE-OPTIONS", "nosniff");
		response.setHeader("Strict-Transport-Security", "max-age=31536000; includeSubDomains");
		if (request.getScheme() != null && request.getScheme().toLowerCase().trim().equals("https")) {
		response.setHeader("Content-Security-Policy", "script-src 'unsafe-inline' " + Parametros.dominioAccessOrigin);
		}
//		response.setHeader("Access-Control-Allow-Headers", "*");
		response.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");

		response.setContentType("text/plain");
		PrintWriter out = response.getWriter();
		
		String result=request.getRemoteAddr();
		 byte[] utf8 = result.getBytes("UTF8");

		result = Base64.encodeToString(utf8, false);
		//log.info("Ip desencriptada por cargar ip: " + ipClient);	

		out.println(result);
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
