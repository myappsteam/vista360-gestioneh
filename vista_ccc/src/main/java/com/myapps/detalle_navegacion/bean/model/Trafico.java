package com.myapps.detalle_navegacion.bean.model;

import java.io.Serializable;

public class Trafico implements Serializable, Comparable<Trafico> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer nro;
	private String aplicacion;
	private double porcentajeTotalTrafico;
	private double totalTrafico;
	private double downTrafico;
	private double upTrafico;

	public Trafico() {
		super();
	}

	public Trafico(Integer nro, String aplicacion, double porcentajeTotalTrafico, double totalTrafico, double downTrafico, double upTrafico) {
		super();
		this.nro = nro;
		this.aplicacion = aplicacion;
		this.porcentajeTotalTrafico = porcentajeTotalTrafico;
		this.totalTrafico = totalTrafico;
		this.downTrafico = downTrafico;
		this.upTrafico = upTrafico;

	}

	public Integer getNro() {
		return nro;
	}

	public void setNro(Integer nro) {
		this.nro = nro;
	}

	public String getAplicacion() {
		return aplicacion;
	}

	public void setAplicacion(String aplicacion) {
		this.aplicacion = aplicacion;
	}

	public double getPorcentajeTotalTrafico() {
		return porcentajeTotalTrafico;
	}

	public void setPorcentajeTotalTrafico(double porcentajeTotalTrafico) {
		this.porcentajeTotalTrafico = porcentajeTotalTrafico;
	}

	public double getTotalTrafico() {
		return totalTrafico;
	}

	public void setTotalTrafico(double totalTrafico) {
		this.totalTrafico = totalTrafico;
	}

	public double getDownTrafico() {
		return downTrafico;
	}

	public void setDownTrafico(double downTrafico) {
		this.downTrafico = downTrafico;
	}

	public double getUpTrafico() {
		return upTrafico;
	}

	public void setUpTrafico(double upTrafico) {
		this.upTrafico = upTrafico;
	}

	@Override
	public String toString() {
		return "Trafico [nro=" + nro + ", aplicacion=" + aplicacion + ", porcentajeTotalTrafico=" + porcentajeTotalTrafico + ", totalTrafico=" + totalTrafico + ", downTrafico=" + downTrafico + ", upTrafico=" + upTrafico + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aplicacion == null) ? 0 : aplicacion.hashCode());
		long temp;
		temp = Double.doubleToLongBits(downTrafico);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((nro == null) ? 0 : nro.hashCode());
		temp = Double.doubleToLongBits(porcentajeTotalTrafico);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(totalTrafico);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(upTrafico);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Trafico other = (Trafico) obj;
		if (aplicacion == null) {
			if (other.aplicacion != null)
				return false;
		} else if (!aplicacion.equals(other.aplicacion))
			return false;
		if (Double.doubleToLongBits(downTrafico) != Double.doubleToLongBits(other.downTrafico))
			return false;
		if (nro == null) {
			if (other.nro != null)
				return false;
		} else if (!nro.equals(other.nro))
			return false;
		if (Double.doubleToLongBits(porcentajeTotalTrafico) != Double.doubleToLongBits(other.porcentajeTotalTrafico))
			return false;
		if (Double.doubleToLongBits(totalTrafico) != Double.doubleToLongBits(other.totalTrafico))
			return false;
		if (Double.doubleToLongBits(upTrafico) != Double.doubleToLongBits(other.upTrafico))
			return false;
		return true;
	}

	@Override
	public int compareTo(Trafico o) {
		Double a = new Double(this.totalTrafico);
		Double b = new Double(o.getTotalTrafico());
		return a.compareTo(b);
	}

}
