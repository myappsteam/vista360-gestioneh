package com.myapps.detalle_navegacion.bean;

import com.myapps.detalle_navegacion.business.AplicacionBL;
import com.myapps.detalle_navegacion.business.AplicacionComercialBL;
import com.myapps.detalle_navegacion.entity.DnAplicacion;
import com.myapps.detalle_navegacion.entity.DnAplicacionComercial;
import com.myapps.detalle_navegacion.util.ParametrosDn;
import com.myapps.detalle_navegacion.util.SysMessage;
import com.myapps.user.bean.ControlerBitacora;
import com.myapps.user.ldap.DescriptorBitacora;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ViewScoped
public class AplicacionComercialBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(AplicacionComercialBean.class);

	@Inject
	private ControlerBitacora controlerBitacora;

	@Inject
	private AplicacionBL aplicacionBl;

	@Inject
	private AplicacionComercialBL aplicacionComercialBl;

	private List<DnAplicacionComercial> list;
	private List<DnAplicacionComercial> listFiltered;

	@PostConstruct
	public void init() {
		iniciar();
	}

	@SuppressWarnings("unchecked")
	public void iniciar() {
		try {

			actualizarConfiguracion();
			list = aplicacionComercialBl.findAll();

		} catch (Exception e) {
			log.error("Error al iniciar: ", e);
		}
	}

	public void update() {
		// log.debug("update.....");
	}

	@SuppressWarnings("unchecked")
	private void actualizarConfiguracion() {
		try {
			List<DnAplicacionComercial> listaActualizar = new ArrayList<>();
			
			List<DnAplicacion> aplicaciones = aplicacionBl.findAll();
			if (aplicaciones != null) {
				for (DnAplicacion item : aplicaciones) {
					DnAplicacionComercial findAplicacionComercial = (DnAplicacionComercial) aplicacionComercialBl.find(item.getAplicacionId());
					if (findAplicacionComercial == null) {
						DnAplicacionComercial entidad = new DnAplicacionComercial();
						entidad.setEstado(1);
						entidad.setIdAplicacion(item.getAplicacionId());
						entidad.setLimite(ParametrosDn.multiplicadorBytes);
						entidad.setLimiteMb(1);
						entidad.setVisible(true);
						entidad.setNombre(item.getNombre());
						entidad.setNombreComercial(item.getNombre());
						listaActualizar.add(entidad);
//						aplicacionComercialBl.save(entidad);
					}
				}
				aplicacionComercialBl.actualizarConfiguracion(listaActualizar);
			}

		} catch (Exception e) {
			log.error("Error al actualizar configuracion de aplicaciones: ", e);
			SysMessage.error("Error al actualizar configuración de aplicaciones", null);
		}
	}

	@SuppressWarnings("unchecked")
	public void save() {
		try {
			// for (DnAplicacionComercial item : list) {
			// item.setLimite(item.getLimiteMb() * 1048576);
			// aplicacionComercialBl.update(item);
			// }
			
			aplicacionComercialBl.actualizarAplicaciones(list, ParametrosDn.multiplicadorBytes);

			try {
				controlerBitacora.accion(DescriptorBitacora.DETALLE_NAVEGACION_CONFIGURACION_APLICACION, "Se guardó la configuración de aplicaciones");
			} catch (Exception e) {
				log.error("Error al guardar bitacora en el sistema: ", e);
				SysMessage.error("Error al guardar bitacora en el sistema", null);
			}

			// RequestContext.getCurrentInstance().execute("PF('tabla').clearFilters();");
			list = aplicacionComercialBl.findAll();
			SysMessage.info("Se registró correctamente", null);
		} catch (Exception e) {
			log.error("Error al guardar configuracion de aplicaciones: ", e);
			SysMessage.error("Error al guardar configuracion de aplicaciones", null);
		}

	}

	public List<DnAplicacionComercial> getList() {
		return list;
	}

	public void setList(List<DnAplicacionComercial> list) {
		this.list = list;
	}

	public List<DnAplicacionComercial> getListFiltered() {
		return listFiltered;
	}

	public void setListFiltered(List<DnAplicacionComercial> listFiltered) {
		this.listFiltered = listFiltered;
	}
}
