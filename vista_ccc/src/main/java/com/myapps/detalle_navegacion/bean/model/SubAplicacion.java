package com.myapps.detalle_navegacion.bean.model;

import java.io.Serializable;

public class SubAplicacion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private String nombre;
	private Long aplicacionId;

	private String aplicacionNombre;
	private Long categoriaId;
	private String categoriaNombre;

	public SubAplicacion() {
		super();
	}

	public SubAplicacion(Long id, String nombre, Long aplicacionId, String aplicacionNombre, Long categoriaId, String categoriaNombre) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.aplicacionId = aplicacionId;
		this.aplicacionNombre = aplicacionNombre;
		this.categoriaId = categoriaId;
		this.categoriaNombre = categoriaNombre;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Long getAplicacionId() {
		return aplicacionId;
	}

	public void setAplicacionId(Long aplicacionId) {
		this.aplicacionId = aplicacionId;
	}

	public String getAplicacionNombre() {
		return aplicacionNombre;
	}

	public void setAplicacionNombre(String aplicacionNombre) {
		this.aplicacionNombre = aplicacionNombre;
	}

	public Long getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(Long categoriaId) {
		this.categoriaId = categoriaId;
	}

	public String getCategoriaNombre() {
		return categoriaNombre;
	}

	public void setCategoriaNombre(String categoriaNombre) {
		this.categoriaNombre = categoriaNombre;
	}

	@Override
	public String toString() {
		return "SubAplicacion [id=" + id + ", nombre=" + nombre + ", aplicacionId=" + aplicacionId + ", aplicacionNombre=" + aplicacionNombre + ", categoriaId=" + categoriaId + ", categoriaNombre=" + categoriaNombre + "]";
	}

}
