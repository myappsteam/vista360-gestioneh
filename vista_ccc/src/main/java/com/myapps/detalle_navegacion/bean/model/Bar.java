package com.myapps.detalle_navegacion.bean.model;

import java.io.Serializable;
import java.util.List;

public class Bar implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String name;
	private List<Double> data;

	public Bar() {
		super();
	}

	public Bar(String name, List<Double> data) {
		super();
		this.name = name;
		this.data = data;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Double> getData() {
		return data;
	}

	public void setData(List<Double> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Bar [name=" + name + ", data=" + data + "]";
	}

}
