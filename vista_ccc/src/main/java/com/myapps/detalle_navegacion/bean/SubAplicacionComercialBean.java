package com.myapps.detalle_navegacion.bean;

import com.myapps.detalle_navegacion.business.SubAplicacionBL;
import com.myapps.detalle_navegacion.business.SubAplicacionComercialBL;
import com.myapps.detalle_navegacion.entity.DnSubaplicacion;
import com.myapps.detalle_navegacion.entity.DnSubaplicacionComercial;
import com.myapps.detalle_navegacion.util.ParametrosDn;
import com.myapps.detalle_navegacion.util.SysMessage;
import com.myapps.user.bean.ControlerBitacora;
import com.myapps.user.ldap.DescriptorBitacora;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ViewScoped
public class SubAplicacionComercialBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(SubAplicacionComercialBean.class);

	@Inject
	private ControlerBitacora controlerBitacora;

	@Inject
	private SubAplicacionBL subAplicacionBl;

	@Inject
	private SubAplicacionComercialBL subAplicacionComercialBl;

	private List<DnSubaplicacionComercial> list;
	private List<DnSubaplicacionComercial> listFiltered;

	@PostConstruct
	public void init() {
		iniciar();
	}

	@SuppressWarnings("unchecked")
	public void iniciar() {
		try {

			actualizarConfiguracion();
			list = subAplicacionComercialBl.findAll();

		} catch (Exception e) {
			log.error("Error al iniciar: ", e);
		}
	}

	public void update() {
		// log.debug("update.....");
	}

	@SuppressWarnings("unchecked")
	private void actualizarConfiguracion() {
		try {
			List<DnSubaplicacionComercial> listaActualizar = new ArrayList<>();
			List<DnSubaplicacion> aplicaciones = subAplicacionBl.findAll();
			if (aplicaciones != null) {
				for (DnSubaplicacion item : aplicaciones) {
					DnSubaplicacionComercial findAplicacionComercial = (DnSubaplicacionComercial) subAplicacionComercialBl.find(item.getSubaplicacionId());
					if (findAplicacionComercial == null) {
						DnSubaplicacionComercial entidad = new DnSubaplicacionComercial();
						entidad.setEstado(1);
						entidad.setIdSubaplicacion(item.getSubaplicacionId());
						entidad.setLimite(ParametrosDn.multiplicadorBytes);
						entidad.setLimiteMb(1);
						entidad.setVisible(true);
						entidad.setNombre(item.getNombre());
						entidad.setNombreComercial(item.getNombre());
						listaActualizar.add(entidad);
						// subAplicacionComercialBl.save(entidad);
					}
				}
				subAplicacionComercialBl.actualizarConfiguracion(listaActualizar);
			}

		} catch (Exception e) {
			log.error("Error al actualizar configuracion de sub aplicaciones: ", e);
			SysMessage.error("Error al actualizar configuración de sub aplicaciones", null);
		}
	}

	@SuppressWarnings("unchecked")
	public void save() {
		try {
			// for (DnSubaplicacionComercial item : list) {
			// item.setLimite(item.getLimiteMb() * 1048576);
			// subAplicacionComercialBl.update(item);
			// }

			subAplicacionComercialBl.actualizarSubAplicaciones(list, ParametrosDn.multiplicadorBytes);

			// RequestContext.getCurrentInstance().execute("PF('tabla').clearFilters();");

			try {
				controlerBitacora.accion(DescriptorBitacora.DETALLE_NAVEGACION_CONFIGURACION_SUBAPLICACION, "Se guardó la configuración de subaplicaciones");
			} catch (Exception e) {
				log.error("Error al guardar bitacora en el sistema: ", e);
				SysMessage.error("Error al guardar bitacora en el sistema", null);
			}

			list = subAplicacionComercialBl.findAll();
			SysMessage.info("Se registró correctamente", null);
		} catch (Exception e) {
			log.error("Error al guardar configuracion de sub aplicaciones: ", e);
			SysMessage.error("Error al guardar configuracion de sub aplicaciones", null);
		}

	}

	public List<DnSubaplicacionComercial> getList() {
		return list;
	}

	public void setList(List<DnSubaplicacionComercial> list) {
		this.list = list;
	}

	public List<DnSubaplicacionComercial> getListFiltered() {
		return listFiltered;
	}

	public void setListFiltered(List<DnSubaplicacionComercial> listFiltered) {
		this.listFiltered = listFiltered;
	}
}
