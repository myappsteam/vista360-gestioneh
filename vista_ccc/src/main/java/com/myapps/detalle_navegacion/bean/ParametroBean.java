package com.myapps.detalle_navegacion.bean;

import com.ibm.icu.util.StringTokenizer;
import com.myapps.detalle_navegacion.business.ParametroBL;
import com.myapps.detalle_navegacion.entity.DnParametro;
import com.myapps.detalle_navegacion.util.SysMessage;
import com.myapps.user.bean.ControlerBitacora;
import com.myapps.user.ldap.DescriptorBitacora;
import com.myapps.vista_ccc.util.Parametros;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ViewScoped
public class ParametroBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(ParametroBean.class);

	@Inject
	private ControlerBitacora controlerBitacora;

	private DnParametro parametro;
	private boolean edit = false;
	private List<DnParametro> listParametro;
	private List<DnParametro> listParametroFiltered;

	@Inject
	private ParametroBL parametroBl;

	@PostConstruct
	public void init() {
		iniciar();
	}

	@SuppressWarnings("unchecked")
	public void iniciar() {
		try {

			edit = false;
			parametro = new DnParametro();
			listParametro = parametroBl.findAll();
			// listParametro = parametroBl.findAllParametros();
			listParametroFiltered = null;
		} catch (Exception e) {
			log.error("Error al iniciar: ", e);
		}
	}

	public void save() {

		String message = parametroBl.validar(parametro, false);
		if (!message.isEmpty()) {
			SysMessage.warn(message, null);
			return;
		}

		try {
			DnParametro antes = (DnParametro) parametroBl.find(parametro.getParametroId(), DnParametro.class);
			if (isEdit()) { // modificar
				parametroBl.update(parametro);
			}
			try {
				// controlerBitacora.accion(DescriptorBitacora.PARAMETROS, "Se actualizó el parametro [id: " + antes.getParametroId() + "] [nombre: " + antes.getNombre() + "], antes: [valor: " + antes.getValor() + "], despues: [valor: " +
				// parametro.getValor() + "]");
				controlerBitacora.accion(DescriptorBitacora.PARAMETROS, "Se actualizó el parametro [id: " + antes.getParametroId() + "] [nombre: " + antes.getNombre() + "], [valor: " + parametro.getValor() + "]");
			} catch (Exception e) {
				log.error("Error al guardar bitacora en el sistema: ", e);
				// SysMessage.error("Error al guardar bitacora en el sistema", null);
			}
			SysMessage.info("Se guardó correctamente", null);
		} catch (Exception e) {
			log.error("Error al momento de modificar o guardar: " + parametro.getNombre(), e);
			SysMessage.error("Error al guardar parametro: " + e.getLocalizedMessage(), null);
		}
		iniciar();
	}

	public void select(long id) {
		try {
			parametro = (DnParametro) parametroBl.find(id, DnParametro.class);
			setEdit(true);
		} catch (Exception e) {
			log.error("Error al seleccionar parametro: ", e);
			SysMessage.error("Error al seleccionar parametro: " + e.getLocalizedMessage(), null);
		}
	}

	public void recargar() {
		try {

			String urls = Parametros.urlServletParametroNodos;
			StringTokenizer st = new StringTokenizer(urls, ",");
			List<String> errores = new ArrayList<>();

			while (st.hasMoreElements()) {
				String token = st.nextToken().trim();
				log.debug("url nodo: " + token);

				HttpURLConnection connection = null;
				InputStream is = null;
				BufferedReader rd = null;

				try {
					URL url = new URL(token);
					connection = (HttpURLConnection) url.openConnection();
					connection.setRequestMethod("GET");
					// connection.setRequestMethod("POST");
					connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

					// connection.setRequestProperty("Content-Length", Integer.toString(urlParameters.getBytes().length));
					connection.setRequestProperty("Content-Language", "en-US");

					// connection.setUseCaches(false);
					connection.setDoOutput(true);

					// Send request
					// DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
					// wr.writeBytes(urlParameters);
					// wr.close();

					// Get Response
					// InputStream is = connection.getInputStream();

					try {
						is = connection.getInputStream();
					} catch (Exception e) {
						log.error("Error de conexion al servlet: " + token, e);
						throw (new Exception("Error de conexion al servlet: " + token));
					}
					rd = new BufferedReader(new InputStreamReader(is, "UTF-8"));
					StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
					String line;
					while ((line = rd.readLine()) != null) {
						response.append(line);
						response.append('\r');
					}
					log.info("respuesta del servlet " + token + ": " + response);

				} catch (Exception e) {
					log.error("Error al hacer request al servlet: " + token, e);
					errores.add("Error al hacer request al servlet: " + token + ", " + e.getMessage());
				} finally {
					if (rd != null) {
						rd.close();
					}
					if (is != null) {
						is.close();
					}
					if (connection != null) {
						connection.disconnect();
					}
				}

			}

			// ParametrosDn.cargarParametros();

			try {
				controlerBitacora.accion(DescriptorBitacora.PARAMETROS, "Se recargaron los parametros");
			} catch (Exception e) {
				log.error("Error al guardar bitacora en el sistema: ", e);
				// SysMessage.error("Error al guardar bitacora en el sistema", null);
			}
			for (String item : errores) {
				SysMessage.error(item, null);
			}
			SysMessage.info("Recarga de Parámetros finalizado", null);
		} catch (Exception e) {
			log.error("Error al recargar parametros: ", e);
			SysMessage.error("Error al recargar parámetros: " + e.getLocalizedMessage(), null);
		}
	}

	public DnParametro getParametro() {
		return parametro;
	}

	public void setParametro(DnParametro parametro) {
		this.parametro = parametro;
	}

	public boolean isEdit() {
		return edit;
	}

	public void setEdit(boolean edit) {
		this.edit = edit;
	}

	public List<DnParametro> getListParametro() {
		return listParametro;
	}

	public void setListParametro(List<DnParametro> listParametro) {
		this.listParametro = listParametro;
	}

	public List<DnParametro> getListParametroFiltered() {
		return listParametroFiltered;
	}

	public void setListParametroFiltered(List<DnParametro> listParametroFiltered) {
		this.listParametroFiltered = listParametroFiltered;
	}

}
