package com.myapps.detalle_navegacion.bean.model;

import java.io.Serializable;

public class PieConsumo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String nombre;
	private Double porcentaje;
	private Double consumo;
	private String unidad;

	public PieConsumo() {
		super();
	}

	public PieConsumo(String nombre, Double porcentaje, Double consumo, String unidad) {
		super();
		this.porcentaje = porcentaje;
		this.nombre = nombre;
		this.consumo = consumo;
		this.unidad = unidad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Double getConsumo() {
		return consumo;
	}

	public void setConsumo(Double consumo) {
		this.consumo = consumo;
	}

	public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	public Double getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(Double porcentaje) {
		this.porcentaje = porcentaje;
	}

	@Override
	public String toString() {
		return "PieConsumo [nombre=" + nombre + ", porcentaje=" + porcentaje + ", consumo=" + consumo + ", unidad=" + unidad + "]";
	}

}
