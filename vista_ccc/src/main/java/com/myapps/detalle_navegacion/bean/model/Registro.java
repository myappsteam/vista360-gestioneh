package com.myapps.detalle_navegacion.bean.model;

import java.io.Serializable;

public class Registro implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long categoriaId;
	private String categoria;
	private Long aplicacionId;
	private String aplicacion;
	private Long subAplicacionId;
	private String subAplicacion;
	private Long egnSubProtocolId;
	private String egnSubProtocol;

	public Registro() {
		super();
	}

	public Registro(Long categoriaId, String categoria, Long aplicacionId, String aplicacion, Long subAplicacionId, String subAplicacion, Long egnSubProtocolId, String egnSubProtocol) {
		super();
		this.categoriaId = categoriaId;
		this.categoria = categoria;
		this.aplicacionId = aplicacionId;
		this.aplicacion = aplicacion;
		this.subAplicacionId = subAplicacionId;
		this.subAplicacion = subAplicacion;
		this.egnSubProtocolId = egnSubProtocolId;
		this.egnSubProtocol = egnSubProtocol;
	}

	public Long getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(Long categoriaId) {
		this.categoriaId = categoriaId;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public Long getAplicacionId() {
		return aplicacionId;
	}

	public void setAplicacionId(Long aplicacionId) {
		this.aplicacionId = aplicacionId;
	}

	public String getAplicacion() {
		return aplicacion;
	}

	public void setAplicacion(String aplicacion) {
		this.aplicacion = aplicacion;
	}

	public Long getSubAplicacionId() {
		return subAplicacionId;
	}

	public void setSubAplicacionId(Long subAplicacionId) {
		this.subAplicacionId = subAplicacionId;
	}

	public String getSubAplicacion() {
		return subAplicacion;
	}

	public void setSubAplicacion(String subAplicacion) {
		this.subAplicacion = subAplicacion;
	}

	public Long getEgnSubProtocolId() {
		return egnSubProtocolId;
	}

	public void setEgnSubProtocolId(Long egnSubProtocolId) {
		this.egnSubProtocolId = egnSubProtocolId;
	}

	public String getEgnSubProtocol() {
		return egnSubProtocol;
	}

	public void setEgnSubProtocol(String egnSubProtocol) {
		this.egnSubProtocol = egnSubProtocol;
	}

	@Override
	public String toString() {
		return "Registro [categoriaId=" + categoriaId + ", categoria=" + categoria + ", aplicacionId=" + aplicacionId + ", aplicacion=" + aplicacion + ", subAplicacionId=" + subAplicacionId + ", subAplicacion=" + subAplicacion + ", egnSubProtocolId=" + egnSubProtocolId + ", egnSubProtocol="
				+ egnSubProtocol + "]";
	}

}
