package com.myapps.detalle_navegacion.bean.model;

import org.apache.log4j.Logger;

import java.io.Serializable;

public class Consumo implements Serializable {
	public static Logger log = Logger.getLogger(Consumo.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String fecha;
	private String hora;
	private String aplicacion;
	private String consumo;

	public Consumo() {
		super();
	}

	public Consumo(String fecha, String hora, String aplicacion, String consumo) {
		super();
		this.fecha = fecha;
		this.hora = hora;
		this.aplicacion = aplicacion;
		this.consumo = consumo;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getAplicacion() {
		return aplicacion;
	}

	public void setAplicacion(String aplicacion) {
		this.aplicacion = aplicacion;
	}

	public String getConsumo() {
		return consumo;
	}

	public void setConsumo(String consumo) {
		this.consumo = consumo;
	}

	@Override
	public String toString() {
		return "Consumo [fecha=" + fecha + ", hora=" + hora + ", aplicacion=" + aplicacion + ", consumo=" + consumo + "]";
	}

	// @Override
	// public boolean equals(Object obj) {
	// if(obj instanceof Consumo){
	// Consumo consumoAux = (Consumo) obj;
	// log.info("Consumo Equals: "+(fecha.equals(consumoAux.getFecha()) && aplicacion.equals(consumoAux.getAplicacion())));
	// return fecha.equals(consumoAux.getFecha()) && aplicacion.equals(consumoAux.getAplicacion());
	// }
	// return false;
	// }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aplicacion == null) ? 0 : aplicacion.hashCode());
		result = prime * result + ((consumo == null) ? 0 : consumo.hashCode());
		result = prime * result + ((fecha == null) ? 0 : fecha.hashCode());
		result = prime * result + ((hora == null) ? 0 : hora.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Consumo other = (Consumo) obj;
		if (aplicacion == null) {
			if (other.aplicacion != null)
				return false;
		} else if (!aplicacion.equals(other.aplicacion))
			return false;
		if (consumo == null) {
			if (other.consumo != null)
				return false;
		} else if (!consumo.equals(other.consumo))
			return false;
		if (fecha == null) {
			if (other.fecha != null)
				return false;
		} else if (!fecha.equals(other.fecha))
			return false;
		if (hora == null) {
			if (other.hora != null)
				return false;
		} else if (!hora.equals(other.hora))
			return false;
		return true;
	}

}
