package com.myapps.detalle_navegacion.bean;

import com.google.gson.Gson;
import com.myapps.detalle_navegacion.bean.model.*;
import com.myapps.detalle_navegacion.business.ConsultaDnBL;
import com.myapps.detalle_navegacion.entity.DnConsulta;
import com.myapps.detalle_navegacion.servicio.ServicioDetalleNavegacion;
import com.myapps.detalle_navegacion.util.*;
import com.myapps.detalle_navegacion_ws.ws.*;
import com.myapps.user.bean.ControlerBitacora;
import com.myapps.user.ldap.DescriptorBitacora;
import com.myapps.vista_ccc.bean.ControlTelnet;
import com.myapps.vista_ccc.bean.ThreadTelnet;
import com.myapps.vista_ccc.business.*;
import com.myapps.vista_ccc.entity.*;
import com.myapps.vista_ccc.servicio.NodoServicio;
import com.myapps.vista_ccc.servicio.Servicio;
import com.myapps.vista_ccc.thread.ControlButton;
import com.myapps.vista_ccc.util.Parametros;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.apache.axis.AxisFault;
import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.ToggleSelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.rpc.ServiceException;
import java.io.*;
import java.rmi.RemoteException;
import java.util.*;
import java.util.Map.Entry;

@ManagedBean
@ViewScoped
public class DetallePorcentajeBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(DetallePorcentajeBean.class);

	// @Inject
	// private AplicacionBL aplicacionBl;
	// @Inject
	// private SubAplicacionBL subAplicacionBl;

	@Inject
	private ServidorBL servidorBL;

	@Inject
	private ConexionTelnetBL blConexionTelnet;

	@Inject
	private ConsultaRedBL blConsultaRed;

	@Inject
	private IncidenteBL blIncidente;

	@Inject
	private LogExcepcionBL blExcepcion;

	@Inject
	private ConsultaDnBL consultaBl;

	@Inject
	private ConsultaBL blConsulta;

	@Inject
	private LogAuditoriaBL blAuditoria;

	@Inject
	private ControlerBitacora controlerBitacora;

	@Inject
	private ExcepcionElementosRedBL ExcepcionBL;

	private List<Consumo> listConsumo;
	private List<PieConsumo> listPieConsumo;

	private String login;
	private String ip;
	private boolean disabledButton;

	private String isdn;
	private boolean porHora;
	private boolean porDia;
	private boolean porAplicacion;
	private boolean porSubAplicacion;

	private List<String> aplicaciones;
	private String[] selectedAplicaciones;
	private List<String> subAplicaciones;
	private String[] selectedSubAplicaciones;

	private Date fechaInicio;
	private Date fechaFin;
	private String pattern;

	private List<Pie3DDouble> listPies;
	private String dataJsonPorcentaje;
	private String dataJsonAnchoBanda;

	private String title;
	private Double totalConsumo;
	private int nroFilas;
	private double subTotalPorcentaje;
	private double subTotalConsumo;
	private int topSeleccionado;

	// ********************* DETALLE TRAFICO ***********************

	private List<Trafico> list;
	private List<Trafico> listFiltered;
	private List<Trafico> listSelected;
	private String tipoUnidad;

	private boolean disabledTotalTrafico;
	private boolean disabledDownTrafico;
	private boolean disabledUpTrafico;

	// ********************* DETALLE ANCHO BANDA *******************

	private List<Bar> listBar;
	private String barTiempos;
	private String titleY;
	private boolean visibleGraficoAnchoBanda;

	// ********************* DETALLE MB VS TIEMPO *******************

	private List<Bar> listBarTiempo;
	private String barTiemposMb;
	// private String titleYMb;
	private String dataJsonMbVsTiempo;

	private List<SelectItem> itemsHoras;
	private String selectHoraInicio;
	private String selectHoraFin;
	private boolean disabledComboInicio;
	private boolean disabledComboFin;

	// ************** INCIDENTE *******************
	private long CodigoAuditoria;
	private boolean disableIncidente;
	private Enum<?> vistaIncidente;

	private int cantidadDecimales;

	private ControlButton controlButtonTelnet;

	// ************* Reporte detalle - resumen
	private boolean exportarDetalle;
	private boolean exportarSinDetalle;

	@PostConstruct
	public void init() {
		iniciar();
	}

	public void dateChange(AjaxBehaviorEvent event) {
		// log.debug("fechaIni: " + fechaInicio + ", fechaFin: " + fechaFin);
	}

	public void inputChanged() {
		// log.debug("inputChanged .....");
	}

	public void iniciar() {
		log.debug("iniciando");
		try {
			Servicio.setPagina(this.getClass().getSimpleName());
			Servicio.eliminarConexionesInactivas(this.getClass().getSimpleName());

			cargarItemsHoras();

			topSeleccionado = -1;
			setDisabledButton(false);
			isdn = "";
			porHora = true;
			porDia = false;
			porAplicacion = true;
			porSubAplicacion = false;

			aplicaciones = new ArrayList<>();
			selectedAplicaciones = new String[0];
			subAplicaciones = new ArrayList<>();
			selectedSubAplicaciones = new String[0];

			listPies = new ArrayList<>();

			dataJsonPorcentaje = "";
			dataJsonAnchoBanda = "";
			dataJsonMbVsTiempo = "";

			pattern = "dd-MM-yyyy";

			Calendar ini = Calendar.getInstance();
			Calendar fin = Calendar.getInstance();
			ini.set(Calendar.MINUTE, 0);
			ini.set(Calendar.SECOND, 0);
			ini.set(Calendar.MILLISECOND, 0);
			fin.setTimeInMillis(ini.getTimeInMillis());
			// ini.add(Calendar.DATE, -1);
			ini.set(Calendar.HOUR_OF_DAY, 0);

			// ini.set(Calendar.DAY_OF_MONTH, 20);
			// ini.set(Calendar.MONTH, 0);

			fechaInicio = new Date(ini.getTimeInMillis());
			fechaFin = new Date(fin.getTimeInMillis());

			// log.debug("FI: " + UtilDate.dateToString(fechaInicio, "dd-MM-yyyy HH:mm:ss"));
			// log.debug("FF: " + UtilDate.dateToString(fechaFin, "dd-MM-yyyy HH:mm:ss"));

			selectHoraInicio = "00";
			selectHoraFin = UtilDate.dateToString(fechaFin, "HH");
			// log.debug("--------------- selectHoraFin: " + selectHoraFin);

			title = "tab1";
			disabledComboInicio = false;
			disabledComboFin = false;
			// cargarAplicaciones();
			// cargarSubAplicaciones();

			// *********************** DETALLE TRAFICO *********************
			// disabledTotalTrafico = ParametrosDn.visibleColumnaTotalTrafico;
			log.debug("Version Coverity 1.0");

			cantidadDecimales = (int) ParametrosDn.cantidadDecimales;
			disabledTotalTrafico = (boolean) ParametrosDn.visibleColumnaTotalTrafico;
			disabledDownTrafico = (boolean) ParametrosDn.visibleColumnaDownTrafico;
			disabledUpTrafico = (boolean) ParametrosDn.visibleColumnaUpTrafico;
			tipoUnidad = (String) ParametrosDn.unidad;
			visibleGraficoAnchoBanda = (boolean) ParametrosDn.visibleGraficoAnchoBanda;
			if (!(boolean) ParametrosDn.visibleUnidad) {
				tipoUnidad = "";
			}
			list = new ArrayList<>();

			// *********************** DETALLE ANCHO BANDA *****************

			titleY = "Total Bandwidth (" + ParametrosDn.velocidadTransferencia + "/s)";
			// titleY = "Total Bandwidth (" + parametroBl.getParametro(Constante.velocidadTransferencia) + "/s)";
			listBar = new ArrayList<>();
			barTiempos = "";

			// *********************** DETALLE ANCHO BANDA *****************

			// titleYMb = "MB";
			listBarTiempo = new ArrayList<>();
			barTiemposMb = "";

			totalConsumo = 0.0;
			CodigoAuditoria = 0;
			disableIncidente = true;
			vistaIncidente = null;
			controlButtonTelnet = new ControlButton(false, false, true);

			cargarIp();
			obtenerUsuario();

			// log.debug("--------------- fin selectHoraFin: " + selectHoraFin);

			exportarDetalle = false;
			exportarSinDetalle = true;
		} catch (Exception e) {
			log.error("Error al iniciar: ", e);
		}
	}

	public String doubleToString(double number) {
		return com.myapps.vista_ccc.util.UtilNumber.doubleToString(number, ParametrosDn.cantidadDecimales);
	}

	private void cargarItemsHoras() {
		try {
			setItemsHoras(new ArrayList<SelectItem>());
			// setItemsHoras.add(new SelectItem("-1", "Seleccione una Encuesta"));

			for (int i = 0; i <= 23; i++) {
				String label = i + "";
				if (i <= 9) {
					label = "0" + label;
				}
				// log.debug("i: " + i + ", label: " + label);
				SelectItem sel = new SelectItem(label, label);
				getItemsHoras().add(sel);
			}

		} catch (Exception e) {
			log.error("Error al cargar combo horas: ", e);
		}
	}

	// private void createPieModel(boolean visibleConsumoPorcentaje) {
	//
	// try {
	//
	// listPies = new ArrayList<Pie3DDouble>();
	// dataJsonPorcentaje = toGsonDouble(listPies);
	//
	// barTiempos = "";
	// dataJsonAnchoBanda = "[]";
	// dataJsonMbVsTiempo = "[]";
	//
	// if (title.equals("tab1")) {
	// callJavaScriptPorcentaje(visibleConsumoPorcentaje);
	// }
	// if (title.equals("tab3")) {
	// callJavaScriptAnchoBanda(nroDecimales);
	// }
	// if (title.equals("tab4")) {
	// callJavaScriptMbVsTiempo(nroDecimales);
	// }
	// } catch (Exception e) {
	// log.error("Error al crear pie model: ", e);
	// }
	// }

	public void buscar() {
		log.debug("title: " + title);
		if (title.equals("tab1")) {
			buscarDetallePorcentaje();
		}
		if (title.equals("tab2")) {
			buscarDetalleTrafico();
		}
		if (title.equals("tab3")) {
			buscarDetalleAnchoBanda();
		}
		if (title.equals("tab4")) {
			buscarDetalleMbVsTiempo();
		}
	}

	public void buscarDetallePorcentaje() {
		log.debug("ingresando a buscar detalle porcentaje");

		boolean continuar = true;
		int reintento = 0;
		long idAuditoria = 0;
		totalConsumo = 0.0;

		int reintentos = 0;
		int esperaReintentos = 0;
		int detalleNavegacionTimeOut = 0;
		String operador = "";
		int numero = 0;
		int nroDecimales = 0;
		String expresionRegularNroTigo = "";
		int cantidadDiasConsultaPorDia = 0;
		int cantidadDiasConsultaPorHora = 0;
		String wsdlLoc = "";
		boolean visibleConsumoPorcentaje;
		try {

			reintentos = (int) ParametrosDn.reintentosTimeOut;
			esperaReintentos = (int) ParametrosDn.esperaReintentosTimeOut;
			detalleNavegacionTimeOut = (int) ParametrosDn.detalleNavegacionTimeOut;
			operador = (String) ParametrosDn.operador;
			numero = (int) ParametrosDn.numero;
			nroDecimales = (int) ParametrosDn.cantidadDecimales;
			expresionRegularNroTigo = (String) ParametrosDn.expresionRegularNroTigo;
			cantidadDiasConsultaPorDia = (int) ParametrosDn.cantidadDiasConsultaPorDia;
			cantidadDiasConsultaPorHora = (int) ParametrosDn.cantidadDiasConsultaPorHora;
			wsdlLoc = (String) ParametrosDn.detalleNavegacionWsdl;
			visibleConsumoPorcentaje = (boolean) ParametrosDn.visibleConsumoPorcentaje;

		} catch (Exception e) {
			log.error("Error al cargar parametros del Sistema: ", e);
			SysMessage.error("Error al cargar parametros del Sistema: " + e.getMessage(), null);
			return;
		}

		while (continuar) {
			continuar = false;
			try {

				listPies = new ArrayList<>();

				if (isdn == null || isdn.trim().isEmpty()) {
					SysMessage.warn("El campo Nro de Cuenta no es válido", null);
					return;
				}
				if (!UtilNumber.esNroTigo(isdn, expresionRegularNroTigo)) {
					SysMessage.warn("Nro de cuenta no válido", null);
					return;
				}
				if (fechaInicio == null) {
					SysMessage.warn("El campo Fecha Inicio no es válido", null);
					return;
				}
				if (fechaFin == null) {
					SysMessage.warn("El campo Fecha Fin no es válido", null);
					return;
				}

				String tiempo = Constante.HORA;
				if (porDia) {
					tiempo = Constante.DIA;
				}

				String categoria = Constante.APLICACION;
				if (porSubAplicacion) {
					categoria = Constante.SUBAPLICACION;
				}

				Calendar fInicio = Calendar.getInstance();
				Calendar fFin = Calendar.getInstance();
				Calendar fActual = Calendar.getInstance();
				Calendar fHoy = Calendar.getInstance();

				fActual.set(Calendar.HOUR_OF_DAY, 0);
				fActual.set(Calendar.MINUTE, 0);
				fActual.set(Calendar.SECOND, 0);
				fActual.set(Calendar.MILLISECOND, 0);
				log.debug("fechaActual: " + UtilDate.dateToString(fActual.getTime(), "dd-MM-yyyy HH:mm:ss"));

				fInicio.setTimeInMillis(fechaInicio.getTime());
				fFin.setTimeInMillis(fechaFin.getTime());

				if (porHora) {
					fInicio.set(Calendar.HOUR_OF_DAY, Integer.parseInt(selectHoraInicio));
					fFin.set(Calendar.HOUR_OF_DAY, Integer.parseInt(selectHoraFin));
				}

				fHoy.set(Calendar.MINUTE, 0);
				fHoy.set(Calendar.SECOND, 0);
				fHoy.set(Calendar.MILLISECOND, 0);

				log.debug("fechaInicio: " + UtilDate.dateToString(fInicio.getTime(), "dd-MM-yyyy HH:mm:ss"));
				log.debug("fechaFin: " + UtilDate.dateToString(fFin.getTime(), "dd-MM-yyyy HH:mm:ss"));

				if (porDia) {
					fInicio.set(Calendar.HOUR_OF_DAY, 0);
					fInicio.set(Calendar.MINUTE, 0);
					fInicio.set(Calendar.SECOND, 0);
					fInicio.set(Calendar.MILLISECOND, 0);

					fFin.set(Calendar.HOUR_OF_DAY, 0);
					fFin.set(Calendar.MINUTE, 0);
					fFin.set(Calendar.SECOND, 0);
					fFin.set(Calendar.MILLISECOND, 0);

					fActual.add(Calendar.DAY_OF_MONTH, -(cantidadDiasConsultaPorDia - 1));
					fHoy.set(Calendar.HOUR_OF_DAY, 0);
				} else {
					fInicio.set(Calendar.MINUTE, 0);
					fInicio.set(Calendar.SECOND, 0);
					fInicio.set(Calendar.MILLISECOND, 0);

					fInicio.set(Calendar.MINUTE, 0);
					fInicio.set(Calendar.SECOND, 0);
					fInicio.set(Calendar.MILLISECOND, 0);

					fActual.add(Calendar.DAY_OF_MONTH, -(cantidadDiasConsultaPorHora - 1));
				}

				log.debug("fechaInicio: " + UtilDate.dateToString(fInicio.getTime(), "dd-MM-yyyy HH:mm:ss") + ", fechaFin: " + UtilDate.dateToString(fFin.getTime(), "dd-MM-yyyy HH:mm:ss"));
				log.debug("fechaActual restada: " + UtilDate.dateToString(fActual.getTime(), "dd-MM-yyyy HH:mm:ss"));
				log.debug("fechaHoy: " + UtilDate.dateToString(fHoy.getTime(), "dd-MM-yyyy HH:mm:ss"));

				if (fFin.getTimeInMillis() > fHoy.getTimeInMillis()) {
					SysMessage.warn("La Fecha Fin tiene que ser menor o igual que la Fecha Actual del Sistema", null);
					return;
				}

				if (fFin.getTimeInMillis() < fInicio.getTimeInMillis()) {
					SysMessage.warn("La Fecha Inicio tiene que ser menor o igual que la Fecha Fin", null);
					return;
				}

				if (fInicio.getTimeInMillis() < fActual.getTimeInMillis()) {
					if (porHora) {
						SysMessage.warn("Solo se puede consultar de los ultimos " + cantidadDiasConsultaPorHora + " dias", null);
					} else {
						SysMessage.warn("Solo se puede consultar de los ultimos " + cantidadDiasConsultaPorDia + " dias", null);
					}
					return;
				}

				// setDisabledButton(true);

				if (idAuditoria == 0) {
					List<VcLogAdicional> adicionales = new ArrayList<>();
					VcLogAdicional ad1 = new VcLogAdicional();
					ad1.setCodigo("Fecha Inicio");
					ad1.setValor(UtilDate.dateToString(fInicio.getTime(), Parametros.fechaFormatPage));
					VcLogAdicional ad2 = new VcLogAdicional();
					ad2.setCodigo("Fecha Fin");
					ad2.setValor(UtilDate.dateToString(fFin.getTime(), Parametros.fechaFormatPage));
					VcLogAdicional ad3 = new VcLogAdicional();
					ad3.setCodigo("Periodo");
					ad3.setValor(tiempo);
					VcLogAdicional ad4 = new VcLogAdicional();
					ad4.setCodigo("NIVEL DETALLE");
					ad4.setValor(categoria);
					adicionales.add(ad1);
					adicionales.add(ad2);
					adicionales.add(ad3);
					adicionales.add(ad4);

					idAuditoria = guardarAuditoria(Calendar.getInstance(), this.getLogin(), this.ip, Parametros.vistaDetalleNavegacionPorcentajeConsumo, Parametros.comandoDetalleNavegacionPorcentajeConsumo, this.isdn, adicionales);

					if (idAuditoria <= 0) {
						SysMessage.error("Error al guardar log de auditoria", null);
						return;
					}
				}

				disableIncidente = false;
				vistaIncidente = DescriptorBitacora.DETALLE_NAVEGACION_PORCENTAJE;

				try {
					controlerBitacora.accion(DescriptorBitacora.DETALLE_NAVEGACION_PORCENTAJE,
							"Se consultó detalle de navegacion isdn: " + isdn + ", tiempo: " + tiempo + ", categoria: " + categoria + ", fechaInicio: " + UtilDate.dateToString(fInicio.getTime(), "dd-MM-yyyy HH:mm:ss") + ", fechaFin: " + UtilDate.dateToString(fFin.getTime(), "dd-MM-yyyy HH:mm:ss"));
				} catch (Exception e) {
					log.error("Error al guardar bitacora en el sistema: ", e);
					SysMessage.error("Error al guardar bitacora en el sistema", null);
				}

				long ini = System.currentTimeMillis();

				NodoServicio<DetalleNavegacionServiceSoapBindingStub> portNodo = ServicioDetalleNavegacion.initPort(wsdlLoc, "DETALLE_NAVEGACION_" + getLogin(), detalleNavegacionTimeOut);

				if (portNodo != null && portNodo.getPort() != null) {

					synchronized (portNodo) {

						try {
							DetalleNavegacionServiceSoapBindingStub port = portNodo.getPort();

							ResponsePorcentaje response = port.getDetalleNavegacionPorcentaje(isdn, fInicio, fFin, tiempo, categoria);
							long fin = System.currentTimeMillis();
							log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio DetalleNavegacion: " + (fin - ini) + " milisegundos");

							Long Con = null;
							if (portNodo.isPrimeraVez()) {
								Con = portNodo.getTiempoConexion();
							}

							if (Parametros.saveRequestResponse) {
								try {
									saveXml(isdn, getLogin(), wsdlLoc, "getDetalleNavegacionPorcentaje", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoria, Con, (fin - ini));
								} catch (Exception e) {
									log.warn("No se logro registrar los request y response del servicio DetalleNavegacion, getDetalleNavegacionPorcentaje: ", e);
								}
							}

							if (response != null) {
								if (response.getCode() != null) {

									log.debug("[isdn: " + getIsdn() + "] codigo: " + response.getCode() + ", descripcion: " + response.getDescription());

									if (response.getCode().intValue() == 0) {
										Porcentaje[] lista = response.getLista();

										if (lista != null) {
											double totalReporte = 0;
											long totalBytes = 0;
											// int contador = 1;

											for (Porcentaje item : lista) {

												// if (contador > ParametrosDn.topAplicacionesReporte) {
												// break;
												// }
												// contador++;

												long total = item.getTotal();
												totalBytes = totalBytes + total;

												ScriptEngineManager manager = new ScriptEngineManager();
												ScriptEngine se = manager.getEngineByName("JavaScript");

												if (se != null) {
													// Object result = se.eval("3.2 * 5.5");
													Object resultTotal = se.eval(total + operador + numero);
													double parseTotal = Double.parseDouble(String.valueOf(resultTotal));
													parseTotal = UtilNumber.redondear(parseTotal, nroDecimales);
													// totalConsumo = totalConsumo + parseTotal;

													// if (item.getPorcentaje() > 0) {
													totalReporte = totalReporte + parseTotal;
													listPies.add(new Pie3DDouble(item.getNombre(), 0.0, parseTotal));
													// }
												} else {
													log.error("ScriptEngine resuelto a nulo");
												}
											}
											for (Pie3DDouble item : listPies) {
												double porcentaje = ((double) item.getM() * (double) 100) / totalReporte;
												item.setY(UtilNumber.redondear(porcentaje, nroDecimales));

											}
											ScriptEngineManager manager = new ScriptEngineManager();
											ScriptEngine se = manager.getEngineByName("JavaScript");

											if (se != null) {
												// log.debug("totalBytes: " + totalBytes);
												Object resultTotal = se.eval(totalBytes + operador + numero);
												totalConsumo = Double.parseDouble(String.valueOf(resultTotal));
											}
											totalConsumo = UtilNumber.redondear(totalConsumo, nroDecimales);
											// totalConsumo = UtilNumber.redondear(totalConsumo, nroDecimales);
											log.debug("PORCENTAJE totalConsumo: " + totalConsumo);
										} else {
											log.warn("[isdn: " + getIsdn() + "] lista resuelto a nulo");
										}
									} else {
										SysMessage.warn("codigo: " + response.getCode() + ", descripcion: " + response.getDescription(), null);
									}
								} else {
									log.warn("[isdn: " + getIsdn() + "] response.getCode resuelto a nulo");
								}
							} else {
								log.warn("[isdn: " + getIsdn() + "] response resuelto a nulo");
							}
						} finally {
							portNodo.setEnUso(false);
							portNodo.setFechaFin(new Date());
						}
					}
				} else {
					log.error("[isdn: " + getIsdn() + "] Port DetalleNavegacionresuelto a nulo");
				}

				try {
					DnConsulta dc = new DnConsulta();
					dc.setIp(ip);
					dc.setUsuario(getLogin());
					dc.setIsdn(isdn);
					dc.setTiempo(tiempo);
					dc.setCategoria(categoria);
					dc.setReporte("DETALLE NAVEGACION POR PORCENTAJE");
					dc.setFechaInicio(fInicio.getTime());
					dc.setFechaFin(fFin.getTime());
					Calendar cal = Calendar.getInstance();
					cal.set(Calendar.HOUR_OF_DAY, 0);
					cal.set(Calendar.MINUTE, 0);
					cal.set(Calendar.SECOND, 0);
					cal.set(Calendar.MILLISECOND, 0);
					dc.setFecha(cal.getTime());
					dc.setFechaCreacion(new Date());
					consultaBl.save(dc);
				} catch (Exception e) {
					log.error("Error al guardar consulta: ", e);
				}

				SysMessage.info("Consulta finalizada.", null);

				// ***********************************

				// listPies = new ArrayList<>();
				// for (int i = 1; i <= 500; i++) {
				// listPies.add(new Pie3DDouble("NAME" + i, (double)i, (double) i));
				// }

				if (listPies.size() > 0) {
					dataJsonPorcentaje = toGsonDouble(listPies);
					callJavaScriptPorcentaje(visibleConsumoPorcentaje);
				} else {

					// createPieModel(visibleConsumoPorcentaje);
					listPies = new ArrayList<Pie3DDouble>();
					dataJsonPorcentaje = toGsonDouble(listPies);
					callJavaScriptPorcentaje(visibleConsumoPorcentaje);
				}

			} catch (AxisFault a) {
				String msg = a.getMessage();
				msg.toLowerCase();

				if (reintento < reintentos && a.getCause() != null && a.getCause().getLocalizedMessage().trim().equals("Read timed out")) {
					// GotoFactory.getSharedInstance().getGoto().go(4);
					log.debug("Reintento de conexion por time out nro: " + (reintento + 1));
					reintento++;
					continuar = true;
					try {
						Thread.sleep(esperaReintentos);
					} catch (Exception e) {
						e.getMessage();
					}
				} else {
					guardarExcepcion(idAuditoria, "Error AxisFault de conexion al servicio DetalleNavegacion: " + stackTraceToString(a));
					if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase()) || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase()) || msg.contains("UnknownHostException".toLowerCase())
							|| msg.contains("No route to host: connect".toLowerCase())) {
						log.error("Error de conexion al servicio DetalleNavegacion: ", a);
						SysMessage.error("Error de conexion del servicio DetalleNavegacion", null);
					} else {
						log.error("Error AxisFault: ", a);
						SysMessage.error("Error de conexion del servicio DetalleNavegacion: " + a.getMessage(), null);
					}
				}

			} catch (ServiceException e) {
				log.error("Error de servicio al conectarse al servicio DetalleNavegacion: ", e);
				guardarExcepcion(idAuditoria, "Error de servicio al conectarse al servicio DetalleNavegacion: " + stackTraceToString(e));
				SysMessage.error("Error de servicio al conectarse al servicio DetalleNavegacion", null);
			} catch (RemoteException e) {
				log.error("Error remoto al conectarse al servicio DetalleNavegacion: ", e);
				guardarExcepcion(idAuditoria, "Error remoto al conectarse al servicio DetalleNavegacion: " + stackTraceToString(e));
				SysMessage.error("Error remoto al conectarse al servicio DetalleNavegacion", null);
			} catch (Exception e) {
				log.error("Error al consultar detalle de navegacion", e);
				guardarExcepcion(idAuditoria, "Error al consultar detalle de navegacion: " + stackTraceToString(e));
				SysMessage.error("Error al consultar detalle de navegacion: " + e.getLocalizedMessage(), null);
			}
		}
	}

	public void buscarDetalleTrafico() {
		log.debug("ingresando a buscar detalle trafico isdn: " + isdn);

		boolean continuar = true;
		int reintento = 0;
		long idAuditoria = 0;
		this.totalConsumo = 0.0;

		list = new ArrayList<>();
		listSelected = new ArrayList<>();
		nroFilas = 0;
		subTotalConsumo = 0;
		subTotalPorcentaje = 0;
		topSeleccionado = -1;

		int reintentos = 0;
		int esperaReintentos = 0;
		int detalleNavegacionTimeOut = 0;
		String operador = "";
		int numero = 0;
		int nroDecimales = 0;
		String expresionRegularNroTigo = "";
		int cantidadDiasConsultaPorDia = 0;
		int cantidadDiasConsultaPorHora = 0;
		String wsdlLoc = "";

		try {

			reintentos = (int) ParametrosDn.reintentosTimeOut;
			esperaReintentos = (int) ParametrosDn.esperaReintentosTimeOut;
			detalleNavegacionTimeOut = (int) ParametrosDn.detalleNavegacionTimeOut;
			operador = (String) ParametrosDn.operador;
			numero = (int) ParametrosDn.numero;
			nroDecimales = (int) ParametrosDn.cantidadDecimales;
			expresionRegularNroTigo = (String) ParametrosDn.expresionRegularNroTigo;
			cantidadDiasConsultaPorDia = (int) ParametrosDn.cantidadDiasConsultaPorDia;
			cantidadDiasConsultaPorHora = (int) ParametrosDn.cantidadDiasConsultaPorHora;
			wsdlLoc = (String) ParametrosDn.detalleNavegacionWsdl;

		} catch (Exception e) {
			log.error("Error al cargar parametros del Sistema: ", e);
			SysMessage.error("Error al cargar parametros del Sistema: " + e.getMessage(), null);
			return;
		}

		while (continuar) {
			continuar = false;
			try {

				list = new ArrayList<>();

				if (isdn == null || isdn.trim().isEmpty()) {
					log.debug("isdn nulo o vacio");
					SysMessage.warn("El campo Nro de Cuenta no es válido", null);
					return;
				}

				if (!UtilNumber.esNroTigo(isdn, expresionRegularNroTigo)) {
					log.debug("isdn no valido");
					SysMessage.warn("Nro de cuenta no válido", null);
					return;
				}
				if (fechaInicio == null) {
					SysMessage.warn("El campo Fecha Inicio no es válido", null);
					return;
				}
				if (fechaFin == null) {
					SysMessage.warn("El campo Fecha Fin no es válido", null);
					return;
				}

				Calendar fInicio = Calendar.getInstance();
				Calendar fFin = Calendar.getInstance();
				Calendar fActual = Calendar.getInstance();
				Calendar fHoy = Calendar.getInstance();

				fActual.set(Calendar.HOUR_OF_DAY, 0);
				fActual.set(Calendar.MINUTE, 0);
				fActual.set(Calendar.SECOND, 0);
				fActual.set(Calendar.MILLISECOND, 0);
				log.debug("fechaActual: " + UtilDate.dateToString(fActual.getTime(), "dd-MM-yyyy HH:mm:ss"));

				fInicio.setTimeInMillis(fechaInicio.getTime());
				fFin.setTimeInMillis(fechaFin.getTime());

				if (porHora) {
					fInicio.set(Calendar.HOUR_OF_DAY, Integer.parseInt(selectHoraInicio));
					fFin.set(Calendar.HOUR_OF_DAY, Integer.parseInt(selectHoraFin));
				}

				fHoy.set(Calendar.MINUTE, 0);
				fHoy.set(Calendar.SECOND, 0);
				fHoy.set(Calendar.MILLISECOND, 0);

				if (porDia) {
					fInicio.set(Calendar.HOUR_OF_DAY, 0);
					fInicio.set(Calendar.MINUTE, 0);
					fInicio.set(Calendar.SECOND, 0);
					fInicio.set(Calendar.MILLISECOND, 0);

					fFin.set(Calendar.HOUR_OF_DAY, 0);
					fFin.set(Calendar.MINUTE, 0);
					fFin.set(Calendar.SECOND, 0);
					fFin.set(Calendar.MILLISECOND, 0);

					fActual.add(Calendar.DAY_OF_MONTH, -(cantidadDiasConsultaPorDia - 1));
					fHoy.set(Calendar.HOUR_OF_DAY, 0);
				} else {
					fInicio.set(Calendar.MINUTE, 0);
					fInicio.set(Calendar.SECOND, 0);
					fInicio.set(Calendar.MILLISECOND, 0);

					fInicio.set(Calendar.MINUTE, 0);
					fInicio.set(Calendar.SECOND, 0);
					fInicio.set(Calendar.MILLISECOND, 0);

					fActual.add(Calendar.DAY_OF_MONTH, -(cantidadDiasConsultaPorHora - 1));
				}

				log.debug("fechaInicio: " + UtilDate.dateToString(fInicio.getTime(), "dd-MM-yyyy HH:mm:ss") + ", fechaFin: " + UtilDate.dateToString(fFin.getTime(), "dd-MM-yyyy HH:mm:ss"));
				log.debug("fechaActual restada: " + UtilDate.dateToString(fActual.getTime(), "dd-MM-yyyy HH:mm:ss"));
				log.debug("fechaHoy: " + UtilDate.dateToString(fHoy.getTime(), "dd-MM-yyyy HH:mm:ss"));

				if (fFin.getTimeInMillis() > fHoy.getTimeInMillis()) {
					SysMessage.warn("La Fecha Fin tiene que ser menor o igual que la Fecha Actual del Sistema", null);
					return;
				}

				if (fFin.getTimeInMillis() < fInicio.getTimeInMillis()) {
					SysMessage.warn("La Fecha Inicio tiene que ser menor o igual que la Fecha Fin", null);
					return;
				}

				if (fInicio.getTimeInMillis() < fActual.getTimeInMillis()) {
					if (porHora) {
						SysMessage.warn("Solo se puede consultar de los ultimos " + cantidadDiasConsultaPorHora + " dias", null);
					} else {
						SysMessage.warn("Solo se puede consultar de los ultimos " + cantidadDiasConsultaPorDia + " dias", null);
					}
					return;
				}

				// setDisabledButton(true);

				String tiempo = Constante.HORA;
				if (porDia) {
					tiempo = Constante.DIA;
				}

				String categoria = Constante.APLICACION;
				if (porSubAplicacion) {
					categoria = Constante.SUBAPLICACION;
				}

				if (idAuditoria == 0) {
					List<VcLogAdicional> adicionales = new ArrayList<>();
					VcLogAdicional ad1 = new VcLogAdicional();
					ad1.setCodigo("Fecha Inicio");
					ad1.setValor(UtilDate.dateToString(fInicio.getTime(), Parametros.fechaFormatPage));
					VcLogAdicional ad2 = new VcLogAdicional();
					ad2.setCodigo("Fecha Fin");
					ad2.setValor(UtilDate.dateToString(fFin.getTime(), Parametros.fechaFormatPage));
					VcLogAdicional ad3 = new VcLogAdicional();
					ad3.setCodigo("Periodo");
					ad3.setValor(tiempo);
					VcLogAdicional ad4 = new VcLogAdicional();
					ad4.setCodigo("NIVEL DETALLE");
					ad4.setValor(categoria);
					adicionales.add(ad1);
					adicionales.add(ad2);
					adicionales.add(ad3);
					adicionales.add(ad4);

					idAuditoria = guardarAuditoria(Calendar.getInstance(), this.getLogin(), this.ip, Parametros.vistaDetalleNavegacionTablaConsumo, Parametros.comandoDetalleNavegacionTablaConsumo, this.isdn, adicionales);

					if (idAuditoria <= 0) {
						SysMessage.error("Error al guardar log de auditoria", null);
						return;
					}
				}

				disableIncidente = false;
				vistaIncidente = DescriptorBitacora.DETALLE_NAVEGACION_TRAFICO;

				try {
					controlerBitacora.accion(DescriptorBitacora.DETALLE_NAVEGACION_TRAFICO,
							"Se consultó detalle de navegacion isdn: " + isdn + ", tiempo: " + tiempo + ", categoria: " + categoria + ", fechaInicio: " + UtilDate.dateToString(fInicio.getTime(), "dd-MM-yyyy HH:mm:ss") + ", fechaFin: " + UtilDate.dateToString(fFin.getTime(), "dd-MM-yyyy HH:mm:ss"));
				} catch (Exception e) {
					log.error("Error al guardar bitacora en el sistema: ", e);
					SysMessage.error("Error al guardar bitacora en el sistema", null);
				}

				long ini = System.currentTimeMillis();

				// URL url = new URL(UtilUrl.getIp(wsdlLoc));
				// if (url.getProtocol().equalsIgnoreCase("https")) {
				// int puerto = url.getPort();
				// String host = url.getHost();
				// if (host.equalsIgnoreCase("localhost")) {
				// host = "127.0.0.1";
				// }
				// validarCertificado(Parametros.detalleNavegacionPathKeystore, host + ":" + puerto);
				// }

				// DetalleNavegacionServiceSoapBindingStub port = ServicioDetalleNavegacion.initPort(wsdlLoc, "DETALLE_NAVEGACION_" + login, ParametrosDn.detalleNavegacionTimeOut);
				NodoServicio<DetalleNavegacionServiceSoapBindingStub> portNodo = ServicioDetalleNavegacion.initPort(wsdlLoc, "DETALLE_NAVEGACION_" + getLogin(), detalleNavegacionTimeOut);

				if (portNodo != null && portNodo.getPort() != null) {

					synchronized (portNodo) {

						try {
							DetalleNavegacionServiceSoapBindingStub port = portNodo.getPort();

							ResponseDistribucion response = port.getDetalleNavegacionDistribucion(isdn, fInicio, fFin, tiempo, categoria);

							long fin = System.currentTimeMillis();
							log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio DetalleNavegacion: " + (fin - ini) + " milisegundos");

							Long Con = null;
							if (portNodo.isPrimeraVez()) {
								Con = portNodo.getTiempoConexion();
							}

							if (Parametros.saveRequestResponse) {
								try {
									saveXml(isdn, getLogin(), wsdlLoc, "getDetalleNavegacionDistribucion", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoria, Con, (fin - ini));
								} catch (Exception e) {
									log.warn("No se logro registrar los request y response del servicio DetalleNavegacion, getDetalleNavegacionDistribucion: ", e);
								}
							}

							if (response != null) {
								if (response.getCode() != null) {

									log.debug("[isdn: " + getIsdn() + "] codigo: " + response.getCode() + ", descripcion: " + response.getDescription());

									if (response.getCode().intValue() == 0) {
										Distribucion[] lista = response.getLista();

										if (lista != null) {

											double totalReporte = 0;
											long totalBytes = 0;
											int contador = 1;

											for (Distribucion item : lista) {

												// if (contador > ParametrosDn.topAplicacionesReporte) {
												// break;
												// }
												contador++;

												long total = item.getTotal();
												long download = item.getDownload();
												long upload = item.getUpload();
												totalBytes = totalBytes + total;

												ScriptEngineManager manager = new ScriptEngineManager();
												ScriptEngine se = manager.getEngineByName("JavaScript");

												if (se != null) {
													// Object result = se.eval("3.2 * 5.5");
													Object resultTotal = se.eval(total + operador + numero);
													Object resultDownload = se.eval(download + operador + numero);
													Object resultUpload = se.eval(upload + operador + numero);

													double parseTotal = Double.parseDouble(String.valueOf(resultTotal));
													double parseDownload = Double.parseDouble(String.valueOf(resultDownload));
													double parseUpload = Double.parseDouble(String.valueOf(resultUpload));

													totalConsumo = totalConsumo + UtilNumber.redondear(parseTotal, nroDecimales);
													totalReporte = totalReporte + UtilNumber.redondear(parseTotal, nroDecimales);

													// Trafico pie = new Trafico((int) item.getNro(), item.getNombre(), "", UtilNumber.doubleToString(parseTotal, nroDecimales),
													// UtilNumber.doubleToString(parseDownload, nroDecimales), UtilNumber.doubleToString(parseUpload, nroDecimales));
													// Trafico pie = new Trafico((int) item.getNro(), item.getNombre(), 0, UtilNumber.redondear(parseTotal, nroDecimales),
													// UtilNumber.redondear(parseDownload, nroDecimales), UtilNumber.redondear(parseUpload, nroDecimales));
													Trafico pie = new Trafico((contador - 1), item.getNombre(), 0, UtilNumber.redondear(parseTotal, nroDecimales), UtilNumber.redondear(parseDownload, nroDecimales), UtilNumber.redondear(parseUpload, nroDecimales));
													list.add(pie);
												} else {
													log.error("ScriptEngine resuelto a nulo");
												}
												// // Object result = se.eval("3.2 * 5.5");
												// Object resultTotal = se.eval(total + operador + numero);
												// Object resultDownload = se.eval(download + operador + numero);
												// Object resultUpload = se.eval(upload + operador + numero);
												//
												// double parseTotal = Double.parseDouble(String.valueOf(resultTotal));
												// double parseDownload = Double.parseDouble(String.valueOf(resultDownload));
												// double parseUpload = Double.parseDouble(String.valueOf(resultUpload));
												//
												// Trafico pie = new Trafico(item.getNro() + "", item.getNombre(), item.getPorcentaje() + "%", UtilNumber.doubleToString(parseTotal, nroDecimales),
												// UtilNumber.doubleToString(parseDownload, nroDecimales), UtilNumber.doubleToString(parseUpload, nroDecimales));
												// list.add(pie);
											}

											for (Trafico item : list) {
												// double porcentaje = (Double.parseDouble(item.getTotalTrafico().replace(",", ".")) * (double) 100) / totalReporte;
												double porcentaje = (item.getTotalTrafico() * (double) 100) / totalReporte;
												porcentaje = UtilNumber.redondear(porcentaje, nroDecimales);
												// item.setPorcentajeTotalTrafico(UtilNumber.doubleToString(porcentaje, nroDecimales));
												// item.setPorcentajeTotalTrafico(UtilNumber.redondear(porcentaje, nroDecimales));
												item.setPorcentajeTotalTrafico(porcentaje);

												if (Double.isNaN(item.getPorcentajeTotalTrafico()) || Double.isInfinite(item.getPorcentajeTotalTrafico())) {
													item.setPorcentajeTotalTrafico(0);
												}
												if (Double.isNaN(item.getTotalTrafico()) || Double.isInfinite(item.getTotalTrafico())) {
													item.setTotalTrafico(0);
												}
												if (Double.isNaN(item.getDownTrafico()) || Double.isInfinite(item.getDownTrafico())) {
													item.setDownTrafico(0);
												}
												if (Double.isNaN(item.getUpTrafico()) || Double.isInfinite(item.getUpTrafico())) {
													item.setUpTrafico(0);
												}
											}

											ScriptEngineManager manager = new ScriptEngineManager();
											ScriptEngine se = manager.getEngineByName("JavaScript");

											if (se != null) {
												// log.debug("totalBytes: " + totalBytes);
												Object resultTotal = se.eval(totalBytes + operador + numero);
												totalConsumo = Double.parseDouble(String.valueOf(resultTotal));
											}
											totalConsumo = UtilNumber.redondear(totalConsumo, nroDecimales);
											log.debug("TABLA CONSUMO totalConsumo: " + totalConsumo);
										} else {
											log.warn("[isdn: " + getIsdn() + "] lista resuelto a nulo");
										}
									} else {
										SysMessage.warn("codigo: " + response.getCode() + ", descripcion: " + response.getDescription(), null);
									}
								} else {
									log.warn("[isdn: " + getIsdn() + "] response.getCode resuelto a nulo");
								}
							} else {
								log.warn("[isdn: " + getIsdn() + "] response resuelto a nulo");
							}
							// try {
							// saveXml(isdn, getLogin(), wsdlLoc, "queryCustomerInfo", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(),
							// port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoria);
							// } catch (Exception e) {
							// log.warn("No se logro registrar los request y response del servicio: ", e);
							// }

						} finally {
							portNodo.setEnUso(false);
							portNodo.setFechaFin(new Date());
						}
					}
				} else {
					log.error("[isdn: " + getIsdn() + "] Port DetalleNavegacionresuelto a nulo");
				}

				try {
					DnConsulta dc = new DnConsulta();
					dc.setIp(ip);
					dc.setUsuario(getLogin());
					dc.setIsdn(isdn);
					dc.setTiempo(tiempo);
					dc.setCategoria(categoria);
					dc.setReporte("DETALLE NAVEGACION POR TRAFICO");
					dc.setFechaInicio(fInicio.getTime());
					dc.setFechaFin(fFin.getTime());
					Calendar cal = Calendar.getInstance();
					cal.set(Calendar.HOUR_OF_DAY, 0);
					cal.set(Calendar.MINUTE, 0);
					cal.set(Calendar.SECOND, 0);
					cal.set(Calendar.MILLISECOND, 0);
					dc.setFecha(cal.getTime());
					dc.setFechaCreacion(new Date());
					consultaBl.save(dc);
				} catch (Exception e) {
					log.error("Error al guardar consulta: ", e);
				}

				SysMessage.info("Consulta finalizada.", null);

			} catch (AxisFault a) {
				String msg = a.getMessage();
				msg.toLowerCase();

				if (reintento < reintentos && a.getCause() != null && a.getCause().getLocalizedMessage().trim().equals("Read timed out")) {
					// GotoFactory.getSharedInstance().getGoto().go(4);
					log.debug("Reintento de conexion por time out nro: " + (reintento + 1));
					reintento++;
					continuar = true;
					try {
						Thread.sleep(esperaReintentos);
					} catch (Exception e) {
						e.getMessage();
					}
				} else {
					guardarExcepcion(idAuditoria, "Error AxisFault de conexion al servicio DetalleNavegacion: " + stackTraceToString(a));
					if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase()) || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase()) || msg.contains("UnknownHostException".toLowerCase())
							|| msg.contains("No route to host: connect".toLowerCase())) {
						log.error("Error de conexion al servicio DetalleNavegacion: ", a);
						SysMessage.error("Error de conexion del servicio DetalleNavegacion", null);
					} else {
						log.error("Error AxisFault: ", a);
						SysMessage.error("Error de conexion del servicio DetalleNavegacion: " + a.getMessage(), null);
					}
				}
			} catch (ServiceException e) {
				log.error("Error de servicio al conectarse al servicio DetalleNavegacion: ", e);
				guardarExcepcion(idAuditoria, "Error de servicio al conectarse al servicio DetalleNavegacion: " + stackTraceToString(e));
				SysMessage.error("Error de servicio al conectarse al servicio DetalleNavegacion", null);
			} catch (RemoteException e) {
				log.error("Error remoto al conectarse al servicio DetalleNavegacion: ", e);
				guardarExcepcion(idAuditoria, "Error remoto al conectarse al servicio DetalleNavegacion: " + stackTraceToString(e));
				SysMessage.error("Error remoto al conectarse al servicio DetalleNavegacion", null);
			} catch (Exception e) {
				log.error("Error al consultar detalle de navegacion", e);
				guardarExcepcion(idAuditoria, "Error al consultar detalle de navegacion: " + stackTraceToString(e));
				SysMessage.error("Error al consultar detalle de navegacion: " + e.getLocalizedMessage(), null);
			}
		}
	}

	public void buscarDetalleAnchoBanda() {
		log.debug("ingresando a buscar detalle ancho de banda");

		// COMENTAR
		// String[] listTiemposTest = null;
		boolean continuar = true;
		int reintento = 0;
		long idAuditoria = 0;
		totalConsumo = 0.0;

		int reintentos = 0;
		int esperaReintentos = 0;
		int detalleNavegacionTimeOut = 0;
		String operador = "";
		int numero = 0;
		int nroDecimales = 0;
		String expresionRegularNroTigo = "";
		int cantidadDiasConsultaPorDia = 0;
		int cantidadDiasConsultaPorHora = 0;
		String wsdlLoc = "";
		String velocidadTransferencia = "";
		// boolean visibleConsumoPorcentaje;

		try {

			reintentos = (int) ParametrosDn.reintentosTimeOut;
			esperaReintentos = (int) ParametrosDn.esperaReintentosTimeOut;
			detalleNavegacionTimeOut = (int) ParametrosDn.detalleNavegacionTimeOut;
			operador = (String) ParametrosDn.operador;
			numero = (int) ParametrosDn.numero;
			nroDecimales = (int) ParametrosDn.cantidadDecimales;
			expresionRegularNroTigo = (String) ParametrosDn.expresionRegularNroTigo;
			cantidadDiasConsultaPorDia = (int) ParametrosDn.cantidadDiasConsultaPorDia;
			cantidadDiasConsultaPorHora = (int) ParametrosDn.cantidadDiasConsultaPorHora;
			wsdlLoc = (String) ParametrosDn.detalleNavegacionWsdl;
			velocidadTransferencia = (String) ParametrosDn.velocidadTransferencia;

		} catch (Exception e) {
			log.error("Error al cargar parametros del Sistema: ", e);
			SysMessage.error("Error al cargar parametros del Sistema: " + e.getMessage(), null);
			return;
		}

		while (continuar) {
			continuar = false;
			try {

				listBar = new ArrayList<Bar>();
				barTiempos = "";

				if (isdn == null || isdn.trim().isEmpty()) {
					SysMessage.warn("El campo Nro de Cuenta no es válido", null);
					return;
				}
				if (!UtilNumber.esNroTigo(isdn, expresionRegularNroTigo)) {
					SysMessage.warn("Nro de cuenta no válido", null);
					return;
				}
				if (fechaInicio == null) {
					SysMessage.warn("El campo Fecha Inicio no es válido", null);
					return;
				}
				if (fechaFin == null) {
					SysMessage.warn("El campo Fecha Fin no es válido", null);
					return;
				}

				Calendar fInicio = Calendar.getInstance();
				Calendar fFin = Calendar.getInstance();
				Calendar fActual = Calendar.getInstance();
				Calendar fHoy = Calendar.getInstance();

				fActual.set(Calendar.HOUR_OF_DAY, 0);
				fActual.set(Calendar.MINUTE, 0);
				fActual.set(Calendar.SECOND, 0);
				fActual.set(Calendar.MILLISECOND, 0);
				log.debug("fechaActual: " + UtilDate.dateToString(fActual.getTime(), "dd-MM-yyyy HH:mm:ss"));

				fInicio.setTimeInMillis(fechaInicio.getTime());
				fFin.setTimeInMillis(fechaFin.getTime());

				if (porHora) {
					fInicio.set(Calendar.HOUR_OF_DAY, Integer.parseInt(selectHoraInicio));
					fFin.set(Calendar.HOUR_OF_DAY, Integer.parseInt(selectHoraFin));
				}

				fHoy.set(Calendar.MINUTE, 0);
				fHoy.set(Calendar.SECOND, 0);
				fHoy.set(Calendar.MILLISECOND, 0);

				if (porDia) {
					fInicio.set(Calendar.HOUR_OF_DAY, 0);
					fInicio.set(Calendar.MINUTE, 0);
					fInicio.set(Calendar.SECOND, 0);
					fInicio.set(Calendar.MILLISECOND, 0);

					fFin.set(Calendar.HOUR_OF_DAY, 0);
					fFin.set(Calendar.MINUTE, 0);
					fFin.set(Calendar.SECOND, 0);
					fFin.set(Calendar.MILLISECOND, 0);

					fActual.add(Calendar.DAY_OF_MONTH, -(cantidadDiasConsultaPorDia - 1));
					fHoy.set(Calendar.HOUR_OF_DAY, 0);
				} else {
					fInicio.set(Calendar.MINUTE, 0);
					fInicio.set(Calendar.SECOND, 0);
					fInicio.set(Calendar.MILLISECOND, 0);

					fInicio.set(Calendar.MINUTE, 0);
					fInicio.set(Calendar.SECOND, 0);
					fInicio.set(Calendar.MILLISECOND, 0);

					fActual.add(Calendar.DAY_OF_MONTH, -(cantidadDiasConsultaPorHora - 1));
				}

				log.debug("fechaInicio: " + UtilDate.dateToString(fInicio.getTime(), "dd-MM-yyyy HH:mm:ss") + ", fechaFin: " + UtilDate.dateToString(fFin.getTime(), "dd-MM-yyyy HH:mm:ss"));
				log.debug("fechaActual restada: " + UtilDate.dateToString(fActual.getTime(), "dd-MM-yyyy HH:mm:ss"));
				log.debug("fechaHoy: " + UtilDate.dateToString(fHoy.getTime(), "dd-MM-yyyy HH:mm:ss"));

				if (fFin.getTimeInMillis() > fHoy.getTimeInMillis()) {
					SysMessage.warn("La Fecha Fin tiene que ser menor o igual que la Fecha Actual del Sistema", null);
					return;
				}

				if (fFin.getTimeInMillis() < fInicio.getTimeInMillis()) {
					SysMessage.warn("La Fecha Inicio tiene que ser menor o igual que la Fecha Fin", null);
					return;
				}

				if (fInicio.getTimeInMillis() < fActual.getTimeInMillis()) {
					if (porHora) {
						SysMessage.warn("Solo se puede consultar de los ultimos " + cantidadDiasConsultaPorHora + " dias", null);
					} else {
						SysMessage.warn("Solo se puede consultar de los ultimos " + cantidadDiasConsultaPorDia + " dias", null);
					}
					return;
				}

				// disabledButton = true;

				String tiempo = Constante.HORA;
				if (porDia) {
					tiempo = Constante.DIA;
				}

				String categoria = Constante.APLICACION;
				if (porSubAplicacion) {
					categoria = Constante.SUBAPLICACION;
				}

				if (idAuditoria == 0) {
					List<VcLogAdicional> adicionales = new ArrayList<>();
					VcLogAdicional ad1 = new VcLogAdicional();
					ad1.setCodigo("Fecha Inicio");
					ad1.setValor(UtilDate.dateToString(fInicio.getTime(), Parametros.fechaFormatPage));
					VcLogAdicional ad2 = new VcLogAdicional();
					ad2.setCodigo("Fecha Fin");
					ad2.setValor(UtilDate.dateToString(fFin.getTime(), Parametros.fechaFormatPage));
					VcLogAdicional ad3 = new VcLogAdicional();
					ad3.setCodigo("Periodo");
					ad3.setValor(tiempo);
					VcLogAdicional ad4 = new VcLogAdicional();
					ad4.setCodigo("NIVEL DETALLE");
					ad4.setValor(categoria);
					adicionales.add(ad1);
					adicionales.add(ad2);
					adicionales.add(ad3);
					adicionales.add(ad4);

					idAuditoria = guardarAuditoria(Calendar.getInstance(), this.getLogin(), this.ip, Parametros.vistaDetalleNavegacionAnchoBanda, Parametros.comandoDetalleNavegacionAnchoBanda, this.isdn, adicionales);

					if (idAuditoria <= 0) {
						SysMessage.error("Error al guardar log de auditoria", null);
						return;
					}
				}

				disableIncidente = false;
				vistaIncidente = DescriptorBitacora.DETALLE_NAVEGACION_ANCHO_BANDA;

				try {
					controlerBitacora.accion(DescriptorBitacora.DETALLE_NAVEGACION_ANCHO_BANDA,
							"Se consultó detalle de navegacion isdn: " + isdn + ", tiempo: " + tiempo + ", categoria: " + categoria + ", fechaInicio: " + UtilDate.dateToString(fInicio.getTime(), "dd-MM-yyyy HH:mm:ss") + ", fechaFin: " + UtilDate.dateToString(fFin.getTime(), "dd-MM-yyyy HH:mm:ss"));
				} catch (Exception e) {
					log.error("Error al guardar bitacora en el sistema: ", e);
					SysMessage.error("Error al guardar bitacora en el sistema", null);
				}

				long ini = System.currentTimeMillis();

				NodoServicio<DetalleNavegacionServiceSoapBindingStub> portNodo = ServicioDetalleNavegacion.initPort(wsdlLoc, "DETALLE_NAVEGACION_" + getLogin(), detalleNavegacionTimeOut);

				if (portNodo != null && portNodo.getPort() != null) {

					synchronized (portNodo) {

						try {
							DetalleNavegacionServiceSoapBindingStub port = portNodo.getPort();

							ResponseAnchoBanda response = port.getDetalleNavegacionAnchoBanda(isdn, fInicio, fFin, tiempo, categoria);

							long fin = System.currentTimeMillis();
							log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio DetalleNavegacion: " + (fin - ini) + " milisegundos");

							Long Con = null;
							if (portNodo.isPrimeraVez()) {
								Con = portNodo.getTiempoConexion();
							}

							if (Parametros.saveRequestResponse) {
								try {
									saveXml(isdn, getLogin(), wsdlLoc, "getDetalleNavegacionAnchoBanda", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoria, Con, (fin - ini));
								} catch (Exception e) {
									log.warn("No se logro registrar los request y response del servicio DetalleNavegacion, getDetalleNavegacionAnchoBanda: ", e);
								}
							}

							if (response != null) {
								if (response.getCode() != null) {

									log.debug("[isdn: " + getIsdn() + "] codigo: " + response.getCode() + ", descripcion: " + response.getDescription());

									if (response.getCode().intValue() == 0) {
										String[] listAplicaciones = response.getAplicaciones();
										String[] listTiempos = response.getTiempos();
										// COMENTAR
										// listTiemposTest = listTiempos;
										ResumenBanda[] listaResumen = response.getListaResumen();

										log.debug("[isdn: " + getIsdn() + "] listAplicaciones: " + Arrays.toString(listAplicaciones));
										log.debug("[isdn: " + getIsdn() + "] listTiempos: " + Arrays.toString(listTiempos));
										// log.debug("[isdn: " + getIsdn() + "] listResumenBanda: " + Arrays.toString(listaResumen));

										HashMap<String, HashMap<String, Double>> hashResumen = new HashMap<>();
										if (listaResumen != null) {

											long totalBytes = 0;

											for (ResumenBanda rb : listaResumen) {

												Banda[] resumen = rb.getResumen();

												HashMap<String, Double> hashAplicacion = new HashMap<>();

												if (resumen != null) {

													// int contador = 1;

													for (Banda item : resumen) {

														// if (contador > ParametrosDn.topAplicacionesReporte) {
														// break;
														// }
														// contador++;

														totalBytes = totalBytes + item.getTotal();

														ScriptEngineManager manager = new ScriptEngineManager();
														ScriptEngine se = manager.getEngineByName("JavaScript");

														if (se != null) {
															// Object result = se.eval("3.2 * 5.5");
															Object resultTotal = se.eval(item.getTotal() + operador + numero);
															double parseTotal = Double.parseDouble(String.valueOf(resultTotal));
															totalConsumo = totalConsumo + UtilNumber.redondear(parseTotal, nroDecimales);
														}

														double bytes = (double) item.getTotal() * (double) 8;
														double segundos = (double) item.getTime() / (double) 1000;

														if (segundos == 0) {
															segundos = 0.001;
														}
														double kbps = bytes / segundos;

														// log.debug("FORMULA: " + "(" + item.getTotal() + "* 8) / (" + item.getTime() + "/ 1000)");
														// log.debug("bytes: " + bytes);
														// log.debug("segundos: " + segundos);
														// log.debug("kbps: " + kbps);

														if (velocidadTransferencia.equals(Constante.Mbit)) {
															kbps = kbps / 1000;
														} else if (!(velocidadTransferencia.equals(Constante.Kbit))) {
															log.error("Error de configuracion de parametro velocidad de transferencia: " + velocidadTransferencia);
														}

														kbps = UtilNumber.redondear(kbps, nroDecimales);
														// log.debug("kbps redondeado: " + kbps);

														// log.debug(item.getNombre() + ", tiempo: " + item.getTime() + ", total: " + item.getTotal() + ", Xbit/s: " + UtilNumber.doubleToString(kbps,
														// 2));

														Double existe = hashAplicacion.get(item.getNombre());
														if (existe != null) {
															if (Double.isNaN(kbps) || Double.isInfinite(kbps)) {
																kbps = 0;
															}
															hashAplicacion.put(item.getNombre(), (kbps + existe));
														} else {
															if (Double.isNaN(kbps) || Double.isInfinite(kbps)) {
																kbps = 0;
															}
															hashAplicacion.put(item.getNombre(), kbps);
														}
														// hashAplicacion.put(item.getNombre(), item.getTotal());
													}
													hashResumen.put(rb.getFecha(), hashAplicacion);
												} else {
													hashResumen.put(rb.getFecha(), null);
												}
											}

											ScriptEngineManager manager = new ScriptEngineManager();
											ScriptEngine se = manager.getEngineByName("JavaScript");

											if (se != null) {
												// log.debug("totalBytes: " + totalBytes);
												Object resultTotal = se.eval(totalBytes + operador + numero);
												totalConsumo = Double.parseDouble(String.valueOf(resultTotal));
											}
											totalConsumo = new Double(UtilNumber.redondear(totalConsumo, nroDecimales));
											log.debug("ANCHO DE BANDA totalConsumo: " + totalConsumo);

											if (listTiempos != null) {
												for (String fechaStr : listTiempos) {
													if (barTiempos.isEmpty()) {
														barTiempos = "'" + fechaStr + "'";
													} else {
														barTiempos = barTiempos + ", '" + fechaStr + "'";
													}
												}
											}

											if (listAplicaciones != null && listAplicaciones.length > 0) {

												for (String aplicacionStr : listAplicaciones) {

													if (listTiempos != null) {
														List<Double> data = new ArrayList<>();
														for (String fechaStr : listTiempos) {

															HashMap<String, Double> hashMapResumen = hashResumen.get(fechaStr);
															if (hashMapResumen != null) {
																if (hashMapResumen.get(aplicacionStr) == null || hashMapResumen.get(aplicacionStr) == 0) {
																	data.add(0.0);
																	// data.add(null);
																} else {
																	data.add(hashMapResumen.get(aplicacionStr));
																}
																// data.add(hashMapResumen.get(aplicacionStr));
															} else {
																// data.add(new Double(0));
																data.add(null);
															}
														}
														Bar bar = new Bar();
														bar.setName(aplicacionStr);
														bar.setData(data);

														listBar.add(bar);
														// log.debug("listBar: " + listBar);
													}
												}
											}
										} else {
											log.warn("[isdn: " + getIsdn() + "] lista de resumen resuelto a nulo");
										}
									} else {
										SysMessage.warn("codigo: " + response.getCode() + ", descripcion: " + response.getDescription(), null);
									}
								} else {
									log.warn("[isdn: " + getIsdn() + "] response.getCode resuelto a nulo");
								}
							} else {
								log.debug("[isdn: " + getIsdn() + "] Respuesta resulto a nulo, isdn", null);
							}

						} finally {
							portNodo.setEnUso(false);
							portNodo.setFechaFin(new Date());
						}
					}
				} else {
					log.error("[isdn: " + getIsdn() + "] Port DetalleNavegacionresuelto a nulo");
				}

				// COMENTAR
				// listBar = new ArrayList<>();
				// for (int i = 1; i <= 200; i++) {
				// List<Double> data = new ArrayList<>();
				// for (String fechaStr : listTiemposTest) {
				// data.add(new Double(i * 1));
				// }
				// listBar.add(new Bar("NAME" + i, data));
				// }

				if (listBar.size() > 0) {
					dataJsonAnchoBanda = toGsonAnchoBanda(listBar);
					callJavaScriptAnchoBanda(nroDecimales, velocidadTransferencia);
				} else {
					// createPieModel(visibleConsumoPorcentaje);
					barTiempos = "";
					dataJsonAnchoBanda = "[]";
					callJavaScriptAnchoBanda(nroDecimales, velocidadTransferencia);
				}

				try {
					DnConsulta dc = new DnConsulta();
					dc.setIp(ip);
					dc.setUsuario(getLogin());
					dc.setIsdn(isdn);
					dc.setTiempo(tiempo);
					dc.setCategoria(categoria);
					dc.setReporte("DETALLE NAVEGACION POR ANCHO DE BANDA");
					dc.setFechaInicio(fInicio.getTime());
					dc.setFechaFin(fFin.getTime());
					Calendar cal = Calendar.getInstance();
					cal.set(Calendar.HOUR_OF_DAY, 0);
					cal.set(Calendar.MINUTE, 0);
					cal.set(Calendar.SECOND, 0);
					cal.set(Calendar.MILLISECOND, 0);
					dc.setFecha(cal.getTime());
					dc.setFechaCreacion(new Date());
					consultaBl.save(dc);
				} catch (Exception e) {
					log.error("Error al guardar consulta: ", e);
					SysMessage.error("Error al guardar consulta en el sistema", null);
				}

				SysMessage.info("Consulta finalizada", null);

			} catch (AxisFault a) {
				String msg = a.getMessage();
				msg.toLowerCase();

				if (reintento < reintentos && a.getCause() != null && a.getCause().getLocalizedMessage().trim().equals("Read timed out")) {
					// GotoFactory.getSharedInstance().getGoto().go(4);
					log.debug("Reintento de conexion por time out nro: " + (reintento + 1));
					reintento++;
					continuar = true;
					try {
						Thread.sleep(esperaReintentos);
					} catch (Exception e) {
						e.getMessage();
					}
				} else {
					guardarExcepcion(idAuditoria, "Error AxisFault de conexion al servicio DetalleNavegacion: " + stackTraceToString(a));
					if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase()) || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase()) || msg.contains("UnknownHostException".toLowerCase())
							|| msg.contains("No route to host: connect".toLowerCase())) {
						log.error("Error de conexion al servicio DetalleNavegacion: ", a);
						SysMessage.error("Error de conexion del servicio DetalleNavegacion", null);
					} else {
						log.error("Error AxisFault: ", a);
						SysMessage.error("Error de conexion del servicio DetalleNavegacion: " + a.getMessage(), null);
					}
				}
			} catch (ServiceException e) {
				log.error("Error de servicio al conectarse al servicio DetalleNavegacion: ", e);
				guardarExcepcion(idAuditoria, "Error de servicio al conectarse al servicio DetalleNavegacion: " + stackTraceToString(e));
				SysMessage.error("Error de servicio al conectarse al servicio DetalleNavegacion", null);
			} catch (RemoteException e) {
				log.error("Error remoto al conectarse al servicio DetalleNavegacion: ", e);
				guardarExcepcion(idAuditoria, "Error remoto al conectarse al servicio DetalleNavegacion: " + stackTraceToString(e));
				SysMessage.error("Error remoto al conectarse al servicio DetalleNavegacion", null);
			} catch (Exception e) {
				log.error("Error al consultar detalle de navegacion", e);
				guardarExcepcion(idAuditoria, "Error al consultar detalle de navegacion: " + stackTraceToString(e));
				SysMessage.error("Error al consultar detalle de navegacion: " + e.getLocalizedMessage(), null);
			}
		}

	}

	public void buscarDetalleMbVsTiempo() {
		log.debug("ingresando a buscar detalle mb vs tiempo");

		// COMENTAR
		// String[] listTiemposTest = null;
		boolean continuar = true;
		int reintento = 0;
		long idAuditoria = 0;
		this.totalConsumo = 0.0;

		int reintentos = 0;
		int esperaReintentos = 0;
		int detalleNavegacionTimeOut = 0;
		String operador = "";
		int numero = 0;
		int nroDecimales = 0;
		String expresionRegularNroTigo = "";
		int cantidadDiasConsultaPorDia = 0;
		int cantidadDiasConsultaPorHora = 0;
		String wsdlLoc = "";
		try {

			reintentos = (int) ParametrosDn.reintentosTimeOut;
			esperaReintentos = (int) ParametrosDn.esperaReintentosTimeOut;
			detalleNavegacionTimeOut = (int) ParametrosDn.detalleNavegacionTimeOut;
			operador = (String) ParametrosDn.operador;
			numero = (int) ParametrosDn.numero;
			nroDecimales = (int) ParametrosDn.cantidadDecimales;
			expresionRegularNroTigo = (String) ParametrosDn.expresionRegularNroTigo;
			cantidadDiasConsultaPorDia = (int) ParametrosDn.cantidadDiasConsultaPorDia;
			cantidadDiasConsultaPorHora = (int) ParametrosDn.cantidadDiasConsultaPorHora;
			wsdlLoc = (String) ParametrosDn.detalleNavegacionWsdl;

		} catch (Exception e) {
			log.error("Error al cargar parametros del Sistema: ", e);
			SysMessage.error("Error al cargar parametros del Sistema: " + e.getMessage(), null);
			return;
		}

		while (continuar) {
			continuar = false;
			try {

				listBarTiempo = new ArrayList<Bar>();
				barTiemposMb = "";

				if (isdn == null || isdn.trim().isEmpty()) {
					SysMessage.warn("El campo Nro de Cuenta no es válido", null);
					return;
				}
				if (!UtilNumber.esNroTigo(isdn, expresionRegularNroTigo)) {
					SysMessage.warn("Nro de cuenta no válido", null);
					return;
				}
				if (fechaInicio == null) {
					SysMessage.warn("El campo Fecha Inicio no es válido", null);
					return;
				}
				if (fechaFin == null) {
					SysMessage.warn("El campo Fecha Fin no es válido", null);
					return;
				}

				Calendar fInicio = Calendar.getInstance();
				Calendar fFin = Calendar.getInstance();
				Calendar fActual = Calendar.getInstance();
				Calendar fHoy = Calendar.getInstance();

				fActual.set(Calendar.HOUR_OF_DAY, 0);
				fActual.set(Calendar.MINUTE, 0);
				fActual.set(Calendar.SECOND, 0);
				fActual.set(Calendar.MILLISECOND, 0);
				log.debug("fechaActual: " + UtilDate.dateToString(fActual.getTime(), "dd-MM-yyyy HH:mm:ss"));

				fInicio.setTimeInMillis(fechaInicio.getTime());
				fFin.setTimeInMillis(fechaFin.getTime());

				if (porHora) {
					fInicio.set(Calendar.HOUR_OF_DAY, Integer.parseInt(selectHoraInicio));
					fFin.set(Calendar.HOUR_OF_DAY, Integer.parseInt(selectHoraFin));
				}

				fHoy.set(Calendar.MINUTE, 0);
				fHoy.set(Calendar.SECOND, 0);
				fHoy.set(Calendar.MILLISECOND, 0);

				if (porDia) {
					fInicio.set(Calendar.HOUR_OF_DAY, 0);
					fInicio.set(Calendar.MINUTE, 0);
					fInicio.set(Calendar.SECOND, 0);
					fInicio.set(Calendar.MILLISECOND, 0);

					fFin.set(Calendar.HOUR_OF_DAY, 0);
					fFin.set(Calendar.MINUTE, 0);
					fFin.set(Calendar.SECOND, 0);
					fFin.set(Calendar.MILLISECOND, 0);

					fActual.add(Calendar.DAY_OF_MONTH, -(cantidadDiasConsultaPorDia - 1));
					fHoy.set(Calendar.HOUR_OF_DAY, 0);
				} else {
					fInicio.set(Calendar.MINUTE, 0);
					fInicio.set(Calendar.SECOND, 0);
					fInicio.set(Calendar.MILLISECOND, 0);

					fInicio.set(Calendar.MINUTE, 0);
					fInicio.set(Calendar.SECOND, 0);
					fInicio.set(Calendar.MILLISECOND, 0);

					fActual.add(Calendar.DAY_OF_MONTH, -(cantidadDiasConsultaPorHora - 1));
				}

				log.debug("fechaInicio: " + UtilDate.dateToString(fInicio.getTime(), "dd-MM-yyyy HH:mm:ss") + ", fechaFin: " + UtilDate.dateToString(fFin.getTime(), "dd-MM-yyyy HH:mm:ss"));
				log.debug("fechaActual restada: " + UtilDate.dateToString(fActual.getTime(), "dd-MM-yyyy HH:mm:ss"));
				log.debug("fechaHoy: " + UtilDate.dateToString(fHoy.getTime(), "dd-MM-yyyy HH:mm:ss"));

				if (fFin.getTimeInMillis() > fHoy.getTimeInMillis()) {
					SysMessage.warn("La Fecha Fin tiene que ser menor o igual que la Fecha Actual del Sistema", null);
					return;
				}

				if (fFin.getTimeInMillis() < fInicio.getTimeInMillis()) {
					SysMessage.warn("La Fecha Inicio tiene que ser menor o igual que la Fecha Fin", null);
					return;
				}

				if (fInicio.getTimeInMillis() < fActual.getTimeInMillis()) {
					if (porHora) {
						SysMessage.warn("Solo se puede consultar de los ultimos " + cantidadDiasConsultaPorHora + " dias", null);
					} else {
						SysMessage.warn("Solo se puede consultar de los ultimos " + cantidadDiasConsultaPorDia + " dias", null);
					}
					return;
				}

				// disabledButton = true;

				String tiempo = Constante.HORA;
				if (porDia) {
					tiempo = Constante.DIA;
				}

				String categoria = Constante.APLICACION;
				if (porSubAplicacion) {
					categoria = Constante.SUBAPLICACION;
				}

				if (idAuditoria == 0) {
					List<VcLogAdicional> adicionales = new ArrayList<>();
					VcLogAdicional ad1 = new VcLogAdicional();
					ad1.setCodigo("Fecha Inicio");
					ad1.setValor(UtilDate.dateToString(fInicio.getTime(), Parametros.fechaFormatPage));
					VcLogAdicional ad2 = new VcLogAdicional();
					ad2.setCodigo("Fecha Fin");
					ad2.setValor(UtilDate.dateToString(fFin.getTime(), Parametros.fechaFormatPage));
					VcLogAdicional ad3 = new VcLogAdicional();
					ad3.setCodigo("Periodo");
					ad3.setValor(tiempo);
					VcLogAdicional ad4 = new VcLogAdicional();
					ad4.setCodigo("NIVEL DETALLE");
					ad4.setValor(categoria);
					adicionales.add(ad1);
					adicionales.add(ad2);
					adicionales.add(ad3);
					adicionales.add(ad4);

					idAuditoria = guardarAuditoria(Calendar.getInstance(), this.getLogin(), this.ip, Parametros.vistaDetalleNavegacionTiempoVsConsumo, Parametros.comandoDetalleNavegacionTiempoVsConsumo, this.isdn, adicionales);

					if (idAuditoria <= 0) {
						SysMessage.error("Error al guardar log de auditoria", null);
						return;
					}
				}

				disableIncidente = false;
				vistaIncidente = DescriptorBitacora.DETALLE_NAVEGACION_MB_VS_TIEMPO;

				try {
					controlerBitacora.accion(DescriptorBitacora.DETALLE_NAVEGACION_MB_VS_TIEMPO,
							"Se consultó detalle de navegacion isdn: " + isdn + ", tiempo: " + tiempo + ", categoria: " + categoria + ", fechaInicio: " + UtilDate.dateToString(fInicio.getTime(), "dd-MM-yyyy HH:mm:ss") + ", fechaFin: " + UtilDate.dateToString(fFin.getTime(), "dd-MM-yyyy HH:mm:ss"));
				} catch (Exception e) {
					log.error("Error al guardar bitacora en el sistema: ", e);
					SysMessage.error("Error al guardar bitacora en el sistema", null);
				}

				long ini = System.currentTimeMillis();
				NodoServicio<DetalleNavegacionServiceSoapBindingStub> portNodo = ServicioDetalleNavegacion.initPort(wsdlLoc, "DETALLE_NAVEGACION_" + getLogin(), detalleNavegacionTimeOut);

				if (portNodo != null && portNodo.getPort() != null) {

					synchronized (portNodo) {

						try {
							DetalleNavegacionServiceSoapBindingStub port = portNodo.getPort();

							ResponseAnchoBanda response = port.getDetalleNavegacionAnchoBanda(isdn, fInicio, fFin, tiempo, categoria);

							long fin = System.currentTimeMillis();
							log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio DetalleNavegacion: " + (fin - ini) + " milisegundos");

							Long Con = null;
							if (portNodo.isPrimeraVez()) {
								Con = portNodo.getTiempoConexion();
							}

							if (Parametros.saveRequestResponse) {
								try {
									saveXml(isdn, getLogin(), wsdlLoc, "getDetalleNavegacionAnchoBanda", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoria, Con, (fin - ini));
								} catch (Exception e) {
									log.warn("No se logro registrar los request y response del servicio DetalleNavegacion, getDetalleNavegacionAnchoBanda: ", e);
								}
							}

							if (response != null) {
								if (response.getCode() != null) {

									log.debug("[isdn: " + getIsdn() + "] codigo: " + response.getCode() + ", descripcion: " + response.getDescription());

									if (response.getCode().intValue() == 0) {
										String[] listAplicaciones = response.getAplicaciones();
										String[] listTiempos = response.getTiempos();
										// COMENTAR
										// listTiemposTest = listTiempos;
										ResumenBanda[] listaResumen = response.getListaResumen();

										log.debug("[isdn: " + getIsdn() + "] listAplicaciones: " + Arrays.toString(listAplicaciones));
										log.debug("[isdn: " + getIsdn() + "] listTiempos: " + Arrays.toString(listTiempos));
										// log.debug("[isdn: " + getIsdn() + "] listResumenBanda: " + Arrays.toString(listaResumen));

										HashMap<String, HashMap<String, Double>> hashResumen = new HashMap<>();

										if (listaResumen != null) {
											long totalBytes = 0;

											for (ResumenBanda rb : listaResumen) {

												Banda[] resumen = rb.getResumen();

												HashMap<String, Double> hashAplicacion = new HashMap<>();

												if (resumen != null) {

													// int contador = 1;

													for (Banda item : resumen) {

														// if (contador > ParametrosDn.topAplicacionesReporte) {
														// break;
														// }
														// contador++;

														long total = item.getTotal();
														totalBytes = totalBytes + total;

														ScriptEngineManager manager = new ScriptEngineManager();
														ScriptEngine se = manager.getEngineByName("JavaScript");

														if (se != null) {
															// Object result = se.eval("3.2 * 5.5");
															Object resultTotal = se.eval(total + operador + numero);
															double parseTotal = Double.parseDouble(String.valueOf(resultTotal));
															parseTotal = UtilNumber.redondear(parseTotal, nroDecimales);
															totalConsumo = totalConsumo + parseTotal;

															Double existe = hashAplicacion.get(item.getNombre());
															if (existe != null) {
																if (Double.isNaN(parseTotal) || Double.isInfinite(parseTotal)) {
																	parseTotal = 0;
																}
																hashAplicacion.put(item.getNombre(), (parseTotal + existe));
															} else {
																if (Double.isNaN(parseTotal) || Double.isInfinite(parseTotal)) {
																	parseTotal = 0;
																}
																hashAplicacion.put(item.getNombre(), parseTotal);
															}
														} else {
															log.error("ScriptEngine resuelto a nulo");
														}
													}
													hashResumen.put(rb.getFecha(), hashAplicacion);
												} else {
													hashResumen.put(rb.getFecha(), null);
												}
											}

											ScriptEngineManager manager = new ScriptEngineManager();
											ScriptEngine se = manager.getEngineByName("JavaScript");

											if (se != null) {
												// log.debug("totalBytes: " + totalBytes);
												Object resultTotal = se.eval(totalBytes + operador + numero);
												totalConsumo = Double.parseDouble(String.valueOf(resultTotal));
											}
											totalConsumo = UtilNumber.redondear(totalConsumo, nroDecimales);
											log.debug("MB VS TIEMPO totalConsumo: " + totalConsumo);

											if (listTiempos != null) {
												for (String fechaStr : listTiempos) {
													if (barTiemposMb.isEmpty()) {
														barTiemposMb = "'" + fechaStr + "'";
													} else {
														barTiemposMb = barTiemposMb + ", '" + fechaStr + "'";
													}
												}
											}

											if (listAplicaciones != null && listAplicaciones.length > 0) {

												for (String aplicacionStr : listAplicaciones) {

													if (listTiempos != null) {
														List<Double> data = new ArrayList<>();
														for (String fechaStr : listTiempos) {

															HashMap<String, Double> hashMapResumen = hashResumen.get(fechaStr);
															if (hashMapResumen != null) {

																if (hashMapResumen.get(aplicacionStr) == null || hashMapResumen.get(aplicacionStr) == 0) {
																	data.add(0.0);
																	// data.add(null);
																} else {
																	data.add(hashMapResumen.get(aplicacionStr));
																}
															} else {
																// data.add(new Double(0));
																data.add(null);
															}
														}
														Bar bar = new Bar();
														bar.setName(aplicacionStr);
														bar.setData(data);

														listBarTiempo.add(bar);
														// log.debug("listBar: " + listBar);
													}
												}
											}
										} else {
											log.warn("[isdn: " + getIsdn() + "] lista de resumen resuelto a nulo");
										}
									} else {
										SysMessage.warn("codigo: " + response.getCode() + ", descripcion: " + response.getDescription(), null);
									}
								} else {
									log.warn("[isdn: " + getIsdn() + "] response.getCode resuelto a nulo");
								}
							} else {
								log.debug("[isdn: " + getIsdn() + "] Respuesta resulto a nulo, isdn", null);
							}

						} finally {
							portNodo.setEnUso(false);
							portNodo.setFechaFin(new Date());
						}
					}
				} else {
					log.error("[isdn: " + getIsdn() + "] Port DetalleNavegacionresuelto a nulo");
				}

				// COMENTAR
				// listBarTiempo = new ArrayList<>();
				// for (int i = 1; i <= 200; i++) {
				// List<Double> data = new ArrayList<>();
				// for (String fechaStr : listTiemposTest) {
				// data.add(new Double(i * 1));
				// }
				// listBarTiempo.add(new Bar("NAME" + i, data));
				// }

				if (listBarTiempo.size() > 0) {
					dataJsonMbVsTiempo = toGsonAnchoBanda(listBarTiempo);
					callJavaScriptMbVsTiempo(nroDecimales);
				} else {
					barTiempos = "";
					dataJsonMbVsTiempo = "[]";
					// createPieModel(visibleConsumoPorcentaje);
					callJavaScriptMbVsTiempo(nroDecimales);
				}

				try {
					DnConsulta dc = new DnConsulta();
					dc.setIp(ip);
					dc.setUsuario(getLogin());
					dc.setIsdn(isdn);
					dc.setTiempo(tiempo);
					dc.setCategoria(categoria);
					dc.setReporte("DETALLE NAVEGACION TRAFICO VS TIEMPO");
					dc.setFechaInicio(fInicio.getTime());
					dc.setFechaFin(fFin.getTime());
					Calendar cal = Calendar.getInstance();
					cal.set(Calendar.HOUR_OF_DAY, 0);
					cal.set(Calendar.MINUTE, 0);
					cal.set(Calendar.SECOND, 0);
					cal.set(Calendar.MILLISECOND, 0);
					dc.setFecha(cal.getTime());
					dc.setFechaCreacion(new Date());
					consultaBl.save(dc);
				} catch (Exception e) {
					log.error("Error al guardar consulta: ", e);
					SysMessage.error("Error al guardar consulta en el sistema", null);
				}

				SysMessage.info("Consulta finalizada", null);

			} catch (AxisFault a) {
				String msg = a.getMessage();
				msg.toLowerCase();

				if (reintento < reintentos && a.getCause() != null && a.getCause().getLocalizedMessage().trim().equals("Read timed out")) {
					// GotoFactory.getSharedInstance().getGoto().go(4);
					log.debug("Reintento de conexion por time out nro: " + (reintento + 1));
					reintento++;
					continuar = true;
					try {
						Thread.sleep(esperaReintentos);
					} catch (Exception e) {
						e.getMessage();
					}
				} else {
					guardarExcepcion(idAuditoria, "Error AxisFault de conexion al servicio DetalleNavegacion: " + stackTraceToString(a));
					if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase()) || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase()) || msg.contains("UnknownHostException".toLowerCase())
							|| msg.contains("No route to host: connect".toLowerCase())) {
						log.error("Error de conexion al servicio DetalleNavegacion: ", a);
						SysMessage.error("Error de conexion del servicio DetalleNavegacion", null);
					} else {
						log.error("Error AxisFault: ", a);
						SysMessage.error("Error de conexion del servicio DetalleNavegacion: " + a.getMessage(), null);
					}
				}
			} catch (ServiceException e) {
				log.error("Error de servicio al conectarse al servicio DetalleNavegacion: ", e);
				guardarExcepcion(idAuditoria, "Error de servicio al conectarse al servicio DetalleNavegacion: " + stackTraceToString(e));
				SysMessage.error("Error de servicio al conectarse al servicio DetalleNavegacion", null);
			} catch (RemoteException e) {
				log.error("Error remoto al conectarse al servicio DetalleNavegacion: ", e);
				guardarExcepcion(idAuditoria, "Error remoto al conectarse al servicio DetalleNavegacion: " + stackTraceToString(e));
				SysMessage.error("Error remoto al conectarse al servicio DetalleNavegacion", null);
			} catch (Exception e) {
				log.error("Error al consultar detalle de navegacion", e);
				guardarExcepcion(idAuditoria, "Error al consultar detalle de navegacion: " + stackTraceToString(e));
				SysMessage.error("Error al consultar detalle de navegacion: " + e.getLocalizedMessage(), null);
			}
		}

	}

	private void callJavaScriptPorcentaje(boolean visibleConsumoPorcentaje) {
		try {
			String titulo = "TRAFICO TOTAL DISTRIBUIDO POR APLICACION";
			titulo = "";
			if (dataJsonPorcentaje.equals("[]")) {
				titulo = "DATOS NO ENCONTRADOS";
			}

			log.debug("dataJsonPorcentaje: " + dataJsonPorcentaje);
			// String js = "$('#container_1').highcharts({ chart: { type: 'pie', renderTo: 'container_1', options3d: {enabled: true, alpha: 45,beta: 0}}, credits: { enabled: false, href:
			// 'http://www.highcharts.com', text: 'Highcharts.com' }, title: {text: '" + titulo
			// + "'},tooltip: { pointFormat: '{series.name}: <b>{point.y}%</b>' }, plotOptions: {pie: {showInLegend: true, allowPointSelect: true, cursor: 'pointer', depth: 35, dataLabels: { enabled:
			// true, format: '{point.name}: {point.y} %' }} },series: [{ name: 'Porcentaje',colorByPoint: true, data: "
			// + dataJsonPorcentaje + "}]});";

			String consumo = "<br/>Consumo: <b>' + this.point.m + ' </b> " + this.tipoUnidad;
			String consumoPie = " - <font style=color:red;>{point.m} " + this.tipoUnidad + "</font>";
			if (!visibleConsumoPorcentaje) {
				consumoPie = "";
				consumo = "";
			}

			// String js = "Highcharts.chart('container_1', { chart: { type: 'pie', renderTo: 'container_1', options3d: {enabled: true, alpha: 45,beta: 0}}, credits: { enabled: false, href:
			// 'http://www.highcharts.com', text: 'Highcharts.com' }, title: {text: '" + titulo
			// + "'},tooltip: { useHTML: true, pointFormat: '{series.name}: <b>{point.y}%</b></b>" + consumo + "' }, plotOptions: {pie: {showInLegend: true, allowPointSelect: true, cursor: 'pointer',
			// depth: 35, dataLabels: { enabled: true, format: '{point.name}: {point.y} %" + consumoPie
			// + "', useHTML: true }} }, series: [{ name: 'Porcentaje',colorByPoint: true, data: " + dataJsonPorcentaje + "}]});";

			String js = "Highcharts.chart('container_1', {        chart: {		type: 'pie', renderTo: 'container_1', options3d: {enabled: true, alpha: 45,beta: 0}}, credits: { enabled: false, href: 'http://www.highcharts.com', text: 'Highcharts.com' }, title: {text: '" + titulo
					+ "'},tooltip: { useHTML: true, formatter: function(){ return '<div style=background-color:rgba(247,247,247,0.9) class=tooltip>' + this.point.name + '<br/>' + this.series.name + ': <b>' + this.point.y + '%</b> " + consumo
					+ " </div>'; } }, plotOptions: {pie: {showInLegend: true, allowPointSelect: true, cursor: 'pointer', depth: 35, dataLabels: { enabled: true, format: '{point.name}: {point.y} %" + consumoPie + "', useHTML: true }} }, series: [{ name: 'Porcentaje',colorByPoint: true, data: "
					+ dataJsonPorcentaje + "}]});";

			// log.debug(js);
			// js = "Highcharts.chart('container_1', { chart: { type: 'pie', options3d: { enabled: true, alpha: 45, beta: 0 } }, title: { text: 'Browser market shares at a specific website, 2014' },
			// tooltip: { pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>' }, plotOptions: { pie: { allowPointSelect: true, cursor: 'pointer', depth: 35, dataLabels: { enabled: true,
			// format: '{point.name}' } } }, series: [{ type: 'pie', name: 'Browser share', data: [ ['Firefox', 45.0], ['IE', 26.8], { name: 'Chrome', y: 12.8, sliced: true, selected: true },
			// ['Safari', 8.5], ['Opera', 6.2], ['Others', 0.7] ] }] }); ";
			log.debug(js);
			RequestContext.getCurrentInstance().execute(js);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void callJavaScriptAnchoBanda(int nroDecimales, String velocidadTransferencia) {
		try {
			// barTiempos = "'29-10-2016', '30-10-2016', '31-10-2016', '01-11-2016'";
			String titulo = "TOTAL ANCHO DE BANDA POR APLICACION";
			titulo = "";
			if (dataJsonAnchoBanda.equals("[]")) {
				titulo = "DATOS NO ENCONTRADOS";
			} // margin: [top, right, bottom, left]

			String margin = "margin: [50, 50, 200, 80],";
			margin = "";
			String js = "Highcharts.chart('container_2', {        chart: {  " + margin
					+ "  borderWidth: 0, height: 500,  style: {    fontFamily: 'Verdana',   fontSize: '11px'  },       type: 'column', renderTo: 'container_2', zoomType: 'xy'        }, legend:{ align: 'center',  verticalAlign:'bottom', borderWidth: 0, padding: 70}, credits: { enabled: false, href: 'http://www.highcharts.com', text: 'Highcharts.com' }, title: {            text: '"
					+ titulo + "'        },        subtitle: {            text: ''        },        xAxis: {            categories: [" + barTiempos
					+ "],            crosshair: true, crosshair: { dashStyle: 'Solid', color: 'rgba(204,214,235,1)' } }, yAxis: { min: 0,            title: {                text: '" + titleY
					+ "'            }        },        tooltip: {       style: 'pointerEvents: auto', hideDelay: 60000, positioner: function(labelWidth, labelHeight, point) { var tooltipX, tooltipY;   if (point.plotX + labelWidth > this.chart.plotWidth) {  tooltipX = point.plotX + this.chart.plotLeft - labelWidth + 30;   } else {	tooltipX = point.plotX + this.chart.plotLeft - 50;  }   tooltipY = this.chart.plotTop + this.chart.plotHeight;  return { x: tooltipX,	y: tooltipY	};   }, headerFormat: '<span style=font-size:14px;font-weight:bold;><b>{point.key}</b></span><div style=max-width:500px;max-height:150px;overflow-y:auto;overflow-x:auto;padding-right:20px;><table id=content-table-dn1  cellspacing=0 cellpadding=2 border=0 style=font-size:11px;><thead><tr><th class=dnnombre >Nombre <a href=javascript:sort(true,&#39;dnnombre&#39;,&#39;content-table-dn1&#39;); style=text-decoration:none;>&uarr;</a><a href=javascript:sort(false,&#39;dnnombre&#39;,&#39;content-table-dn1&#39;); style=text-decoration:none;>&darr;</a>    </th><th class=unidad>Datos  <a href=javascript:sort(true,&#39;dnmb&#39;,&#39;content-table-dn1&#39;); style=text-decoration:none;>&uarr;</a><a href=javascript:sort(false,&#39;dnmb&#39;,&#39;content-table-dn1&#39;); style=text-decoration:none;>&darr;</a>  </th><th class=unidad ></th></tr></thead><tbody>',  pointFormat: '<tr><td class=dnnombre style=color:{series.color};>{series.name}</td>' + '<td class=dnmb >{point.y:."
					+ nroDecimales + "f}" + "</td><td class=unidad > " + velocidadTransferencia
					+ "/s</td></tr>', footerFormat: '</tbody></table><img src=../resources/img/startup.png height=0 width=10 onload=sort(false,&#39;dnmb&#39;,&#39;content-table-dn1&#39;)></div>',  shared: true,            useHTML: true        },        plotOptions: {     series: {  cursor: 'pointer'    },       column: {                pointPadding: 0.2,                borderWidth: 0            }        },        series: "
					+ dataJsonAnchoBanda + "    });";
			// log.debug(js);
			RequestContext.getCurrentInstance().execute(js);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void callJavaScriptMbVsTiempo(int nroDecimales) {
		try {
			// barTiempos = "'29-10-2016', '30-10-2016', '31-10-2016', '01-11-2016'";
			String titulo = "DETALLE DE NAVEGACION POR TIEMPO";
			titulo = "";
			if (dataJsonMbVsTiempo.equals("[]")) {
				titulo = "DATOS NO ENCONTRADOS";
			}
			String margin = "margin: [50, 50, 200, 80], ";
			margin = "";
			String js = "Highcharts.chart('container_3', {        chart: {   " + margin
					+ " borderWidth: 0, height: 500,  style: {    fontFamily: 'Verdana',   fontSize: '11px'  },     type: 'column', renderTo: 'container_3', zoomType: 'xy'        }, legend:{ align: 'center',  verticalAlign:'bottom', borderWidth: 0, padding: 70}, credits: { enabled: false, href: 'http://www.highcharts.com', text: 'Highcharts.com' },       title: {            text: '"
					+ titulo + "'        },        subtitle: {            text: ''        },        xAxis: {            categories: [" + barTiemposMb
					+ "],            crosshair: true, crosshair: { dashStyle: 'Solid', color: 'rgba(204,214,235,1)' }        },        yAxis: {            min: 0,            title: {                text: '" + this.tipoUnidad
					+ "'            }        },        tooltip: {     style: 'pointerEvents: auto', hideDelay: 30000,  positioner: function(labelWidth, labelHeight, point) {    var tooltipX, tooltipY;   if (point.plotX + labelWidth > this.chart.plotWidth) {   tooltipX = point.plotX + this.chart.plotLeft - labelWidth + 30;   } else {  	tooltipX = point.plotX + this.chart.plotLeft - 50;  }   tooltipY = this.chart.plotTop + this.chart.plotHeight;  return { x: tooltipX,	y: tooltipY	}; },   headerFormat: '<span style=font-size:14px; font-weight: bold;><b>{point.key}</b></span><div style=max-width:500px;max-height:150px;overflow-y:auto;overflow-x:auto;padding-right:20px;><table id=content-table-dn cellspacing=0 cellpadding=2 border=0 style=font-size:11px;><thead><tr><th class=dnnombre>Nombre <a href=javascript:sort(true,&#39;dnnombre&#39;,&#39;content-table-dn&#39;); style=text-decoration:none;>&uarr;</a><a href=javascript:sort(false,&#39;dnnombre&#39;,&#39;content-table-dn&#39;); style=text-decoration:none;>&darr;</a>    </th><th class=unidad >Datos  <a href=javascript:sort(true,&#39;dnmb&#39;,&#39;content-table-dn&#39;); style=text-decoration:none;>&uarr;</a><a href=javascript:sort(false,&#39;dnmb&#39;,&#39;content-table-dn&#39;); style=text-decoration:none;>&darr;</a>  </th><th class=unidad></th></tr></thead><tbody>',  pointFormat: '<tr><td class=dnnombre style=color:{series.color};>{series.name}</td>' + '<td class=dnmb>{point.y:."
					+ nroDecimales + "f}" + "</td><td class=unidad > " + this.tipoUnidad
					+ "</td></tr>', footerFormat: '</tbody></table><img src=../resources/img/startup.png height=0 width=10 onload=sort(false,&#39;dnmb&#39;,&#39;content-table-dn&#39;)></div>',  shared: true,            useHTML: true        },        plotOptions: {        cursor: 'pointer',    column: {                pointPadding: 0.2,                borderWidth: 0            }        },        series: "
					+ dataJsonMbVsTiempo + "   });";

			log.debug(js);
			RequestContext.getCurrentInstance().execute(js);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unused")
	private String toGson(List<Pie3D> list) {
		Gson gson = new Gson();
		// GsonAlternative
		String data = gson.toJson(list);
		// data = data.replace("\"", "'");
		data = data.replace("\"name\"", "name");
		data = data.replace("\"y\"", "y");
		data = data.replace("\"", "'");

		// log.debug("data: " + data);
		return data;
	}

	private String toGsonDouble(List<Pie3DDouble> list) throws Exception {
		String data = "";
		try {
			log.debug(list);
			// List<Pie3DDouble> remover = new ArrayList<>();
			for (Pie3DDouble item : list) {
				if (item.getM().isNaN() || item.getM().isInfinite() || item.getM().doubleValue() <= 0) {
					item.setM(0.0);
					// item.setM(null);
				}
				if (item.getY().isNaN() || item.getY().isInfinite() || item.getY().doubleValue() <= 0) {
					item.setY(0.0);
					// item.setY(null);
				}
				// if (item.getM().doubleValue() <= 0 && item.getY().doubleValue() <= 0) {
				// remover.add(item);
				// }
			}

			// list.removeAll(remover);
			log.debug(list);
			Gson gson = new Gson();
			// GsonAlternative
			data = gson.toJson(list);
			// data = data.replace("\"", "'");
			data = data.replace("\"name\"", "name");
			data = data.replace("\"y\"", "y");
			data = data.replace("\"", "'");

			// log.debug("data: " + data);

		} catch (Exception e) {
			log.debug(list);
			log.error("Error al obtener jason: " + list);
			throw new Exception(e);
		}
		return data;
	}

	private String toGsonAnchoBanda(List<Bar> list) {
		Gson gson = new Gson();
		// GsonAlternative

		log.debug("LIST: " + list);

		String data = gson.toJson(list);

		// data = data.replace("\"", "'");
		data = data.replace("\"name\"", "name");
		data = data.replace("\"data\"", "data");
		data = data.replace("\"", "'");

		// log.debug("data: " + data);
		return data;
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
	private static HashMap sortByValues(HashMap map) {

		List list = new LinkedList(map.entrySet());
		// Defined Custom Comparator here
		Collections.sort(list, new Comparator() {
			public int compare(Object o1, Object o2) {
				return ((Comparable) ((Map.Entry) (o1)).getValue()).compareTo(((Map.Entry) (o2)).getValue());
			}
		});

		// Here I am copying the sorted list in HashMap
		// using LinkedHashMap to preserve the insertion order
		HashMap sortedHashMap = new LinkedHashMap();
		for (Iterator it = list.iterator(); it.hasNext();) {
			Map.Entry entry = (Map.Entry) it.next();
			sortedHashMap.put(entry.getKey(), entry.getValue());
		}
		return sortedHashMap;
	}

	private transient StreamedContent file;

	@SuppressWarnings("deprecation")
	public void exportar(String tipo) {
		log.debug("ingresando a exportar " + tipo);

		boolean continuar = true;
		int reintento = 0;
		long idAuditoria = 0;
		totalConsumo = 0.0;
		listConsumo = new ArrayList<>();
		listPieConsumo = new ArrayList<>();
		// listPies = new ArrayList<>();

		int reintentos = 0;
		int esperaReintentos = 0;
		int detalleNavegacionTimeOut = 0;
		String operador = "";
		int numero = 0;
		int nroDecimales = 0;
		String expresionRegularNroTigo = "";
		int cantidadDiasConsultaPorDia = 0;
		int cantidadDiasConsultaPorHora = 0;
		String wsdlLoc = "";
		// boolean visibleConsumoPorcentaje;
		int topAplicacionesReporte;
		String textoReporteFuente;
		int textoReporteSize;
		String textoReporte;
		String textoReporteFuenteHeader;
		int textoReporteHeaderSize;
		String textoReporteHeader;
		String columna1;
		String columna2;
		String columna3;
		String columna4;
		try {

			reintentos = (int) ParametrosDn.reintentosTimeOut;
			esperaReintentos = (int) ParametrosDn.esperaReintentosTimeOut;
			detalleNavegacionTimeOut = (int) ParametrosDn.detalleNavegacionTimeOut;
			operador = (String) ParametrosDn.operador;
			numero = (int) ParametrosDn.numero;
			nroDecimales = (int) ParametrosDn.cantidadDecimales;
			expresionRegularNroTigo = (String) ParametrosDn.expresionRegularNroTigo;
			cantidadDiasConsultaPorDia = (int) ParametrosDn.cantidadDiasConsultaPorDia;
			cantidadDiasConsultaPorHora = (int) ParametrosDn.cantidadDiasConsultaPorHora;
			wsdlLoc = (String) ParametrosDn.detalleNavegacionWsdl;
			topAplicacionesReporte = (int) ParametrosDn.topAplicacionesReporte;
			textoReporteSize = (int) ParametrosDn.textoReporteSize;
			textoReporteFuente = (String) ParametrosDn.textoReporteFuente;
			textoReporte = (String) ParametrosDn.textoReporte;
			columna1 = (String) ParametrosDn.columna1;
			columna2 = (String) ParametrosDn.columna2;
			columna3 = (String) ParametrosDn.columna3;
			columna4 = (String) ParametrosDn.columna4;

			textoReporteFuenteHeader = ParametrosDn.textoHeaderReporteFuente;
			textoReporteHeaderSize = ParametrosDn.textoHeaderReporteSize;
			textoReporteHeader = ParametrosDn.textoHeaderReporte;

		} catch (Exception e) {
			log.error("Error al cargar parametros del Sistema: ", e);
			SysMessage.error("Error al cargar parametros del Sistema: " + e.getMessage(), null);
			return;
		}

		while (continuar) {
			continuar = false;
			try {

				if (isdn == null || isdn.trim().isEmpty()) {
					SysMessage.warn("El campo Nro de Cuenta no es válido", null);
					return;
				}
				if (!UtilNumber.esNroTigo(isdn, expresionRegularNroTigo)) {
					SysMessage.warn("Nro de cuenta no válido", null);
					return;
				}
				if (fechaInicio == null) {
					SysMessage.warn("El campo Fecha Inicio no es válido", null);
					return;
				}
				if (fechaFin == null) {
					SysMessage.warn("El campo Fecha Fin no es válido", null);
					return;
				}

				Calendar fInicio = Calendar.getInstance();
				Calendar fFin = Calendar.getInstance();
				Calendar fActual = Calendar.getInstance();
				Calendar fHoy = Calendar.getInstance();

				fActual.set(Calendar.HOUR_OF_DAY, 0);
				fActual.set(Calendar.MINUTE, 0);
				fActual.set(Calendar.SECOND, 0);
				fActual.set(Calendar.MILLISECOND, 0);
				log.debug("fechaActual: " + UtilDate.dateToString(fActual.getTime(), "dd-MM-yyyy HH:mm:ss"));

				fInicio.setTimeInMillis(fechaInicio.getTime());
				fFin.setTimeInMillis(fechaFin.getTime());

				if (porHora) {
					fInicio.set(Calendar.HOUR_OF_DAY, Integer.parseInt(selectHoraInicio));
					fFin.set(Calendar.HOUR_OF_DAY, Integer.parseInt(selectHoraFin));
				}

				fHoy.set(Calendar.MINUTE, 0);
				fHoy.set(Calendar.SECOND, 0);
				fHoy.set(Calendar.MILLISECOND, 0);

				boolean esDiaMenorAlActual = false;
				if (porDia) {
					fInicio.set(Calendar.HOUR_OF_DAY, 0);
					fFin.set(Calendar.HOUR_OF_DAY, fHoy.get(Calendar.HOUR_OF_DAY));

					Calendar cHoyDia = Calendar.getInstance();
					cHoyDia.set(Calendar.HOUR_OF_DAY, 0);
					cHoyDia.set(Calendar.MINUTE, 0);
					cHoyDia.set(Calendar.SECOND, 0);
					cHoyDia.set(Calendar.MILLISECOND, 0);

					Calendar cFin = Calendar.getInstance();
					cFin.setTimeInMillis(fFin.getTimeInMillis());
					cHoyDia.set(Calendar.HOUR_OF_DAY, 0);
					cHoyDia.set(Calendar.MINUTE, 0);
					cHoyDia.set(Calendar.SECOND, 0);
					cHoyDia.set(Calendar.MILLISECOND, 0);

					if (cFin.getTimeInMillis() < cHoyDia.getTimeInMillis()) {
						fFin.set(Calendar.HOUR_OF_DAY, 23);
						esDiaMenorAlActual = true;
					}

				}
				// if (porDia) {
				// fInicio.set(Calendar.HOUR_OF_DAY, 0);
				// fInicio.set(Calendar.MINUTE, 0);
				// fInicio.set(Calendar.SECOND, 0);
				// fInicio.set(Calendar.MILLISECOND, 0);
				//
				// fFin.set(Calendar.HOUR_OF_DAY, 0);
				// fFin.set(Calendar.MINUTE, 0);
				// fFin.set(Calendar.SECOND, 0);
				// fFin.set(Calendar.MILLISECOND, 0);
				//
				// fActual.add(Calendar.DAY_OF_MONTH, -(ParametrosDn.cantidadDiasConsultaPorDia - 1));
				// fHoy.set(Calendar.HOUR_OF_DAY, 0);
				// } else {
				fInicio.set(Calendar.MINUTE, 0);
				fInicio.set(Calendar.SECOND, 0);
				fInicio.set(Calendar.MILLISECOND, 0);

				fInicio.set(Calendar.MINUTE, 0);
				fInicio.set(Calendar.SECOND, 0);
				fInicio.set(Calendar.MILLISECOND, 0);

				if (porHora) {
					fActual.add(Calendar.DAY_OF_MONTH, -(cantidadDiasConsultaPorHora - 1));
				} else {
					fActual.add(Calendar.DAY_OF_MONTH, -(cantidadDiasConsultaPorDia - 1));
				}
				// }

				log.debug("fechaInicio: " + UtilDate.dateToString(fInicio.getTime(), "dd-MM-yyyy HH:mm:ss") + ", fechaFin: " + UtilDate.dateToString(fFin.getTime(), "dd-MM-yyyy HH:mm:ss"));
				log.debug("fechaActual restada: " + UtilDate.dateToString(fActual.getTime(), "dd-MM-yyyy HH:mm:ss"));
				log.debug("fechaHoy: " + UtilDate.dateToString(fHoy.getTime(), "dd-MM-yyyy HH:mm:ss"));

				if (fFin.getTimeInMillis() > fHoy.getTimeInMillis()) {
					log.debug("La Fecha Fin tiene que ser menor o igual que la Fecha Actual del Sistema");
					SysMessage.warn("La Fecha Fin tiene que ser menor o igual que la Fecha Actual del Sistema", null);
					return;
				}

				if (fFin.getTimeInMillis() < fInicio.getTimeInMillis()) {
					log.debug("La Fecha Inicio tiene que ser menor o igual que la Fecha Fin");
					SysMessage.warn("La Fecha Inicio tiene que ser menor o igual que la Fecha Fin", null);
					return;
				}

				if (fInicio.getTimeInMillis() < fActual.getTimeInMillis()) {
					if (porHora) {
						log.debug("Solo se puede consultar de los ultimos " + cantidadDiasConsultaPorHora + " dias");
						SysMessage.warn("Solo se puede consultar de los ultimos " + cantidadDiasConsultaPorHora + " dias", null);
						return;
					} else {
						log.debug("Solo se puede consultar de los ultimos " + cantidadDiasConsultaPorDia + " dias");
						SysMessage.warn("Solo se puede consultar de los ultimos " + cantidadDiasConsultaPorDia + " dias", null);
						return;
					}
					// return;
				}

				// disabledButton = true;

				String tiempo = Constante.HORA;
				if (porDia) {
					tiempo = Constante.DIA;
				}

				String categoria = Constante.APLICACION;
				if (porSubAplicacion) {
					categoria = Constante.SUBAPLICACION;
				}

				if (idAuditoria == 0) {
					List<VcLogAdicional> adicionales = new ArrayList<>();
					VcLogAdicional ad1 = new VcLogAdicional();
					ad1.setCodigo("Fecha Inicio");
					ad1.setValor(UtilDate.dateToString(fInicio.getTime(), Parametros.fechaFormatPage));
					VcLogAdicional ad2 = new VcLogAdicional();
					ad2.setCodigo("Fecha Fin");
					ad2.setValor(UtilDate.dateToString(fFin.getTime(), Parametros.fechaFormatPage));
					VcLogAdicional ad3 = new VcLogAdicional();
					ad3.setCodigo("Periodo");
					ad3.setValor(tiempo);
					VcLogAdicional ad4 = new VcLogAdicional();
					ad4.setCodigo("NIVEL DETALLE");
					ad4.setValor(categoria);
					VcLogAdicional ad5 = new VcLogAdicional();
					ad5.setCodigo("Formato");
					ad5.setValor(tipo.toUpperCase());

					adicionales.add(ad1);
					adicionales.add(ad2);
					adicionales.add(ad3);
					adicionales.add(ad4);
					adicionales.add(ad5);

					idAuditoria = guardarAuditoria(Calendar.getInstance(), this.getLogin(), this.ip, Parametros.vistaDetalleNavegacionExportar, Parametros.comandoDetalleNavegacionExportar, this.isdn, adicionales);

					if (idAuditoria <= 0) {
						log.debug("Error al guardar log de auditoria");
						SysMessage.error("Error al guardar log de auditoria", null);
						return;
					}
				}

				disableIncidente = false;
				vistaIncidente = DescriptorBitacora.DETALLE_NAVEGACION_EXPORTAR;

				try {
					controlerBitacora.accion(DescriptorBitacora.DETALLE_NAVEGACION_EXPORTAR, "Se exportó detalle de navegacion con datos, formato: " + tipo + ", isdn: " + isdn + ", tiempo: " + tiempo + ", categoria: " + categoria + ", fechaInicio: "
							+ UtilDate.dateToString(fInicio.getTime(), "dd-MM-yyyy HH:mm:ss") + ", fechaFin: " + UtilDate.dateToString(fFin.getTime(), "dd-MM-yyyy HH:mm:ss"));
				} catch (Exception e) {
					log.error("Error al guardar bitacora en el sistema: ", e);
					SysMessage.error("Error al guardar bitacora en el sistema", null);
				}

				long ini = System.currentTimeMillis();
				HashMap<String, String> hashTopNombres = new HashMap<>();

				// log.debug("consultando servicion .......");
				// DetalleNavegacionServiceSoapBindingStub port = ServicioDetalleNavegacion.initPort(wsdlLoc, "DETALLE_NAVEGACION_" + login, ParametrosDn.detalleNavegacionTimeOut);
				NodoServicio<DetalleNavegacionServiceSoapBindingStub> portNodo = ServicioDetalleNavegacion.initPort(wsdlLoc, "DETALLE_NAVEGACION_" + getLogin(), detalleNavegacionTimeOut);

				if (portNodo != null && portNodo.getPort() != null) {

					synchronized (portNodo) {

						try {
							DetalleNavegacionServiceSoapBindingStub port = portNodo.getPort();

							// --------- PIE CHART

							ini = System.currentTimeMillis();
							// ResponsePorcentaje responsePie = port.getDetalleNavegacionPorcentaje(isdn, fInicio, fFin, "HORA_REPORTE", categoria);
							ResponsePorcentaje responsePie = port.getDetalleNavegacionPorcentaje(isdn, fInicio, fFin, tiempo, categoria);
							long fin = System.currentTimeMillis();
							log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio DetalleNavegacion getDetalleNavegacionPorcentaje: " + (fin - ini) + " milisegundos");

							Long Con = null;
							if (portNodo.isPrimeraVez()) {
								Con = portNodo.getTiempoConexion();
							}

							if (Parametros.saveRequestResponse) {
								try {
									saveXml(isdn, getLogin(), wsdlLoc, "getDetalleNavegacionPorcentaje", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoria, Con, (fin - ini));
								} catch (Exception e) {
									log.warn("No se logro registrar los request y response del servicio DetalleNavegacion, getDetalleNavegacionPorcentaje: ", e);
								}
							}

							if (responsePie != null) {
								if (responsePie.getCode() != null) {

									log.debug("[isdn: " + getIsdn() + "] codigo: " + responsePie.getCode() + ", descripcion: " + responsePie.getDescription());

									if (responsePie.getCode().intValue() == 0) {
										Porcentaje[] lista = responsePie.getLista();

										if (lista != null) {
											double totalReporte = 0;
											long totalBytes = 0;
											int contador = 1;
											long otrosBytes = 0;
											boolean swOtrosBytes = false;

											for (Porcentaje item : lista) {

												if (contador > topAplicacionesReporte) {
													long total = item.getTotal();
													totalBytes = totalBytes + total;
													otrosBytes = otrosBytes + total;
													swOtrosBytes = true;

													ScriptEngineManager manager = new ScriptEngineManager();
													ScriptEngine se = manager.getEngineByName("JavaScript");

													if (se != null) {
														// Object result = se.eval("3.2 * 5.5");
														Object resultTotal = se.eval(total + operador + numero);
														double parseTotal = Double.parseDouble(String.valueOf(resultTotal));
														parseTotal = UtilNumber.redondear(parseTotal, nroDecimales);
														totalReporte = totalReporte + parseTotal;
													}
												} else {
													contador++;

													hashTopNombres.put(item.getNombre(), item.getNombre());
													long total = item.getTotal();
													totalBytes = totalBytes + total;

													ScriptEngineManager manager = new ScriptEngineManager();
													ScriptEngine se = manager.getEngineByName("JavaScript");

													if (se != null) {
														// Object result = se.eval("3.2 * 5.5");
														Object resultTotal = se.eval(total + operador + numero);
														double parseTotal = Double.parseDouble(String.valueOf(resultTotal));
														parseTotal = UtilNumber.redondear(parseTotal, nroDecimales);
														// totalConsumo = totalConsumo + parseTotal;
														listPieConsumo.add(new PieConsumo(item.getNombre(), 0.0, parseTotal, this.tipoUnidad));

														// if (item.getPorcentaje() > 0) {
														totalReporte = totalReporte + parseTotal;
														// listPies.add(new Pie3DDouble(item.getNombre(), 0.0, parseTotal));
														// }
													} else {
														log.error("ScriptEngine resuelto a nulo");
													}
												}
											}

											if (swOtrosBytes) {
												ScriptEngineManager manager = new ScriptEngineManager();
												ScriptEngine se = manager.getEngineByName("JavaScript");

												if (se != null) {
													// Object result = se.eval("3.2 * 5.5");
													Object resultTotal = se.eval(otrosBytes + operador + numero);
													double parseTotal = Double.parseDouble(String.valueOf(resultTotal));
													parseTotal = UtilNumber.redondear(parseTotal, nroDecimales);
													listPieConsumo.add(new PieConsumo("Otros", 0.0, parseTotal, this.tipoUnidad));
												}
											}

											for (PieConsumo item : listPieConsumo) {
												double porcentaje = (item.getConsumo() * (double) 100) / totalReporte;
												item.setPorcentaje(UtilNumber.redondear(porcentaje, nroDecimales));

												if (item.getConsumo().isNaN() || item.getConsumo().isInfinite()) {
													item.setConsumo(0.0);
												}
												if (item.getPorcentaje().isNaN() || item.getPorcentaje().isInfinite()) {
													item.setPorcentaje(0.0);
												}
											}

											// for (Pie3DDouble item : listPies) {
											// double porcentaje = ((double) item.getM() * (double) 100) / totalReporte;
											// item.setY(UtilNumber.redondear(porcentaje, nroDecimales));
											//
											// }
											ScriptEngineManager manager = new ScriptEngineManager();
											ScriptEngine se = manager.getEngineByName("JavaScript");

											if (se != null) {
												// log.debug("totalBytes: " + totalBytes);
												Object resultTotal = se.eval(totalBytes + operador + numero);
												totalConsumo = Double.parseDouble(String.valueOf(resultTotal));
											}
											totalConsumo = UtilNumber.redondear(totalConsumo, nroDecimales);
											// totalConsumo = UtilNumber.redondear(totalConsumo, nroDecimales);
											log.debug("PORCENTAJE totalConsumo: " + totalConsumo);
										} else {
											log.warn("[isdn: " + getIsdn() + "] lista resuelto a nulo");
										}
									} else {
										SysMessage.warn("codigo: " + responsePie.getCode() + ", descripcion: " + responsePie.getDescription(), null);
									}
								} else {
									log.warn("[isdn: " + getIsdn() + "] responsePie.getCode resuelto a nulo");
								}
							} else {
								log.warn("[isdn: " + getIsdn() + "] responsePie resuelto a nulo");
							}

							// if (title.equals("tab1")) { // PORCENTAJE
							// if (listPies.size() > 0) {
							// dataJsonPorcentaje = toGsonDouble(listPies);
							// callJavaScriptPorcentaje();
							// } else {
							// createPieModel();
							// }
							// }

							// --------- TABLA RESUMEN -------

							String categoriaAux = "";
							if (categoria.equals(Constante.APLICACION)) {
								if (tiempo.equals(Constante.DIA)) {
									categoriaAux = Constante.APLICACION_DIA;
								} else {
									categoriaAux = Constante.APLICACION;
								}
							} else {
								if (tiempo.equals(Constante.DIA)) {
									categoriaAux = Constante.SUBAPLICACION_DIA;
								} else {
									categoriaAux = Constante.SUBAPLICACION;
								}
							}

							if (exportarDetalle) {
								ResponseReporte response = port.getConsumoPorHora(isdn, fInicio, fFin, categoriaAux);
								fin = System.currentTimeMillis();
								log.debug("[isdn: " + getIsdn() + "] Tiempo de respuesta del servicio DetalleNavegacion: " + (fin - ini) + " milisegundos");

								Con = null;
								if (portNodo.isPrimeraVez()) {
									Con = portNodo.getTiempoConexion();
								}

								if (Parametros.saveRequestResponse) {
									try {
										saveXml(isdn, getLogin(), wsdlLoc, "getConsumoPorHora", port._getCall().getMessageContext().getRequestMessage().getSOAPPartAsString(), port._getCall().getResponseMessage().getSOAPPartAsString(), idAuditoria, Con, (fin - ini));
									} catch (Exception e) {
										log.warn("No se logro registrar los request y response del servicio DetalleNavegacion, getConsumoPorHora: ", e);
									}
								}

								if (response != null) {
									if (response.getCode() != null) {

										log.debug("[isdn: " + getIsdn() + "] codigo: " + response.getCode() + ", descripcion: " + response.getDescription());

										if (response.getCode().intValue() == 0) {

											// long totalBytes = 0;

											Reporte[] lista = response.getLista();
											if (lista != null) {

												// HashMap<String, String> hashFecha = new HashMap<>();

												ScriptEngineManager manager = new ScriptEngineManager();
												ScriptEngine se = manager.getEngineByName("JavaScript");

												for (Reporte item : lista) {

													double totalParcial = 0;
													if (se != null) {
														Object resultTotal = se.eval(item.getTotal() + operador + numero);
														totalParcial = Double.parseDouble(String.valueOf(resultTotal));
														totalParcial = UtilNumber.redondear(totalParcial, nroDecimales);
													}

													if (Double.isNaN(totalParcial) || Double.isInfinite(totalParcial)) {
														totalParcial = 0;
													}
													Date itemDate = UtilDate.stringToDate(item.getFecha(), "dd/MM/yyyy");
													String dia = UtilDate.dateToString(itemDate, "dd");
													String mes = UtilDate.dateToString(itemDate, "MM");
													String anio = UtilDate.dateToString(itemDate, "yy");

													String itemFecha = Integer.parseInt(dia) + "/" + Integer.parseInt(mes) + "/" + anio;

													if (porDia) {
														listConsumo.add(new Consumo(itemFecha, "", item.getNombre(), UtilNumber.doubleToString(totalParcial, nroDecimales)));
													} else {
														listConsumo.add(new Consumo(itemFecha, item.getHora() + ":00 - " + item.getHora() + ":59", item.getNombre(), UtilNumber.doubleToString(totalParcial, nroDecimales)));
													}
												}
												// listConsumo = listConsumo;
											} else {
												log.debug("Lista de consulta resuelto a nulo");
											}

										} else {
											SysMessage.warn("codigo: " + response.getCode() + ", descripcion: " + response.getDescription(), null);
										}
									} else {
										log.warn("[isdn: " + getIsdn() + "] response.getCode resuelto a nulo");
									}
								} else {
									log.debug("[isdn: " + getIsdn() + "] Respuesta resulto a nulo, isdn", null);
								}
							}
							log.debug("listConsumo.size: " + listConsumo);
							log.debug("listPieConsumo.size: " + listPieConsumo);

							// --------------- EXPORT PDF - EXCEL ------------------
							FacesContext faces = FacesContext.getCurrentInstance();
							ServletContext context = (ServletContext) faces.getExternalContext().getContext();
							String pathFile = context.getRealPath("/WEB-INF/template/report.jrxml");
							// String pathFile = "C:/Users/Jose Luis/Desktop/configuracion/pie/report.jrxml";
							// -- custom
							String source = UtilFile.fileToString(pathFile, textoReporteFuente, textoReporteSize + "", textoReporteFuenteHeader, textoReporteHeaderSize + "", ParametrosDn.colorReporte);
							// log.debug(source);
							InputStream inputReport = new ByteArrayInputStream(source.getBytes("UTF-8"));
							JasperDesign jasperDesign = JRXmlLoader.load(inputReport);

							// JasperDesign jasperDesign = JRXmlLoader.load(pathFile);
							if (!exportarDetalle) {
								jasperDesign.removeGroup("Group2");
							}
							if (!porHora && exportarDetalle) {
								jasperDesign.getGroupsMap().get("Group2").getGroupHeaderSection().getBands()[0].getElementByKey("hora").setWidth(0);
								jasperDesign.getGroupsMap().get("Group2").getGroupHeaderSection().getBands()[0].getElementByKey("fecha").setWidth(jasperDesign.getGroupsMap().get("Group2").getGroupHeaderSection().getBands()[0].getElementByKey("fecha").getWidth() + 60);
								jasperDesign.getGroupsMap().get("Group2").getGroupHeaderSection().getBands()[0].getElementByKey("app").setWidth(jasperDesign.getGroupsMap().get("Group2").getGroupHeaderSection().getBands()[0].getElementByKey("app").getWidth() + 50);
								jasperDesign.getGroupsMap().get("Group2").getGroupHeaderSection().getBands()[0].getElementByKey("app").setX(jasperDesign.getGroupsMap().get("Group2").getGroupHeaderSection().getBands()[0].getElementByKey("app").getX() - 50);

								jasperDesign.getDetailSection().getBands()[0].getElementByKey("horad").setWidth(0);
								jasperDesign.getDetailSection().getBands()[0].getElementByKey("fechad").setWidth(jasperDesign.getDetailSection().getBands()[0].getElementByKey("fechad").getWidth() + 60);
								jasperDesign.getDetailSection().getBands()[0].getElementByKey("appd").setWidth(jasperDesign.getDetailSection().getBands()[0].getElementByKey("appd").getWidth() + 50);
								jasperDesign.getDetailSection().getBands()[0].getElementByKey("appd").setX(jasperDesign.getDetailSection().getBands()[0].getElementByKey("appd").getX() - 50);
							}
							JasperReport reporte = JasperCompileManager.compileReport(jasperDesign);

							// loading subreport
							String pathFileSubReport = context.getRealPath("/WEB-INF/template/report_chart.jrxml");
							// String pathFileSubReport = "C:/Users/Jose Luis/Desktop/configuracion/pie/report_chart.jrxml";
							JasperDesign jasperDesignSubReport = JRXmlLoader.load(pathFileSubReport);
							JasperReport subreport = JasperCompileManager.compileReport(jasperDesignSubReport);

							String logo = context.getRealPath("/WEB-INF/template/logo_tigo.png");

							// Preparing parameters
							Map<String, Object> params = new HashMap<String, Object>();
							params.put("isdn", isdn);
							params.put("logo", logo);
							params.put("totalConsumo", UtilNumber.doubleToString(totalConsumo, nroDecimales));
							// params.put("fechaInicio", "10/01/2017");

							String dia = UtilDate.dateToString(fInicio.getTime(), "dd");
							String mes = UtilDate.dateToString(fInicio.getTime(), "MM");
							String anio = UtilDate.dateToString(fInicio.getTime(), "yy");
							String hora = UtilDate.dateToString(fInicio.getTime(), "HH:mm");
							String iFechaInicio = Integer.parseInt(dia) + "/" + Integer.parseInt(mes) + "/" + anio + " " + hora + " hrs";
							params.put("fechaInicio", iFechaInicio);

							if (porDia) {
								if (esDiaMenorAlActual) {
									fFin.set(Calendar.MINUTE, 59);
								}
							}
							dia = UtilDate.dateToString(fFin.getTime(), "dd");
							mes = UtilDate.dateToString(fFin.getTime(), "MM");
							anio = UtilDate.dateToString(fFin.getTime(), "yy");
							hora = UtilDate.dateToString(fFin.getTime(), "HH:mm");
							String iFechaFin = Integer.parseInt(dia) + "/" + Integer.parseInt(mes) + "/" + anio + " " + hora + " hrs";
							params.put("fechaFin", iFechaFin);

							Date fechaHoy = new Date();
							dia = UtilDate.dateToString(fechaHoy, "dd");
							mes = UtilDate.dateToString(fechaHoy, "MM");
							anio = UtilDate.dateToString(fechaHoy, "yy");
							hora = UtilDate.dateToString(fechaHoy, "HH:mm");
							String iFechaImpresion = Integer.parseInt(dia) + "/" + Integer.parseInt(mes) + "/" + anio + " " + hora + " hrs";
							params.put("fechaImpresion", iFechaImpresion);

							// params.put("fechaInicio", UtilDate.dateToString(fInicio.getTime(), "dd/MM/yyyy HH:mm"));
							// params.put("fechaFin", UtilDate.dateToString(fFin.getTime(), "dd/MM/yyyy HH:mm"));
							// params.put("fechaImpresion", UtilDate.dateToString(new Date(), "dd/MM/yyyy hh:mm a"));
							params.put("subreportParameter", subreport);
							params.put("unidad", tipoUnidad);

							params.put("columna1", columna1);
							if (porHora) {
								params.put("columna2", columna2);
							} else {
								params.put("columna2", "");
							}
							params.put("columna3", columna3);
							params.put("columna4", columna4);

							params.put("mensaje", textoReporte);
							params.put("titulo", textoReporteHeader);
							params.put("dataSourceSubReport", new JRBeanCollectionDataSource(listPieConsumo));
							JasperPrint jasperPrint;
							if (!exportarDetalle) {
								jasperPrint = JasperFillManager.fillReport(reporte, params);
							} else {
								jasperPrint = JasperFillManager.fillReport(reporte, params, new JRBeanCollectionDataSource(listConsumo));
							}

							// PDF
							// JasperExportManager.exportReportToPdfFile(jasperPrint, "E:/simple_report.pdf");
							// exporting to excel file
							// String name = "report_excel" + (new Date()).getTime() + ".xls";
							// String path = "E:/" + name;// context.getRealPath("/WEB-INF/" + name);
							// File destFile = new File(path);
							// boolean fvar = destFile.createNewFile();
							// if (fvar) {
							// JRXlsxExporter exportador = new JRXlsxExporter();
							// exportador.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
							// exportador.setParameter(JRExporterParameter.OUTPUT_FILE, destFile);
							// exportador.exportReport();
							// }

							String contextPath = context.getRealPath(File.separator);
							// log.debug("contextPath: " + contextPath);
							String name = "reporte_" + (new Date()).getTime() + (tipo.equals("pdf") ? ".pdf" : ".xlsx");
							// log.debug("name: " + name);

							if (tipo.equals("pdf")) {
								JasperExportManager.exportReportToPdfFile(jasperPrint, contextPath + name);
							} else {
								JRXlsxExporter exportador = new JRXlsxExporter();
								// exportador.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
								// este parametro es para decirle que todo el reporte se genera en
								// una sola hoja de excel
								// exportador.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, false);
								// con este le decimos que detecte el tipo de celda.
								// exportador.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, true);
								// El siguiente es para decirle que no ignore los bordes de las
								// celdas, y el ultimo es para que no use un fondo blanco, con estos
								// dos parametros jasperreport genera la hoja de excel con los
								// bordes de las celdas.
								// exportador.setParameter(JRXlsExporterParameter.IS_IGNORE_CELL_BORDER, false);
								// exportador.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, false);
								exportador.setParameter(net.sf.jasperreports.engine.JRExporterParameter.JASPER_PRINT, jasperPrint);
								exportador.setParameter(net.sf.jasperreports.engine.JRExporterParameter.OUTPUT_FILE, new File(contextPath + name));
								exportador.exportReport();
							}

							byte[] bytesFile = getBytesFromFile(new File(contextPath + name));
							// HttpServletResponse hsr = (HttpServletResponse) faces.getExternalContext().getResponse();
							// hsr.setContentType("application/octet-stream");
							// // hsr.setContentType("application/pdf");
							// hsr.setContentLength(bytesFile.length);
							//
							// hsr.setHeader("Content-disposition", "attachment; filename=\"" + name + "\"");
							// ServletOutputStream out;
							// out = hsr.getOutputStream();
							// out.write(bytesFile);
							// out.close();

							InputStream stream = new ByteArrayInputStream(bytesFile);
							file = new DefaultStreamedContent(stream, "application/octet-stream", name);

							try {
								File f = new File(contextPath + name);
								boolean delete = f.delete();
								log.debug("Resultado de eliminar archivo " + contextPath + name + ": " + delete);
							} catch (Exception e) {
								log.warn("Error al eliminar archivo: ", e);
							}
							faces.responseComplete();

							log.debug("Exportador finalizado.....");

							// RequestContext.getCurrentInstance().execute("PF('dPorcentaje').hide();");

						} finally {
							portNodo.setEnUso(false);
							portNodo.setFechaFin(new Date());
						}
					}
				} else {
					log.error("[isdn: " + getIsdn() + "] Port DetalleNavegacion resuelto a nulo");
				}

				try {
					DnConsulta dc = new DnConsulta();
					dc.setIp(ip);
					dc.setUsuario(getLogin());
					dc.setIsdn(isdn);
					dc.setTiempo(tiempo);
					dc.setCategoria(categoria);
					dc.setReporte("DETALLE NAVEGACION REPORTE");
					dc.setFechaInicio(fInicio.getTime());
					dc.setFechaFin(fFin.getTime());
					Calendar cal = Calendar.getInstance();
					cal.set(Calendar.HOUR_OF_DAY, 0);
					cal.set(Calendar.MINUTE, 0);
					cal.set(Calendar.SECOND, 0);
					cal.set(Calendar.MILLISECOND, 0);
					dc.setFecha(cal.getTime());
					dc.setFechaCreacion(new Date());
					consultaBl.save(dc);
				} catch (Exception e) {
					log.error("Error al guardar consulta: ", e);
					SysMessage.error("Error al guardar consulta en el sistema", null);
				}

				SysMessage.info("Consulta finalizada", null);

			} catch (AxisFault a) {
				String msg = a.getMessage();
				msg.toLowerCase();

				if (reintento < reintentos && a.getCause() != null && a.getCause().getLocalizedMessage().trim().equals("Read timed out")) {
					// GotoFactory.getSharedInstance().getGoto().go(4);
					log.debug("Reintento de conexion por time out nro: " + (reintento + 1));
					reintento++;
					continuar = true;
					try {
						Thread.sleep(esperaReintentos);
					} catch (Exception e) {
						e.getMessage();
					}
				} else {
					guardarExcepcion(idAuditoria, "Error AxisFault de conexion al servicio DetalleNavegacion: " + stackTraceToString(a));
					if (msg.contains("Connection reset".toLowerCase()) || msg.contains("Read timed out".toLowerCase()) || msg.contains("Bad Request".toLowerCase()) || msg.contains("Connection timed out".toLowerCase()) || msg.contains("UnknownHostException".toLowerCase())
							|| msg.contains("No route to host: connect".toLowerCase())) {
						log.error("Error de conexion al servicio DetalleNavegacion: ", a);
						SysMessage.error("Error de conexion del servicio DetalleNavegacion", null);
					} else {
						log.error("Error AxisFault: ", a);
						SysMessage.error("Error de conexion del servicio DetalleNavegacion: " + a.getMessage(), null);
					}
				}
			} catch (ServiceException e) {
				log.error("Error de servicio al conectarse al servicio DetalleNavegacion: ", e);
				guardarExcepcion(idAuditoria, "Error de servicio al conectarse al servicio DetalleNavegacion: " + stackTraceToString(e));
				SysMessage.error("Error de servicio al conectarse al servicio DetalleNavegacion", null);
			} catch (RemoteException e) {
				log.error("Error remoto al conectarse al servicio DetalleNavegacion: ", e);
				guardarExcepcion(idAuditoria, "Error remoto al conectarse al servicio DetalleNavegacion: " + stackTraceToString(e));
				SysMessage.error("Error remoto al conectarse al servicio DetalleNavegacion", null);
			} catch (Exception e) {
				log.error("Error al consultar detalle de navegacion", e);
				guardarExcepcion(idAuditoria, "Error al consultar detalle de navegacion: " + stackTraceToString(e));
				SysMessage.error("Error al consultar detalle de navegacion: " + e.getLocalizedMessage(), null);
			}
		}
	}

	public static byte[] getBytesFromFile(File file) {
		log.debug("Ingresando a obtener bytes de archivo");
		InputStream is = null;
		byte[] bytes = null;
		try {
			is = new FileInputStream(file);

			// Get the size of the file
			long length = file.length();

			if (length > Integer.MAX_VALUE) {
				// File is too large
			}

			// Create the byte array to hold the data
			bytes = new byte[(int) length];

			// Read in the bytes
			int offset = 0;
			int numRead = 0;
			while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
				offset += numRead;
			}

			// Ensure all the bytes have been read in
			if (offset < bytes.length) {
				throw new IOException("Could not completely read file " + file.getName());
			}

			// Close the input stream and return bytes
			// is.close();

		} catch (FileNotFoundException e) {
			log.error("No existe el archivo = " + file.getName(), e);
		} catch (IOException e) {
			log.error("Error obteniendo bytes del archivo = " + file.getName(), e);
		} catch (Exception e) {
			log.error("Error obteniendo bytes del archivo = " + file.getName(), e);
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
				}
			}
		}
		return bytes;
	}

	public void porHoraDia(String item) {
		// log.debug("*****************************************************************************************");
		// log.debug("item: " + item);
		// log.debug("porDia: " + porDia);
		// log.debug("porHora: " + porHora);
		// log.debug("*****************************************************************************************");
		if (item.equals(Constante.HORA)) {
			disabledComboInicio = false;
			disabledComboFin = false;
			if (porHora) {
				porDia = false;
				pattern = "dd-MM-yyyy";
			} else {
				porDia = true;
				pattern = "dd-MM-yyyy";
			}
		} else {
			if (porDia) {
				porHora = false;
				pattern = "dd-MM-yyyy";
			} else {
				porHora = true;
				pattern = "dd-MM-yyyy";
			}
			disabledComboInicio = true;
			disabledComboFin = true;
		}
		// log.debug("porDia: " + porDia);
		// log.debug("porHora: " + porHora);
	}

	public void porAplicacionSubAplicacion(String item) {
		if (item.equals(Constante.APLICACION)) {
			if (porAplicacion) {
				porSubAplicacion = false;
			} else {
				porSubAplicacion = true;
			}
		} else {
			if (porSubAplicacion) {
				porAplicacion = false;
			} else {
				porAplicacion = true;
			}
		}
	}

	public void conOSinDetalle(String item) {
		if (item.equals("CON_DETALLE")) {
			if (this.exportarDetalle) {
				this.exportarSinDetalle = false;
			} else {
				this.exportarSinDetalle = true;
			}
		} else if (this.exportarSinDetalle) {
			this.exportarDetalle = false;
		} else {
			this.exportarDetalle = true;
		}
	}

	private void cargarIp() {
		try {
			// ip = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRemoteAddr();

			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();

			ip = UtilUrl.getClientIp(request);
			log.debug("ip cliente: " + ip);

		} catch (Exception e) {
			log.error("Error al obtener ip: ", e);
		}
	}

	private void obtenerUsuario() {
		try {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			this.setLogin((String) request.getSession().getAttribute("TEMP$USER_NAME"));
			// log.info("login: " + login);
		} catch (Exception e) {
			log.error("Error al obtener usuario: ", e);
		}
	}

	public void guardarIncidente() {
		log.debug("Ingresando..");

		try {
			if (CodigoAuditoria > 0) {
				VcLogAuditoria find = (VcLogAuditoria) blAuditoria.find(CodigoAuditoria, VcLogAuditoria.class);

				if (disableIncidente && find != null) {
					SysMessage.warn(Parametros.mensajeValidacionIncidente, null);
					return;
				}
				VcIncidente item = new VcIncidente();
				item.setFechaIncidente(Calendar.getInstance());
				item.setAtencion(false);
				item.setUsuario(this.login);
				item.setVcLogAuditoria(find);
				String str = blIncidente.validar(item, true);
				if (!str.isEmpty()) {
					SysMessage.warn(str, null);
					return;
				}
				blIncidente.save(item);
				// ThreadTelnetRun t = new ThreadTelnetRun(CodigoAuditoria, isdn, getLogin(),blAuditoria,blExcepcion,blConsultaRed,servidorBL,blConexionTelnet);
				// t.start();

				try {
					RequestContext reqCtx = RequestContext.getCurrentInstance();
					reqCtx.execute("PF('pollTelnet').start();");
				} catch (Exception e) {
					log.error("Error: " + e.getMessage());
				}

				controlButtonTelnet.setDisabled(true);
				controlButtonTelnet.setPollStop(false);
				controlButtonTelnet.setFinalizado(false);
				controlButtonTelnet.setErrores(new ArrayList<String>());

				Thread t = new Thread(new Runnable() {

					@Override
					public void run() {
						boolean live = true;

						while (live) {
							try {
								log.debug("Conectando... ");

								ConexionElementosRed(CodigoAuditoria, isdn, getLogin());

								live = false;// finish the thread
							} catch (Exception e) {
								log.warn("Error al conectar el hilo telnet: ", e);

							} finally {
								live = false;
								log.debug("Finalizo el hilo: Elementos Red.");

								controlButtonTelnet.setDisabled(false);
								controlButtonTelnet.setFinalizado(true);
								log.debug("FINALIZARON TODOS LOS HILOS DE ELEMENTOS RED controlButton.disabled: " + controlButtonTelnet.getDisabled());
								try {
									Thread.sleep(3000);
								} catch (Exception e2) {
									e2.getMessage();
								}
								controlButtonTelnet.setPollStop(true);
								log.debug("FINALIZARON TODOS LOS HILOS DE ELEMENTOS RED controlButton.PollStop: " + controlButtonTelnet.getPollStop());
							}
						}

					}

				});
				t.start();

				// controlButtonTelnet.setDisabled(true);
				// controlButtonTelnet.setPollStop(false);
				// controlButtonTelnet.setFinalizado(false);
				// controlButtonTelnet.setErrores(new ArrayList<String>());

				controlerBitacora.accion(this.vistaIncidente, "Se adiciono " + DescriptorBitacora.INCIDENTE + " con Id: " + String.valueOf(item.getIdIncidente()));
				SysMessage.info("Incidente Nro. " + String.valueOf(item.getIdIncidente()) + ": " + Parametros.mensajeReporteIncidente, null);
				disableIncidente = true;
				vistaIncidente = null;
			} else {
				SysMessage.warn(Parametros.mensajeValidacionIncidente, null);
				return;
			}
		} catch (Exception e) {
			log.error("Error al guardar incidente: ", e);
			SysMessage.error("Fallo al guardar en la Base de Datos.", null);
		}

	}

	public void refreshTelnet() {
		// log.debug(".......refresh, PollStop: " + controlButton.getPollStop()
		// + ", disableButton: " + controlButton.getDisabled());
		// log.debug(".......controlButton.getFinalizado: " +
		// controlButton.getFinalizado() + ", disableButton: " + disableButton);
		log.debug("Refresh Telnet");
		try {
			if (controlButtonTelnet.getFinalizado()) {
				RequestContext reqCtx = RequestContext.getCurrentInstance();
				reqCtx.execute("PF('pollTelnet').stop();");
				// disableButton = false;
				// log.debug("+++++++++++++++++++++++++++++++++++ stop poll");
				SysMessage.info("Consulta finalizada", null);
				if (controlButtonTelnet.getErrores() != null && controlButtonTelnet.getErrores().size() > 0) {
					List<String> errores = controlButtonTelnet.getErrores();
					for (String error : errores) {
						SysMessage.warn(error, null);
					}
					controlButtonTelnet.setErrores(null);
				}
			}
		} catch (Exception e) {
			log.error("Error: " + e.getMessage());
		}
	}

	public void ConexionElementosRed(long IdAuditoria, String isdn, String login) {

		log.debug("Entrando a elementos de red");
		try {
			String idServidorPadre = "0";

			lanzandoHilos(IdAuditoria, isdn, login, idServidorPadre, "");
		} catch (Exception e) {
			log.error("error en conexion de elementos de red: " + e);
			guardarExcepcion(IdAuditoria, "error en conexion de elementos de red: " + stackTraceToString(e));
			controlButtonTelnet.getErrores().add("error en conexion de elementos de red. ");
			// SysMessage.warn("error en conexion de elementos de red: ", null);
		}

	}

	public void lanzandoHilos(long IdAuditoria, String isdn, String login, String idServidorPadre, String tramaPadre) {
		log.debug("Iniciando con la consulta...");
		try {
			List<VcServidor> elementosRed = servidorBL.findAllActive(idServidorPadre);
			if (elementosRed == null || elementosRed.isEmpty()) {
				log.debug("No se encontraron ELEMENTO DE RED hijos de: " + idServidorPadre);
				if (idServidorPadre.equals("0"))
					controlButtonTelnet.getErrores().add("No se encontraron ELEMENTO DE RED hijos de: " + idServidorPadre);
			} else {
				log.debug("Entro para la conexion:");
				ControlTelnet control = new ControlTelnet();
				control.setCantidadHilos(elementosRed.size());
				String trama = "";
				if (!tramaPadre.isEmpty())
					trama = tramaPadre.replaceAll(" ", "");
				for (VcServidor item : elementosRed) {
					List<VcConfigElementoRed> listExcepcion = ExcepcionBL.findAllExcepciones(item.getIdServidor());
					List<VcConexionTelnet> listaNodosTelnet = new ArrayList<>();

					if (item.getKeyCodServidor() == null || item.getKeyCodServidor().isEmpty() || trama.isEmpty()) {
						log.debug("No existe keycodServidor");
						listaNodosTelnet = blConexionTelnet.findAllActive(item.getIdServidor());

					} else {
						String key = item.getKeyCodServidor() + "=";
						String codServer = trama.substring(trama.indexOf(key) + key.length(), trama.indexOf(key) + key.length() + 11);
						log.debug("El codigo key: " + key + ": " + codServer);

						try {
							listaNodosTelnet = blConexionTelnet.findAllActiveOrderByCodServidor(item.getIdServidor(), codServer);

						} catch (Exception e) {
							// TODO Auto-generated catch block
							log.error("Finalizo los hilos de elementos red de la conexion: ", e);
							guardarExcepcion(IdAuditoria, "Finalizo los hilos de elementos red de la conexion: " + stackTraceToString(e));
						}
					}
					if (listaNodosTelnet.size() == 0) {
						log.debug("No se encuentran mas nodos para este elemento de red de la conexion: " + item.getNombreServidor());
					}
					Log.debug("Se inicio el Hilo de la conexion");
					ThreadTelnet telnet = new ThreadTelnet(item, control, isdn, listaNodosTelnet, controlButtonTelnet, listExcepcion);
					telnet.start();
				}
				while (!control.finalizaron()) {
					// log.debug("Aun no finalizaron los hilos.");
					try {
						Thread.sleep(2000);
					} catch (Exception e) {
						// log.error("Finalizo los hilos: ", e);
						guardarExcepcion(IdAuditoria, "Finalizo los hilos de elementos red de la conexion: " + stackTraceToString(e));
					}
				}
				log.debug("Han finalizado a los nodos hijos de elementos de red de la conexion: " + idServidorPadre);
				if (control.getSuccessConnections().size() == 0) {
					log.debug("No se encontro ninguna trama exitosa para los nodos hijos de elementos red: " + idServidorPadre);

				} else {
					// SAVE TRAMA HERE
					for (VcConexionTelnet nodo : control.getSuccessConnections()) {
						saveXmlRed(IdAuditoria, isdn, login, nodo.getIp(), nodo.getNombreElementoRed(), nodo.getEstado());
						log.debug("TRAMA:" + nodo.getEstado());
					}
				}
				// launching the sons threads of actual elemento Red
				for (VcServidor item : elementosRed) {
					// getting trama in each elementoRed
					String tramaSuccess = "";
					if (control.getSuccessConnections().size() != 0)
						for (VcConexionTelnet nodo : control.getSuccessConnections()) {
							if (nodo.getVcServidor().getIdServidor() == item.getIdServidor()) {
								tramaSuccess = nodo.getEstado();
								break;
							}
						}
					idServidorPadre = String.valueOf(item.getIdServidor());
					lanzandoHilos(IdAuditoria, isdn, login, idServidorPadre, tramaSuccess);
				}
				for (String ExcepcionServidor : control.getExcepcionServidor()) {
					guardarExcepcion(IdAuditoria, ExcepcionServidor);
				}
				for (String Error : control.getErrores()) {
					guardarExcepcion(IdAuditoria, Error);
				}
			}
		} catch (Exception e) {
			log.error("Error al lanzar hilos de elementos red conexion: ", e);
			guardarExcepcion(IdAuditoria, "Error al lanzar hilos de elementos red conexion: " + stackTraceToString(e));
			controlButtonTelnet.getErrores().add("Error al lanzar hilos de elementos red conexion");
		}
	}

	public void saveXmlRed(long idAuditoria, String isdn, String login, String ip, String tipo, String trama) {
		try {
			VcConsultaRed R = new VcConsultaRed();
			R.setFechaCreacion(Calendar.getInstance());
			R.setIsdn(isdn);
			R.setLogin(login);
			R.setIp(ip);
			R.setTipo(tipo);
			R.setTrama(trama);

			Object find = blAuditoria.find(idAuditoria, VcLogAuditoria.class);
			R.setVcLogAuditoria((VcLogAuditoria) find);

			blConsultaRed.save(R);
		} catch (Exception e) {
			log.warn("No se registro en bd la trama: ", e);
		}
	}

	// private void validarCertificado(String pathKeystore, String ipPort) {
	// try {
	// log.debug("Ingresando a validar certificado pathKeystore: " + pathKeystore + ", ipPort: " + ipPort);
	//
	// System.setProperty("javax.net.ssl.trustStore", pathKeystore);
	// System.setProperty("java.protocol.handler.pkgs", ipPort);
	//
	// HostnameVerifier hv = new HostnameVerifier() {
	// public boolean verify(String urlHostName, SSLSession session) {
	// return true;
	// }
	// };
	//
	// // trustAllHttpsCertificates();
	// HttpsURLConnection.setDefaultHostnameVerifier(hv);
	// } catch (Exception e) {
	// log.error("Error al validar certificado: ", e);
	// }
	// }

	public void onTabChange(TabChangeEvent event) {
		title = event.getTab().getTitle();
		String id = event.getTab().getId();
		title = id;
		log.debug("tab = " + title);

		if (id.equals("tab1")) { // PORCENTAJE
			buscarDetallePorcentaje();
		}
		if (id.equals("tab3")) { // ANCHO BANDA
			buscarDetalleAnchoBanda();
		}
		if (id.equals("tab2")) { // DISTRIBUCION DEL TRAFICO
			buscarDetalleTrafico();
		}

		if (id.equals("tab4")) { // MB VS TIEMPO
			buscarDetalleMbVsTiempo();
		}

		// RequestContext context = RequestContext.getCurrentInstance();
		// context.update(":formTab:tabView:panel1 :formTab:tabView:panel2 :formTab:tabView:panel3");
	}

	private long guardarAuditoria(Calendar fechaConsulta, String usuario, String ip, String vista, int comando, String isdn, List<VcLogAdicional> adicionales) {
		try {

			VcLogAuditoria item = new VcLogAuditoria();
			item.setFechaConsulta(fechaConsulta);
			item.setIsdn(isdn);
			item.setUsuario(usuario);
			item.setIp(ip);
			item.setVista(vista);
			item.setComando(comando);
			item.setAdicional("Y");
			item.setFechaCreacion(Calendar.getInstance());
			if (adicionales == null || adicionales.size() == 0) {
				item.setAdicional("N");
			}

			blAuditoria.guardarAuditoria(item, adicionales);
			CodigoAuditoria = item.getIdLogAuditoria();
			return item.getIdLogAuditoria();

		} catch (Exception e) {
			log.error("Error al guardar auditoria: ", e);
		}

		return -1;
	}

	private void saveXml(String isdn, String login, String servicio, String metodo, String request, String response, long idAuditoria, Long tiempoConexion, Long tiempoRespuesta) {
		try {
			VcConsulta c = new VcConsulta();
			c.setFechaCreacion(Calendar.getInstance());
			c.setIsdn(isdn);
			c.setLogin(login);
			c.setServicio(servicio);
			c.setMetodo(metodo);
			c.setRequest(request);
			c.setResponse(response);
			c.setTiempoConexion(tiempoConexion);
			c.setTiempoRespuesta(tiempoRespuesta);

			Object find = blAuditoria.find(idAuditoria, VcLogAuditoria.class);
			c.setVcLogAuditoria((VcLogAuditoria) find);

			blConsulta.save(c);
		} catch (Exception e) {
			log.warn("No se registro en bd el request y response: ", e);
		}
	}

	public void guardarExcepcion(long IdAuditoria, String exepcion) {
		VcLogExcepcion item = new VcLogExcepcion();
		VcLogAuditoria find;
		try {
			find = (VcLogAuditoria) blAuditoria.find(IdAuditoria, VcLogAuditoria.class);
			item.setFechaCreacion(Calendar.getInstance());
			item.setExcepcion(exepcion);
			item.setVcLogAuditoria(find);

			blExcepcion.save(item);
		} catch (Exception e) {
			log.error("Error al guardar Excepcion: ", e);
		}
	}

	public String stackTraceToString(Throwable e) {
		CharArrayWriter sw = new CharArrayWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		pw.close();

		return sw.toString(); // stack trace a
	}

	public void onRowSelect(SelectEvent event) {
		// FacesMessage msg = new FacesMessage("Car Selected", ((Car) event.getObject()).getId());
		// FacesContext.getCurrentInstance().addMessage(null, msg);
		show();
	}

	public void onRowUnselect(UnselectEvent event) {
		show();
	}

	public void onRowSelectCheckbox(SelectEvent event) {
		show();
	}

	public void onRowUnselectCheckbox(UnselectEvent event) {
		show();
	}

	public void onToggleSelect(ToggleSelectEvent event) {
		show();
	}

	public void show() {
		try {

			// int cantidadDecimales = (int) parametroBl.getParametro(Constante.cantidadDecimales);

			if (listSelected != null) {
				log.debug("listSelected size: " + listSelected.size());
				subTotalConsumo = 0;
				subTotalPorcentaje = 0;
				nroFilas = listSelected.size();

				if (listSelected.size() == list.size()) { // si todos son seleccionados all check
					subTotalPorcentaje = 100;
					subTotalConsumo = this.totalConsumo.doubleValue();
					nroFilas = list.size();
				} else {

					for (Trafico item : listSelected) {
						// log.debug(item.toString());
						subTotalConsumo = subTotalConsumo + item.getTotalTrafico();
						subTotalPorcentaje = subTotalPorcentaje + item.getPorcentajeTotalTrafico();
					}
				}
			} else {
				log.debug("listSelected NULO");
			}
			if (subTotalPorcentaje > 100) {
				subTotalPorcentaje = 100;
			}

			subTotalConsumo = UtilNumber.redondear(subTotalConsumo, cantidadDecimales);
			subTotalPorcentaje = UtilNumber.redondear(subTotalPorcentaje, cantidadDecimales);
		} catch (Exception e) {
			log.error("Error al actualizar datos parciales de la tabla de consumo: ", e);
			SysMessage.error("Error al actualizar datos parciales de la tabla de consumo", null);
		}

		// if (listSelected.size() == list.size()) {
		// subTotalPorcentaje = 100;
		// subTotalConsumo = this.totalConsumo.doubleValue();
		// nroFilas = list.size();
		// } else {
	}

	public void onChangeOneMenu() {
		log.debug("topSeleccionado: " + topSeleccionado + ", size: " + list.size());
		try {

			if (topSeleccionado == -1) {
				listSelected = new ArrayList<>();
				subTotalConsumo = 0;
				subTotalPorcentaje = 0;
				nroFilas = 0;
			} else if (topSeleccionado == 0) { // TODOS

				subTotalConsumo = this.totalConsumo.doubleValue();
				subTotalPorcentaje = 100;
				nroFilas = list.size();
				listSelected = new ArrayList<>();
				for (Trafico item : list) {
					listSelected.add(item);
					// subTotalConsumo = subTotalConsumo + item.getTotalTrafico();
					// subTotalPorcentaje = subTotalPorcentaje + item.getPorcentajeTotalTrafico();
				}
			} else {
				listSelected = new ArrayList<>();
				subTotalConsumo = 0;
				subTotalPorcentaje = 0;

				if (topSeleccionado > list.size()) {
					nroFilas = list.size();
				} else {
					nroFilas = topSeleccionado;
				}

				// if (list != null) {
				// Collections.sort(list);
				// Collections.reverse(list);
				// }

				int i = 1;
				for (Trafico item : list) {
					if (i <= nroFilas) {
						listSelected.add(item);
						subTotalConsumo = subTotalConsumo + item.getTotalTrafico();
						subTotalPorcentaje = subTotalPorcentaje + item.getPorcentajeTotalTrafico();
						i++;
					} else
						break;
				}

			}

			// int cantidadDecimales = (int) parametroBl.getParametro(Constante.cantidadDecimales);
			if (subTotalPorcentaje > 100) {
				subTotalPorcentaje = 100;
			}

			subTotalConsumo = UtilNumber.redondear(subTotalConsumo, cantidadDecimales);
			subTotalPorcentaje = UtilNumber.redondear(subTotalPorcentaje, cantidadDecimales);

		} catch (Exception e) {
			log.error("Error al actualizar datos parciales de la tabla consumo: ", e);
			SysMessage.error("Error al actualizar datos parciales de la tabla consumo", null);
		}
	}

	// DataTable theDataTable = new DataTable();
	// public void filterListener(FilterEvent filterEvent) {

	public void seleccionarFiltrado() {
		try {
			// <p:commandButton value="Filtrar" actionListener="#{detallePorcentajeBean.seleccionarFiltrado()}" update="formCuenta:tabView:tabla :formCuenta:tabView:panelInformacion" />
			if (listFiltered != null) {
				listSelected = new ArrayList<>();
				nroFilas = listFiltered.size();
				subTotalConsumo = 0;
				subTotalPorcentaje = 0;
				for (Trafico item : listFiltered) {
					listSelected.add(item);
					subTotalConsumo = subTotalConsumo + item.getTotalTrafico();
					subTotalPorcentaje = subTotalPorcentaje + item.getPorcentajeTotalTrafico();
				}
			}

			// int cantidadDecimales = (int) parametroBl.getParametro(Constante.cantidadDecimales);

			subTotalConsumo = UtilNumber.redondear(subTotalConsumo, cantidadDecimales);
			subTotalPorcentaje = UtilNumber.redondear(subTotalPorcentaje, cantidadDecimales);
		} catch (Exception e) {
			log.error("Error al seleccionar filas filtradas de la tabla consumo: ", e);
			SysMessage.error("Error al seleccionar filas filtradas de la tabla consumo", null);
		}
	}

	@SuppressWarnings("rawtypes")
	public Map<String, Object> filterListener(AjaxBehaviorEvent filterEvent) {
		log.debug("filter Listener");
		try

		{

			DataTable table = (DataTable) filterEvent.getSource();
			List filteredValue = table.getFilteredValue();
			if (filteredValue != null) {
				log.debug("*************** INICIO filteredValue ****************************");
				for (Object item : filteredValue) {
					log.debug("filterValue: " + item);
				}
				log.debug("**************** FIN filteredValue ***************************");
			}

			if (filteredValue != null) {
				System.out.println("filtered = " + filteredValue.size());
			} else {
				System.out.println("No records found");
			}

			Map<String, Object> filters = table.getFilters();
			if (filters != null) {
				for (Entry<String, Object> entry : filters.entrySet()) {
					log.debug(entry.getKey() + " - " + entry.getValue());
				}
			} else {
				log.debug("hash resuelto a nulo");
			}

			// List<?> data = filterEvent.getData();
			// if (data != null) {
			// log.debug("++++++++++++++++++++ DATA INI +++++++++++++++++++");
			// for (Object item : data) {
			// log.debug("item: " + item.toString());
			// }
			// log.debug("++++++++++++++++++++ DATA FIN +++++++++++++++++++");
			// }

			log.debug("listFiltered: " + listFiltered);
			if (listFiltered != null) {
				log.debug("---------------------- INICIO ----------------------");
				for (Trafico item : listFiltered) {
					log.debug(item.toString());
				}
				log.debug("---------------------- FIN ----------------------");
			}

			return filters;
		} catch (Exception e) {
			log.error("Error en el evento filter de la tabla consumo: ", e);
			return null;
		}
	}

	public String getIsdn() {
		return isdn;
	}

	public void setIsdn(String isdn) {
		this.isdn = isdn;
	}

	public boolean isPorHora() {
		return porHora;
	}

	public void setPorHora(boolean porHora) {
		this.porHora = porHora;
	}

	public boolean isPorDia() {
		return porDia;
	}

	public void setPorDia(boolean porDia) {
		this.porDia = porDia;
	}

	public boolean isPorAplicacion() {
		return porAplicacion;
	}

	public void setPorAplicacion(boolean porAplicacion) {
		this.porAplicacion = porAplicacion;
	}

	public boolean isPorSubAplicacion() {
		return porSubAplicacion;
	}

	public void setPorSubAplicacion(boolean porSubAplicacion) {
		this.porSubAplicacion = porSubAplicacion;
	}

	public List<String> getAplicaciones() {
		return aplicaciones;
	}

	public void setAplicaciones(List<String> aplicaciones) {
		this.aplicaciones = aplicaciones;
	}

	public String[] getSelectedAplicaciones() {
		return selectedAplicaciones;
	}

	public void setSelectedAplicaciones(String[] selectedAplicaciones) {
		this.selectedAplicaciones = selectedAplicaciones;
	}

	public List<String> getSubAplicaciones() {
		return subAplicaciones;
	}

	public void setSubAplicaciones(List<String> subAplicaciones) {
		this.subAplicaciones = subAplicaciones;
	}

	public String[] getSelectedSubAplicaciones() {
		return selectedSubAplicaciones;
	}

	public void setSelectedSubAplicaciones(String[] selectedSubAplicaciones) {
		this.selectedSubAplicaciones = selectedSubAplicaciones;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public boolean isDisabledButton() {
		return disabledButton;
	}

	public void setDisabledButton(boolean disabledButton) {
		this.disabledButton = disabledButton;
	}

	public List<Trafico> getList() {
		return list;
	}

	public void setList(List<Trafico> list) {
		this.list = list;
	}

	public String getTipoUnidad() {
		return tipoUnidad;
	}

	public void setTipoUnidad(String tipoUnidad) {
		this.tipoUnidad = tipoUnidad;
	}

	public boolean isDisabledTotalTrafico() {
		return disabledTotalTrafico;
	}

	public void setDisabledTotalTrafico(boolean disabledTotalTrafico) {
		this.disabledTotalTrafico = disabledTotalTrafico;
	}

	public boolean isDisabledDownTrafico() {
		return disabledDownTrafico;
	}

	public void setDisabledDownTrafico(boolean disabledDownTrafico) {
		this.disabledDownTrafico = disabledDownTrafico;
	}

	public boolean isDisabledUpTrafico() {
		return disabledUpTrafico;
	}

	public void setDisabledUpTrafico(boolean disabledUpTrafico) {
		this.disabledUpTrafico = disabledUpTrafico;
	}

	public List<Bar> getListBar() {
		return listBar;
	}

	public void setListBar(List<Bar> listBar) {
		this.listBar = listBar;
	}

	public String getBarTiempos() {
		return barTiempos;
	}

	public void setBarTiempos(String barTiempos) {
		this.barTiempos = barTiempos;
	}

	public String getTitleY() {
		return titleY;
	}

	public void setTitleY(String titleY) {
		this.titleY = titleY;
	}

	public List<SelectItem> getItemsHoras() {
		return itemsHoras;
	}

	public void setItemsHoras(List<SelectItem> itemsHoras) {
		this.itemsHoras = itemsHoras;
	}

	public String getSelectHoraInicio() {
		return selectHoraInicio;
	}

	public void setSelectHoraInicio(String selectHoraInicio) {
		this.selectHoraInicio = selectHoraInicio;
	}

	public String getSelectHoraFin() {
		return selectHoraFin;
	}

	public void setSelectHoraFin(String selectHoraFin) {
		this.selectHoraFin = selectHoraFin;
	}

	public boolean isDisabledComboInicio() {
		return disabledComboInicio;
	}

	public void setDisabledComboInicio(boolean disabledComboInicio) {
		this.disabledComboInicio = disabledComboInicio;
	}

	public boolean isDisabledComboFin() {
		return disabledComboFin;
	}

	public void setDisabledComboFin(boolean disabledComboFin) {
		this.disabledComboFin = disabledComboFin;
	}

	public boolean isDisableIncidente() {
		return disableIncidente;
	}

	public void setDisableIncidente(boolean disableIncidente) {
		this.disableIncidente = disableIncidente;
	}

	public ControlButton getControlButtonTelnet() {
		return controlButtonTelnet;
	}

	public void setControlButtonTelnet(ControlButton controlButtonTelnet) {
		this.controlButtonTelnet = controlButtonTelnet;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public Double getTotalConsumo() {
		return totalConsumo;
	}

	public void setTotalConsumo(Double totalConsumo) {
		this.totalConsumo = totalConsumo;
	}

	public List<Trafico> getListSelected() {
		return listSelected;
	}

	public void setListSelected(List<Trafico> listSelected) {
		this.listSelected = listSelected;
	}

	public double getSubTotalPorcentaje() {
		return subTotalPorcentaje;
	}

	public void setSubTotalPorcentaje(double subTotalPorcentaje) {
		this.subTotalPorcentaje = subTotalPorcentaje;
	}

	public double getSubTotalConsumo() {
		return subTotalConsumo;
	}

	public void setSubTotalConsumo(double subTotalConsumo) {
		this.subTotalConsumo = subTotalConsumo;
	}

	public int getNroFilas() {
		return nroFilas;
	}

	public void setNroFilas(int nroFilas) {
		this.nroFilas = nroFilas;
	}

	public int getTopSeleccionado() {
		return topSeleccionado;
	}

	public void setTopSeleccionado(int topSeleccionado) {
		this.topSeleccionado = topSeleccionado;
	}

	public List<Trafico> getListFiltered() {
		return listFiltered;
	}

	public void setListFiltered(List<Trafico> listFiltered) {
		this.listFiltered = listFiltered;
	}

	public List<Consumo> getListConsumo() {
		return listConsumo;
	}

	public void setListConsumoTest(List<Consumo> listConsumo) {
		this.listConsumo = listConsumo;
	}

	public List<PieConsumo> getListPieConsumo() {
		return listPieConsumo;
	}

	public void setListPieConsumo(List<PieConsumo> listPieConsumo) {
		this.listPieConsumo = listPieConsumo;
	}

	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}

	public boolean isExportarDetalle() {
		return exportarDetalle;
	}

	public void setExportarDetalle(boolean exportarDetalle) {
		this.exportarDetalle = exportarDetalle;
	}

	public boolean isVisibleGraficoAnchoBanda() {
		return visibleGraficoAnchoBanda;
	}

	public void setVisibleGraficoAnchoBanda(boolean visibleGraficoAnchoBanda) {
		this.visibleGraficoAnchoBanda = visibleGraficoAnchoBanda;
	}

	public boolean isExportarSinDetalle() {
		return exportarSinDetalle;
	}

	public void setExportarSinDetalle(boolean exportarSinDetalle) {
		this.exportarSinDetalle = exportarSinDetalle;
	}

}
