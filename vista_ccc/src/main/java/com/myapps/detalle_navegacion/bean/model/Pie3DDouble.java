package com.myapps.detalle_navegacion.bean.model;

import java.io.Serializable;

public class Pie3DDouble implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String name;
	private Double y; // porcentaje
	private Double m; // total trafico

	public Pie3DDouble() {
		super();
	}

	public Pie3DDouble(String name, Double y, Double m) {
		super();
		this.name = name;
		this.y = y;
		this.setM(m);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getY() {
		return y;
	}

	public void setY(Double y) {
		this.y = y;
	}

	public Double getM() {
		return m;
	}

	public void setM(Double m) {
		this.m = m;
	}

	@Override
	public String toString() {
		return "Pie3DDouble [name=" + name + ", y=" + y + ", m=" + m + "]";
	}
}
