package com.myapps.detalle_navegacion.bean.model;

import java.io.Serializable;

public class Pie3D implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String name;
	private Integer y;

	public Pie3D() {
		super();
	}

	public Pie3D(String name, Integer y) {
		super();
		this.name = name;
		this.y = y;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getY() {
		return y;
	}

	public void setY(Integer y) {
		this.y = y;
	}
}
