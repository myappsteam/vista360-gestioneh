package com.myapps.detalle_navegacion.bean.model;

import java.io.Serializable;

public class EgnSubProtocol implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private String nombre;

	private Long subAplicacionId;
	private String subAplicacionNombre;

	private Long aplicacionId;
	private String aplicacionNombre;

	private Long categoriaId;
	private String categoriaNombre;

	public EgnSubProtocol() {
		super();
	}

	public EgnSubProtocol(Long id, String nombre, Long subAplicacionId, String subAplicacionNombre, Long aplicacionId, String aplicacionNombre, Long categoriaId, String categoriaNombre) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.subAplicacionId = subAplicacionId;
		this.subAplicacionNombre = subAplicacionNombre;
		this.aplicacionId = aplicacionId;
		this.aplicacionNombre = aplicacionNombre;
		this.categoriaId = categoriaId;
		this.categoriaNombre = categoriaNombre;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Long getAplicacionId() {
		return aplicacionId;
	}

	public void setAplicacionId(Long aplicacionId) {
		this.aplicacionId = aplicacionId;
	}

	public String getAplicacionNombre() {
		return aplicacionNombre;
	}

	public void setAplicacionNombre(String aplicacionNombre) {
		this.aplicacionNombre = aplicacionNombre;
	}

	public Long getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(Long categoriaId) {
		this.categoriaId = categoriaId;
	}

	public String getCategoriaNombre() {
		return categoriaNombre;
	}

	public void setCategoriaNombre(String categoriaNombre) {
		this.categoriaNombre = categoriaNombre;
	}

	public Long getSubAplicacionId() {
		return subAplicacionId;
	}

	public void setSubAplicacionId(Long subAplicacionId) {
		this.subAplicacionId = subAplicacionId;
	}

	public String getSubAplicacionNombre() {
		return subAplicacionNombre;
	}

	public void setSubAplicacionNombre(String subAplicacionNombre) {
		this.subAplicacionNombre = subAplicacionNombre;
	}

	@Override
	public String toString() {
		return "EgnSubProtocol [id=" + id + ", nombre=" + nombre + ", subAplicacionId=" + subAplicacionId + ", subAplicacionNombre=" + subAplicacionNombre + ", aplicacionId=" + aplicacionId + ", aplicacionNombre=" + aplicacionNombre + ", categoriaId=" + categoriaId + ", categoriaNombre="
				+ categoriaNombre + "]";
	}

}
