package com.myapps.detalle_navegacion.bean;

import com.csvreader.CsvReader;
import com.myapps.detalle_navegacion.bean.model.*;
import com.myapps.detalle_navegacion.business.CategoriaBL;
import com.myapps.detalle_navegacion.util.SysMessage;
import com.myapps.detalle_navegacion.util.UtilDate;
import com.myapps.detalle_navegacion.util.UtilUrl;
import com.myapps.user.bean.ControlerBitacora;
import com.myapps.user.ldap.DescriptorBitacora;
import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.UploadedFile;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@ManagedBean
@ViewScoped
public class ImportacionBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(ImportacionBean.class);

	@Inject
	private CategoriaBL categoriaBl;

	@Inject
	private ControlerBitacora controlerBitacora;

	public final char SEPARATOR = ';';
	public final char QUOTE = '"';

	private String login;
	private String ip;

	// private UploadedFile file;
	// private long sizeLimit;
	private TreeNode root;

	private List<Registro> list;
	private String msg;

	@PostConstruct
	public void init() {
		iniciar();
	}

	public void iniciar() {
		try {
			msg = "";
			// sizeLimit = 52428800;
			// sizeLimit = ParametrosDn.sizeArchivoImportacion * 1024 * 1024;
			// root = new DefaultTreeNode(new Registro("Root", "", ""), null);
			list = new ArrayList<>();

			cargarIp();
			obtenerUsuario();
		} catch (Exception e) {
			log.error("Error al iniciar: ", e);
		}
	}

	// public void cargarThreeImportacion(String jsonCategorias, String jsonAplicaciones, String jsonSubAplicaciones) {
	// try {
	// root = new DefaultTreeNode(new Registro("Root", "", ""), null);
	// ObjectMapper mapper = new ObjectMapper();
	//
	// ArrayList<Categoria> listCategoria = mapper.readValue(jsonCategorias, TypeFactory.defaultInstance().constructCollectionType(List.class, Categoria.class));
	// ArrayList<Aplicacion> listAplicacion = mapper.readValue(jsonAplicaciones, TypeFactory.defaultInstance().constructCollectionType(List.class, Aplicacion.class));
	// ArrayList<SubAplicacion> listSubAplicacion = mapper.readValue(jsonSubAplicaciones, TypeFactory.defaultInstance().constructCollectionType(List.class, SubAplicacion.class));
	//
	// if (listCategoria != null) {
	// for (Categoria cat : listCategoria) {
	// TreeNode categoria = new DefaultTreeNode(new Registro(cat.getNombre(), "", ""), root);
	// categoria.setExpanded(false);
	//
	// if (listAplicacion != null) {
	//
	// for (Aplicacion apl : listAplicacion) {
	// if (apl.getCategoriaId().longValue() == cat.getId().longValue()) {
	// // TreeNode aplicacion = new DefaultTreeNode(new Registro(apl.getNombre(), "", ""), categoria);
	// // aplicacion.setExpanded(false);
	//
	// if (listSubAplicacion != null) {
	//
	// boolean primerFila = true;
	// for (SubAplicacion sub : listSubAplicacion) {
	// if (sub.getAplicacionId().longValue() == apl.getId().longValue()) {
	// if (primerFila) {
	// TreeNode subAplicacion = new DefaultTreeNode(new Registro("", apl.getNombre(), sub.getNombre()), categoria);
	// subAplicacion.setExpanded(false);
	// primerFila = false;
	// } else {
	// TreeNode subAplicacion = new DefaultTreeNode(new Registro("", "", sub.getNombre()), categoria);
	// subAplicacion.setExpanded(false);
	// }
	// }
	// }
	// }
	// }
	// }
	// }
	// }
	// }
	// } catch (Exception e) {
	// log.error("Error al cargar importacion: ", e);
	// }
	// }

	private void cargarImportacion(List<Categoria> listCategoria, List<Aplicacion> listAplicacion, List<SubAplicacion> listSubAplicacion, List<EgnSubProtocol> listEgnSubProtocol, boolean guardar) {
		try {
			msg = "";
			list = new ArrayList<>();
			// ObjectMapper mapper = new ObjectMapper();
			// List<Categoria> listCategoria = mapper.readValue(jsonCategorias, TypeFactory.defaultInstance().constructCollectionType(List.class, Categoria.class));
			// List<Aplicacion> listAplicacion = mapper.readValue(jsonAplicaciones, TypeFactory.defaultInstance().constructCollectionType(List.class, Aplicacion.class));
			// List<SubAplicacion> listSubAplicacion = mapper.readValue(jsonSubAplicaciones, TypeFactory.defaultInstance().constructCollectionType(List.class, SubAplicacion.class));

			if (guardar) {
				msg = "Categorias registradas: " + (listCategoria != null ? listCategoria.size() : "NULL");
				msg = msg + "<br/>Aplicaciones registradas: " + (listAplicacion != null ? listAplicacion.size() : "NULL");
				msg = msg + "<br/>SubAplicaciones registradas: " + (listSubAplicacion != null ? listSubAplicacion.size() : "NULL");
				msg = msg + "<br/>EgnSubProtocol registradas: " + (listEgnSubProtocol != null ? listEgnSubProtocol.size() : "NULL");
			} else {
				msg = "Categorias obtenidas: " + (listCategoria != null ? listCategoria.size() : "NULL");
				msg = msg + "<br/>Aplicaciones obtenidas: " + (listAplicacion != null ? listAplicacion.size() : "NULL");
				msg = msg + "<br/>SubAplicaciones obtenidas: " + (listSubAplicacion != null ? listSubAplicacion.size() : "NULL");
				msg = msg + "<br/>EgnSubProtocol obtenidas: " + (listEgnSubProtocol != null ? listEgnSubProtocol.size() : "NULL");
			}

			if (listCategoria != null) {
				for (Categoria cat : listCategoria) {

					boolean tieneAplicacion = false;
					if (listAplicacion != null) {

						for (Aplicacion apl : listAplicacion) {
							if (apl.getCategoriaId().longValue() == cat.getId().longValue()) {
								tieneAplicacion = true;

								boolean tieneSubaplicacion = false;
								if (listSubAplicacion != null) {

									for (SubAplicacion sub : listSubAplicacion) {
										if (sub.getAplicacionId().longValue() == apl.getId().longValue()) {
											tieneSubaplicacion = true;

											boolean tieneEgnSubProtocol = false;
											if (listEgnSubProtocol != null) {

												for (EgnSubProtocol egn : listEgnSubProtocol) {
													if (egn.getSubAplicacionId().longValue() == sub.getId().longValue()) {
														tieneEgnSubProtocol = true;
														list.add(new Registro(cat.getId(), cat.getNombre(), apl.getId(), apl.getNombre(), sub.getId(), sub.getNombre(), egn.getId(), egn.getNombre()));
													}
												}
											}
											if (!tieneEgnSubProtocol) {
												list.add(new Registro(cat.getId(), cat.getNombre(), apl.getId(), apl.getNombre(), sub.getId(), sub.getNombre(), null, ""));
											}
										}
									}
								}
								if (!tieneSubaplicacion) {
									list.add(new Registro(cat.getId(), cat.getNombre(), apl.getId(), apl.getNombre(), null, "", null, ""));
									// log.debug("aplicacion no tiene subaplicacion: " + apl.getId());
								}
							}
						}
					}
					if (!tieneAplicacion) {
						list.add(new Registro(cat.getId(), cat.getNombre(), null, "", null, "", null, ""));
						// log.debug("categoria no tiene aplicacion: " + cat.getId());
					}
				}
			}

		} catch (Exception e) {
			log.error("Error al cargar categorias, aplicaciones y subaplicaciones: ", e);
			SysMessage.error("Error al cargar categorias, aplicaciones y subaplicaciones: " + e.getLocalizedMessage(), null);
		}
	}

	public void cargar() {
		try {
			List<Categoria> listCategoria = new ArrayList<>();
			List<Aplicacion> listAplicacion = new ArrayList<>();
			List<SubAplicacion> listSubAplicacion = new ArrayList<>();
			List<EgnSubProtocol> listEgnSubProtocol = new ArrayList<>();
			categoriaBl.obtenerAplicaciones(listCategoria, listAplicacion, listSubAplicacion, listEgnSubProtocol);

			// log.debug("size categoria: " + listCategoria.size());
			// log.debug("size aplicacion: " + listAplicacion.size());
			// log.debug("size subaplicacion: " + listSubAplicacion.size());

			cargarImportacion(listCategoria, listAplicacion, listSubAplicacion, listEgnSubProtocol, false);

			SysMessage.info("Consulta finalizada", null);

		} catch (Exception e) {
			log.error("Error al cargar aplicaciones: ", e);
		}
	}

	// <p:treeTable id="treeCategorias" value="#{importacionBean.root}" var="item" tableStyle="table-layout: auto;" styleClass="ui-datatable ui-corner-all">
	// <p:column headerText="Categoria" style="word-wrap: break-word;">
	// <h:outputText value="#{item.categoria}" />
	// </p:column>
	// <p:column headerText="Aplicacion" style="word-wrap: break-word;">
	// <h:outputText value="#{item.aplicacion}" />
	// </p:column>
	// <p:column headerText="SubAplicacion" style="word-wrap: break-word;">
	// <h:outputText value="#{item.subAplicacion}" />
	// </p:column>
	// </p:treeTable>

	public boolean vacio() {
		return msg.isEmpty();
	}

	private void read(InputStream is) {
		// System.out.println(path);

		try {
			long ini = System.currentTimeMillis();

			log.debug(UtilDate.dateToString(new Date(), "dd-MM-yyyy HH:mm:ss"));
			CsvReader csvReader = new CsvReader(is, Charset.defaultCharset());
			csvReader.setDelimiter(this.SEPARATOR);
			csvReader.readHeaders();

			List<Categoria> listCategoria = new ArrayList<>();
			List<Aplicacion> listAplicacion = new ArrayList<>();
			List<SubAplicacion> listSubAplicacion = new ArrayList<>();
			List<EgnSubProtocol> listEgnSubProtocol = new ArrayList<>();

			Long idCategoria = new Long(0);
			Long idAplicacion = new Long(0);
			Long idSubAplicacion = new Long(0);
			// Long idEgnSubProtocol = new Long(0);

			int i = 0;
			while (csvReader.readRecord()) {
				i++;

				String categoria = csvReader.get("Category"); // Category
				String categoriaId = csvReader.get("Category ID"); // Category ID
				String aplicacion = csvReader.get("Application");
				String aplicacionId = csvReader.get("Application ID");
				String subAplicacion = csvReader.get("Sub Application");
				String subAplicacionId = csvReader.get("Sub Application ID");
				String egnSubProtocol = csvReader.get("EGN Sub Protocol");
				String egnSubProtocolId = csvReader.get("EGN Sub Protocol ID");

				// log.debug(categoria + " - " + categoriaId + " - " + aplicacion + " - " + aplicacionId + " - " + subAplicacion + " - " + subAplicacionId);

				if (categoriaId != null && !categoriaId.trim().isEmpty()) {
					idCategoria = new Long(categoriaId.trim());
					listCategoria.add(new Categoria(idCategoria, categoria));
				}
				if (aplicacionId != null && !aplicacionId.trim().isEmpty()) {
					idAplicacion = new Long(aplicacionId.trim());
					listAplicacion.add(new Aplicacion(idAplicacion, aplicacion, idCategoria, categoria));
				}
				if (subAplicacionId != null && !subAplicacionId.trim().isEmpty()) {
					idSubAplicacion = new Long(subAplicacionId.trim());
					listSubAplicacion.add(new SubAplicacion(idSubAplicacion, subAplicacion, idAplicacion, aplicacion, idCategoria, categoria));
				}
				if (egnSubProtocolId != null && !egnSubProtocolId.trim().isEmpty()) {
					Long idEgnSubProtocol = new Long(egnSubProtocolId.trim());
					listEgnSubProtocol.add(new EgnSubProtocol(idEgnSubProtocol, egnSubProtocol, idSubAplicacion, subAplicacion, idAplicacion, aplicacion, idCategoria, categoria));
				}

				// if (i >= 5)
				// break;
			}

			// ObjectMapper mapper = new ObjectMapper();
			// String jsonCategorias = mapper.writeValueAsString(listCategoria);
			// String jsonAplicaciones = mapper.writeValueAsString(listAplicacion);
			// String jsonSubAplicaciones = mapper.writeValueAsString(listSubAplicacion);
			// log.debug("Categorias: " + jsonCategorias);
			// log.debug("Aplicaciones: " + jsonAplicaciones);
			// log.debug("SubAplicaciones: " + jsonSubAplicaciones);

			log.debug("Categorias: " + listCategoria);
			log.debug("Aplicaciones: " + listAplicacion);
			log.debug("SubAplicaciones: " + listSubAplicacion);
			log.debug("EgnSubProtocol: " + listEgnSubProtocol);

			categoriaBl.guardarAplicaciones(listCategoria, listAplicacion, listSubAplicacion, listEgnSubProtocol);
			// categoriaBl.guardarAplicaciones(jsonCategorias, jsonAplicaciones, jsonSubAplicaciones);

			try {
				controlerBitacora.accion(DescriptorBitacora.IMPORTACION_APLICACIONES, "Se hizo la importacion de aplicaciones");
			} catch (Exception e) {
				log.error("Error al guardar bitacora en el sistema: ", e);
				SysMessage.error("Error al guardar bitacora en el sistema", null);
			}

			cargarImportacion(listCategoria, listAplicacion, listSubAplicacion, listEgnSubProtocol, true);
			// cargarImportacion(jsonCategorias, jsonAplicaciones, jsonSubAplicaciones);

			long fin = System.currentTimeMillis();
			long ml = (fin - ini);
			log.debug("ml: " + ml);
			log.debug(UtilDate.dateToString(new Date(), "dd-MM-yyyy HH:mm:ss"));
			log.debug("cant: " + i);

			SysMessage.info("Importación finalizada", null);

		} catch (Exception e) {
			log.error("Error al importar categorias, aplicaciones, subaplicaciones: ", e);
			SysMessage.error("Error al importar archivo: " + e.getLocalizedMessage(), null);
		}
	}

	public void upload(FileUploadEvent event) {
		try {

			UploadedFile uploadedFile = event.getFile();
			String fileName = uploadedFile.getFileName();
			String contentType = uploadedFile.getContentType();

			long size = uploadedFile.getSize();
			log.debug("fileName: " + fileName + ", size: " + size + " bytes" + ", contentType: " + contentType);

			fileName = fileName.toLowerCase();
			if (fileName.endsWith("csv")) {

				// byte[] contents = uploadedFile.getContents(); // Or getInputStream()
				InputStream is = uploadedFile.getInputstream();
				read(is);
				// ... Save it, now!

			} else {
				SysMessage.warn("Archivo no válido", null);
			}
		} catch (Exception e) {
			log.error("Error al cargar archivo: ", e);
			SysMessage.error("Error al cargar archivo: " + e.getLocalizedMessage(), null);
		}
	}
	// public void handleFileUpload(FileUploadEvent event) {
	// <p:fileUpload fileUploadListener="#{importacionBean.handleFileUpload}" mode="advanced" dragDropSupport="false" update="messages" sizeLimit="#{importacionBean.sizeLimit}" fileLimit="1"
	// allowTypes="/(\.|\/)(csv)$/" />
	// <p:growl id="messages" showDetail="true" />
	// FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
	// FacesContext.getCurrentInstance().addMessage(null, message);
	// }

	// public void upload2() {
	// log.debug("cargando archivo...");
	// if (file != null) {
	// FacesMessage message = new FacesMessage("Succesful", file.getFileName() + " is uploaded.");
	// FacesContext.getCurrentInstance().addMessage(null, message);
	// }
	// }

	// public UploadedFile getFile() {
	// return file;
	// }
	//
	// public void setFile(UploadedFile file) {
	// this.file = file;
	// }

	private void cargarIp() {
		try {
			// ip = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRemoteAddr();

			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();

			ip = UtilUrl.getClientIp(request);
			log.debug("ip cliente: " + ip);

		} catch (Exception e) {
			log.error("Error al obtener ip: ", e);
		}
	}

	private void obtenerUsuario() {
		try {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			this.setLogin((String) request.getSession().getAttribute("TEMP$USER_NAME"));
			// log.info("login: " + login);
		} catch (Exception e) {
			log.error("Error al obtener usuario: ", e);
		}
	}

	// public long getSizeLimit() {
	// return sizeLimit;
	// }
	//
	// public void setSizeLimit(long sizeLimit) {
	// this.sizeLimit = sizeLimit;
	// }

	public TreeNode getRoot() {
		return root;
	}

	public void setRoot(TreeNode root) {
		this.root = root;
	}

	public List<Registro> getList() {
		return list;
	}

	public void setList(List<Registro> list) {
		this.list = list;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

}
