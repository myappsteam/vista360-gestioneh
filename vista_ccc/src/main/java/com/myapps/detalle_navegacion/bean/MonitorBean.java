package com.myapps.detalle_navegacion.bean;

import com.myapps.detalle_navegacion.business.MonitorBL;
import com.myapps.detalle_navegacion.entity.DnMonitor;
import com.myapps.detalle_navegacion.util.SysMessage;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;

@ManagedBean
@ViewScoped
public class MonitorBean implements Serializable {

	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(MonitorBean.class);

	@Inject
	private MonitorBL monitorBl;

	private List<DnMonitor> lista;

	@PostConstruct
	public void init() {

		try {
			iniciar();
		} catch (Exception e) {
			log.error("Error en el init: ", e);
		}

	}

	@SuppressWarnings("unchecked")
	public void iniciar() {
		try {

			lista = monitorBl.findAll();

		} catch (Exception e) {
			log.error("Error al iniciar: ", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void recargar() {
		try {

			lista = monitorBl.findAll();
			SysMessage.info("Consulta satisfactoria", null);

		} catch (Exception e) {
			log.error("Error al recargar monitor: ", e);
			SysMessage.error("Error al recargar parametros: " + e.getMessage(), null);
		}
	}

	public List<DnMonitor> getLista() {
		return lista;
	}

	public void setLista(List<DnMonitor> lista) {
		this.lista = lista;
	}

}
