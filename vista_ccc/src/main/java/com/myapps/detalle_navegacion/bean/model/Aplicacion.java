package com.myapps.detalle_navegacion.bean.model;

import java.io.Serializable;

public class Aplicacion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private String nombre;
	private Long categoriaId;
	private String categoriaNombre;

	public Aplicacion() {
		super();
	}

	public Aplicacion(Long id, String nombre, Long categoriaId, String categoriaNombre) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.categoriaId = categoriaId;
		this.categoriaNombre = categoriaNombre;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Long getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(Long categoriaId) {
		this.categoriaId = categoriaId;
	}

	public String getCategoriaNombre() {
		return categoriaNombre;
	}

	public void setCategoriaNombre(String categoriaNombre) {
		this.categoriaNombre = categoriaNombre;
	}

	@Override
	public String toString() {
		return "Aplicacion [id=" + id + ", nombre=" + nombre + ", categoriaId=" + categoriaId + ", categoriaNombre=" + categoriaNombre + "]";
	}

}
