package com.myapps.detalle_navegacion.dao;

import javax.annotation.Resource;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Named
public class MasterDaoDn implements Serializable {

	private static final long serialVersionUID = 1L;

	@PersistenceContext(unitName = "detalle_navegacion")
	private transient EntityManager entityManager;

	@Resource
	private transient UserTransaction transaction;

	public void save(Object entidad) throws Exception {
		transaction.begin();
		entityManager.joinTransaction();
		try {
			entityManager.persist(entidad);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
			throw e;
		}
	}

	public void remove(Object entidad) throws Exception {
		transaction.begin();
		entityManager.joinTransaction();
		try {
			entityManager.remove(entityManager.contains(entidad) ? entidad : entityManager.merge(entidad));
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
			throw e;
		}
	}

	public void update(Object entidad) throws Exception {
		transaction.begin();
		entityManager.joinTransaction();
		try {
			entityManager.merge(entidad);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
			throw e;
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object find(Object key, Class clase) throws Exception {
		try {
			return entityManager.find(clase, key);
		} catch (Exception e) {
			throw e;
		}
	}

	public void begin() throws Exception {
		transaction.begin();
		entityManager.joinTransaction();
	}

	public void commit() throws Exception {
		transaction.commit();
	}

	public void rollback() throws Exception {
		transaction.rollback();
	}

	public void updateOnly(Object entidad) throws Exception {
		entityManager.merge(entidad);
	}

	public void saveOnly(Object entidad) throws Exception {
		entityManager.persist(entidad);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List findAllQuery(Class clase, String sql, ParameterQuery parameterQuery) throws Exception {
		Map<String, Object> parametros = parameterQuery != null ? parameterQuery.getParametros() : null;
		try {
			Query query = entityManager.createQuery(sql, clase);
			if (parametros != null) {
				List<String> keys = new ArrayList<String>(parametros.keySet());
				for (String key : keys) {
					query.setParameter(key, parametros.get(key));
				}
			}
			return query.getResultList();
		} catch (Exception e) {
			throw e;
		}
	}

	@SuppressWarnings({ "rawtypes" })
	public List findAllNativeQuery(String sql, ParameterQuery parameterQuery) throws Exception {
		Map<String, Object> parametros = parameterQuery != null ? parameterQuery.getParametros() : null;
		try {
			Query query = entityManager.createNativeQuery(sql);
			if (parametros != null) {
				List<String> keys = new ArrayList<String>(parametros.keySet());
				for (String key : keys) {
					query.setParameter(key, parametros.get(key));
				}
			}
			return query.getResultList();
		} catch (Exception e) {
			throw e;
		}
	}

	@SuppressWarnings({ "rawtypes" })
	public List findAllNativeQuery(Class clase, String sql, ParameterQuery parameterQuery) throws Exception {
		Map<String, Object> parametros = parameterQuery != null ? parameterQuery.getParametros() : null;
		try {
			Query query = entityManager.createNativeQuery(sql, clase);
			if (parametros != null) {
				List<String> keys = new ArrayList<String>(parametros.keySet());
				for (String key : keys) {
					query.setParameter(key, parametros.get(key));
				}
			}
			return query.getResultList();
		} catch (Exception e) {
			throw e;
		}
	}

	public void executeUpdateNativeQueryOnly(String sql, ParameterQuery parameterQuery) throws Exception {
		Map<String, Object> parametros = parameterQuery != null ? parameterQuery.getParametros() : null;
		Query query = entityManager.createNativeQuery(sql);
		if (parametros != null) {
			List<String> keys = new ArrayList<String>(parametros.keySet());
			for (String key : keys) {
				query.setParameter(key, parametros.get(key));
			}
		}
		query.executeUpdate();
	}

	public void executeUpdateNativeQuery(String sql, ParameterQuery parameterQuery) throws Exception {
		Map<String, Object> parametros = parameterQuery != null ? parameterQuery.getParametros() : null;
		transaction.begin();
		entityManager.joinTransaction();
		try {
			Query query = entityManager.createNativeQuery(sql);
			if (parametros != null) {
				List<String> keys = new ArrayList<String>(parametros.keySet());
				for (String key : keys) {
					query.setParameter(key, parametros.get(key));
				}
			}
			query.executeUpdate();
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
			throw e;
		}
	}

	public List findAllNativeQuery(Class clase, String sql, ParameterQuery parameterQuery, int first, int pageSize) throws Exception {
		Map<String, Object> parametros = parameterQuery != null ? parameterQuery.getParametros() : null;
		try {
			Query query = entityManager.createNativeQuery(sql, clase);
			if (parametros != null) {
				List<String> keys = new ArrayList<String>(parametros.keySet());
				for (String key : keys) {
					query.setParameter(key, parametros.get(key));
				}
			}
			if (first > 0) {
				query.setFirstResult(first);
			}
			if (pageSize > 0) {
				query.setMaxResults(pageSize);
			}
			return query.getResultList();
		} catch (Exception e) {
			throw e;
		}
	}

}
