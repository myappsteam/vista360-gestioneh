package com.myapps.detalle_navegacion.util;

import java.io.Serializable;

public class Constante implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// public static final String activeInitialContext = "DIRECTORIO.ACTIVO.INITIAL.CONTEXT.FACTORY";
	// public static final String activeProviderUrl = "DIRECTORIO.ACTIVO.PROVIDER.URL";
	// public static final String activeSecurityAuthentication = "DIRECTORIO.ACTIVO.SECURITY.AUTHENTICATION";
	// public static final String activeSecurityPrincipal = "DIRECTORIO.ACTIVO.SECURITY.PRINCIPAL";
	// public static final String activeSecurityCredentials = "DIRECTORIO.ACTIVO.SECURITY.CREDENTIALS";
	// public static final String activeSecurityUser = "DIRECTORIO.ACTIVO.SECURITY.USER";
	// public static final String activeDominio = "DIRECTORIO.ACTIVO.DOMINIO";
	// public static final String consultarDirectorioActivo = "CONSULTAR.DIRECTORIO.ACTIVO";
	// public static final String usuario = "USUARIO";
	// public static final String password = "PASSWORD";
	// public static final String nroIntentos = "NRO.INTENTOS";
	// public static final String tiempoFuera = "TIEMPO.FUERA";
	public static final String reintentosTimeOut = "REINTENTOS.TIME.OUT";
	public static final String esperaReintentosTimeOut = "TIEMPO.ESPERA.REINTENTOS.TIME.OUT";

	// public static final String tituloAplicacion = "TITULO.APLICACION";
	// public static final String tituloPiePagina = "TITULO.PIE.PAGINA";
	// public static final String expresionRegularTextoNormal = "EXPRESION.REGULAR.TEXTO.NORMAL";
	// public static final String expresionRegularUsuario = "EXPRESION.REGULAR.USUARIO";
	// public static final String expresionRegularPassword = "EXPRESION.REGULAR.PASSWORD";
	public static final String expresionRegularNroTigo = "EXPRESION.REGULAR.NRO.TIGO";
	// public static final String mensajeValidacionUsuario = "MENSAJE.VALIDACION.USUARIO";
	// public static final String mensajeValidacionPassword = "MENSAJE.VALIDACION.PASSWORD";
	// public static final String mensajeValidacionTextoNormal = "MENSAJE.VALIDACION.TEXTO.NORMAL";
	// public static final String mensajeValidacionIp = "MENSAJE.VALIDACION.IP";

	public static final String detalleNavegacionWsdl = "DETALLE.NAVEGACION.WSDL";
	// public static final String detalleNavegacionPathKeystore = "DETALLE.NAVEGACION.PATH.KEYSTORE";
	public static final String detalleNavegacionTimeOut = "DETALLE.NAVEGACION.TIMEOUT";

	public static final String cantidadDiasConsultaPorHora = "CANTIDAD.DIAS.CONSULTA.MAXIMO.PORHORA";
	public static final String cantidadDiasConsultaPorDia = "CANTIDAD.DIAS.CONSULTA.MAXIMO.PORDIA";

	public static final String visibleColumnaTotalTrafico = "VISIBLE.COLUMNA.TOTAL.TRAFICO";
	public static final String visibleColumnaDownTrafico = "VISIBLE.COLUMNA.DOWN.TRAFICO";
	public static final String visibleColumnaUpTrafico = "VISIBLE.COLUMNA.UP.TRAFICO";

	public static final String unidad = "UNIDAD";
	public static final String visibleUnidad = "VISIBLE.UNIDAD";
	public static final String visibleConsumoPorcentaje = "VISIBLE.CONSUMO.PORCENTAJE";

	public static final String operador = "OPERADOR";
	public static final String numero = "NUMERO";
	public static final String cantidadDecimales = "CANTIDAD.DECIMALES";
	public static final String velocidadTransferencia = "UNIDAD.VELOCIDAD.TRANSFERENCIA";

	public static final String textoReporte = "TEXTO.REPORTE";
	public static final String textoReporteFuente = "TEXTO.REPORTE.FUENTE";
	public static final String textoReporteSize = "TEXTO.REPORTE.SIZE";

	public static final String topAplicacionesReporte = "TOP.APLICACIONES.REPORTE";

	public static final String columna1 = "REPORTE.COLUMNA.FECHA";
	public static final String columna2 = "REPORTE.COLUMNA.HORA";
	public static final String columna3 = "REPORTE.COLUMNA.APLICACION";
	public static final String columna4 = "REPORTE.COLUMNA.TOTAL";

	public static final String multiplicadorBytes = "MULTIPLICACOR.PARA.BYTES";
	public static final String colorReporte = "COLOR.REPORTE";
//	public static final String sizeArchivoImportacion = "SIZE.ARCHIVO.IMPORTACION.MEGABYTE";

	public static final String STRING = "STRING";
	public static final String ENTERO = "ENTERO";
	public static final String LONG = "LONG";
	public static final String BOOLEAN = "BOOLEAN";
	public static final String DATE = "DATE";
	public static final String DOUBLE = "DOUBLE";

	public static final String Kbit = "Kbit";
	public static final String Mbit = "Mbit";
	
	public static final String visibleGraficoAnchoBanda = "VISIBLE.GRAFICO.ANCHO.BANDA";
	
	public static final String textoHeaderReporte = "TEXTO.HEADER.REPORTE";
	public static final String textoHeaderReporteFuente="TEXTO.HEADER.REPORTE.FUENTE";
	public static final String textoHeaderReporteSize="TEXTO.HEADER.REPORTE.SIZE";
	
	
	
	
	public static final String HORA = "HORA";
	public static final String DIA = "DIA";

	public static final String APLICACION = "APLICACION";
	public static final String SUBAPLICACION = "SUBAPLICACION";
	public static final String APLICACION_DIA = "APLICACION_DIA";
	public static final String SUBAPLICACION_DIA = "SUBAPLICACION_DIA";

}
