package com.myapps.detalle_navegacion.util;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UtilDate implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static Date stringToDate(String fecha, String format) {
		Date date = null;
		try {
			DateFormat formatter;
			// yyyy-MM-dd HH:mm
			formatter = new SimpleDateFormat(format);
			date = (Date) formatter.parse(fecha);
		} catch (Exception e) {
			e.getMessage();
		}
		return date;
	}

	public static String dateToString(Date fecha, String format) {
		String date = "";
		try {
			DateFormat formatter = new SimpleDateFormat(format);
			date = formatter.format(fecha);
		} catch (Exception e) {
			e.getMessage();
		}
		return date;
	}

	// public static void main(String[] args) {
		// TODO Auto-generated method stub
//		Date fecha = UtilDate.stringToDate("20160627121601", "YYYYMMDDhhmmss");
//		Date fecha = UtilDate.stringToDate("20160627121601", "yyyyMMddHHmmss");
		// 2016-06-27 121601
//		Date fecha = new Date();
		
//		System.out.println(UtilDate.dateToString(fecha, "yyyyMMddHHmmss"));
//		System.out.println(UtilDate.dateToString(fecha, "dd-MM-yyyy HH:mm:ss"));
		// 28-12-2015 12:00:00

//		 System.out.println(UtilDate.dateToString(fecha, "YYYYMMDDhhmmss"));
//		System.out.println(UtilDate.dateToString(fecha, "yyyyMMddHHmmss"));
		// 2016-12-36 2121201
		
//		System.out.println(UtilDate.dateToString(new Date(), "HH:mm:ss"));
//	}

}
