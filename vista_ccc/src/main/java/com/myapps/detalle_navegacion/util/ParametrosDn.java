package com.myapps.detalle_navegacion.util;


import com.myapps.detalle_navegacion.entity.DnParametro;
import com.myapps.detalle_navegacion.facade.ParametroFacade;
import com.myapps.vista_ccc.util.Parametros;
import org.apache.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

public class ParametrosDn {

	public static Logger log = Logger.getLogger(ParametrosDn.class);
	// private static final String JNDI = "java:global/vista_ccc/ParametroFacade!com.myapps.detalle_navegacion.facade.ParametroFacade";
	private static final String JNDI = Parametros.jndi;

	// java:global/vista_ccc/ParametroFacade!com.myapps.detalle_navegacion.facade.ParametroFacade
	// java:app/vista_ccc/ParametroFacade!com.myapps.detalle_navegacion.facade.ParametroFacade
	// java:module/ParametroFacade!com.myapps.detalle_navegacion.facade.ParametroFacade
	// java:global/vista_ccc/ParametroFacade
	// java:app/vista_ccc/ParametroFacade
	// java:module/ParametroFacade

	static {
		try {
			ParametrosDn p = new ParametrosDn();
			// p.init();
			p.cargarParametros();
		} catch (Exception e) {
			log.error("Error al cargar tabla de propiedades", e);
		}
	}

	public static synchronized void cargarParametros() throws Exception {
		log.info("Ingresando a cargar parametros en la aplicacion web");
		try {
			Properties properties = new Properties();
			properties.put("java.naming.provider.url", Parametros.urlProvider);
			Context context = new InitialContext(properties);

			HashMap<String, Object> hash = new HashMap<>();

			final ParametroFacade parametroFacade = (ParametroFacade) context.lookup(JNDI);

			List<DnParametro> findAll = parametroFacade.findAll();

			if (findAll != null) {
				for (DnParametro item : findAll) {
					log.debug(item.getNombre() + " = " + item.getValor());
					try {
						if (item.getTipoDato().equals("STRING")) {
							hash.put(item.getNombre(), item.getValor());
						}
						if (item.getTipoDato().equals("ENTERO")) {
							hash.put(item.getNombre(), Integer.parseInt(item.getValor()));
						}
						if (item.getTipoDato().equals("BOOLEAN")) {
							hash.put(item.getNombre(), Boolean.parseBoolean(item.getValor()));
						}
						if (item.getTipoDato().equals("DOUBLE")) {
							hash.put(item.getNombre(), Double.parseDouble(item.getValor()));
						}
						if (item.getTipoDato().equals("LONG")) {
							hash.put(item.getNombre(), Long.parseLong(item.getValor()));
						}
						if (item.getTipoDato().equals("DATE")) {
							hash.put(item.getNombre(), UtilDate.stringToDate(item.getValor(), "dd-MM-yyyy HH:mm:ss"));
						}
					} catch (Exception e) {
						log.error("Error al cargar parametro nombre: " + item.getNombre() + ", tipoDato: " + item.getTipoDato() + ", valor: " + item.getValor(), e);
						new Throwable(e);
					}
				}

				if (hash != null) {
					ParametrosDn.expresionRegularNroTigo = hash.get(Constante.expresionRegularNroTigo).toString();

					// ParametrosDn.detalleNavegacionWsdl = "https://172.16.79.13:8443/detalle_navegacion_ws/DetalleNavegacion?wsdl";
					// ParametrosDn.detalleNavegacionWsdl = "http://localhost:8087/detalle_navegacion_ws/DetalleNavegacion?wsdl";
					ParametrosDn.detalleNavegacionWsdl = hash.get(Constante.detalleNavegacionWsdl).toString();
					ParametrosDn.detalleNavegacionTimeOut = (Integer) hash.get(Constante.detalleNavegacionTimeOut);

					ParametrosDn.cantidadDiasConsultaPorHora = (Integer) hash.get(Constante.cantidadDiasConsultaPorHora);
					ParametrosDn.cantidadDiasConsultaPorDia = (Integer) hash.get(Constante.cantidadDiasConsultaPorDia);
					//
					ParametrosDn.visibleColumnaTotalTrafico = (Boolean) hash.get(Constante.visibleColumnaTotalTrafico);
					ParametrosDn.visibleColumnaDownTrafico = (Boolean) hash.get(Constante.visibleColumnaDownTrafico);
					ParametrosDn.visibleColumnaUpTrafico = (Boolean) hash.get(Constante.visibleColumnaUpTrafico);

					ParametrosDn.unidad = hash.get(Constante.unidad).toString();
					ParametrosDn.visibleUnidad = (Boolean) hash.get(Constante.visibleUnidad);
					ParametrosDn.visibleConsumoPorcentaje = (Boolean) hash.get(Constante.visibleConsumoPorcentaje);

					ParametrosDn.operador = hash.get(Constante.operador).toString();
					ParametrosDn.numero = (Integer) hash.get(Constante.numero);
					ParametrosDn.cantidadDecimales = (Integer) hash.get(Constante.cantidadDecimales);
					ParametrosDn.velocidadTransferencia = hash.get(Constante.velocidadTransferencia).toString();

					ParametrosDn.reintentosTimeOut = (Integer) hash.get(Constante.reintentosTimeOut);
					ParametrosDn.esperaReintentosTimeOut = (Integer) hash.get(Constante.esperaReintentosTimeOut);

					ParametrosDn.textoReporteSize = (Integer) hash.get(Constante.textoReporteSize);

					ParametrosDn.textoReporte = hash.get(Constante.textoReporte).toString();
					ParametrosDn.textoReporteFuente = hash.get(Constante.textoReporteFuente).toString();

					ParametrosDn.topAplicacionesReporte = (Integer) hash.get(Constante.topAplicacionesReporte);

					ParametrosDn.columna1 = hash.get(Constante.columna1).toString();
					ParametrosDn.columna2 = hash.get(Constante.columna2).toString();
					ParametrosDn.columna3 = hash.get(Constante.columna3).toString();
					ParametrosDn.columna4 = hash.get(Constante.columna4).toString();

					ParametrosDn.multiplicadorBytes = (Integer) hash.get(Constante.multiplicadorBytes);
					ParametrosDn.colorReporte = hash.get(Constante.colorReporte).toString();
					// ParametrosDn.sizeArchivoImportacion = (Integer) hash.get(Constante.sizeArchivoImportacion);
					ParametrosDn.visibleGraficoAnchoBanda = (Boolean) hash.get(Constante.visibleGraficoAnchoBanda);
					ParametrosDn.textoHeaderReporte = hash.get(Constante.textoHeaderReporte).toString();
					ParametrosDn.textoHeaderReporteFuente = hash.get(Constante.textoHeaderReporteFuente).toString();
					ParametrosDn.textoHeaderReporteSize = (Integer) hash.get(Constante.textoHeaderReporteSize);
				}
			} else {
				log.error("Parametros no encontrados en la tabla DN_PARAMETRO");
			}

		} catch (RuntimeException e) {
			log.error("Error al cargar parametros: " + e);
		} catch (Exception e) {
			log.error("Error al cargar parametros: ", e);
			// new Throwable(e);
		}
	}

	public static String expresionRegularNroTigo;

	public static String detalleNavegacionWsdl;
	public static Integer detalleNavegacionTimeOut;

	public static Integer cantidadDiasConsultaPorHora;
	public static Integer cantidadDiasConsultaPorDia;

	public static Boolean visibleColumnaTotalTrafico;
	public static Boolean visibleColumnaDownTrafico;
	public static Boolean visibleColumnaUpTrafico;

	public static String unidad;
	public static boolean visibleUnidad;
	public static boolean visibleConsumoPorcentaje;

	public static String operador;
	public static Integer numero;
	public static Integer cantidadDecimales;
	public static String velocidadTransferencia;

	public static int reintentosTimeOut;
	public static int esperaReintentosTimeOut;

	public static String textoReporte;
	public static String textoReporteFuente;
	public static int textoReporteSize;

	public static int topAplicacionesReporte;

	public static String columna1;
	public static String columna2;
	public static String columna3;
	public static String columna4;

	public static int multiplicadorBytes;
	public static String colorReporte;

	public static boolean visibleGraficoAnchoBanda;

	public static String textoHeaderReporte;
	public static String textoHeaderReporteFuente;
	public static int textoHeaderReporteSize;
	// public static int sizeArchivoImportacion;

	// public static synchronized void init() {
	// log.debug("cargando parametros......");
	// try {
	// HashMap<String, Object> hash = obtenerParametros();
	// if (hash != null) {
	//
	// ParametrosDn.expresionRegularNroTigo = hash.get(Constante.expresionRegularNroTigo).toString();
	//
	// ParametrosDn.detalleNavegacionWsdl = hash.get(Constante.detalleNavegacionWsdl).toString();
	// ParametrosDn.detalleNavegacionTimeOut = (Integer) hash.get(Constante.detalleNavegacionTimeOut);
	//
	// ParametrosDn.cantidadDiasConsultaPorHora = (Integer) hash.get(Constante.cantidadDiasConsultaPorHora);
	// ParametrosDn.cantidadDiasConsultaPorDia = (Integer) hash.get(Constante.cantidadDiasConsultaPorDia);
	//
	// ParametrosDn.visibleColumnaTotalTrafico = (Boolean) hash.get(Constante.visibleColumnaTotalTrafico);
	// ParametrosDn.visibleColumnaDownTrafico = (Boolean) hash.get(Constante.visibleColumnaDownTrafico);
	// ParametrosDn.visibleColumnaUpTrafico = (Boolean) hash.get(Constante.visibleColumnaUpTrafico);
	//
	// ParametrosDn.unidad = hash.get(Constante.unidad).toString();
	// ParametrosDn.visibleUnidad = (Boolean) hash.get(Constante.visibleUnidad);
	// ParametrosDn.visibleConsumoPorcentaje = (Boolean) hash.get(Constante.visibleConsumoPorcentaje);
	//
	// ParametrosDn.operador = hash.get(Constante.operador).toString();
	// ParametrosDn.numero = (Integer) hash.get(Constante.numero);
	// ParametrosDn.cantidadDecimales = (Integer) hash.get(Constante.cantidadDecimales);
	// ParametrosDn.velocidadTransferencia = hash.get(Constante.velocidadTransferencia).toString();
	//
	// ParametrosDn.reintentos = (Integer) hash.get(Constante.reintentosTimeOut);
	// ParametrosDn.esperaReintentos = (Integer) hash.get(Constante.esperaReintentosTimeOut);
	//
	// ParametrosDn.textoReporteSize = (Integer) hash.get(Constante.textoReporteSize);
	//
	// ParametrosDn.textoReporte = hash.get(Constante.textoReporte).toString();
	// ParametrosDn.textoReporteFuente = hash.get(Constante.textoReporteFuente).toString();
	//
	// ParametrosDn.topAplicacionesReporte = (Integer) hash.get(Constante.topAplicacionesReporte);
	//
	// ParametrosDn.columna1 = hash.get(Constante.columna1).toString();
	// ParametrosDn.columna2 = hash.get(Constante.columna2).toString();
	// ParametrosDn.columna3 = hash.get(Constante.columna3).toString();
	// ParametrosDn.columna4 = hash.get(Constante.columna4).toString();
	// } else {
	// log.error("No se encontraron parametros de configuracion del sistema");
	// }
	// } catch (Exception e) {
	// log.error("Error al cargar parametros de configuracion: ", e);
	// }
	// }

	// private static HashMap<String, Object> obtenerParametros() throws Exception {
	// HashMap<String, Object> response = new HashMap<String, Object>();
	//
	// Connection conection = getConnection();
	// String sql = "SELECT * FROM DN_PARAMETRO WHERE MODULO = 'WEB' ORDER BY PARAMETRO_ID ASC";
	// Statement st = null;
	// ResultSet rs = null;
	// if (conection != null) {
	// try {
	// st = conection.createStatement();
	// if (st != null) {
	// rs = st.executeQuery(sql);
	// while ((rs != null) && rs.next()) {
	// String nombre = (String) rs.getObject("NOMBRE");
	// String tipoDato = (String) rs.getObject("TIPO_DATO");
	// String valor = (String) rs.getObject("VALOR");
	// log.debug(tipoDato + ": " + nombre + ": " + valor);
	//
	// try {
	// if (tipoDato != null) {
	//
	// if (tipoDato.equals(Constante.STRING)) {
	// response.put(nombre, valor);
	// }
	// if (tipoDato.equals(Constante.ENTERO)) {
	// response.put(nombre, Integer.parseInt(valor));
	// }
	// if (tipoDato.equals(Constante.BOOLEAN)) {
	// response.put(nombre, Boolean.parseBoolean(valor));
	// }
	// if (tipoDato.equals(Constante.DOUBLE)) {
	// response.put(nombre, Double.parseDouble(valor));
	// }
	// if (tipoDato.equals(Constante.LONG)) {
	// response.put(nombre, Long.parseLong(valor));
	// }
	// if (tipoDato.equals(Constante.DATE)) {
	// response.put(nombre, UtilDate.stringToDate(valor, "dd-MM-yyyy HH:mm:ss"));
	// }
	// } else {
	// log.error("Tipo de dato no valido nombre: " + nombre + ", tipoDato: " + tipoDato + ", valor: " + valor);
	// }
	// } catch (Exception e2) {
	// log.error("Error al cargar parametro nombre: " + nombre + ", tipoDato: " + tipoDato + ", valor: " + valor, e2);
	// }
	// }
	// }
	// } finally {
	// try {
	// if (rs != null) {
	// rs.close();
	// }
	// } catch (SQLException ex) {
	// log.error(ex, ex);
	// }
	// try {
	// if (st != null) {
	// st.close();
	// }
	// } catch (SQLException ex) {
	// log.error(ex, ex);
	// }
	// try {
	// if (conection != null) {
	// conection.close();
	// }
	// } catch (SQLException ex) {
	// log.error(ex, ex);
	// }
	// }
	// }
	// return response;
	// }

	// private static Connection getConnection() {
	// Context context;
	// String param = Parametros.dsTusMegasPorAplicacion;
	// try {
	// context = new InitialContext();
	// log.info("*** FacesContext:" + FacesContext.getCurrentInstance());
	// // param = FacesContext.getCurrentInstance().getExternalContext().getInitParameter("dataSource.name.jni");
	// log.debug("dsTusMegasPorAplicacion:" + param);
	// try {
	// return ((javax.sql.DataSource) context.lookup("java:/" + param)).getConnection();
	// } catch (Exception e) {
	// log.error("Fallo al crear la connection: ", e);
	// }
	// } catch (Exception e) {
	// log.error("Fallo al crear el InitialContext: ", e);
	// }
	// return null;
	// }

}
