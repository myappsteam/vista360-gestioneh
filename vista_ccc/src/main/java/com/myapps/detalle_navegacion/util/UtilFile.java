package com.myapps.detalle_navegacion.util;

import org.apache.log4j.Logger;

import java.io.*;

public class UtilFile implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(UtilFile.class);

	public static String fileToString(String pathFile, String font, String size, String fontHeader, String sizeHeader,
			String color) {
		log.debug("pathFile: " + pathFile + ", font: " + font + ", size: " + size + ", fontHeader: " + fontHeader
				+ ", sizeHeader: " + sizeHeader + ", color: " + color);
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder("");
		try {
			// fr = new FileReader(pathFile);
			String sCurrentLine;
			br = new BufferedReader(new InputStreamReader(new FileInputStream(pathFile), "UTF-8"));
			while ((sCurrentLine = br.readLine()) != null) {
				// System.out.println(sCurrentLine);
				String replace = sCurrentLine.trim().replace("\"", "");

				// if (replace.equals("<style name=miestilo fontName=Serif
				// fontSize=8/>")) {
				if (replace.equals("<style name=miestilo fontName=Serif fontSize=8 isBold=true/>")) {
					sCurrentLine = sCurrentLine.replace("Serif", font);
					sCurrentLine = sCurrentLine.replace("8", size);
					// System.out.println(sCurrentLine);
					sCurrentLine = sCurrentLine.replace("#F2F2F2", color);
					sb.append(sCurrentLine);
				} else if (replace.equals(
						"<style name=miestiloheader fontName=SansSerif fontSize=15 pdfFontName=Times-Roman/>")) {
					sCurrentLine = sCurrentLine.replace("SansSerif", fontHeader);
					sCurrentLine = sCurrentLine.replace("15", sizeHeader);
					// System.out.println(sCurrentLine);
					sCurrentLine = sCurrentLine.replace("#F2F2F2", color);
					sb.append(sCurrentLine);
				} else {
					sCurrentLine = sCurrentLine.replace("#F2F2F2", color);
					sb.append(sCurrentLine);
				}
			}
		} catch (Exception e) {
			log.error("Error al obtener contenido string de archivo: ", e);
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.getMessage();
			}
		}
		return sb.toString();
	}

	public static String fileToString(String pathFile) {
		log.debug("pathFile: " + pathFile);
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder("");
		try {
			// fr = new FileReader(pathFile);
			String sCurrentLine;
			br = new BufferedReader(new InputStreamReader(new FileInputStream(pathFile), "UTF-8"));
			while ((sCurrentLine = br.readLine()) != null) {
				// System.out.println(sCurrentLine);
				sb.append(sCurrentLine);
			}
		} catch (Exception e) {
			log.error("Error al obtener contenido string de archivo: ", e);
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.getMessage();
			}
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// String fileToString = UtilFile.fileToString("C:/Users/Jose
		// Luis/Desktop/configuracion/pie/report.jrxml", "Serif", "8");
		// String fileToString = UtilFile.fileToString("E:/report.jrxml",
		// "Serif", "8");
		// System.out.println(fileToString);
	}

}
