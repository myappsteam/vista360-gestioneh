package com.myapps.detalle_navegacion.business;

import com.myapps.detalle_navegacion.dao.MasterDaoDn;
import com.myapps.detalle_navegacion.dao.MasterDaoInterface;
import com.myapps.detalle_navegacion.entity.DnEgnSubProtocol;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
public class EgnSubProtocolBL implements Serializable, MasterDaoInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private MasterDaoDn masterDao;

	@Override
	public String validar(Object entidad, boolean nuevo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(Object entidad) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void remove(Object entidad) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(Object entidad) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public Object find(Object key, Class clase) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List findAll() throws Exception {
		return masterDao.findAllQuery(DnEgnSubProtocol.class, "SELECT d FROM DnEgnSubProtocol d ORDER BY d.nombre ASC", null);
	}

}
