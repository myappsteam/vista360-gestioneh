package com.myapps.detalle_navegacion.business;

import com.myapps.detalle_navegacion.dao.MasterDaoDn;
import com.myapps.detalle_navegacion.dao.MasterDaoInterface;
import com.myapps.detalle_navegacion.entity.DnAplicacion;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
public class AplicacionBL implements Serializable, MasterDaoInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private MasterDaoDn masterDao;

	@Override
	public String validar(Object entidad, boolean nuevo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(Object entidad) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void remove(Object entidad) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(Object entidad) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public Object find(Object key, Class clase) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List findAll() throws Exception {
		return masterDao.findAllQuery(DnAplicacion.class, "SELECT d FROM DnAplicacion d WHERE d.estado = 1 ORDER BY d.nombre ASC", null);
	}

}
