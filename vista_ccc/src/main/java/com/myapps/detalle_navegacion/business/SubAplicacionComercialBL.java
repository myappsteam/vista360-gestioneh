package com.myapps.detalle_navegacion.business;

import com.myapps.detalle_navegacion.dao.MasterDaoDn;
import com.myapps.detalle_navegacion.dao.MasterDaoInterface;
import com.myapps.detalle_navegacion.dao.ParameterQuery;
import com.myapps.detalle_navegacion.entity.DnSubaplicacionComercial;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
public class SubAplicacionComercialBL implements Serializable, MasterDaoInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private MasterDaoDn masterDao;

	@Override
	public String validar(Object entidad, boolean nuevo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(Object entidad) throws Exception {
		masterDao.save(entidad);
	}

	@Override
	public void remove(Object entidad) throws Exception {
		masterDao.remove(entidad);
	}

	@Override
	public void update(Object entidad) throws Exception {
		masterDao.update(entidad);
	}

	@Override
	public Object find(Object key, Class clase) throws Exception {
		return masterDao.find(key, clase);
	}

	@Override
	public List findAll() throws Exception {
		return masterDao.findAllQuery(DnSubaplicacionComercial.class, "SELECT d FROM DnSubaplicacionComercial d WHERE d.estado = 1 ORDER BY d.nombre ASC", null);
	}

	public DnSubaplicacionComercial find(long subAplicacionId) throws Exception {

		String sql = "SELECT r FROM DnSubaplicacionComercial r WHERE r.idSubaplicacion = :subAplicacionId AND r.estado = 1";

		ParameterQuery pq = ParameterQuery.getParameterQuery();
		pq.put("subAplicacionId", subAplicacionId);

		List<DnSubaplicacionComercial> lista = masterDao.findAllQuery(DnSubaplicacionComercial.class, sql, pq);
		return lista.isEmpty() ? null : lista.get(0);
	}

	public void actualizarSubAplicaciones(List<DnSubaplicacionComercial> list, int multiplicador) throws Exception {
		try {

			masterDao.begin();

			for (DnSubaplicacionComercial item : list) {
				item.setLimite(item.getLimiteMb() * multiplicador);
				masterDao.updateOnly(item);
			}
			masterDao.commit();
		} catch (Exception e) {
			masterDao.rollback();
			throw (e);
		}
	}

	public void actualizarConfiguracion(List<DnSubaplicacionComercial> list) throws Exception {
		try {

			masterDao.begin();

			for (DnSubaplicacionComercial item : list) {
				masterDao.saveOnly(item);
			}
			masterDao.commit();
		} catch (Exception e) {
			masterDao.rollback();
			throw (e);
		}
	}
}
