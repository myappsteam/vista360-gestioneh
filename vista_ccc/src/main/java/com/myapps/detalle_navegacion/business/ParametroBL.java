package com.myapps.detalle_navegacion.business;

import com.myapps.detalle_navegacion.dao.MasterDaoDn;
import com.myapps.detalle_navegacion.dao.MasterDaoInterface;
import com.myapps.detalle_navegacion.dao.ParameterQuery;
import com.myapps.detalle_navegacion.entity.DnParametro;
import com.myapps.detalle_navegacion.util.Constante;
import com.myapps.detalle_navegacion.util.UtilDate;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

@Named
public class ParametroBL implements Serializable, MasterDaoInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(ParametroBL.class);

	@Inject
	private MasterDaoDn masterDao;

	@Override
	public String validar(Object entidad, boolean nuevo) {
		if (entidad != null && entidad instanceof DnParametro) {
			DnParametro parametro = (DnParametro) entidad;
			StringBuilder sb = new StringBuilder();
			// if (parametro.getValor() == null || parametro.getValor().trim().isEmpty()) {
			if (parametro.getValor() == null || parametro.getValor().isEmpty()) {
				sb.append("Campo Valor es requerido");
			}

			try {
				if (parametro.getTipoDato().equals(Constante.BOOLEAN)) {
					if (!(parametro.getValor().toUpperCase().equals("TRUE") || parametro.getValor().toUpperCase().equals("FALSE"))) {
						if (sb.length() > 0)
							sb.append(", ");
						sb.append("Campo Valor no es válido");
					}
				} else if (parametro.getTipoDato().equals(Constante.ENTERO)) {
					int parseInt = Integer.parseInt(parametro.getValor().trim());
					if (parseInt < 0) {
						if (sb.length() > 0)
							sb.append(", ");
						sb.append("Campo Valor no es válido");
					}
				} else if (parametro.getTipoDato().equals(Constante.DOUBLE)) {
					Double.parseDouble(parametro.getValor().trim());
				} else if (parametro.getTipoDato().equals(Constante.LONG)) {
					Long.parseLong(parametro.getValor().trim());
				} else if (parametro.getTipoDato().equals(Constante.DATE)) {
					UtilDate.stringToDate(parametro.getValor().trim(), "dd-MM-yyyy HH:mm:ss");
				} else if (parametro.getTipoDato().equals(Constante.STRING)) {
				} else {
					log.warn("Tipo de Dato no resuelto: " + parametro.getTipoDato());
				}

				// if (!parametro.getNombre().equals(Constante.separadorOctetoIp)) {
				// parametro.setValor(parametro.getValor().trim());
				// }

				if (parametro.getNombre() != null && parametro.getNombre().equals("OPERADOR")) {
					String op = parametro.getValor();
					if (!(op.equals("*") || op.equals("/") || op.equals("+") || op.equals("-"))) {
						if (sb.length() > 0)
							sb.append(", ");
						sb.append("El campo Valor no es válido");
					}
				}
			} catch (Exception e) {
				if (sb.length() > 0)
					sb.append(", ");
				sb.append("Campo Valor no es válido");
				e.getMessage();
			}
			return sb.toString();
		} else {
			return "No se pudo validar el objeto.";
		}
	}

	@Override
	public void save(Object entidad) throws Exception {
		masterDao.save(entidad);
	}

	@Override
	public void remove(Object entidad) throws Exception {
		masterDao.remove(entidad);
	}

	@Override
	public void update(Object entidad) throws Exception {
		masterDao.update(entidad);
	}

	@Override
	public Object find(Object key, Class clase) throws Exception {
		return masterDao.find(key, clase);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List findAll() throws Exception {
		return masterDao.findAllQuery(DnParametro.class, "SELECT d FROM DnParametro d WHERE d.modulo = 'WEB' AND d.estado = 1 ORDER BY d.parametroId ASC", null);
	}

	// public List findAllParametros() throws Exception {
	// List<DnParametro> response = new ArrayList<>();
	// List<DnParametro> findAll = findAll();
	// if (findAll != null) {
	// for (DnParametro item : findAll) {
	// if (item.getEstado().intValue() == 1) {
	// response.add(item);
	// }
	// }
	// }
	// return response;
	// }

	// @SuppressWarnings("unchecked")
	// public HashMap<String, Object> obtenerParametros() throws Exception {
	// HashMap<String, Object> response = new HashMap<String, Object>();
	// List<DnParametro> list = findAll();
	// if (list != null) {
	// for (DnParametro item : list) {
	// try {
	//
	// if (item.getTipoDato().equals("STRING")) {
	// response.put(item.getNombre(), item.getValor());
	// }
	// if (item.getTipoDato().equals("ENTERO")) {
	// response.put(item.getNombre(), Integer.parseInt(item.getValor()));
	// }
	// if (item.getTipoDato().equals("BOOLEAN")) {
	// response.put(item.getNombre(), Boolean.parseBoolean(item.getValor()));
	// }
	// if (item.getTipoDato().equals("DOUBLE")) {
	// response.put(item.getNombre(), Double.parseDouble(item.getValor()));
	// }
	// if (item.getTipoDato().equals("LONG")) {
	// response.put(item.getNombre(), Long.parseLong(item.getValor()));
	// }
	// if (item.getTipoDato().equals("DATE")) {
	// response.put(item.getNombre(), UtilDate.stringToDate(item.getValor(), "dd-MM-yyyy HH:mm:ss"));
	// }
	// } catch (Exception e) {
	// log.error("Error al cargar parametro nombre: " + item.getNombre() + ", tipoDato: " + item.getTipoDato() + ", valor: " + item.getValor(), e);
	// new Throwable(e);
	// }
	// }
	// }
	//
	// return response;
	// }

	public DnParametro getParametroValor(String name) throws Exception {

		String sql = "SELECT d FROM DnParametro d WHERE d.nombre = :name AND d.modulo = 'WEB' AND d.estado = 1";

		ParameterQuery pq = ParameterQuery.getParameterQuery();
		pq.put("name", name);

		List<DnParametro> lista = masterDao.findAllQuery(DnParametro.class, sql, pq);
		return lista.isEmpty() ? null : lista.get(0);
	}

	public Object getParametro2(String name) throws Exception {

		DnParametro item = getParametroValor(name);
		Object response = null;
		if (item != null) {

			try {

				if (item.getTipoDato().equals("STRING")) {
					response = item.getValor();
				}
				if (item.getTipoDato().equals("ENTERO")) {
					response = Integer.parseInt(item.getValor());
				}
				if (item.getTipoDato().equals("BOOLEAN")) {
					response = Boolean.parseBoolean(item.getValor());
				}
				if (item.getTipoDato().equals("DOUBLE")) {
					response = Double.parseDouble(item.getValor());
				}
				if (item.getTipoDato().equals("LONG")) {
					response = Long.parseLong(item.getValor());
				}
				if (item.getTipoDato().equals("DATE")) {
					response = UtilDate.stringToDate(item.getValor(), "dd-MM-yyyy HH:mm:ss");
				}
			} catch (Exception e) {
				log.error("Error al cargar parametro nombre: " + item.getNombre() + ", tipoDato: " + item.getTipoDato() + ", valor: " + item.getValor(), e);
				new Throwable(e);
			}
		}
		return response;
		// HashMap<String, Object> hash = obtenerParametros();
		// return hash.get(name);
	}

	public HashMap<String, Object> obtenerParametros2(List<String> lista) throws Exception {
		HashMap<String, Object> response = new HashMap<String, Object>();
		if (lista != null) {
			List<DnParametro> findAll = findAll();
			if (findAll != null) {
				for (DnParametro item : findAll) {
					try {
						if (item.getTipoDato().equals("STRING")) {
							response.put(item.getNombre(), item.getValor());
						}
						if (item.getTipoDato().equals("ENTERO")) {
							response.put(item.getNombre(), Integer.parseInt(item.getValor()));
						}
						if (item.getTipoDato().equals("BOOLEAN")) {
							response.put(item.getNombre(), Boolean.parseBoolean(item.getValor()));
						}
						if (item.getTipoDato().equals("DOUBLE")) {
							response.put(item.getNombre(), Double.parseDouble(item.getValor()));
						}
						if (item.getTipoDato().equals("LONG")) {
							response.put(item.getNombre(), Long.parseLong(item.getValor()));
						}
						if (item.getTipoDato().equals("DATE")) {
							response.put(item.getNombre(), UtilDate.stringToDate(item.getValor(), "dd-MM-yyyy HH:mm:ss"));
						}
					} catch (Exception e) {
						log.error("Error al cargar parametro nombre: " + item.getNombre() + ", tipoDato: " + item.getTipoDato() + ", valor: " + item.getValor(), e);
						new Throwable(e);
					}
				}
			}
		}
		return response;
	}
}
