package com.myapps.detalle_navegacion.business;

import com.myapps.detalle_navegacion.bean.model.Aplicacion;
import com.myapps.detalle_navegacion.bean.model.Categoria;
import com.myapps.detalle_navegacion.bean.model.EgnSubProtocol;
import com.myapps.detalle_navegacion.bean.model.SubAplicacion;
import com.myapps.detalle_navegacion.dao.MasterDaoDn;
import com.myapps.detalle_navegacion.dao.MasterDaoInterface;
import com.myapps.detalle_navegacion.entity.DnAplicacion;
import com.myapps.detalle_navegacion.entity.DnCategoria;
import com.myapps.detalle_navegacion.entity.DnEgnSubProtocol;
import com.myapps.detalle_navegacion.entity.DnSubaplicacion;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Named
public class CategoriaBL implements Serializable, MasterDaoInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(CategoriaBL.class);

	@Inject
	private MasterDaoDn masterDao;

	@Override
	public String validar(Object entidad, boolean nuevo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(Object entidad) throws Exception {
		masterDao.save(entidad);
	}

	@Override
	public void remove(Object entidad) throws Exception {
		masterDao.remove(entidad);
	}

	@Override
	public void update(Object entidad) throws Exception {
		masterDao.update(entidad);
	}

	@Override
	public Object find(Object key, Class clase) throws Exception {
		return masterDao.find(key, clase);
	}

	@Override
	public List findAll() throws Exception {
		return masterDao.findAllQuery(DnCategoria.class, "SELECT d FROM DnCategoria d ORDER BY d.nombre ASC", null);
	}

	// public String guardarAplicaciones(String jsonCategorias, String jsonAplicaciones, String jsonSubAplicaciones) {
	public void guardarAplicaciones(List<Categoria> listCategoria, List<Aplicacion> listAplicacion, List<SubAplicacion> listSubAplicacion, List<EgnSubProtocol> listEgnSubProtocol) throws Exception {

		try {

			masterDao.begin();

			// masterDao.executeUpdateNativeQuery("DELETE FROM DN_EGN_SUB_PROTOCOL", null);
			// masterDao.executeUpdateNativeQuery("DELETE FROM DN_SUBAPLICACION", null);
			// masterDao.executeUpdateNativeQuery("DELETE FROM DN_APLICACION", null);
			// masterDao.executeUpdateNativeQuery("DELETE FROM DN_CATEGORIA", null);
			masterDao.executeUpdateNativeQueryOnly("DELETE FROM DN_EGN_SUB_PROTOCOL", null);
			masterDao.executeUpdateNativeQueryOnly("DELETE FROM DN_SUBAPLICACION", null);
			masterDao.executeUpdateNativeQueryOnly("DELETE FROM DN_APLICACION", null);
			masterDao.executeUpdateNativeQueryOnly("DELETE FROM DN_CATEGORIA", null);

			// ObjectMapper mapper = new ObjectMapper();
			// List<Categoria> listCategoria = mapper.readValue(jsonCategorias, TypeFactory.defaultInstance().constructCollectionType(List.class, Categoria.class));
			// List<Aplicacion> listAplicacion = mapper.readValue(jsonAplicaciones, TypeFactory.defaultInstance().constructCollectionType(List.class, Aplicacion.class));
			// List<SubAplicacion> listSubAplicacion = mapper.readValue(jsonSubAplicaciones, TypeFactory.defaultInstance().constructCollectionType(List.class, SubAplicacion.class));

			if (listCategoria != null) {
				for (Categoria item : listCategoria) {
					DnCategoria dc = new DnCategoria();
					dc.setCategoriaId(item.getId());
					dc.setNombre(item.getNombre());
					dc.setEstado(1);
					dc.setFechaCreacion(new Date());
					// masterDao.save(dc);
					masterDao.saveOnly(dc);
				}
			}
			if (listAplicacion != null) {
				for (Aplicacion item : listAplicacion) {
					DnAplicacion da = new DnAplicacion();
					da.setAplicacionId(item.getId());
					da.setNombre(item.getNombre());
					da.setEstado(1);
					da.setFechaCreacion(new Date());
					DnCategoria dnCategoria = (DnCategoria) masterDao.find(item.getCategoriaId(), DnCategoria.class);
					da.setDnCategoria(dnCategoria);

					// log.debug(ToStringBuilder.reflectionToString(da));
					// log.debug(ToStringBuilder.reflectionToString(dnCategoria));
					// masterDao.save(da);
					masterDao.saveOnly(da);
				}
			}
			if (listSubAplicacion != null) {
				for (SubAplicacion item : listSubAplicacion) {
					DnSubaplicacion ds = new DnSubaplicacion();
					ds.setSubaplicacionId(item.getId());
					ds.setNombre(item.getNombre());
					ds.setEstado(1);
					ds.setFechaCreacion(new Date());
					DnAplicacion dnAplicacion = (DnAplicacion) masterDao.find(item.getAplicacionId(), DnAplicacion.class);
					ds.setDnAplicacion(dnAplicacion);
					// masterDao.save(ds);
					masterDao.saveOnly(ds);
				}
			}
			if (listEgnSubProtocol != null) {
				for (EgnSubProtocol item : listEgnSubProtocol) {
					DnEgnSubProtocol de = new DnEgnSubProtocol();
					de.setEgnSubProtocolId(item.getId());
					de.setNombre(item.getNombre());
					de.setEstado(1);
					de.setFechaCreacion(new Date());
					DnSubaplicacion dnSubAplicacion = (DnSubaplicacion) masterDao.find(item.getSubAplicacionId(), DnSubaplicacion.class);
					de.setDnSubaplicacion(dnSubAplicacion);
					// masterDao.save(de);
					masterDao.saveOnly(de);
				}
			}
			masterDao.commit();
		} catch (Exception e) {
			masterDao.rollback();
			log.error("Error al guardar aplicaciones: ", e);
			throw (e);
		}
	}

	public void obtenerAplicaciones(List<Categoria> listCategoria, List<Aplicacion> listAplicacion, List<SubAplicacion> listSubAplicacion, List<EgnSubProtocol> listEgnSubProtocol) throws Exception {

		List<DnCategoria> rowCategoria = masterDao.findAllQuery(DnCategoria.class, "SELECT d FROM DnCategoria d ORDER BY d.fechaCreacion ASC", null);
		List<DnAplicacion> rowAplicacion = masterDao.findAllQuery(DnAplicacion.class, "SELECT d FROM DnAplicacion d ORDER BY d.fechaCreacion ASC", null);
		List<DnSubaplicacion> rowSubAplicacion = masterDao.findAllQuery(DnSubaplicacion.class, "SELECT d FROM DnSubaplicacion d ORDER BY d.fechaCreacion ASC", null);
		List<DnEgnSubProtocol> rowEgnSubProtocol = masterDao.findAllQuery(DnEgnSubProtocol.class, "SELECT d FROM DnEgnSubProtocol d ORDER BY d.fechaCreacion ASC", null);

		// List<Categoria> listCategoria = new ArrayList<>();
		// List<Aplicacion> listAplicacion = new ArrayList<>();
		// List<SubAplicacion> listSubAplicacion = new ArrayList<>();

		if (rowCategoria != null) {
			for (DnCategoria item : rowCategoria) {
				listCategoria.add(new Categoria(item.getCategoriaId(), item.getNombre()));
			}
		}
		if (rowAplicacion != null) {
			for (DnAplicacion item : rowAplicacion) {
				listAplicacion.add(new Aplicacion(item.getAplicacionId(), item.getNombre(), item.getDnCategoria().getCategoriaId(), item.getDnCategoria().getNombre()));
			}
		}
		if (rowSubAplicacion != null) {
			for (DnSubaplicacion item : rowSubAplicacion) {
				listSubAplicacion.add(new SubAplicacion(item.getSubaplicacionId(), item.getNombre(), item.getDnAplicacion().getAplicacionId(), item.getDnAplicacion().getNombre(), item.getDnAplicacion().getDnCategoria().getCategoriaId(), item.getDnAplicacion().getDnCategoria().getNombre()));
			}
		}
		if (rowEgnSubProtocol != null) {
			for (DnEgnSubProtocol item : rowEgnSubProtocol) {
				listEgnSubProtocol.add(new EgnSubProtocol(item.getEgnSubProtocolId(), item.getNombre(), item.getDnSubaplicacion().getSubaplicacionId(), item.getDnSubaplicacion().getNombre(), item.getDnSubaplicacion().getDnAplicacion().getAplicacionId(),
						item.getDnSubaplicacion().getDnAplicacion().getNombre(), item.getDnSubaplicacion().getDnAplicacion().getDnCategoria().getCategoriaId(), item.getDnSubaplicacion().getDnAplicacion().getDnCategoria().getNombre()));
			}
		}
	}

}
