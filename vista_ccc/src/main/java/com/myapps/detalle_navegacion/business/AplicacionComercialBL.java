package com.myapps.detalle_navegacion.business;

import com.myapps.detalle_navegacion.dao.MasterDaoDn;
import com.myapps.detalle_navegacion.dao.MasterDaoInterface;
import com.myapps.detalle_navegacion.dao.ParameterQuery;
import com.myapps.detalle_navegacion.entity.DnAplicacionComercial;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
public class AplicacionComercialBL implements Serializable, MasterDaoInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private MasterDaoDn masterDao;

	@Override
	public String validar(Object entidad, boolean nuevo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(Object entidad) throws Exception {
		masterDao.save(entidad);
	}

	@Override
	public void remove(Object entidad) throws Exception {
		masterDao.remove(entidad);
	}

	@Override
	public void update(Object entidad) throws Exception {
		masterDao.update(entidad);
	}

	@Override
	public Object find(Object key, Class clase) throws Exception {
		return masterDao.find(key, clase);
	}

	@Override
	public List findAll() throws Exception {
		return masterDao.findAllQuery(DnAplicacionComercial.class, "SELECT d FROM DnAplicacionComercial d WHERE d.estado = 1 ORDER BY d.nombre ASC", null);
	}

	public DnAplicacionComercial find(long aplicacionId) throws Exception {

		String sql = "SELECT r FROM DnAplicacionComercial r WHERE r.idAplicacion = :aplicacionId AND r.estado = 1";

		ParameterQuery pq = ParameterQuery.getParameterQuery();
		pq.put("aplicacionId", aplicacionId);

		List<DnAplicacionComercial> lista = masterDao.findAllQuery(DnAplicacionComercial.class, sql, pq);
		return lista.isEmpty() ? null : lista.get(0);
	}

	public void actualizarAplicaciones(List<DnAplicacionComercial> list, int multiplicador) throws Exception {
		try {

			masterDao.begin();

			for (DnAplicacionComercial item : list) {
				item.setLimite(item.getLimiteMb() * multiplicador);
				masterDao.updateOnly(item);
			}
			masterDao.commit();
		} catch (Exception e) {
			masterDao.rollback();
			throw (e);
		}
	}
	
	public void actualizarConfiguracion(List<DnAplicacionComercial> list) throws Exception {
		try {

			masterDao.begin();

			for (DnAplicacionComercial item : list) {
				masterDao.saveOnly(item);
			}
			masterDao.commit();
		} catch (Exception e) {
			masterDao.rollback();
			throw (e);
		}
	}
}
