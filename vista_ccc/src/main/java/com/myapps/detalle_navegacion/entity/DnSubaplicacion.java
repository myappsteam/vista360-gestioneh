package com.myapps.detalle_navegacion.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the DN_SUBAPLICACION database table.
 * 
 */
@Entity
@Table(name="DN_SUBAPLICACION")
@NamedQuery(name="DnSubaplicacion.findAll", query="SELECT d FROM DnSubaplicacion d")
public class DnSubaplicacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="SUBAPLICACION_ID", unique=true, nullable=false, precision=19)
	private long subaplicacionId;

	@Column(length=1000)
	private String descripcion;

	@Column(nullable=false, precision=1)
	private int estado;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CREACION", nullable=false)
	private Date fechaCreacion;

	@Column(nullable=false, length=200)
	private String nombre;

	//bi-directional many-to-one association to DnAplicacion
	@ManyToOne
	@JoinColumn(name="APLICACION_ID", nullable=false)
	private DnAplicacion dnAplicacion;

	public DnSubaplicacion() {
	}

	public long getSubaplicacionId() {
		return this.subaplicacionId;
	}

	public void setSubaplicacionId(long subaplicacionId) {
		this.subaplicacionId = subaplicacionId;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getEstado() {
		return this.estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public DnAplicacion getDnAplicacion() {
		return this.dnAplicacion;
	}

	public void setDnAplicacion(DnAplicacion dnAplicacion) {
		this.dnAplicacion = dnAplicacion;
	}

}