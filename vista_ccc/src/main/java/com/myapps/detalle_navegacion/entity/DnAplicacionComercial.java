package com.myapps.detalle_navegacion.entity;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the DN_APLICACION_COMERCIAL database table.
 * 
 */
@Entity
@Table(name="DN_APLICACION_COMERCIAL")
@NamedQuery(name="DnAplicacionComercial.findAll", query="SELECT d FROM DnAplicacionComercial d")
public class DnAplicacionComercial implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="DN_APLICACION_COMERCIAL_ID_GENERATOR", sequenceName="SEQ_DN_APLICACION_COMERCIAL", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DN_APLICACION_COMERCIAL_ID_GENERATOR")
	private long id;

	private int estado;

	@Column(name="ID_APLICACION")
	private long idAplicacion;

	private long limite;

	@Column(name="LIMITE_MB")
	private long limiteMb;

	private String nombre;

	@Column(name="NOMBRE_COMERCIAL")
	private String nombreComercial;

	private boolean visible;

	public DnAplicacionComercial() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getEstado() {
		return this.estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public long getIdAplicacion() {
		return this.idAplicacion;
	}

	public void setIdAplicacion(long idAplicacion) {
		this.idAplicacion = idAplicacion;
	}

	public long getLimite() {
		return this.limite;
	}

	public void setLimite(long limite) {
		this.limite = limite;
	}

	public long getLimiteMb() {
		return this.limiteMb;
	}

	public void setLimiteMb(long limiteMb) {
		this.limiteMb = limiteMb;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombreComercial() {
		return this.nombreComercial;
	}

	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}

	public boolean getVisible() {
		return this.visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

}