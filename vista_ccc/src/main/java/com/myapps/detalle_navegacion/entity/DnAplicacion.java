package com.myapps.detalle_navegacion.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the DN_APLICACION database table.
 * 
 */
@Entity
@Table(name="DN_APLICACION")
@NamedQuery(name="DnAplicacion.findAll", query="SELECT d FROM DnAplicacion d")
public class DnAplicacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="APLICACION_ID", unique=true, nullable=false, precision=19)
	private long aplicacionId;

	@Column(length=1000)
	private String descripcion;

	@Column(nullable=false, precision=1)
	private int estado;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CREACION", nullable=false)
	private Date fechaCreacion;

	@Column(nullable=false, length=200)
	private String nombre;

	//uni-directional many-to-one association to DnCategoria
	@ManyToOne
	@JoinColumn(name="CATEGORIA_ID", nullable=false)
	private DnCategoria dnCategoria;

	//bi-directional many-to-one association to DnSubaplicacion
//	@OneToMany(mappedBy="dnAplicacion")
//	private List<DnSubaplicacion> dnSubaplicacions;

	public DnAplicacion() {
	}

	public long getAplicacionId() {
		return this.aplicacionId;
	}

	public void setAplicacionId(long aplicacionId) {
		this.aplicacionId = aplicacionId;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getEstado() {
		return this.estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public DnCategoria getDnCategoria() {
		return this.dnCategoria;
	}

	public void setDnCategoria(DnCategoria dnCategoria) {
		this.dnCategoria = dnCategoria;
	}

//	public List<DnSubaplicacion> getDnSubaplicacions() {
//		return this.dnSubaplicacions;
//	}

//	public void setDnSubaplicacions(List<DnSubaplicacion> dnSubaplicacions) {
//		this.dnSubaplicacions = dnSubaplicacions;
//	}

//	public DnSubaplicacion addDnSubaplicacion(DnSubaplicacion dnSubaplicacion) {
//		getDnSubaplicacions().add(dnSubaplicacion);
//		dnSubaplicacion.setDnAplicacion(this);
//
//		return dnSubaplicacion;
//	}

//	public DnSubaplicacion removeDnSubaplicacion(DnSubaplicacion dnSubaplicacion) {
//		getDnSubaplicacions().remove(dnSubaplicacion);
//		dnSubaplicacion.setDnAplicacion(null);
//
//		return dnSubaplicacion;
//	}

}