package com.myapps.detalle_navegacion.entity;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the DN_MONITOR database table.
 * 
 */
@Entity
@Table(name="DN_MONITOR")
@NamedQuery(name="DnMonitor.findAll", query="SELECT d FROM DnMonitor d")
public class DnMonitor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="MONITOR_ID")
	private long monitorId;

	private String parametro;

	private String valor;

	public DnMonitor() {
	}

	public long getMonitorId() {
		return this.monitorId;
	}

	public void setMonitorId(long monitorId) {
		this.monitorId = monitorId;
	}

	public String getParametro() {
		return this.parametro;
	}

	public void setParametro(String parametro) {
		this.parametro = parametro;
	}

	public String getValor() {
		return this.valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

}