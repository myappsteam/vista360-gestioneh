package com.myapps.detalle_navegacion.entity;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the DN_PARAMETRO database table.
 * 
 */
@Entity
@Table(name="DN_PARAMETRO")
@NamedQuery(name="DnParametro.findAll", query="SELECT d FROM DnParametro d")
public class DnParametro implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PARAMETRO_ID", unique=true, nullable=false, precision=19)
	private long parametroId;

	@Column(length=500)
	private String descripcion;

	@Column(length=200)
	private String nombre;

	private Integer estado;
	
	@Column(name="TIPO_DATO", nullable=false, length=50)
	private String tipoDato;

	@Column(name="MODULO", nullable=false, length=50)
	private String modulo;
	
	@Column(nullable=false, length=200)
	private String valor;

	public DnParametro() {
	}

	public long getParametroId() {
		return this.parametroId;
	}

	public void setParametroId(long parametroId) {
		this.parametroId = parametroId;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipoDato() {
		return this.tipoDato;
	}

	public void setTipoDato(String tipoDato) {
		this.tipoDato = tipoDato;
	}

	public String getValor() {
		return this.valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getModulo() {
		return modulo;
	}

	public void setModulo(String modulo) {
		this.modulo = modulo;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

}