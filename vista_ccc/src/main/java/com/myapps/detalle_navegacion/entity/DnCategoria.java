package com.myapps.detalle_navegacion.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the DN_CATEGORIA database table.
 * 
 */
@Entity
@Table(name="DN_CATEGORIA")
@NamedQuery(name="DnCategoria.findAll", query="SELECT d FROM DnCategoria d")
public class DnCategoria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CATEGORIA_ID", unique=true, nullable=false, precision=19)
	private long categoriaId;

	@Column(length=1000)
	private String descripcion;

	@Column(nullable=false, precision=1)
	private int estado;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CREACION", nullable=false)
	private Date fechaCreacion;

	@Column(nullable=false, length=200)
	private String nombre;

	public DnCategoria() {
	}

	public long getCategoriaId() {
		return this.categoriaId;
	}

	public void setCategoriaId(long categoriaId) {
		this.categoriaId = categoriaId;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getEstado() {
		return this.estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}