package com.myapps.detalle_navegacion.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the DN_CONSULTA database table.
 * 
 */
@Entity
@Table(name="DN_CONSULTA")
@NamedQuery(name="DnConsulta.findAll", query="SELECT d FROM DnConsulta d")
public class DnConsulta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="DN_CONSULTA_CONSULTAID_GENERATOR", sequenceName="SEQ_DN_CONSULTA", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DN_CONSULTA_CONSULTAID_GENERATOR")
	@Column(name="CONSULTA_ID", unique=true, nullable=false, precision=19)
	private long consultaId;

	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date fecha;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CREACION", nullable=false)
	private Date fechaCreacion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_FIN")
	private Date fechaFin;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_INICIO", nullable=false)
	private Date fechaInicio;

	@Column(nullable=false, length=20)
	private String ip;

	@Column(nullable=false, length=10)
	private String isdn;
	
	@Column(nullable=false, length=10)
	private String tiempo;
	
	@Column(nullable=false, length=20)
	private String categoria;
	
	@Column(nullable=false, length=100)
	private String reporte;

	@Column(nullable=false, length=50)
	private String usuario;

	public DnConsulta() {
	}

	public long getConsultaId() {
		return this.consultaId;
	}

	public void setConsultaId(long consultaId) {
		this.consultaId = consultaId;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaFin() {
		return this.fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Date getFechaInicio() {
		return this.fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getIsdn() {
		return this.isdn;
	}

	public void setIsdn(String isdn) {
		this.isdn = isdn;
	}

	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getTiempo() {
		return tiempo;
	}

	public void setTiempo(String tiempo) {
		this.tiempo = tiempo;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getReporte() {
		return reporte;
	}

	public void setReporte(String reporte) {
		this.reporte = reporte;
	}

}