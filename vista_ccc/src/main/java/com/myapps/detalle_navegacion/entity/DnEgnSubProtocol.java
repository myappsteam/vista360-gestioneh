package com.myapps.detalle_navegacion.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the DN_EGN_SUB_PROTOCOL database table.
 * 
 */
@Entity
@Table(name="DN_EGN_SUB_PROTOCOL")
@NamedQuery(name="DnEgnSubProtocol.findAll", query="SELECT d FROM DnEgnSubProtocol d")
public class DnEgnSubProtocol implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="EGN_SUB_PROTOCOL_ID", unique=true, nullable=false, precision=19)
	private long egnSubProtocolId;

	@Column(length=1000)
	private String descripcion;

	@Column(nullable=false, precision=1)
	private int estado;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CREACION", nullable=false)
	private Date fechaCreacion;

	@Column(nullable=false, length=200)
	private String nombre;

	//uni-directional many-to-one association to DnSubaplicacion
	@ManyToOne
	@JoinColumn(name="DN_SUBAPLICACION_ID", nullable=false)
	private DnSubaplicacion dnSubaplicacion;

	public DnEgnSubProtocol() {
	}

	public long getEgnSubProtocolId() {
		return this.egnSubProtocolId;
	}

	public void setEgnSubProtocolId(long egnSubProtocolId) {
		this.egnSubProtocolId = egnSubProtocolId;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getEstado() {
		return this.estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public DnSubaplicacion getDnSubaplicacion() {
		return this.dnSubaplicacion;
	}

	public void setDnSubaplicacion(DnSubaplicacion dnSubaplicacion) {
		this.dnSubaplicacion = dnSubaplicacion;
	}

}