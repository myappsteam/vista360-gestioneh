package com.myapps.detalle_navegacion.facade;

import com.myapps.detalle_navegacion.entity.DnParametro;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class ParametroFacade extends AbstractFacade<DnParametro> {

	@PersistenceContext(unitName = "detalle_navegacion")
	private EntityManager em;

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	public ParametroFacade() {
		super(DnParametro.class);
	}

}
