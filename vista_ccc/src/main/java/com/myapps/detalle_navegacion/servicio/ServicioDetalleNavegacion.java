package com.myapps.detalle_navegacion.servicio;

import com.myapps.detalle_navegacion.util.UtilUrl;
import com.myapps.detalle_navegacion_ws.ws.DetalleNavegacionServiceLocator;
import com.myapps.detalle_navegacion_ws.ws.DetalleNavegacionServiceSoapBindingStub;
import com.myapps.vista_ccc.servicio.NodoServicio;
import com.myapps.vista_ccc.util.Parametros;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

//import com.myapps.detalle_navegacion_ws.ws.DetalleNavegacionServiceLocator;

public class ServicioDetalleNavegacion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static Logger log = Logger.getLogger(ServicioDetalleNavegacion.class);

	private static Map<String, NodoServicio<DetalleNavegacionServiceSoapBindingStub>> ports = new HashMap<String, NodoServicio<DetalleNavegacionServiceSoapBindingStub>>();

	public static synchronized NodoServicio<DetalleNavegacionServiceSoapBindingStub> initPort(String url, String username, int timeout) throws Exception {

		long ini = System.currentTimeMillis();
		url = UtilUrl.getIp(url);
		long fin = System.currentTimeMillis();
		log.debug("[username: " + username + ", url: " + url + "] Tiempo de respuesta para obtener ip DetalleNavegacionService: " + (fin - ini) + " milisegundos");

		ini = System.currentTimeMillis();

		NodoServicio<DetalleNavegacionServiceSoapBindingStub> port = ports.get(username);

		if (port == null) {
			log.debug("Creando un nuevo port ... " + username);
			port = getService(url, username, timeout);
			port.setPrimeraVez(true);
		} else {
			log.debug("Ya existe el port creado ... " + username);
			port.setEnUso(true);
			port.setFechaInicio(new Date());
			port.setFechaFin(null);
			port.setPrimeraVez(false);
		}
		fin = System.currentTimeMillis();
		
		port.setTiempoConexion(fin-ini);
		
		log.debug("[username: " + username + "] Tiempo de respuesta para obtener conexion DetalleNavegacionService: " + (fin - ini) + " milisegundos");

		return port;
	}

	public static synchronized NodoServicio<DetalleNavegacionServiceSoapBindingStub> getService(String url, String username, int timeout) throws Exception {

		NodoServicio<DetalleNavegacionServiceSoapBindingStub> port = ports.get(username);

		if (port == null) {

			DetalleNavegacionServiceLocator service = new DetalleNavegacionServiceLocator(url);

			log.debug("url DetalleNavegacion: " + service.getDetalleNavegacionPortAddress());

			DetalleNavegacionServiceSoapBindingStub portStub = (DetalleNavegacionServiceSoapBindingStub) service.getDetalleNavegacionPort();
			portStub.setTimeout(timeout);

			port = new NodoServicio<DetalleNavegacionServiceSoapBindingStub>();
			port.setPort(portStub);
			port.setKey(username);

			port.setEnUso(true);
			port.setFechaInicio(new Date());
			port.setFechaFin(null);

			ports.put(username, port);

		}

		return port;
	}

	public static synchronized void liberar(String key) {
		if (ports != null) {
			synchronized (ports) {
				ArrayList<String> listaEliminar = new ArrayList<>();

				for (Entry<String, NodoServicio<DetalleNavegacionServiceSoapBindingStub>> entry : ports.entrySet()) {
					if (entry.getKey().endsWith(key)) {
						listaEliminar.add(entry.getKey());
					}
				}

				int sizeAntes = ports.size();
				for (String item : listaEliminar) {
					ports.remove(item);
				}
				int sizeDespues = ports.size();

				log.debug("Eliminados del hashmap DetalleNavegacionService [key: " + key + "][size antes: " + sizeAntes + ", size despues: " + sizeDespues + "]: " + listaEliminar.size() + " conexiones : " + listaEliminar);
			}
		}
	}

	public static synchronized void liberarPorTimeOut() {
		if (ports != null) {
			synchronized (ports) {
				ArrayList<String> listaEliminar = new ArrayList<>();

				for (Entry<String, NodoServicio<DetalleNavegacionServiceSoapBindingStub>> entry : ports.entrySet()) {

					if (!entry.getValue().isEnUso() && entry.getValue().getFechaFin() != null) {
						long milisegundos = (new Date()).getTime() - entry.getValue().getFechaFin().getTime();
						long segundos = milisegundos / 1000;
						long minutos = segundos / 60;

						if (minutos >= Parametros.timpoFueraPoolConexion) {
							listaEliminar.add(entry.getKey());
						}
					}
				}

				int sizeAntes = ports.size();
				for (String item : listaEliminar) {
					ports.remove(item);
				}
				int sizeDespues = ports.size();

				log.debug("Eliminados por time out de la cola de conexiones activas DetalleNavegacionService [size antes: " + sizeAntes + ", size despues: " + sizeDespues + "]: " + listaEliminar.size() + " conexiones : " + listaEliminar);
			}
		}
	}
	
	public static synchronized void removeAll() {
		if (ports != null) {
			synchronized (ports) {
				int sizeAntes = ports.size();
				ports.clear();
				log.debug("Eliminados del hashmap DetalleNavegacionService [size antes: " + sizeAntes + ", size despues: " + ports.size() + "]: ");
			}
		}
	}

}
